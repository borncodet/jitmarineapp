import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { AsyncComponent } from './_components/general/AsyncComponent';

const AuthorizedRoute = React.lazy(() => import('./_components/login/AuthorizedRoute'));
const UnauthorizedLayout = React.lazy(() => import('./_components/login/UnauthorizedLayout'));

export class RoutePaths {
  public static Home = '/';
  public static SignIn = '/auth/login/';
  public static UserManagement = "/user-management";
  public static AudioManagement = "/audio-management";
  public static CategoryManagement = "/category-management";
  public static MembershipTypes = "/membershiptypes";
  public static PublisherManagement = "/publisher-management";
  public static NarratorManagement = "/narrator-management";
  public static AuthorManagement = "/author-management";
  public static LanguageManagement = "/language-management";
  public static ProductManagement="/product-management";
  public static PromotionManagement="/promotion-management";
}

const handleUnauthorizedLayout = (props: any) => {
  return <AsyncComponent>
    <UnauthorizedLayout {...props} />
  </AsyncComponent>;
};

export const routes = (
  <Switch>
    <Route path="/auth" render={handleUnauthorizedLayout} />
    <AuthorizedRoute path="/" />
    <Redirect to="/auth" />
  </Switch>
);


import axios from 'axios';
import * as qs from 'querystring';
import ConfigurationService from './ConfigurationService';

class EndpointFactory {
    private readonly _loginUrl: string = '/connect/token';

    private get loginUrl() { return ConfigurationService.tokenUrl + this._loginUrl; }

    public async getLoginEndpoint<T>(userName: string, password: string) {
        const header = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'access-control-allow-origin': 'http://192.168.1.10:3000'
        };
        const params = {
            'username': userName,
            'password': password,
            'grant_type': 'password',
            'scope': 'openid email phone profile offline_access roles audiobook_api',
            'resource': window.location.origin,
            'login_hint': 'admin',
            'client_secret': 'AudioBook@2019',
            'client_id': 'audio_book_spa'
        };
        return axios.post<T>(this.loginUrl, qs.stringify(params), { headers: header });
    }

    public async loadData<T>(url: string, data: string) {
        const header = {
            'Content-Type': "application/json",
            'Authorization':"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJlNjI0NWZlYi1mNzNlLTRiODMtYmQ4NC1mZDExOTE2NmM5OTciLCJuYW1lIjoibmlzaGlsYTIiLCJyb2xlIjoiU2l0ZUFkbWluaXN0cmF0b3IiLCJwZXJtaXNzaW9uIjpbInVzZXJzLnZpZXciLCJ1c2Vycy5tYW5hZ2UiLCJyb2xlcy52aWV3Iiwicm9sZXMubWFuYWdlIiwicm9sZXMuYXNzaWduIiwibWVtYmVycy52aWV3IiwibWVtYmVycy5tYW5hZ2UiLCJ1c2VyLnZpZXciLCJtZW1iZXIudmlldyIsImFsbC52aWV3Il0sInRva2VuX3VzYWdlIjoiYWNjZXNzX3Rva2VuIiwianRpIjoiMTJmODI2OWYtNWVkMS00N2MyLTk0YTktZTVkMmFmZjUwYjg4Iiwic2NvcGUiOlsib3BlbmlkIiwiZW1haWwiLCJwaG9uZSIsInByb2ZpbGUiLCJvZmZsaW5lX2FjY2VzcyIsInJvbGVzIl0sIm5iZiI6MTU2NzQxMzQwMiwiZXhwIjoxNTk4OTQ5NDAyLCJpYXQiOjE1Njc0MTM0MDIsImlzcyI6Imh0dHA6Ly9haS5ib3JuY29kZXRlY2hub2xvZ2llcy5jb20vIn0.JBl29wTrOCyqvDfjOh1Hxckgyn31opmmfgX3EY3zf9w"
        };
        const response = await axios.post(url, data, { headers: header });
        return response.data.data as T;
    }
}

export default new EndpointFactory();
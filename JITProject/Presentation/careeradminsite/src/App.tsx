import React from 'react';

export interface IAppProps {
  children: React.ReactNode;
}

const App = (props: IAppProps) => {
  return <>
    {props.children}
  </>
}

export default App;
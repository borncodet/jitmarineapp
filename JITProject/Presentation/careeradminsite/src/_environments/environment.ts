export const environment = {
  production: false,
  baseUrl: "http://192.168.1.10:4444",
  //baseUrl: "https://localhost:44338", // Change this to the address of your backend API if different from frontend address
  tokenUrl: "https://localhost:9090", // For IdentityServer/Authorization Server API. You can set to null if same as baseUrl
  loginUrl: '/login'
};

export const AppUrls = {
  NarratorGetAllUrl: "/api/narrator/gaaa",

  LanguageGetAllUrl: environment.baseUrl + "/api/language/gaaa",
  LanguageCreateOrUpdateUrl: environment.baseUrl + "/api/language/coea",
  LanguageDeleteUrl: environment.baseUrl + "/api/language/da",
  LanguageGetUrl: environment.baseUrl + "/api/language/ga",

  AuthorGetAllUrl: environment.baseUrl + "/api/author/gaaa",
  AuthorCreateOrUpdateUrl: environment.baseUrl + "/api/author/coea",
  AuthorDeleteUrl: environment.baseUrl + "/api/author/da",
  AuthorGetUrl: environment.baseUrl + "/api/author/ga",
}


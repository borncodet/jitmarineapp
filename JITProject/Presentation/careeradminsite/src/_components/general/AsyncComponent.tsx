import * as React from 'react';
import { ReactNode } from 'react';

interface IAsyncComponentProps {
  children: ReactNode;
}

export class AsyncComponent extends React.Component<IAsyncComponentProps> {
  public render() {
    return (
      <React.Suspense fallback={<div className="loader" />}>
        {this.props.children}
      </React.Suspense>
    );
  }
}

import * as React from 'react';
import { User } from '../../_models/User';

export interface IAppContextInterface {
  isLoggedIn: boolean;
  loggedUser: User | null;
  updateUser: () => void;
}

export interface IAppContextWrapperComponentProps {
  appContext?: IAppContextInterface;
}

const defaultContextState = { isLoggedIn: false, loggedUser: null } as IAppContextInterface;

export const AppContext = React.createContext<IAppContextInterface>(defaultContextState);

export const AppContextProvider = AppContext.Provider;

const AppContextConsumer = AppContext.Consumer;

export function withAppContext<C extends React.ComponentClass>(
  Component: C
): C {
  return (((props: any) => (
    <AppContextConsumer>
      {context => <Component {...props} appContext={context} />}
    </AppContextConsumer>
  )) as any) as C;
}

export const setTitle = (title: string) => (
  Component: React.ComponentClass | React.StatelessComponent
) => {
  return (class extends React.Component {
    public componentDidMount() {
      document.title = title;
    }
    public render() {
      return <Component {...this.props} />;
    }
  } as any) as typeof Component;
};

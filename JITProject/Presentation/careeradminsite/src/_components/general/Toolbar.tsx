import * as React from 'react';
import { Button } from 'antd';

export interface IToolbarButtonProps {
    label?: string;
    onClick: () => void;
    align: 'left' | 'right';
}

export interface IToolbarProps {
    buttons: IToolbarButtonProps[];
}

const Toolbar = (props?: IToolbarProps) => {
    let leftButtons: IToolbarButtonProps[] = [];
    let rightButtons: IToolbarButtonProps[] = [];

    if (props) {
        leftButtons = props.buttons.filter(x => x.align === 'left');
        rightButtons = props.buttons.filter(x => x.align === 'right');
    }

    return <div>
        {
            leftButtons.length > 0 ? <div className="left-panel-button">
                {
                    leftButtons.map((value: IToolbarButtonProps, index: number) => {
                        return <Button key={index} onClick={value.onClick}>{value.label}</Button>
                    })
                }
            </div> : null
        }
        {
            rightButtons.length > 0 ? <div className="">
                {
                    rightButtons.map((value: IToolbarButtonProps, index: number) => {
                        return <Button key={index} onClick={value.onClick}>{value.label}</Button>
                    })
                }
            </div> : null
        }
    </div>
}

export default Toolbar;
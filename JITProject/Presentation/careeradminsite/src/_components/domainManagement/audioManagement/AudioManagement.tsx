import * as React from 'react';
import 'antd/dist/antd.css';
import { Row } from 'antd';
// import axios from 'axios';
import { IAppContextWrapperComponentProps } from "../../general/AppContext";
import { RouteComponentProps } from 'react-router-dom';
import AddToAudioForm from './AddToAudioForm';
import { AudioCard } from './AudioCard';

interface IAudioManagementProps extends IAppContextWrapperComponentProps, RouteComponentProps<any> {
    index: string;
}

interface IAudioManagementState {
    isBusy: boolean;
    Audios: any[];
}

export class AudioManagement extends React.Component<IAudioManagementProps, IAudioManagementState>{

    constructor(props: IAudioManagementProps) {
        super(props)

        this.state = {
            isBusy: true,
            Audios: []
        }
    }

    public componentDidMount() {
        this.getData();
    }

    public render() {
        return (
            <>
                <div className="sub-header-01">
                    <AddToAudioForm />
                </div>
                <div className="gutter-example" style={{ paddingLeft: 85 }}>
                    <Row gutter={32}>
                        <AudioCard Audios={this.state.Audios} />
                    </Row>
                </div>
            </>
        )
    }

    private getData = () => {
        // const url = "";
        // axios.get(url).then(res => {
        // console.log(13, res);

        this.setState({
            // Audios: res.data
            Audios: [
                {
                    Id: "",
                    Title: "ABC",
                    Description: "Abcdefg"
                }
            ]
        });
        // });
    }

}
import * as React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { AudioModelForm } from '../../../_models/AudioFormModel';
const { TextArea } = Input;

interface IAudioFormProps extends FormComponentProps {


}

interface IAudioFormstate {
  AudioForms: AudioModelForm
}

class AudioForm extends React.Component<IAudioFormProps, IAudioFormstate> {
  constructor(props: IAudioFormProps) {
    super(props);
    this.state = {
      AudioForms: new AudioModelForm()
    }
  }

  public render() {

    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} >
        <Form.Item key="1" label="Title">
          {getFieldDecorator('Title', {
            rules: [{ required: true, message: 'Please input title!' }],
          })(<Input placeholder="Title"
            name="title"
            onChange={(e) => this.handleInputChange(e)}
          />)}
        </Form.Item>
        <Form.Item key="2" label="SubTitle">
          {getFieldDecorator('SubTitle', {
            rules: [{ required: true, message: 'Please input title!' }],
          })(<Input placeholder="Title"
            name="subTitle"
            onChange={(e) => this.handleInputChange(e)}
          />)}
        </Form.Item>
        <Form.Item key="3" label="Description">
          {getFieldDecorator('Description', {
            rules: [{ required: true, message: 'Please input  description!' }],
          })(<TextArea rows={4}
            placeholder="Description"
            name="description"
            onChange={(e: any) => this.handleInputChange(e)}
          />)}
        </Form.Item>
        <Button style={{ marginRight: 8 }}>
          Close
            </Button>
        <Button htmlType="submit" type="primary">
          Save
            </Button>
        <Button   >
          Save and Continue
            </Button>
      </Form>
    )
  }

  private handleSubmit = (e: any) => {
    console.log(12, e)
    e.preventDefault();
    this.props.form.validateFields((err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  private handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    const AudioFormUpdates = {
      [name]: value
    };
    console.log(11, AudioFormUpdates);
    this.setState({ AudioForms: Object.assign(this.state.AudioForms, AudioFormUpdates) as AudioModelForm });
  }

}
export const AddAudioForm = Form.create<IAudioFormProps>()(AudioForm);
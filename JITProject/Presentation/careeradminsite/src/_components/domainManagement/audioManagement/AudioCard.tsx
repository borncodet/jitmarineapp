import * as React from 'react';
import 'antd/dist/antd.css';
import { Card, Icon, } from 'antd';
import { Col } from 'antd';

interface IAudioCardProps {

    Audios: any[]
}

interface IAudioCardState {
    isBusy: boolean;
}

export class AudioCard extends React.Component<IAudioCardProps, IAudioCardState>{

    public render() {
        return (
            this.props.Audios.map((Audio, index) => {
                return (<Col className="gutter-row" key={index} style={{ width: 250, paddingTop: 18 }} span={6}>
                    <div className="gutter-box" >
                        <Card
                            cover={<img
                                alt="example"
                                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
                            actions={[<Icon key="1" type="edit" />, <Icon key="2" type="delete" />]}>
                            <div className="card-body" style={{}}>
                                <h2 className="card-title">{Audio.Title}</h2>
                                <h3 className="card-text">{Audio.Description}</h3>
                                <p className="card-text">Some Description about Audio.</p>
                            </div>
                        </Card>
                    </div>
                </Col>)
            })


        );
    }
}
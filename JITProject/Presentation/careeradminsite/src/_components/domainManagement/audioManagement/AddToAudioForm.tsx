import * as React from 'react';
import 'antd/dist/antd.css';
import { Tooltip } from 'antd';
import { RouteComponentProps, Route, MemoryRouter } from "react-router-dom"
import { Button, Icon } from 'antd';
import { AddAudioForm } from "./AudioForm";
import { withRouter } from "react-router";
import Toolbar, { IToolbarButtonProps } from '../../general/Toolbar';

interface IAddToAudioFormProps extends RouteComponentProps<{}> {
    key?: string;
}

interface IAddToAudioFormState {
    visible: boolean;
    LeftSide: any[];
}

class AddToAudioForm extends React.Component<IAddToAudioFormProps, IAddToAudioFormState> {

    constructor(props: IAddToAudioFormProps) {
        super(props);
        this.state = {
            visible: false,
            LeftSide: [
                { save: "string" },
                { cancel: "string" }
            ]
        }
    }

    handleClick = () => {
        console.log('AddtoAudioForm Click 1');
    }

    get getToolbarButtons(): IToolbarButtonProps[] {
        return [{
            onClick: this.handleClick,
            align: 'left',
            label: 'Add'
        }, {
            onClick: this.handleClick,
            align: 'right',
            label: 'Close'
        }];
    }

    public render() {
        const { match } = this.props;
        return (
            <>
                <Tooltip placement="topLeft" title="Add Audio">
                    <Button type="default" onClick={() => this.goToForm()}>
                        <Icon type="plus" />
                    </Button>
                </Tooltip>
                <Toolbar buttons={this.getToolbarButtons} />
                <Route path={`${match.path}/create-new`} render={this.audioCreateForm} />
            </>
        );
    }

    private audioCreateForm = (props: RouteComponentProps<any>) => {
        return (<MemoryRouter>
            <Route component={AddAudioForm} />
        </MemoryRouter>)
    }

    private goToForm = () => {
        this.props.history.push('/create-new')
    }

}
export default withRouter(AddToAudioForm)
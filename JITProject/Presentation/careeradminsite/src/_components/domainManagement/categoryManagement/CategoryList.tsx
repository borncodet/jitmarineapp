import * as React from 'react';
import 'antd/dist/antd.css';
import { List } from 'antd';

interface ICategoryListProps {

  Categories: any[]
}

interface ICategoryListState {
  isBusy: boolean;
}

export class CategoryList extends React.Component<ICategoryListProps, ICategoryListState>{

  public render() {
    return (
      <List
        itemLayout="horizontal"
        dataSource={this.props.Categories}
        renderItem={item => (
          <List.Item>
            <List.Item.Meta
              title={item.title}
              description={item.description}
            />
          </List.Item>
        )}
      />
    );
  }
}
import * as React from 'react';
import 'antd/dist/antd.css';
import { Row } from 'antd';
// import axios from 'axios';
import { IAppContextWrapperComponentProps } from "../../general/AppContext";
import { RouteComponentProps } from 'react-router-dom';
import { AddToCategoryForm } from './AddToCategoryForm';
import { CategoryList } from './CategoryList';
import axios from "axios";

interface ICategoryManagementProps extends IAppContextWrapperComponentProps, RouteComponentProps<any> {
    index: string;
}

interface ICategoryManagementState {
    isBusy: boolean;
    Categories: any[];
    searchTerm: string;
}

export class CategoryManagement extends React.Component<ICategoryManagementProps, ICategoryManagementState>{

    constructor(props: ICategoryManagementProps) {
        super(props)

        this.state = {
            isBusy: true,
            Categories: [],
            searchTerm: "",
        }
    }

    public componentDidMount() {
        this.getData();
    }

    public render() {
        return (
            <>
                <div className="sub-header-01" style={{ textAlign: 'right', paddingRight: 9 }}>
                    <AddToCategoryForm getData={this.getData} />
                </div>
                <div className="gutter-example" style={{ paddingLeft: 85 }}>
                    <Row gutter={32}>
                        <CategoryList Categories={this.state.Categories} />
                    </Row>
                </div>
            </>
        )
    }

    private getData = () => {
        console.log(14);
        let header = {
            'Content-Type': "application/json",
        };
        let formData = new FormData();
        formData.append('searchTerm', this.state.searchTerm);
        const url = "https://localhost:44338/api/category/gaaa";
        axios.post(url, JSON.stringify(formData), { headers: header }).then(res => {
            console.log(13, res.data);

            this.setState({
                Categories: res.data.data
            });
        });
    }

}
import * as React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { CategoryModelForm } from '../../../_models/CategoryModelForm';
import axios from "axios";
const { TextArea } = Input;

interface ICategoryFormProps extends FormComponentProps {
  onClose: () => any,
  getData: () => any
}

interface ICategoryFormstate {
  CategoryForms: CategoryModelForm;
}

class CategoryForm extends React.Component<ICategoryFormProps, ICategoryFormstate> {
  constructor(props: ICategoryFormProps) {
    super(props);
    this.state = {
      CategoryForms: { active: true, id: "", title: "", description: "" } as CategoryModelForm
    }

  }

  public render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} >
        <Form.Item key="1" label="Title">
          {getFieldDecorator('Title', {
            rules: [{ required: true, message: 'Please input title!' }],
          })(<Input placeholder="Title"
            name="title"
            onChange={(e) => this.handleInputChange(e)}
          />)}
        </Form.Item>
        <Form.Item key="2" label="Description">
          {getFieldDecorator('Description', {
            rules: [{ required: false, message: 'Please input  description!' }],
          })(<TextArea rows={4} style={{ resize: "none" }}
            placeholder="Description"
            name="description"
            onChange={(e: any) => this.handleInputChange(e)}
          />)}
        </Form.Item>

        <Button onClick={this.onCloser} style={{ marginRight: 12, marginLeft: 150, marginTop: 260 }}>
          Cancel
            </Button>
        <Button htmlType="submit" onClick={this.onCloser} type="primary">
          Submit
            </Button>
      </Form>
    )
  }

  private handleSubmit = (e: any) => {
    console.log(12, e)
    e.preventDefault();
    this.props.form.validateFields((err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.CreateOrUpdateCategory();
      }
    });
  };

  private CreateOrUpdateCategory = () => {
    console.log(15);
    let header = {
      'Content-Type': "application/json",
    };
    const { id, title, description, active } = this.state.CategoryForms;
    let data = JSON.stringify({
      Id: id,
      Title: title,
      Description: description,
      Active: active
    })

    axios.post('https://localhost:44338/api/category/coea', data, { headers: header })
      .then((res) => {
        let result = res.data;
        console.log(result);
        this.props.getData();
      })
  }

  private onCloser = () => {
    this.props.onClose();
  };

  private handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    const CategoryFormUpdates = {
      [name]: value
    };
    console.log(11, CategoryFormUpdates);
    this.setState({ CategoryForms: Object.assign(this.state.CategoryForms, CategoryFormUpdates) as CategoryModelForm });
  }

}
export const AddCategoryForm = Form.create<ICategoryFormProps>()(CategoryForm);
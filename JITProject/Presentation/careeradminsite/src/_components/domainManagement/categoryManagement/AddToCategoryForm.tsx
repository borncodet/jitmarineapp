import * as React from 'react';
import 'antd/dist/antd.css';
import { Tooltip } from 'antd';
import { Drawer, Button, Icon } from 'antd';
import { AddCategoryForm } from "./CategoryForm";

interface IAddToCategoryFormProps {
    key?: string;
    getData: () => any
}

interface IAddToCategoryFormState {
    visible: boolean;
}

export class AddToCategoryForm extends React.Component<IAddToCategoryFormProps, IAddToCategoryFormState> {

    public state = { visible: false };

    public render() {
        return (
            <>
                <Tooltip placement="topLeft" title="Add Category">
                    <Button type="default" onClick={this.showDrawer}>
                        <Icon type="plus" />
                    </Button>
                </Tooltip>
                <Drawer
                    title="Add a new Category"
                    width={650}
                    onClose={this.onClose}
                    visible={this.state.visible}>
                    <AddCategoryForm 
                    onClose={this.onClose}
                    getData={this.props.getData}
                     />
                </Drawer>
            </>
        );
    }

    private onClose = () => {
        this.setState({
            visible: false,
        });
    };

    private showDrawer = () => {
        this.setState({
            visible: true,
        });
    };
}
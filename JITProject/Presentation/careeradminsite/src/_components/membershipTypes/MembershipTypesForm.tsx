import * as React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { MembershiptypeModelForm } from '../../_models/MembershipTypeModelForm';
import axios from "axios";
const { TextArea } = Input;

interface IMembershipTypesFormProps extends FormComponentProps {
  onClose: () => any

}

interface IMembershipTypesFormstate {
  MembershipTypesForm: MembershiptypeModelForm
}

class MembershipTypesForm extends React.Component<IMembershipTypesFormProps, IMembershipTypesFormstate> {
  constructor(props: IMembershipTypesFormProps) {
    super(props);
    this.state = {
      MembershipTypesForm: {active: true, id: "",title:"",description:""} as MembershiptypeModelForm
    }

  }

  public render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} >
        <Form.Item key="1" label="Title">
          {getFieldDecorator('title', {
            rules: [{ required: true, message: 'Please input title!' }],
          })(<Input placeholder="Title"
            name="title"
            onChange={(e: any) => this.handleInputChange(e)}
          />)}
        </Form.Item>
        <Form.Item key="2" label="Description">
          {getFieldDecorator('description', {
            rules: [{ required: true, message: 'Please input  description!' }],
          })(<TextArea rows={4} style={{ resize: "none" }}
            placeholder="Description"
            onChange={(e: any) => this.handleInputChange(e)}
            name="description" />)}
        </Form.Item>
        <Button onClick={this.onCloser} style={{ marginRight: 12, marginLeft: 150, marginTop: 260 }}>
          Cancel
            </Button>
        <Button htmlType="submit" type="primary">
          Submit
            </Button>
      </Form>
    )
  }

  private handleSubmit = (e: any) => {
    console.log(12, e)
    e.preventDefault();
    this.props.form.validateFields((err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.CreateOrUpdateMembershipType();
      }
    });
  };

  private CreateOrUpdateMembershipType = () => {
    let formData = new FormData();
    const { id, title, description, active }  = this.state.MembershipTypesForm;
    formData.append('id', id ? id : " ");
    formData.append('title', title ? title : "");
    formData.append('description', description ? description : "");
    formData.append('active', active ? "true" : "false");
    axios.post('', formData)
      .then((res) => {
        let result = res.data;
        console.log(result);
      })
  }

  private onCloser = () => {
    this.props.onClose();
  };

  private handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    const MembershipTypesFormUpdates = {
      [name]: value
    };
    console.log(11, MembershipTypesFormUpdates);
    this.setState({ MembershipTypesForm: Object.assign(this.state.MembershipTypesForm, MembershipTypesFormUpdates) as MembershiptypeModelForm });
  }
}
export const AddMembershipTypesForm = Form.create<IMembershipTypesFormProps>()(MembershipTypesForm);
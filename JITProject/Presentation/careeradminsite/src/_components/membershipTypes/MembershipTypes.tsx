import * as React from 'react';
import 'antd/dist/antd.css';
import { Row } from 'antd';
 import axios from 'axios';
import { IAppContextWrapperComponentProps } from "../general/AppContext";
import { RouteComponentProps } from 'react-router-dom';
import { AddToMembershipTypesForm } from './AddToMembershipTypesForm';
import { MembershipTypesList } from './MembershipTypesList';

interface IMembershipTypesProps extends IAppContextWrapperComponentProps, RouteComponentProps<any> {
    index: string;
}

interface IMembershipTypesState {
    isBusy: boolean;
    MembershipTypes: any[];
}

export class MembershipTypes extends React.Component<IMembershipTypesProps, IMembershipTypesState>{

    constructor(props: IMembershipTypesProps) {
        super(props)

        this.state = {
            isBusy: true,
            MembershipTypes: []
        }
    }

    public componentDidMount() {
        this.getData();
    }

    public render() {
        return (
            <>
                <div className="sub-header-01" style={{ textAlign: 'right', paddingRight: 9 }}>
                    <AddToMembershipTypesForm />
                </div>
                <div className="gutter-example" style={{ paddingLeft: 85 }}>
                    <Row gutter={32}>
                        <MembershipTypesList MembershipTypes={this.state.MembershipTypes} />
                    </Row>
                </div>
            </>
        )
    }

    private getData = () => {
        const url = "";
         axios.get(url).then(res => {
         console.log(13, res);

        this.setState({
            // Audios: res.data
            MembershipTypes: [
                {
                    Id: "",
                    Title: "ABC",
                    Description: "Abcdefg"
                }
            ]
        });
         });
    }

}
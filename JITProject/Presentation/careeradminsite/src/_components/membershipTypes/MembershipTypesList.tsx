import * as React from 'react';
import 'antd/dist/antd.css';
import { List } from 'antd';

interface IMembershipTypesListProps {

  MembershipTypes: any[]
}

interface IMembershipTypesListState {
  isBusy: boolean;
}

export class MembershipTypesList extends React.Component<IMembershipTypesListProps, IMembershipTypesListState>{
  public render() {
    return (
      <List
        itemLayout="horizontal"
        dataSource={this.props.MembershipTypes}
        renderItem={item => (
          <List.Item>
            <List.Item.Meta
              title={item.Title}
              description={item.description}
            />
          </List.Item>
        )}
      />
    );
  }
}
import * as React from 'react';
import 'antd/dist/antd.css';
import { Tooltip } from 'antd';
import { Drawer, Button, Icon } from 'antd';
import { AddMembershipTypesForm } from "./MembershipTypesForm";

interface IAddToMembershipTypesFormProps {
    key?: string;
}

interface IAddToMembershipTypesFormState {
    visible: boolean;
}

export class AddToMembershipTypesForm extends React.Component<IAddToMembershipTypesFormProps, IAddToMembershipTypesFormState> {

    public state = { visible: false };

    public render() {
        return (
            <>
                <Tooltip placement="topLeft" title="Add MembershipTypes">
                    <Button type="default" onClick={this.showDrawer}>
                        <Icon type="plus" />
                    </Button>
                </Tooltip>
                <Drawer
                    title="Add a new MembershipTypes"
                    width={650}
                    onClose={this.onClose}
                    visible={this.state.visible}>
                    <AddMembershipTypesForm onClose={this.onClose} />
                </Drawer>
            </>
        );
    }

    private onClose = () => {
        this.setState({
            visible: false,
        });
    };

    private showDrawer = () => {
        this.setState({
            visible: true,
        });
    };
}
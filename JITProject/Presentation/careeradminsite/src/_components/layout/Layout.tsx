import * as React from "react";
import "../../_assets/css/app.css";
import 'antd/dist/antd.css';
import { RouteComponentProps, Switch, Route, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import { AsyncComponent } from "../general/AsyncComponent";
import { IAppContextWrapperComponentProps } from "../general/AppContext";
import { UserManagement } from '../userManagement/UserManagement';
import { AudioManagement } from '../domainManagement/audioManagement/AudioManagement';
import { MembershipTypes } from '../membershipTypes/MembershipTypes';
import LanguageManagement from "../../_app/pages/LanguageManagement";
import { AppNavigation } from '../app/AppNavigation';
import { AppHeader } from '../app/AppHeader';
import { LanguageContextProvider, NarratorContextProvider, PublisherContextProvider, AuthorContextProvider, CategoryContextProvider, PromotionContextProvider, ProductContextProvider } from "../../_app/contexts";
import NarratorManagement from "_app/pages/NarratorManagement";
import PublisherManagement from "_app/pages/PublisherManagement";
import AuthorManagement from "_app/pages/AuthorManagement";
import CategoryManagement from "_app/pages/CategoryManagement";
import PromotionManagement from "_app/pages/PromotionManagement";
import { ProductManagementRoute } from "_app/pages";
import { PromotionMain } from "_app/containers/promotion";
const { Content, Footer, Sider } = Layout;
const Home = React.lazy(() => import('../home/Home'));

export interface ILayoutProps
    extends IAppContextWrapperComponentProps,
    RouteComponentProps<any> {
    children?: React.ReactNode;
    classes: any;
}

class AdminLayout extends React.Component<ILayoutProps, { collapsed: boolean }> {

    constructor(props: ILayoutProps) {
        super(props);

        this.state = {
            collapsed: false
        }
    }

    public render() {
        const { match } = this.props;
        const { pathname } = this.props.location;

        const renderHome = (homeProps: any) => {
            return <AsyncComponent>
                <Home {...homeProps} />
            </AsyncComponent>;
        };

        return (
            <Layout>
                <AppHeader />
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider width={250}
                        collapsible={true}
                        collapsed={this.state.collapsed}
                        onCollapse={this.onCollapse}
                        theme="light">
                        <AppNavigation pathname={pathname} />
                    </Sider>
                    <Layout>
                        <Content>
                            <Switch>
                                <Route path={`${match.path}`} exact={true} render={renderHome} />
                                <Route path={`${match.path}user-management`} exact={true} component={UserManagement} />
                                <Route path={`${match.path}audio-management`} exact={true} component={AudioManagement} />
                                <Route path={`${match.path}membershiptypes`} exact={true} component={MembershipTypes} />
                                <Route
                                    path={`${match.path}language-management`}
                                    exact={true}
                                    render={(props) => {
                                        return <LanguageContextProvider>
                                            <LanguageManagement {...props} />
                                        </LanguageContextProvider>
                                    }} />
                                <Route
                                    path={`${match.path}narrator-management`}
                                    exact={true}
                                    render={(props) => {
                                        return <NarratorContextProvider>
                                            <NarratorManagement {...props} />
                                        </NarratorContextProvider>
                                    }}
                                />
                                <Route
                                    path={`${match.path}publisher-management`}
                                    exact={true}
                                    render={(props) => {
                                        return <PublisherContextProvider>
                                            <PublisherManagement {...props} />
                                        </PublisherContextProvider>
                                    }}
                                />
                                <Route
                                    path={`${match.path}author-management`}
                                    exact={true}
                                    render={(props) => {
                                        return <AuthorContextProvider>
                                            <AuthorManagement {...props} />
                                        </AuthorContextProvider>
                                    }}
                                />
                                <Route
                                    path={`${match.path}category-management`}
                                    exact={true}
                                    render={(props) => {
                                        return <CategoryContextProvider>
                                            <CategoryManagement {...props} />
                                        </CategoryContextProvider>
                                    }}
                                />
                                <Route
                                    exact={true}
                                    path={`${match.path}promotion-management/add-new`}
                                    render={(props) => {
                                        return <ProductContextProvider>
                                         <PromotionContextProvider>
                                            <PromotionMain {...props} />
                                        </PromotionContextProvider>
                                        </ProductContextProvider>
                                    }} />
                                {/* {PromotionManagementRoute(match)} */}
                                <Route
                                    exact={true}
                                    path={`${match.path}promotion-management/edit/:id`}
                                    render={(props) => {
                                        return <ProductContextProvider>
                                        <PromotionContextProvider>
                                            <PromotionMain {...props} />
                                        </PromotionContextProvider>
                                        </ProductContextProvider>
                                    }} />
                                <Route
                                    exact={true}
                                    path={`${match.path}promotion-management`}
                                    render={(props) => {
                                        return <ProductContextProvider>
                                        <PromotionContextProvider>
                                            <PromotionManagement {...props} />
                                        </PromotionContextProvider>
                                        </ProductContextProvider>
                                    }}
                                />
                                {ProductManagementRoute(match)}

                                <Redirect to={`${match.url}`} />
                            </Switch>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            AudioBook
                        </Footer>
                    </Layout>
                </Layout>
            </Layout>

        );
    }

    private onCollapse = (collapsed: boolean) => {
        this.setState({ collapsed });
    }

}
export default AdminLayout;

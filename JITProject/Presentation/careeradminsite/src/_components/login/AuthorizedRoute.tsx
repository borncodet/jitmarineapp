import * as React from 'react';
import { IAppContextInterface, withAppContext } from '../general/AppContext';
import {  Route, RouteProps } from 'react-router-dom';
import { AsyncComponent } from '../general/AsyncComponent';
import Layout from '../layout/Layout';
// import AuthService from '../../_services/AuthService';

interface IAuthorizedRouteProps extends RouteProps {
  appContext?: IAppContextInterface;
}

@withAppContext
export default class AuthorizedRoute extends React.Component<
IAuthorizedRouteProps
> {
  public render() {
    const { component: Component, ...rest } = this.props;
    return <Route {...rest} render={this.handleRedirect} />;
  }

  private handleRedirect = (props: any) => {
    return ( <AsyncComponent>
          <Layout {...props} />
         </AsyncComponent>);
    // return AuthService.isLoggedIn ? (
    //   <AsyncComponent>
    //     <Layout {...props} />
    //   </AsyncComponent>
    // ) : (
    //     <Redirect to="/auth/login" />
    //   );
  };
}

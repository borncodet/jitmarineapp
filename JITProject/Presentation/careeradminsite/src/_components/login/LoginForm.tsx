import * as React from 'react';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import AuthService from '../../_services/AuthService';
import { RouteComponentProps } from 'react-router-dom';

interface ILoginFormProps extends FormComponentProps, RouteComponentProps<any> { }

const LoginForm = (props: ILoginFormProps) => {
    const { getFieldDecorator } = props.form;

    React.useEffect(() => {
        if (AuthService.isLoggedIn) {
            AuthService.logout();
        }
    });

    const handleSubmit = async (e: any) => {
        e.preventDefault();
        props.form.validateFields(async (err: any, values: any) => {
            if (!err) {
                try {
                    const user = await AuthService.login(values.username, values.password, values.remember);
                    if (user) {
                        props.history.replace("/");
                    }
                    else {
                        props.history.replace("/");
                    }
                } catch (error) {
                    console.log('Error occurred while login' + error);
                }
            }
        });
    };

    return (
        <>
            <Row>
                <Col span={9}></Col>
                <Col span={8} >
                    <Form onSubmit={handleSubmit} className="login-form">
                        <Form.Item>
                            {getFieldDecorator('username', {
                                initialValue: "nishila2",
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="Username"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                initialValue: "Audio@2019",
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    type="password"
                                    placeholder="Password"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(<Checkbox>Remember me</Checkbox>)}
                            <a className="login-form-forgot" href="/">
                                Forgot password
                            </a>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                            Or <a href="/">register now!</a>
                        </Form.Item>
                    </Form>
                </Col>
                <Col span={7}></Col>
            </Row>
        </>
    )
}

export const Login = Form.create<ILoginFormProps>()(LoginForm);
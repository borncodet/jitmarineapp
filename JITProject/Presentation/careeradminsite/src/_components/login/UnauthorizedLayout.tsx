import * as React from 'react';

import { Redirect, Route, Switch, RouteComponentProps } from 'react-router-dom';

import { Login } from './LoginForm';

const handleAuthPageRender = (props: RouteComponentProps<any>) => {
  return <Login {...props} />;
};

const UnauthorizedLayout = () => (
  <>
    <Switch>
       <Route path="/auth/login" render={handleAuthPageRender} /> 
      <Redirect to="/auth/login" />
    </Switch>
  </>
);

export default UnauthorizedLayout;

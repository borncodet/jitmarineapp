import * as React from 'react';
import 'antd/dist/antd.css';
import { RoutePaths } from '../../routes';
import { NavLink } from 'react-router-dom';
import { Menu, Icon } from 'antd';
const { SubMenu } = Menu;

interface IAppNavigationProps {
    pathname: string;
}

interface IAppNavigationState {
    defaultOpenKeys: string[];
    selectedKeys: string[];
}

export class AppNavigation extends React.Component<IAppNavigationProps, IAppNavigationState>{

    constructor(props: IAppNavigationProps) {
        super(props);
        this.state = { defaultOpenKeys: [], selectedKeys: [] }
    }

    componentDidMount() {
        this.setState(() => {
            return {
                selectedKeys: this.getSelectedKey,
                defaultOpenKeys: this.getSelectedOpenKey
            }
        });
    }

    public render() {
        const { defaultOpenKeys, selectedKeys } = this.state;
        return (
            <>
                <Menu mode="inline" defaultOpenKeys={defaultOpenKeys} selectedKeys={selectedKeys}>
                    <Menu.Item key="1" onClick={() => this.setSelectedKey = '/'}>
                        <NavLink to="/">
                            <Icon type="home" />
                            <span>Home</span>
                        </NavLink>
                    </Menu.Item>
                    <Menu.Item key="2" onClick={() => this.setSelectedKey = RoutePaths.UserManagement}>
                        <NavLink key="user-management" to={RoutePaths.UserManagement}>
                            <Icon type="bars" />
                            <span>User Management</span>
                        </NavLink>
                    </Menu.Item>
                    <SubMenu
                        key="domain-sub-menu"
                        title={
                            <span>
                                <Icon type="team" />
                                <span>Domain Management</span>
                            </span>
                        }
                    >
                        <Menu.Item key="3" onClick={() => this.setSelectedKey = RoutePaths.AudioManagement}>
                            <NavLink key="audio-management" to={RoutePaths.AudioManagement}>
                                <Icon type="bars" />
                                <span>Audio Management</span>
                            </NavLink>
                        </Menu.Item>
                    </SubMenu>
                    <Menu.Item key="5">
                        <NavLink key="membershiptypes" to={RoutePaths.MembershipTypes}>
                            <Icon type="usergroup-add" />
                            <span>Membership Types</span>
                        </NavLink>
                    </Menu.Item>
                    <SubMenu
                        key="product-sub-menu"
                        title={
                            <span>
                                <Icon type="read" />
                                <span>Product Management</span>
                            </span>
                        }
                    >
                        <Menu.Item key="10" onClick={() => this.setSelectedKey = RoutePaths.ProductManagement}>
                            <NavLink key="product-management" to={RoutePaths.ProductManagement}>
                                <Icon type="book" />
                                <span>Product Management</span>
                            </NavLink>
                        </Menu.Item>
                        <Menu.Item key="6" onClick={() => this.setSelectedKey = RoutePaths.AuthorManagement}>
                            <NavLink key="author-management" to={RoutePaths.AuthorManagement}>
                                <Icon type="edit" />
                                <span>Author Management</span>
                            </NavLink>
                        </Menu.Item>
                        <Menu.Item key="7" onClick={() => this.setSelectedKey = RoutePaths.PublisherManagement}>
                            <NavLink key="publisher-management" to={RoutePaths.PublisherManagement}>
                                <Icon type="book" />
                                <span>Publisher Management</span>
                            </NavLink>
                        </Menu.Item>
                        <Menu.Item key="8" onClick={() => this.setSelectedKey = RoutePaths.NarratorManagement}>
                            <NavLink key="narrator-management" to={RoutePaths.NarratorManagement}>
                                <Icon type="sound" />
                                <span>Narrator Management</span>
                            </NavLink>
                        </Menu.Item>
                        <Menu.Item key="4" onClick={() => this.setSelectedKey = RoutePaths.CategoryManagement}>
                            <NavLink key="category-management" to={RoutePaths.CategoryManagement}>
                                <Icon type="unordered-list" />
                                <span>Category Management</span>
                            </NavLink>
                        </Menu.Item>
                        <Menu.Item key="9" onClick={() => this.setSelectedKey = RoutePaths.LanguageManagement}>
                            <NavLink key="category-management" to={RoutePaths.LanguageManagement}>
                                <Icon type="font-size" />
                                <span>Language Management</span>
                            </NavLink>
                        </Menu.Item>

                    </SubMenu>
                    <Menu.Item key="11" onClick={() => this.setSelectedKey = RoutePaths.PromotionManagement}>
                        <NavLink key="promotion-management" to={RoutePaths.PromotionManagement}>
                            <Icon type="bars" />
                            <span>Promotion Management</span>
                        </NavLink>
                    </Menu.Item>
                </Menu>
            </>
        );
    }


    private set setSelectedKey(v: string) {
        this.setState({ selectedKeys: [v] });
    }

    private set setSelectedOpenKey(v: string) {
        this.setState({ defaultOpenKeys: [v] });
    }

    get getSelectedKey(): string[] {
        let selectedKey: string[] = [];
        switch (this.props.pathname) {
            case RoutePaths.Home:
                selectedKey.push('1');
                break;
            case RoutePaths.UserManagement:
                selectedKey.push('2');
                break;
            case RoutePaths.AudioManagement:
                selectedKey.push('3');
                break;
            case RoutePaths.CategoryManagement:
                selectedKey.push('4');
                break;
            case RoutePaths.MembershipTypes:
                selectedKey.push('5');
                break;
            case RoutePaths.AuthorManagement:
                selectedKey.push('6');
                break;
            case RoutePaths.PublisherManagement:
                selectedKey.push('7');
                break;
            case RoutePaths.NarratorManagement:
                selectedKey.push('8');
                break;
            case RoutePaths.LanguageManagement:
                selectedKey.push('9');
                break;
            case RoutePaths.ProductManagement:
                selectedKey.push('10');
                break;
            case RoutePaths.PromotionManagement:
                selectedKey.push('11');
                break;
            default:
                break;
        }
        return selectedKey;
    }

    get getSelectedOpenKey(): string[] {
        let selectedKey: string[] = [];
        switch (this.props.pathname) {
            case RoutePaths.AudioManagement:
                selectedKey.push('domain-sub-menu');
                break;
            case RoutePaths.CategoryManagement:
                selectedKey.push('domain-sub-menu');
                break;
            default:
                break;
        }
        return selectedKey;
    }
}
import * as React from 'react';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import {AppSettings} from './AppSettings';
const { Header } = Layout;

interface IAppHeaderProps {

}

interface IAppHeaderState {

}

export class AppHeader extends React.Component<IAppHeaderProps, IAppHeaderState>{
    public render() {
        return (
            <>
                <Header className="header">

                    <div className="header-left">
                        <span className="header-title">Audio Book</span>
                    </div>
                    <div className="header-right">
                        <ul className="header-list-inline">
                            <li>
                                <input className="header-search" type="text" placeholder="Search.." name="search2" />
                            </li>
                            <li>
                                <div className="header-avatar-txt">
                                    <div className="header-avatar-email">nishila2@gmail.com</div>
                                    <div className="header-avatar-role">Administrator</div>
                                </div>
                            </li>
                            <li>
                                <AppSettings/>
                            </li>
                        </ul>
                    </div>

                </Header>
            </>

        );
    }
}
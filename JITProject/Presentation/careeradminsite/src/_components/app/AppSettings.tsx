import * as React from 'react';
import 'antd/dist/antd.css';
import { Menu } from 'antd';
import { Dropdown,Icon} from 'antd';
import { RoutePaths } from '../../routes';
import { NavLink } from 'react-router-dom';

interface IAppSettingsProps {

}

interface IAppSettingsState {
  selectedKeys: string[];
}

export class AppSettings extends React.Component<IAppSettingsProps, IAppSettingsState>{

  private menu = (
    <Menu>
      <Menu.Item key="0">
      
        <a href="settings"><Icon type="setting" style={{padding:8}} />Settings</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href="FAQ"><Icon type="snippets"  style={{padding:8}}/>FAQ</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">
      <NavLink key="auth/login" to={RoutePaths.SignIn}>
       <Icon type="logout"  style={{padding:8}}/>LogOut
       </NavLink>
      </Menu.Item>
    </Menu>
  );

  public render() {
    const menu = this.menu;
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <button className="ant-dropdown-link">
        <img alt="avatar" 
        className="header-avatar-icon" 
        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAAM1BMVEVndYWzuL2Nl6Gbo6yqsLZxfYx/ipd1gpCgp69seYmSm6WlrLN6hpOvtLqEjpqXn6iIkp4fvOWIAAAAiklEQVR4XtXS2woDMQgE0BnNbe/7/19bqKxQMrTPPa8adIL4D7uT3E5oJ4MvECofK4SLqWJ2MBkmCxNdjk4DMya528akojc+Cr4m2yGt8VZVu10V9TZrwD7ss6UVkscdCd9Dtj5vVVYzZzi6yKS+bqGUZ0Cp5x1IDcEoGYJTcoRBaSAUSgWBGn+XX0A2A4V4OvWYAAAAAElFTkSuQmCC" />
        </button>
      </Dropdown>
    );
  }
}
import * as React from 'react';
import 'antd/dist/antd.css';
import { Row } from 'antd';
import axios from 'axios';
import { IAppContextWrapperComponentProps } from "../general/AppContext";
import { RouteComponentProps } from 'react-router-dom';
import { UserCard } from './UserCard';
import { AddToUserForm } from './AddToUserForm';


interface IUserManagementProps extends IAppContextWrapperComponentProps, RouteComponentProps<any> {
    index: string;
}

interface IUserManagementState {
    isBusy: boolean;
    Users: any[];
    searchTerm: string;
}

export class UserManagement extends React.Component<IUserManagementProps, IUserManagementState>{

    constructor(props: IUserManagementProps) {
        super(props)

        this.state = {
            isBusy: true,
            Users: [],
            searchTerm: " ",
        }
    }

    public componentDidMount() {
        this.getData();
    }

    public render() {
        return (
            <>
                <div className="sub-header-01">
                    <AddToUserForm />
                </div>
                <div className="gutter-example">
                    <Row gutter={32}>
                        <UserCard
                         Users={this.state.Users}
                         getData={this.getData}
                          />
                    </Row>
                </div>
            </>
        )
    }

    private getData = () => {
        console.log(14);
        let header = {
            'Content-Type': "application/json",
        };
        let formData = new FormData();
        formData.append('searchTerm', this.state.searchTerm);
        const url = "https://localhost:44338/api/productapi/get-users";
        axios.post(url, JSON.stringify(formData), { headers: header }).then(res => {
            console.log(13, res.data);

            this.setState({
                Users: res.data.data
            });
        });
    }

}
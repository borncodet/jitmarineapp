import * as React from 'react';
import 'antd/dist/antd.css';
import { Card, Button, Tooltip } from 'antd';
import { Col } from 'antd';
import { UserProfileModal } from './UserProfileModal';
import axios from 'axios';

interface IUserCardProps {
    Users: any[],
    getData: () => any,
}

interface IUserCardState {
    isBusy: boolean;
    visible: boolean;
    userId: any;
    isModal: boolean;
    Users: any[];
}

export class UserCard extends React.Component<IUserCardProps, IUserCardState>{

    constructor(props: IUserCardProps) {
        super(props)

        this.state = {
            isBusy: true,
            visible: false,
            userId: "",
            isModal: false,
            Users: this.props.Users,
        }
    }

    public render() {
        return (
            this.props.Users.map((User, index) => {
                return (<>
                    <Col className="gutter-row" key={index} style={{ width: 200, marginBottom: 10 }} span={6}>
                        <div className="gutter-box" >
                            <Card
                                cover={<img
                                    alt="example"
                                    src="https://www.w3schools.com/bootstrap4/img_avatar1.png" />}
                                actions={[
                                    <Tooltip title="View User Profile">
                                        <Button shape="circle"
                                            icon="eye"
                                            size="small"
                                            onClick={() => { this.handleViewProfile(User.id); this.showModal() }}
                                        />
                                    </Tooltip>,
                                    <Tooltip title="Edit"><Button shape="circle" icon="edit" size="small" /></Tooltip>,
                                    <Tooltip title="Delete">
                                        <Button shape="circle"
                                            icon="delete"
                                            size="small"
                                            onClick={() => this.handleDelete(User.id)}
                                        /></Tooltip>
                                ]}>
                                <div className="card-body">
                                    <h5 className="card-title">{User.userName}</h5>
                                    <h6 className="card-text">{User.email}</h6>
                                    <h6 className="card-text">{User.phoneNumber}</h6>
                                </div>
                            </Card>
                        </div>
                    </Col>
                    <UserProfileModal
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        UserId={this.state.userId}
                        isModal={this.state.isModal}
                    />
                </>
                )
            })
        );
    }

    private handleViewProfile = (UserId: any) => {
        this.setState({
            userId: UserId,
            isModal: true,
        })
    }

    private showModal = () => {
        this.setState({
            visible: true,
        });
    };

    private handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    private handleDelete = (UserId: any) => {
        console.log(3333, UserId);
        let header = {
            'Content-Type': "application/json",
        };
        let data = JSON.stringify({
            Id: UserId,

        })
        const url = "https://localhost:44338/api/account/da";
        axios.post(url, data, { headers: header }).then(res => {
            console.log(13, res.data);
            this.props.getData();
        });

    }
};
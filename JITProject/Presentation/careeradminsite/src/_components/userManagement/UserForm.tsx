import * as React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { User } from '../../_models/User';
import { Select } from 'antd';
import axios from "axios";

interface IUserFormProps extends FormComponentProps {
  onClose: () => any

}

interface IUserFormstate {
  UserForm: User;
  confirmDirty: boolean;
}

class UserForm extends React.Component<IUserFormProps, IUserFormstate> {
  constructor(props: IUserFormProps) {
    super(props);
    this.state = {
      UserForm: new User(),
      confirmDirty: false,  
    }

  }

  public render() {

    const { Option } = Select;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} >
        <Form.Item key="1" label="First Name">
          {getFieldDecorator('FirstName', {
            rules: [{
              required: true,
              message: 'Please input  First Name!',
            }],
          })(
            <Input placeholder="First Name"
              name="firstName"
              onChange={(e) => this.handleInputChange(e)}
            />)}
        </Form.Item>
        <Form.Item key="2" label="Middle Name">
          {getFieldDecorator('MiddleName', {
            rules: [{
              required: true,
              message: 'Please input  Middle Name!'
            }],
          })(
            <Input placeholder="Middle Name"
              name="middleName"
              onChange={(e) => this.handleInputChange(e)}
            />)}
        </Form.Item>
        <Form.Item key="3" label="Last Name">
          {getFieldDecorator('LastName', {
            rules: [{
              required: true,
              message: 'Please input  Last Name!'
            }],
          })(
            <Input placeholder="Last Name"
              name="lastName"
              onChange={(e) => this.handleInputChange(e)}
            />)}
        </Form.Item>
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ],
          })(
            <Input />)}
        </Form.Item>
        <Form.Item label="Password" hasFeedback>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: 'Please input your password!',
              },
              {
                validator: this.validateToNextPassword,
              },
            ],
          })(
            <Input.Password />)}
        </Form.Item>
        <Form.Item label="Confirm Password" hasFeedback>
          {getFieldDecorator('confirm', {
            rules: [
              {
                required: true,
                message: 'Please confirm your password!',
              },
              {
                validator: this.compareToFirstPassword,
              },
            ],
          })(
            <Input.Password onBlur={this.handleConfirmBlur} />)}
        </Form.Item>
        <Form.Item label="Phone Number">
          {getFieldDecorator('phone', {
            rules: [{
              required: true,
              message: 'Please input your phone number!'
            }],
          })(
            <Input style={{ width: '100%' }} />
          )}
        </Form.Item>

        <Form.Item label="Registration Type">
          {getFieldDecorator('RegisterType', {
            rules: [{
              required: true,
              message: 'Please Select Registration Type!'
            }],
          })(
            <Select placeholder="Select one"
              labelInValue
              style={{ width: 120 }}
              onChange={(e: any) => this.handleChange(e)}
            >
              <Option value="1">User</Option>
              <Option value="2">GroupAdmin</Option>
              <Option value="3">GroupMember</Option>
            </Select>)}
        </Form.Item>

        <Button onClick={this.onCloser} style={{ marginRight: 12, marginLeft: 150, marginTop: 100 }}>
          Cancel
            </Button>
        <Button htmlType="submit" onClick={this.onCloser} type="primary">
          Save
            </Button>
      </Form>
    )
  }

  private handleSubmit = (e: any) => {
    console.log(12, e)
    e.preventDefault();
    this.props.form.validateFields((err: any, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.CreateOrUpdateUser();
      }
    });
  };

  private CreateOrUpdateUser = () => {
    console.log(15);
    let header = {
      'Content-Type': "application/json",
    };

    let data = JSON.stringify({
      Id:"" ,
       
   })
 axios.post('',data , { headers: header })
      .then((res) => {
        let result = res.data;
        console.log(result);
      })
  }

  private onCloser = () => {
    this.props.onClose();
  };

  private handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    const UserFormUpdates = {
      [name]: value
    };
    console.log(11, UserFormUpdates);
    this.setState({ UserForm: Object.assign(this.state.UserForm, UserFormUpdates) as User });
  }

  private handleChange = (value: any) => {
    console.log(value);
  }

  private handleConfirmBlur = (e: any) => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  private compareToFirstPassword = (rule: any, value: any, callback: any) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  private validateToNextPassword = (rule: any, value: any, callback: any) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

}

export const AddUserForm = Form.create<IUserFormProps>()(UserForm);
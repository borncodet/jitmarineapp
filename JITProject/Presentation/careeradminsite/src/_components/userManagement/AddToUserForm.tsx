import * as React from 'react';
import 'antd/dist/antd.css';
import { Tooltip } from 'antd';
import { Drawer, Button, Icon } from 'antd';
import { AddUserForm } from "./UserForm";

interface IAddToUserFormProps {
    key?: string;
}

interface IAddToUserFormState {
    visible: boolean;
}

export class AddToUserForm extends React.Component<IAddToUserFormProps, IAddToUserFormState> {

    public state = { visible: false };

    public render() {
        return (
            <>
                <Tooltip placement="topLeft" title="Add users">
                    <Button type="default" onClick={this.showDrawer}>
                        <Icon type="plus" />
                    </Button>
                </Tooltip>
                <Drawer
                    title="Add a new Users"
                    width={650}
                    onClose={this.onClose}
                    visible={this.state.visible}>
                    <AddUserForm onClose={this.onClose} />
                </Drawer>
            </>
        );
    }

    private onClose = () => {
        this.setState({
            visible: false,
        });
    };

    private showDrawer = () => {
        this.setState({
            visible: true,
        });
    };
}
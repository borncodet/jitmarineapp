import * as React from 'react';
import 'antd/dist/antd.css';
import { Modal } from 'antd';
import axios from 'axios';

interface IUserProfileModalProps {
    visible: any,
    onCancel: () => any,
    UserId: any,
    isModal: any,
}

interface IUserProfileModalState {
    isBusy: boolean;
    userData: any[];
}

export class UserProfileModal extends React.Component<IUserProfileModalProps, IUserProfileModalState>{

    constructor(props: IUserProfileModalProps) {
        super(props)

        this.state = {
            isBusy: true,
            userData: []
        }
    }

    public render() {


        return (
            <>
                {this.getData()}
                <Modal
                    title="User Name"
                    visible={this.props.visible}
                    onCancel={this.handleCancel}
                >
                    <div className="container">
                        <div className="card" style={{ width: 400 }}>
                            <img className="card-img-top" src="https://www.w3schools.com/bootstrap4/img_avatar1.png" alt=" " style={{ width: 150 }}></img>
                            <div className="card-body">
                                <h3 className="card-title">John Doe</h3>
                                <h4 className="card-title">ID:{this.props.UserId}</h4>
                                <p className="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                            </div>
                        </div>
                    </div>
                </Modal>
            </>
        )
    }

    private handleCancel = (e: any) => {
        this.props.onCancel();
    }

    private getData = () => {
        console.log(2222, this.props.UserId);
        let header = {
            'Content-Type': "application/json",
        };
        let formData = new FormData();
        formData.append('userId', this.props.UserId);
        const url = "";
        axios.post(url, JSON.stringify(formData), { headers: header }).then(res => {
            console.log(13, res.data);

            this.setState({
                userData: res.data.data
            });
        });
    }
};

import './_assets/css/site.css';
import 'antd/dist/antd.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as RoutesModule from './routes';
import { AsyncComponent } from './_components/general/AsyncComponent';
import { BrowserRouter } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
const App = React.lazy(() => import('./App'));
const routes = RoutesModule.routes;

ReactDOM.render(
  <AsyncComponent>
    <App>
      <BrowserRouter children={routes} />
    </App>
  </AsyncComponent>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
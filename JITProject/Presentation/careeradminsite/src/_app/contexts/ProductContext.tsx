import * as React from "react";
import { useImmerReducer } from "use-immer";
import { IProductManagementState } from "..";
import { IProductAction, productReducer } from "_app/actions";
import { ProductDataResultModel } from "_app/api";

export const ProductDispatcherContext = React.createContext<React.Dispatch<IProductAction> | null>(null);
export const ProductStateContext = React.createContext<IProductManagementState | null>(null);

export const initialProductManagementState = {
      products: {}as ProductDataResultModel,
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false
} as IProductManagementState;

export const ProductContextProvider: React.FC = ({ children }) => {
    const [productState, dispatcher] = useImmerReducer(productReducer, initialProductManagementState);

    return (
        <ProductDispatcherContext.Provider value={dispatcher}>
            <ProductStateContext.Provider value={productState}>
                {children}
            </ProductStateContext.Provider>
        </ProductDispatcherContext.Provider>
    )
}
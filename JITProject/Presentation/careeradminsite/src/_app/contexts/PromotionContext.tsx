import * as React from "react";
import { useImmerReducer } from "use-immer";
import { IPromotionManagementState } from "..";
import { IPromotionAction, promotionReducer } from "_app/actions";

export const PromotionDispatcherContext = React.createContext<React.Dispatch<IPromotionAction> | null>(null);
export const PromotionStateContext = React.createContext<IPromotionManagementState | null>(null);

export const initialPromotionManagementState = {
    //   promotions: {}as PromotionDataResultModel,
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false
} as IPromotionManagementState;

export const PromotionContextProvider: React.FC = ({ children }) => {
    const [promotionState, dispatcher] = useImmerReducer(promotionReducer, initialPromotionManagementState);

    return (
        <PromotionDispatcherContext.Provider value={dispatcher}>
            <PromotionStateContext.Provider value={promotionState}>
                {children}
            </PromotionStateContext.Provider>
        </PromotionDispatcherContext.Provider>
    )
}
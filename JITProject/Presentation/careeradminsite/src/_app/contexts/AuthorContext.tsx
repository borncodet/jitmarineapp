import * as React from "react";
import { IAuthorAction, authorReducer } from "../actions";
import { useImmerReducer } from "use-immer";
import { IAuthorManagementState } from "..";

export const AuthorDispatcherContext = React.createContext<React.Dispatch<IAuthorAction> | null>(null);
export const AuthorStateContext = React.createContext<IAuthorManagementState | null>(null);

export const initialAuthorManagementState = {
    authors: null,
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false
} as IAuthorManagementState;

export const AuthorContextProvider: React.FC = ({ children }) => {
    const [authorState, dispatcher] = useImmerReducer(authorReducer, initialAuthorManagementState);

    return (
        <AuthorDispatcherContext.Provider value={dispatcher}>
            <AuthorStateContext.Provider value={authorState}>
                {children}
            </AuthorStateContext.Provider>
        </AuthorDispatcherContext.Provider>
    )
}
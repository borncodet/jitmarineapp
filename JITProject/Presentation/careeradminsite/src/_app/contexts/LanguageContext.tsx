import * as React from "react";
import { ILanguageAction, languageReducer } from "../actions";
import { useImmerReducer } from "use-immer";
import { ILanguageManagementState } from "..";

export const LanguageDispatcherContext = React.createContext<React.Dispatch<ILanguageAction> | null>(null);
export const LanguageStateContext = React.createContext<ILanguageManagementState | null>(null);

export interface ILanguageDispatcherContextProps {
    dispatcher: React.Dispatch<ILanguageAction>
}

export const initialLanguageManagementState = {
    languages: [],
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false,
    currentSelectedLanguageId:""
} as ILanguageManagementState;

export const LanguageContextProvider: React.FC = ({ children }) => {
    const [languageState, dispatcher] = useImmerReducer(languageReducer, initialLanguageManagementState);

    return (
        <LanguageDispatcherContext.Provider value={dispatcher}>
            <LanguageStateContext.Provider value={languageState}>
                {children}
            </LanguageStateContext.Provider>
        </LanguageDispatcherContext.Provider>
    )
}
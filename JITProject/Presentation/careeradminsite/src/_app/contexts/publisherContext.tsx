import * as React from "react";
import { IPublisherAction, publisherReducer,} from "../actions";
import { useImmerReducer } from "use-immer";
import { IPublisherManagementState } from "..";
import { PublisherDataResultModel } from "_app/api";

export const PublisherDispatcherContext = React.createContext<React.Dispatch<IPublisherAction> | null>(null);
export const PublisherStateContext = React.createContext<IPublisherManagementState | null>(null);

export interface INarratorDispatcherContextProps {
    dispatcher: React.Dispatch<IPublisherAction>
}

export const initialState = {
    publishers: {} as PublisherDataResultModel,
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false
} as IPublisherManagementState;

export const PublisherContextProvider: React.FC = ({ children }) => {
    const [publisherState, dispatcher] = useImmerReducer(publisherReducer, initialState);

    return (
        <PublisherDispatcherContext.Provider value={dispatcher}>
            <PublisherStateContext.Provider value={publisherState}>
                {children}
            </PublisherStateContext.Provider>
        </PublisherDispatcherContext.Provider>
    )
}
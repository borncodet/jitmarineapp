import * as React from "react";
import { INarratorAction, narratorReducer } from "../actions";
import { useImmerReducer } from "use-immer";
import { INarratorManagementState } from "..";

export const NarratorDispatcherContext = React.createContext<React.Dispatch<INarratorAction> | null>(null);
export const NarratorStateContext = React.createContext<INarratorManagementState | null>(null);

export const initialNarratorManagementState = {
    narrators: null,
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false
} as INarratorManagementState;

export const NarratorContextProvider: React.FC = ({ children }) => {
    const [narratorState, dispatcher] = useImmerReducer(narratorReducer, initialNarratorManagementState);

    return (
        <NarratorDispatcherContext.Provider value={dispatcher}>
            <NarratorStateContext.Provider value={narratorState}>
                {children}
            </NarratorStateContext.Provider>
        </NarratorDispatcherContext.Provider>
    )
}
import * as React from "react";
import { ICategoryAction, categoryReducer } from "../actions";
import { useImmerReducer } from "use-immer";
import { ICategoryManagementState } from "..";

export const CategoryDispatcherContext = React.createContext<React.Dispatch<ICategoryAction> | null>(null);
export const CategoryStateContext = React.createContext<ICategoryManagementState | null>(null);

export const initialCategoryManagementState = {
    categorys: null,
    isEditScreenVisible: false,
    isBusy: true,
    statusCode: 0,
    errorMessage: "",
    isDirty: false
} as ICategoryManagementState;

export const CategoryContextProvider: React.FC = ({ children }) => {
    const [categoryState, dispatcher] = useImmerReducer(categoryReducer, initialCategoryManagementState);

    return (
        <CategoryDispatcherContext.Provider value={dispatcher}>
            <CategoryStateContext.Provider value={categoryState}>
                {children}
            </CategoryStateContext.Provider>
        </CategoryDispatcherContext.Provider>
    )
}
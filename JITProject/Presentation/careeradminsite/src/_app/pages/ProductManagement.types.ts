import { RouteComponentProps } from "react-router-dom";
import { IErrorState } from "../shared/common";
import { ProductDataResultModel } from "_app/api";

export interface IProductManagementProps extends RouteComponentProps<any> { }

export interface IProductManagementState extends IErrorState {
    products: ProductDataResultModel | null;
    isBusy: boolean;
    isEditScreenVisible: boolean;
}
import React from "react";
import { Route } from "react-router-dom";

import { ProductManagementEditContainer } from "_app/containers/product";
import { PromotionContextProvider } from "_app/contexts";
import { PromotionManagement } from ".";
import { PromotionMain } from "_app/containers";


export const PromotionManagementRoute = (match: any) => {
    return (
        <>
             <Route
                                   exact={true}
                                   path={`${match.path}promotion-management`}
                                   render={(props) => {
                                       return <PromotionContextProvider>
                                           <PromotionManagement {...props} />
                                       </PromotionContextProvider>
                                   }}
                                />
            <Route
                exact={true}
                path={`${match.path}promotion-management/add-new`}
                render={(props) => {
                    return<PromotionContextProvider>
                        <PromotionMain {...props} />
                    </PromotionContextProvider>
                }} />
            <Route
                exact={true}
                path={`${match.path}promotion-management/edit/:id`}
                render={(props) => {
                    return <PromotionContextProvider>
                         <PromotionMain {...props} />
                    </PromotionContextProvider>
                }} />
        </>
    );
}
import React from 'react';
import { IToolbarContainerItemProps, ToolbarContainer, INarratorManagementProps } from '..';
import { Icon, Spin } from 'antd';
import {NarratorDataRequestModel } from '_app/api';
import {
     updateNarratorEditScreenVisibility, 
     useNarratorDispatcher, 
     useNarratorContext, 
     fetchActiveNarrators 
    } from '_app/actions/NarratorAction';
import NarratorList from '_app/containers/narrator/NarratorList';

const NarratorManagement: React.FC<INarratorManagementProps> = (props: INarratorManagementProps) => {

    const narratorDispatcher = useNarratorDispatcher();
    const narratorContext = useNarratorContext();
    const { isBusy } = narratorContext;

    React.useEffect(() => {
        (async () => {
            await fetchActiveNarrators(narratorDispatcher, {} as NarratorDataRequestModel);
        })();
    }, [narratorDispatcher])

    const onAddButtonClick = (): void => {
        updateNarratorEditScreenVisibility(narratorDispatcher, true);
    }

    const onRefreshButtonClick = (): void => {
        fetchActiveNarrators(narratorDispatcher, {} as NarratorDataRequestModel);
    }

    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        },{
            label: "Refresh",
            type: "button",
            onClick: onRefreshButtonClick,
            icon: <Icon type="add" />
        }  as IToolbarContainerItemProps];
    }

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    return (
        <>
            <ToolbarContainer
                items={getItems()}
                farItems={getFarItems()} />
            {
                !isBusy ? (
                    <NarratorList />
                ) :
                    (
                        <Spin spinning={isBusy}  size={"large"}>
                        </Spin>
                    )
            }
        </>
    )
}

export default NarratorManagement;
import { RouteComponentProps } from "react-router-dom";
import { LanguageInfo } from "../api/controller";
import { IErrorState } from "../shared/common";

export interface ILanguageManagementProps extends RouteComponentProps<any> { }

export interface ILanguageManagementState extends IErrorState {
    languages: LanguageInfo[] | null;
    isBusy: boolean;
    isEditScreenVisible: boolean;
    
}
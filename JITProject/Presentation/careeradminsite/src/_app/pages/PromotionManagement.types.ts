import { RouteComponentProps } from "react-router-dom";
import { IErrorState } from "../shared/common";

export interface IPromotionManagementProps extends RouteComponentProps<any> { }

export interface IPromotionManagementState extends IErrorState {
    isBusy: boolean;
    isEditScreenVisible: boolean;
}
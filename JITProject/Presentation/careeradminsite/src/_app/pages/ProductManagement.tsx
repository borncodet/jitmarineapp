import React from 'react';
import { IProductManagementProps } from '.';
import {
    ToolbarContainer,
    IToolbarContainerItemProps,
    ProductDataRequestModel,
} from '..';
import { Icon } from 'antd';
import {  ProductManagementList } from '_app/containers';
import {
    useProductDispatcher,
    fetchActiveProducts
} from '_app/actions';

const ProductManagement: React.FC<IProductManagementProps> = (props: IProductManagementProps) => {
    const { match, history } = props;

    const productDispatcher = useProductDispatcher();

    React.useEffect(() => {
        (async () => {
            await fetchActiveProducts(productDispatcher, {} as ProductDataRequestModel);
        })();
    }, [productDispatcher])

    const onAddButtonClick = (): void => {
        history.push(`${match.path}/add-new`);

    }

    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    return (
        <>
            <div>
                <ToolbarContainer
                    items={getItems()} />
            </div>
            <div style={{ padding: 50 }}>
                <ProductManagementList />
            </div>

        </>
    )
}
export default ProductManagement;

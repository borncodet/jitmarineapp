import React from "react";
import { Route } from "react-router-dom";
import { ProductContextProvider } from "_app/contexts/ProductContext";
import { ProductManagementEditContainer } from "_app/containers/product";
import { ProductManagement } from "_app/pages";

export const ProductManagementRoute = (match: any) => {
    return (
        <>
            <Route
                exact={true}
                path={`${match.path}product-management`}
                render={(props) => {
                    return <ProductContextProvider>
                        <ProductManagement {...props} />
                    </ProductContextProvider>
                }}
            />
            <Route
                exact={true}
                path={`${match.path}product-management/add-new`}
                render={(props) => {
                    return <ProductContextProvider>
                        <ProductManagementEditContainer {...props} />
                    </ProductContextProvider>
                }} />
            <Route
                exact={true}
                path={`${match.path}product-management/edit/:id`}
                render={(props) => {
                    return <ProductContextProvider>
                        <ProductManagementEditContainer {...props} />
                    </ProductContextProvider>
                }} />
        </>
    );
}
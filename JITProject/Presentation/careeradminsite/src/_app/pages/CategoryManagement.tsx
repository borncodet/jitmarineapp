import React from 'react';
import { IToolbarContainerItemProps, ToolbarContainer, ICategoryManagementProps } from '..';
import { Icon, Spin } from 'antd';
import {CategoryDataRequestModel } from '_app/api';
import {
     updateCategoryEditScreenVisibility, 
     useCategoryDispatcher, 
     useCategoryContext, 
     fetchActiveCategorys 
    } from '../actions/CategoryAction';
import CategoryList from '_app/containers/category/CategoryList';

const CategoryManagement: React.FC<ICategoryManagementProps> = (props: ICategoryManagementProps) => {

    const categoryDispatcher = useCategoryDispatcher();
    const categoryContext = useCategoryContext();
    const { isBusy } = categoryContext;

    React.useEffect(() => {
        (async () => {
            await fetchActiveCategorys(categoryDispatcher, {} as CategoryDataRequestModel);
        })();
    }, [categoryDispatcher])

    const onAddButtonClick = (): void => {
        updateCategoryEditScreenVisibility(categoryDispatcher, true);
    }

    const onRefreshButtonClick = (): void => {
        fetchActiveCategorys(categoryDispatcher, {} as CategoryDataRequestModel);
    }

    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        },{
            label: "Refresh",
            type: "button",
            onClick: onRefreshButtonClick,
            icon: <Icon type="add" />
        }  as IToolbarContainerItemProps];
    }

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    return (
        <>
            <ToolbarContainer
                items={getItems()}
                farItems={getFarItems()} />
            {
                !isBusy ? (
                    <CategoryList />
                ) :
                    (
                        <Spin spinning={isBusy}  size={"large"}>
                        </Spin>
                    )
            }
        </>
    )
}

export default CategoryManagement;
import { RouteComponentProps } from "react-router-dom";
import { IErrorState } from "../shared/common";
import { PublisherDataResultModel } from "../api/controller";

export interface IPublisherManagementProps extends RouteComponentProps<any> { }

export interface IPublisherManagementState extends IErrorState {
    publishers: PublisherDataResultModel| null;
    isBusy: boolean;
    isEditScreenVisible: boolean;
}

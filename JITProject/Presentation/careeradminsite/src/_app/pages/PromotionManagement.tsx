import React from 'react';
import { IPromotionManagementProps } from '.';
import {  PromotionList } from '_app/containers/promotion';
import { IToolbarContainerItemProps, ToolbarContainer } from '_app/containers/shared';
import { Icon } from 'antd';


const PromotionManagement: React.FC<IPromotionManagementProps> = (props: IPromotionManagementProps) => {
    const { match, history } = props;
    
    const onAddButtonClick = (): void => {
        history.push(`${match.path}/add-new`);
    }
    


    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }
    return (
        <>
            <div>
                <ToolbarContainer
                    items={getItems()} />
            </div>
            {/* <PromotionMain/> */}
            <div style={{ padding: 25 }}>
                <PromotionList {...props} />
            </div>

        </>
    )
}
export default PromotionManagement;
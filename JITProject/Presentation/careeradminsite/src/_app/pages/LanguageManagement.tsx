import React from 'react';
import LanguageList from '../containers/language/LanguageList';
import { ILanguageManagementProps } from '.';
import { IToolbarContainerItemProps, ToolbarContainer } from '..';
import { Icon, Spin } from 'antd';
import { LanguageDataRequestModel } from '_app/api';
import { useLanguageDispatcher, useLanguageContext, fetchActiveLanguages, updateLanguageEditScreenVisibility } from '../actions';


const LanguageManagement: React.FC<ILanguageManagementProps> = (props: ILanguageManagementProps) => {
    const languageDispatcher = useLanguageDispatcher();
    const languageContext = useLanguageContext();
    const { isBusy, } = languageContext;
    
    React.useEffect(() => {
        (async () => {
            await fetchActiveLanguages(languageDispatcher, {} as LanguageDataRequestModel);
        })();
    }, [languageDispatcher])

    
    const onAddButtonClick = (): void => {  
        updateLanguageEditScreenVisibility(languageDispatcher, true);
    }

    const onRefreshButtonClick = (): void => {
        fetchActiveLanguages(languageDispatcher, {} as LanguageDataRequestModel);
    }

    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        }, {
            label: "Refresh",
            type: "button",
            onClick: onRefreshButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    return (
        <>
            {
                !isBusy ? (
                    <>
                        <ToolbarContainer
                            items={getItems()}
                            farItems={getFarItems()} />
                        {/* {!isBusy ? ( */}
                        <LanguageList
                           

                        />
                        {/* ) :
                             (<Spin spinning={isBusy} size={"large"} >
                            </Spin>) 
                         } */}
                    </>
                ) :
                    (
                        <Spin spinning={isBusy} size={"large"} style={{ paddingLeft: 550, marginTop: 75 }}>
                        </Spin>
                    )
            }
        </>
    )
}
export default LanguageManagement;
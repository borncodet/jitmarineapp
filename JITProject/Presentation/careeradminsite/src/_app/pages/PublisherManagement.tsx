import React from 'react';
import { IToolbarContainerItemProps, ToolbarContainer, IPublisherManagementProps } from '..';
import { Icon, Spin } from 'antd';
import {PublisherDataRequestModel } from '_app/api';
import {
     updateEditScreenVisibility, 
     usePublisherDispatcher, 
     fetchActivePublishers, 
     usePublisherContext
    } from '../actions/PublisherAction';
import PublisherList from '_app/containers/publisher/PublisherList';

const PublisherManagement: React.FC<IPublisherManagementProps> = (props: IPublisherManagementProps) => {

    const publisherDispatcher = usePublisherDispatcher();
    const publisherContext = usePublisherContext();
    const { isBusy } = publisherContext;

    React.useEffect(() => {
        (async () => {
            await fetchActivePublishers(publisherDispatcher, {} as PublisherDataRequestModel);
        })();
    }, [publisherDispatcher])

    const onAddButtonClick = (): void => {
        updateEditScreenVisibility(publisherDispatcher, true);
    }

    const onRefreshButtonClick = (): void => {
        fetchActivePublishers(publisherDispatcher, {} as PublisherDataRequestModel);
    }

    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        },{
            label: "Refresh",
            type: "button",
            onClick: onRefreshButtonClick,
            icon: <Icon type="add" />
        }  as IToolbarContainerItemProps];
    }

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    return (
        <>
            <ToolbarContainer
                items={getItems()}
                farItems={getFarItems()} />
            {
                !isBusy ? (
                    <PublisherList />
                ) :
                    (
                        <Spin spinning={isBusy}  size={"large"}>
                        </Spin>
                    )
            }
        </>
    )
}

export default PublisherManagement;
import { RouteComponentProps } from "react-router-dom";
import { IErrorState } from "../shared/common";
import { NarratorDataResultModel } from "../api/controller";

export interface INarratorManagementProps extends RouteComponentProps<any> { }

export interface INarratorManagementState extends IErrorState {
    narrators: NarratorDataResultModel| null;
    isBusy: boolean;
    isEditScreenVisible: boolean;
}

import { RouteComponentProps } from "react-router-dom";
import { IErrorState } from "../shared/common";
import { CategoryDataResultModel } from "../api/controller";

export interface ICategoryManagementProps extends RouteComponentProps<any> { }

export interface ICategoryManagementState extends IErrorState {
    categorys: CategoryDataResultModel| null;
    isBusy: boolean;
    isEditScreenVisible: boolean;
}

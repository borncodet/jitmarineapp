import React from 'react';
import { IToolbarContainerItemProps, ToolbarContainer, IAuthorManagementProps } from '..';
import { Icon, Spin } from 'antd';
import {AuthorDataRequestModel } from '_app/api';
import {
     updateAuthorEditScreenVisibility, 
     useAuthorDispatcher, 
     useAuthorContext, 
     fetchActiveAuthors 
    } from '../actions/AuthorAction';
import AuthorList from '_app/containers/author/AuthorList';

const AuthorManagement: React.FC<IAuthorManagementProps> = (props: IAuthorManagementProps) => {

    const authorDispatcher = useAuthorDispatcher();
    const authorContext = useAuthorContext();
    const { isBusy } = authorContext;

    React.useEffect(() => {
        (async () => {
            await fetchActiveAuthors(authorDispatcher, {} as AuthorDataRequestModel);
        })();
    }, [authorDispatcher])

    const onAddButtonClick = (): void => {
        updateAuthorEditScreenVisibility(authorDispatcher, true);
    }

    const onRefreshButtonClick = (): void => {
        fetchActiveAuthors(authorDispatcher, {} as AuthorDataRequestModel);
    }

    const getItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        },{
            label: "Refresh",
            type: "button",
            onClick: onRefreshButtonClick,
            icon: <Icon type="add" />
        }  as IToolbarContainerItemProps];
    }

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Add",
            type: "button",
            onClick: onAddButtonClick,
            icon: <Icon type="add" />
        } as IToolbarContainerItemProps];
    }

    return (
        <>
            <ToolbarContainer
                items={getItems()}
                farItems={getFarItems()} />
            {
                !isBusy ? (
                    <AuthorList />
                ) :
                    (
                        <Spin spinning={isBusy}  size={"large"}>
                        </Spin>
                    )
            }
        </>
    )
}

export default AuthorManagement;
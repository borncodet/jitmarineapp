import { RouteComponentProps } from "react-router-dom";
import { IErrorState } from "../shared/common";
import { AuthorDataResultModel } from "../api/controller";

export interface IAuthorManagementProps extends RouteComponentProps<any> { }

export interface IAuthorManagementState extends IErrorState {
    authors: AuthorDataResultModel| null;
    isBusy: boolean;
    isEditScreenVisible: boolean;
}

export interface IErrorState {
    statusCode: number;
    errorMessage: string;
}
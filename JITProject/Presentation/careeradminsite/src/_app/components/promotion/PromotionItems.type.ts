import { FormComponentProps } from "antd/lib/form";
import { PromotionItemInfo, } from "_app/api";

export interface IPromotionItemsProps  extends FormComponentProps {
    items:PromotionItemInfo[] | undefined;
}

export interface IPromotionItemstState {
    isBusy: boolean;
    visible: boolean
}
import React from 'react';
import { Form, Row, Col, Input, Select, Button, Checkbox } from 'antd';
import { IPromotionAddItemToGridProps, IPromotionAddItemToGridState } from './PromotionAddItemToGrid.type';
import { PromotionItemPostModel } from '_app/api';
const initialState = {
    promotionGetType:0,
    itemId:"",
    title:"",
    id:"",
    getPrice:0,
    active:false,
    item:new PromotionItemPostModel() ,
} as IPromotionAddItemToGridState

const PromotionAddItemToGrid: React.FC<IPromotionAddItemToGridProps> = (props: IPromotionAddItemToGridProps) => {
    const { getFieldDecorator } = props.form;
    const { products, rollValue, onHandleItemPush} = props
    const [
        promotionAddItemToGridState,
        setPromotionAddItemToGridState
    ] = React.useState<IPromotionAddItemToGridState>(initialState);

const {itemId,getPrice,active,item,id,title}=promotionAddItemToGridState

    React.useEffect(() => {
        setPromotionAddItemToGridState((promotionAddItemToGridState) => {
            return {
                ...promotionAddItemToGridState,
                 item:{
                        id:id,itemTypeId:1,promotionGetType:parseInt(rollValue),itemId:itemId,title:title, get:getPrice,active:active
                    } as PromotionItemPostModel
            }
        });
        // const item={
        //     itemTypeId:1,promotionGetType:parseInt(rollValue),itemId:itemId,get:getPrice,active:active
        // }as PromotionItemPostModel;

        // if(item)
        // onHandleItemPush(item);
    }, [rollValue,itemId,getPrice,active])

    const onCreateSelectItems = (data: any[], valueFieldName: string, captionFieldName: string, keyName: string) => {
        let items = [];
        if (data) {
            for (let i = 0; i < data.length; i++) {
                items.push(
                    <Select.Option
                        key={keyName + i}
                        value={data[i][valueFieldName]}>
                        {data[i][captionFieldName]}
                    </Select.Option>
                );
            }
        }
        return items;
    }
    
    const handleSubmit = (e:any) => {
        console.log(123400)
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                 onHandleItemPush(item);
            }
        });
    }

    const onHandleChangeProduct = (value:any, event:any) =>{
        setPromotionAddItemToGridState((promotionAddItemToGridState) => {
            return {
                ...promotionAddItemToGridState,
                itemId: value,
                title:event.props.children
            }
        });
    }

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setPromotionAddItemToGridState({
            ...promotionAddItemToGridState,
             getPrice: value,
        });
    }

    const onActiveChange = (value: any) => {
        setPromotionAddItemToGridState((promotionAddItemToGridState) => {
            return {
                ...promotionAddItemToGridState,
                active: value.target.checked,
            }
        });
    }

    const makeTextInput = (rollValue: any) => {
       
        if (rollValue === "1") {
            return (
                <Form.Item key="7" label="Discount Percentage %">
                    {
                        getFieldDecorator(' Percentage', {
                            initialValue: null,
                            rules: [{
                                required: true,
                                message: 'Please Input  Percentage!'
                            }],
                        })(
                            <Input placeholder=""
                                name="Percentage"
                              onChange={onInputChange}
                            />
                        )
                    }
                </Form.Item>
            );
        }
        else if (rollValue === "2") {
            return (
                <Form.Item key="7" label="Discount Amount">
                    {
                        getFieldDecorator('price', {
                            initialValue: null,
                            rules: [{
                                required: true,
                                message: 'Please Input  Discount!'
                            }],
                        })(
                            <Input placeholder=""
                                name="productName"
                              onChange={onInputChange}
                            />
                        )
                    }
                </Form.Item>
            );
        }
        else {
            return (
                <Form.Item key="7" label=" New Product Price">
                    {
                        getFieldDecorator('newPrice', {
                            initialValue: null,
                            rules: [{
                                required: true,
                                message: 'Please NewPrice  new Price!'
                            }],
                        })(
                            <Input placeholder=""
                                name="newPrice"
                                onChange={onInputChange}
                            />
                        )
                    }
                </Form.Item>
            );
        }
    };

    return (
        <div style={{ border: '1px solid Gray', backgroundColor: 'white', borderRadius: 5, padding: 10, marginTop: 10 }}>
            <Form
                onSubmit={handleSubmit}
            >
                <Row gutter={24}>
                    <Col className="gutter-row" span={12}>
                        <div className="gutter-box">
                            <Form.Item key="6" label="Product">
                                {
                                    getFieldDecorator('product', {
                                        initialValue: null,
                                        rules: [{
                                            required: true,
                                            message: 'Please Select  Discount!'
                                        }],
                                    })(
                                        <Select
                                            placeholder="Select"
                                            style={{}}
                                            showSearch={true}
                                            onSelect={onHandleChangeProduct}
                                            optionFilterProp="children">
                                            {
                                                products &&
                                                onCreateSelectItems(products, "id", "productName", "product")
                                            }
                                        </Select>
                                    )
                                }
                            </Form.Item>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={4}>
                        <div className="gutter-box">
                            {makeTextInput(rollValue)}
                        </div>
                    </Col>
                    <Col className="gutter-row" span={4}>
                        <div className="gutter-box" style={{ paddingTop: 40 }}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" >
                                    Add
                                </Button>
                            </Form.Item>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={4}>
                        <div className="gutter-box">
                            <Form.Item key="12" style={{ paddingTop: 36 }}>
                                {getFieldDecorator('free', {
                                    valuePropName: 'checked',
                                    initialValue: null,
                                })(<Checkbox
                                onChange={onActiveChange}
                                >Active</Checkbox>)}
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}
export const PromotionAddItemToGridComponent = Form.create<IPromotionAddItemToGridProps>()(PromotionAddItemToGrid);
import React from 'react';
import { IPromotionMasterComponentProps, IPromotionMasterComponentState } from '.';
import { Row, Col, Form, Input, DatePicker, Select, Checkbox} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import moment from 'moment';
import { PromotionInfo, PromotionPostModel } from '_app/api';
// import InfiniteScroll from 'react-infinite-scroller';
const { Option } = Select;
const initialState = {
    discountId: '',
    validFrom: '',
    validTo: "",
    isBusy: false,
    loading: false,
    hasMore: true,
    data: [],
    roleId:'',
    promotionMasterEditForm: new PromotionInfo(),
} as IPromotionMasterComponentState

const PromotionMaster: React.FC<IPromotionMasterComponentProps> = (props: IPromotionMasterComponentProps) => {
    const { getFieldDecorator } = props.form;

    const { promotion, onHandleRollChange, onHandleMasterPush, userTypes,onHandleRollPush } = props
    const [
        promotionMasterState,
        setPromotionMasterState
    ] = React.useState<IPromotionMasterComponentState>(initialState);
    const { promotionMasterEditForm, validFrom, validTo,roleId } = promotionMasterState

    React.useEffect(() => {
        if (promotion && promotion.id !== undefined) {
            setPromotionMasterState({
                ...promotionMasterState,
                promotionMasterEditForm: promotion,
                validFrom: promotion.validFrom,
                validTo: promotion.validTo,
            });
        }
    }, [promotion && promotion.id])

    React.useEffect(() => {
        const master = {
            promotionCode: promotionMasterEditForm.promotionCode, title: promotionMasterEditForm.title, validFrom: validFrom, validTo: validTo,
            active: promotionMasterEditForm.active, 
        } as PromotionPostModel;
        if (master)
            onHandleMasterPush(master);
    }, [promotionMasterEditForm, promotionMasterEditForm.promotionCode, promotionMasterEditForm.title, validFrom, validTo,
        promotionMasterEditForm.active, promotionMasterEditForm.description])

        React.useEffect(() => {
            // const rolls = {
            //     roleId:`${roleId}`, active: promotionMasterEditForm.active
            // }
             if (roleId!=="")
                 onHandleRollPush(roleId);
        }, [roleId,promotionMasterEditForm.active])

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        const promotionMasterEditFormUpdates = {
            [name]: value,
        };
        setPromotionMasterState({
            ...promotionMasterState,
            promotionMasterEditForm: Object.assign(promotionMasterEditForm, promotionMasterEditFormUpdates) as PromotionInfo,
        });
    }

    const onCreateSelectItems = (data: any[], valueFieldName: string, captionFieldName: string, keyName: string) => {
        let items = [];
        if (data) {
            for (let i = 0; i < data.length; i++) {
                items.push(
                    <Select.Option
                        key={keyName + i}
                        value={data[i][valueFieldName]}>
                        {data[i][captionFieldName]}
                    </Select.Option>
                );
            }
        }
        return items;
    }

    const onHandleChangeAvailableEndDate = (value: any) => {
        setPromotionMasterState((promotionMasterState,

        ) => {
            return {
                ...promotionMasterState,
                validTo: value._d
            }
        });
    }
    const onHandleChangeAvailableStartDate = (value: any) => {
        setPromotionMasterState(( promotionMasterState,
        ) => {
            return {
                ...promotionMasterState,
                validFrom: value._d
            }
        });
    }
    
    const onHandleChangePromotionFor = (value: any) => {
        setPromotionMasterState((promotionMasterState,
        ) => {
            return {
                ...promotionMasterState,
                roleId: value
            }
        });

    }

    const onHandleChangeDiscount = (value: any) => {
        setPromotionMasterState((promotionMasterState,
        ) => {
            return {
                ...promotionMasterState,
                discountId: value
            }
        });
        onHandleRollChange(value);
    }

    // const handleSubmit=(e:any)=>{
    //     e.preventDefault();
    //     props.form.validateFieldsAndScroll((err, values) => {
    //         if (!err) {

    //         }
    //     }); 
    // }

    return (
        <div style={{ border: '1px solid Gray', backgroundColor: 'white', borderRadius: 5, padding: 10 }}>
            <Form
            // onSubmit={handleSubmit}
            >
                <Row gutter={24}>
                    <Col className="gutter-row" span={12}>
                        <div className="gutter-box">
                            <Form.Item
                                key="1"
                                label="Title">
                                {
                                    getFieldDecorator('promoCode', {
                                        initialValue: promotion && promotion.title,
                                        rules: [{ required: true, message: 'Please input  Title!' }],
                                    })(
                                        <Input placeholder="Promo Code"
                                            name="title"
                                            onChange={onInputChange}
                                        />
                                    )
                                }
                            </Form.Item>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={12}>
                        <div className="gutter-box">
                            <div className="gutter-box">
                                <Form.Item
                                    key="2"
                                    label="Promo Code">
                                    {
                                        getFieldDecorator('promocode', {
                                            initialValue: promotion && promotion.promotionCode,
                                            rules: [{ required: true, message: 'Please input  Title!' }],
                                        })(
                                            <Input placeholder="Promo Number"
                                                name="promotionCode"
                                                onChange={onInputChange}
                                            />
                                        )
                                    }
                                </Form.Item>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row gutter={16} >
                    <Col className="gutter-row" span={12}>
                        <div className="gutter-box">
                            <Form.Item key="3" label="Description">
                                {
                                    getFieldDecorator('Description', {
                                        initialValue: promotion && promotion.description,
                                        rules: [{ required: false, message: 'Please input  description!' }],
                                    })(<TextArea rows={4} style={{ resize: "none" }}
                                        placeholder="Description"
                                        name="description"
                                        onChange={onInputChange}
                                    />)
                                }
                            </Form.Item>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={12}>
                        <div className="gutter-box">
                            <Form.Item key="8" label="PromotionFor">
                                {
                                    getFieldDecorator('PromotionFor', {
                                        // initialValue: null,
                                        rules: [{ required: false, message: 'Please input  description!' }],
                                    })(<Select
                                        placeholder="Select"
                                        mode="multiple"
                                        style={{}}
                                        // onChange={onInputChange}
                                         onChange={onHandleChangePromotionFor}
                                        showSearch={true}
                                        optionFilterProp="children">
                                        {
                                            userTypes &&
                                            onCreateSelectItems(userTypes, "value", "caption", "userType")
                                        }
                                    </Select>)
                                }
                            </Form.Item>
                        </div>
                    </Col>

                </Row>
                <Row gutter={24} style={{ paddingLeft: 50, paddingRight: 50 }}>
                    <Col className="gutter-row" span={6}>
                        <div className="gutter-box">
                            <Form.Item key="4" label="Available Start Date" >
                                {getFieldDecorator('availableStartDate', {
                                    initialValue: moment(promotion && promotion.validFrom),
                                    rules: [{
                                        required: false,
                                        message: 'Please select AvailableStartDate !'
                                    }],
                                })(<DatePicker
                                    // disabledDate={disabledStartDate}
                                    showTime
                                    format="YYYY-MM-DD HH:mm:ss"
                                    // value={startValue}
                                    placeholder="Start"
                                    onChange={onHandleChangeAvailableStartDate}
                                // onOpenChange={handleStartOpenChange}
                                />)}
                            </Form.Item>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <div className="gutter-box">
                            <div className="gutter-box">
                                <Form.Item key="5" label="Available End Date" >
                                    {getFieldDecorator('availableEndDate', {
                                        initialValue: moment(promotion && promotion.validTo),
                                        rules: [{
                                            required: false,
                                            message: 'Please select AvailableStartDate !'
                                        }],
                                    })(<DatePicker
                                        // disabledDate={disabledStartDate}
                                        showTime
                                        format="YYYY-MM-DD HH:mm:ss"
                                        // value={startValue}
                                        placeholder="Start"
                                        onChange={onHandleChangeAvailableEndDate}
                                    // onOpenChange={handleStartOpenChange}
                                    />)}
                                </Form.Item>
                            </div>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <div className="gutter-box">
                            <div className="gutter-box">
                                <Form.Item key="6" label="Discount">
                                    {
                                        getFieldDecorator('discount', {
                                            initialValue: "",
                                            rules: [{
                                                required: true,
                                                message: 'Please Select  Discount!'
                                            }],
                                        })(
                                            <Select
                                                placeholder="Select"
                                                style={{}}
                                                showSearch={true}
                                                onChange={onHandleChangeDiscount}
                                                optionFilterProp="children">
                                                <Option value="1">Persentage</Option>
                                                <Option value="2">Amount</Option>
                                                <Option value="3">NewPrice</Option>
                                            </Select>,

                                        )
                                    }
                                </Form.Item>
                            </div>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={6} style={{ paddingLeft: 80 }}>
                        <div className="gutter-box">
                            <div className="gutter-box">
                                <Form.Item key="12" style={{ paddingTop: 36 }}>
                                    {getFieldDecorator('free', {
                                        valuePropName: 'checked',
                                        initialValue: promotion && promotion.active,
                                    })(<Checkbox
                                        onChange={onInputChange}
                                        name="active"
                                    >Active</Checkbox>)}
                                </Form.Item>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}
export const PromotionMasterComponent = Form.create<IPromotionMasterComponentProps>()(PromotionMaster);
import { IErrorState } from "_app/shared/common";
import { FormComponentProps } from "antd/lib/form";
import { ProductInfo,  PromotionItemPostModel } from "_app/api";

export interface IPromotionAddItemToGridProps  extends FormComponentProps {
    products:ProductInfo[] | undefined;
    rollValue:string;
    onHandleItemPush:(item:any)=>void;
    
}

export interface IPromotionAddItemToGridState extends IErrorState {
    isBusy: boolean;
    promotionGetType:number;
    itemId:string;
    title:string;
    id:string;
    getPrice:number;
    active:boolean;
    item:PromotionItemPostModel
}
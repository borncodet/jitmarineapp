import { PromotionInfo } from "_app/api";
import { FormComponentProps } from "antd/lib/form";

export interface IPromotionsListProps  extends FormComponentProps {
     promotions:PromotionInfo[] | undefined;
     onEditButtonClick:(id: string) => void;
    }
    
    export interface IPromotionsListState {
        isBusy: boolean;
        visible: boolean
    }
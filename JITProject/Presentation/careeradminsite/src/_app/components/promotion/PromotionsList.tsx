import React from 'react';
import { Form, Table } from 'antd';
import { PromotionInfo } from '_app/api';
import { IPromotionsListProps } from '.';
import PromotionListOperation from './PromotionListOperation';



// const initialState = {
// } as IPromotionItemstState

const PromotionsList: React.FC<IPromotionsListProps> = (props: IPromotionsListProps) => {
    
     const {promotions,onEditButtonClick,}=props
    // const { getFieldDecorator } = props.form;

    // const [
    //     promotionItemsState,
    //     setPromotionItemsState
    // ] = React.useState<IPromotionItemstState>(initialState);

    const handleDeleteItem = async (id: string) => {
        console.log(" handleDeleteItem " + id);
    }

    const handleEditItem = async (id: string) => {
        onEditButtonClick(id);
        console.log(" handleEditItem " + id);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Promotion',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '15%',
            render: (text: any, record: PromotionInfo, index: number) => {
                return <PromotionListOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }
    return (
        <div style={{ border: '1px solid Gray', backgroundColor: 'white', borderRadius: 5, padding: 10, marginTop: 10 }}>

            <div style={{ padding: '10px', marginTop: '15px' }}>

                {
                    // audios && audios.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                              dataSource={promotions}
                            columns={getcolumnHeaders()}
                        />
                    )
                    // : null
                }

            </div>
        </div>
    )
}
export const PromotionsListComponent = Form.create<IPromotionsListProps>()(PromotionsList);
import React from 'react';
import { IPromotionItemsProps } from '_app/components/promotion';
import { Form, Table } from 'antd';
import { PromotionItemInfo } from '_app/api';
import PromotionListOperation from './PromotionListOperation';

// const initialState = {
// } as IPromotionItemstState

const PromotionItems: React.FC<IPromotionItemsProps> = (props: IPromotionItemsProps) => {
    const {items}=props;
    // const { getFieldDecorator } = props.form;

    // const [
    //     promotionItemsState,
    //     setPromotionItemsState
    // ] = React.useState<IPromotionItemstState>(initialState);

    const handleDeleteItem = async (id: string) => {
        console.log(" handleDeleteItem " + id);
    }

    const handleEditItem = async (id: string) => {
        console.log(" handleEditItem " + id);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Product',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '15%',
            render: (text: any, record: PromotionItemInfo, index: number) => {
                return <PromotionListOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }
    return (
        <div style={{ border: '1px solid Gray', backgroundColor: 'white', borderRadius: 5, padding: 10, marginTop: 10 }}>

            <div style={{ padding: '10px', marginTop: '15px' }}>

                {
                    // audios && audios.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                           dataSource={items}
                        columns={getcolumnHeaders()}
                        />
                    )
                    // : null
                }

            </div>
        </div>
    )
}
export const PromotionItemsComponent = Form.create<IPromotionItemsProps>()(PromotionItems);
import { FormComponentProps } from "antd/lib/form";
import { PromotionInfo, ValueCaptionPair } from "_app/api";

export interface IPromotionMasterComponentProps  extends FormComponentProps {
    promotion?: PromotionInfo | undefined;
    onHandleRollChange:(value:any)=>void;
    onHandleMasterPush:(master:any)=>void;
    userTypes?: ValueCaptionPair[] | undefined;
    onHandleRollPush:(roll:any)=>void;
}

export interface IPromotionMasterComponentState  {
    isBusy: boolean;
    discountId:any;
    validFrom:any;
    validTo:any;
    loading: boolean,
    hasMore: boolean,
    data:any[];
    roleId:any;
    promotionMasterEditForm:PromotionInfo;
}
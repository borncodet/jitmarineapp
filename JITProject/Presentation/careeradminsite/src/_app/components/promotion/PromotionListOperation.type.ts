export interface IPromotionListOperastionProps   {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}

export interface IPromotionListOperastionState {
    isBusy: boolean;
}
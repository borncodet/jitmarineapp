import React from 'react';
import { Button, Modal, Form, Input, Tooltip, Select, Upload, Icon } from 'antd';
import {
    IAudioFileModalProps,
    IAudioFileModalState
} from '.';
import TextArea from 'antd/lib/input/TextArea';
import AWS from "aws-sdk";
import { UploadProps } from 'antd/lib/upload';
import { AudioFileInfo, AudioType } from '_app/api/controller';
const { Option } = Select;

const initialState = {
    audioTypeId: "",
    audioFileEditForm: new AudioFileInfo(),
    imgKey: "",
    bucket: "",
    progress:"",
    title:"",
    description:"",
     initialAudioData:[],
} as IAudioFileModalState;

const AudioFileModal: React.FC<IAudioFileModalProps> = (props) => {
    const { getFieldDecorator } = props.form;
    const { visible, onHandleEditItem, OnHandleCancel, audioFileId, audioFiles, onHandleAudioFilePush, audioId } = props;
    const [audioFileModalState,
        setAudioFileModalState] = React.useState<IAudioFileModalState>(initialState);
    const { audioFileEditForm,imgKey,bucket,progress,initialAudioData} = audioFileModalState

    //   const initialAudioData = audioFiles && audioFiles.filter(audiofile => audiofile.id === audioFileId);

    // React.useEffect(() => {
    //     setAudioFileModalState((audioFileModalState) => {
    //         return {
    //             ...audioFileModalState,
    //               initialAudioData: audioFiles && audioFiles.filter(audiofile => audiofile.id === audioFileId),
    //         }
    //     });
    // }, [])

    React.useEffect(() => {
        // const initialAudioData = audioFiles && audioFiles.filter(audiofile => audiofile.id === audioFileId);
       
        setAudioFileModalState((audioFileModalState) => {
                    return {
                        ...audioFileModalState,
                          initialAudioData: audioFiles && audioFiles.filter(audiofile => audiofile.id === audioFileId),
                    }
                });

        if (initialAudioData) {
            console.log(40)
            
            setAudioFileModalState((audioFileModalState) => {
                return {
                    ...audioFileModalState,
                    audioTypeId: `${initialAudioData && initialAudioData[0] && initialAudioData[0].audioTypeId}`,
                    audioFileEditForm: initialAudioData && initialAudioData[0]
                }
            });
        } 
        if (audioFileId==="") {
            console.log(39)
            setAudioFileModalState((audioFileModalState) => {
                return {
                    ...audioFileModalState,
                    audioTypeId: "",
                    audioFileEditForm:  new AudioFileInfo(),
                }
            });
        }
        
    }, [audioFileId])

    console.log(222,audioFileId);
    console.log(333,initialAudioData);
    console.log(555,audioFileEditForm);
    const onHandleChangeaudioType = (value: any) => {
        setAudioFileModalState((audioFileModalState) => {
            return {
                ...audioFileModalState,
                audioTypeId: value,
            }
        });
    }

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        const audioFileEditFormUpdates = {
            [name]: value,
        };

        setAudioFileModalState({
            ...audioFileModalState,
            audioFileEditForm: Object.assign(audioFileEditForm, audioFileEditFormUpdates) as AudioFileInfo
        });
    }

    // const onInputChangetitle = (event: any) => {
    //     const target = event.target;
    //     const value = target.type === 'checkbox' ? target.checked : target.value;
    //     const name = target.name;
    //     const audioFileEditFormUpdates = {
    //         [name]: value,
    //     };
    //     setAudioFileModalState((audioFileModalState) => {
    //         return {
    //             ...audioFileModalState,
    //             title: Object.assign(audioFileEditFormUpdates),
    //         }
    //     });
    // }

    
    const audioFile = {
        title: audioFileEditForm && audioFileEditForm.title,
         description: audioFileEditForm && audioFileEditForm.description,
        audioId: audioId, audioTypeId: AudioType.FullLength, audioUrl: `https://s3.ap-south-1.amazonaws.com/${bucket}/${imgKey}`,
        contentLegth: audioFileEditForm && audioFileEditForm.contentLegth, id: audioFileEditForm && audioFileEditForm.id,
        contentType: audioFileEditForm && audioFileEditForm.contentType, key: audioFileEditForm && audioFileEditForm.key
    } as AudioFileInfo;

    const showModal = () => {
        onHandleEditItem("");
    };

    const handleCancel = () => {
        OnHandleCancel();
    };

    const handleSave = () => {
        OnHandleCancel();
        onHandleAudioFilePush(audioFile);
    };

    const onStart = (file: any) => {
        console.log(21, "onStart", file, file.name);
        setAudioFileModalState((audioFileModalState) => {
            return {
                ...audioFileModalState,
                progress: `${file.name} is started to upload `,
            }
        });
    }

    const onProgress = (d: any, file: any) => {
        console.log(22, "onProgress", `${d.percent}%`, file.name);
        setAudioFileModalState((audioFileModalState) => {
            return {
                ...audioFileModalState,
                progress: `file upload ${d.percent}% "completed`,
            }
        });
    }

    const onSuccess = (ret: any, file: any) => {
        console.log(23, "onSuccess", ret, file);

    }

    const onError = (err?: any) => {
        console.log(24, "onError", err);
    }

    const uploadProps = {
        multiple: false,
        onStart: onStart,
        onSuccess: onSuccess,
        onError: onError,
        onProgress: onProgress,
        customRequest(f: any) {
            AWS.config.update({
                accessKeyId: "AKIAXVD5BCMCJQWMDSBI",
                region: "ap-south-1",
                secretAccessKey: "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"
            });

            const S3 = new AWS.S3();
            console.log(25, "DEBUG filename", f.file.name);
            console.log(26, "DEBUG file type", f.file.type);

            const objParams = {
                Bucket: "btc.audiotrial",
                Key: (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase() + f.file.name,
                Body: f.file,
                ACL: 'public-read'
            };
            S3.putObject(objParams)
                .on("httpUploadProgress", ({ loaded, total }) => {
                    onProgress(
                        {
                            percent: Math.round((loaded / total) * 100)
                        },
                        f.file
                    );
                })
                .send((err: any, data: any) => {
                    console.log(data, 20);
                    if (err) {
                        onError();
                        console.log("Something went wrong");
                        console.log(err.code);
                        console.log(err.message);
                    } else {
                        
                        onSuccess(data.response, f.file);
                        console.log("SEND FINISHED");
                        setAudioFileModalState((audioFileModalState) => {
                            return {
                                ...audioFileModalState,
                                imgKey: objParams.Key,
                                bucket:objParams.Bucket                           }
                        });
                    }
                });
        }
    } as UploadProps;
     
    return (
        <>
            <div>
                <Tooltip title="Add new Audio File">
                    <Button type="primary"
                        onClick={showModal}
                    >
                        Add New Audio File
                    </Button>
                </Tooltip>
                <Modal
                    title={audioFileId ? "Edit Audio File" : "Add new Audio File"}
                    visible={visible}
                    onOk={handleSave}
                    okText="Save"
                    onCancel={handleCancel}
                >
                    <Form>
                        <Form.Item
                            key="1"
                            label="Title">
                            {
                                getFieldDecorator('title', {
                                    initialValue: initialAudioData && initialAudioData[0] && initialAudioData[0].title,
                                    rules: [{ required: true, message: 'Please input  Title!' }],
                                })(
                                    <Input placeholder="Title"
                                        name="title"
                                        onChange={onInputChange}
                                    />
                                )
                            }
                        </Form.Item>
                        <Form.Item key="2" label="Description">
                            {
                                getFieldDecorator('Description', {
                                    initialValue: initialAudioData && initialAudioData[0] && initialAudioData[0].description,
                                    rules: [{ required: false, message: 'Please input  description!' }],
                                })(<TextArea rows={4} style={{ resize: "none" }}
                                    placeholder="Description"
                                    name="description"
                                     onChange={onInputChange}
                                />)
                            }
                        </Form.Item>
                        <Form.Item key="3" label="AudioType">
                            {
                                getFieldDecorator('audioType', {
                                    initialValue: `${initialAudioData && initialAudioData[0] && initialAudioData[0].audioTypeId}`,
                                    rules: [{
                                        required: true,
                                        message: 'Please Select  AudioType!'
                                    }],
                                })(
                                    <Select
                                        placeholder="Select"
                                        onChange={onHandleChangeaudioType}
                                    >
                                        <Option value="1">Introduction</Option>
                                        <Option value="2"> FullLength</Option>
                                    </Select>
                                )
                            }
                        </Form.Item>
                        <Form.Item key="4" label="Upload AudioFile">
                            {
                                getFieldDecorator('uploadAudioFile', {
                                    initialValue: null,
                                    rules: [{
                                        required: true,
                                        message: 'Please upload AudioFile!'
                                    }],
                                })(
                                    <Upload {...uploadProps}>
                                        <Button>
                                            <Icon type="upload" /> Click to Upload
                                        </Button>
                                    </Upload>
                                )
                            }
                        </Form.Item>
                    </Form>
                    <h3 style={{ color: "green" }}>{progress}</h3>
                </Modal>
            </div>
        </>
    )
}

export const AudioFileIModalForm = Form.create<IAudioFileModalProps>()(AudioFileModal);
import React from 'react';
import { AudioListItemOperation, IAudioDetailsListState, IAudioDetailsListProps, AudioDetailsFormModalComponent } from '.';
import { Table, Icon } from 'antd';
import { AudioClient, AudioInfo, AudioDataRequestModel } from '_app/api';
import { IToolbarContainerItemProps, ToolbarContainer } from '_app/containers/shared';

const initialState = {
    audios: [],
    audioId: "",
    isBusy: true,
    visible: false,
    isUpdate: false,
} as IAudioDetailsListState

const AudioDetailsList: React.FC<IAudioDetailsListProps> = (props) => {
    const { languages, currencies, translators, narrators, publishers, taxCategories, tabeKeyValueChange, getAudios } = props
    const { id } = props.match.params;
    const [
        audioDetailsListState,
        setAudioDetailsListState
    ] = React.useState<IAudioDetailsListState>(initialState);
    const { audios, visible, audioId, isUpdate } = audioDetailsListState

    React.useEffect(() => {
        (async () => {
            const audioApi = new AudioClient();
            setAudioDetailsListState((audioDetailsListState) => {
                return {
                    ...audioDetailsListState,
                    isBusy: true
                }
            });
            try {
                const postModel = {
                    productId: id
                } as AudioDataRequestModel
                const result = await audioApi.getAllActive(postModel)
                getAudios(result.data)
                setAudioDetailsListState((audioDetailsListState) => {
                    return {
                        ...audioDetailsListState,
                        isBusy: false,
                        audios: result.data as []
                    }
                });
            } catch (error) {
                setAudioDetailsListState((audioDetailsListState) => {
                    return {
                        ...audioDetailsListState,
                        isBusy: false
                    }
                });
            }
            })();
    }, [id, isUpdate])

    const OnHandleBack = async () => {
        tabeKeyValueChange("1");
    }
    const OnHandleNext = async () => {
        tabeKeyValueChange("3");
    }

    const OnHandleCancel = async () => {
        setAudioDetailsListState((audioDetailsListState) => {
            return {
                ...audioDetailsListState,
                visible: false,
                isUpdate: true
            }
        });
    }

    const handleDeleteItem = async (id: string) => {
        console.log(" handleDeleteItem " + id);
    }

    const handleEditItem = async (id: string) => {
        console.log(" handleEditItem " + id);
        setAudioDetailsListState((audioDetailsListState) => {
            return {
                ...audioDetailsListState,
                audioId: id,
                visible: true
            }
        });
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Next",
            type: "button",
            onClick: OnHandleNext,
            icon: <Icon type="step-forward" />
        }, {
            label: "Back",
            type: "button",
            onClick: OnHandleBack,
            icon: <Icon type="step-backward" />
        } as IToolbarContainerItemProps];
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '18%',
            render: (text: any, record: AudioInfo, index: number) => {
                return <AudioListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    return (
        <>
            <ToolbarContainer
                farItems={getFarItems()} />
            < AudioDetailsFormModalComponent {...props}
                visible={visible}
                languages={languages}
                currencies={currencies}
                translators={translators}
                narrators={narrators}
                publishers={publishers}
                taxCategories={taxCategories}
                onHandleEditItem={handleEditItem}
                OnHandleCancel={OnHandleCancel}
                audioId={audioId}
                productId={id}
                tabeKeyValueChange={tabeKeyValueChange}
            />
            <div style={{ padding: '10px', marginTop: '15px' }}>

                {
                    audios && audios.length > 0 ?
                        (
                            <Table
                                pagination={false}
                                bordered={true}
                                dataSource={audios}
                                columns={getcolumnHeaders()}
                            />
                        ) : null
                }

            </div>
        </>
    )
}

export default AudioDetailsList;
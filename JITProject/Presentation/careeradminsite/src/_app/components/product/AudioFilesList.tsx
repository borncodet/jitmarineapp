import React from 'react';
import { Table } from 'antd';
import { IAudioFilesListProps, AudioListItemOperation, IAudioFilesListState } from '.';
import { AudioFileInfo } from '_app/api';
import { AudioFileIModalForm } from './AudioFilesModal';

const initialState = {
    audioFileId: "",
   visible: false 
} as IAudioFilesListState

const AudioFilesList: React.FC<IAudioFilesListProps> = (props) => {
    const { audioFiles,onHandleAudioFilePush,audioId } = props;
    const [
        audioFilesListState,
        setAudioFilesListState
    ] = React.useState<IAudioFilesListState>(initialState);
    const { audioFileId, visible } = audioFilesListState

    const handleDeleteItem = async (id: string) => {
        console.log(" handleDeleteItem " + id);
    }

    const handleEditItem = async (id: string) => {
        console.log(" handleEditItem " + id);
        setAudioFilesListState((audioFilesListState) => {
            return {
                audioFileId: id,
                visible: true
            }
        });

    }

    const OnHandleCancel = async () => {
        setAudioFilesListState((audioFilesListState) => {
            return {
                ...audioFilesListState,
                visible: false
            }
        });
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Audio File Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '15%',
            render: (text: any, record: AudioFileInfo, index: number) => {
                return <AudioListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    return (
        <>
            <AudioFileIModalForm
                visible={visible}
                onHandleEditItem={handleEditItem}
                OnHandleCancel={OnHandleCancel}
                audioFileId={audioFileId}
                audioId={audioId}
                audioFiles={audioFiles}
                onHandleAudioFilePush={ onHandleAudioFilePush}
            />
            <div style={{ padding: '10px', marginTop: '15px' }}>

                {
                    // audios && audios.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            dataSource={audioFiles}
                            columns={getcolumnHeaders()}
                        />
                    )
                    // : null
                }

            </div>
        </>
    )
}

export default AudioFilesList;
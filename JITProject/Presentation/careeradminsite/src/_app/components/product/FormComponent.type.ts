import { FormComponentProps } from "antd/lib/form";
import { ValueCaptionPair, ValueCaptionPairWithParent, ProductEditModel, AudioInfo, AudioEditModel, AudioFileInfo, AudioPictureInfo } from "_app/api";
import { RouteComponentProps } from "react-router";

export interface IBasicDetailsFormProps extends FormComponentProps {
    authors?: ValueCaptionPair[];
    categories?: ValueCaptionPair[];
    subCategories?: ValueCaptionPairWithParent[];
    productEditForm: ProductEditModel;
    tabeKeyValueChange: (value: string) => void;
}

export interface IBasicDetailsFormState {
    categoryIds?: any;
    basicDetailsEditForm: ProductEditModel,
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean,
    authorIds?:any;
    subCategoryIds?:any;
}

export interface IAudioDetailsListProps extends RouteComponentProps<any> {
    languages?: ValueCaptionPair[];
    publishers?: ValueCaptionPair[];
    narrators?: ValueCaptionPair[];
    currencies?: ValueCaptionPair[];
    taxCategories?: ValueCaptionPair[];
    translators?: ValueCaptionPair[];
    tabeKeyValueChange: (value: string) => void;
    getAudios:(audioData:any)=>void;
}

export interface IAudioDetailsListState {
    audios: AudioInfo[] | null;
    isBusy: boolean;
    // audioDetailsEditForm:AudioEditModel;
    visible: boolean;
    audioId: string;
    isUpdate: boolean;
}

export interface IImageDetailsFormProps extends FormComponentProps{
    // type: UploadListType;
    tabeKeyValueChange: (value: string) => void;
    audios:AudioInfo[] | null;
}

export interface IImageDetailsFormState {
    // type: UploadListType;
    // tabeKeyValueChange: (value: string) => void;
    imgKey:string;
    bucket:string;
    isCreatedOrUpdated:boolean;
     audioPictures:AudioPictureInfo[];
     displayValue:number;
     title:string;
     audioId:string;
     progress:string;
}

export interface IAudioListItemOperationProps {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}

export interface IAudioDetailsFormModalProps extends FormComponentProps {
    productId?: string;
    languages?: ValueCaptionPair[];
    publishers?: ValueCaptionPair[];
    narrators?: ValueCaptionPair[];
    currencies?: ValueCaptionPair[];
    taxCategories?: ValueCaptionPair[];
    translators?: ValueCaptionPair[];
    visible: boolean;
    onHandleEditItem: (id: string) => void;
    OnHandleCancel: () => void;
    audioId?: string;
    tabeKeyValueChange: (value: string) => void;
}

export interface IAudioDetailsFormModalState {
    // visible:boolean;
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean,
    audioDetailsEditForm: AudioEditModel;
    languageId: any;
    publisherId: any;
    bookPublisherId: any;
    narratorIds: any;
    translatorIds: any;
    currencyId: any;
    taxCategoryId: any;
    availableStartDateTimeUtc:any;
    audioFiles: AudioFileInfo[] | undefined;
    displayOrder:number;
}

export interface IAudioFileModalProps extends FormComponentProps {
    visible: boolean;
    onHandleEditItem: (id: string) => void;
    OnHandleCancel: () => void;
    audioFileId: string;
    audioId?: string;
    audioFiles: AudioFileInfo[] | undefined
    onHandleAudioFilePush: (audioFile:any) => void;
}

export interface IAudioFileModalState {
    audioTypeId:any;
    audioFileEditForm: AudioFileInfo;
    imgKey:string;
    bucket:string;
    progress:string;
    title:string;
    description:string;
  initialAudioData:AudioFileInfo[] | undefined;
}

export interface IAudioFilesListProps {
    audioFiles: AudioFileInfo[] | undefined,
    audioId?: string;
    onHandleAudioFilePush: (audioFile:AudioFileInfo) => void;
}

export interface IAudioFilesListState {
    audioFileId: string;
    visible: boolean;
}

export declare type UploadListType = 'text' | 'picture' | 'picture-card';

export interface IProductImageListProps {
    
}

export interface IProductImageListState {
    
}

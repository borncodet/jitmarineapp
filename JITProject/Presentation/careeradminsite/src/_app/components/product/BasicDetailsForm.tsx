import React from 'react';
import { IBasicDetailsFormProps, IBasicDetailsFormState } from '.';
import { Form, Input, Select, Button } from 'antd';
import { ProductEditModel, ProductClient, ProductPostmodel } from '_app/api';

const initialState = {
    basicDetailsEditForm: new ProductEditModel(),
    isCreateOrUpdateTriggered: false,
    isBusy: false,
    categoryIds: [],
    authorIds: [],
    subCategoryIds: [],
} as IBasicDetailsFormState

const BasicDetailsForm: React.FC<IBasicDetailsFormProps> = (props) => {
    const { categories, authors, subCategories, productEditForm, tabeKeyValueChange } = props;
    const [BasicDetailsFormState, setBasicDetailsFormState] = React.useState<IBasicDetailsFormState>(initialState);

    React.useEffect(() => {
        if (productEditForm.id !== undefined) {
            setBasicDetailsFormState({
                basicDetailsEditForm: new ProductEditModel(),
                isCreateOrUpdateTriggered: false,
                isBusy: false,
                categoryIds: productEditForm && productEditForm.categories &&
                    productEditForm.categories.map(c => c.value),
                authorIds: productEditForm && productEditForm.authors &&
                    productEditForm.authors.map(c => c.value),
                subCategoryIds:productEditForm && productEditForm.subCategories &&
                productEditForm.subCategories.map(c => c.value),
            });
        }
    }, [productEditForm])

    const { categoryIds, isCreateOrUpdateTriggered, basicDetailsEditForm, authorIds, subCategoryIds } = BasicDetailsFormState
    const { getFieldDecorator } = props.form;

    // React.useEffect(() => {
    //     {
    //         if(productEditForm && productEditForm.authors)
    //         (async () => {
    //             setBasicDetailsFormState((BasicDetailsFormState) => {
    //                 return ({
    //                     ...BasicDetailsFormState,
    //                     // authorIds: productEditForm && productEditForm.authors &&
    //                         // productEditForm.authors.map(c => c.value),
    //                       authorIds:["123456"]
    //                 });
    //             });
    //         })();
    //     }
    // }, [])

    //multy selectbox option push
    const onCreateSelectItems = (data: any[], valueFieldName: string, captionFieldName: string, keyName: string) => {
        let items = [];
        if (data) {
            for (let i = 0; i < data.length; i++) {
                items.push(
                    <Select.Option
                        key={keyName + i}
                        value={data[i][valueFieldName]}>
                        {data[i][captionFieldName]}
                    </Select.Option>
                );
            }
        }
        return items;
    }

    //multy selectbox option based on the parent Id
    const onCreateSelectItemsWithParentId = (data: any[], valueFieldName: string,
        captionFieldName: string, keyName: string, parentIdFiledName: string, parentIds: string[], selectedSubItems: any[]) => {
        let items = [], subItems = [];


        if (data && parentIds) {
            //list subItems
            for (let i = 0; i < data.length; i++) {
                for (let j = 0; j < parentIds.length; j++) {
                    if (data[i][parentIdFiledName] === parentIds[j]) {
                        subItems.push(data[i]);
                    }
                }
            }


            //create push list of subItem to return
            for (let i = 0; i < subItems.length; i++) {
                items.push(
                    <Select.Option
                        key={keyName + i}
                        value={subItems[i][valueFieldName]}>
                        {subItems[i][captionFieldName]}
                    </Select.Option>
                );
            }
            //check whether already  selected subItem is in subitem
            //and if it is not excist,then remove from the selected sub item
            
            if (selectedSubItems) {
                for (let i = 0, isExist = false; i < selectedSubItems.length; i++ , isExist = false) {
                    for (let j = 0; j < subItems.length; j++) {
                        if (selectedSubItems[j] === subItems[j][valueFieldName]) {
                            isExist = true;
                            break;
                        }
                    }
                    if (!isExist) {
                        selectedSubItems.splice(i, 1);
                    }
                }
            }
        }
        return items;
    }

    const onHandleChangeAuthor = (value: any) => {
        setBasicDetailsFormState((BasicDetailsFormState) => {
            return {
                ...BasicDetailsFormState,
                authorIds: value,
            }
        });
    }

    const onHandleChangeCategory = (value: any) => {
        setBasicDetailsFormState((BasicDetailsFormState) => {
            return {
                ...BasicDetailsFormState,
                categoryIds: value,
            }
        });
    }

    const onHandleChangeSubCategory = (value: any) => {
        setBasicDetailsFormState((BasicDetailsFormState) => {
            return {
                ...BasicDetailsFormState,
                subCategoryIds: value,
            }
        });
    }

    React.useEffect(() => {

        if (isCreateOrUpdateTriggered) {
            (async () => {
                const productApi = new ProductClient();
                try {
                    const { productName, id, active } = basicDetailsEditForm;
                    const postModel = {
                        id,
                        productName,
                        subCategoryIds,
                        authorIds,
                        active
                    } as ProductPostmodel;
                    const result = await productApi.createOrEdit(postModel);
                    if (result.isSuccess) {
                        setBasicDetailsFormState((BasicDetailsFormState) => {
                            return {
                                ...BasicDetailsFormState,
                                isCreateOrUpdateTriggered: false,
                                isBusy: false
                            }
                        })
                    }
                } catch (error) {
                    console.error(error);
                    setBasicDetailsFormState((BasicDetailsFormState) => {
                        return {
                            ...BasicDetailsFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered])

    const onSaveButonClick = (e:any) => {
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
              setBasicDetailsFormState((BasicDetailsFormState) => {
            return {
                ...BasicDetailsFormState,
                isCreateOrUpdateTriggered: true,
            }
        });
        tabeKeyValueChange("2");
            }
          });
    }

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        const basicDetailsFormUpdates = {
            [name]: value,
        };
        setBasicDetailsFormState({
            ...BasicDetailsFormState,
            basicDetailsEditForm: Object.assign(basicDetailsEditForm, basicDetailsFormUpdates) as ProductEditModel
        });
    }

    return (
        <>
            <Form
                 onSubmit={onSaveButonClick} 
                 style={{ paddingRight: 300 }}
                >
                <Form.Item
                    key="1"
                    label="Name">
                    {
                        getFieldDecorator('Product Name', {
                            initialValue: "",
                            rules: [{ required: true, message: 'Please input product name!' }],
                        })(
                            <Input placeholder="Name"
                                name="productName"
                                onChange={onInputChange}
                            />
                        )
                    }
                </Form.Item>
                <Form.Item key="2" label="Author">
                    {
                        getFieldDecorator('Author', {
                            initialValue: productEditForm && productEditForm.authors &&
                                productEditForm.authors.map(c => c.value),
                            rules: [{
                                required: true,
                                message: 'Please Select  Author!'
                            }],
                        })(
                            <Select
                                placeholder="Select"
                                mode="multiple"

                                onChange={onHandleChangeAuthor}
                                showSearch={true}
                                optionFilterProp="children">
                                {
                                    authors &&
                                    onCreateSelectItems(authors, "value", "caption", "author")
                                }
                            </Select>
                        )
                    }
                </Form.Item>
                <Form.Item key="3" label="Category">
                    {
                        getFieldDecorator('Category', {
                            initialValue: productEditForm && productEditForm.categories &&
                                productEditForm.categories.map(c => c.value),
                            rules: [{
                                required: true,
                                message: 'Please Select  Category!'
                            }],
                        })(
                            <Select
                                placeholder="Select one"
                                style={{}}
                                mode="multiple"
                                showSearch={true}
                                onChange={onHandleChangeCategory}
                                optionFilterProp="children">
                                {
                                    categories &&
                                    onCreateSelectItems(categories,
                                        "value", "caption", "category")
                                }
                            </Select>
                        )
                    }
                </Form.Item>
                <Form.Item key="4" label="SubCategory">
                    {
                        getFieldDecorator('SubCategory', {
                            initialValue: productEditForm && productEditForm.subCategories &&
                                productEditForm.subCategories.map(c => c.caption),
                            rules: [{
                                required: true,
                                message: 'Please Select  SubCategory!'
                            }],
                        })(
                            <Select
                                placeholder="Select one"
                                mode="multiple"
                                style={{}}
                                showSearch={true}
                                onChange={onHandleChangeSubCategory}
                                optionFilterProp="children">
                                {
                                    subCategories &&
                                    onCreateSelectItemsWithParentId(subCategories,
                                        "value", "caption", "category", "parentId", categoryIds, subCategoryIds )
                                }
                            </Select>
                        )
                    }
                </Form.Item>
                <Button type="primary" htmlType="submit" >
            save
          </Button>
          <Button>
                Cancel
            </Button>
            </Form>
            {/* <Button
                htmlType="button"
                style={{ marginRight: 12 }}
                onClick={onSaveButonClick}
                type="primary">
                Save
            </Button> */}
            {/* <Button>
                Cancel
            </Button> */}
        </>
    )

}

export const BasicDetailsFormComponent = Form.create<IBasicDetailsFormProps>()(BasicDetailsForm);
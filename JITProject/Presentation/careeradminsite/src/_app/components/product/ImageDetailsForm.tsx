import React from 'react';
import { IImageDetailsFormProps, IImageDetailsFormState, ProductImageList } from '.';
import { Upload, Button, Icon, Select, Input, Form } from 'antd';
import { UploadProps } from 'antd/lib/upload';
import { ToolbarContainer, IToolbarContainerItemProps } from '_app/containers/shared';
import AWS from "aws-sdk";
import { AudioClient, AudioPicturePostModel, AudioPictureInfo, PictureInfo } from '_app/api';


const initialState = {
    imgKey: "",
    bucket: "",
    isCreatedOrUpdated: false,
    audioPictures: [],
    displayValue: 0,
    title: "",
    audioId: "",
    progress: ""
} as IImageDetailsFormState

const ImageDetailsForm: React.FC<IImageDetailsFormProps> = (props) => {
    const { getFieldDecorator } = props.form;

    const [
        imageDetailsFormState,
        setImageDetailsFormState
    ] = React.useState<IImageDetailsFormState>(initialState);

    const { tabeKeyValueChange, audios } = props
    const { imgKey, bucket, isCreatedOrUpdated, audioPictures, displayValue, title, audioId, progress } = imageDetailsFormState


    React.useEffect(() => {
        if (isCreatedOrUpdated) {
            (async () => {
                const audioApi = new AudioClient();

                setImageDetailsFormState((imageDetailsFormState) => {
                    return {
                        ...imageDetailsFormState,
                        isCreatedOrUpdated: false,

                    }
                });

                //    const audioPictures=audioPicture[]
                try {
                    const result = await audioApi.saveProductPictures({ audioPictures } as AudioPicturePostModel);
                    setImageDetailsFormState((imageDetailsFormState) => {
                        return {
                            ...imageDetailsFormState,

                        }
                    });

                } catch (error) {
                    setImageDetailsFormState((imageDetailsFormState) => {
                        return {
                            ...imageDetailsFormState,
                            isCreatedOrUpdated: false,
                        }
                    });
                }

            })();
        }
    }, [isCreatedOrUpdated])

    const onCreateSelectItems = (data: any[], valueFieldName: string, captionFieldName: string, keyName: string) => {
        let items = [];
        if (data) {
            for (let i = 0; i < data.length; i++) {
                items.push(
                    <Select.Option
                        key={keyName + i}
                        value={data[i][valueFieldName]}>
                        {data[i][captionFieldName]}
                    </Select.Option>
                );
            }
        }
        return items;
    }

    const OnHandleBack = async () => {
        tabeKeyValueChange("2");
    }

    const handleSave = async (e: any) => {
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                setImageDetailsFormState((imageDetailsFormState) => {
                    return {
                        ...imageDetailsFormState,
                        isCreatedOrUpdated: true,
                    }
                });
            }
        })
    }

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        // const audioFileEditFormUpdates = {
        //     [name]: value,
        // }
        setImageDetailsFormState((imageDetailsFormState) => {
            return {
                ...imageDetailsFormState,
                // title: Object.assign(audioFileEditFormUpdates),
                title:value,
            }
        });

    }

    const onValueSelect = (value: any) => {
        setImageDetailsFormState((imageDetailsFormState) => {
            return {
                ...imageDetailsFormState,
                audioId: value,
            }
        });
        console.log(8889, value)
    }
    console.log(75648, audios)

    const getFarItems = (): IToolbarContainerItemProps[] => {
        return [{
            label: "Back",
            type: "button",
            onClick: OnHandleBack,
            icon: <Icon type="step-backward" />
        } as IToolbarContainerItemProps];
    }

    // const imgData = {
    //     action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    //     listType: 'picture'
    // } as UploadProps;

    const onStart = (file: any) => {
        console.log(21, "onStart", file, file.name);
        setImageDetailsFormState((imageDetailsFormState) => {
            return {
                ...imageDetailsFormState,
                progress: `${file.name} is started to upload `,
            }
        });

    }

    const onProgress = (d: any, file: any) => {
        console.log(22, "onProgress", `${d.percent}%`, file.name);
        setImageDetailsFormState((imageDetailsFormState) => {
            return {
                ...imageDetailsFormState,
                progress: `file upload ${d.percent}% "completed`,
            }
        });

    }

    const onSuccess = (ret: any, file: any) => {
        console.log(23, "onSuccess", ret, file);
        const pictureInfo = { filename: imgKey, mimeType: "", seoFilename: imgKey, titleAttribute: title, isNew: true, altAttribute: "", active: true } as PictureInfo
        const audioPicture = { id: "", audioId: audioId, displayOrder: displayValue, pictureInfo: pictureInfo, active: true } as AudioPictureInfo

        setImageDetailsFormState((imageDetailsFormState) => {
            return {
                ...imageDetailsFormState,
                isCreatedOrUpdated: false,
                displayValue: displayValue + 1,
                //    audioPictures: audioPictures && audioPictures.concat(audioPicture)
                audioPictures: [...audioPictures, audioPicture]
            }
        });

    }

    const onError = (err?: any) => {
        console.log(24, "onError", err);
    }

    const uploadProps = {

        multiple: false,
        onStart: onStart,
        onSuccess: onSuccess,
        onError: onError,
        onProgress: onProgress,
        customRequest(f: any) {
            AWS.config.update({
                accessKeyId: "AKIAXVD5BCMCJQWMDSBI",
                region: "ap-south-1",
                secretAccessKey: "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"
            });

            const S3 = new AWS.S3();
            console.log(25, "DEBUG filename", f.file.name);
            console.log(26, "DEBUG file type", f.file.type);

            const objParams = {
                Bucket: "btc.audiotrial",
                Key: (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase() + f.file.name,
                Body: f.file,
                ACL: 'public-read'
            };
            S3.putObject(objParams)
                .on("httpUploadProgress", ({ loaded, total }) => {
                    onProgress(
                        {
                            percent: Math.round((loaded / total) * 100)
                        },
                        f.file
                    );
                })
                .send((err: any, data: any) => {
                    console.log(data, 20);
                    if (err) {
                        onError();
                        console.log("Something went wrong");
                        console.log(err.code);
                        console.log(err.message);
                    } else {
                        console.log(10, objParams)
                        onSuccess(data.response, f.file);
                        console.log("SEND FINISHED");
                        setImageDetailsFormState((imageDetailsFormState,
                        ) => {
                            return {
                                ...imageDetailsFormState,
                                imgKey: objParams.Key,
                                bucket: objParams.Bucket,
                            }
                        });
                    }
                });
        }
    } as UploadProps;
    console.log(777, imgKey);
    console.log(888, bucket);
    const imgUrl = `https://s3.ap-south-1.amazonaws.com/${bucket}/${imgKey}`;
    console.log(999, imgUrl);
    return (
        <>
            <ToolbarContainer
                farItems={getFarItems()} />
            <div style={{ paddingLeft: 100, paddingRight: 100, }}>
                <Form
                    onSubmit={handleSave}
                >
                    <Form.Item
                        key="1"
                        label="Title">
                        {
                            getFieldDecorator('title', {
                                // initialValue: initialAudioData && initialAudioData[0] && initialAudioData[0].title,
                                rules: [{ required: true, message: 'Please input  Title!' }],
                            })(
                                <Input placeholder="Title"
                                    name="title"
                                    onChange={onInputChange}
                                />
                            )
                        }
                    </Form.Item>
                    <Form.Item key="2" label="Audio">
                        {
                            getFieldDecorator('Publisher', {
                                // initialValue: audioDetailsEditForm && audioDetailsEditForm.publisher.caption,
                                rules: [{
                                    required: true,
                                    message: 'Please Select  Audio!'
                                }],
                            })(
                                <Select
                                    placeholder="Select"
                                    style={{}}
                                    showSearch={true}
                                    onChange={onValueSelect}
                                    optionFilterProp="children">
                                    {
                                        audios &&
                                        onCreateSelectItems(audios, "id", "title", "key")
                                    }
                                </Select>
                            )
                        }
                    </Form.Item>


                    <h4>Upload Book Images</h4>

                    <div>
                        <Upload {...uploadProps}>
                            <Button>
                                <Icon type="upload" /> Click to Upload
                     </Button>
                        </Upload>
                        <br />
                    </div>
                    <h1 style={{ color: "green" }}>{progress}</h1>
                    <Button
                        // onClick={handleSave}
                        htmlType="submit"
                        style={{ marginRight: 12 }}>
                        Save
                    </Button>
                </Form>
                <ProductImageList/>
                {/* <Button>
                Cancel
            </Button> */}
            </div>

        </>
    )
}

export const AudioImageDetailsForm = Form.create<IImageDetailsFormProps>()(ImageDetailsForm);
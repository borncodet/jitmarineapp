import { ProductInfo } from "_app/api/controller";
import { RouteComponentProps } from "react-router";

export interface IProductCardComponentProps extends RouteComponentProps<{}>  {
    product:ProductInfo;
}
import React from 'react';
import { IAudioDetailsFormModalProps, AudioFilesList, IAudioDetailsFormModalState } from '.';
import { Button, Modal, Form, Select, Input, Row, Col, InputNumber, Checkbox, DatePicker, Tooltip } from 'antd';
import { AudioClient, AudioPostmodel, BaseViewModel, AudioEditModel } from '_app/api';
import moment from 'moment';
const { TextArea } = Input;

const initialState = {
    isCreateOrUpdateTriggered: false,
    isBusy: false,
    audioDetailsEditForm: new AudioEditModel(),
    languageId: "",
    publisherId: "",
    bookPublisherId: "",
    narratorIds: [],
    translatorIds: [],
    currencyId: "",
    taxCategoryId: "",
    audioFiles: [],
    availableStartDateTimeUtc:null,
    displayOrder:0,
} as IAudioDetailsFormModalState

const AudioDetailsFormModal: React.FC<IAudioDetailsFormModalProps> = (props) => {
    const { getFieldDecorator } = props.form;
    const { languages, currencies, translators,
        narrators, publishers, taxCategories,
        onHandleEditItem, visible, OnHandleCancel, audioId, productId, tabeKeyValueChange } = props;

    const [audioDetailsFormModalState,
        setAudioDetailsFormModalState] = React.useState<IAudioDetailsFormModalState>(initialState);

    const { isCreateOrUpdateTriggered, audioDetailsEditForm, languageId, publisherId, bookPublisherId,
        narratorIds, translatorIds, currencyId, taxCategoryId, audioFiles,availableStartDateTimeUtc ,
        displayOrder} = audioDetailsFormModalState

    React.useEffect(() => {
        if (audioDetailsEditForm.id !== null) {
            setAudioDetailsFormModalState({
                ...audioDetailsFormModalState,
                 availableStartDateTimeUtc:moment(audioDetailsEditForm && audioDetailsEditForm.availableStartDateTimeUtc).toDate(),
                audioFiles: audioDetailsEditForm && audioDetailsEditForm.audioFiles,
                languageId: audioDetailsEditForm && audioDetailsEditForm.language.value,
                publisherId: audioDetailsEditForm && audioDetailsEditForm.publisher.value,
                bookPublisherId: audioDetailsEditForm && audioDetailsEditForm.bookPublisher.value,
                narratorIds: audioDetailsEditForm && audioDetailsEditForm.narrators.map(c => c.value),
                translatorIds: audioDetailsEditForm && audioDetailsEditForm.translators &&
                    audioDetailsEditForm.translators.map(c => c.value),
                currencyId: audioDetailsEditForm && audioDetailsEditForm.currency && audioDetailsEditForm.currency.value,
                taxCategoryId: audioDetailsEditForm && audioDetailsEditForm.taxCategory && audioDetailsEditForm.taxCategory.value,
            });
        }
    }, [audioDetailsEditForm, languages, currencies, translators, narrators, publishers, taxCategories,
        // audioFiles
    ])
    //multy selectbox option push
    const onCreateSelectItems = (data: any[], valueFieldName: string, captionFieldName: string, keyName: string) => {
        let items = [];
        if (data) {
            for (let i = 0; i < data.length; i++) {
                items.push(
                    <Select.Option
                        key={keyName + i}
                        value={data[i][valueFieldName]}>
                        {data[i][captionFieldName]}
                    </Select.Option>
                );
            }
        }
        return items;
    }

    React.useEffect(() => {
        (async () => {
            const audioApi = new AudioClient();
            setAudioDetailsFormModalState((audioDetailsListState) => {
                return {
                    ...audioDetailsFormModalState,
                    isBusy: true
                }
            });
            try {
                const result = await audioApi.get({ id: audioId } as BaseViewModel);
                setAudioDetailsFormModalState((audioDetailsFormModalState) => {
                    return {
                        ...audioDetailsFormModalState,
                        isBusy: false,
                        audioDetailsEditForm: result.data as AudioEditModel
                    }
                });

            } catch (error) {
                setAudioDetailsFormModalState((audioDetailsFormModalState) => {
                    return {
                        ...audioDetailsFormModalState,
                        isBusy: false
                    }
                });
            }

        })();
    }, [audioId])

    // Save Audio Details
    React.useEffect(() => {

        if (isCreateOrUpdateTriggered) {
            (async () => {
                const audioApi = new AudioClient();
                try {
                    const { id, title, shortDescription, fullDescription, adminComment, price,
                        isFree, releaseDate, isFeaturedProduct,
                        showOnHomePage, allowCustomerReviews, published, availableForPreOrder,
                        callForPrice, sku, metaKeywords, availableStartDateTimeUtc, availableEndDateTimeUtc,
                        preOrderAvailabilityStartDateTimeUtc, active,incTax
                    } = audioDetailsEditForm;

                    const postModel = {
                         id,
                         productId,
                        title, 
                        languageId,
                        publisherId,
                        bookPublisherId,
                         narratorIds,
                        taxCategoryId,
                         shortDescription,
                        fullDescription,
                         adminComment,
                        price,
                         isFree,
                        //  releaseDate,
                           translatorIds,
                        isFeaturedProduct, 
                         showOnHomePage,
                        allowCustomerReviews,
                          published,
                          incTax,
                          availableForPreOrder, 
                         callForPrice,
                         currencyId,
                        sku,
                         metaKeywords,
                           availableStartDateTimeUtc,
                         availableEndDateTimeUtc,
                          preOrderAvailabilityStartDateTimeUtc,
                        //    displayOrder,
                        active,
                          audioFiles
                    } as AudioPostmodel;

                    const result = await audioApi.createOrEdit(postModel);
                    if (result.isSuccess) {
                        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
                            return {
                                ...audioDetailsFormModalState,
                                isCreateOrUpdateTriggered: false,
                                displayOrder:displayOrder+1,
                            }
                        })
                    }

                } catch (error) {
                    console.error(error);
                    setAudioDetailsFormModalState((audioDetailsFormModalState) => {
                        return {
                            ...audioDetailsFormModalState,
                            isCreateOrUpdateTriggered: false,

                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered])

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        const audioDetailsEditFormUpdates = {
            [name]: value,
        };
        setAudioDetailsFormModalState({
            ...audioDetailsFormModalState,
            audioDetailsEditForm: Object.assign(audioDetailsEditForm, audioDetailsEditFormUpdates) as AudioEditModel
        });
    }

    const onHandleAudioFilePush = (audioFile: any) => {
        
        if (audioFile) {
            console.log(6668,audioFile);
            if(audioFile.id===undefined){
                // audioFile.splice(audioFile.id, 1)
                // const au = audioFiles && audioFiles.splice( audioFiles && audioFiles.indexOf(audioFile.id), 1)
                //  console.log(6668,audioFiles && audioFiles.indexOf(audioFile));
                setAudioDetailsFormModalState((audioDetailsFormModalState) => {

                    return {
                        ...audioDetailsFormModalState,
                        // audioFiles: audioFiles && audioFiles.push(audioFile),
                        audioFiles: audioFiles && audioFiles.concat(audioFile)
    
                    }
                });
              
            }
           
            
        }
    }


    console.log(444,audioFiles);


    // const onHandleChangeAvailableStartDate = (value: any) => {
    //     setAudioDetailsFormModalState(( audioDetailsFormModalState,
    //     ) => {
    //         return {
    //             ...audioDetailsFormModalState,
    //             validFrom: value._d
    //         }
    //     });
    // }

    const onHandleChangePublisher = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                publisherId: value
            }
        });
    }

    const onHandleChangeBookPublisher = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                bookPublisherId: value,
            }
        });
    }

    const onHandleChangeNarrator = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                narratorIds: value,
            }
        });
    }

    const onHandleChangeTranslator = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                translatorIds: value,
            }
        });
    }

    const onHandleChangeCurrency = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                currencyId: value,
            }
        });
    }

    const onHandleChangeTaxCategory = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                taxCategoryId: value,
            }
        });
    }

    const onHandleChangeLanguage = (value: any) => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                languageId: value,
            }
        });
    }

    const showModal = () => {
        onHandleEditItem("");
    };

    const handleCancel = () => {
        OnHandleCancel();
    };

    const handleSave = () => {
        setAudioDetailsFormModalState((audioDetailsFormModalState) => {
            return {
                ...audioDetailsFormModalState,
                isCreateOrUpdateTriggered: true,
            }
        });
        tabeKeyValueChange("3");
        OnHandleCancel();
    };

    // function onChange(time, timeString) {
    //     console.log(time, timeString);
    //   
    return (
        <>
            <div style={{ paddingLeft: '10px' }}>
                <Tooltip title="Add new Audiot">
                    <Button type="primary"
                        onClick={showModal}
                    >
                        Add New
                    </Button>
                </Tooltip>
                <Modal
                    title={audioDetailsEditForm.id ? "Edit Audio" : "Audio Details"}
                    visible={visible}
                    onOk={handleSave}
                    okText="Save"
                    onCancel={handleCancel}
                >
                    <Form>
                        <Form.Item
                            key="1"
                            label="Title">
                            {
                                getFieldDecorator('Title', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.title,
                                    rules: [{ required: true, message: 'Please input  Title!' }],
                                })(
                                    <Input placeholder="Title"
                                        name="title"
                                        onChange={onInputChange}
                                    />
                                )
                            }
                        </Form.Item>
                        <Form.Item key="2" label="Language">
                            {
                                getFieldDecorator('Language', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.language.caption,
                                    rules: [{
                                        required: true,
                                        message: 'Please Select  Language!'
                                    }],
                                })(
                                    <Select
                                        placeholder="Select"
                                        style={{}}
                                        showSearch={true}
                                        onChange={onHandleChangeLanguage}
                                        optionFilterProp="children">
                                        {
                                            languages &&
                                            onCreateSelectItems(languages, "value", "caption", "language")
                                        }
                                    </Select>
                                )
                            }
                        </Form.Item>
                        <Form.Item key="3" label="publisher">
                            {
                                getFieldDecorator('Publisher', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.publisher.caption,
                                    rules: [{
                                        required: true,
                                        message: 'Please Select  publisher!'
                                    }],
                                })(
                                    <Select
                                        placeholder="Select"
                                        style={{}}
                                        showSearch={true}
                                        onChange={onHandleChangePublisher}
                                        optionFilterProp="children">
                                        {
                                            publishers &&
                                            onCreateSelectItems(publishers, "value", "caption", "publisher")
                                        }
                                    </Select>
                                )
                            }
                        </Form.Item>
                        <Form.Item key="4" label="Narrator">
                            {
                                getFieldDecorator('Narrator', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.narrators &&
                                        audioDetailsEditForm.narrators.map(c => c.value),
                                    rules: [{
                                        required: true,
                                        message: 'Please Select  Narrator!'
                                    }],
                                })(
                                    <Select
                                        placeholder="Select"
                                        mode="multiple"
                                        style={{}}
                                        showSearch={true}
                                        onChange={onHandleChangeNarrator}
                                        optionFilterProp="children">
                                        {
                                            narrators &&
                                            onCreateSelectItems(narrators, "value", "caption", "narrators")
                                        }
                                    </Select>
                                )
                            }
                        </Form.Item>
                        <Form.Item key="5" label="Book Publisher">
                            {
                                getFieldDecorator('Book Publisher', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.bookPublisher.caption,
                                    rules: [{
                                        required: true,
                                        message: 'Please Select  Book Publisher!'
                                    }],
                                })(
                                    <Select
                                        placeholder="Select"
                                        style={{}}
                                        showSearch={true}
                                        onChange={onHandleChangeBookPublisher}
                                        optionFilterProp="children">
                                        {
                                            publishers &&
                                            onCreateSelectItems(publishers, "value", "caption", "publisher")
                                        }
                                    </Select>
                                )
                            }
                        </Form.Item>
                        <Form.Item key="6" label="Translator">
                            {
                                getFieldDecorator('Translator', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.translators &&
                                        audioDetailsEditForm.translators.map(c => c.value),
                                    rules: [{
                                        required: false,
                                        message: 'Please Select Transilator!'
                                    }],
                                })(
                                    <Select
                                        placeholder="Select"
                                        mode="multiple"
                                        style={{}}
                                        onChange={onHandleChangeTranslator}
                                        showSearch={true}
                                        optionFilterProp="children">
                                        {
                                            translators &&
                                            onCreateSelectItems(translators, "value", "caption", "translator")
                                        }
                                    </Select>
                                )
                            }
                        </Form.Item>
                        <Form.Item key="7" label="Short Description">
                            {
                                getFieldDecorator('Short Description', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.shortDescription,
                                    rules: [{
                                        required: true,
                                        message: 'Please enter short description!'
                                    }],
                                })(
                                    <TextArea
                                        rows={2} style={{ resize: "none" }}
                                        placeholder=" Short Description"
                                        name="shortDescription"
                                        onChange={onInputChange}

                                    />
                                )
                            }
                        </Form.Item>
                        <Form.Item key="8" label="Full Description">
                            {
                                getFieldDecorator('Full Description', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.fullDescription,
                                    rules: [{
                                        required: true,
                                        message: 'Please enter  full description!'
                                    }],
                                })(
                                    <TextArea
                                        rows={3} style={{ resize: "none" }}
                                        placeholder="Full Description"
                                        name="fullDescription"
                                        onChange={onInputChange}

                                    />
                                )
                            }
                        </Form.Item>
                        <Form.Item key="9" label="Admin Comment">
                            {
                                getFieldDecorator('Admin Comment', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.adminComment,
                                    rules: [{
                                        required: false,
                                        message: 'Please enter admin comment!'
                                    }],
                                })(
                                    <TextArea
                                        rows={3} style={{ resize: "none" }}
                                        placeholder="Full Description"
                                        name="adminComment"
                                        onChange={onInputChange}

                                    />
                                )
                            }
                        </Form.Item>
                        <Row gutter={16}>
                            <Col span={6}>
                                <Form.Item key="10" label="Price">
                                    {
                                        getFieldDecorator('price', {
                                            initialValue: audioDetailsEditForm && audioDetailsEditForm.price,
                                            rules: [{
                                                required: true,
                                                message: 'Please enter price!'
                                            }],
                                        })(
                                            <InputNumber
                                                // defaultValue={0}
                                                // onChange={onInputChange}
                                                name="price"
                                                type="number"
                                            />
                                        )
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={4}>
                                <Form.Item key="11" label="Currency">
                                    {
                                        getFieldDecorator('Currency', {
                                            initialValue: audioDetailsEditForm && audioDetailsEditForm.currency
                                                && audioDetailsEditForm.currency.caption,
                                            rules: [{
                                                required: true,
                                                message: 'Please Select Currency!'
                                            }],
                                        })(
                                            <Select
                                                placeholder="Select"
                                                style={{}}
                                                onChange={onHandleChangeCurrency}
                                                showSearch={true}
                                                optionFilterProp="children">
                                                {
                                                    currencies &&
                                                    onCreateSelectItems(currencies, "value", "caption", "currency")
                                                }
                                            </Select>
                                        )
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={4}></Col>
                            <Col span={6}>
                                <Form.Item key="12" style={{ paddingTop: 36 }}>
                                    {getFieldDecorator('isFree', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.isFree,
                                    })(<Checkbox
                                        name="isFree"
                                        onChange={onInputChange}
                                    >Free</Checkbox>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item key="13" label="Release Date">
                            {
                                getFieldDecorator('release date', {
                                    initialValue: moment(audioDetailsEditForm && audioDetailsEditForm.releaseDate),
                                    rules: [{
                                        required: true,
                                        message: 'Please enter release date !'
                                    }],
                                })(
                                    <DatePicker />
                                )
                            }
                        </Form.Item>
                        <Row>
                            <Col span={18}>
                                <Form.Item key="14" label="Tax ">
                                    {
                                        getFieldDecorator('tax', {
                                            initialValue: audioDetailsEditForm && audioDetailsEditForm.taxCategory
                                                && audioDetailsEditForm.taxCategory.caption,
                                            rules: [{
                                                required: true,
                                                message: 'Please select tax!'
                                            }],
                                        })(
                                            <Select
                                                placeholder="Select"
                                                style={{}}
                                                showSearch={true}
                                                onChange={onHandleChangeTaxCategory}
                                                optionFilterProp="children">
                                                {
                                                    taxCategories &&
                                                    onCreateSelectItems(taxCategories, "value", "caption", "taxCategory")
                                                }
                                            </Select>)
                                    }
                                </Form.Item>
                            </Col>
                            <Col span={6}>
                                <Form.Item key="35" style={{ paddingTop: 36, paddingLeft: 100 }} >
                                    {getFieldDecorator('incTax', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.incTax,
                                    })(<Checkbox
                                        name="incTax"
                                        onChange={onInputChange}
                                    >IncTax</Checkbox>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={8}>
                                <Form.Item key="15" >
                                    {getFieldDecorator('isFeaturedProduct', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.isFeaturedProduct,
                                    })(<Checkbox>IsFeaturedProduct</Checkbox>)}
                                </Form.Item>
                            </Col>
                            <Col span={8} offset={8}>
                                <Form.Item key="16" >
                                    {getFieldDecorator('showOnHomePage', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.showOnHomePage,
                                    })(<Checkbox
                                        name="showOnHomePage"
                                        onChange={onInputChange}
                                    >ShowOnHomePage</Checkbox>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={10}>
                                <Form.Item key="17" >
                                    {getFieldDecorator(' allowCustomerReviews', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.allowCustomerReviews,
                                    })(<Checkbox
                                        name="allowCustomerReviews"
                                        onChange={onInputChange}
                                    >AllowCustomerReviews</Checkbox>)}
                                </Form.Item>
                            </Col>
                            <Col span={6} offset={6}>
                                <Form.Item key="18" >
                                    {getFieldDecorator(' published', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.published,
                                    })(<Checkbox
                                        name="published"
                                        onChange={onInputChange}
                                    > Published</Checkbox>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={10}>
                                <Form.Item key="19" >
                                    {getFieldDecorator('availableForPreOrder', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.availableForPreOrder,
                                    })(<Checkbox
                                        name="availableForPreOrder"
                                        onChange={onInputChange}
                                    > availableForPreOrder</Checkbox>)}
                                </Form.Item>
                            </Col>
                            <Col span={6} offset={6}>
                                <Form.Item key="20" >
                                    {getFieldDecorator(' callForPrice', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.callForPrice,
                                    })(<Checkbox
                                        name="callForPrice"
                                        onChange={onInputChange}
                                    >CallForPrice</Checkbox>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item
                            key="21"
                            label="SKU">
                            {
                                getFieldDecorator('sku', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.sku,
                                    rules: [{ required: false, message: 'Please input  SKU!' }],
                                })(
                                    <Input placeholder="SKU"
                                        name="sku"
                                        onChange={onInputChange}
                                    />
                                )
                            }
                        </Form.Item>
                        <Form.Item key="22" label="MetaKeywords">
                            {
                                getFieldDecorator('  metaKeywords', {
                                    initialValue: audioDetailsEditForm && audioDetailsEditForm.metaKeywords,
                                    rules: [{
                                        required: false,
                                        message: 'Please enter metaKeywords!'
                                    }],
                                })(
                                    <TextArea
                                        rows={2} style={{ resize: "none" }}
                                        placeholder="MetaKeywords"
                                        name="metaKeywords"
                                    />
                                )
                            }
                        </Form.Item>
                        <Row>
                            <Col span={10}>
                                <Form.Item key="23" label="Available Start Date" >
                                    {getFieldDecorator('availableStartDate', {
                                        initialValue: moment(audioDetailsEditForm && audioDetailsEditForm.availableStartDateTimeUtc),
                                        rules: [{
                                            required: false,
                                            message: 'Please select AvailableStartDate !'
                                        }],
                                    })(<DatePicker
                                        // disabledDate={disabledStartDate}
                                        showTime
                                        format="YYYY-MM-DD HH:mm:ss"
                                        // value={startValue}
                                        placeholder="Start"
                                    // onChange={onStartChange}
                                    // onOpenChange={handleStartOpenChange}
                                    />)}
                                </Form.Item>
                            </Col>
                            <Col span={6} offset={4}>
                                <Form.Item key="24" label="Available End Date" >
                                    {getFieldDecorator('AvailableEndDate ', {
                                        initialValue: moment(audioDetailsEditForm && audioDetailsEditForm.availableEndDateTimeUtc),
                                        rules: [{
                                            required: false,
                                            message: 'Please select Available End Date !'
                                        }],
                                    })(<DatePicker
                                        // disabledDate={disabledStartDate}
                                        showTime
                                        format="YYYY-MM-DD HH:mm:ss"
                                        // value={startValue}
                                        placeholder="Start"
                                    // onChange={onStartChange}
                                    // onOpenChange={handleStartOpenChange}
                                    />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={8}>
                                <Form.Item key="25" label="PreOrder Availability Start Date" >
                                    {getFieldDecorator('PreOrderAvailabilityStartDate ', {
                                        initialValue: moment(audioDetailsEditForm && audioDetailsEditForm.preOrderAvailabilityStartDateTimeUtc),
                                        rules: [{
                                            required: false,
                                            message: 'Please select PreOrderAvailabilityStartDate !'
                                        }],
                                    })(<DatePicker
                                        // disabledDate={disabledStartDate}
                                        showTime
                                        format="YYYY-MM-DD HH:mm:ss"
                                        // value={startValue}
                                        placeholder="Start"
                                    // onChange={onStartChange}
                                    // onOpenChange={handleStartOpenChange}
                                    />)}
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item key="26" style={{ paddingLeft: 220 }} >
                                    {getFieldDecorator('  active', {
                                        valuePropName: 'checked',
                                        initialValue: audioDetailsEditForm && audioDetailsEditForm.active,
                                    })(<Checkbox
                                        name="active"
                                        onChange={onInputChange}
                                    > Active</Checkbox>)}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                    <AudioFilesList
                        audioFiles={audioFiles}
                        audioId={audioId}
                        onHandleAudioFilePush={onHandleAudioFilePush}
                    />
                </Modal>
            </div>
        </>
    )
}

export const AudioDetailsFormModalComponent = Form.create<IAudioDetailsFormModalProps>()(AudioDetailsFormModal);
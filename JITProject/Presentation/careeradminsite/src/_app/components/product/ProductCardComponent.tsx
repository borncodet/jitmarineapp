import React from 'react';
import { IProductCardComponentProps } from "./ProductCardComponent.type";
import { Col, Card, Tooltip, Button } from 'antd';
import { withRouter } from 'react-router-dom';

const ProductCardComponent: React.FC<IProductCardComponentProps> = React.memo((props) => {
    const { product, match } = props
    const onEditButtonClick = () => {
        props.history.push(`${match.path}/edit/${product.id}`);
    }

    return (
        <Col className="gutter-row"
            // key={index} 
            style={{ width: 200, marginBottom: 10, padding: 10 }} span={6}>
            <div className="gutter-box" >
                <Card
                    cover={
                        <img
                            alt="example"
                            src={product.bookImages && product.bookImages[0].filename}
                        //src="https://c8.alamy.com/comp/R8CJR9/dh-english-thesaurus-book-books-uk-front-cover-oxford-dictionaries-books-hardback-R8CJR9.jpg"
                        />
                    }
                    actions={[
                        <Tooltip title="View User Profile">
                            <Button shape="circle"
                                icon="eye"
                                size="small"
                            // onClick={}
                            />
                        </Tooltip>,
                        <Tooltip title="Edit">
                            <Button shape="circle"
                                icon="edit"
                                size="small"
                                onClick={onEditButtonClick}
                            />
                        </Tooltip>,
                        <Tooltip title="Delete">
                            <Button shape="circle"
                                icon="delete"
                                size="small"
                            // onClick={}
                            /></Tooltip>
                    ]}>
                    <div className="card-body" >
                        <h5 className="card-title">{product.productName}</h5>
                    </div>
                </Card>
            </div>
        </Col>
    );
})

export default withRouter(ProductCardComponent);

import React from 'react';
import { Table } from 'antd';
import {AudioListItemOperation,IProductImageListState,IProductImageListProps} from "."
const initialState = {
    
} as IProductImageListState

const ProductImageList: React.FC<IProductImageListProps> = (props) => {

    const [
        productImageListState,
        setProductImageListState
    ] = React.useState<IProductImageListState>(initialState);
    const {  } = productImageListState

    const handleDeleteItem = async (id: string) => {
        console.log(" handleDeleteItem " + id);
    }

    const handleEditItem = async (id: string) => {
        console.log(" handleEditItem " + id);
        setProductImageListState((productImageListState) => {
            return {
               ...productImageListState 
            }
        });

    }

    const OnHandleCancel = async () => {
        setProductImageListState((productImageListState) => {
            return {
                ...productImageListState,
               
            }
        });
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Image Title',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '15%',
            render: (text: any, record:any, index: number) => {
                return <AudioListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    return (
        <>
            <div style={{ padding: '10px', marginTop: '25px' }}>

                {
                    // audios && audios.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            // dataSource={audioFiles}
                            columns={getcolumnHeaders()}
                        />
                    )
                    // : null
                }

            </div>
        </>
    )
}

export default ProductImageList;
import { FormComponentProps } from "antd/lib/form";
import { NarratorEditModel } from "_app/api";

export interface INarratorEditFormProps extends FormComponentProps {
    narratorEditForm: NarratorEditModel;
    onFieldValueChange: (name: string, value: string | boolean) => void;
}

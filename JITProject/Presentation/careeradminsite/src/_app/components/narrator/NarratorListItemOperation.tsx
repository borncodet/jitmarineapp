import React from 'react';
import { Tooltip, Button, Popconfirm } from 'antd';
import { INarratorListItemOperationProps } from '_app/containers/narrator';


const NarratorListItemOperation: React.FunctionComponent<INarratorListItemOperationProps> = (props) => {

    const { id, handleEditItem, handleDeleteItem } = props;

    const onViewButtonClick = () => {
        console.log("onViewButtonClick");
    }

    const onEditButtonClick = () => {
        handleEditItem(id);
    }

    const onDeleteButtonClick = () => {
        handleDeleteItem(id);
    }

    return <div>
        <Tooltip title="View User Profile">
            <Button
                shape="circle"
                style={{ margin: 12 }}
                icon="eye"
                size="small"
                onClick={onViewButtonClick}
            />
        </Tooltip>
        <Tooltip title="Edit">
            <Button
                shape="circle"
                style={{ margin: 12 }}
                icon="edit"
                size="small"
                onClick={onEditButtonClick}
            />
        </Tooltip>
        <Popconfirm
            title="Are you sure you want to delete?"
            onConfirm={onDeleteButtonClick}>
            <Tooltip title="Delete">
                <Button
                    shape="circle"
                    style={{ margin: 12 }}
                    icon="delete"
                    size="small"
                />
            </Tooltip>
        </Popconfirm>
    </div>
}

export default NarratorListItemOperation;
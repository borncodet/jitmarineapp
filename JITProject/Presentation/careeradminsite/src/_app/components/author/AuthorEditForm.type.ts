import { FormComponentProps } from "antd/lib/form";
import { AuthorEditModel } from "_app/api";

export interface IAuthorEditFormProps extends FormComponentProps {
    authorEditForm: AuthorEditModel;
    onFieldValueChange: (name: string, value: string | boolean) => void;
}

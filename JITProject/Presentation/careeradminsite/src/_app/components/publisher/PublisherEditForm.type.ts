import { FormComponentProps } from "antd/lib/form";
import { PublisherEditModel } from "_app/api";

export interface IPublisherEditFormProps extends FormComponentProps {
    publisherEditForm: PublisherEditModel;
    onFieldValueChange: (name: string, value: string | boolean) => void;
}

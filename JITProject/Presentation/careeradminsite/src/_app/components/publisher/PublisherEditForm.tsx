import * as React from 'react';
import { Form, Input, Checkbox } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { IPublisherEditFormProps } from '.';

const PublisherForm: React.FC<IPublisherEditFormProps> = (props: IPublisherEditFormProps) => {

    const {  onFieldValueChange,  publisherEditForm } = props;
    const { getFieldDecorator } = props.form;

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        onFieldValueChange(name, value);
    }

    return <>
        <Form
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 12 }} >
            <Form.Item key="1" label="Title">
                {
                    getFieldDecorator('Title', {
                        initialValue: publisherEditForm.title,
                        rules: [{ required: true, message: 'Please input title!' }],
                    })(<Input placeholder="Title"
                        name="title"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="2" label="Description">
                {
                    getFieldDecorator('Description', {
                        initialValue: publisherEditForm.description,
                        rules: [{ required: false, message: 'Please input  description!' }],
                    })(<TextArea rows={4} style={{ resize: "none" }}
                        placeholder="Description"
                        name="description"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="3" label="Active">
                {
                    getFieldDecorator('Active', {
                        initialValue: publisherEditForm.active,
                        rules: [{ required: false, message: '' }],
                    })(
                        <Checkbox
                            checked={publisherEditForm.active}
                            name="active"
                            onChange={onInputChange}
                        />
                    )
                }
            </Form.Item>
        </Form>
    </>
}

export const PublisherEditForm = Form.create<IPublisherEditFormProps>()(PublisherForm);

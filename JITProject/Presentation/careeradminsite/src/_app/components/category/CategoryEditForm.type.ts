import { FormComponentProps } from "antd/lib/form";
import { CategoryEditModel } from "_app/api";

export interface ICategoryEditFormProps extends FormComponentProps {
    categoryEditForm: CategoryEditModel;
    onFieldValueChange: (name: string, value: string | boolean) => void;
}

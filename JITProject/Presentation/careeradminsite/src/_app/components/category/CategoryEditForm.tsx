import * as React from 'react';
import { Form, Input, Checkbox } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { ICategoryEditFormProps } from '.';

const CategoryForm: React.FC<ICategoryEditFormProps> = (props: ICategoryEditFormProps) => {

    const { onFieldValueChange, categoryEditForm } = props;
    const { getFieldDecorator } = props.form;

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        onFieldValueChange(name, value);
    }

    return <>
        <Form
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 12 }} >
            <Form.Item key="1" label="Title">
                {
                    getFieldDecorator('Title', {
                        initialValue: categoryEditForm.title,
                        rules: [{ required: true, message: 'Please input title!' }],
                    })(<Input placeholder="Title"
                        name="title"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="2" label="Description">
                {
                    getFieldDecorator('Description', {
                        initialValue: categoryEditForm.description,
                        rules: [{ required: false, message: 'Please input  description!' }],
                    })(<TextArea rows={4} style={{ resize: "none" }}
                        placeholder="Description"
                        name="description"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="3" label="Active">
                {
                    getFieldDecorator('Active', {
                        initialValue: categoryEditForm.active,
                        rules: [{ required: false, message: '' }],
                    })(
                        <Checkbox
                            checked={categoryEditForm.active}
                            name="active"
                            onChange={onInputChange}
                        />
                    )
                }
            </Form.Item>
        </Form>
    </>
}

export const CategoryEditForm = Form.create<ICategoryEditFormProps>()(CategoryForm);

import * as React from 'react';
import { Form, Input, Checkbox } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { ILanguageEditFormProps } from '.';

const LanguageForm: React.FC<ILanguageEditFormProps> = (props: ILanguageEditFormProps) => {

    const { onFieldValueChange, languageEditForm } = props;
    const { getFieldDecorator } = props.form;

    const onInputChange = (event: any) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        onFieldValueChange(name, value);
    }

    return <>
        <Form
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 12 }} >
            <Form.Item key="1" label="Title">
                {
                    getFieldDecorator('Title', {
                        initialValue: languageEditForm.title,
                        rules: [{ required: true, message: 'Please input title!' }],
                    })(<Input placeholder="Title"
                        name="title"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="2" label="Language Code">
                {
                    getFieldDecorator('Code', {
                        initialValue: languageEditForm.code,
                        rules: [{ required: true, message: 'Please input title!' }],
                    })(<Input placeholder="Language Code"
                        name="code"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="3" label="Description">
                {
                    getFieldDecorator('Description', {
                        initialValue: languageEditForm.description,
                        rules: [{ required: false, message: 'Please input  description!' }],
                    })(<TextArea rows={4} style={{ resize: "none" }}
                        placeholder="Description"
                        name="description"
                        onChange={onInputChange}
                    />)
                }
            </Form.Item>
            <Form.Item key="4" label="Active">
                {
                    getFieldDecorator('Active', {
                        initialValue: languageEditForm.active,
                        rules: [{ required: false, message: '' }],
                    })(
                        <Checkbox
                            checked={languageEditForm.active}
                            name="active"
                            onChange={onInputChange}
                        />
                    )
                }
            </Form.Item>
        </Form>
    </>
}

export const LanguageEditForm = Form.create<ILanguageEditFormProps>()(LanguageForm);

import { FormComponentProps } from "antd/lib/form";
import { LanguageEditModel } from "_app/api";

export interface ILanguageEditFormProps extends FormComponentProps {
    languageEditForm: LanguageEditModel;
    onFieldValueChange: (name: string, value: string | boolean) => void;
}

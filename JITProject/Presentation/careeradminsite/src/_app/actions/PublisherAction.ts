import * as React from "react";
import { Reducer } from "use-immer";
import { IErrorState } from "../shared";
import { PublisherDataResultModel, PublisherClient, PublisherDataRequestModel } from "_app/api";
import { IPublisherManagementState } from "..";
import { PublisherDispatcherContext, PublisherStateContext } from "_app/contexts/publisherContext";


/**
 * The definition for an action accepted by the narratorReducer function.
 */
export type IPublisherAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
}
    | {
        type: "SET_PUBLISHER",
        publishers: PublisherDataResultModel | null;
    } | {
        type: "ERROR",
        exception: IErrorState;
    }

export const publisherReducer: Reducer<IPublisherManagementState, IPublisherAction> = (draft, action): IPublisherManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        case "SET_PUBLISHER":
            draft.isBusy = false;
            draft.publishers = action.publishers;
            return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const publisherApi = new PublisherClient();

/**
 * Fetch details of current Narrator
 * @param dispatcher of type useNarratorDispatcher
 * @param PublisherId - Id of current Narrator
 */
export const fetchActivePublishers = async (dispatcher: React.Dispatch<IPublisherAction>, query: PublisherDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const publishers = await publisherApi.getAllActive(query);
        dispatcher({ type: "SET_PUBLISHER", publishers: publishers});
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Narrator dispatcher from NarratorDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a NarratorDispatcherContext.Provider.
 */
export const usePublisherDispatcher = (): React.Dispatch<IPublisherAction> => {
    const publisherDispatcher = React.useContext(PublisherDispatcherContext);
    if (!publisherDispatcher) {
        throw new Error("You have to provide the Narrator dispatcher using the PublisherDispatcherContext.Provider in a parent component.");
    }
    return publisherDispatcher;
}


export const usePublisherContext = (): IPublisherManagementState => {
    const publisherContext = React.useContext(PublisherStateContext);
    if (!publisherContext) throw new Error("You have to provide the narrator context using the PublisherStateContext.Provider in a parent component.");
    return publisherContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setIsBusy = async (dispatcher: React.Dispatch<IPublisherAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updateEditScreenVisibility = async (dispatcher: React.Dispatch<IPublisherAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
import * as React from "react";
import { Reducer } from "use-immer";
import { ILanguageManagementState } from "..";
import { LanguageInfo, LanguageClient, LanguageDataRequestModel } from "../api";
import { IErrorState } from "../shared";
import { LanguageDispatcherContext, LanguageStateContext } from "../contexts";

/**
 * The definition for an action accepted by the languageReducer function.
 */
export type ILanguageAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
}
    | {
        type: "SET_LANGUAGES",
        languages: LanguageInfo[] | null;
    } | {
        type: "ERROR",
        exception: IErrorState;
    }

export const languageReducer: Reducer<ILanguageManagementState, ILanguageAction> = (draft, action): ILanguageManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        case "SET_LANGUAGES":
            draft.isBusy = false;
            draft.languages = action.languages;
            return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const languageApi = new LanguageClient();

/**
 * Fetch details of current Language
 * @param dispatcher of type useLanguageDispatcher
 * @param LanguageId - Id of current Language
 */
export const fetchActiveLanguages = async (dispatcher: React.Dispatch<ILanguageAction>, query: LanguageDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const languages = await languageApi.getAllActive(query);
        dispatcher({ type: "SET_LANGUAGES", languages: languages });
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Language dispatcher from LanguageDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a LanguageDispatcherContext.Provider.
 */
export const useLanguageDispatcher = (): React.Dispatch<ILanguageAction> => {
    const languageDispatcher = React.useContext(LanguageDispatcherContext);
    if (!languageDispatcher) {
        throw new Error("You have to provide the Language dispatcher using the LanguageDispatcherContext.Provider in a parent component.");
    }
    return languageDispatcher;
}


export const useLanguageContext = (): ILanguageManagementState => {
    const languageContext = React.useContext(LanguageStateContext);
    if (!languageContext) throw new Error("You have to provide the language context using the LanguageStateContext.Provider in a parent component.");
    return languageContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setLanguageIsBusy = async (dispatcher: React.Dispatch<ILanguageAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updateLanguageEditScreenVisibility = async (dispatcher: React.Dispatch<ILanguageAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
import * as React from "react";
import { Reducer } from "use-immer";
import { IAuthorManagementState } from "..";
import { AuthorClient, AuthorDataRequestModel, AuthorDataResultModel } from "../api";
import { IErrorState } from "../shared";
import { AuthorDispatcherContext, AuthorStateContext } from "../contexts";

/**
 * The definition for an action accepted by the authorReducer function.
 */
export type IAuthorAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
} | {
    type: "SET_AUTHORS",
    authors: AuthorDataResultModel | null;
} | {
    type: "ERROR",
    exception: IErrorState;
}

export const authorReducer: Reducer<IAuthorManagementState, IAuthorAction> = (draft, action): IAuthorManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        case "SET_AUTHORS":
            draft.isBusy = false;
            draft.authors = action.authors;
            return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const authorApi = new AuthorClient();

/**
 * Fetch details of current Author
 * @param dispatcher of type useAuthorDispatcher
 * @param AuthorId - Id of current Author
 */
export const fetchActiveAuthors = async (dispatcher: React.Dispatch<IAuthorAction>, query: AuthorDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const authors = await authorApi.getAllActive(query);
        dispatcher({ type: "SET_AUTHORS", authors: authors });
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Author dispatcher from AuthorDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a AuthorDispatcherContext.Provider.
 */
export const useAuthorDispatcher = (): React.Dispatch<IAuthorAction> => {
    const authorDispatcher = React.useContext(AuthorDispatcherContext);
    if (!authorDispatcher) {
        throw new Error("You have to provide the Author dispatcher using theAuthorDispatcherContext.Provider in a parent component.");
    }
    return authorDispatcher;
}


export const useAuthorContext = (): IAuthorManagementState => {
    const authorContext = React.useContext(AuthorStateContext);
    if (!authorContext) throw new Error("You have to provide the author context using the AuthorStateContext.Provider in a parent component.");
    return authorContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setAuthorIsBusy = async (dispatcher: React.Dispatch<IAuthorAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updateAuthorEditScreenVisibility = async (dispatcher: React.Dispatch<IAuthorAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
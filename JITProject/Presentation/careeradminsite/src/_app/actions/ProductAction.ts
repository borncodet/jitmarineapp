import * as React from "react";
import { Reducer } from "use-immer";
import { IProductManagementState } from "..";
import { ProductClient, ProductDataResultModel, ProductDataRequestModel } from "../api";
import { IErrorState } from "../shared";
import { ProductDispatcherContext, ProductStateContext } from "../contexts";

/**
 * The definition for an action accepted by the productReducer function.
 */
export type IProductAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
} | {
    type: "SET_PRODUCT",
    products: ProductDataResultModel | null;
} | {
    type: "ERROR",
    exception: IErrorState;
}

export const productReducer: Reducer<IProductManagementState, IProductAction> = (draft, action): IProductManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        case "SET_PRODUCT":
            draft.isBusy = false;
             draft.products = action.products;
            return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const productApi = new ProductClient();
/**
 * Fetch details of current Product
 * @param dispatcher of type useProductDispatcher
 * @param ProductId - Id of current Product
 */
export const fetchActiveProducts = async (dispatcher: React.Dispatch<IProductAction>, query: ProductDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const products = await productApi.getAllActive(query);
        dispatcher({ type: "SET_PRODUCT", products: products });
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Product dispatcher from ProductDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a ProductDispatcherContext.Provider.
 */
export const useProductDispatcher = (): React.Dispatch<IProductAction> => {
    const productDispatcher = React.useContext(ProductDispatcherContext);
    if (!productDispatcher) {
        throw new Error("You have to provide the Product dispatcher using theProductDispatcherContext.Provider in a parent component.");
    }
    return productDispatcher;
}


export const useProductContext = (): IProductManagementState => {
    const productContext = React.useContext(ProductStateContext);
    if (!productContext) throw new Error("You have to provide the product context using the ProductStateContext.Provider in a parent component.");
    return productContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setProductIsBusy = async (dispatcher: React.Dispatch<IProductAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updateProductEditScreenVisibility = async (dispatcher: React.Dispatch<IProductAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
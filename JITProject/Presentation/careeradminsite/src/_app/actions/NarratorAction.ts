import * as React from "react";
import { Reducer } from "use-immer";
import { INarratorManagementState } from "..";
import { NarratorClient, NarratorDataRequestModel, NarratorDataResultModel } from "../api";
import { IErrorState } from "../shared";
import { NarratorDispatcherContext, NarratorStateContext } from "../contexts";

/**
 * The definition for an action accepted by the narratorReducer function.
 */
export type INarratorAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
} | {
    type: "SET_NARRATORS",
    narrators: NarratorDataResultModel | null;
} | {
    type: "ERROR",
    exception: IErrorState;
}

export const narratorReducer: Reducer<INarratorManagementState, INarratorAction> = (draft, action): INarratorManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        case "SET_NARRATORS":
            draft.isBusy = false;
            draft.narrators = action.narrators;
            return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const narratorApi = new NarratorClient();

/**
 * Fetch details of current Narrator
 * @param dispatcher of type useNarratorDispatcher
 * @param NarratorId - Id of current Narrator
 */
export const fetchActiveNarrators = async (dispatcher: React.Dispatch<INarratorAction>, query: NarratorDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const narrators = await narratorApi.getAllActive(query);
        dispatcher({ type: "SET_NARRATORS", narrators: narrators });
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Narrator dispatcher from NarratorDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a NarratorDispatcherContext.Provider.
 */
export const useNarratorDispatcher = (): React.Dispatch<INarratorAction> => {
    const narratorDispatcher = React.useContext(NarratorDispatcherContext);
    if (!narratorDispatcher) {
        throw new Error("You have to provide the Narrator dispatcher using theNarratorDispatcherContext.Provider in a parent component.");
    }
    return narratorDispatcher;
}


export const useNarratorContext = (): INarratorManagementState => {
    const narratorContext = React.useContext(NarratorStateContext);
    if (!narratorContext) throw new Error("You have to provide the narrator context using the NarratorStateContext.Provider in a parent component.");
    return narratorContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setNarratorIsBusy = (dispatcher: React.Dispatch<INarratorAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updateNarratorEditScreenVisibility = (dispatcher: React.Dispatch<INarratorAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
import * as React from "react";
import { Reducer } from "use-immer";
import { IPromotionManagementState } from "..";
import { PromotionClient, PromotionDataRequestModel, PromotionDataResultModel } from "../api";
import { IErrorState } from "../shared";
import { PromotionDispatcherContext, PromotionStateContext } from "../contexts";

/**
 * The definition for an action accepted by the promotionReducer function.
 */
export type IPromotionAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
} | {
    type: "SET_AUTHORS",
    promotions: PromotionDataResultModel | null;
} | {
    type: "ERROR",
    exception: IErrorState;
}

export const promotionReducer: Reducer<IPromotionManagementState, IPromotionAction> = (draft, action): IPromotionManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        // case "SET_AUTHORS":
        //     draft.isBusy = false;
        //     draft.promotions = action.promotions;
        //     return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const promotionApi = new PromotionClient();

/**
 * Fetch details of current Promotion
 * @param dispatcher of type usePromotionDispatcher
 * @param PromotionId - Id of current Promotion
 */
export const fetchActivePromotions = async (dispatcher: React.Dispatch<IPromotionAction>, query: PromotionDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const promotions = await promotionApi.getAllActive(query);
        dispatcher({ type: "SET_AUTHORS", promotions: promotions });
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Promotion dispatcher from PromotionDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a PromotionDispatcherContext.Provider.
 */
export const usePromotionDispatcher = (): React.Dispatch<IPromotionAction> => {
    const promotionDispatcher = React.useContext(PromotionDispatcherContext);
    if (!promotionDispatcher) {
        throw new Error("You have to provide the Promotion dispatcher using thePromotionDispatcherContext.Provider in a parent component.");
    }
    return promotionDispatcher;
}


export const usePromotionContext = (): IPromotionManagementState => {
    const promotionContext = React.useContext(PromotionStateContext);
    if (!promotionContext) throw new Error("You have to provide the promotion context using the PromotionStateContext.Provider in a parent component.");
    return promotionContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setPromotionIsBusy = async (dispatcher: React.Dispatch<IPromotionAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updatePromotionEditScreenVisibility = async (dispatcher: React.Dispatch<IPromotionAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
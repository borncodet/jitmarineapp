import * as React from "react";
import { Reducer } from "use-immer";
import { ICategoryManagementState } from "..";
import { CategoryClient, CategoryDataRequestModel, CategoryDataResultModel } from "../api";
import { IErrorState } from "../shared";
import { CategoryDispatcherContext, CategoryStateContext } from "../contexts";

/**
 * The definition for an action accepted by the categoryReducer function.
 */
export type ICategoryAction = {
    type: "SET_IS_BUSY"
} | {
    type: "UPDATE_EDIT_SCREEN_VISIBILITY",
    visible: boolean;
} | {
    type: "SET_CATEGORYS",
    categorys: CategoryDataResultModel | null;
} | {
    type: "ERROR",
    exception: IErrorState;
}

export const categoryReducer: Reducer<ICategoryManagementState, ICategoryAction> = (draft, action): ICategoryManagementState => {
    switch (action.type) {
        case "SET_IS_BUSY":
            draft.isBusy = true;
            return draft;
        case "UPDATE_EDIT_SCREEN_VISIBILITY":
            draft.isBusy = false;
            draft.isEditScreenVisible = action.visible;
            return draft;
        case "SET_CATEGORYS":
            draft.isBusy = false;
            draft.categorys = action.categorys;
            return draft;
        case "ERROR":
            draft.isBusy = false;
            draft.statusCode = action.exception.statusCode;
            draft.errorMessage = action.exception.errorMessage;
            return draft;
        default:
            throw Error("unknonw action");
    }
}

/**
 * Local constant to hold the api client.
 */
const categoryApi = new CategoryClient();

/**
 * Fetch details of current Category
 * @param dispatcher of type useCategoryDispatcher
 * @param CategoryId - Id of current Category
 */
export const fetchActiveCategorys = async (dispatcher: React.Dispatch<ICategoryAction>, query: CategoryDataRequestModel) => {
    dispatcher({ type: "SET_IS_BUSY" });
    try {
        const categorys = await categoryApi.getAllActive(query);
        dispatcher({ type: "SET_CATEGORYS", categorys: categorys });
    }
    catch (e) {
        dispatcher({ type: "ERROR", exception: { errorMessage: e.message, statusCode: 500 } });
    }
}

/**
 * A hook to provide the Category dispatcher from CategoryDispatcherContext.
 * This function throws an error if the dispatcher was not provided using a CategoryDispatcherContext.Provider.
 */
export const useCategoryDispatcher = (): React.Dispatch<ICategoryAction> => {
    const categoryDispatcher = React.useContext(CategoryDispatcherContext);
    if (!categoryDispatcher) {
        throw new Error("You have to provide the Category dispatcher using theCategoryDispatcherContext.Provider in a parent component.");
    }
    return categoryDispatcher;
}


export const useCategoryContext = (): ICategoryManagementState => {
    const categoryContext = React.useContext(CategoryStateContext);
    if (!categoryContext) throw new Error("You have to provide the category context using the CategoryStateContext.Provider in a parent component.");
    return categoryContext;
}

/**
 * toggles the isBusy flag state.
 */
export const setCategoryIsBusy = async (dispatcher: React.Dispatch<ICategoryAction>) => {
    dispatcher({ type: "SET_IS_BUSY" });
}

/**
 * toggles the isBusy flag state.
 */
export const updateCategoryEditScreenVisibility = async (dispatcher: React.Dispatch<ICategoryAction>, isVisible: boolean) => {
    dispatcher({ type: "UPDATE_EDIT_SCREEN_VISIBILITY", visible: isVisible });
}
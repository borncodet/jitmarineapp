import { IErrorState } from "../../shared";

export interface ICategoryListProps {
}

export interface ICategoryListState extends IErrorState {
    isDelete: boolean
}

export interface ICategoryListItemOperationProps {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}
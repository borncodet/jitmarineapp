import React from 'react';
import {
    ICategoryListProps,
    CategoryEditFormContainer
} from '.';
import { Table } from 'antd';
import {
    useCategoryDispatcher,
    useCategoryContext,
    fetchActiveCategorys,
    updateCategoryEditScreenVisibility
} from '../../actions/CategoryAction';
import {
    CategoryDataRequestModel,
    CategoryInfo
} from '_app/api';
import { CategoryListItemOperation } from '_app/components/category';

const CategoryList: React.FC<ICategoryListProps> = React.memo((props) => {

    const categoryDispatcher = useCategoryDispatcher();
    const categoryContext = useCategoryContext();
    const { categorys, isEditScreenVisible } = categoryContext;
    const currentSelectedCategoryId = React.useRef("");

    const handleDeleteItem = async (id: string) => {
        await fetchActiveCategorys(categoryDispatcher, {} as CategoryDataRequestModel);
    }

    const handleEditItem = async (id: string) => {
        currentSelectedCategoryId.current = id;
        await updateCategoryEditScreenVisibility(categoryDispatcher, true);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '18%',
            render: (text: any, record: CategoryInfo, index: number) => {
                return <CategoryListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    const onCategoryEditFormContainerClose = React.useCallback(() => {
        currentSelectedCategoryId.current = "";
    }, [])

    return (
        <div style={{ padding: '10px', marginTop: '15px' }}>
            {
                categorys && categorys.data && categorys.data.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            dataSource={categorys.data}
                            columns={getcolumnHeaders()}
                        />
                    ) : null
            }
            {
                isEditScreenVisible ?
                    (
                        <CategoryEditFormContainer
                            onClose={onCategoryEditFormContainerClose}
                            id={currentSelectedCategoryId.current} />
                    ) : null
            }
        </div>
    )
})

export default CategoryList;
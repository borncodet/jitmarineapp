import * as React from 'react';
import { Drawer, Button } from 'antd';
import {
    useCategoryDispatcher,
    useCategoryContext,
    updateCategoryEditScreenVisibility
} from '_app/actions/CategoryAction';
import { CategoryEditModel, CategoryClient, CategoryPostmodel, BaseViewModel } from '_app/api';
import { ICategoryEditFormContainerProps, ICategoryEditFormContainerState } from '.';
import { CategoryEditForm } from '_app/components/category';

const initialState = {
    categoryEditForm: new CategoryEditModel(),
    isCreateOrUpdateTriggered: false,
    isBusy: false
} as ICategoryEditFormContainerState

const CategoryEditFormContainer: React.FC<ICategoryEditFormContainerProps> = React.memo((props: ICategoryEditFormContainerProps) => {

    const [categoryEditFormState, setCategoryEditFormState] = React.useState<ICategoryEditFormContainerState>(initialState);
    const { isCreateOrUpdateTriggered, categoryEditForm, isBusy } = categoryEditFormState

    const categoryDispatcher = useCategoryDispatcher();
    const categoryContext = useCategoryContext();
    const { isEditScreenVisible } = categoryContext;
    const { id, onClose } = props;
    const editScreenVisibility = React.useRef(true);

    React.useEffect(() => {
        return () => {
            if (onClose) {
                onClose();
            }
        }
    }, [onClose])

    React.useEffect(() => {
        (async () => {
            if (id) {
                const categoryApi = new CategoryClient();
                setCategoryEditFormState((categoryEditFormState) => {
                    return {
                        ...categoryEditFormState,
                        isBusy: true
                    }
                });
                try {
                    const result = await categoryApi.get({ id } as BaseViewModel);
                    setCategoryEditFormState((categoryEditFormState) => {
                        return {
                            ...categoryEditFormState,
                            isBusy: false,
                            categoryEditForm: result.data as CategoryEditModel
                        }
                    });
                } catch (error) {
                    setCategoryEditFormState((categoryEditFormState) => {
                        return {
                            ...categoryEditFormState,
                            isBusy: false
                        }
                    });
                }
            }
        })();
    }, [id])

    const onInputChange = (name: string, value: string | boolean) => {
        const categoryFormUpdates = {
            [name]: value,
        };
        setCategoryEditFormState({
            ...categoryEditFormState,
            categoryEditForm: Object.assign(categoryEditForm, categoryFormUpdates) as CategoryEditModel
        });
    }

    const onDrawerClose = async () => {
        await updateCategoryEditScreenVisibility(categoryDispatcher, false);
    }

    React.useEffect(() => {
        if (isCreateOrUpdateTriggered) {
            (async () => {
                const categoryApi = new CategoryClient();
                try {
                    const { active, id, title, description } = categoryEditForm;
                    const postModel = {
                        active,
                        id,
                        description,
                        title
                    } as CategoryPostmodel;
                    const result = await categoryApi.createOrEdit(postModel);
                    result.isSuccess && editScreenVisibility.current && await updateCategoryEditScreenVisibility(categoryDispatcher, false);
                    if (result.isSuccess) {
                        //alert(result.message);
                    }
                    else {
                        //alert(result.message);
                    }
                    setCategoryEditFormState((categoryEditFormState) => {
                        return {
                            ...categoryEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                } catch (error) {
                    console.error(error);
                    setCategoryEditFormState((categoryEditFormState) => {
                        return {
                            ...categoryEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered, categoryEditForm, categoryDispatcher])

    const onFormSubmit = async (closeEditScreen: boolean) => {
        setCategoryEditFormState({
            ...categoryEditFormState,
            isCreateOrUpdateTriggered: true,
            isBusy: true
        });
        editScreenVisibility.current = closeEditScreen;
        console.log(editScreenVisibility, 10)
    }

    const onSaveButonClick = () => {
        onFormSubmit(true);
    }

    const onSaveAndContinueButonClick = () => {
        onFormSubmit(false);
    }

    return <div>
        <Drawer
            title={id ? 'Edit Category' : 'Add Category'}
            width={650}
            onClose={onDrawerClose}
            visible={isEditScreenVisible}>
            <CategoryEditForm
                categoryEditForm={categoryEditForm}
                onFieldValueChange={onInputChange}
            />
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Button
                    htmlType="button"
                    style={{ marginRight: 12 }}
                    onClick={onSaveButonClick}
                    type="primary">
                    Save
            </Button>
                <Button
                    onClick={onSaveAndContinueButonClick}
                    htmlType="button"
                    style={{ marginRight: 12 }}>
                    Save And Continue
            </Button>
                <Button onClick={onDrawerClose}>
                    Cancel
            </Button>
            </div>
            {isBusy ? <>Loading</> : null}
        </Drawer>
    </div>
})

export default CategoryEditFormContainer;

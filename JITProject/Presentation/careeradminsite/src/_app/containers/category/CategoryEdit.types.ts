import { CategoryEditModel } from "_app/api";
import { IErrorState } from "_app/shared";

export interface ICategoryEditFormContainerProps {
    id?: string;
    onClose?: () => void;
}

export interface ICategoryEditFormContainerState extends IErrorState {
    categoryEditForm: CategoryEditModel,
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean
}
import React from 'react';
import {
    INarratorListProps,
    NarratorEditFormContainer
} from '.';
import { Table } from 'antd';
import {
    useNarratorDispatcher,
    useNarratorContext,
    fetchActiveNarrators,
    updateNarratorEditScreenVisibility
} from '../../actions/NarratorAction';
import {
    NarratorDataRequestModel,
    NarratorInfo
} from '_app/api';
import { NarratorListItemOperation } from '_app/components/narrator';

const NarratorList: React.FC<INarratorListProps> = React.memo((props) => {

    const narratorDispatcher = useNarratorDispatcher();
    const narratorContext = useNarratorContext();
    const { narrators, isEditScreenVisible } = narratorContext;
    const currentSelectedNarratorId = React.useRef("");

    const handleDeleteItem = async (id: string) => {
        await fetchActiveNarrators(narratorDispatcher, {} as NarratorDataRequestModel);
    }

    const handleEditItem = async (id: string) => {
        currentSelectedNarratorId.current = id;
        await updateNarratorEditScreenVisibility(narratorDispatcher, true);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '18%',
            render: (text: any, record: NarratorInfo, index: number) => {
                return <NarratorListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    const onNarratorEditFormContainerClose = React.useCallback(() => {
        currentSelectedNarratorId.current = "";
    }, [])

    return (
        <div style={{ padding: '10px', marginTop: '15px' }}>
            {
                narrators && narrators.data && narrators.data.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            dataSource={narrators.data}
                            columns={getcolumnHeaders()}
                        />
                    ) : null
            }
            {
                isEditScreenVisible ?
                    (
                        <NarratorEditFormContainer
                            onClose={onNarratorEditFormContainerClose}
                            id={currentSelectedNarratorId.current} />
                    ) : null
            }
        </div>
    )
})

export default NarratorList;
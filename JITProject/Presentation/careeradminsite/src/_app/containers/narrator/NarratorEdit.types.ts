import { NarratorEditModel } from "_app/api";
import { IErrorState } from "_app/shared";

export interface INarratorEditFormContainerProps {
    id?: string;
    onClose?: () => void;
}

export interface INarratorEditFormContainerState extends IErrorState {
    narratorEditForm: NarratorEditModel,
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean
}
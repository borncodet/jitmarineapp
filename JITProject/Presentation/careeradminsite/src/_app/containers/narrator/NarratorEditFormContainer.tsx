import * as React from 'react';
import { Drawer, Button } from 'antd';
import {
    useNarratorDispatcher,
    useNarratorContext,
    updateNarratorEditScreenVisibility
} from '_app/actions/NarratorAction';
import { NarratorEditModel, NarratorClient, NarratorPostmodel, BaseViewModel } from '_app/api';
import { INarratorEditFormContainerProps, INarratorEditFormContainerState } from '.';
import { NarratorEditForm } from '_app/components/narrator';

const initialState = {
    narratorEditForm: new NarratorEditModel(),
    isCreateOrUpdateTriggered: false,
    isBusy: false
} as INarratorEditFormContainerState

const NarratorEditFormContainer: React.FC<INarratorEditFormContainerProps> = React.memo((props: INarratorEditFormContainerProps) => {

    const [narratorEditFormState, setNarratorEditFormState] = React.useState<INarratorEditFormContainerState>(initialState);
    const { isCreateOrUpdateTriggered, narratorEditForm, isBusy } = narratorEditFormState

    const narratorDispatcher = useNarratorDispatcher();
    const narratorContext = useNarratorContext();
    const { isEditScreenVisible } = narratorContext;
    const { id, onClose } = props;
    const editScreenVisibility = React.useRef(true);

    React.useEffect(() => {
        return () => {
            if (onClose) {
                onClose();
            }
        }
    }, [onClose])

    React.useEffect(() => {
        (async () => {
            if (id) {
                const narratorApi = new NarratorClient();
                setNarratorEditFormState((narratorEditFormState) => {
                    return {
                        ...narratorEditFormState,
                        isBusy: true
                    }
                });
                try {
                    const result = await narratorApi.get({ id } as BaseViewModel);
                    setNarratorEditFormState((narratorEditFormState) => {
                        return {
                            ...narratorEditFormState,
                            isBusy: false,
                            narratorEditForm: result.data as NarratorEditModel
                        }
                    });
                } catch (error) {
                    setNarratorEditFormState((narratorEditFormState) => {
                        return {
                            ...narratorEditFormState,
                            isBusy: false
                        }
                    });
                }
            }
        })();
    }, [id])

    const onInputChange = (name: string, value: string | boolean) => {
        const narratorFormUpdates = {
            [name]: value,
        };
        setNarratorEditFormState({
            ...narratorEditFormState,
            narratorEditForm: Object.assign(narratorEditForm, narratorFormUpdates) as NarratorEditModel
        });
    }

    const onDrawerClose = () => {
        updateNarratorEditScreenVisibility(narratorDispatcher, false);
    }

    React.useEffect(() => {
        if (isCreateOrUpdateTriggered) {
            (async () => {
                const narratorApi = new NarratorClient();
                try {
                    const { active, id, title, description } = narratorEditForm;
                    const postModel = {
                        active,
                        id,
                        description,
                        title
                    } as NarratorPostmodel;
                    const result = await narratorApi.createOrEdit(postModel);
                    result.isSuccess && editScreenVisibility.current && await updateNarratorEditScreenVisibility(narratorDispatcher, false);
                    if (result.isSuccess) {
                        //alert(result.message);
                    }
                    else {
                        //alert(result.message);
                    }
                    setNarratorEditFormState((narratorEditFormState) => {
                        return {
                            ...narratorEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                } catch (error) {
                    console.error(error);
                    setNarratorEditFormState((narratorEditFormState) => {
                        return {
                            ...narratorEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered, narratorEditForm, narratorDispatcher])

    const onFormSubmit = async (closeEditScreen: boolean) => {
        setNarratorEditFormState({
            ...narratorEditFormState,
            isCreateOrUpdateTriggered: true,
            isBusy: true
        });
        editScreenVisibility.current = closeEditScreen;
        console.log(editScreenVisibility, 10)
    }

    const onSaveButonClick = () => {
        onFormSubmit(true);
    }

    const onSaveAndContinueButonClick = () => {
        onFormSubmit(false);
    }

    return <div>
        <Drawer
            title={id ? 'Edit Narrator' : 'Add Narrator'}
            width={650}
            onClose={onDrawerClose}
            visible={isEditScreenVisible}>
            <NarratorEditForm
                narratorEditForm={narratorEditForm}
                onFieldValueChange={onInputChange}
            />
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Button
                    htmlType="button"
                    style={{ marginRight: 12 }}
                    onClick={onSaveButonClick}
                    type="primary">
                    Save
            </Button>
                <Button
                    onClick={onSaveAndContinueButonClick}
                    htmlType="button"
                    style={{ marginRight: 12 }}>
                    Save And Continue
            </Button>
                <Button onClick={onDrawerClose}>
                    Cancel
            </Button>
            </div>
            {isBusy ? <>Loading</> : null}
        </Drawer>
    </div>
})

export default NarratorEditFormContainer;

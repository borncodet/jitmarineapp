import { IErrorState } from "../../shared";

export interface INarratorListProps {
}

export interface INarratorListState extends IErrorState {
    isDelete:boolean
}

export interface INarratorListItemOperationProps {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}
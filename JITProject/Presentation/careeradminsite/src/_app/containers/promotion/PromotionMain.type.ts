import { PromotionInfo, PromotionDetailsInfo, ProductInfo, PromotionPostModel, PromotionItemInfo, PromotionDefaultValueViewModel } from "_app/api";
import { RouteComponentProps } from "react-router";

export interface IPromotionMainParams {
    id?: string;
}

export interface IPromotionMainProps extends RouteComponentProps<IPromotionMainParams> { 

}

export interface IPromotionMainState  {
    isBusy: boolean;
    promotions:  PromotionInfo[] | undefined;
    searchTearm:string;
    isSave:boolean;
    promotionEditForm:PromotionDetailsInfo;
    product:ProductInfo[]| undefined;
    rollValue:string;
    master:PromotionPostModel;
    items:PromotionItemInfo[] | undefined;
    itemList:PromotionItemInfo[] | undefined;
    availableTo :string[]| undefined
    promotionDefaultValue:  PromotionDefaultValueViewModel
}
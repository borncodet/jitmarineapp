import React from 'react';
import { PromotionMasterComponent, PromotionItemsComponent, PromotionAddItemToGridComponent } from '_app/components/promotion';
import { IPromotionMainProps, IPromotionMainState } from '.'
import { PromotionClient, BaseViewModelWithIdRequired, PromotionDetailsInfo, PromotionDetailPostmodel, ProductClient, ProductDataRequestModel, PromotionPostModel, PromotionDefaultValueViewModel } from '_app/api';

import { Button } from 'antd';

const initialState = {
    promotions: [],
    isBusy: false,
    searchTearm: "",
    isSave: false,
    promotionEditForm: new PromotionDetailsInfo(),
    product: [],
    rollValue: "",
    master: new PromotionPostModel(),
    items: [],
    itemList: [],
    availableTo: [],
    promotionDefaultValue: new PromotionDefaultValueViewModel()
} as IPromotionMainState

const PromotionMain: React.FC<IPromotionMainProps> = (props: IPromotionMainProps) => {

    const { id } = props.match.params;
    const [promotionMainState,
        setPromotionMainState] = React.useState<IPromotionMainState>(initialState);
    const {  promotionEditForm, product, rollValue, master, items,
        promotionDefaultValue, availableTo, isSave } = promotionMainState

    React.useEffect(() => {
        (async () => {
            // if (id) {
            const promotionApi = new PromotionClient();
            setPromotionMainState((promotionMainState) => {
                return {
                    ...promotionMainState,
                    isBusy: true
                }
            });
            try {
                const result = await promotionApi.get({ id } as BaseViewModelWithIdRequired);

                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: false,
                        promotionEditForm: result as PromotionDetailsInfo
                    }
                });

            } catch (error) {
                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: false
                    }
                });
                //}
            }
        })();
    }, [id])

    React.useEffect(() => {
        (async () => {
            // if (id) {
            const promotionApi = new PromotionClient();
            setPromotionMainState((promotionMainState) => {
                return {
                    ...promotionMainState,
                    isBusy: true
                }
            });
            try {
                const result = await promotionApi.getDefaultValues();
                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: false,
                        promotionDefaultValue: result as PromotionDefaultValueViewModel
                    }
                });

            } catch (error) {
                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: false
                    }
                });
                //}
            }
        })();
    }, [id])


    React.useEffect(() => {
        (async () => {
            // if (id) {
            const productApi = new ProductClient();
            setPromotionMainState((promotionMainState) => {
                return {
                    ...promotionMainState,
                    isBusy: true
                }
            });
            try {
                const result = await productApi.getAllActive({ searchTearm: "" } as unknown as ProductDataRequestModel);

                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: false,
                        product: result.data
                    }
                });

            } catch (error) {
                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: false
                    }
                });
                //}
            }
        })();
    }, [id])

    React.useEffect(() => {
        (async () => {
            if (isSave === true) {
                const promotionApi = new PromotionClient();
                setPromotionMainState((promotionMainState) => {
                    return {
                        ...promotionMainState,
                        isBusy: true
                    }
                });
                try {
                    const result = await promotionApi.createOrEdit({ master, items, availableTo } as PromotionDetailPostmodel);
                    // setPromotionMainState((promotionMainState) => {
                    //     return {
                    //         ...promotionMainState,
                    //         isBusy: false,
                    //        promotionDefaultValue: result as PromotionDefaultValueViewModel
                    //     }
                    // });

                } catch (error) {
                    setPromotionMainState((promotionMainState) => {
                        return {
                            ...promotionMainState,
                            isBusy: false,
                            isSave: false,
                        }
                    });
                }
            }
        })();
    }, [isSave])

    //     React.useEffect(() => {
    //         (async () => {
    //             // items: items && items.concat(item)
    //             promotionEditForm.items.concat(items)
    //             setPromotionMainState((promotionMainState) => {
    //                 return {
    //                     ...promotionMainState,
    //                     // itemList:promotionEditForm.items
    //                 }
    //             });
    //             })();
    // }, [])

    React.useEffect(() => {
        (async () => {
            // items: items && items.concat(item)

            setPromotionMainState((promotionMainState) => {
                return {
                    ...promotionMainState,
                    itemList: promotionEditForm.items
                }
            });
        })();
    }, [])

    const onHandleRollChange = (value: any) => {
        setPromotionMainState((promotionMainState) => {
            return {
                ...promotionMainState,
                rollValue: value,
            }
        });
    }

    const onHandleMasterPush = (master: any) => {
        setPromotionMainState((promotionMainState) => {
            return {
                ...promotionMainState,
                master: master
            }
        });
    }

    const onHandleItemPush = (item: any) => {
        setPromotionMainState((promotionMainState) => {
            return {
                ...promotionMainState,
                // items:[...items,item]
                items: items && items.concat(item)
                // promotionEditForm && promotionEditForm.items.concat(items)
            }
        });
    }

    const onHandleRollPush = (roll: any) => {
        setPromotionMainState((promotionMainState) => {
            return {
                ...promotionMainState,
                // availableTo: availableTo && availableTo.concat(roll)
                availableTo: roll
            }
        });
    }

    const handleSave = () => {
        setPromotionMainState((promotionMainState) => {
            return {
                ...promotionMainState,
                isSave: true,
            }
        });
    }
    return (
        <div style={{ padding: 10 }}>
            <PromotionMasterComponent
                promotion={promotionEditForm.promotion}
                onHandleRollChange={onHandleRollChange}
                onHandleMasterPush={onHandleMasterPush}
                userTypes={promotionDefaultValue.userTypes}
                onHandleRollPush={onHandleRollPush}
            />
            <PromotionAddItemToGridComponent

                products={product}
                rollValue={rollValue}
                onHandleItemPush={onHandleItemPush}
            />
            <PromotionItemsComponent
                items={items}
            />
            <Button type="primary" style={{ marginLeft: 470, marginTop: 10 }}
                onClick={handleSave}
            >
                Save
            </Button>
        </div>
    )
}
export default PromotionMain;
export {default as PromotionMain} from './PromotionMain';
export * from './PromotionMain.type';
export * from './PromotionList.type';
export {default as PromotionList} from './PromotionList';

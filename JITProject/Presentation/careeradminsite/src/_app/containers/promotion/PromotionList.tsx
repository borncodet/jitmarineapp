import React from 'react';
import { IPromotionListState, IPromotionListProps,} from '.';
import { PromotionsListComponent } from '_app/components';
import { PromotionClient, PromotionDataRequestModel } from '_app/api/controller';

const initialState = {
    promotions:[],
    isBusy:false,
    searchTearm:""
} as IPromotionListState

const PromotionList: React.FC<IPromotionListProps> = (props: IPromotionListProps) => {
    const {  match } = props
    const [promotionListState,
        setPromotionListState] = React.useState<IPromotionListState>(initialState);
        const{promotions}=promotionListState

        React.useEffect(() => {
            (async () => {
                const promotionApi = new PromotionClient();
                setPromotionListState((promotionListState) => {
                    return {
                        ...promotionListState,
                        isBusy: true
                    }
                });
                try {
                    const result = await promotionApi.getAllActive({searchTerm:''} as PromotionDataRequestModel);
                    
                    setPromotionListState((promotionListState) => {
                        return {
                            ...promotionListState,
                            isBusy: false,
                            promotions: result.data
                        }
                    });
    
                } catch (error) {
                    setPromotionListState((promotionListState) => {
                        return {
                            ...promotionListState,
                            isBusy: false
                        }
                    });
                }
    
            })();
        }, [])

        const onEditButtonClick = (id:any) => {
            props.history.push(`${match.path}/edit/${id}`);
        }
        
    return (
    <div style={{padding:10}}>
           <PromotionsListComponent
           promotions={promotions}
           onEditButtonClick={onEditButtonClick}
           />
    </div>     
    )
}
export default PromotionList;
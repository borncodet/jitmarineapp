
import { PromotionInfo } from "_app/api";
import { RouteComponentProps } from "react-router";

export interface IPromotionListProps  extends RouteComponentProps<any> {

}

export interface IPromotionListState  {
    isBusy: boolean;
    promotions:  PromotionInfo[] | undefined;
    searchTearm:string;
}
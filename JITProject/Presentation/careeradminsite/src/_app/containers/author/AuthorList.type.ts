import { IErrorState } from "../../shared";

export interface IAuthorListProps {
}

export interface IAuthorListState extends IErrorState {
    isDelete: boolean
}

export interface IAuthorListItemOperationProps {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}
import { AuthorEditModel } from "_app/api";
import { IErrorState } from "_app/shared";

export interface IAuthorEditFormContainerProps {
    id?: string;
    onClose?: () => void;
}

export interface IAuthorEditFormContainerState extends IErrorState {
    authorEditForm: AuthorEditModel,
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean
}
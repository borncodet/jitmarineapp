import * as React from 'react';
import { Drawer, Button } from 'antd';
import {
    useAuthorDispatcher,
    useAuthorContext,
    updateAuthorEditScreenVisibility
} from '_app/actions/AuthorAction';
import { AuthorEditModel, AuthorClient, AuthorPostmodel, BaseViewModel } from '_app/api';
import { IAuthorEditFormContainerProps, IAuthorEditFormContainerState } from '.';
import { AuthorEditForm } from '_app/components/author';

const initialState = {
    authorEditForm: new AuthorEditModel(),
    isCreateOrUpdateTriggered: false,
    isBusy: false
} as IAuthorEditFormContainerState

const AuthorEditFormContainer: React.FC<IAuthorEditFormContainerProps> = React.memo((props: IAuthorEditFormContainerProps) => {

    const [authorEditFormState, setAuthorEditFormState] = React.useState<IAuthorEditFormContainerState>(initialState);
    const { isCreateOrUpdateTriggered, authorEditForm, isBusy } = authorEditFormState

    const authorDispatcher = useAuthorDispatcher();
    const authorContext = useAuthorContext();
    const { isEditScreenVisible } = authorContext;
    const { id, onClose } = props;
    const editScreenVisibility = React.useRef(true);

    React.useEffect(() => {
        return () => {
            if (onClose) {
                onClose();
            }
        }
    }, [onClose])

    React.useEffect(() => {
        (async () => {
            if (id) {
                const authorApi = new AuthorClient();
                setAuthorEditFormState((authorEditFormState) => {
                    return {
                        ...authorEditFormState,
                        isBusy: true
                    }
                });
                try {
                    const result = await authorApi.get({ id } as BaseViewModel);
                    setAuthorEditFormState((authorEditFormState) => {
                        return {
                            ...authorEditFormState,
                            isBusy: false,
                            authorEditForm: result.data as AuthorEditModel
                        }
                    });
                } catch (error) {
                    setAuthorEditFormState((authorEditFormState) => {
                        return {
                            ...authorEditFormState,
                            isBusy: false
                        }
                    });
                }
            }
        })();
    }, [id])

    const onInputChange = (name: string, value: string | boolean) => {
        const authorFormUpdates = {
            [name]: value,
        };
        setAuthorEditFormState({
            ...authorEditFormState,
            authorEditForm: Object.assign(authorEditForm, authorFormUpdates) as AuthorEditModel
        });
    }

    const onDrawerClose = async () => {
        await updateAuthorEditScreenVisibility(authorDispatcher, false);
    }

    React.useEffect(() => {
        if (isCreateOrUpdateTriggered) {
            (async () => {
                const authorApi = new AuthorClient();
                try {
                    const { active, id, title, description } = authorEditForm;
                    const postModel = {
                        active,
                        id,
                        description,
                        title
                    } as AuthorPostmodel;
                    const result = await authorApi.createOrEdit(postModel);
                    result.isSuccess && editScreenVisibility.current && await updateAuthorEditScreenVisibility(authorDispatcher, false);
                    if (result.isSuccess) {
                        //alert(result.message);
                    }
                    else {
                        //alert(result.message);
                    }
                    setAuthorEditFormState((authorEditFormState) => {
                        return {
                            ...authorEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                } catch (error) {
                    console.error(error);
                    setAuthorEditFormState((authorEditFormState) => {
                        return {
                            ...authorEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered, authorEditForm, authorDispatcher])

    const onFormSubmit = async (closeEditScreen: boolean) => {
        setAuthorEditFormState({
            ...authorEditFormState,
            isCreateOrUpdateTriggered: true,
            isBusy: true
        });
        editScreenVisibility.current = closeEditScreen;
        console.log(editScreenVisibility, 10)
    }

    const onSaveButonClick = () => {
        onFormSubmit(true);
    }

    const onSaveAndContinueButonClick = () => {
        onFormSubmit(false);
    }

    return <div>
        <Drawer
            title={id ? 'Edit Author' : 'Add Author'}
            width={650}
            onClose={onDrawerClose}
            visible={isEditScreenVisible}>
            <AuthorEditForm
                authorEditForm={authorEditForm}
                onFieldValueChange={onInputChange}
            />
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Button
                    htmlType="button"
                    style={{ marginRight: 12 }}
                    onClick={onSaveButonClick}
                    type="primary">
                    Save
            </Button>
                <Button
                    onClick={onSaveAndContinueButonClick}
                    htmlType="button"
                    style={{ marginRight: 12 }}>
                    Save And Continue
            </Button>
                <Button onClick={onDrawerClose}>
                    Cancel
            </Button>
            </div>
            {isBusy ? <>Loading</> : null}
        </Drawer>
    </div>
})

export default AuthorEditFormContainer;

import React from 'react';
import {
    IAuthorListProps,
    AuthorEditFormContainer
} from '.';
import { Table } from 'antd';
import {
    useAuthorDispatcher,
    useAuthorContext,
    fetchActiveAuthors,
    updateAuthorEditScreenVisibility
} from '../../actions/AuthorAction';
import {
    AuthorDataRequestModel,
    AuthorInfo
} from '_app/api';
import { AuthorListItemOperation } from '_app/components/author';

const AuthorList: React.FC<IAuthorListProps> = React.memo((props) => {

    const authorDispatcher = useAuthorDispatcher();
    const authorContext = useAuthorContext();
    const { authors, isEditScreenVisible } = authorContext;
    const currentSelectedAuthorId = React.useRef("");

    const handleDeleteItem = async (id: string) => {
        await fetchActiveAuthors(authorDispatcher, {} as AuthorDataRequestModel);
    }

    const handleEditItem = async (id: string) => {
        currentSelectedAuthorId.current = id;
        await updateAuthorEditScreenVisibility(authorDispatcher, true);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }
    const getcolumnHeaders = () => {
        return [{
            title: 'Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '18%',
            render: (text: any, record: AuthorInfo, index: number) => {
                return <AuthorListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    const onAuthorEditFormContainerClose = React.useCallback(() => {
        currentSelectedAuthorId.current = "";
    }, [])

    return (
        <div style={{ padding: '10px', marginTop: '15px' }}>
            {
                authors && authors.data && authors.data.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            dataSource={authors.data}
                            columns={getcolumnHeaders()}
                        />
                    ) : null
            }
            {
                isEditScreenVisible ?
                    (
                        <AuthorEditFormContainer
                            onClose={onAuthorEditFormContainerClose}
                            id={currentSelectedAuthorId.current} />
                    ) : null
            }
        </div>
    )
})

export default AuthorList;
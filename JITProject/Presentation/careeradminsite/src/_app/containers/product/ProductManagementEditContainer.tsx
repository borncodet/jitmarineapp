import React from 'react';
import { IProductManagementEditContainerProps, ProductEditFormContainer } from '.';

const ProductManagementEditContainer: React.FC<IProductManagementEditContainerProps> = (props) => {
 
    return <>
      <ProductEditFormContainer/>
    </>;
  
}

export default ProductManagementEditContainer;

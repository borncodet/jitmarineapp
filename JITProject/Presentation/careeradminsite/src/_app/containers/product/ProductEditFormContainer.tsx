import React from 'react';
import { IProductEditFormComponentProps, IProductEditFormContainerState } from '.';
import { Tabs, } from 'antd';
import { ProductEditModel, ProductClient, BaseViewModel, ProductSelectBoxDataViewModel, } from '_app/api';
import { BasicDetailsFormComponent, AudioImageDetailsForm, AudioDetailsList } from '_app/components';
import { withRouter } from 'react-router';

const initialState = {
    productEditForm: new ProductEditModel(),
    productSelectBoxData: new ProductSelectBoxDataViewModel(),
    isBusy: true,
    tabKey: "1",
    audios: [],
} as IProductEditFormContainerState

const ProductEditFormContainer: React.FC<IProductEditFormComponentProps> = (props) => {
    const [productEditFormContainerState,
        setProductEditFormContainerState] = React.useState<IProductEditFormContainerState>(initialState);
    const { id } = props.match.params;
    const { productEditForm, productSelectBoxData, tabKey, isBusy,audios } = productEditFormContainerState

    React.useEffect(() => {
        (async () => {
            const productApi = new ProductClient();
            setProductEditFormContainerState((productEditFormContainerState) => {
                return {
                    ...productEditFormContainerState,
                    isBusy: true
                }
            });
            try {
                const result = await productApi.getSelectboxData()
                setProductEditFormContainerState((productEditFormContainerState) => {
                    return {
                        ...productEditFormContainerState,
                        isBusy: false,
                        productSelectBoxData: result as ProductSelectBoxDataViewModel
                    }
                });
            } catch (error) {
                setProductEditFormContainerState((productEditFormContainerState) => {
                    return {
                        ...productEditFormContainerState,
                        isBusy: false
                    }
                });
            }
        })();
    }, [])

    React.useEffect(() => {
        (async () => {
            // if (id) {
            const productApi = new ProductClient();
            setProductEditFormContainerState((productEditFormContainerState) => {
                return {
                    ...productEditFormContainerState,
                    isBusy: true
                }
            });
            try {
                const result = await productApi.get({ id } as BaseViewModel);
                setProductEditFormContainerState((productEditFormContainerState) => {
                    return {
                        ...productEditFormContainerState,
                        isBusy: false,
                        productEditForm: result.data as ProductEditModel
                    }
                });

            } catch (error) {
                setProductEditFormContainerState((productEditFormContainerState) => {
                    return {
                        ...productEditFormContainerState,
                        isBusy: false
                    }
                });
                //}
            }
        })();
    }, [id])

    const getAudios = (audioData: any) => {
        setProductEditFormContainerState((productEditFormContainerState) => {
            return {
                ...productEditFormContainerState,
                audios: audioData,
            }

        });
    }   

    // const onInputChange = (event: any) => {
    // const target = event.target;
    // const value = target.type === 'checkbox' ? target.checked : target.value;
    // const name = target.name;
    // }

    // const getSubCategoryOptions = () => {
    //     if (subCategoryState &&
    //         productEditForm.categories &&
    //         productEditForm.categories.length > 0) {
    //         return onCreateSelectItems(subCategoryState,
    //             "value", "caption", "sub-category");
    //     }
    //     else {
    //         return null
    //     };
    // }

    // const handleSubmit = (e: any) => {
    //     e.preventDefault();
    //     props.form.validateFields((err, values) => {
    //         if (!err) {
    //             console.log('Received values of form: ', values);
    //         }
    //     });
    // };
    const tabeKeyValueChange = (value: any) => {
        setProductEditFormContainerState((productEditFormContainerState) => {
            return {
                ...productEditFormContainerState,
                tabKey: value,
            }
        });
    }

    if (isBusy && !productSelectBoxData) return null;
    return (
        <>

            <Tabs defaultActiveKey="1" activeKey={tabKey}>
                <Tabs.TabPane key="1" tab="Basic Details" >
                    <BasicDetailsFormComponent
                        authors={productSelectBoxData.authors}
                        categories={productSelectBoxData.categories}
                        subCategories={productSelectBoxData.subCategories}
                        productEditForm={productEditForm}
                        tabeKeyValueChange={tabeKeyValueChange}
                    />
                </Tabs.TabPane>
                <Tabs.TabPane key="2" tab="Audio Details" >
                    <AudioDetailsList {...props}
                        languages={productSelectBoxData.languages}
                        publishers={productSelectBoxData.publishers}
                        narrators={productSelectBoxData.narrators}
                        currencies={productSelectBoxData.currencies}
                        taxCategories={productSelectBoxData.taxCategories}
                        translators={productSelectBoxData.translators}
                        tabeKeyValueChange={tabeKeyValueChange}
                        getAudios={getAudios}
                    />
                </Tabs.TabPane>
                <Tabs.TabPane key="3" tab="Image Details" >
                    <AudioImageDetailsForm
                        tabeKeyValueChange={tabeKeyValueChange}
                        audios={audios}
                    />
                </Tabs.TabPane>
            </Tabs>
        </>
    )
}

export default withRouter(ProductEditFormContainer);
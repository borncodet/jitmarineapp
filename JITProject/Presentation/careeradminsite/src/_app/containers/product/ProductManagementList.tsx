import React from 'react';
import { IProductManagementListProps } from '.';
import { useProductContext } from '_app/actions';
import { ProductCardComponent } from '_app/components';
import { withRouter } from 'react-router';

const ProductManagementList: React.FC<IProductManagementListProps> = React.memo((props) => {

    const productContext = useProductContext();
    const { products } = productContext;
    if (!products || !products.data || products.data.length <= 0) {
        return (
            <>
                No data available...!
            </>
        );
    }

    return (
        <>
            {
                products.data.map((product, index) => {
                    return (
                        <>
                            <ProductCardComponent
                                key={`Product_${index}`}
                                product={product}
                            />
                        </>
                    )
                })
            }
        </>
    );
})

export default withRouter(ProductManagementList);

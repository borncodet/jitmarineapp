import { IErrorState } from "_app";
import { RouteComponentProps } from "react-router";

export interface IProductManagementListProps extends RouteComponentProps<any> {
}

export interface IProductManagementListState extends IErrorState {
}

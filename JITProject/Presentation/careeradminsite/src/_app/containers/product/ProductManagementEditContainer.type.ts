import { RouteComponentProps } from "react-router";

export interface IProductManagementEditContainerParams {
    id?: string;
}
export interface IProductManagementEditContainerProps extends RouteComponentProps<IProductManagementEditContainerParams> {
}


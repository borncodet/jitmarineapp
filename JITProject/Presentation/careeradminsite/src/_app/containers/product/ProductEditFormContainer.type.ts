import { ProductEditModel, ProductSelectBoxDataViewModel, AudioInfo } from "_app/api";
import { RouteComponentProps } from "react-router";

export interface IProductEditFormContainerParams {
    id?: string;
}

export interface IProductEditFormComponentProps extends RouteComponentProps<IProductEditFormContainerParams> {
   
}

export interface IProductEditFormContainerState {
    productSelectBoxData: ProductSelectBoxDataViewModel,
    productEditForm: ProductEditModel,
    isBusy: boolean,
    tabKey:string,
    audios:AudioInfo[] | null;
}


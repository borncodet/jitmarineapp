import React from 'react';
import {
    IPublisherListProps,
    PublisherEditFormContainer
} from '.';
import { Table } from 'antd';
import {
    usePublisherDispatcher,
    usePublisherContext,
    fetchActivePublishers,
    updateEditScreenVisibility
} from '../../actions/PublisherAction';
import {
    PublisherInfo,
    PublisherDataRequestModel
} from '_app/api';
import PublisherListItemOperation from '_app/components/publisher/PublisherListItemOperation';

const PublisherList: React.FC<IPublisherListProps> = React.memo((props) => {

    const publisherDispatcher = usePublisherDispatcher();
    const publisherContext = usePublisherContext();
    const { publishers, isEditScreenVisible } = publisherContext;
    const currentSelectedPublisherId = React.useRef("");

    const handleDeleteItem = async (id: string) => {
        await fetchActivePublishers(publisherDispatcher, {} as PublisherDataRequestModel);
    }

    const handleEditItem = async (id: string) => {
        currentSelectedPublisherId.current = id;
        await updateEditScreenVisibility(publisherDispatcher, true);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '18%',
            render: (text: any, record: PublisherInfo, index: number) => {
                return <PublisherListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }



    const onPublisherEditFormContainerClose = React.useCallback(() => {
        currentSelectedPublisherId.current = "";
    }, [])

    return (
        <div style={{ padding: '10px', marginTop: '15px' }}>
            {
                publishers && publishers.data && publishers.data.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            dataSource={publishers.data}
                            columns={getcolumnHeaders()}
                        />
                    ) : null
            }
            {
                isEditScreenVisible ?
                    (
                        <PublisherEditFormContainer
                            onClose={onPublisherEditFormContainerClose}
                            id={currentSelectedPublisherId.current} />
                    ) : null
            }
        </div>
    )
})

export default PublisherList;
import * as React from 'react';
import { Drawer, Button } from 'antd';
import {
    usePublisherDispatcher,
    usePublisherContext,
    updateEditScreenVisibility
} from '_app/actions/PublisherAction';
import { NarratorEditModel, NarratorClient, PublisherEditModel, PublisherClient, PublisherPostmodel, BaseViewModel } from '_app/api';
import { IPublisherEditFormContainerProps, IPublisherEditFormContainerState } from '.';
import { PublisherEditForm } from '_app/components/publisher';



const initialState = {
    publisherEditForm: new PublisherEditModel(),
    isCreateOrUpdateTriggered: false,
    isBusy: false
} as IPublisherEditFormContainerState

const PublisherEditFormContainer: React.FC<IPublisherEditFormContainerProps> = React.memo((props: IPublisherEditFormContainerProps) => {

    const [publisherEditFormState, setPublisherEditFormState] = React.useState<IPublisherEditFormContainerState>(initialState);
    const { isCreateOrUpdateTriggered, publisherEditForm, isBusy } = publisherEditFormState

    const publisherDispatcher = usePublisherDispatcher();
    const publisherContext = usePublisherContext();
    const { isEditScreenVisible } = publisherContext;
    const { id, onClose } = props;
    const editScreenVisibility = React.useRef(true);

    React.useEffect(() => {
        return () => {
            if (onClose) {
                onClose();
            }
        }
    }, [onClose])

    React.useEffect(() => {
        (async () => {
            if (id) {
                const publisherApi = new PublisherClient();
                setPublisherEditFormState((publisherEditFormState) => {
                    return {
                        ...publisherEditFormState,
                        isBusy: true
                    }
                });
                try {
                    const result = await publisherApi.get({ id } as BaseViewModel);
                    setPublisherEditFormState((publisherEditFormState) => {
                        return {
                            ...publisherEditFormState,
                            isBusy: false,
                            publisherEditForm: result.data as PublisherEditModel
                        }
                    });
                } catch (error) {
                    setPublisherEditFormState((publisherEditFormState) => {
                        return {
                            ...publisherEditFormState,
                            isBusy: false
                        }
                    });
                }
            }
        })();
    }, [id])

    const onInputChange = (name: string, value: string | boolean) => {
        const publisherFormUpdates = {
            [name]: value,
        };
        setPublisherEditFormState({
            ...publisherEditFormState,
            publisherEditForm: Object.assign(publisherEditForm, publisherFormUpdates) as NarratorEditModel
        });
    }

    const onDrawerClose = async () => {
        await updateEditScreenVisibility(publisherDispatcher, false);
    }

    React.useEffect(() => {
        if (isCreateOrUpdateTriggered) {
            (async () => {
                const narratorApi = new NarratorClient();
                try {
                    const { active, id, title, description } = publisherEditForm;
                    const postModel = {
                        active,
                        id,
                        description,
                        title
                    } as PublisherPostmodel;
                    const result = await narratorApi.createOrEdit(postModel);
                    result.isSuccess && editScreenVisibility.current && await updateEditScreenVisibility(publisherDispatcher, false);
                    if (result.isSuccess) {
                        //alert(result.message);
                    }
                    else {
                        //alert(result.message);
                    }
                    setPublisherEditFormState((publisherEditFormState) => {
                        return {
                            ...publisherEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                } catch (error) {
                    console.error(error);
                    setPublisherEditFormState((publisherEditFormState) => {
                        return {
                            ...publisherEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered, publisherEditForm, publisherDispatcher])

    const onFormSubmit = async (closeEditScreen: boolean) => {
        setPublisherEditFormState({
            ...publisherEditFormState,
            isCreateOrUpdateTriggered: true,
            isBusy: true
        });
        editScreenVisibility.current = closeEditScreen;
        console.log(editScreenVisibility, 10)
    }

    const onSaveButonClick = () => {
        onFormSubmit(true);
    }

    const onSaveAndContinueButonClick = () => {
        onFormSubmit(false);
    }

    return <div>
        <Drawer
            title={id ? 'Edit Narrator' : 'Add Narrator'}
            width={650}
            onClose={onDrawerClose}
            visible={isEditScreenVisible}>
            <PublisherEditForm
                publisherEditForm={publisherEditForm}
                onFieldValueChange={onInputChange}
            />
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Button
                    htmlType="button"
                    style={{ marginRight: 12 }}
                    onClick={onSaveButonClick}
                    type="primary">
                    Save
            </Button>
                <Button
                    onClick={onSaveAndContinueButonClick}
                    htmlType="button"
                    style={{ marginRight: 12 }}>
                    Save And Continue
            </Button>
                <Button onClick={onDrawerClose}>
                    Cancel
            </Button>
            </div>
            {isBusy ? <>Loading</> : null}
        </Drawer>
    </div>
})

export default PublisherEditFormContainer;

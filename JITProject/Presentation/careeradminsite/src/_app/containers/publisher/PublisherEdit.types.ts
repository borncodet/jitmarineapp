import { PublisherEditModel } from "_app/api";
import { IErrorState } from "_app/shared";

export interface IPublisherEditFormContainerProps {
    id?: string;
    onClose?: () => void;
}

export interface IPublisherEditFormContainerState extends IErrorState {
    publisherEditForm: PublisherEditModel,
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean
}
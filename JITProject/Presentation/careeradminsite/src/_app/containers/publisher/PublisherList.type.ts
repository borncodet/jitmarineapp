import { IErrorState } from "../../shared";

export interface IPublisherListProps {
}

export interface IPublisherListState extends IErrorState {
    isDelete:boolean
}

export interface IPublisherListItemOperationProps {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}
import { LanguageEditModel } from "_app/api";
import { IErrorState } from "_app/shared";

export interface ILanguageEditFormContainerProps {
    id?: string;
    onClose?: () => void;
}

export interface ILanguageEditFormContainerState extends IErrorState {
    languageEditForm: LanguageEditModel,
    isCreateOrUpdateTriggered: boolean,
    isBusy: boolean,
    isButtonDisabled:boolean,
}
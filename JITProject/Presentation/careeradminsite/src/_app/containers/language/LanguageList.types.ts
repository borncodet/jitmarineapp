import { IErrorState } from "../../shared";

export interface ILanguageListProps {
   
}

export interface ILanguageListState extends IErrorState {
    isDelete:boolean,
}

export interface ILanguageListItemOperationProps {
    id: string;
    handleEditItem: (id: string) => void;
    handleDeleteItem: (id: string) => void;
    handleViewItem: (id: string) => void;
}
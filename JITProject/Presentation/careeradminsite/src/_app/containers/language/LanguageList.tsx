import React from 'react';
import { ILanguageListProps, LanguageEditFormContainer } from '.';
import { Table } from 'antd';
import {
    LanguageInfo,
    LanguageDataRequestModel
} from '../../api/controller';
import { LanguageListItemOperation } from '../../components/language';
import {
    useLanguageDispatcher,
    useLanguageContext,
    fetchActiveLanguages,
    updateLanguageEditScreenVisibility
} from '../../actions';

// const initialState = {
//     isDelete: false,
//     statusCode: 0,
//     errorMessage: ""
// } as ILanguageListState

const LanguageList: React.FC<ILanguageListProps> = React.memo((props) => {

    // const [languageListState, setLanguageList] = React.useState<ILanguageListState>(initialState);
    const languageDispatcher = useLanguageDispatcher();
    const languageContext = useLanguageContext();
    const { languages, isEditScreenVisible, } = languageContext;
     const currentSelectedLanguageId = React.useRef("");

     const handleDeleteItem = async (id: string) => {
        await fetchActiveLanguages(languageDispatcher, {} as LanguageDataRequestModel);
    }

    const handleEditItem = async (id: string) => {
        currentSelectedLanguageId.current = id;
        await updateLanguageEditScreenVisibility(languageDispatcher, true);
    }

    const handleViewItem = async (id: string) => {
        console.log("handleViewProfile " + id);
    }

    const getcolumnHeaders = () => {
        return [{
            title: 'Name',
            key: 'title',
            dataIndex: 'title',
            width: '25%',
            editable: true,
        }, {
            title: 'Description',
            key: 'description',
            dataIndex: 'description',
        }, {
            title: 'Language Code',
            dataIndex: 'code',
            width: '15%',
            key: 'code',
        }, {
            title: 'Actions',
            key: 'actions',
            dataIndex: '',
            width: '18%',
            render: (text: any, record: LanguageInfo, index: number) => {
                return <LanguageListItemOperation
                    key={`${index}_${record.id}`}
                    id={record.id ? record.id : ""}
                    handleDeleteItem={handleDeleteItem}
                    handleEditItem={handleEditItem}
                    handleViewItem={handleViewItem}
                />
            }
        }];
    }

    const onLanguageEditFormContainerClose = React.useCallback(() => {
        currentSelectedLanguageId.current = "";
    }, [])
    
    return (
        <div style={{ padding: '10px', marginTop: '15px' }}>
            {
                languages && languages.length > 0 ?
                    (
                        <Table
                            pagination={false}
                            bordered={true}
                            dataSource={languages}
                            columns={getcolumnHeaders()}
                        />
                    ) : null
            }
            {
                isEditScreenVisible ?
                    (
                        <LanguageEditFormContainer  
                        onClose={onLanguageEditFormContainerClose}
                        id={currentSelectedLanguageId.current} />
                    ) : null
            }
        </div>
    )
})

export default LanguageList;
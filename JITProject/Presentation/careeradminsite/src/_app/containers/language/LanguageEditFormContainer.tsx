import * as React from 'react';
import { Drawer, Button } from 'antd';
import {
    useLanguageDispatcher,
    useLanguageContext,
    updateLanguageEditScreenVisibility
} from '_app/actions';
import { LanguageEditModel, LanguageClient, LanguagePostmodel, BaseViewModel } from '_app/api';
import { ILanguageEditFormContainerProps, ILanguageEditFormContainerState } from '.';
import { LanguageEditForm } from '_app/components/language';

const initialState = {
    languageEditForm: new LanguageEditModel(),
    isCreateOrUpdateTriggered: false,
    isBusy: false,
    isButtonDisabled: false,
} as ILanguageEditFormContainerState

const LanguageEditFormContainer: React.FC<ILanguageEditFormContainerProps> = React.memo((props: ILanguageEditFormContainerProps) => {
    const [languageEditFormState, setLanguageEditFormState] = React.useState<ILanguageEditFormContainerState>(initialState);
    const { isCreateOrUpdateTriggered, languageEditForm, isBusy, isButtonDisabled } = languageEditFormState
    const languageDispatcher = useLanguageDispatcher();
    const languageContext = useLanguageContext();
    const { isEditScreenVisible } = languageContext;
    const { id,onClose } = props;
    const editScreenVisibility = React.useRef(true);

    React.useEffect(() => {
        return () => {
            if (onClose) {
                onClose();
            }
        }
    }, [onClose])

    React.useEffect(() => {
        (async () => {
            const languageApi = new LanguageClient();
            setLanguageEditFormState((languageEditFormState) => {
                return {
                    ...languageEditFormState,
                    isBusy: true,
                    isButtonDisabled: false
                }
            });
            try {
                const result = await languageApi.get({ id } as BaseViewModel);
                setLanguageEditFormState((languageEditFormState) => {
                    return {
                        ...languageEditFormState,
                        isBusy: false,
                        languageEditForm: result.data as LanguageEditModel
                    }
                });
            }
            catch (error) {
                setLanguageEditFormState((languageEditFormState) => {
                    return {
                        ...languageEditFormState,
                        isBusy: false
                    }
                });
            }
        })();
    }, [id])

    const onInputChange = (name: string, value: string | boolean) => {
        const languageFormUpdates = {
            [name]: value,
        };
        setLanguageEditFormState({
            ...languageEditFormState,
            languageEditForm: Object.assign(languageEditForm, languageFormUpdates) as LanguageEditModel
        });
    }

    const onDrawerClose = async () => {
        await updateLanguageEditScreenVisibility(languageDispatcher, false);
    }

    React.useEffect(() => {
        if (isCreateOrUpdateTriggered) {
            (async () => {
                const languageApi = new LanguageClient();
                try {
                    const { active, code, id, title, description } = languageEditForm;
                    const postModel = {
                        active,
                        code,
                        id,
                        description,
                        title
                    } as LanguagePostmodel;
                    const result = await languageApi.createOrEdit(postModel);
                    result.isSuccess && editScreenVisibility.current && await updateLanguageEditScreenVisibility(languageDispatcher, false);
                    if (result.isSuccess) {
                        //alert(result.message);
                    }
                    else {
                        //alert(result.message);
                    }
                    setLanguageEditFormState((languageEditFormState) => {
                        return {
                            ...languageEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false,
                            isButtonDisabled: false,
                        }
                    })
                } catch (error) {
                    console.error(error);
                    setLanguageEditFormState((languageEditFormState) => {
                        return {
                            ...languageEditFormState,
                            isCreateOrUpdateTriggered: false,
                            isBusy: false
                        }
                    })
                }
            })();
        }
    }, [isCreateOrUpdateTriggered, languageEditForm, languageDispatcher])

    const onFormSubmit = async (closeEditScreen: boolean) => {
        setLanguageEditFormState({
            ...languageEditFormState,
            isButtonDisabled: true,
            isCreateOrUpdateTriggered: true,
            isBusy: true,

        });
        editScreenVisibility.current = closeEditScreen;
    }

    const onSaveButonClick = () => {
        onFormSubmit(true);
    }

    const onSaveAndContinueButonClick = () => {
        onFormSubmit(false);
    }

    return <div>
        <Drawer
            title={id ? 'Edit Language' : 'Add Language'}
            width={650}
            onClose={onDrawerClose}
            visible={isEditScreenVisible}>
            {isBusy ? <>Loading</> : <LanguageEditForm
                languageEditForm={languageEditForm}
                onFieldValueChange={onInputChange}
            />}

            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Button
                    htmlType="button"
                    disabled={isButtonDisabled}
                    style={{ marginRight: 12 }}
                    onClick={onSaveButonClick}
                    type="primary">
                    Save
            </Button>
                <Button
                    onClick={onSaveAndContinueButonClick}
                    disabled={isButtonDisabled}
                    htmlType="button"
                    style={{ marginRight: 12 }}>
                    Save And Continue
            </Button>
                <Button onClick={onDrawerClose}>
                    Cancel
            </Button>
            </div>

        </Drawer>
    </div>
})

export default LanguageEditFormContainer;

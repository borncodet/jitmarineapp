import React from 'react';
import { IToolbarContainerItemProps, IToolbarContainerProps } from '.';
import { Button } from 'antd';

const ToolbarContainer: React.FC<IToolbarContainerProps> = ({ items, farItems }) => {

    const getFormatedItem = (item: IToolbarContainerItemProps): React.ReactNode => {
        const { onClick, label, type } = item;
        var element = null;
        switch (type) {
            case "button":
                element = <Button 
                    onClick={onClick}>
                    {label}
                </Button>
                break;
        }
        return element;
    }

    return (
        <div style={{ display: 'block' }}>
            {
                items && items.length > 0 ?
                    items.map((item: IToolbarContainerItemProps, index: number) => {
                        return <div style={{ float: 'left' }} key={`ToolbarContainerItem_` + index}>
                            {
                                getFormatedItem(item)
                            }
                        </div>
                    })
                    : null
            }
            {
                farItems && farItems.length > 0 ?
                    farItems.map((item: IToolbarContainerItemProps, index: number) => {
                        return <div style={{ float: 'right' }} key={`ToolbarContainerItem_` + index}>
                            {
                                getFormatedItem(item)
                            }
                        </div>
                    })
                    : null
            }
        </div>
    )
}

export default ToolbarContainer;
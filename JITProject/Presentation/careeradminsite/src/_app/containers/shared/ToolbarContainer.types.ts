export interface IToolbarContainerProps {
    items?: IToolbarContainerItemProps[];
    farItems?: IToolbarContainerItemProps[];
}

export type ToolbarContainerItemTypes = "button" | "text" | "link";

export interface IToolbarContainerItemProps {
    type: ToolbarContainerItemTypes;
    label?: string;
    icon?: React.ReactNode;
    onClick?: () => void;
    onChange?: () => void;
}
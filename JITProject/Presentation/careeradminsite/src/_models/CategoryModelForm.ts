export class CategoryModelForm{
    public  id:string | undefined
    public title: string | undefined;
    public description: string | undefined;
    public active:boolean | undefined
}
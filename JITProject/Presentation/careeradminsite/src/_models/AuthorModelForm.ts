export class AuthorModelForm{
    public  id:string | undefined
    public name: string | undefined;
    public code: string | undefined;
    public description: string | undefined;
    public active:boolean | undefined
}
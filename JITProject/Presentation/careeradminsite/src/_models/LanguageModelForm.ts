export class LanguageModelForm{
    public  id:string | undefined
    public title: string | undefined;
    public code: string | undefined;
    public description: string | undefined;
    public active:boolean | undefined
}
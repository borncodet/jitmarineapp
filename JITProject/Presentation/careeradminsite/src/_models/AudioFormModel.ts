export class AudioModelForm{
    public title: string | undefined;
    public subTitle:string | undefined;
    public description: string | undefined;
}
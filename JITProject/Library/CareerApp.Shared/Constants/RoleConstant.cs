﻿namespace CareerApp.Shared.Constants
{
	public static class RoleConstant
	{
		public const string RoleIndividualUser = "IndividualUser";
		public const string RoleGroupMember = "GroupMember";
		public const string RoleGroupAdmin = "GroupAdmin";
	}
}

﻿namespace CareerApp.Shared.Enums
{
	public enum ResponseCode
	{
		Ok = 1,
		InvalidData = 2,
		RequestWasSuccess = 3,
		RequestWasFailure = 4,
		InternalServerError = 5
	}
}

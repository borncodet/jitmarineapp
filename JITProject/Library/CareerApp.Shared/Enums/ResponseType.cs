﻿namespace CareerApp.Shared.Enums
{
	public enum ResponseType
	{
		Success = 1,
		Error,
		Info,
		Warning
	}
}

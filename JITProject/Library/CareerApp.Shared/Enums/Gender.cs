﻿namespace CareerApp.Shared.Enums
{
	public enum Gender
	{
		NotRequiredForThisApp = 1,
		NotSpecify = 10,
		Male = 11,
		Female = 12,
		Transgender = 13
	}
}

﻿namespace CareerApp.Shared.Enums
{
    public enum VendorStatus
    {
        Submitted = 1,
        Processing,
        Approved,
        Rejected,
    }
}

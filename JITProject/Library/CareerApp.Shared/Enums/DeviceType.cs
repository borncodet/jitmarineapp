﻿namespace CareerApp.Shared.Enums
{
	public enum DeviceType
	{
		WebBrowser = 1,
		MobileBrowser,
		Ios,
		Android
	}
}

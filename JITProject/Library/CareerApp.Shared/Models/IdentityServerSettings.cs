﻿namespace CareerApp.Shared.Models
{
    public class IdentityServerSettings
    {
        public string Authority { get; set; }

        public bool RequireHttpsMetadata { get; set; }

        public string ApiName { get; set; }
    }
}

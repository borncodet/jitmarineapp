﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CareerApp.Shared.Authorization
{
	public static class ApplicationPermissions
	{
		public static ReadOnlyCollection<ApplicationPermission> AllPermissions;

		public const string UsersPermissionGroupName = "User Permissions";
		public static ApplicationPermission ViewUsers = new ApplicationPermission("View Users", "users.view", UsersPermissionGroupName, "Permission to view other users account details");
		public static ApplicationPermission ManageUsers = new ApplicationPermission("Manage Users", "users.manage", UsersPermissionGroupName, "Permission to create, delete and modify other users account details");

		public const string RolesPermissionGroupName = "Role Permissions";
		public static ApplicationPermission ViewRoles = new ApplicationPermission("View Roles", "roles.view", RolesPermissionGroupName, "Permission to view available roles");
		public static ApplicationPermission ManageRoles = new ApplicationPermission("Manage Roles", "roles.manage", RolesPermissionGroupName, "Permission to create, delete and modify roles");
		public static ApplicationPermission AssignRoles = new ApplicationPermission("Assign Roles", "roles.assign", RolesPermissionGroupName, "Permission to assign roles to users");

		public const string GroupAdminPermissionGroupName = "Group Admin Permissions";
		public static ApplicationPermission ViewGroupMembers = new ApplicationPermission("View Members", "members.view", GroupAdminPermissionGroupName, "Permission to view other users account details");
		public static ApplicationPermission ManageGroupMembers = new ApplicationPermission("Manage Members", "members.manage", GroupAdminPermissionGroupName, "Permission to create, delete and modify group member details");

		public const string IndividualPermissionGroupName = "Individual Group Permissions";
		public static ApplicationPermission ManageIndividualMembers = new ApplicationPermission("View User", "user.view", IndividualPermissionGroupName, "Permission to view the user details");

		public const string GroupMemberPermissionGroupName = "Group Members Permissions";
		public static ApplicationPermission ManageGroupUsers = new ApplicationPermission("View User", "member.view", GroupMemberPermissionGroupName, "Permission to view the group member details");

		public const string GuestPermissionGroupName = "Guest Permissions";
		public static ApplicationPermission ManageGuestMembers = new ApplicationPermission("View All", "all.view", GuestPermissionGroupName, "Permission to view the details");

		static ApplicationPermissions()
		{
			List<ApplicationPermission> allPermissions = new List<ApplicationPermission>()
			{
				ViewUsers,
				ManageUsers,

				ViewRoles,
				ManageRoles,
				AssignRoles,

				ViewGroupMembers,
				ManageGroupMembers,

				ManageIndividualMembers,

				ManageGroupUsers,

				ManageGuestMembers
			};

			AllPermissions = allPermissions.AsReadOnly();
		}

		public static ApplicationPermission GetPermissionByName(string permissionName)
		{
			return AllPermissions.Where(p => p.Name == permissionName).SingleOrDefault();
		}

		public static ApplicationPermission GetPermissionByValue(string permissionValue)
		{
			return AllPermissions.Where(p => p.Value == permissionValue).SingleOrDefault();
		}

		public static string[] GetAllPermissionValues()
		{
			return AllPermissions.Select(p => p.Value).ToArray();
		}

		public static string[] GetAdministrativePermissionValues()
		{
			return new string[] { ManageUsers, ManageRoles, AssignRoles };
		}

		public static string[] GetGroupAdminPermissionValues()
		{
			return new string[] { ViewGroupMembers, ManageGroupMembers };
		}

		public static string[] GetGroupMemberPermissionValues()
		{
			return new string[] { ManageGroupUsers };
		}

		public static string[] GetIndividualPermissionValues()
		{
			return new string[] { ManageIndividualMembers };
		}

		public static string[] GetGuestPermissionValues()
		{
			return new string[] { ManageGuestMembers };
		}
	}

	public class ApplicationPermission
	{
		public ApplicationPermission()
		{ }

		public ApplicationPermission(string name, string value, string groupName, string description = null)
		{
			Name = name;
			Value = value;
			GroupName = groupName;
			Description = description;
		}

		public string Name { get; set; }
		public string Value { get; set; }
		public string GroupName { get; set; }
		public string Description { get; set; }

		public override string ToString()
		{
			return Value;
		}

		public static implicit operator string(ApplicationPermission permission)
		{
			return permission.Value;
		}
	}
}

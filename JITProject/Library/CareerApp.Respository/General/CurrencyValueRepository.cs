﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICurrencyValueRepository : IRepository<CurrencyValue>
    {
        Task<IEnumerable<CurrencyValueInfo>> GeAllByCurrencyIdAsync(string currencyId);
        Task<CommonEntityResponse> Save(int userId, CurrencyValuePostmodel model);
    }

    public class CurrencyValueRepository : BaseRepository<CurrencyValue>, ICurrencyValueRepository
    {
        public CurrencyValueRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IEnumerable<CurrencyValueInfo>> GeAllByCurrencyIdAsync(string currencyId)
        {
            return await QueryAsync<CurrencyValueInfo>($@"SELECT  Id, CurrencyId, ValueInId, Value, Active
            FROM CA_CurrencyValues
            Where currencyId='{currencyId}'");
        }

        public async Task<CommonEntityResponse> Save(int userId, CurrencyValuePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();
            var cn = GetDbConnection();
            cn.Open();
            using (var tran = cn.BeginTransaction())
            {
                try
                {
                    ///Update excisiting entries
                    var excistingEntries = model.Items.Where(c => !String.IsNullOrEmpty(c.RowId));
                    await ExecuteAsync(cn, $@"Update CA_CurrencyValues Set UpdatedBy='{userId}', UpdatedDate=GETDATE(), CurrencyId=@CurrencyId , ValueInId=@ValueInId, Value=@Value where Id=@Id", excistingEntries, transaction: tran);

                    ///Insert new entriess
                    var newEntries = model.Items.Where(c => String.IsNullOrEmpty(c.RowId));
                    await ExecuteAsync(cn, $@"Insert into CA_CurrencyValues (Id, CreatedBy, UpdatedBy, CreatedDate, UpdatedDate, Active, CurrencyId, ValueInId, Value)
                    values(NEWID(),'{userId}','{userId}',GETDATE(),GETDATE(),1, @CurrencyId, @ValueInId,@Value)", newEntries, transaction: tran);

                    tran.Commit();
                    viewModel.CreateSuccessResponse();
                }
                catch (Exception err)
                {
                    tran.Rollback();
                    viewModel.CreateFailureResponse(err.InnerException.Message.ToString());
                }
            }
            return viewModel;
        }
    }
}

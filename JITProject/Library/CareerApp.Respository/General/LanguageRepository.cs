﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ILanguageRepository : IRepository<Language>
    {
        Task<IPagedList<Language>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class LanguageRepository : BaseRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<Language>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<Language> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.Name.Contains(searchWord));
            }

            query = query.OrderBy(c => c.Name);
            return await Task.Run(() => new PagedList<Language>(query, pageIndex, pageSize));
        }

    }
}

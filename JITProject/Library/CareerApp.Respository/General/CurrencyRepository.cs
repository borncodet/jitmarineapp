﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICurrencyRepository : IRepository<Currency>
    {
        Task<IPagedList<Currency>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class CurrencyRepository : BaseRepository<Currency>, ICurrencyRepository
    {
        public CurrencyRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<Currency>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<Currency> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.Name.Contains(searchWord));
            }

            query = query.OrderBy(c => c.Name);
            return await Task.Run(() => new PagedList<Currency>(query, pageIndex, pageSize));
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IGenderRepository : IRepository<Gender>
    {
        Task<IPagedList<Gender>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class GenderRepository : BaseRepository<Gender>, IGenderRepository
    {
        public GenderRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<Gender>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<Gender> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.Title.Contains(searchWord));
            }

            query = query.OrderBy(c => c.Title);
            return await Task.Run(() => new PagedList<Gender>(query, pageIndex, pageSize));
        }

    }
}

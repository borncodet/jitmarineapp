﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IMediaRepository : IRepository<Country>
    {
        Task<string> SaveFile(string path, IFormFile file);
        Task<List<string>> SaveMultipleFile(string path, List<IFormFile> files);
    }

    public class MediaRepository : BaseRepository<Country>, IMediaRepository
    {
        public MediaRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        //Uploading Files 
        public async Task<string> SaveFile(string path, IFormFile file)
        {
            string fileName = null;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string fileId = Guid.NewGuid().ToString("N");
            string extension = Path.GetExtension(file.FileName).Substring(1);
            fileName = $"{fileId}.{extension}";
            using (var fileStream = new FileStream(path + @"\" + fileName, FileMode.Create))
            {
                fileStream.Position = 0;
                await file.CopyToAsync(fileStream);
                fileStream.Flush();
            }
            return fileName;
        }

        //Uploading Multiple Files 
        public async Task<List<string>> SaveMultipleFile(string path, List<IFormFile> files)
        {
            List<string> fileNames = new List<string>();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            foreach (var file in files)
            {
                string fileId = Guid.NewGuid().ToString("N");
                string extension = Path.GetExtension(file.FileName).Substring(1);
                var fileName = $"{fileId}.{extension}";
                fileNames.Add(fileName);
                using (var fileStream = new FileStream(path + @"\" + fileName, FileMode.Create))
                {
                    fileStream.Position = 0;
                    await file.CopyToAsync(fileStream);
                    fileStream.Flush();
                }
            }
            return fileNames;
        }
    }
}

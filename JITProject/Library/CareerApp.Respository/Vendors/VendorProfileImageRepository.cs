﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IVendorProfileImageRepository : IRepository<VendorProfileImage>
    {
        Task<IPagedList<VendorProfileImageInfo>> GetAllAsync(DataSourceVendorRequestModel searchModel);
        Task<VendorProfileImageSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class VendorProfileImageRepository : BaseRepository<VendorProfileImage>, IVendorProfileImageRepository
    {
        public VendorProfileImageRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<VendorProfileImageInfo>> GetAllAsync(DataSourceVendorRequestModel searchModel)
        {
            string query = @"SELECT vpi.RowId as RowId, VendorProfileImageId, vpi.VendorId as VendorId,
                                    vn.VendorName AS VendorName, ImageUrl, Base64Image, vpi.IsActive as IsActive
                            FROM CA_VendorProfileImage vpi
                            JOIN CA_Vendors vn on vpi.VendorId = vn.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and vpi.IsActive=1";
            }
            if (searchModel.VendorId > 0)
            {
                query += $" and vpi.VendorId ='{searchModel.VendorId}'";
            }
            query += "  ORDER BY vpi.CreatedDate";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<VendorProfileImageInfo>(queries.query);
            return new DapperPagedList<VendorProfileImageInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<VendorProfileImageSelectBoxDataViewModel> GetSelectBoxData()
        {
            VendorProfileImageSelectBoxDataViewModel result = new VendorProfileImageSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

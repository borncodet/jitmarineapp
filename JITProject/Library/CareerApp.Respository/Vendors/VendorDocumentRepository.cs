﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IVendorDocumentRepository : IRepository<VendorDocument>
    {

        Task<IPagedList<VendorDocumentInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<VendorDocumentSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class VendorDocumentRepository : BaseRepository<VendorDocument>, IVendorDocumentRepository
    {
        public VendorDocumentRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<VendorDocumentInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT vd.RowId as RowId, VendorDocumentId, Document1, Document2, Document3, vd.VendorId VendorId, CompanyName, vd.VerificationStatus VerificationStatus, EmployeeId, vd.IsActive as IsActive
            FROM CA_VendorDocuments vd
            JOIN CA_Vendors vn on vd.VendorId = vn.RowId
            Where vd.IsActive=1
            Order by CompanyName";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<VendorDocumentInfo>(queries.query);
            return new DapperPagedList<VendorDocumentInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<VendorDocumentSelectBoxDataViewModel> GetSelectBoxData()
        {
            VendorDocumentSelectBoxDataViewModel result = new VendorDocumentSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

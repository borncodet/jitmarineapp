﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IVendorRepository : IRepository<Vendor>
    {
        Task<IPagedList<VendorInfo>> GetAllAsync(DataSourceVendorRequestModel searchModel);
        Task<int> GetVendorIdAsync(int userId);
        Task<VendorSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class VendorRepository : BaseRepository<Vendor>, IVendorRepository
    {
        public VendorRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<VendorInfo>> GetAllAsync(DataSourceVendorRequestModel searchModel)
        {
            string query = @"SELECT emp.RowId as RowId, VendorId, emp.VendorName as VendorName, emp.JobRole as JobRole, emp.Location as Location,
                                    emp.Designation as Designation, emp.CountryCode as CountryCode, emp.PhoneNumber as PhoneNumber, 
                                    emp.Organisation as Organisation, emp.Email as Email, emp.AboutMe as AboutMe, emp.VendorDocument as VendorDocument, 
                                    emp.UserId as UserId, emp.VendorStatusId as VendorStatusId, vs.Title as VendorStatus, emp.IsActive as IsActive
                             FROM CA_Vendors emp
                             JOIN CA_VendorsStatus vs on emp.VendorStatusId = vs.RowId   
                             WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " AND emp.IsActive=1";
            }
            if (searchModel.VendorId > 0)
            {
                query += $" AND emp.RowId ='{searchModel.VendorId}'";
            }
            query += "  ORDER BY emp.VendorName";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<VendorInfo>(queries.query);
            return new DapperPagedList<VendorInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<int> GetVendorIdAsync(int userId)
        {
            string query = $@"SELECT RowId
                              FROM CA_Vendors
                              WHERE UserId = '{userId}'";

            int count = await ExecuteScalarAsync(query);
            return count;
        }

        public async Task<VendorSelectBoxDataViewModel> GetSelectBoxData()
        {
            VendorSelectBoxDataViewModel result = new VendorSelectBoxDataViewModel
            {
                States = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Name as Caption From CA_States where IsActive=1"),
                Countries = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Name as Caption From CA_Countries where IsActive=1"),
                VendorStatuses = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title as Caption From CA_VendorsStatus where IsActive=1")
            };
            return result;
        }

    }
}

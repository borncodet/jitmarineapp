﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IBankAccountTypeRepository : IRepository<BankAccountType>
    {
        Task<IPagedList<BankAccountTypeInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<BankAccountTypeSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class BankAccountTypeRepository : BaseRepository<BankAccountType>, IBankAccountTypeRepository
    {
        public BankAccountTypeRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<BankAccountTypeInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT bat.RowId as RowId, BankAccountTypeId, Title, Description, bat.IsActive as IsActive
                            FROM CA_BankAccountType bat
                            WHERE bat.IsActive=1
                            ORDER BY bat.Title";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<BankAccountTypeInfo>(queries.query);
            return new DapperPagedList<BankAccountTypeInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<BankAccountTypeSelectBoxDataViewModel> GetSelectBoxData()
        {
            BankAccountTypeSelectBoxDataViewModel result = new BankAccountTypeSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

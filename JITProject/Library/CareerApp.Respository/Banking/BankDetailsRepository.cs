﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IBankDetailsRepository : IRepository<BankDetails>
    {
        Task<IPagedList<BankDetailsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<BankDetailsSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class BankDetailsRepository : BaseRepository<BankDetails>, IBankDetailsRepository
    {
        public BankDetailsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<BankDetailsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT bd.RowId as RowId, BankDetailsId, bd.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, BankName, BranchName, AccountNo, IFSCIBANSWIFTNo,
                                    bd.BankAccountTypeId as BankAccountTypeId, bat.Title as BankAccountTypeTitle, bd.StateId as StateId, 
                                    st.Name as StateName,  bd.CountryId as CountryId, cn.Name as CountryName, DigiDocumentDetailId, bd.IsActive as IsActive
                            FROM CA_BankDetails bd
                            JOIN CA_Candidate can on bd.CandidateId = can.RowId
                            JOIN CA_BankAccountType bat on bd.BankAccountTypeId = bat.RowId
                            JOIN CA_States st on bd.StateId = st.RowId
                            JOIN CA_Countries cn on bd.CountryId = cn.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and bd.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and bd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY bd.BankName";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<BankDetailsInfo>(queries.query);
            return new DapperPagedList<BankDetailsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<BankDetailsSelectBoxDataViewModel> GetSelectBoxData()
        {
            BankDetailsSelectBoxDataViewModel result = new BankDetailsSelectBoxDataViewModel
            {
                BankAccountTypes = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_BankAccountType where IsActive=1"),
                States = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Name as Caption From CA_States where IsActive=1"),
                Countries = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Name as Caption From CA_Countries where IsActive=1")
            };
            return result;
        }

    }
}

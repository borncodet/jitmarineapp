﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IUniversityRepository : IRepository<University>
    {
        Task<IPagedList<UniversityInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<UniversitySelectBoxDataViewModel> GetSelectBoxData();
    }

    public class UniversityRepository : BaseRepository<University>, IUniversityRepository
    {
        public UniversityRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<UniversityInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT uty.RowId as RowId, UniversityId, uty.Name as Name, uty.Email as Email,
                                    uty.TelephoneNumber as TelephoneNumber, uty.Address1 as Address1, uty.Address2 as Address2,
                                    uty.City as City, uty.StateId as StateId, st.Name as StateName,
                                    uty.CountryId as CountryId, cn.Name as CountryName, uty.ZipCode as ZipCode, uty.IsActive as IsActive
                            FROM CA_University uty
                            JOIN CA_States st on uty.StateId = st.RowId
                            JOIN CA_Countries cn on uty.CountryId = cn.RowId
                            WHERE uty.IsActive=1
                            ORDER BY uty.Name";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<UniversityInfo>(queries.query);
            return new DapperPagedList<UniversityInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<UniversitySelectBoxDataViewModel> GetSelectBoxData()
        {
            UniversitySelectBoxDataViewModel result = new UniversitySelectBoxDataViewModel
            {
                States = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Name as Caption From CA_States where IsActive=1"),
                Countries = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Name as Caption From CA_Countries where IsActive=1")
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAppliedDigiDocMapRepository : IRepository<JobAppliedDigiDocMap>
    {
        Task<IPagedList<JobAppliedDigiDocMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<JobAppliedDigiDocMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAppliedDigiDocMapRepository : BaseRepository<JobAppliedDigiDocMap>, IJobAppliedDigiDocMapRepository
    {
        public JobAppliedDigiDocMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAppliedDigiDocMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT ja.RowId as RowId, JobAppliedDigiDocMapId, ja.JobAppliedDetailsId as JobAppliedDetailsId,
                                    ja.DigiDocumentDetailId as DigiDocumentDetailId, ddd.Name as Name, 
                                    ddd.DocumentNumber as DocumentNumber, ddd.Description as Description, 
                                    ddd.DigiDocumentTypeId as DigiDocumentTypeId, ddt.Title as DigiDocumentTypeTitle, 
                                    ddd.ExpiryDate as ExpiryDate, ddd.ExpiryFlag as ExpiryFlag, 
                                    CAST(CASE WHEN  datediff(DAY, GETDATE(), ddd.ExpiryDate) <=0 THEN 1 ELSE 0 END AS BIT) AS IsExpired,
                                    ja.IsActive as IsActive
                             FROM CA_JobAppliedDigiDocMap ja
                             JOIN  CA_DigiDocumentDetails ddd on ja.DigiDocumentDetailId = ddd.RowId
                             JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                             WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            query += "  ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAppliedDigiDocMapInfo>(queries.query);
            return new DapperPagedList<JobAppliedDigiDocMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<int> GetJobAppliedDigiDocMapCountAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = $@"SELECT COUNT(*) as JobAppliedDigiDocMapCount
                              FROM CA_JobAppliedDigiDocMap ja
                              JOIN CA_Candidate can on ja.CandidateId = can.RowId
                              JOIN CA_JobLists jl on ja.JobId = jl.RowId
                              WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }

            var count = await ExecuteScalarAsync(query);
            return count;
        }

        public async Task<JobAppliedDigiDocMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAppliedDigiDocMapSelectBoxDataViewModel result = new JobAppliedDigiDocMapSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

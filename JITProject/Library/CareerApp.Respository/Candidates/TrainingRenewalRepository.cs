﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ITrainingRenewalRepository : IRepository<TrainingRenewal>
    {
        Task<IPagedList<TrainingRenewalInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<TrainingRenewalSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class TrainingRenewalRepository : BaseRepository<TrainingRenewal>, ITrainingRenewalRepository
    {
        public TrainingRenewalRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<TrainingRenewalInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT trl.RowId as RowId, TrainingRenewalId, trl.TrainingId as TrainingId, tr.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, tr.CourseId as CourseId, cs.Title as CourseTitle, tr.IssuedBy as IssuedBy, 
                                     tr.ValidUpTo as ValidUpTo, trl.IsActive as IsActive
                            FROM CA_TrainingRenewal trl
                            JOIN CA_Training tr on trl.TrainingId = tr.RowId
                            JOIN CA_Candidate can on tr.CandidateId = can.RowId
                            JOIN CA_Courses cs on tr.CourseId = cs.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and trl.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and tr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cs.Title";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<TrainingRenewalInfo>(queries.query);
            return new DapperPagedList<TrainingRenewalInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<TrainingRenewalSelectBoxDataViewModel> GetSelectBoxData()
        {
            TrainingRenewalSelectBoxDataViewModel result = new TrainingRenewalSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

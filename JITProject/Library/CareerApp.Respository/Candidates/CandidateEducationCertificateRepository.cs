﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateEducationCertificateRepository : IRepository<CandidateEducationCertificate>
    {
        Task<IPagedList<CandidateEducationCertificateInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateEducationCertificateSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateEducationCertificateRepository : BaseRepository<CandidateEducationCertificate>, ICandidateEducationCertificateRepository
    {
        public CandidateEducationCertificateRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateEducationCertificateInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT cec.RowId as RowId, CandidateEducationCertificateId, cec.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, cec.EducationQualificationId as EducationQualificationId,
                                    cec.Certificate as Certificate, eq.Course as Course, eq.University as University, 
                                    eq.Grade as Grade, eq.DateFrom as DateFrom, eq.DateTo as DateTo, cec.IsActive as IsActive
                            FROM CA_CandidateEducationCertificate cec
                            JOIN CA_Candidate can on cec.CandidateId = can.RowId
                            JOIN CA_EducationQualification eq on cec.EducationQualificationId = eq.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and cec.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cec.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY eq.DateFrom";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateEducationCertificateInfo>(queries.query);
            return new DapperPagedList<CandidateEducationCertificateInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateEducationCertificateSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateEducationCertificateSelectBoxDataViewModel result = new CandidateEducationCertificateSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

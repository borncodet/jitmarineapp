﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateProjectsRepository : IRepository<CandidateProjects>
    {
        Task<IPagedList<CandidateProjectsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateProjectsSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateProjectsRepository : BaseRepository<CandidateProjects>, ICandidateProjectsRepository
    {
        public CandidateProjectsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateProjectsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT cp.RowId as RowId, CandidateProjectId, cp.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, cp.ProjectName as ProjectName, cp.TeamSize as TeamSize, 
                                    cp.Description as Description, cp.FromDate as FromDate, cp.ToDate as ToDate, 
                                    cp.IsActive as IsActive
                            FROM CA_CandidateProjects cp
                            JOIN CA_Candidate can on cp.CandidateId = can.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cp.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cp.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cp.FromDate";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateProjectsInfo>(queries.query);
            return new DapperPagedList<CandidateProjectsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateProjectsSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateProjectsSelectBoxDataViewModel result = new CandidateProjectsSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

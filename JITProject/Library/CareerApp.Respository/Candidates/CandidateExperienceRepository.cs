﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateExperienceRepository : IRepository<CandidateExperience>
    {
        Task<IPagedList<CandidateExperienceInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateExperienceSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateExperienceRepository : BaseRepository<CandidateExperience>, ICandidateExperienceRepository
    {
        public CandidateExperienceRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateExperienceInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT ce.RowId as RowId, CandidateExperienceId, ce.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, EmployerName, Locationid, JobRole, FromDate, 
                                     ToDate, CurrentlyWorkHereFlag, Responsibilities, Achievements, ce.IsActive as IsActive
                            FROM CA_CandidateExperience ce
                            JOIN CA_Candidate can on ce.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ce.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ce.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ce.FromDate";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateExperienceInfo>(queries.query);
            return new DapperPagedList<CandidateExperienceInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateExperienceSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateExperienceSelectBoxDataViewModel result = new CandidateExperienceSelectBoxDataViewModel
            {
                JobRoles = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_JobRoles where IsActive=1")
            };
            return result;
        }

    }
}

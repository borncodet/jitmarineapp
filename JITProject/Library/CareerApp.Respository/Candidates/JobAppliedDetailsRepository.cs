﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAppliedDetailsRepository : IRepository<JobAppliedDetails>
    {
        Task<IPagedList<JobAppliedDetailsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<IPagedList<RecentlyAppliedJobDetailsInfo>> GetRecentlyAppliedJobAsync(DataSourceCandidateRequestModel searchModel);
        Task<JobAppliedAndDocInfo> GetJobAppliedDetails(DataSourceCandidateJobAppliedRequestModel searchModel);
        Task<JobAppliedDetailsSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAppliedDetailsRepository : BaseRepository<JobAppliedDetails>, IJobAppliedDetailsRepository
    {
        public JobAppliedDetailsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        //Get applied job list - for admin(brief)
        public async Task<IPagedList<JobAppliedDetailsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = $@"SELECT ja.RowId as RowId, JobAppliedDetailsId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ja.JobId as JobId, jl.Title as JobTitle, 
                                    ja.ResumeCandidateMapId as ResumeCandidateMapId, ja.CoverLetterCandidateMapId as CoverLetterCandidateMapId, 
                                    ja.ResumeDocument as ResumeDocument, ja.CoverLetterDocument as CoverLetterDocument, 
                                    CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                                    CAST(1 AS BIT) AS IsApplied, ja.IsActive as IsActive
                            FROM CA_JobAppliedDetails ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId
                            JOIN CA_JobLists jl on ja.JobId = jl.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jl.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }
            query += " ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAppliedDetailsInfo>(queries.query);
            return new DapperPagedList<JobAppliedDetailsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        //Get applied job list - for candidate(detailed)
        public async Task<IPagedList<RecentlyAppliedJobDetailsInfo>> GetRecentlyAppliedJobAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = $@"SELECT ja.RowId as RowId, ja.JobId as JobId, JobAppliedDetailsId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, ja.CreatedDate as PostedDate, 
                            ja.ResumeCandidateMapId as ResumeCandidateMapId, ja.CoverLetterCandidateMapId as CoverLetterCandidateMapId, 
                            ja.ResumeDocument as ResumeDocument, ja.CoverLetterDocument as CoverLetterDocument, 
                            DATEDIFF(d, ja.CreatedDate, '{DateTime.Now.Date}') as DaysAgo, CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(1 AS BIT) AS IsApplied, jb.IsActive as IsActive
                            FROM CA_JobAppliedDetails ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId                        
                            JOIN CA_JobLists jb on ja.JobId = jb.RowId
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }
            query += " ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<RecentlyAppliedJobDetailsInfo>(queries.query);
            return new DapperPagedList<RecentlyAppliedJobDetailsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        //Get applied job details
        public async Task<JobAppliedAndDocInfo> GetJobAppliedDetails(DataSourceCandidateJobAppliedRequestModel searchModel)
        {
            JobAppliedAndDocInfo objInfo = new JobAppliedAndDocInfo();
            string query = $@"SELECT ja.RowId as RowId, JobAppliedDetailsId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ja.JobId as JobId, jl.Title as JobTitle, 
                                    ja.ResumeCandidateMapId as ResumeCandidateMapId, ja.CoverLetterCandidateMapId as CoverLetterCandidateMapId, 
                                    ja.ResumeDocument as ResumeDocument, ja.CoverLetterDocument as CoverLetterDocument, 
                                    CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                                    CAST(1 AS BIT) AS IsApplied, ja.IsActive as IsActive
                            FROM CA_JobAppliedDetails ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId
                            JOIN CA_JobLists jl on ja.JobId = jl.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jl.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }
            if (searchModel.JobId > 0)
            {
                query += $" and ja.JobId ='{searchModel.JobId}'";
            }
            query += "  ORDER BY ja.CreatedDate DESC";

            var jobAppliedDetails = await QueryFirstOrDefaultAsync<JobAppliedDetailsInfo>(query);
            objInfo.JobAppliedDetailsInfo = jobAppliedDetails;

            if (jobAppliedDetails != null)
            {
                query = @"SELECT ja.RowId as RowId, JobAppliedDigiDocMapId, ja.JobAppliedDetailsId as JobAppliedDetailsId,
                                    ja.DigiDocumentDetailId as DigiDocumentDetailId, ddd.Name as Name, 
                                    ddd.DocumentNumber as DocumentNumber, ddd.Description as Description, 
                                    ddd.DigiDocumentTypeId as DigiDocumentTypeId, ddt.Title as DigiDocumentTypeTitle, 
                                    ddd.ExpiryDate as ExpiryDate, ddd.ExpiryFlag as ExpiryFlag, 
                                    CAST(CASE WHEN  datediff(DAY, GETDATE(), ddd.ExpiryDate) <=0 THEN 1 ELSE 0 END AS BIT) AS IsExpired,
                                    ja.IsActive as IsActive
                             FROM CA_JobAppliedDigiDocMap ja
                             JOIN  CA_DigiDocumentDetails ddd on ja.DigiDocumentDetailId = ddd.RowId
                             JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                             WHERE 1=1 ";

                if (!searchModel.ShowInactive)
                {
                    query += " and ja.IsActive=1";
                }
                query += $" and ja.JobAppliedDetailsId='{jobAppliedDetails.RowId}'";
                query += "  ORDER BY ja.CreatedDate DESC";

                var jobDocDetails = await QueryAsync<JobAppliedDigiDocMapInfo>(query);
                objInfo.JobAppliedDigiDocMapInfo = jobDocDetails;
            }
            return objInfo;
        }

        public async Task<JobAppliedDetailsSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAppliedDetailsSelectBoxDataViewModel result = new JobAppliedDetailsSelectBoxDataViewModel
            {
                DigiDocumentDetails = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Name as Caption From CA_DigiDocumentDetails where IsActive=1")
            };
            return result;
        }
    }
}

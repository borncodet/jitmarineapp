﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateReferencesRepository : IRepository<CandidateReferences>
    {
        Task<IPagedList<CandidateReferencesInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateReferencesSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateReferencesRepository : BaseRepository<CandidateReferences>, ICandidateReferencesRepository
    {
        public CandidateReferencesRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateReferencesInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT cr.RowId as RowId, CandidateReferenceId, cr.CandidateId as CandidateId,
                                    can.FirstName CandidateFirstName, can.LastName CandidateLastName, cr.NamePrefixId as NamePrefixId, 
                                    np.Title as NamePrefixTitle, cr.FirstName ReferenceFirstName,cr.LastName ReferenceLastName, cr.Email as Email, 
                                    cr.CountryCode as CountryCode, cr.PhoneNumber as PhoneNumber, cr.IsActive as IsActive
                            FROM CA_CandidateReferences cr
                            JOIN CA_Candidate can on cr.CandidateId = can.RowId
                            LEFT JOIN CA_NamePrefix np on cr.NamePrefixId = np.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cr.FirstName";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateReferencesInfo>(queries.query);
            return new DapperPagedList<CandidateReferencesInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateReferencesSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateReferencesSelectBoxDataViewModel result = new CandidateReferencesSelectBoxDataViewModel
            {
                NamePrefix = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_NamePrefix where IsActive=1"),
                Genders = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Gender where IsActive=1"),
                Relationships = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Relationship where IsActive=1")
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAppliedRepository : IRepository<JobApplied>
    {
        Task<IPagedList<JobAppliedInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CountInfo> GetCountAsync(DataSourceCandidateRequestModel searchModel);
        Task<ProgressInfo> GetProgressAsync(DataSourceCandidateRequestModel searchModel);
        Task<IPagedList<RecentlyAppliedJobInfo>> GetRecentlyAppliedJobAsync(int CandidateId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<int> GetJobAppliedCountAsync(BaseCandidateSearchViewModel searchModel);
        Task<JobAppliedSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAppliedRepository : BaseRepository<JobApplied>, IJobAppliedRepository
    {
        public JobAppliedRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAppliedInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = $@"SELECT ja.RowId as RowId, JobAppliedDetailsId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ja.JobId as JobId, jl.Title as JobTitle, 
                                     CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked,
                                     CAST(1 AS BIT) AS IsApplied, ja.IsActive as IsActive
                            FROM CA_JobAppliedDetails ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId
                            JOIN CA_JobLists jl on ja.JobId = jl.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jl.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAppliedInfo>(queries.query);
            return new DapperPagedList<JobAppliedInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<IPagedList<RecentlyAppliedJobInfo>> GetRecentlyAppliedJobAsync(int CandidateId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = $@"SELECT ja.RowId as RowId, ja.JobId as JobId, JobAppliedDetailsId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, ja.CreatedDate as PostedDate, 
                            DATEDIFF(d, ja.CreatedDate, '{DateTime.Now.Date}') as DaysAgo, CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(1 AS BIT) AS IsApplied, jb.IsActive as IsActive
                            FROM CA_JobAppliedDetails ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId                        
                            JOIN CA_JobLists jb on ja.JobId = jb.RowId
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{CandidateId}'
                            WHERE can.RowId = '{CandidateId}' and ja.IsActive=1";

            query += " ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<RecentlyAppliedJobInfo>(queries.query);
            return new DapperPagedList<RecentlyAppliedJobInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<int> GetJobAppliedCountAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = $@"SELECT COUNT(*) as JobAppliedCount
                              FROM CA_JobAppliedDetails ja
                              JOIN CA_Candidate can on ja.CandidateId = can.RowId
                              JOIN CA_JobLists jl on ja.JobId = jl.RowId
                              WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }

            var count = await ExecuteScalarAsync(query);
            return count;
        }

        public async Task<JobAppliedSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAppliedSelectBoxDataViewModel result = new JobAppliedSelectBoxDataViewModel
            {
            };
            return result;
        }

        public async Task<CountInfo> GetCountAsync(DataSourceCandidateRequestModel searchModel)
        {
            CountInfo objCount = new CountInfo();
            // Job Applied
            string query = $@"SELECT COUNT(*) as JobAppliedCount
                              FROM CA_JobAppliedDetails ja
                              JOIN CA_Candidate can on ja.CandidateId = can.RowId
                              JOIN CA_JobLists jl on ja.JobId = jl.RowId
                              WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ja.CandidateId ='{searchModel.CandidateId}'";
            }

            var jobAppliedCount = await ExecuteScalarAsync(query);
            objCount.JobApplied = jobAppliedCount;

            // Job Saved
            query = $@"SELECT COUNT(*) as JobSavedCount
                              FROM CA_JobBookmarked jb
                              JOIN CA_Candidate can on jb.CandidateId = can.RowId
                              JOIN CA_JobLists jl on jb.JobId = jl.RowId
                              WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and jb.CandidateId ='{searchModel.CandidateId}'";
            }

            var jobSavedCount = await ExecuteScalarAsync(query);
            objCount.JobSaved = jobSavedCount;

            // Resume Created
            query = $@"SELECT COUNT(*) as ResumeCreated
                              FROM CA_ResumeCandidateMap rcm
                              JOIN CA_Candidate can on rcm.CandidateId = can.RowId
                              WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and rcm.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and rcm.CandidateId ='{searchModel.CandidateId}'";
            }

            var resumeCreated = await ExecuteScalarAsync(query);
            objCount.ResumeCreated = resumeCreated;

            // Resume Created
            objCount.ShortListed = 0;

            // Resume Created
            objCount.NewMessages = 0;

            // New Alerts
            query = $@"
                            SELECT COUNT(*) as JobAlertCount
                            FROM CA_Candidate can, (CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId),(CA_JobPostAlerts jpa
                            JOIN CA_JobAlertCategoryMap jacm on jacm.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertExperienceMap jaem on jaem.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertIndustryMap jaim on jaim.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertRoleMap jarm on jarm.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertTypeMap jatm on jatm.JobAlertId = jpa.RowId)
                            WHERE jatm.JobTypeId = jb.JobTypeId and jacm.JobCategoryId = jb.CategoryId and jaim.IndustryId = jb.IndustryId and ( DATEDIFF(d, jb.CreatedDate, '{DateTime.Now.Date}') <= 7 )";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and jpa.UserId ='{searchModel.CandidateId}'";
            }

            var newAlertCount = await ExecuteScalarAsync(query);
            objCount.NewAlerts = newAlertCount;

            // Digital Documents
            query = $@"(SELECT ddt.Title as DocumentTypeTitle, COUNT(*) as DocumentCount
                              FROM CA_DigiDocumentDetails ddd
                              JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                              JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                              WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += $" GROUP BY ddt.Title)";
            query += "  UNION ";
            query += $@"(SELECT ddt.Title as DocumentTypeTitle, CONVERT(int, 0) as DocumentCount
                              FROM CA_DigiDocumentTypes ddt 
                              WHERE ddt.Title NOT IN (SELECT ddt.Title as DocumentTypeTitle
                              FROM CA_DigiDocumentDetails ddd
                              JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                              JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                              WHERE 1=1";
            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += $"))";

            var digiDocs = await QueryAsync<DigiDocumentCountInfo>(query);
            objCount.DigiDocs = digiDocs;

            // Expiry Digital Documents
            query = $@"(SELECT ddt.Title as DocumentTypeTitle, COUNT(*) as DocumentCount
                              FROM CA_DigiDocumentDetails ddd
                              JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                              JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                              WHERE datediff(DAY, GETDATE(), ddd.ExpiryDate) <=15 and ddd.ExpiryFlag=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += $" GROUP BY ddt.Title)";
            query += "  UNION ";
            query += $@"(SELECT ddt.Title as DocumentTypeTitle, CONVERT(int, 0) as DocumentCount
                              FROM CA_DigiDocumentTypes ddt 
                              WHERE ddt.Title NOT IN (SELECT ddt.Title as DocumentTypeTitle
                              FROM CA_DigiDocumentDetails ddd
                              JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                              JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                              WHERE datediff(DAY, GETDATE(), ddd.ExpiryDate) <=15 and ddd.ExpiryFlag=1 ";
            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += $"))";

            var expiryDigiDocs = await QueryAsync<DigiDocumentCountInfo>(query);
            objCount.ExpiryDigiDocs = expiryDigiDocs;

            return objCount;
        }

        public async Task<ProgressInfo> GetProgressAsync(DataSourceCandidateRequestModel searchModel)
        {
            ProgressInfo objProgressInfo = new ProgressInfo();
            var progressPercentage = 0;

            // Personal Information
            string query = @"SELECT can.RowId as RowId, CandidateId, can.NamePrefixId as NamePrefixId, np.Title as NamePrefixTitle, FirstName, LastName, 
                                    can.GenderId as GenderId, gen.Title as GenderTitle, can.MartialStatusId as MartialStatusId, ms.Title as MartialStatusTitle, 
                                    can.Email as Email, can.PhoneNumber as PhoneNumber, can.CountryCode as CountryCode, can.TelephoneNumber as TelephoneNumber, can.PassportInformationId as PassportInformationId, 
                                    pi.PassportNo as PassportNo, can.SeamanBookCdcId as SeamanBookCdcId, sbc.CdcNumber as CdcNumber, can.LicenceInformationId as LicenceInformationId,
                                    li.LicenceNo as LicenceNo, PanNo, AadharNo, ResidenceId, PanDigiDocumentDetailId , AadharDigiDocumentDetailId, ResidenceDigiDocumentDetailId,
                                    can.JobCategoryId as CategoryId, jc.Title as CategoryName, can.DesignationId as DesignationId, des.Title as DesignationName, 
                                    can.JobTypeId as JobTypeId, jt.Title as JobTypeName, CurrentCTC, ExpectedPackage, can.IndustryId as IndustryId, 
                                    ind.Title as IndustryName, can.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalAreaName, 
                                    can.JobRoleId as JobRoleId, jr.Title as JobRoleName, Skills, ProfileSummary, PositionApplyingFor, Dob, CurrentAddress1, CurrentAddress2, 
                                    PermanantAddress1, PermanantAddress2, City, can.StateId as StateId, st.Name as StateName, 
                                    can.CountryId as CountryId, cn.Name as CountryName, ZipCode, UserId, can.IsActive as IsActive
                            FROM CA_Candidate can
                            LEFT JOIN CA_NamePrefix np on can.NamePrefixId = np.RowId
                            LEFT JOIN CA_Gender gen on can.GenderId = gen.RowId
                            LEFT JOIN CA_MartialStatus ms on can.MartialStatusId = ms.RowId
                            LEFT JOIN CA_LicenceInformation li on can.LicenceInformationId = li.RowId
                            LEFT JOIN CA_PassportInformation pi on can.PassportInformationId = pi.RowId
                            LEFT JOIN CA_SeamanBookCdc sbc on can.SeamanBookCdcId = sbc.RowId
                            LEFT JOIN CA_JobCategories jc on can.JobCategoryId = jc.RowId
                            LEFT JOIN CA_Designation des on can.DesignationId = des.RowId
                            LEFT JOIN CA_JobTypes jt on can.JobTypeId = jt.RowId
                            LEFT JOIN CA_Industries ind on can.IndustryId = ind.RowId
                            LEFT JOIN CA_FunctionalAreas fa on can.FunctionalAreaId = fa.RowId
                            LEFT JOIN CA_JobRoles jr on can.JobRoleId = jr.RowId
                            LEFT JOIN CA_States st on can.StateId = st.RowId
                            LEFT JOIN CA_Countries cn on can.StateId = cn.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and can.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and can.RowId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY can.CandidateId";

            var personalInfo = await QueryAsync<CandidateInfo>(query);

            // Personal Information Progress
            if (personalInfo != null)
            {
                if (personalInfo.Count > 0)
                {
                    if (!string.IsNullOrEmpty(personalInfo[0].FirstName) && !string.IsNullOrWhiteSpace(personalInfo[0].FirstName))
                    {
                        progressPercentage += 3;
                    }
                    if (personalInfo[0].Dob != null)
                    {
                        progressPercentage += 1;
                    }
                    if (personalInfo[0].CountryId != 0 && personalInfo[0].CountryId != null)
                    {
                        progressPercentage += 1;
                    }
                    if (personalInfo[0].DesignationId != 0 && personalInfo[0].DesignationId != null)
                    {
                        progressPercentage += 1;
                    }
                    if (personalInfo[0].GenderId != 0 && personalInfo[0].GenderId != null)
                    {
                        progressPercentage += 1;
                    }
                    if (personalInfo[0].MartialStatusId != 0 && personalInfo[0].MartialStatusId != null)
                    {
                        progressPercentage += 1;
                    }
                    if (!string.IsNullOrEmpty(personalInfo[0].ProfileSummary) && !string.IsNullOrWhiteSpace(personalInfo[0].ProfileSummary))
                    {
                        progressPercentage += 1;
                    }
                    // Communication Progress
                    if (!string.IsNullOrEmpty(personalInfo[0].PhoneNumber) && !string.IsNullOrWhiteSpace(personalInfo[0].PhoneNumber))
                    {
                        progressPercentage += 2;
                    }
                    if (!string.IsNullOrEmpty(personalInfo[0].Email) && !string.IsNullOrWhiteSpace(personalInfo[0].Email))
                    {
                        progressPercentage += 2;
                    }
                    if (!string.IsNullOrEmpty(personalInfo[0].TelephoneNumber) && !string.IsNullOrWhiteSpace(personalInfo[0].TelephoneNumber))
                    {
                        progressPercentage += 2;
                    }
                    if (!string.IsNullOrEmpty(personalInfo[0].PermanantAddress1) && !string.IsNullOrWhiteSpace(personalInfo[0].PermanantAddress1))
                    {
                        progressPercentage += 2;
                    }
                    if (!string.IsNullOrEmpty(personalInfo[0].CurrentAddress1) && !string.IsNullOrWhiteSpace(personalInfo[0].CurrentAddress1))
                    {
                        progressPercentage += 2;
                    }
                    // Document Progress
                    if (personalInfo[0].PassportInformationId != 0 && personalInfo[0].PassportInformationId != null)
                    {
                        progressPercentage += 5;
                    }
                    if (!string.IsNullOrEmpty(personalInfo[0].AadharNo) && !string.IsNullOrWhiteSpace(personalInfo[0].AadharNo))
                    {
                        progressPercentage += 5;
                    }
                }
            }

            // Candidate Language
            query = @"SELECT clm.RowId as RowId, CandidateLanguageMapId, clm.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, clm.LanguageId as LanguageId, lan.Name as LanguageName, clm.IsActive as IsActive
                            FROM CA_CandidateLanguageMap clm
                            JOIN CA_Candidate can on clm.CandidateId = can.RowId
                            JOIN CA_Languages lan on clm.LanguageId = lan.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and clm.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and clm.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY lan.Name";

            var candidateLanguages = await QueryAsync<CandidateLanguageMapInfo>(query);

            // Candidate Language Progress
            if (candidateLanguages != null)
            {
                if (candidateLanguages.Count > 0)
                {
                    progressPercentage += 1;
                }
            }

            // Candidate Relatives
            query = @"SELECT cr.RowId as RowId, CandidateRelativeId, cr.CandidateId as CandidateId,
                                    can.FirstName CandidateFirstName, can.LastName CandidateLastName, cr.NamePrefixId as NamePrefixId, np.Title as NamePrefixTitle,
                                    cr.FirstName RelativeFirstName,cr.LastName RelativeLastName, cr.GenderId as GenderId, gn.Title as GenderTitle, cr.Email as Email, 
                                    cr.CountryCode as CountryCode, cr.PhoneNumber as PhoneNumber, cr.TelephoneNumber as TelephoneNumber, cr.RelationshipId as RelationshipId, rs.Title as RelationshipTitle, 
                                    cr.IsActive as IsActive
                            FROM CA_CandidateRelatives cr
                            JOIN CA_Candidate can on cr.CandidateId = can.RowId
                            LEFT JOIN CA_NamePrefix np on cr.NamePrefixId = np.RowId
                            LEFT JOIN CA_Gender gn on cr.GenderId = gn.RowId
                            LEFT JOIN CA_Relationship rs on cr.RelationshipId = rs.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cr.FirstName";

            var candidateRelatives = await QueryAsync<CandidateRelativesInfo>(query);

            // Candidate Relatives Progress
            if (candidateRelatives != null)
            {
                if (candidateRelatives.Count > 0)
                {
                    progressPercentage += 5;
                }
            }

            // Candidate References
            query = @"SELECT cr.RowId as RowId, CandidateReferenceId, cr.CandidateId as CandidateId,
                             can.FirstName CandidateFirstName, can.LastName CandidateLastName, cr.NamePrefixId as NamePrefixId, 
                             np.Title as NamePrefixTitle, cr.FirstName ReferenceFirstName,cr.LastName ReferenceLastName, cr.Email as Email, 
                             cr.CountryCode as CountryCode, cr.PhoneNumber as PhoneNumber, cr.IsActive as IsActive
                            FROM CA_CandidateReferences cr
                            JOIN CA_Candidate can on cr.CandidateId = can.RowId
                            LEFT JOIN CA_NamePrefix np on cr.NamePrefixId = np.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cr.FirstName";

            var candidateReferences = await QueryAsync<CandidateReferencesInfo>(query);

            // Candidate References Progress
            if (candidateReferences != null)
            {
                if (candidateReferences.Count > 0)
                {
                    progressPercentage += 5;
                }
            }

            // Candidate Education Qualification
            query = @"SELECT COUNT(*) as QualificationCount
                            FROM CA_EducationQualification eq
                            JOIN CA_Candidate can on eq.CandidateId = can.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and eq.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and eq.CandidateId ='{searchModel.CandidateId}'";
            }

            var QualificationCount = await ExecuteScalarAsync(query);

            // Candidate Education Qualification Progress
            if (QualificationCount >= 3)
            {
                progressPercentage += 15;
            }
            else if (QualificationCount >= 2)
            {
                progressPercentage += 10;
            }
            else if (QualificationCount >= 1)
            {
                progressPercentage += 5;
            }

            // Candidate Experiences
            query = @"SELECT COUNT(*) as ExperienceCount
                            FROM CA_CandidateExperience ce
                            JOIN CA_Candidate can on ce.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ce.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ce.CandidateId ='{searchModel.CandidateId}'";
            }

            var ExperienceCount = await ExecuteScalarAsync(query);

            // Candidate Experiences Progress
            if (ExperienceCount >= 3)
            {
                progressPercentage += 15;
            }
            else if (ExperienceCount >= 2)
            {
                progressPercentage += 10;
            }
            else if (ExperienceCount >= 1)
            {
                progressPercentage += 5;
            }

            // Candidate Training
            query = @"SELECT COUNT(*) TrainingCount
                            FROM CA_Training tr
                            JOIN CA_Candidate can on tr.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and tr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and tr.CandidateId ='{searchModel.CandidateId}'";
            }

            var TrainingCount = await ExecuteScalarAsync(query);

            // Candidate Training Progress
            if (TrainingCount >= 4)
            {
                progressPercentage += 12;
            }
            else if (TrainingCount >= 3)
            {
                progressPercentage += 9;
            }
            else if (TrainingCount >= 2)
            {
                progressPercentage += 6;
            }
            else if (TrainingCount >= 1)
            {
                progressPercentage += 3;
            }

            // Candidate Skills
            query = @"SELECT COUNT(*) as SkillCount
                            FROM CA_CandidateSkills cs
                            JOIN CA_Candidate can on cs.CandidateId = can.RowId
                            JOIN CA_Proficiency pf on cs.ProficiencyId = pf.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cs.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cs.CandidateId ='{searchModel.CandidateId}'";
            }

            var SkillCount = await ExecuteScalarAsync(query);

            // Candidate Skills Progress
            if (SkillCount >= 4)
            {
                progressPercentage += 8;
            }
            else if (SkillCount >= 3)
            {
                progressPercentage += 6;
            }
            else if (SkillCount >= 2)
            {
                progressPercentage += 4;
            }
            else if (SkillCount >= 1)
            {
                progressPercentage += 2;
            }

            // Candidate Profile
            query = @"SELECT cpi.RowId as RowId, CandidateProfileImageId, cpi.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ImageUrl, Base64Image, cpi.IsActive as IsActive
                            FROM CA_CandidateProfileImage cpi
                            JOIN CA_Candidate can on cpi.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and cpi.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cpi.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cpi.CreatedDate";

            var candidateProfile = await QueryAsync<CandidateProfileImageInfo>(query);

            // Candidate Profile Progress
            if (candidateProfile != null)
            {
                if (candidateProfile.Count > 0)
                {
                    if (!string.IsNullOrEmpty(candidateProfile[0].ImageUrl) && !string.IsNullOrWhiteSpace(candidateProfile[0].ImageUrl))
                    {
                        progressPercentage += 5;
                    }
                }
            }

            // Candidate Social Networks
            query = @"SELECT COUNT(*) as SocialNetworkCount
                            FROM CA_SocialAccounts sa
                            JOIN CA_Candidate can on sa.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and sa.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and sa.CandidateId ='{searchModel.CandidateId}'";
            }

            var SocialNetworkCount = await ExecuteScalarAsync(query);

            // Candidate Social Networks Progress
            if (SocialNetworkCount >= 1)
            {
                progressPercentage += 5;
            }

            objProgressInfo.ProfileProgress = progressPercentage;

            return objProgressInfo;
        }
    }
}

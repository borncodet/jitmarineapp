﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ISocialAccountsRepository : IRepository<SocialAccounts>
    {
        Task<IPagedList<SocialAccountsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<SocialAccountsSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class SocialAccountsRepository : BaseRepository<SocialAccounts>, ISocialAccountsRepository
    {
        public SocialAccountsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<SocialAccountsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT sa.RowId as RowId, SocialAccountId, sa.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, sa.Facebooks as Facebooks, sa.Google as Google, sa.Twitter as Twitter, 
                                    sa.LinkedIn as LinkedIn, sa.Pinterest as Pinterest, sa.Instagram as Instagram, sa.Other as Other, 
                                    sa.IsActive as IsActive
                            FROM CA_SocialAccounts sa
                            JOIN CA_Candidate can on sa.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and sa.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and sa.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY sa.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<SocialAccountsInfo>(queries.query);
            return new DapperPagedList<SocialAccountsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<SocialAccountsSelectBoxDataViewModel> GetSelectBoxData()
        {
            SocialAccountsSelectBoxDataViewModel result = new SocialAccountsSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

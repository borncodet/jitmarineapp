﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IEducationQualificationRepository : IRepository<EducationQualification>
    {
        Task<IPagedList<EducationQualificationInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<EducationQualificationSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class EducationQualificationRepository : BaseRepository<EducationQualification>, IEducationQualificationRepository
    {
        public EducationQualificationRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<EducationQualificationInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT eq.RowId as RowId, EducationQualificationId, eq.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, Course, University, 
                                    Grade, DateFrom, DateTo, DigiDocumentDetailId, eq.IsActive as IsActive
                            FROM CA_EducationQualification eq
                            JOIN CA_Candidate can on eq.CandidateId = can.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and eq.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and eq.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY Course";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<EducationQualificationInfo>(queries.query);
            return new DapperPagedList<EducationQualificationInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<EducationQualificationSelectBoxDataViewModel> GetSelectBoxData()
        {
            EducationQualificationSelectBoxDataViewModel result = new EducationQualificationSelectBoxDataViewModel
            {
                Courses = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Courses where IsActive=1")
            };
            return result;
        }

    }
}

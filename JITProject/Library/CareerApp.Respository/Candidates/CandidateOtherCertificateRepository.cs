﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateOtherCertificateRepository : IRepository<CandidateOtherCertificate>
    {
        Task<IPagedList<CandidateOtherCertificateInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateOtherCertificateSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateOtherCertificateRepository : BaseRepository<CandidateOtherCertificate>, ICandidateOtherCertificateRepository
    {
        public CandidateOtherCertificateRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateOtherCertificateInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT coc.RowId as RowId, CandidateOtherCertificateId, coc.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, coc.TrainingId as TrainingId, DocumentName,
                                     DocumentNumber, ExpiryDate, ReminderOnExpiryFlag, coc.Certificate as Certificate, coc.IsActive as IsActive
                            FROM CA_CandidateOtherCertificate coc
                            JOIN CA_Candidate can on coc.CandidateId = can.RowId
                            JOIN CA_Training tr on coc.TrainingId = tr.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and coc.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and coc.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY  coc.CandidateId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateOtherCertificateInfo>(queries.query);
            return new DapperPagedList<CandidateOtherCertificateInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateOtherCertificateSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateOtherCertificateSelectBoxDataViewModel result = new CandidateOtherCertificateSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

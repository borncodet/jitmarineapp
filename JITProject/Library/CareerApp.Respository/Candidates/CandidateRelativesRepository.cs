﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateRelativesRepository : IRepository<CandidateRelatives>
    {
        Task<IPagedList<CandidateRelativesInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateRelativesSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateRelativesRepository : BaseRepository<CandidateRelatives>, ICandidateRelativesRepository
    {
        public CandidateRelativesRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateRelativesInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT cr.RowId as RowId, CandidateRelativeId, cr.CandidateId as CandidateId,
                                    can.FirstName CandidateFirstName, can.LastName CandidateLastName, cr.NamePrefixId as NamePrefixId, np.Title as NamePrefixTitle,
                                    cr.FirstName RelativeFirstName,cr.LastName RelativeLastName, cr.GenderId as GenderId, gn.Title as GenderTitle, cr.Email as Email, 
                                    cr.CountryCode as CountryCode, cr.PhoneNumber as PhoneNumber, cr.TelephoneNumber as TelephoneNumber, cr.RelationshipId as RelationshipId, rs.Title as RelationshipTitle, 
                                    cr.IsActive as IsActive
                            FROM CA_CandidateRelatives cr
                            JOIN CA_Candidate can on cr.CandidateId = can.RowId
                            LEFT JOIN CA_NamePrefix np on cr.NamePrefixId = np.RowId
                            LEFT JOIN CA_Gender gn on cr.GenderId = gn.RowId
                            LEFT JOIN CA_Relationship rs on cr.RelationshipId = rs.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cr.FirstName";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateRelativesInfo>(queries.query);
            return new DapperPagedList<CandidateRelativesInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateRelativesSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateRelativesSelectBoxDataViewModel result = new CandidateRelativesSelectBoxDataViewModel
            {
                NamePrefix = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_NamePrefix where IsActive=1"),
                Genders = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Gender where IsActive=1"),
                Relationships = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Relationship where IsActive=1")
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateLanguageMapRepository : IRepository<CandidateLanguageMap>
    {
        Task<IPagedList<CandidateLanguageMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateLanguageMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateLanguageMapRepository : BaseRepository<CandidateLanguageMap>, ICandidateLanguageMapRepository
    {
        public CandidateLanguageMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateLanguageMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT clm.RowId as RowId, CandidateLanguageMapId, clm.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, clm.LanguageId as LanguageId, lan.Name as LanguageName, clm.IsActive as IsActive
                            FROM CA_CandidateLanguageMap clm
                            JOIN CA_Candidate can on clm.CandidateId = can.RowId
                            JOIN CA_Languages lan on clm.LanguageId = lan.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and clm.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and clm.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY lan.Name";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateLanguageMapInfo>(queries.query);
            return new DapperPagedList<CandidateLanguageMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateLanguageMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateLanguageMapSelectBoxDataViewModel result = new CandidateLanguageMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

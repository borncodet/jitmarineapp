﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IProjectRoleRepository : IRepository<ProjectRole>
    {
        Task<IPagedList<ProjectRoleInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<ProjectRoleSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class ProjectRoleRepository : BaseRepository<ProjectRole>, IProjectRoleRepository
    {
        public ProjectRoleRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<ProjectRoleInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT pr.RowId as RowId, ProjectRoleId, cp.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, pr.CandidateProjectId as CandidateProjectId, cp.ProjectName as ProjectName, 
                                    pr.JobRoleId as JobRoleId, jr.Title as JobRoleTitle, pr.IsActive as IsActive
                            FROM CA_ProjectRole pr
                            JOIN CA_CandidateProjects cp on pr.CandidateProjectId = cp.RowId
                            JOIN CA_Candidate can on cp.CandidateId = can.RowId
                            JOIN CA_JobRoles jr on pr.JobRoleId = jr.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and pr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cp.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY pr.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ProjectRoleInfo>(queries.query);
            return new DapperPagedList<ProjectRoleInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<ProjectRoleSelectBoxDataViewModel> GetSelectBoxData()
        {
            ProjectRoleSelectBoxDataViewModel result = new ProjectRoleSelectBoxDataViewModel
            {
                JobRoles = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_JobRoles where IsActive=1")
            };
            return result;
        }
    }
}

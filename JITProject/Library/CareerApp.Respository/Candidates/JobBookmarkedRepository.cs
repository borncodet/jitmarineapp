﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobBookmarkedRepository : IRepository<JobBookmarked>
    {
        Task<IPagedList<JobBookmarkedInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<IPagedList<RecentlySavedJobInfo>> GetRecentlySavedJobAsync(int CandidateId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<int> GetJobSavedCountAsync(BaseCandidateSearchViewModel searchModel);
        Task<JobBookmarkedSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobBookmarkedRepository : BaseRepository<JobBookmarked>, IJobBookmarkedRepository
    {
        public JobBookmarkedRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobBookmarkedInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT jb.RowId as RowId, JobBookmarkedId, jb.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, jb.JobId as JobId, jl.Title as JobTitle, CAST(1 AS BIT) AS IsBookmarked, jb.IsActive as IsActive
                            FROM CA_JobBookmarked jb
                            JOIN CA_Candidate can on jb.CandidateId = can.RowId
                            JOIN CA_JobLists jl on jb.JobId = jl.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and jb.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY jb.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobBookmarkedInfo>(queries.query);
            return new DapperPagedList<JobBookmarkedInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<IPagedList<RecentlySavedJobInfo>> GetRecentlySavedJobAsync(int CandidateId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = $@"SELECT ja.RowId as RowId, ja.JobId as JobId,JobBookmarkedId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, ja.CreatedDate as PostedDate, 
                            DATEDIFF(d, ja.CreatedDate, '{DateTime.Now.Date}') as DaysAgo,
                            CAST(1 AS BIT) AS IsBookmarked, 
                            CAST(CASE WHEN jad.RowId >0 and jad.IsActive = 1 and jad.CandidateId = '{CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsApplied,
                            jb.IsActive as IsActive
                            FROM CA_JobBookmarked ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId                        
                            JOIN CA_JobLists jb on ja.JobId = jb.RowId
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobAppliedDetails jad on jb.RowId = jad.JobId and jad.CandidateId = '{CandidateId}'
                            WHERE can.RowId = '{CandidateId}' and ja.IsActive=1";

            query += " ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<RecentlySavedJobInfo>(queries.query);
            return new DapperPagedList<RecentlySavedJobInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<int> GetJobSavedCountAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = $@"SELECT COUNT(*) as JobSavedCount
                              FROM CA_JobBookmarked jb
                              JOIN CA_Candidate can on jb.CandidateId = can.RowId
                              JOIN CA_JobLists jl on jb.JobId = jl.RowId
                              WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and jb.CandidateId ='{searchModel.CandidateId}'";
            }

            var count = await ExecuteScalarAsync(query);
            return count;
        }

        public async Task<JobBookmarkedSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobBookmarkedSelectBoxDataViewModel result = new JobBookmarkedSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

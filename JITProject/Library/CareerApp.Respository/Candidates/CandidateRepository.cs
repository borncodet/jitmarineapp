﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateRepository : IRepository<Candidate>
    {
        Task<IPagedList<CandidateInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<int> GetCandidateIdAsync(int userId);
        Task<CandidateSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateRepository : BaseRepository<Candidate>, ICandidateRepository
    {
        public CandidateRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT can.RowId as RowId, CandidateId, can.NamePrefixId as NamePrefixId, np.Title as NamePrefixTitle, FirstName, LastName, 
                                    can.GenderId as GenderId, gen.Title as GenderTitle, can.MartialStatusId as MartialStatusId, ms.Title as MartialStatusTitle, 
                                    can.Email as Email, can.PhoneNumber as PhoneNumber, can.CountryCode as CountryCode, can.TelephoneNumber as TelephoneNumber, can.PassportInformationId as PassportInformationId, 
                                    pi.PassportNo as PassportNo, can.SeamanBookCdcId as SeamanBookCdcId, sbc.CdcNumber as CdcNumber, can.LicenceInformationId as LicenceInformationId,
                                    li.LicenceNo as LicenceNo, PanNo, AadharNo, ResidenceId, PanDigiDocumentDetailId , AadharDigiDocumentDetailId, ResidenceDigiDocumentDetailId, 
                                    can.JobCategoryId as CategoryId, jc.Title as CategoryName, can.DesignationId as DesignationId, des.Title as DesignationName, 
                                    can.JobTypeId as JobTypeId, jt.Title as JobTypeName, CurrentCTC, ExpectedPackage, can.IndustryId as IndustryId, 
                                    ind.Title as IndustryName, can.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalAreaName, 
                                    can.JobRoleId as JobRoleId, jr.Title as JobRoleName, Skills, ProfileSummary, PositionApplyingFor, Dob, CurrentAddress1, CurrentAddress2, 
                                    PermanantAddress1, PermanantAddress2, City, can.StateId as StateId, st.Name as StateName, 
                                    can.CountryId as CountryId, cn.Name as CountryName, ZipCode, UserId, can.IsActive as IsActive
                            FROM CA_Candidate can
                            LEFT JOIN CA_NamePrefix np on can.NamePrefixId = np.RowId
                            LEFT JOIN CA_Gender gen on can.GenderId = gen.RowId
                            LEFT JOIN CA_MartialStatus ms on can.MartialStatusId = ms.RowId
                            LEFT JOIN CA_LicenceInformation li on can.LicenceInformationId = li.RowId
                            LEFT JOIN CA_PassportInformation pi on can.PassportInformationId = pi.RowId
                            LEFT JOIN CA_SeamanBookCdc sbc on can.SeamanBookCdcId = sbc.RowId
                            LEFT JOIN CA_JobCategories jc on can.JobCategoryId = jc.RowId
                            LEFT JOIN CA_Designation des on can.DesignationId = des.RowId
                            LEFT JOIN CA_JobTypes jt on can.JobTypeId = jt.RowId
                            LEFT JOIN CA_Industries ind on can.IndustryId = ind.RowId
                            LEFT JOIN CA_FunctionalAreas fa on can.FunctionalAreaId = fa.RowId
                            LEFT JOIN CA_JobRoles jr on can.JobRoleId = jr.RowId
                            LEFT JOIN CA_States st on can.StateId = st.RowId
                            LEFT JOIN CA_Countries cn on can.StateId = cn.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and can.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and can.RowId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY can.CandidateId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateInfo>(queries.query);
            return new DapperPagedList<CandidateInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<int> GetCandidateIdAsync(int userId)
        {
            string query = $@"SELECT RowId
                              FROM CA_Candidate
                              WHERE UserId = '{userId}'";

            int count = await ExecuteScalarAsync(query);
            return count;
        }

        public async Task<CandidateSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateSelectBoxDataViewModel result = new CandidateSelectBoxDataViewModel
            {
                NamePrefix = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_NamePrefix where IsActive=1"),
                Genders = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_Gender where IsActive=1"),
                MartialStatus = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_MartialStatus where IsActive=1"),
                JobCategories = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_JobCategories where IsActive=1"),
                Designations = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_Designation where IsActive=1"),
                JobTypes = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_JobTypes where IsActive=1"),
                Industries = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_Industries where IsActive=1"),
                FunctionalAreas = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_FunctionalAreas where IsActive=1"),
                JobRoles = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Title as Caption From CA_JobRoles where IsActive=1"),
                States = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Name as Caption From CA_States where IsActive=1"),
                Countries = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Name as Caption From CA_Countries where IsActive=1"),
                Languages = await QueryAsync<ValueCaptionPair>("Select RowId as Value, Name as Caption From CA_Languages where IsActive=1"),
                CountryCode = await QueryAsync<ValueCaptionPair>("Select CountryCode as Value, CountryCode as Caption From CA_Countries where IsActive=1")
            };
            return result;
        }

    }
}

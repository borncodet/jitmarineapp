﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ITrainingDurationMapRepository : IRepository<TrainingDurationMap>
    {
        Task<IPagedList<TrainingDurationMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<TrainingDurationMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class TrainingDurationMapRepository : BaseRepository<TrainingDurationMap>, ITrainingDurationMapRepository
    {
        public TrainingDurationMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<TrainingDurationMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT tdm.RowId as RowId, TrainingDurationMapId, tdm.TrainingId as TrainingId, tr.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, tr.CourseId as CourseId, cs.Title as CourseTitle, tr.IssuedBy as IssuedBy,
                                     tdm.DurationId as DurationId, dr.Day as Day, dr.Month as Month, dr.Year as Year, tdm.IsActive as IsActive
                            FROM CA_TrainingDurationMap tdm
                            JOIN CA_Training tr on tdm.TrainingId = tr.RowId
                            JOIN CA_Durations dr on tdm.DurationId = dr.RowId
                            JOIN CA_Candidate can on tr.CandidateId = can.RowId
                            JOIN CA_Courses cs on tr.CourseId = cs.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and tdm.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and tr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cs.Title";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<TrainingDurationMapInfo>(queries.query);
            return new DapperPagedList<TrainingDurationMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<TrainingDurationMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            TrainingDurationMapSelectBoxDataViewModel result = new TrainingDurationMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

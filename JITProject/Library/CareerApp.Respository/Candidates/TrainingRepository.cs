﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ITrainingRepository : IRepository<Training>
    {
        Task<IPagedList<TrainingInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<TrainingSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class TrainingRepository : BaseRepository<Training>, ITrainingRepository
    {
        public TrainingRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<TrainingInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT tr.RowId as RowId, TrainingId, tr.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, TrainingCertificate, Institute, DigiDocumentDetailId, tr.IsActive as IsActive
                            FROM CA_Training tr
                            JOIN CA_Candidate can on tr.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and tr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and tr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY tr.CandidateId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<TrainingInfo>(queries.query);
            return new DapperPagedList<TrainingInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<TrainingSelectBoxDataViewModel> GetSelectBoxData()
        {
            TrainingSelectBoxDataViewModel result = new TrainingSelectBoxDataViewModel
            {
                Courses = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Courses where IsActive=1")
            };
            return result;
        }

    }
}

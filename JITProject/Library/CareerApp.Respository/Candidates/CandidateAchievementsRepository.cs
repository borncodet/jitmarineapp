﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateAchievementsRepository : IRepository<CandidateAchievements>
    {
        Task<IPagedList<CandidateAchievementsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateAchievementsSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateAchievementsRepository : BaseRepository<CandidateAchievements>, ICandidateAchievementsRepository
    {
        public CandidateAchievementsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateAchievementsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT ca.RowId as RowId, CandidateAchievementId, ce.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ca.CandidateExperienceId as CandidateExperienceId, ce.EmployerName as EmployerName, ca.Title as Title, ca.Description as Description, ca.IsActive as IsActive
                            FROM CA_CandidateAchievements ca
                            JOIN CA_CandidateExperience ce on ca.CandidateExperienceId = ce.RowId
                            JOIN CA_Candidate can on ce.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ca.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ce.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ce.EmployerName";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateAchievementsInfo>(queries.query);
            return new DapperPagedList<CandidateAchievementsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateAchievementsSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateAchievementsSelectBoxDataViewModel result = new CandidateAchievementsSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

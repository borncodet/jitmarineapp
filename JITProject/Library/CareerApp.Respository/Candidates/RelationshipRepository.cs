﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IRelationshipRepository : IRepository<Relationship>
    {
        Task<IPagedList<RelationshipInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<RelationshipSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class RelationshipRepository : BaseRepository<Relationship>, IRelationshipRepository
    {
        public RelationshipRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<RelationshipInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT rel.RowId as RowId, RelationshipId, Title, Description, rel.IsActive as IsActive
                             FROM CA_Relationship rel
                             WHERE rel.IsActive=1
                             ORDER BY Title";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<RelationshipInfo>(queries.query);
            return new DapperPagedList<RelationshipInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<RelationshipSelectBoxDataViewModel> GetSelectBoxData()
        {
            RelationshipSelectBoxDataViewModel result = new RelationshipSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobSharedRepository : IRepository<JobShared>
    {
        Task<IPagedList<JobSharedInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<JobSharedSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobSharedRepository : BaseRepository<JobShared>, IJobSharedRepository
    {
        public JobSharedRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobSharedInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT js.RowId as RowId, JobSharedId, js.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, js.JobId as JobId, jl.Title as JobTitle, SharedThrough, js.IsActive as IsActive
                            FROM CA_JobShared js
                            JOIN CA_Candidate can on js.CandidateId = can.RowId
                            JOIN CA_JobLists jl on js.JobId = jl.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and js.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and js.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY js.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobSharedInfo>(queries.query);
            return new DapperPagedList<JobSharedInfo>(data, searchModel.Page, searchModel.PageSize);
        }

        public async Task<JobSharedSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobSharedSelectBoxDataViewModel result = new JobSharedSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateProfileImageRepository : IRepository<CandidateProfileImage>
    {
        Task<IPagedList<CandidateProfileImageInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateProfileImageSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateProfileImageRepository : BaseRepository<CandidateProfileImage>, ICandidateProfileImageRepository
    {
        public CandidateProfileImageRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateProfileImageInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT cpi.RowId as RowId, CandidateProfileImageId, cpi.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ImageUrl, Base64Image, cpi.IsActive as IsActive
                            FROM CA_CandidateProfileImage cpi
                            JOIN CA_Candidate can on cpi.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and cpi.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cpi.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cpi.CreatedDate";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateProfileImageInfo>(queries.query);
            return new DapperPagedList<CandidateProfileImageInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateProfileImageSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateProfileImageSelectBoxDataViewModel result = new CandidateProfileImageSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

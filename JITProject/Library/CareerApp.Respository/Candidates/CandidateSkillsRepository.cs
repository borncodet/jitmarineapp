﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ICandidateSkillsRepository : IRepository<CandidateSkills>
    {
        Task<IPagedList<CandidateSkillsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<CandidateSkillsSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class CandidateSkillsRepository : BaseRepository<CandidateSkills>, ICandidateSkillsRepository
    {
        public CandidateSkillsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<CandidateSkillsInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT cs.RowId as RowId, CandidateSkillId, cs.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, cs.SkillName as SkillName, cs.ProficiencyId as ProficiencyId, 
                                    pf.Title as ProficiencyTitle, cs.IsActive as IsActive
                            FROM CA_CandidateSkills cs
                            JOIN CA_Candidate can on cs.CandidateId = can.RowId
                            JOIN CA_Proficiency pf on cs.ProficiencyId = pf.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cs.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cs.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cs.SkillName";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<CandidateSkillsInfo>(queries.query);
            return new DapperPagedList<CandidateSkillsInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<CandidateSkillsSelectBoxDataViewModel> GetSelectBoxData()
        {
            CandidateSkillsSelectBoxDataViewModel result = new CandidateSkillsSelectBoxDataViewModel
            {
                Proficiencies = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_Proficiency where IsActive=1")
            };
            return result;
        }

    }
}

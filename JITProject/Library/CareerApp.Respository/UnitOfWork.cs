﻿using CareerApp.Core.DB;
using CareerApp.Respository.DigiLocker;
using CareerApp.Respository.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public ApplicationDbContext Context { get; }
        private readonly IConfiguration _config;
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public UnitOfWork(IConfiguration config, ApplicationDbContext context)
        {
            Context = context;
            _config = config;
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }

        private ICountryRepository _countries;
        public ICountryRepository Countries
        {
            get
            {
                if (_countries == null)
                {
                    _countries = new CountryRepository(_config, Context);
                }
                return _countries;
            }
        }

        private ICurrencyRepository _currencies;
        public ICurrencyRepository Currencies
        {
            get
            {
                if (_currencies == null)
                {
                    _currencies = new CurrencyRepository(_config, Context);
                }
                return _currencies;
            }
        }

        private ICurrencyValueRepository _currencyValues;
        public ICurrencyValueRepository CurrencyValues
        {
            get
            {
                if (_currencyValues == null)
                {
                    _currencyValues = new CurrencyValueRepository(_config, Context);
                }
                return _currencyValues;
            }
        }

        private IEmployerRepository _employers;
        public IEmployerRepository Employers
        {
            get
            {
                if (_employers == null)
                {
                    _employers = new EmployerRepository(_config, Context);
                }
                return _employers;
            }
        }

        private IVendorRepository _vendors;
        public IVendorRepository Vendors
        {
            get
            {
                if (_vendors == null)
                {
                    _vendors = new VendorRepository(_config, Context);
                }
                return _vendors;
            }
        }

        public ILanguageRepository _languages;
        public ILanguageRepository Languages
        {
            get
            {
                if (_languages == null)
                {
                    _languages = new LanguageRepository(_config, Context);
                }
                return _languages;
            }
        }

        public IStateRepository _states;
        public IStateRepository States
        {
            get
            {
                if (_states == null)
                {
                    _states = new StateRepository(_config, Context);
                }
                return _states;
            }
        }

        public ICourseRepository _courses;
        public ICourseRepository Courses
        {
            get
            {
                if (_courses == null)
                {
                    _courses = new CourseRepository(_config, Context);
                }
                return _courses;
            }
        }

        public IExpereinceTypeRepository _expereinceTypes;
        public IExpereinceTypeRepository ExpereinceTypes
        {
            get
            {
                if (_expereinceTypes == null)
                {
                    _expereinceTypes = new ExpereinceTypeRepository(_config, Context);
                }
                return _expereinceTypes;
            }
        }

        public IFunctionalAreaRepository _functionalAreas;
        public IFunctionalAreaRepository FunctionalAreas
        {
            get
            {
                if (_functionalAreas == null)
                {
                    _functionalAreas = new FunctionalAreaRepository(_config, Context);
                }
                return _functionalAreas;
            }
        }

        public IIndustryRepository _industries;
        public IIndustryRepository Industries
        {
            get
            {
                if (_industries == null)
                {
                    _industries = new IndustryRepository(_config, Context);
                }
                return _industries;
            }
        }

        public IJobByAdminRepository _jobByAdmin;
        public IJobByAdminRepository JobByAdmin
        {
            get
            {
                if (_jobByAdmin == null)
                {
                    _jobByAdmin = new JobByAdminRepository(_config, Context);
                }
                return _jobByAdmin;
            }
        }

        public IJobByVendorRepository _jobByVendors;
        public IJobByVendorRepository JobByVendors
        {
            get
            {
                if (_jobByVendors == null)
                {
                    _jobByVendors = new JobByVendorRepository(_config, Context);
                }
                return _jobByVendors;
            }
        }

        public IJobCategoryRepository _jobCategories;
        public IJobCategoryRepository JobCategories
        {
            get
            {
                if (_jobCategories == null)
                {
                    _jobCategories = new JobCategoryRepository(_config, Context);
                }
                return _jobCategories;
            }
        }

        public IJobCourseMapRepository _jobCourseMap;
        public IJobCourseMapRepository JobCourseMap
        {
            get
            {
                if (_jobCourseMap == null)
                {
                    _jobCourseMap = new JobCourseMapRepository(_config, Context);
                }
                return _jobCourseMap;
            }
        }

        public IJobLanguageMapRepository _jobLanguageMap;
        public IJobLanguageMapRepository JobLanguageMap
        {
            get
            {
                if (_jobLanguageMap == null)
                {
                    _jobLanguageMap = new JobLanguageMapRepository(_config, Context);
                }
                return _jobLanguageMap;
            }
        }

        public IJobListRepository _jobLists;
        public IJobListRepository JobLists
        {
            get
            {
                if (_jobLists == null)
                {
                    _jobLists = new JobListRepository(_config, Context);
                }
                return _jobLists;
            }
        }

        public IVendorJobListRepository _vendorJobLists;
        public IVendorJobListRepository VendorJobLists
        {
            get
            {
                if (_vendorJobLists == null)
                {
                    _vendorJobLists = new VendorJobListRepository(_config, Context);
                }
                return _vendorJobLists;
            }
        }

        public IJobRequiredDocMapRepository _jobRequiredDocMap;
        public IJobRequiredDocMapRepository JobRequiredDocMap
        {
            get
            {
                if (_jobRequiredDocMap == null)
                {
                    _jobRequiredDocMap = new JobRequiredDocMapRepository(_config, Context);
                }
                return _jobRequiredDocMap;
            }
        }

        public IJobShiftMapRepository _jobShiftMap;
        public IJobShiftMapRepository JobShiftMap
        {
            get
            {
                if (_jobShiftMap == null)
                {
                    _jobShiftMap = new JobShiftMapRepository(_config, Context);
                }
                return _jobShiftMap;
            }
        }

        public IJobTypeRepository _jobTypes;
        public IJobTypeRepository JobTypes
        {
            get
            {
                if (_jobTypes == null)
                {
                    _jobTypes = new JobTypeRepository(_config, Context);
                }
                return _jobTypes;
            }
        }

        public IRequiredDocumentTypeRepository _requiredDocumentTypes;
        public IRequiredDocumentTypeRepository RequiredDocumentTypes
        {
            get
            {
                if (_requiredDocumentTypes == null)
                {
                    _requiredDocumentTypes = new RequiredDocumentTypeRepository(_config, Context);
                }
                return _requiredDocumentTypes;
            }
        }

        public IShiftAvailableRepository _shiftAvailable;
        public IShiftAvailableRepository ShiftAvailable
        {
            get
            {
                if (_shiftAvailable == null)
                {
                    _shiftAvailable = new ShiftAvailableRepository(_config, Context);
                }
                return _shiftAvailable;
            }
        }

        public IVendorDocumentRepository _vendorDocuments;
        public IVendorDocumentRepository VendorDocuments
        {
            get
            {
                if (_vendorDocuments == null)
                {
                    _vendorDocuments = new VendorDocumentRepository(_config, Context);
                }
                return _vendorDocuments;
            }
        }

        public IVendorsStatusRepository _vendorsStatus;
        public IVendorsStatusRepository VendorsStatus
        {
            get
            {
                if (_vendorsStatus == null)
                {
                    _vendorsStatus = new VendorsStatusRepository(_config, Context);
                }
                return _vendorsStatus;
            }
        }

        public ICandidateRepository _candidates;
        public ICandidateRepository Candidates
        {
            get
            {
                if (_candidates == null)
                {
                    _candidates = new CandidateRepository(_config, Context);
                }
                return _candidates;
            }
        }

        public IPassportInformationRepository _passportInformations;
        public IPassportInformationRepository PassportInformations
        {
            get
            {
                if (_passportInformations == null)
                {
                    _passportInformations = new PassportInformationRepository(_config, Context);
                }
                return _passportInformations;
            }
        }

        public ISeamanBookCdcRepository _seamanBookCdcs;
        public ISeamanBookCdcRepository SeamanBookCdcs
        {
            get
            {
                if (_seamanBookCdcs == null)
                {
                    _seamanBookCdcs = new SeamanBookCdcRepository(_config, Context);
                }
                return _seamanBookCdcs;
            }
        }

        public ILicenceInformationRepository _licenceInformations;
        public ILicenceInformationRepository LicenceInformations
        {
            get
            {
                if (_licenceInformations == null)
                {
                    _licenceInformations = new LicenceInformationRepository(_config, Context);
                }
                return _licenceInformations;
            }
        }

        public IDesignationRepository _designations;
        public IDesignationRepository Designations
        {
            get
            {
                if (_designations == null)
                {
                    _designations = new DesignationRepository(_config, Context);
                }
                return _designations;
            }
        }

        public IGenderRepository _genders;
        public IGenderRepository Genders
        {
            get
            {
                if (_genders == null)
                {
                    _genders = new GenderRepository(_config, Context);
                }
                return _genders;
            }
        }

        public IMartialStatusRepository _martialStatus;
        public IMartialStatusRepository MartialStatus
        {
            get
            {
                if (_martialStatus == null)
                {
                    _martialStatus = new MartialStatusRepository(_config, Context);
                }
                return _martialStatus;
            }
        }

        public INamePrefixRepository _namePrefix;
        public INamePrefixRepository NamePrefix
        {
            get
            {
                if (_namePrefix == null)
                {
                    _namePrefix = new NamePrefixRepository(_config, Context);
                }
                return _namePrefix;
            }
        }

        public IMediaRepository _medias;
        public IMediaRepository Medias
        {
            get
            {
                if (_medias == null)
                {
                    _medias = new MediaRepository(_config, Context);
                }
                return _medias;
            }
        }

        public IJobRoleRepository _jobRoles;
        public IJobRoleRepository JobRoles
        {
            get
            {
                if (_jobRoles == null)
                {
                    _jobRoles = new JobRoleRepository(_config, Context);
                }
                return _jobRoles;
            }
        }

        public IJobAppliedRepository _jobApplied;
        public IJobAppliedRepository JobApplied
        {
            get
            {
                if (_jobApplied == null)
                {
                    _jobApplied = new JobAppliedRepository(_config, Context);
                }
                return _jobApplied;
            }
        }

        public IJobAppliedDetailsRepository _jobAppliedDetails;
        public IJobAppliedDetailsRepository JobAppliedDetails
        {
            get
            {
                if (_jobAppliedDetails == null)
                {
                    _jobAppliedDetails = new JobAppliedDetailsRepository(_config, Context);
                }
                return _jobAppliedDetails;
            }
        }

        public IJobAppliedDigiDocMapRepository _jobAppliedDigiDocMap;
        public IJobAppliedDigiDocMapRepository JobAppliedDigiDocMap
        {
            get
            {
                if (_jobAppliedDigiDocMap == null)
                {
                    _jobAppliedDigiDocMap = new JobAppliedDigiDocMapRepository(_config, Context);
                }
                return _jobAppliedDigiDocMap;
            }
        }

        public ICandidateLanguageMapRepository _candidateLanguageMap;
        public ICandidateLanguageMapRepository CandidateLanguageMap
        {
            get
            {
                if (_candidateLanguageMap == null)
                {
                    _candidateLanguageMap = new CandidateLanguageMapRepository(_config, Context);
                }
                return _candidateLanguageMap;
            }
        }

        public IBankAccountTypeRepository _bankAccountTypes;
        public IBankAccountTypeRepository BankAccountTypes
        {
            get
            {
                if (_bankAccountTypes == null)
                {
                    _bankAccountTypes = new BankAccountTypeRepository(_config, Context);
                }
                return _bankAccountTypes;
            }
        }

        public IBankDetailsRepository _bankDetails;
        public IBankDetailsRepository BankDetails
        {
            get
            {
                if (_bankDetails == null)
                {
                    _bankDetails = new BankDetailsRepository(_config, Context);
                }
                return _bankDetails;
            }
        }

        public ICandidateAchievementsRepository _candidateAchievements;
        public ICandidateAchievementsRepository CandidateAchievements
        {
            get
            {
                if (_candidateAchievements == null)
                {
                    _candidateAchievements = new CandidateAchievementsRepository(_config, Context);
                }
                return _candidateAchievements;
            }
        }

        public ICandidateEducationCertificateRepository _candidateEducationCertificates;
        public ICandidateEducationCertificateRepository CandidateEducationCertificates
        {
            get
            {
                if (_candidateEducationCertificates == null)
                {
                    _candidateEducationCertificates = new CandidateEducationCertificateRepository(_config, Context);
                }
                return _candidateEducationCertificates;
            }
        }

        public ICandidateExperienceRepository _candidateExperiences;
        public ICandidateExperienceRepository CandidateExperiences
        {
            get
            {
                if (_candidateExperiences == null)
                {
                    _candidateExperiences = new CandidateExperienceRepository(_config, Context);
                }
                return _candidateExperiences;
            }
        }

        public ICandidateProfileImageRepository _candidateProfileImage;
        public ICandidateProfileImageRepository CandidateProfileImage
        {
            get
            {
                if (_candidateProfileImage == null)
                {
                    _candidateProfileImage = new CandidateProfileImageRepository(_config, Context);
                }
                return _candidateProfileImage;
            }
        }

        public ICandidateOtherCertificateRepository _candidateOtherCertificates;
        public ICandidateOtherCertificateRepository CandidateOtherCertificates
        {
            get
            {
                if (_candidateOtherCertificates == null)
                {
                    _candidateOtherCertificates = new CandidateOtherCertificateRepository(_config, Context);
                }
                return _candidateOtherCertificates;
            }
        }

        public ICandidateProjectsRepository _candidateProjects;
        public ICandidateProjectsRepository CandidateProjects
        {
            get
            {
                if (_candidateProjects == null)
                {
                    _candidateProjects = new CandidateProjectsRepository(_config, Context);
                }
                return _candidateProjects;
            }
        }

        public ICandidateRelativesRepository _candidateRelatives;
        public ICandidateRelativesRepository CandidateRelatives
        {
            get
            {
                if (_candidateRelatives == null)
                {
                    _candidateRelatives = new CandidateRelativesRepository(_config, Context);
                }
                return _candidateRelatives;
            }
        }

        public ICandidateReferencesRepository _candidateReferences;
        public ICandidateReferencesRepository CandidateReferences
        {
            get
            {
                if (_candidateReferences == null)
                {
                    _candidateReferences = new CandidateReferencesRepository(_config, Context);
                }
                return _candidateReferences;
            }
        }

        public ICandidateSkillsRepository _candidateSkills;
        public ICandidateSkillsRepository CandidateSkills
        {
            get
            {
                if (_candidateSkills == null)
                {
                    _candidateSkills = new CandidateSkillsRepository(_config, Context);
                }
                return _candidateSkills;
            }
        }

        public IEducationQualificationRepository _educationQualifications;
        public IEducationQualificationRepository EducationQualifications
        {
            get
            {
                if (_educationQualifications == null)
                {
                    _educationQualifications = new EducationQualificationRepository(_config, Context);
                }
                return _educationQualifications;
            }
        }

        public IProjectRoleRepository _projectRoles;
        public IProjectRoleRepository ProjectRoles
        {
            get
            {
                if (_projectRoles == null)
                {
                    _projectRoles = new ProjectRoleRepository(_config, Context);
                }
                return _projectRoles;
            }
        }

        public IRelationshipRepository _relationships;
        public IRelationshipRepository Relationships
        {
            get
            {
                if (_relationships == null)
                {
                    _relationships = new RelationshipRepository(_config, Context);
                }
                return _relationships;
            }
        }

        public ISocialAccountsRepository _socialAccounts;
        public ISocialAccountsRepository SocialAccounts
        {
            get
            {
                if (_socialAccounts == null)
                {
                    _socialAccounts = new SocialAccountsRepository(_config, Context);
                }
                return _socialAccounts;
            }
        }

        public ITrainingRepository _trainings;
        public ITrainingRepository Trainings
        {
            get
            {
                if (_trainings == null)
                {
                    _trainings = new TrainingRepository(_config, Context);
                }
                return _trainings;
            }
        }

        public ITrainingDurationMapRepository _trainingDurationMaps;
        public ITrainingDurationMapRepository TrainingDurationMaps
        {
            get
            {
                if (_trainingDurationMaps == null)
                {
                    _trainingDurationMaps = new TrainingDurationMapRepository(_config, Context);
                }
                return _trainingDurationMaps;
            }
        }

        public ITrainingRenewalRepository _trainingRenewals;
        public ITrainingRenewalRepository TrainingRenewals
        {
            get
            {
                if (_trainingRenewals == null)
                {
                    _trainingRenewals = new TrainingRenewalRepository(_config, Context);
                }
                return _trainingRenewals;
            }
        }

        public IUniversityRepository _universities;
        public IUniversityRepository Universities
        {
            get
            {
                if (_universities == null)
                {
                    _universities = new UniversityRepository(_config, Context);
                }
                return _universities;
            }
        }

        public IDurationRepository _durations;
        public IDurationRepository Durations
        {
            get
            {
                if (_durations == null)
                {
                    _durations = new DurationRepository(_config, Context);
                }
                return _durations;
            }
        }

        public IProficiencyRepository _proficiencies;
        public IProficiencyRepository Proficiencies
        {
            get
            {
                if (_proficiencies == null)
                {
                    _proficiencies = new ProficiencyRepository(_config, Context);
                }
                return _proficiencies;
            }
        }

        public IJobBookmarkedRepository _jobBookmarked;
        public IJobBookmarkedRepository JobBookmarked
        {
            get
            {
                if (_jobBookmarked == null)
                {
                    _jobBookmarked = new JobBookmarkedRepository(_config, Context);
                }
                return _jobBookmarked;
            }
        }

        public IJobPostAlertRepository _jobPostAlert;
        public IJobPostAlertRepository JobPostAlert
        {
            get
            {
                if (_jobPostAlert == null)
                {
                    _jobPostAlert = new JobPostAlertRepository(_config, Context);
                }
                return _jobPostAlert;
            }
        }

        public IJobAlertTypeMapRepository _jobAlertTypeMap;
        public IJobAlertTypeMapRepository JobAlertTypeMap
        {
            get
            {
                if (_jobAlertTypeMap == null)
                {
                    _jobAlertTypeMap = new JobAlertTypeMapRepository(_config, Context);
                }
                return _jobAlertTypeMap;
            }
        }

        public IJobAlertRoleMapRepository _jobAlertRoleMap;
        public IJobAlertRoleMapRepository JobAlertRoleMap
        {
            get
            {
                if (_jobAlertRoleMap == null)
                {
                    _jobAlertRoleMap = new JobAlertRoleMapRepository(_config, Context);
                }
                return _jobAlertRoleMap;
            }
        }

        public IJobAlertIndustryMapRepository _jobAlertIndustryMap;
        public IJobAlertIndustryMapRepository JobAlertIndustryMap
        {
            get
            {
                if (_jobAlertIndustryMap == null)
                {
                    _jobAlertIndustryMap = new JobAlertIndustryMapRepository(_config, Context);
                }
                return _jobAlertIndustryMap;
            }
        }

        public IJobAlertExperienceMapRepository _jobAlertExperienceMap;
        public IJobAlertExperienceMapRepository JobAlertExperienceMap
        {
            get
            {
                if (_jobAlertExperienceMap == null)
                {
                    _jobAlertExperienceMap = new JobAlertExperienceMapRepository(_config, Context);
                }
                return _jobAlertExperienceMap;
            }
        }

        public IJobAlertCategoryMapRepository _jobAlertCategoryMap;
        public IJobAlertCategoryMapRepository JobAlertCategoryMap
        {
            get
            {
                if (_jobAlertCategoryMap == null)
                {
                    _jobAlertCategoryMap = new JobAlertCategoryMapRepository(_config, Context);
                }
                return _jobAlertCategoryMap;
            }
        }

        public IJobSharedRepository _jobShared;
        public IJobSharedRepository JobShared
        {
            get
            {
                if (_jobShared == null)
                {
                    _jobShared = new JobSharedRepository(_config, Context);
                }
                return _jobShared;
            }
        }

        public IDatePostedRepository _datePosted;
        public IDatePostedRepository DatePosted
        {
            get
            {
                if (_datePosted == null)
                {
                    _datePosted = new DatePostedRepository(_config, Context);
                }
                return _datePosted;
            }
        }

        public IDigiDocumentTypeRepository _digiDocumentTypes;
        public IDigiDocumentTypeRepository DigiDocumentTypes
        {
            get
            {
                if (_digiDocumentTypes == null)
                {
                    _digiDocumentTypes = new DigiDocumentTypeRepository(_config, Context);
                }
                return _digiDocumentTypes;
            }
        }
        public IDigiDocumentDetailsRepository _digiDocumentDetails;
        public IDigiDocumentDetailsRepository DigiDocumentDetails
        {
            get
            {
                if (_digiDocumentDetails == null)
                {
                    _digiDocumentDetails = new DigiDocumentDetailsRepository(_config, Context);
                }
                return _digiDocumentDetails;
            }
        }

        public IDigiDocumentSharedRepository _digiDocumentShared;
        public IDigiDocumentSharedRepository DigiDocumentShared
        {
            get
            {
                if (_digiDocumentShared == null)
                {
                    _digiDocumentShared = new DigiDocumentSharedRepository(_config, Context);
                }
                return _digiDocumentShared;
            }
        }

        public IDigiDocumentUploadRepository _digiDocumentUpload;
        public IDigiDocumentUploadRepository DigiDocumentUpload
        {
            get
            {
                if (_digiDocumentUpload == null)
                {
                    _digiDocumentUpload = new DigiDocumentUploadRepository(_config, Context);
                }
                return _digiDocumentUpload;
            }
        }

        public IDashboardListRepository _dashboardList;
        public IDashboardListRepository DashboardList
        {
            get
            {
                if (_dashboardList == null)
                {
                    _dashboardList = new DashboardListRepository(_config, Context);
                }
                return _dashboardList;
            }
        }

        public IFieldOfExpertiseRepository _fieldOfExpertises;
        public IFieldOfExpertiseRepository FieldOfExpertises
        {
            get
            {
                if (_fieldOfExpertises == null)
                {
                    _fieldOfExpertises = new FieldOfExpertiseRepository(_config, Context);
                }
                return _fieldOfExpertises;
            }
        }

        public IResumeTemplateRepository _resumeTemplates;
        public IResumeTemplateRepository ResumeTemplates
        {
            get
            {
                if (_resumeTemplates == null)
                {
                    _resumeTemplates = new ResumeTemplateRepository(_config, Context);
                }
                return _resumeTemplates;
            }
        }

        public IResumeCandidateMapRepository _resumeCandidateMap;
        public IResumeCandidateMapRepository ResumeCandidateMap
        {
            get
            {
                if (_resumeCandidateMap == null)
                {
                    _resumeCandidateMap = new ResumeCandidateMapRepository(_config, Context);
                }
                return _resumeCandidateMap;
            }
        }

        public IResumeDesignationMapRepository _resumeDesignationMap;
        public IResumeDesignationMapRepository ResumeDesignationMap
        {
            get
            {
                if (_resumeDesignationMap == null)
                {
                    _resumeDesignationMap = new ResumeDesignationMapRepository(_config, Context);
                }
                return _resumeDesignationMap;
            }
        }

        public IResumeExperienceMapRepository _resumeExperienceMap;
        public IResumeExperienceMapRepository ResumeExperienceMap
        {
            get
            {
                if (_resumeExperienceMap == null)
                {
                    _resumeExperienceMap = new ResumeExperienceMapRepository(_config, Context);
                }
                return _resumeExperienceMap;
            }
        }

        public IResumeExpertiseMapRepository _resumeExpertiseMap;
        public IResumeExpertiseMapRepository ResumeExpertiseMap
        {
            get
            {
                if (_resumeExpertiseMap == null)
                {
                    _resumeExpertiseMap = new ResumeExpertiseMapRepository(_config, Context);
                }
                return _resumeExpertiseMap;
            }
        }

        public IDigiDocumentDownloadRepository _digiDocumentDownload;
        public IDigiDocumentDownloadRepository DigiDocumentDownload
        {
            get
            {
                if (_digiDocumentDownload == null)
                {
                    _digiDocumentDownload = new DigiDocumentDownloadRepository(_config, Context);
                }
                return _digiDocumentDownload;
            }
        }

        public IVendorProfileImageRepository _vendorProfileImage;
        public IVendorProfileImageRepository VendorProfileImage
        {
            get
            {
                if (_vendorProfileImage == null)
                {
                    _vendorProfileImage = new VendorProfileImageRepository(_config, Context);
                }
                return _vendorProfileImage;
            }
        }
    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAlertIndustryMapRepository : IRepository<JobAlertIndustryMap>
    {
        Task<IPagedList<JobAlertIndustryMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobAlertIndustryMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAlertIndustryMapRepository : BaseRepository<JobAlertIndustryMap>, IJobAlertIndustryMapRepository
    {
        public JobAlertIndustryMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAlertIndustryMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jaim.RowId as RowId, JobAlertIndustryMapId, jaim.IndustryId as IndustryId,
                                    ind.Title as IndustryTitle, jaim.JobAlertId as JobAlertId, jpa.AlertTitle as JobAlertTitle, 
                                    jaim.IsActive as IsActive
                            FROM CA_JobAlertIndustryMap jaim
                            JOIN CA_Industries ind on jaim.IndustryId = ind.RowId
                            JOIN CA_JobPostAlerts jpa on jaim.JobAlertId = jpa.RowId
                            WHERE jaim.IsActive=1
                            ORDER BY jpa.AlertTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAlertIndustryMapInfo>(queries.query);
            return new DapperPagedList<JobAlertIndustryMapInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<JobAlertIndustryMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAlertIndustryMapSelectBoxDataViewModel result = new JobAlertIndustryMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAlertCategoryMapRepository : IRepository<JobAlertCategoryMap>
    {
        Task<IPagedList<JobAlertCategoryMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobAlertCategoryMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAlertCategoryMapRepository : BaseRepository<JobAlertCategoryMap>, IJobAlertCategoryMapRepository
    {
        public JobAlertCategoryMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAlertCategoryMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jacm.RowId as RowId, JobAlertCategoryMapId, jacm.JobCategoryId as JobCategoryId,
                                    jc.Title as JobCategoryTitle, jacm.JobAlertId as JobAlertId, jpa.AlertTitle as JobAlertTitle, 
                                    jacm.IsActive as IsActive
                            FROM CA_JobAlertCategoryMap jacm
                            JOIN CA_JobCategories jc on jacm.JobCategoryId = jc.RowId
                            JOIN CA_JobPostAlerts jpa on jacm.JobAlertId = jpa.RowId
                            WHERE jacm.IsActive=1
                            ORDER BY jpa.AlertTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAlertCategoryMapInfo>(queries.query);
            return new DapperPagedList<JobAlertCategoryMapInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<JobAlertCategoryMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAlertCategoryMapSelectBoxDataViewModel result = new JobAlertCategoryMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

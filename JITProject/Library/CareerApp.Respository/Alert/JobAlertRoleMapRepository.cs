﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAlertRoleMapRepository : IRepository<JobAlertRoleMap>
    {
        Task<IPagedList<JobAlertRoleMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobAlertRoleMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAlertRoleMapRepository : BaseRepository<JobAlertRoleMap>, IJobAlertRoleMapRepository
    {
        public JobAlertRoleMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAlertRoleMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jarm.RowId as RowId, JobAlertRoleMapId, jarm.JobRoleId as JobRoleId,
                                    jr.Title as JobRoleTitle, jarm.JobAlertId as JobAlertId, jpa.AlertTitle as JobAlertTitle, 
                                    jarm.IsActive as IsActive
                            FROM CA_JobAlertRoleMap jarm
                            JOIN CA_JobRoles jr on jarm.JobRoleId = jr.RowId
                            JOIN CA_JobPostAlerts jpa on jarm.JobAlertId = jpa.RowId
                            WHERE jarm.IsActive=1
                            ORDER BY jpa.AlertTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAlertRoleMapInfo>(queries.query);
            return new DapperPagedList<JobAlertRoleMapInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<JobAlertRoleMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAlertRoleMapSelectBoxDataViewModel result = new JobAlertRoleMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

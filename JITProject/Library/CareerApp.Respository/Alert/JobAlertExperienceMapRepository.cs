﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAlertExperienceMapRepository : IRepository<JobAlertExperienceMap>
    {
        Task<IPagedList<JobAlertExperienceMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobAlertExperienceMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAlertExperienceMapRepository : BaseRepository<JobAlertExperienceMap>, IJobAlertExperienceMapRepository
    {
        public JobAlertExperienceMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAlertExperienceMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jaem.RowId as RowId, JobAlertExperienceMapId, jaem.DurationId as DurationId, dr.Day as Day, dr.Month as Month, 
                                    dr.Year as Year, jaem.JobAlertId as JobAlertId, jpa.AlertTitle as JobAlertTitle, jaem.IsActive as IsActive
                            FROM CA_JobAlertExperienceMap jaem
                            JOIN CA_Durations dr on jaem.DurationId = dr.RowId
                            JOIN CA_JobPostAlerts jpa on jaem.JobAlertId = jpa.RowId
                            WHERE jaem.IsActive=1
                            ORDER BY jpa.AlertTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAlertExperienceMapInfo>(queries.query);
            return new DapperPagedList<JobAlertExperienceMapInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<JobAlertExperienceMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAlertExperienceMapSelectBoxDataViewModel result = new JobAlertExperienceMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

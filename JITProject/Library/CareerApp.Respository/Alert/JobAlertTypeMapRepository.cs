﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobAlertTypeMapRepository : IRepository<JobAlertTypeMap>
    {
        Task<IPagedList<JobAlertTypeMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobAlertTypeMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobAlertTypeMapRepository : BaseRepository<JobAlertTypeMap>, IJobAlertTypeMapRepository
    {
        public JobAlertTypeMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobAlertTypeMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jatm.RowId as RowId, JobAlertTypeMapId, jatm.JobTypeId as JobTypeId,
                                    jt.Title as JobTypeTitle, jatm.JobAlertId as JobAlertId, jpa.AlertTitle as JobAlertTitle, 
                                    jatm.IsActive as IsActive
                            FROM CA_JobAlertTypeMap jatm
                            JOIN CA_JobTypes jt on jatm.JobTypeId = jt.RowId
                            JOIN CA_JobPostAlerts jpa on jatm.JobAlertId = jpa.RowId
                            WHERE jatm.IsActive=1
                            ORDER BY jpa.AlertTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobAlertTypeMapInfo>(queries.query);
            return new DapperPagedList<JobAlertTypeMapInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<JobAlertTypeMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobAlertTypeMapSelectBoxDataViewModel result = new JobAlertTypeMapSelectBoxDataViewModel
            {
            };
            return result;
        }

    }
}

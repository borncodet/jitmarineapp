﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobPostAlertRepository : IRepository<JobPostAlerts>
    {
        Task<IPagedList<JobPostAlertInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<JobPostAlertSelectBoxDataViewModel> GetSelectBoxData();
        Task<IEnumerable<ValueCaptionPair>> GetTitleByCategory(TitleRequestModel model);
    }

    public class JobPostAlertRepository : BaseRepository<JobPostAlerts>, IJobPostAlertRepository
    {
        public JobPostAlertRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobPostAlertInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT jpa.RowId as RowId, JobAlertId, Keywords as Keywords,
                                    jpa.TotalExperience as TotalExperience, jpa.LocationId as LocationId, jpa.AlertTitle as AlertTitle, 
                                    jpa.SalaryFrom as SalaryFrom, jpa.UserId as UserId, jpa.IsActive as IsActive
                            FROM CA_JobPostAlerts jpa
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and jpa.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and jpa.UserId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY jpa.AlertTitle";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobPostAlertInfo>(queries.query);
            return new DapperPagedList<JobPostAlertInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<JobPostAlertSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobPostAlertSelectBoxDataViewModel result = new JobPostAlertSelectBoxDataViewModel
            {
                JobTitles = await QueryAsync<ValueCaptionPair>("Select CAST('--------Select--------' AS varchar) as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select Title as Value, Title as Caption From CA_JobLists where IsActive=1"),
            };
            return result;
        }

        public async Task<IEnumerable<ValueCaptionPair>> GetTitleByCategory(TitleRequestModel model)
        {
            var JobTitles = await QueryAsync<ValueCaptionPair>($"Select CAST('--------Select--------' AS varchar) as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select Title as Value, Title as Caption From CA_JobLists where CategoryId ='{model.CategoryId}' and IsActive=1");
            return JobTitles;
        }

    }
}

﻿using CareerApp.Core.DB;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Security.Claims;

namespace CareerApp.Respository
{
    public class HttpUnitOfWork : UnitOfWork
    {
        public HttpUnitOfWork(ApplicationDbContext context, IHttpContextAccessor httpAccessor, IConfiguration configuration) : base(configuration, context)
        {
            //context.CurrentUserId = int.Parse(httpAccessor.HttpContext.User.FindFirst(OpenIdConnectConstants.Claims.Subject)?.Value?.Trim());
            var CurrentUserId = httpAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value?.Trim() ?? "0";
            context.CurrentUserId = int.Parse(CurrentUserId);
        }
    }
}

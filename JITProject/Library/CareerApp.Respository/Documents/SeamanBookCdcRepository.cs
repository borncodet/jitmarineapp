﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ISeamanBookCdcRepository : IRepository<SeamanBookCdc>
    {
        Task<IPagedList<SeamanBookCdc>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class SeamanBookCdcRepository : BaseRepository<SeamanBookCdc>, ISeamanBookCdcRepository
    {
        public SeamanBookCdcRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<SeamanBookCdc>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<SeamanBookCdc> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.CdcNumber.Contains(searchWord));
            }

            query = query.OrderBy(c => c.CdcNumber);
            return await Task.Run(() => new PagedList<SeamanBookCdc>(query, pageIndex, pageSize));
        }

    }
}

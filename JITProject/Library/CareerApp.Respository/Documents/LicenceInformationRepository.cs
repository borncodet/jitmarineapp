﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface ILicenceInformationRepository : IRepository<LicenceInformation>
    {
        Task<IPagedList<LicenceInformation>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class LicenceInformationRepository : BaseRepository<LicenceInformation>, ILicenceInformationRepository
    {
        public LicenceInformationRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<LicenceInformation>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<LicenceInformation> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.LicenceNo.Contains(searchWord));
            }

            query = query.OrderBy(c => c.LicenceNo);
            return await Task.Run(() => new PagedList<LicenceInformation>(query, pageIndex, pageSize));
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IPassportInformationRepository : IRepository<PassportInformation>
    {
        Task<IPagedList<PassportInformation>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class PassportInformationRepository : BaseRepository<PassportInformation>, IPassportInformationRepository
    {
        public PassportInformationRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<PassportInformation>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<PassportInformation> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.PassportNo.Contains(searchWord));
            }

            query = query.OrderBy(c => c.PassportNo);
            return await Task.Run(() => new PagedList<PassportInformation>(query, pageIndex, pageSize));
        }

    }
}

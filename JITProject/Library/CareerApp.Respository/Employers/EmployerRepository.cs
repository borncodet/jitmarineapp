﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IEmployerRepository : IRepository<Employer>
    {
        Task<IPagedList<EmployerInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<EmployerSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class EmployerRepository : BaseRepository<Employer>, IEmployerRepository
    {
        public EmployerRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<EmployerInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT emp.RowId as RowId, EmployerId, CompanyName, RegisterNumber, Email, PhoneNumber, Address1, Address2, City, emp.StateId as StateId, st.Name as StateName, emp.CountryId as CountryId, cn.Name as CountryName, ZipCode, UserId, emp.IsActive as IsActive
            FROM CA_Employer emp
            JOIN CA_Countries cn on emp.CountryId = cn.RowId
            JOIN CA_States st on emp.StateId = st.RowId
            Where emp.IsActive=1
            Order by CompanyName";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<EmployerInfo>(queries.query);
            return new DapperPagedList<EmployerInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<EmployerSelectBoxDataViewModel> GetSelectBoxData()
        {
            EmployerSelectBoxDataViewModel result = new EmployerSelectBoxDataViewModel
            {
                States = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Name  as Caption From CA_States where IsActive=1"),
                Countries = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Name  as Caption From CA_Countries where IsActive=1")
            };
            return result;
        }

    }
}

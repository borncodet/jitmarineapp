﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobLanguageMapRepository : IRepository<JobLanguageMap>
    {
        Task<IPagedList<JobLanguageMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobLanguageMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobLanguageMapRepository : BaseRepository<JobLanguageMap>, IJobLanguageMapRepository
    {
        public JobLanguageMapRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobLanguageMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jlm.RowId as RowId, JobLanguageMapId, jlm.JobId as JobId, jl.Title as JobTitle, jlm.LanguageId as LanguageId, ln.Name as LanguageName, jlm.IsActive as IsActive
            FROM CA_JobLanguageMap jlm
            JOIN CA_JobLists jl on jlm.JobId = jl.RowId
            JOIN CA_Languages ln on jlm.LanguageId = ln.RowId
            Where jlm.IsActive=1
            Order by JobTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobLanguageMapInfo>(queries.query);
            return new DapperPagedList<JobLanguageMapInfo>(data, count, pageIndex, pageSize);

        }

        public async Task<JobLanguageMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobLanguageMapSelectBoxDataViewModel result = new JobLanguageMapSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

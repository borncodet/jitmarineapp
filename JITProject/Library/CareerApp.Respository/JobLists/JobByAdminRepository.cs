﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobByAdminRepository : IRepository<JobByAdmin>
    {
        Task<IPagedList<JobByAdminInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobByAdminSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobByAdminRepository : BaseRepository<JobByAdmin>, IJobByAdminRepository
    {
        public JobByAdminRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobByAdminInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jba.RowId as RowId, JobByAdminId, jba.JobId as JobId, jl.Title as JobTitle, EmployeeId, jba.IsActive as IsActive
            FROM CA_JobByAdmin jba
            JOIN CA_JobLists jl on jba.JobId = jl.RowId
            Where jba.IsActive=1
            Order by JobTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobByAdminInfo>(queries.query);
            return new DapperPagedList<JobByAdminInfo>(data, count, pageIndex, pageSize);

        }

        public async Task<JobByAdminSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobByAdminSelectBoxDataViewModel result = new JobByAdminSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

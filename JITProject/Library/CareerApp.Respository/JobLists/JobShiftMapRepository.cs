﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobShiftMapRepository : IRepository<JobShiftMap>
    {
        Task<IPagedList<JobShiftMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobShiftMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobShiftMapRepository : BaseRepository<JobShiftMap>, IJobShiftMapRepository
    {
        public JobShiftMapRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobShiftMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jsm.RowId as RowId, JobShiftMapId, jsm.JobId as JobId, jl.Title as JobTitle, jsm.ShiftId as ShiftId, sa.Title as ShiftTitle, jsm.IsActive as IsActive
            FROM CA_JobShiftMap jsm
            JOIN CA_JobLists jl on jsm.JobId = jl.RowId
            JOIN CA_ShiftAvailable sa on jsm.ShiftId = sa.RowId
            Where jsm.IsActive=1
            Order by JobTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobShiftMapInfo>(queries.query);
            return new DapperPagedList<JobShiftMapInfo>(data, count, pageIndex, pageSize);

        }

        public async Task<JobShiftMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobShiftMapSelectBoxDataViewModel result = new JobShiftMapSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

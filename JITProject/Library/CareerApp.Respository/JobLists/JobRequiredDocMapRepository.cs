﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobRequiredDocMapRepository : IRepository<JobRequiredDocMap>
    {
        Task<IPagedList<JobRequiredDocMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobRequiredDocMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobRequiredDocMapRepository : BaseRepository<JobRequiredDocMap>, IJobRequiredDocMapRepository
    {
        public JobRequiredDocMapRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobRequiredDocMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jrd.RowId as RowId, JobRequiredDocMapId, jrd.JobId as JobId, jl.Title as JobTitle, jrd.RequiredDocId as RequiredDocId, rdt.Title as RequiredDocTitle, jrd.IsActive as IsActive
            FROM CA_JobRequiredDocMap jrd
            JOIN CA_JobLists jl on jrd.JobId = jl.RowId
            JOIN CA_RequiredDocumentTypes rdt on jrd.RequiredDocId = rdt.RowId
            Where jrd.IsActive=1
            Order by JobTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobRequiredDocMapInfo>(queries.query);
            return new DapperPagedList<JobRequiredDocMapInfo>(data, count, pageIndex, pageSize);

        }

        public async Task<JobRequiredDocMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobRequiredDocMapSelectBoxDataViewModel result = new JobRequiredDocMapSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

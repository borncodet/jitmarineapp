﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobCourseMapRepository : IRepository<JobCourseMap>
    {
        Task<IPagedList<JobCourseMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobCourseMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobCourseMapRepository : BaseRepository<JobCourseMap>, IJobCourseMapRepository
    {
        public JobCourseMapRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobCourseMapInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jc.RowId as RowId, JobCourseMapId, jc.JobId as JobId, jl.Title as JobTitle, jc.CourseId as CourseId, cs.Title as CourseName, jc.IsActive as IsActive
            FROM CA_JobCourseMap jc
            JOIN CA_JobLists jl on jc.JobId = jl.RowId
            JOIN CA_Courses cs on jc.CourseId = cs.RowId
            Where jc.IsActive=1
            Order by JobTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobCourseMapInfo>(queries.query);
            return new DapperPagedList<JobCourseMapInfo>(data, count, pageIndex, pageSize);

        }

        public async Task<JobCourseMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobCourseMapSelectBoxDataViewModel result = new JobCourseMapSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

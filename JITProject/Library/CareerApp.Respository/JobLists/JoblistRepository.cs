﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobListRepository : IRepository<JobList>
    {
        Task<IPagedList<JobListInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<IPagedList<JobListInfo>> SearchAsync(BaseSearchViewModel searchModel);
        Task<IPagedList<JobListInfo>> SearchMultipleAsync(BaseSearchMultipleViewModel searchModel);
        Task<IPagedList<JobListInfo>> SearchByCandidateAsync(BaseSearchByCandidateViewModel searchModel);
        Task<IPagedList<JobListInfo>> GetAlertJobAsync(BaseSearchByCandidateViewModel searchModel);
        Task<int> GetAlertJobCountAsync(BaseCandidateSearchViewModel searchModel);
        Task<IPagedList<JobListInfo>> SuggestedJobAsync(BaseSearchByCandidateViewModel searchModel);
        Task<IPagedList<RecentlyAppliedJobInfo>> SearchAppliedJobAsync(AppliedSearchMultipleViewModel searchModel);
        Task<JobListSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobListRepository : BaseRepository<JobList>, IJobListRepository
    {
        public JobListRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        // Index or Home page job listing
        public async Task<IPagedList<JobListInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"
                            SELECT jb.RowId as RowId, JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,
                            IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, cur.Name as Currency, jb.IndustryId as IndustryId,
                            ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea, ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId,
                            lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, jb.CreatedDate as PostedDate, jb.IsActive as IsActive
                            FROM CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            WHERE 1=1";
            if (!showInactive)
            {
                query += " and jb.IsActive=1 ";
            }

            query += " ORDER BY jb.CreatedDate DESC";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobListInfo>(queries.query);
            return new DapperPagedList<JobListInfo>(data, count, pageIndex, pageSize);
        }

        // Job search
        public async Task<IPagedList<JobListInfo>> SearchAsync(BaseSearchViewModel searchModel)
        {
            string query = @"
                            SELECT jb.RowId as RowId, JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, 
                                   jb.Description Description, ExperienceId, exp1.Title as Experience, NumberOfVacancies, jb.JobTypeId as JobTypeId,
                                   jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, 
                                   jb.CurrencyId as CurrencyId, cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, 
                                   jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea, ProfileDescription, WillingnessToTravelFlag, 
                                   jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag,     
                                   jb.CreatedDate as PostedDate, jb.IsActive as IsActive
                            FROM CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (!string.IsNullOrEmpty(searchModel.Title) && !string.IsNullOrEmpty(searchModel.Title))
            {
                query += $" and ( jb.Title LIKE '%{searchModel.Title}%'";
                query += $" or jc.Title LIKE '%{searchModel.Title}%' )";
            }
            if (!string.IsNullOrEmpty(searchModel.Location) && !string.IsNullOrEmpty(searchModel.Location))
            {
                query += $" and LocationId LIKE '%{searchModel.Location}%'";
            }
            if (!string.IsNullOrEmpty(searchModel.Type) && !string.IsNullOrEmpty(searchModel.Type))
            {
                query += $" and jbt.RowId ='{searchModel.Type}'";
            }
            if (!string.IsNullOrEmpty(searchModel.Expereince) && !string.IsNullOrEmpty(searchModel.Expereince))
            {
                query += $" and jb.ExperienceId  ='{searchModel.Expereince}'";
            }
            if (searchModel.LastDays > 0)
            {
                var datePosted = await _context.DatePosted.FindAsync(searchModel.LastDays);
                query += $" and DATEDIFF(d, jb.CreatedDate, '{DateTime.Now.Date}') <= {datePosted.Day}";
            }
            query += " ORDER BY jb.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobListInfo>(queries.query);
            return new DapperPagedList<JobListInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        // Job search with multiple condition
        public async Task<IPagedList<JobListInfo>> SearchMultipleAsync(BaseSearchMultipleViewModel searchModel)
        {
            string query = $@"
                            SELECT jb.RowId as RowId, jb.JobId as JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, jb.CreatedDate as PostedDate, 
                            CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(CASE WHEN jad.RowId >0 and jad.IsActive = 1 and jad.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsApplied, 
                            jb.IsActive as IsActive
                            FROM CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            LEFT JOIN CA_JobAppliedDetails jad on jb.RowId = jad.JobId and jad.CandidateId = '{searchModel.CandidateId}'
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (searchModel.Title != null)
            {
                if (searchModel.Title.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Title)
                    {
                        if (flag)
                        {
                            query += $" jb.Title LIKE '%{item}%' ";
                        }
                        else
                        {
                            query += $" or jb.Title LIKE '%{item}%' ";
                        }
                        query += $" or jc.Title LIKE '%{item}%' ";
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.Location != null)
            {
                if (searchModel.Location.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Location)
                    {
                        if (flag)
                        {
                            query += $" LocationId LIKE '%{item}%' ";
                        }
                        else
                        {
                            query += $" or LocationId LIKE '%{item}%' ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.Type != null)
            {
                if (searchModel.Type.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Type)
                    {
                        if (flag)
                        {
                            query += $" jbt.RowId='{item}' ";
                        }
                        else
                        {
                            query += $" or jbt.RowId='{item}' ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.Expereince != null)
            {
                if (searchModel.Expereince.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Expereince)
                    {
                        if (flag)
                        {
                            query += $" jb.ExperienceId='{item}' ";
                        }
                        else
                        {
                            query += $" or jb.ExperienceId='{item}' ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            //if (searchModel.Expereince != null)
            //{
            //    if (searchModel.Expereince.Count > 0)
            //    {
            //        query += $" and ( 1 = 1 ";
            //        foreach (var item in searchModel.Expereince)
            //        {
            //            if (item.Contains("year"))
            //            {
            //                if (int.TryParse(item.Split("year")[0], out int result))
            //                {
            //                    query += $" or ( exp1.FromYear <= '{result}' and  exp1.ToYear >= '{result}' ) ";
            //                }
            //            }
            //        }
            //        query += $" ) ";
            //        query += $" and ( 1 = 1 ";
            //        foreach (var item in searchModel.Expereince)
            //        {
            //            if (item.Contains("month"))
            //            {
            //                if (int.TryParse(item.Split("month")[0], out int result))
            //                {
            //                    query += $" or ( exp1.FromMonth <= '{result}' and  exp1.ToMonth >= '{result}' ) ";
            //                }
            //            }
            //        }
            //        query += $" ) ";
            //        query += $" and ( 1 = 1 ";
            //        foreach (var item in searchModel.Expereince)
            //        {
            //            if (item.Contains("day"))
            //            {
            //                if (int.TryParse(item.Split("day")[0], out int result))
            //                {
            //                    query += $" or ( exp1.FromDay <= '{result}' and  exp1.FromDay >= '{result}' ) ";
            //                }
            //            }
            //        }
            //        query += $" ) ";
            //    }
            //}         
            if (searchModel.LastDays != null)
            {
                if (searchModel.LastDays.Count > 0)
                {
                    bool flag = true;
                    int i = searchModel.LastDays.Count;
                    int j = 0;
                    foreach (var item in searchModel.LastDays)
                    {
                        var datePosted = await _context.DatePosted.FindAsync(item);
                        j += 1;
                        if (datePosted != null)
                        {
                            if (flag)
                            {
                                query += $" and ( DATEDIFF(d, jb.CreatedDate, '{DateTime.Now.Date}') <= {datePosted.Day} ";
                            }
                            else
                            {
                                query += $" or DATEDIFF(d, jb.CreatedDate, '{DateTime.Now.Date}') <= {datePosted.Day} ";
                            }
                            flag = false;
                        }
                        if (flag == false && j == i)
                        {
                            query += $" ) ";
                        }
                    }
                }
            }
            query = NewMethod(query);

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobListInfo>(queries.query);
            return new DapperPagedList<JobListInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        private static string NewMethod(string query)
        {
            query += " ORDER BY jb.CreatedDate DESC";
            return query;
        }

        // Matched Job list
        public async Task<IPagedList<JobListInfo>> SearchByCandidateAsync(BaseSearchByCandidateViewModel searchModel)
        {
            string query = $@"
                            SELECT jb.RowId as RowId, jb.JobId as JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, jb.CreatedDate as PostedDate,  
                            CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(CASE WHEN jad.RowId >0 and jad.IsActive = 1 and jad.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsApplied,
                            jb.IsActive as IsActive
                            FROM CA_Candidate can,(CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            LEFT JOIN CA_JobAppliedDetails jad on jb.RowId = jad.JobId and jad.CandidateId = '{searchModel.CandidateId}')
                            WHERE can.RowId = '{searchModel.CandidateId}' and can.JobTypeId = jb.JobTypeId and can.JobCategoryId = jb.CategoryId and jb.IsActive=1";

            query += " ORDER BY jb.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobListInfo>(queries.query);
            return new DapperPagedList<JobListInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        // Job list by job alert created or suggested job list by job alert and profile
        public async Task<IPagedList<JobListInfo>> GetAlertJobAsync(BaseSearchByCandidateViewModel searchModel)
        {
            string query = $@"
                            SELECT distinct jb.RowId as RowId, jb.JobId as JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, jb.LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, jpa.CreatedDate as PostedDate, 
                            CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(CASE WHEN jad.RowId >0 and jad.IsActive = 1 and jad.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsApplied, 
                            jb.IsActive as IsActive
                            FROM CA_Candidate can, (CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            LEFT JOIN CA_JobAppliedDetails jad on jb.RowId = jad.JobId and jad.CandidateId = '{searchModel.CandidateId}'),
                            (CA_JobPostAlerts jpa
                            JOIN CA_JobAlertCategoryMap jacm on jacm.JobAlertId = jpa.RowId and jpa.IsActive=1
                            JOIN CA_JobAlertTypeMap jatm on jatm.JobAlertId = jpa.RowId
                            LEFT JOIN CA_JobAlertRoleMap jarm on jarm.JobAlertId = jpa.RowId
                            LEFT JOIN CA_JobAlertExperienceMap jaem on jaem.JobAlertId = jpa.RowId
                            LEFT JOIN CA_JobAlertIndustryMap jaim on jaim.JobAlertId = jpa.RowId
                            )
                            WHERE jpa.UserId = '{searchModel.CandidateId}' and  ( jatm.JobTypeId = jb.JobTypeId or jacm.JobCategoryId = jb.CategoryId or  jb.Title like '%'+jpa.AlertTitle+'%' or  jb.LocationId like '%'+jpa.LocationId+'%' )  and jb.IsActive=1";

            query += " ORDER BY jpa.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobListInfo>(queries.query);
            return new DapperPagedList<JobListInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        // Count of job by job alert created or count of suggested job by job alert and profile
        public async Task<int> GetAlertJobCountAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = $@"
                            SELECT COUNT(*) as JobAlertCount
                            FROM CA_Candidate can, (CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId),(CA_JobPostAlerts jpa
                            JOIN CA_JobAlertCategoryMap jacm on jacm.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertExperienceMap jaem on jaem.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertIndustryMap jaim on jaim.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertRoleMap jarm on jarm.JobAlertId = jpa.RowId
                            JOIN CA_JobAlertTypeMap jatm on jatm.JobAlertId = jpa.RowId)
                            WHERE jatm.JobTypeId = jb.JobTypeId and jacm.JobCategoryId = jb.CategoryId and jaim.IndustryId = jb.IndustryId and ( DATEDIFF(d, jb.CreatedDate, '{DateTime.Now.Date}') <= 7 )";

            if (!searchModel.ShowInactive)
            {
                query += " and jb.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and jpa.UserId ='{searchModel.CandidateId}'";
            }

            var count = await ExecuteScalarAsync(query);
            return count;
        }

        // Suggested job by category candidate chosen
        public async Task<IPagedList<JobListInfo>> SuggestedJobAsync(BaseSearchByCandidateViewModel searchModel)
        {
            string query = $@"
                            SELECT jb.RowId as RowId, jb.JobId as JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, jb.CreatedDate as PostedDate,
                            CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(CASE WHEN jad.RowId >0 and jad.IsActive = 1 and jad.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsApplied, 
                            jb.IsActive as IsActive
                            FROM CA_Candidate can,(CA_JobLists jb
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            LEFT JOIN CA_JobAppliedDetails jad on jb.RowId = jad.JobId and jad.CandidateId = '{searchModel.CandidateId}')
                            WHERE can.RowId = '{searchModel.CandidateId}' and can.JobCategoryId = jb.CategoryId and jb.IsActive=1";

            query += " ORDER BY jb.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobListInfo>(queries.query);
            return new DapperPagedList<JobListInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        // Job Applied Search
        public async Task<IPagedList<RecentlyAppliedJobInfo>> SearchAppliedJobAsync(AppliedSearchMultipleViewModel searchModel)
        {
            string query = $@"SELECT ja.RowId as RowId, ja.JobId as JobId, JobAppliedDetailsId, ja.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description Description, ExperienceId, exp1.Title as Experience, 
                            NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, IsPreferred,IsRequired, LocationId, RegionId, TerritoryId, MinAnnualSalary, MaxAnnualSalary, jb.CurrencyId as CurrencyId, 
                            cur.Name as Currency, jb.IndustryId as IndustryId, ind.Title as Industry, jb.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalArea,
                            ProfileDescription, WillingnessToTravelFlag, jb.PreferedLangId as PreferedLangId, lan.Name as PreferedLanguage, AutoScreeningFilterFlag, AutoSkillAssessmentFlag, ja.CreatedDate as PostedDate, 
                            DATEDIFF(d, ja.CreatedDate, '{DateTime.Now.Date}') as DaysAgo, CAST(CASE WHEN jbd.RowId >0 and jbd.IsActive = 1 and jbd.CandidateId = '{searchModel.CandidateId}' THEN 1 ELSE 0 END AS BIT) AS IsBookmarked, 
                            CAST(1 AS BIT) AS IsApplied, jb.IsActive as IsActive
                            FROM CA_JobAppliedDetails ja
                            JOIN CA_Candidate can on ja.CandidateId = can.RowId                        
                            JOIN CA_JobLists jb on ja.JobId = jb.RowId
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.ExperienceId = exp1.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            JOIN CA_Currencies cur on jb.CurrencyId = cur.RowId
                            JOIN CA_Industries ind on jb.IndustryId = ind.RowId
                            JOIN CA_FunctionalAreas fa on jb.FunctionalAreaId = fa.RowId
                            JOIN CA_Languages lan on jb.PreferedLangId = lan.RowId
                            LEFT JOIN CA_JobBookmarked jbd on jb.RowId = jbd.JobId and jbd.CandidateId = '{searchModel.CandidateId}'
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ja.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and can.RowId ='{searchModel.CandidateId}'";
            }
            if (searchModel.Title != null)
            {
                if (searchModel.Title.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Title)
                    {
                        if (flag)
                        {
                            query += $" jb.Title LIKE '%{item}%' ";
                        }
                        else
                        {
                            query += $" or jb.Title LIKE '%{item}%' ";
                        }
                        query += $" or jc.Title LIKE '%{item}%' ";
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.Location != null)
            {
                if (searchModel.Location.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Location)
                    {
                        if (flag)
                        {
                            query += $" LocationId LIKE '%{item}%' ";
                        }
                        else
                        {
                            query += $" or LocationId LIKE '%{item}%' ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.Type != null)
            {
                if (searchModel.Type.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.Type)
                    {
                        if (flag)
                        {
                            query += $" jbt.RowId='{item}' ";
                        }
                        else
                        {
                            query += $" or jbt.RowId='{item}' ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            query += " ORDER BY ja.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<RecentlyAppliedJobInfo>(queries.query);
            return new DapperPagedList<RecentlyAppliedJobInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<JobListSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobListSelectBoxDataViewModel result = new JobListSelectBoxDataViewModel
            {
                Categories = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_JobCategories where IsActive=1"),
                MinExperiences = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ExpereinceTypes where IsActive=1"),
                MaxExperiences = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ExpereinceTypes where IsActive=1"),
                JobTypes = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_JobTypes where IsActive=1"),
                Currencies = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Name  as Caption From CA_Currencies where IsActive=1"),
                Industries = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_Industries where IsActive=1"),
                FunctionalAreas = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_FunctionalAreas where IsActive=1"),
                PreferedLanguages = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Name  as Caption From CA_Languages where IsActive=1")
            };
            return result;
        }
    }
}

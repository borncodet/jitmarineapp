﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IDatePostedRepository : IRepository<DatePosted>
    {
        Task<IPagedList<DatePosted>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class DatePostedRepository : BaseRepository<DatePosted>, IDatePostedRepository
    {
        public DatePostedRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<DatePosted>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<DatePosted> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                if (int.TryParse(searchWord, out int numValue))
                {
                    query = query.Where(c => c.Year == numValue);
                }
            }

            query = query.OrderBy(c => c.Year);
            return await Task.Run(() => new PagedList<DatePosted>(query, pageIndex, pageSize));
        }

    }
}

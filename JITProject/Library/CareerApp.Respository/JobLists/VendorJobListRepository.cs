﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IVendorJobListRepository : IRepository<VendorJobList>
    {
        Task<IPagedList<VendorJobListInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<VendorJobListSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class VendorJobListRepository : BaseRepository<VendorJobList>, IVendorJobListRepository
    {
        public VendorJobListRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<VendorJobListInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"
                            SELECT jb.RowId as RowId, jb.VendorId as VendorId, vnd.CompanyName as CompanyName, JobId, jb.CategoryId as CategoryId, jc.Title as CategoryName, jb.Title as Title, jb.Description as Description, MinExperienceId, exp1.Title as MinimumExperience, 
                            MaxExperienceId, exp2.Title as MaximumExperience, NumberOfVacancies, jb.JobTypeId as JobTypeId,jbt.Title as JobType, jb.IsActive as IsActive
                            FROM CA_VendorJobLists jb
                            JOIN CA_Vendors vnd on jb.VendorId = vnd.RowId
                            JOIN CA_JobCategories jc on jb.CategoryId = jc.RowId
                            JOIN CA_ExpereinceTypes exp1 on jb.MinExperienceId = exp1.RowId
                            JOIN CA_ExpereinceTypes exp2 on jb.MinExperienceId = exp2.RowId
                            JOIN CA_JobTypes jbt on jb.JobTypeId = jbt.RowId
                            Where jb.IsActive=1
                            Order by jb.Title";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<VendorJobListInfo>(queries.query);
            return new DapperPagedList<VendorJobListInfo>(data, count, pageIndex, pageSize);
        }

        public async Task<VendorJobListSelectBoxDataViewModel> GetSelectBoxData()
        {
            VendorJobListSelectBoxDataViewModel result = new VendorJobListSelectBoxDataViewModel
            {
                Categories = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_JobCategories where IsActive=1"),
                MinExperiences = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ExpereinceTypes where IsActive=1"),
                MaxExperiences = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ExpereinceTypes where IsActive=1"),
                JobTypes = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_JobTypes where IsActive=1"),
            };
            return result;
        }

    }
}

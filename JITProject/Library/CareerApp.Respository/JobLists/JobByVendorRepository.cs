﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IJobByVendorRepository : IRepository<JobByVendor>
    {
        Task<IPagedList<JobByVendorInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<JobByVendorSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class JobByVendorRepository : BaseRepository<JobByVendor>, IJobByVendorRepository
    {
        public JobByVendorRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<JobByVendorInfo>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            string query = @"SELECT jbv.RowId as RowId, JobByVendorId, jbv.JobId as JobId, jl.Title as JobTitle, jbv.VendorId as VendorId, CompanyName, VerificationStatus, vs.Title as VerificationStatusTitile, EmployeeId, jbv.IsActive as IsActive
            FROM CA_JobByVendors jbv
            JOIN CA_JobLists jl on jbv.JobId = jl.RowId
            JOIN CA_Vendors vn on jbv.VendorId = vn.RowId
            JOIN CA_VendorsStatus vs on jbv.VerificationStatus = vs.RowId
            Where jbv.IsActive=1
            Order by JobTitle";

            var queries = GetPagingQueries(query, pageIndex, pageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<JobByVendorInfo>(queries.query);
            return new DapperPagedList<JobByVendorInfo>(data, count, pageIndex, pageSize);

        }

        public async Task<JobByVendorSelectBoxDataViewModel> GetSelectBoxData()
        {
            JobByVendorSelectBoxDataViewModel result = new JobByVendorSelectBoxDataViewModel
            {
            };
            return result;
        }
    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IResumeTemplateRepository : IRepository<ResumeTemplate>
    {
        Task<IPagedList<ResumeTemplateInfo>> GetAllAsync(BaseResumeSearchByCandidateViewModel searchModel);
        Task<ResumeTemplateSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class ResumeTemplateRepository : BaseRepository<ResumeTemplate>, IResumeTemplateRepository
    {
        public ResumeTemplateRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<ResumeTemplateInfo>> GetAllAsync(BaseResumeSearchByCandidateViewModel searchModel)
        {
            string query = @"SELECT rt.RowId as RowId, rt.ResumeTemplateId as ResumeTemplateId, rt.Title as Title,
                                    rt.Description as Description, rt.ResumeContent as ResumeContent, rt.ResumeImage as ResumeImage, 
                                    rt.IsActive as IsActive
                            FROM CA_ResumeTemplates rt
                            LEFT JOIN CA_ResumeExpertiseMap rf on rf.ResumeTemplateId = rt.RowId
                            LEFT JOIN CA_ResumeExperienceMap re on re.ResumeTemplateId = rt.RowId 
                            LEFT JOIN CA_ResumeDesignationMap rd on rd.ResumeTemplateId = rt.RowId 
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and rt.IsActive=1";
            }
            if (searchModel.FieldOfExpertiseId != null)
            {
                if (searchModel.FieldOfExpertiseId.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.FieldOfExpertiseId)
                    {
                        if (flag)
                        {
                            query += $" rf.FieldOfExpertiseId = {item} ";
                        }
                        else
                        {
                            query += $" or rf.FieldOfExpertiseId = {item} ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.ExperienceTypeId != null)
            {
                if (searchModel.ExperienceTypeId.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.ExperienceTypeId)
                    {
                        if (flag)
                        {
                            query += $" re.ExpereinceTypeId = {item} ";
                        }
                        else
                        {
                            query += $" or re.ExpereinceTypeId = {item} ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            if (searchModel.DesignationId != null)
            {
                if (searchModel.DesignationId.Count > 0)
                {
                    bool flag = true;
                    query += $" and ( ";
                    foreach (var item in searchModel.DesignationId)
                    {
                        if (flag)
                        {
                            query += $" rd.DesignationId = {item} ";
                        }
                        else
                        {
                            query += $" or rd.DesignationId = {item} ";
                        }
                        flag = false;
                    }
                    query += $" ) ";
                }
            }
            //if (searchModel.CandidateId > 0)
            //{
            //    query += $" and rc.CandidateId ='{searchModel.CandidateId}'";
            //}
            query += " ORDER BY rt.CreatedDate DESC";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ResumeTemplateInfo>(queries.query);
            return new DapperPagedList<ResumeTemplateInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<ResumeTemplateSelectBoxDataViewModel> GetSelectBoxData()
        {
            ResumeTemplateSelectBoxDataViewModel result = new ResumeTemplateSelectBoxDataViewModel
            {
                FieldOfExeprtises = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_FieldOfExpertise where IsActive=1"),
                ExperienceTypes = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ExpereinceTypes where IsActive=1"),
                Designations = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_Designation where IsActive=1"),
            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IResumeExperienceMapRepository : IRepository<ResumeExperienceMap>
    {
        Task<IPagedList<ResumeExperienceMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<ResumeExperienceMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class ResumeExperienceMapRepository : BaseRepository<ResumeExperienceMap>, IResumeExperienceMapRepository
    {
        public ResumeExperienceMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<ResumeExperienceMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT rem.RowId as RowId, ResumeExperienceMapId, rem.ExpereinceTypeId as ExpereinceTypeId,
                                    rem.ResumeTemplateId as ResumeTemplateId, rem.IsActive as IsActive
                            FROM CA_ResumeExperienceMap rem
                            JOIN CA_ExpereinceTypes et on rem.ExpereinceTypeId = et.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and rem.IsActive=1";
            }
            query += "  ORDER BY rem.RowId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ResumeExperienceMapInfo>(queries.query);
            return new DapperPagedList<ResumeExperienceMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<ResumeExperienceMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            ResumeExperienceMapSelectBoxDataViewModel result = new ResumeExperienceMapSelectBoxDataViewModel
            {
                ResumeTemplateId = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ResumeTemplates where IsActive=1"),
            };
            return result;
        }

    }
}

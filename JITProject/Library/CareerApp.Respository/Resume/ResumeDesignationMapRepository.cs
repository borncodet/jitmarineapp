﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IResumeDesignationMapRepository : IRepository<ResumeDesignationMap>
    {
        Task<IPagedList<ResumeDesignationMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<ResumeDesignationMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class ResumeDesignationMapRepository : BaseRepository<ResumeDesignationMap>, IResumeDesignationMapRepository
    {
        public ResumeDesignationMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<ResumeDesignationMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT rdm.RowId as RowId, ResumeDesignationMapId, rdm.DesignationId as DesignationId,
                                    rdm.ResumeTemplateId as ResumeTemplateId, rdm.IsActive as IsActive
                            FROM CA_ResumeDesignationMap rdm
                            JOIN CA_Designation des on rdm.DesignationId = des.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and rdm.IsActive=1";
            }
            query += "  ORDER BY rdm.RowId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ResumeDesignationMapInfo>(queries.query);
            return new DapperPagedList<ResumeDesignationMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<ResumeDesignationMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            ResumeDesignationMapSelectBoxDataViewModel result = new ResumeDesignationMapSelectBoxDataViewModel
            {
                ResumeTemplateId = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ResumeTemplates where IsActive=1"),
            };
            return result;
        }

    }
}

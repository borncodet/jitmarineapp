﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IResumeExpertiseMapRepository : IRepository<ResumeExpertiseMap>
    {
        Task<IPagedList<ResumeExpertiseMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<ResumeExpertiseMapSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class ResumeExpertiseMapRepository : BaseRepository<ResumeExpertiseMap>, IResumeExpertiseMapRepository
    {
        public ResumeExpertiseMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<ResumeExpertiseMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT rem.RowId as RowId, ResumeExpertiseMapId, rem.FieldOfExpertiseId as FieldOfExpertiseId,
                                    rem.ResumeTemplateId as ResumeTemplateId, rem.IsActive as IsActive
                            FROM CA_ResumeExpertiseMap rem
                            JOIN CA_FieldOfExpertise fe on rem.FieldOfExpertiseId = fe.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and rem.IsActive=1";
            }
            query += "  ORDER BY rem.RowId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ResumeExpertiseMapInfo>(queries.query);
            return new DapperPagedList<ResumeExpertiseMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<ResumeExpertiseMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            ResumeExpertiseMapSelectBoxDataViewModel result = new ResumeExpertiseMapSelectBoxDataViewModel
            {
                ResumeTemplateId = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ResumeTemplates where IsActive=1"),
            };
            return result;
        }

    }
}

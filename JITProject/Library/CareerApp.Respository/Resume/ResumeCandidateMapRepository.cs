﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IResumeCandidateMapRepository : IRepository<ResumeCandidateMap>
    {
        Task<IPagedList<ResumeCandidateMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel);
        Task<ResumeContent> GetResumeAsync(DataSourceCandidateRequestModel searchModel);
        Task<ResumeCandidateMapSelectBoxDataViewModel> GetSelectBoxData();
        Task<IPagedList<ResumeCandidateMapTemplateInfo>> GetCandidateAllAsync(DataSourceCandidateRequestModel searchModel);
    }

    public class ResumeCandidateMapRepository : BaseRepository<ResumeCandidateMap>, IResumeCandidateMapRepository
    {
        public ResumeCandidateMapRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<ResumeCandidateMapInfo>> GetAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT rcm.RowId as RowId, ResumeCandidateMapId, rcm.CandidateId as CandidateId,
                                    rcm.ResumeTemplateId as ResumeTemplateId, rcm.Resume as Resume, rcm.ResumeName as ResumeName,
                                    rcm.ResumeFile as ResumeFile, rcm.ResumeImage as ResumeImage, rcm.IsActive as IsActive
                            FROM CA_ResumeCandidateMap rcm
                            JOIN CA_Candidate can on rcm.CandidateId = can.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and rcm.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and rcm.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY rcm.RowId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ResumeCandidateMapInfo>(queries.query);
            return new DapperPagedList<ResumeCandidateMapInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }

        public async Task<ResumeContent> GetResumeAsync(DataSourceCandidateRequestModel searchModel)
        {
            ResumeContent objResumeContent = new ResumeContent();

            // Candidate Details
            string query = @"SELECT can.RowId as RowId, CandidateId, can.NamePrefixId as NamePrefixId, np.Title as NamePrefixTitle, FirstName, LastName, 
                                    can.GenderId as GenderId, gen.Title as GenderTitle, can.MartialStatusId as MartialStatusId, ms.Title as MartialStatusTitle, 
                                    can.Email as Email, can.PhoneNumber as PhoneNumber, can.CountryCode as CountryCode, can.TelephoneNumber as TelephoneNumber, can.PassportInformationId as PassportId, 
                                    pi.PassportNo as PassportNo, can.SeamanBookCdcId as SeamanBookCdcId, sbc.CdcNumber as CdcNumber, can.LicenceInformationId as LicenceId,
                                    li.LicenceNo as LicenceNo, PanNo, AadharNo, ResidenceId, PanDigiDocumentDetailId , AadharDigiDocumentDetailId, ResidenceDigiDocumentDetailId, 
                                    can.JobCategoryId as CategoryId, jc.Title as CategoryName, can.DesignationId as DesignationId, des.Title as DesignationName, 
                                    can.JobTypeId as JobTypeId, jt.Title as JobTypeName, CurrentCTC, ExpectedPackage, can.IndustryId as IndustryId, 
                                    ind.Title as IndustryName, can.FunctionalAreaId as FunctionalAreaId, fa.Title as FunctionalAreaName, 
                                    can.JobRoleId as JobRoleId, jr.Title as JobRoleName, Skills, ProfileSummary, PositionApplyingFor, Dob, CurrentAddress1, CurrentAddress2, 
                                    PermanantAddress1, PermanantAddress2, City, can.StateId as StateId, st.Name as StateName, 
                                    can.CountryId as CountryId, cn.Name as CountryName, ZipCode, UserId, can.IsActive as IsActive
                            FROM CA_Candidate can
                            LEFT JOIN CA_NamePrefix np on can.NamePrefixId = np.RowId
                            LEFT JOIN CA_Gender gen on can.GenderId = gen.RowId
                            LEFT JOIN CA_MartialStatus ms on can.MartialStatusId = ms.RowId
                            LEFT JOIN CA_LicenceInformation li on can.LicenceInformationId = li.RowId
                            LEFT JOIN CA_PassportInformation pi on can.PassportInformationId = pi.RowId
                            LEFT JOIN CA_SeamanBookCdc sbc on can.SeamanBookCdcId = sbc.RowId
                            LEFT JOIN CA_JobCategories jc on can.JobCategoryId = jc.RowId
                            LEFT JOIN CA_Designation des on can.DesignationId = des.RowId
                            LEFT JOIN CA_JobTypes jt on can.JobTypeId = jt.RowId
                            LEFT JOIN CA_Industries ind on can.IndustryId = ind.RowId
                            LEFT JOIN CA_FunctionalAreas fa on can.FunctionalAreaId = fa.RowId
                            LEFT JOIN CA_JobRoles jr on can.JobRoleId = jr.RowId
                            LEFT JOIN CA_States st on can.StateId = st.RowId
                            LEFT JOIN CA_Countries cn on can.CountryId = cn.RowId
                            WHERE 1 = 1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and can.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and can.RowId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY can.CandidateId";

            var candidate = await QueryAsync<CandidateInfo>(query);
            objResumeContent.CandidateInfo = candidate;

            // Candidate Relatives
            query = @"SELECT cr.RowId as RowId, CandidateRelativeId, cr.CandidateId as CandidateId,
                                    can.FirstName CandidateFirstName, can.LastName CandidateLastName, cr.NamePrefixId as NamePrefixId, np.Title as NamePrefixTitle,
                                    cr.FirstName RelativeFirstName,cr.LastName RelativeLastName, cr.GenderId as GenderId, gn.Title as GenderTitle, cr.Email as Email, 
                                    cr.CountryCode as CountryCode, cr.PhoneNumber as PhoneNumber, cr.TelephoneNumber as TelephoneNumber, cr.RelationshipId as RelationshipId, rs.Title as RelationshipTitle, 
                                    cr.IsActive as IsActive
                            FROM CA_CandidateRelatives cr
                            JOIN CA_Candidate can on cr.CandidateId = can.RowId
                            LEFT JOIN CA_NamePrefix np on cr.NamePrefixId = np.RowId
                            LEFT JOIN CA_Gender gn on cr.GenderId = gn.RowId
                            LEFT JOIN CA_Relationship rs on cr.RelationshipId = rs.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cr.FirstName";

            var candidateRelatives = await QueryAsync<CandidateRelativesInfo>(query);
            objResumeContent.CandidateRelativesInfo = candidateRelatives;

            // Candidate Skills
            query = @"SELECT cs.RowId as RowId, CandidateSkillId, cs.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, cs.SkillName as SkillName, cs.ProficiencyId as ProficiencyId, 
                                    pf.Title as ProficiencyTitle, cs.IsActive as IsActive
                            FROM CA_CandidateSkills cs
                            JOIN CA_Candidate can on cs.CandidateId = can.RowId
                            JOIN CA_Proficiency pf on cs.ProficiencyId = pf.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cs.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cs.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cs.SkillName";

            var candidateSkills = await QueryAsync<CandidateSkillsInfo>(query);
            objResumeContent.CandidateSkillsInfo = candidateSkills;

            // Candidate References
            query = @"SELECT cr.RowId as RowId, CandidateReferenceId, cr.CandidateId as CandidateId,
                             can.FirstName CandidateFirstName, can.LastName CandidateLastName, cr.NamePrefixId as NamePrefixId, 
                             np.Title as NamePrefixTitle, cr.FirstName ReferenceFirstName,cr.LastName ReferenceLastName, cr.Email as Email, 
                             cr.CountryCode as CountryCode, cr.PhoneNumber as PhoneNumber, cr.IsActive as IsActive
                            FROM CA_CandidateReferences cr
                            JOIN CA_Candidate can on cr.CandidateId = can.RowId
                            LEFT JOIN CA_NamePrefix np on cr.NamePrefixId = np.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cr.FirstName";

            var candidateReferences = await QueryAsync<CandidateReferencesInfo>(query);
            objResumeContent.CandidateReferencesInfo = candidateReferences;

            // Candidate Projects
            query = @"SELECT cp.RowId as RowId, CandidateProjectId, cp.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, cp.ProjectName as ProjectName, cp.TeamSize as TeamSize, 
                                    cp.Description as Description, cp.FromDate as FromDate, cp.ToDate as ToDate, 
                                    cp.IsActive as IsActive
                            FROM CA_CandidateProjects cp
                            JOIN CA_Candidate can on cp.CandidateId = can.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and cp.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cp.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cp.FromDate";

            var candidateProjects = await QueryAsync<CandidateProjectsInfo>(query);
            objResumeContent.CandidateProjectsInfo = candidateProjects;

            // Candidate Profile
            query = @"SELECT cpi.RowId as RowId, CandidateProfileImageId, cpi.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ImageUrl, Base64Image, cpi.IsActive as IsActive
                            FROM CA_CandidateProfileImage cpi
                            JOIN CA_Candidate can on cpi.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and cpi.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cpi.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY cpi.CreatedDate";

            var candidateProfile = await QueryAsync<CandidateProfileImageInfo>(query);
            objResumeContent.CandidateProfileImageInfo = candidateProfile;

            // Candidate Other Certificates
            query = @"SELECT coc.RowId as RowId, CandidateOtherCertificateId, coc.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, coc.TrainingId as TrainingId, DocumentName,
                                     DocumentNumber, ExpiryDate, ReminderOnExpiryFlag, coc.Certificate as Certificate,
                             coc.IsActive as IsActive
                            FROM CA_CandidateOtherCertificate coc
                            JOIN CA_Candidate can on coc.CandidateId = can.RowId
                            JOIN CA_Training tr on coc.TrainingId = tr.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and coc.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and coc.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY coc.CandidateId";

            var candidateOtherCertificate = await QueryAsync<CandidateOtherCertificateInfo>(query);
            objResumeContent.CandidateOtherCertificateInfo = candidateOtherCertificate;

            // Candidate Language Map
            query = @"SELECT clm.RowId as RowId, CandidateLanguageMapId, clm.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, clm.LanguageId as LanguageId, lan.Name as LanguageName, clm.IsActive as IsActive
                            FROM CA_CandidateLanguageMap clm
                            JOIN CA_Candidate can on clm.CandidateId = can.RowId
                            JOIN CA_Languages lan on clm.LanguageId = lan.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and clm.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and clm.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY lan.Name";

            var candidateLanguageMap = await QueryAsync<CandidateLanguageMapInfo>(query);
            objResumeContent.CandidateLanguageMapInfo = candidateLanguageMap;

            // Candidate Experience 
            query = @"SELECT ce.RowId as RowId, CandidateExperienceId, ce.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, EmployerName, Locationid, JobRole, FromDate, 
                                     ToDate, CurrentlyWorkHereFlag, Responsibilities, Achievements, ce.IsActive as IsActive
                        FROM CA_CandidateExperience ce
                        JOIN CA_Candidate can on ce.CandidateId = can.RowId
                        WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ce.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ce.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ce.FromDate";

            var candidateExperience = await QueryAsync<CandidateExperienceInfo>(query);
            objResumeContent.CandidateExperienceInfo = candidateExperience;

            // Candidate Education Certificate
            query = @"SELECT cec.RowId as RowId, CandidateEducationCertificateId, cec.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, cec.EducationQualificationId as EducationQualificationId,
                                     cec.Certificate as Certificate, eq.Course as Course, eq.University as University, 
                                    eq.Grade as Grade, eq.DateFrom as DateFrom, eq.DateTo as DateTo, cec.IsActive as IsActive
                            FROM CA_CandidateEducationCertificate cec
                            JOIN CA_Candidate can on cec.CandidateId = can.RowId
                            JOIN CA_EducationQualification eq on cec.EducationQualificationId = eq.RowId
                           WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and cec.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and cec.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY eq.DateFrom";

            var candidateEducationCertificate = await QueryAsync<CandidateEducationCertificateInfo>(query);
            objResumeContent.CandidateEducationCertificateInfo = candidateEducationCertificate;

            // Candidate Achievements
            query = @"SELECT ca.RowId as RowId, CandidateAchievementId, ce.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ca.CandidateExperienceId as CandidateExperienceId, ce.EmployerName as EmployerName, ca.Title as Title, ca.Description as Description, ca.IsActive as IsActive
                            FROM CA_CandidateAchievements ca
                            JOIN CA_CandidateExperience ce on ca.CandidateExperienceId = ce.RowId
                            JOIN CA_Candidate can on ce.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ca.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ce.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ce.EmployerName";

            var candidateAchievements = await QueryAsync<CandidateAchievementsInfo>(query);
            objResumeContent.CandidateAchievementsInfo = candidateAchievements;

            // Education Qualifications
            query = @"SELECT eq.RowId as RowId, EducationQualificationId, eq.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, Course, University, 
                                    Grade, DateFrom, DateTo, DigiDocumentDetailId, eq.IsActive as IsActive
                            FROM CA_EducationQualification eq
                            JOIN CA_Candidate can on eq.CandidateId = can.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and eq.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and eq.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY DateFrom";

            var educationQualification = await QueryAsync<EducationQualificationInfo>(query);
            objResumeContent.EducationQualificationInfo = educationQualification;

            // Social Accounts
            query = @"SELECT sa.RowId as RowId, SocialAccountId, sa.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, sa.Facebooks as Facebooks, sa.Google as Google, sa.Twitter as Twitter, 
                                    sa.LinkedIn as LinkedIn, sa.Pinterest as Pinterest, sa.Instagram as Instagram, sa.Other as Other, 
                                    sa.IsActive as IsActive
                            FROM CA_SocialAccounts sa
                            JOIN CA_Candidate can on sa.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and sa.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and sa.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY sa.CreatedDate DESC";

            var socialAccounts = await QueryAsync<SocialAccountsInfo>(query);
            objResumeContent.SocialAccountsInfo = socialAccounts;

            // Candidate Training
            query = @"SELECT tr.RowId as RowId, TrainingId, tr.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, TrainingCertificate, Institute, DigiDocumentDetailId, tr.IsActive as IsActive
                            FROM CA_Training tr
                            JOIN CA_Candidate can on tr.CandidateId = can.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and tr.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and tr.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY tr.CandidateId";

            var candidateTraining = await QueryAsync<TrainingInfo>(query);
            objResumeContent.TrainingInfo = candidateTraining;

            // Candidat Bank Details
            query = @"SELECT bd.RowId as RowId, BankDetailsId, bd.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, BankName, BranchName, AccountNo, IFSCIBANSWIFTNo,
                                    bd.BankAccountTypeId as BankAccountTypeId, bat.Title as BankAccountTypeTitle, bd.StateId as StateId, 
                                    st.Name as StateName,  bd.CountryId as CountryId, cn.Name as CountryName, bd.IsActive as IsActive
                            FROM CA_BankDetails bd
                            JOIN CA_Candidate can on bd.CandidateId = can.RowId
                            JOIN CA_BankAccountType bat on bd.BankAccountTypeId = bat.RowId
                            JOIN CA_States st on bd.StateId = st.RowId
                            JOIN CA_Countries cn on bd.CountryId = cn.RowId
                            WHERE 1=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and bd.IsActive=1";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and bd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY bd.BankName";

            var candidatBankDetails = await QueryAsync<BankDetailsInfo>(query);
            objResumeContent.BankDetailsInfo = candidatBankDetails;

            return objResumeContent;
        }

        public async Task<ResumeCandidateMapSelectBoxDataViewModel> GetSelectBoxData()
        {
            ResumeCandidateMapSelectBoxDataViewModel result = new ResumeCandidateMapSelectBoxDataViewModel
            {
                ResumeTemplateId = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value,Title  as Caption From CA_ResumeTemplates where IsActive=1"),
            };
            return result;
        }

        public async Task<IPagedList<ResumeCandidateMapTemplateInfo>> GetCandidateAllAsync(DataSourceCandidateRequestModel searchModel)
        {
            string query = @"SELECT rcm.RowId as RowId, ResumeCandidateMapId, rcm.CandidateId as CandidateId,
                                    rcm.ResumeTemplateId as ResumeTemplateId, rcm.Resume as Resume, rcm.ResumeName as ResumeName,
                                    rcm.ResumeFile as ResumeFile, rcm.ResumeImage as CandidateResumeImage,  rcm.IsActive as IsActive,
                                    rt.Title as Title ,rt.Description as Description,rt.ResumeContent as ResumeContent,
                                    rt.ResumeImage as ResumeImage,rcm.CreatedDate as CreatedDate,rcm.UpdatedDate as UpdatedDate
                            FROM CA_ResumeCandidateMap rcm
                            INNER JOIN CA_ResumeTemplates rt on rcm.ResumeTemplateId = rt.RowId
                            WHERE 1=1 and rcm.IsActive=1";

            /*if (!searchModel.ShowInactive)
            {
                query += " and rcm.IsActive=1";
            }*/
            if (searchModel.CandidateId > 0)
            {
                query += $" and rcm.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY rcm.RowId";

            var queries = GetPagingQueries(query, searchModel.Page, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<ResumeCandidateMapTemplateInfo>(queries.query);
            return new DapperPagedList<ResumeCandidateMapTemplateInfo>(data, count, searchModel.Page, searchModel.PageSize);
        }


    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IFieldOfExpertiseRepository : IRepository<FieldOfExpertise>
    {
        Task<IPagedList<FieldOfExpertise>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
    }

    public class FieldOfExpertiseRepository : BaseRepository<FieldOfExpertise>, IFieldOfExpertiseRepository
    {
        public FieldOfExpertiseRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<FieldOfExpertise>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<FieldOfExpertise> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.Title.Contains(searchWord));
            }

            query = query.OrderBy(c => c.Title);
            return await Task.Run(() => new PagedList<FieldOfExpertise>(query, pageIndex, pageSize));
        }

    }
}

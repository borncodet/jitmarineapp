﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IDashboardListRepository : IRepository<DashboardList>
    {
        Task<IPagedList<DashboardList>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false);
        Task<DashboardListSelectBoxDataViewModel> GetSelectBoxDataAsync();
    }

    public class DashboardListRepository : BaseRepository<DashboardList>, IDashboardListRepository
    {
        public DashboardListRepository(
        IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }


        public async Task<IPagedList<DashboardList>> GetAllAsync(string searchWord = null, int pageIndex = 0, int pageSize = int.MaxValue, bool showInactive = false)
        {
            IQueryable<DashboardList> query = Table;
            if (!showInactive)
            {
                query = query.Where(c => c.IsActive);
            }

            if (!string.IsNullOrEmpty(searchWord))
            {
                query = query.Where(c => c.DashboardName.Contains(searchWord));
            }

            query = query.OrderBy(c => c.DashboardName);
            return await Task.Run(() => new PagedList<DashboardList>(query, pageIndex, pageSize));
        }

        public async Task<DashboardListSelectBoxDataViewModel> GetSelectBoxDataAsync()
        {
            DashboardListSelectBoxDataViewModel result = new DashboardListSelectBoxDataViewModel
            {
                DashboardList = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, DashboardName as Caption From CA_DashboardList where IsActive=1"),
            };
            return result;
        }
    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IDigiDocumentSharedRepository : IRepository<DigiDocumentShared>
    {
        Task<IPagedList<DigiDocumentSharedInfo>> GetAllAsync(BaseCandidateSearchViewModel searchModel);
        Task<DigiDocumentSharedSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class DigiDocumentSharedRepository : BaseRepository<DigiDocumentShared>, IDigiDocumentSharedRepository
    {
        public DigiDocumentSharedRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<DigiDocumentSharedInfo>> GetAllAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = @"SELECT dds.RowId as RowId, DigiDocumentSharedId, dds.DigiDocumentDetailId as DigiDocumentDetailId,
                                    dds.SharedThrough as SharedThrough, dds.IsActive as IsActive
                            FROM CA_DigiDocumentShared dds
                            JOIN CA_DigiDocumentDetails ddd on dds.DigiDocumentDetailId = ddd.RowId
                            JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId 
                            WHERE 1=1";
            if (!searchModel.ShowInactive)
            {
                query += " and dds.IsActive=1";
            }
            if (searchModel.DigiDocumentTypeId > 0)
            {
                query += $" and ddd.DigiDocumentTypeId ='{searchModel.DigiDocumentTypeId}'";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ddd.Name";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<DigiDocumentSharedInfo>(queries.query);
            return new DapperPagedList<DigiDocumentSharedInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<DigiDocumentSharedSelectBoxDataViewModel> GetSelectBoxData()
        {
            DigiDocumentSharedSelectBoxDataViewModel result = new DigiDocumentSharedSelectBoxDataViewModel
            {

            };
            return result;
        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models.DigiLocker;
using CareerApp.Respository.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareerApp.Respository.DigiLocker
{
    public interface IDigiDocumentDownloadRepository : IRepository<DigiDocumentDownload>
    {
    }

    public class DigiDocumentDownloadRepository : BaseRepository<DigiDocumentDownload>, IDigiDocumentDownloadRepository
    {
        public DigiDocumentDownloadRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }
    }
}

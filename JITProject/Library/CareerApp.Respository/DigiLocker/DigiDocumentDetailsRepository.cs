﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IDigiDocumentDetailsRepository : IRepository<DigiDocumentDetails>
    {
        Task<IPagedList<DigiDocumentDetailsInfo>> GetAllAsync(BaseCandidateSearchViewModel searchModel);
        Task<IPagedList<DigiDocumentDetailsInfo>> GetExpiryDocumentAsync(BaseCandidateSearchViewModel searchModel);
        Task<IPagedList<DigiDocumentCountInfo>> GetDigiDocumentCountAsync(BaseCandidateSearchViewModel searchModel);
        Task<IPagedList<DigiDocumentCountInfo>> GetExpiryDocumentCountAsync(BaseCandidateSearchViewModel searchModel);
        Task<DigiDocumentDetailsSelectBoxDataViewModel> GetSelectBoxData();
        Task<DigiDocumentDetailsEditModel> GetDigiDocumentDetailsById(long id);
    }

    public class DigiDocumentDetailsRepository : BaseRepository<DigiDocumentDetails>, IDigiDocumentDetailsRepository
    {
        public DigiDocumentDetailsRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<DigiDocumentDetailsInfo>> GetAllAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = @"SELECT ddd.RowId as RowId, ddu.DigiDocumentDetailId, ddd.Name as Name, ddd.DocumentNumber as DocumentNumber, ddd.ExpiryFlag as ExpiryFlag, 
                                    ddd.Description as Description, ddd.CandidateId as CandidateId,
                                    RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                         ) AS CandidateName, ddd.DigiDocumentTypeId as DigiDocumentTypeId, ddt.Title as DigiDocumentTypeTitle, ddd.ExpiryDate as ExpiryDate, 
                                    ddd.UpdatedDate as UpdatedDate, ddd.IsActive as IsActive,ddu.DigiDocument as DigiDocument
                            FROM CA_DigiDocumentDetails ddd
                            JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                            JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                            JOIN CA_DigiDocumentUpload ddu on ddd.RowId = ddu.DigiDocumentDetailId and ddu.IsActive=1 
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.DigiDocumentTypeId > 0)
            {
                query += $" and ddd.DigiDocumentTypeId ='{searchModel.DigiDocumentTypeId}'";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ddd.Name";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<DigiDocumentDetailsInfo>(queries.query);
            return new DapperPagedList<DigiDocumentDetailsInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<IPagedList<DigiDocumentDetailsInfo>> GetExpiryDocumentAsync(BaseCandidateSearchViewModel searchModel)
        {
            bool isActive = !searchModel.ShowInactive;
            string query = $@"SELECT ddd.RowId as RowId, ddd.DigiDocumentDetailId, ddd.Name as Name, ddd.DocumentNumber as DocumentNumber, ddd.ExpiryFlag as ExpiryFlag, 
                                     ddd.Description as Description, ddd.CandidateId as CandidateId,
                                     RTRIM(
                                          LTRIM(
                                                CONCAT(
                                                         COALESCE(can.FirstName + ' ', ''),
                                                         COALESCE(can.LastName + ' ', '')
                                                      )
                                               )
                                          ) AS CandidateName, ddd.DigiDocumentTypeId as DigiDocumentTypeId, ddt.Title as DigiDocumentTypeTitle, ddd.ExpiryDate as ExpiryDate,
                                      ddd.UpdatedDate as UpdatedDate, CAST(CASE WHEN  datediff(DAY, GETDATE(), ddd.ExpiryDate) <=0 THEN 1 ELSE 0 END AS BIT) AS IsExpired,
                                      ddd.IsActive as IsActive,
                                      ddu.DigiDocument as DigiDocument
                            FROM CA_DigiDocumentDetails ddd
                            JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                            JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                            JOIN CA_DigiDocumentUpload ddu on ddd.RowId = ddu.DigiDocumentDetailId and ddu.IsActive=1
                            WHERE datediff(DAY, GETDATE(), ddd.ExpiryDate) <=15 and  ddd.ExpiryFlag=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.DigiDocumentTypeId > 0)
            {
                query += $" and ddd.DigiDocumentTypeId ='{searchModel.DigiDocumentTypeId}'";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ddd.Name";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<DigiDocumentDetailsInfo>(queries.query);
            return new DapperPagedList<DigiDocumentDetailsInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<IPagedList<DigiDocumentCountInfo>> GetDigiDocumentCountAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = $@"SELECT COUNT(*) as DocumentCount, ddt.Title as DocumentTypeTitle
                              FROM CA_DigiDocumentDetails ddd
                              JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                              JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                              WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.DigiDocumentTypeId > 0)
            {
                query += $" and ddd.DigiDocumentTypeId ='{searchModel.DigiDocumentTypeId}'";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += $" GROUP BY ddt.Title";
            query += "  ORDER BY ddt.Title";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<DigiDocumentCountInfo>(queries.query);
            return new DapperPagedList<DigiDocumentCountInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<IPagedList<DigiDocumentCountInfo>> GetExpiryDocumentCountAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = $@"SELECT ddt.Title as DocumentTypeTitle, COUNT(*) as DocumentCount
                              FROM CA_DigiDocumentDetails ddd
                              JOIN CA_Candidate can on ddd.CandidateId = can.RowId
                              JOIN CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId = ddt.RowId
                              WHERE datediff(DAY, GETDATE(), ddd.ExpiryDate) <=15 and  ddd.ExpiryFlag=1 ";

            if (!searchModel.ShowInactive)
            {
                query += " and ddd.IsActive=1";
            }
            if (searchModel.DigiDocumentTypeId > 0)
            {
                query += $" and ddd.DigiDocumentTypeId ='{searchModel.DigiDocumentTypeId}'";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += $" GROUP BY ddt.Title";
            query += "  ORDER BY ddt.Title";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<DigiDocumentCountInfo>(queries.query);
            return new DapperPagedList<DigiDocumentCountInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<DigiDocumentDetailsSelectBoxDataViewModel> GetSelectBoxData()
        {
            DigiDocumentDetailsSelectBoxDataViewModel result = new DigiDocumentDetailsSelectBoxDataViewModel
            {
                DigiDocumentTypes = await QueryAsync<ValueCaptionPair>("Select 0 as Value, CAST('--------Select--------' AS varchar) as Caption UNION Select RowId as Value, Title as Caption From CA_DigiDocumentTypes where IsActive=1"),
            };
            return result;
        }

        public async Task<DigiDocumentDetailsEditModel> GetDigiDocumentDetailsById(long id)
        {
            var query = @"select ddd.RowId,ddd.Name,ddd.DocumentNumber,ddd.Description,ddd.CandidateId,ddd.DigiDocumentTypeId,
                        ddd.ExpiryDate,ddd.UpdatedDate,ddd.ExpiryFlag,ddd.IsActive,ddt.Title as DigiDocumentTypeTitle,ddu.DigiDocument
                        from CA_DigiDocumentDetails ddd 
                        join CA_DigiDocumentTypes ddt on ddd.DigiDocumentTypeId= ddt.RowId
                        left join CA_DigiDocumentUpload ddu on ddu.DigiDocumentDetailId = ddd.RowId
                        where ddd.RowId=@digiDocumentDetailId and ddd.IsActive = 1";

            var parameters = new { digiDocumentDetailId = id };
            return (await QueryAsync<DigiDocumentDetailsEditModel>(query, parameters)).FirstOrDefault();

        }

    }
}

﻿using CareerApp.Core.DB;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CareerApp.Respository
{
    public interface IDigiDocumentUploadRepository : IRepository<DigiDocumentUpload>
    {
        Task<IPagedList<DigiDocumentUploadInfo>> GetAllAsync(BaseCandidateSearchViewModel searchModel);
        Task<DigiDocumentUploadSelectBoxDataViewModel> GetSelectBoxData();
    }

    public class DigiDocumentUploadRepository : BaseRepository<DigiDocumentUpload>, IDigiDocumentUploadRepository
    {
        public DigiDocumentUploadRepository(
       IConfiguration config,
        ApplicationDbContext context) : base(config, context)
        { }

        public async Task<IPagedList<DigiDocumentUploadInfo>> GetAllAsync(BaseCandidateSearchViewModel searchModel)
        {
            string query = @"SELECT ddu.RowId as RowId, DigiDocumentUploadId, ddu.DigiDocumentDetailId as DigiDocumentDetailId,
                                    ddu.DigiDocument as DigiDocument, ddu.IsActive as IsActive
                            FROM CA_DigiDocumentUpload ddu
                            JOIN CA_DigiDocumentDetails ddd on ddu.DigiDocumentDetailId = ddd.RowId
                            WHERE 1=1";

            if (!searchModel.ShowInactive)
            {
                query += " and ddu.IsActive=1";
            }
            if (searchModel.DigiDocumentTypeId > 0)
            {
                query += $" and ddd.DigiDocumentTypeId ='{searchModel.DigiDocumentTypeId}'";
            }
            if (searchModel.CandidateId > 0)
            {
                query += $" and ddd.CandidateId ='{searchModel.CandidateId}'";
            }
            query += "  ORDER BY ddd.Name";

            var queries = GetPagingQueries(query, searchModel.PageIndex, searchModel.PageSize);
            var count = await ExecuteScalarAsync(queries.countQuery);
            var data = await QueryAsync<DigiDocumentUploadInfo>(queries.query);
            return new DapperPagedList<DigiDocumentUploadInfo>(data, count, searchModel.PageIndex, searchModel.PageSize);
        }

        public async Task<DigiDocumentUploadSelectBoxDataViewModel> GetSelectBoxData()
        {
            DigiDocumentUploadSelectBoxDataViewModel result = new DigiDocumentUploadSelectBoxDataViewModel
            {

            };
            return result;
        }

    }
}

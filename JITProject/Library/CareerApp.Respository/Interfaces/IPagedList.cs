﻿using System.Collections.Generic;

namespace CareerApp.Respository.Interfaces
{
    public interface IPagedList<T> : IPagedList, IList<T>
	{

	}

	public interface IPagedList
	{
		int PageIndex { get; }
		int PageSize { get; }
		int TotalCount { get; }
		int TotalPages { get; }
		bool HasPreviousPage { get; }
		bool HasNextPage { get; }
	}
}

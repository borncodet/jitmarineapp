﻿using CareerApp.Core.DB;
using CareerApp.Respository.DigiLocker;
using System;
using System.Threading.Tasks;


namespace CareerApp.Respository.Interface
{
    public partial interface IUnitOfWork : IDisposable
    {
        ApplicationDbContext Context { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();

        ICandidateRepository Candidates { get; }
        IBankAccountTypeRepository BankAccountTypes { get; }
        IBankDetailsRepository BankDetails { get; }
        ICandidateAchievementsRepository CandidateAchievements { get; }
        ICandidateEducationCertificateRepository CandidateEducationCertificates { get; }
        ICandidateExperienceRepository CandidateExperiences { get; }
        ICandidateOtherCertificateRepository CandidateOtherCertificates { get; }
        ICandidateProfileImageRepository CandidateProfileImage { get; }
        ICandidateProjectsRepository CandidateProjects { get; }
        ICandidateRelativesRepository CandidateRelatives { get; }
        ICandidateReferencesRepository CandidateReferences { get; }
        ICandidateSkillsRepository CandidateSkills { get; }
        IEducationQualificationRepository EducationQualifications { get; }
        IProjectRoleRepository ProjectRoles { get; }
        IRelationshipRepository Relationships { get; }
        ISocialAccountsRepository SocialAccounts { get; }
        ITrainingRepository Trainings { get; }
        ITrainingDurationMapRepository TrainingDurationMaps { get; }
        ITrainingRenewalRepository TrainingRenewals { get; }
        IUniversityRepository Universities { get; }
        IJobAppliedRepository JobApplied { get; }
        IJobAppliedDetailsRepository JobAppliedDetails { get; }
        IJobAppliedDigiDocMapRepository JobAppliedDigiDocMap { get; }
        IJobBookmarkedRepository JobBookmarked { get; }
        IJobSharedRepository JobShared { get; }
        IJobPostAlertRepository JobPostAlert { get; }
        IJobAlertTypeMapRepository JobAlertTypeMap { get; }
        IJobAlertRoleMapRepository JobAlertRoleMap { get; }
        IJobAlertIndustryMapRepository JobAlertIndustryMap { get; }
        IJobAlertExperienceMapRepository JobAlertExperienceMap { get; }
        IJobAlertCategoryMapRepository JobAlertCategoryMap { get; }
        ICandidateLanguageMapRepository CandidateLanguageMap { get; }
        ILicenceInformationRepository LicenceInformations { get; }
        IPassportInformationRepository PassportInformations { get; }
        ISeamanBookCdcRepository SeamanBookCdcs { get; }

        IDigiDocumentTypeRepository DigiDocumentTypes { get; }
        IDigiDocumentDetailsRepository DigiDocumentDetails { get; }
        IDigiDocumentSharedRepository DigiDocumentShared { get; }
        IDigiDocumentUploadRepository DigiDocumentUpload { get; }

        IDashboardListRepository DashboardList { get; }

        IFieldOfExpertiseRepository FieldOfExpertises { get; }
        IResumeTemplateRepository ResumeTemplates { get; }
        IResumeCandidateMapRepository ResumeCandidateMap { get; }
        IResumeDesignationMapRepository ResumeDesignationMap { get; }
        IResumeExperienceMapRepository ResumeExperienceMap { get; }
        IResumeExpertiseMapRepository ResumeExpertiseMap { get; }

        IDesignationRepository Designations { get; }
        IGenderRepository Genders { get; }
        IMartialStatusRepository MartialStatus { get; }
        INamePrefixRepository NamePrefix { get; }
        ICountryRepository Countries { get; }
        ICurrencyRepository Currencies { get; }
        ICurrencyValueRepository CurrencyValues { get; }
        ILanguageRepository Languages { get; }
        IStateRepository States { get; }
        IDurationRepository Durations { get; }
        IDatePostedRepository DatePosted { get; }
        IProficiencyRepository Proficiencies { get; }

        IMediaRepository Medias { get; }
        IEmployerRepository Employers { get; }
        IVendorRepository Vendors { get; }
        IVendorDocumentRepository VendorDocuments { get; }
        IVendorsStatusRepository VendorsStatus { get; }
        IVendorProfileImageRepository VendorProfileImage { get; }
        IVendorJobListRepository VendorJobLists { get; }
        IJobByVendorRepository JobByVendors { get; }

        ICourseRepository Courses { get; }
        IExpereinceTypeRepository ExpereinceTypes { get; }
        IFunctionalAreaRepository FunctionalAreas { get; }
        IIndustryRepository Industries { get; }
        IJobByAdminRepository JobByAdmin { get; }
        IJobCategoryRepository JobCategories { get; }
        IJobCourseMapRepository JobCourseMap { get; }
        IJobLanguageMapRepository JobLanguageMap { get; }
        IJobListRepository JobLists { get; }
        IJobRequiredDocMapRepository JobRequiredDocMap { get; }
        IJobShiftMapRepository JobShiftMap { get; }
        IJobTypeRepository JobTypes { get; }
        IJobRoleRepository JobRoles { get; }
        IRequiredDocumentTypeRepository RequiredDocumentTypes { get; }
        IShiftAvailableRepository ShiftAvailable { get; }
        IDigiDocumentDownloadRepository DigiDocumentDownload { get; }
    }
}

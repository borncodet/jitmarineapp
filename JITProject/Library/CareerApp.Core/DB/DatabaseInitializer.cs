﻿using CareerApp.Core.DB.Interfaces;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.Core.DB
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly IAccountManager _accountManager;
        private readonly ILogger _logger;

        public DatabaseInitializer(ApplicationDbContext context, IAccountManager accountManager, ILogger<DatabaseInitializer> logger)
        {
            _accountManager = accountManager;
            _context = context;
            _logger = logger;
        }

        public virtual async Task SeedAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (!await _context.Roles.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt roles");

                await EnsureRoleAsync("SuperAdmin", "Super Administrator", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync("Vendor", "Vendorn", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync("Employee", "Company Employee", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync("Candidate", "Candidate", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync("WebGuest", "Web Guest", ApplicationPermissions.GetGuestPermissionValues());
                await EnsureRoleAsync("MobileGuest", "Mobile Guest", ApplicationPermissions.GetGuestPermissionValues());

                _logger.LogInformation("Inbuilt roles creation completed");
            }

            if (!await _context.Users.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt accounts");

                await CreateUserAsync("bct", "Bct@2020", "Site", "Super Administrator", "borncode@gmail.com", "9562161303", "+91", new string[] { "SuperAdmin" });

                _logger.LogInformation("Inbuilt account generation completed");
            }

            if (!await _context.Currencies.AnyAsync())
            {
                _context.Currencies.Add(new Currency()
                {
                    CurrencyId = 0001,
                    CurrencySymbol = "₹",
                    DecimalPoints = 2,
                    MainSuffix = "Rupees",
                    SubSuffix = "Paisa",
                    Name = "Rupees",
                    CurrencyCode = "INR",
                    Rate = 1,
                    IsActive = true
                });
                _context.SaveChanges();
            }

            if (!await _context.Countries.AnyAsync())
            {
                _context.Countries.Add(new Country()
                {
                    CountryId = 1,
                    Name = "India",
                    CurrencyId = 1,
                    CountryCode = "+91",
                    IsActive = true
                });
                _context.SaveChanges();
            }

            if (!await _context.States.AnyAsync())
            {
                _context.States.Add(new State()
                {
                    StateId = 1,
                    Name = "Kerala",
                    CountryId = 1,
                    IsActive = true
                });
                _context.SaveChanges();
            }

            if (!await _context.VendorsStatus.AnyAsync())
            {
                _context.VendorsStatus.Add(new VendorsStatus()
                {
                    VendorsStatusId = 1,
                    Title = "Submitted",
                    Description = "",
                    IsActive = true
                });
                _context.VendorsStatus.Add(new VendorsStatus()
                {
                    VendorsStatusId = 2,
                    Title = "Processing",
                    Description = "",
                    IsActive = true
                });
                _context.VendorsStatus.Add(new VendorsStatus()
                {
                    VendorsStatusId = 3,
                    Title = "Approved",
                    Description = "",
                    IsActive = true
                });
                _context.VendorsStatus.Add(new VendorsStatus()
                {
                    VendorsStatusId = 4,
                    Title = "Rejected",
                    Description = "",
                    IsActive = true
                });
                _context.VendorsStatus.Add(new VendorsStatus()
                {
                    VendorsStatusId = 5,
                    Title = "Verified",
                    Description = "",
                    IsActive = true
                });
                _context.VendorsStatus.Add(new VendorsStatus()
                {
                    VendorsStatusId = 1,
                    Title = "Not Verified",
                    Description = "",
                    IsActive = true
                });
                _context.SaveChanges();
            }

        }

        private async Task EnsureRoleAsync(string roleName, string description, string[] claims)
        {
            if ((await _accountManager.GetRoleByNameAsync(roleName)) == null)
            {
                ApplicationRole applicationRole = new ApplicationRole(roleName, description);

                var (Succeeded, Errors) = await this._accountManager.CreateRoleAsync(applicationRole, claims);

                if (!Succeeded)
                {
                    throw new Exception($"Seeding \"{description}\" role failed. Errors: {string.Join(Environment.NewLine, Errors)}");
                }
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string userName, string password, string firstName, string lastName, string email, string phoneNumber, string countrycode, string[] roles)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserId = 1,
                RoleId = 1,
                EmployerFlag = true,
                VendorFlag = true,
                EmployeeFlag = true,
                CandidateFlag = false,
                IsActive = true,
                Email = email,
                PhoneNumber = phoneNumber,
                CountryCode = countrycode,
                UserName = userName,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow

            };

            var (Succeeded, Errors) = await _accountManager.CreateUserAsync(applicationUser, roles, password);

            if (!Succeeded)
            {
                throw new Exception($"Seeding \"{userName}\" user failed. Errors: {string.Join(Environment.NewLine, Errors)}");
            }

            return applicationUser;
        }

        private async Task CreateMembershipTypeAsync(string name, string description)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.UserName == "nishila2");
            await _context.SaveChangesAsync();
        }
    }
}

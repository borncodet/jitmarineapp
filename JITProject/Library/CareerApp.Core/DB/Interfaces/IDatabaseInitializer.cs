﻿using System.Threading.Tasks;

namespace CareerApp.Core.DB.Interfaces
{
	public interface IDatabaseInitializer
	{
		Task SeedAsync();
	}
}

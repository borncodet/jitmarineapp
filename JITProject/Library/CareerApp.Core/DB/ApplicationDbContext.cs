﻿using CareerApp.Core.Models;
using CareerApp.Core.Models.Accounts;
using CareerApp.Core.Models.DigiLocker;
using CareerApp.Core.Models.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CareerApp.Core.DB
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public int CurrentUserId { get; set; }

        public ILoggerFactory LoggerFactory { get; }
        public IHttpContextAccessor ContextAccessor { get; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            LoggerFactory = this.GetService<ILoggerFactory>();
            ContextAccessor = this.GetService<IHttpContextAccessor>();
        }

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<MartialStatus> MartialStatus { get; set; }
        public DbSet<NamePrefix> NamePrefix { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<JobRole> JobRoles { get; set; }
        public DbSet<LicenceInformation> LicenceInformations { get; set; }
        public DbSet<PassportInformation> PassportInformations { get; set; }
        public DbSet<SeamanBookCdc> SeamanBookCdc { get; set; }
        public DbSet<LoggedDeviceHistory> LoggedDeviceHistorys { get; set; }
        public DbSet<Employer> Employer { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorDocument> VendorDocuments { get; set; }
        public DbSet<VendorsStatus> VendorsStatus { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CurrencyValue> CurrencyValues { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<ExpereinceType> ExpereinceTypes { get; set; }
        public DbSet<FunctionalArea> FunctionalAreas { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<JobByAdmin> JobByAdmins { get; set; }
        public DbSet<JobByVendor> JobByVendors { get; set; }
        public DbSet<JobCategory> JobCategories { get; set; }
        public DbSet<JobCourseMap> JobCourseMap { get; set; }
        public DbSet<JobLanguageMap> JobLanguageMap { get; set; }
        public DbSet<JobList> JobLists { get; set; }
        public DbSet<JobAppliedDetails> JobAppliedDetails { get; set; }
        public DbSet<JobAppliedDigiDocMap> JobAppliedDigiDocMap { get; set; }
        public DbSet<VendorJobList> VendorJobLists { get; set; }
        public DbSet<JobRequiredDocMap> JobRequiredDocMap { get; set; }
        public DbSet<JobShiftMap> JobShiftMap { get; set; }
        public DbSet<JobType> JobTypes { get; set; }
        public DbSet<RequiredDocumentType> RequiredDocumentTypes { get; set; }
        public DbSet<ShiftAvailable> ShiftAvailable { get; set; }
        public DbSet<DatePosted> DatePosted { get; set; }
        public DbSet<DigiDocumentDownload> DigiDocumentDownload { get; set; }
        public DbSet<DashboardList> DashboardList { get; set; }
        //public DbSet<ResumeTemplate> ResumeTemplates { get; set; }
        //public DbSet<FieldOfExpertise> FieldOfExpertise { get; set; }
        //public DbSet<ResumeCandidateMap> ResumeCandidateMap { get; set; }
        //public DbSet<ResumeDesignationMap> ResumeDesignationMap { get; set; }
        //public DbSet<ResumeExperienceMap> ResumeExperienceMap { get; set; }
        //public DbSet<ResumeExpertiseMap> ResumeExpertiseMap { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (Microsoft.EntityFrameworkCore.Metadata.IMutableEntityType entityType in builder.Model.GetEntityTypes())
            {
                entityType.GetForeignKeys()
                        .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade)
                        .ToList()
                        .ForEach(fk => fk.DeleteBehavior = DeleteBehavior.Restrict);
            }

            builder.Ignore<IdentityUserLogin<string>>();
            builder.Ignore<IdentityUserRole<string>>();
            builder.Ignore<IdentityUserClaim<string>>();
            builder.Ignore<IdentityUserToken<string>>();
            builder.Ignore<IdentityUser<string>>();
            builder.Ignore<IdentityRoleClaim<string>>();
            //builder.Ignore<ApplicationUser>();

            // builder.Entity<ApplicationUser>()
            //.HasIndex(u => u.Email)
            //.IsUnique();

            // builder.Entity<ApplicationUser>()
            // .HasKey(a => new { a.CountryCode, a.PhoneNumber });

            base.OnModelCreating(builder);

            //builder.Entity<ApplicationUser>(entity =>
            //{
            //    // Added these two lines
            //    var index1 = entity.HasIndex(u => new { u.UserName }).Metadata;
            //    entity.Metadata.RemoveIndex(index1.Properties);

            //    var index2 = entity.HasIndex(u => new { u.NormalizedUserName }).Metadata;
            //    entity.Metadata.RemoveIndex(index2.Properties);
            //});


            builder.Entity<ApplicationUser>(i =>
            {
                i.ToTable("CA_Users");
                i.HasKey(x => x.Id);
            });

            builder.Entity<ApplicationRole>(i =>
            {
                i.ToTable("CA_Role");
                i.HasKey(x => x.Id);
            });
            builder.Entity<IdentityUserRole<int>>(i =>
            {
                i.ToTable("CA_UserRole");
                i.HasKey(x => new { x.RoleId, x.UserId });
            });
            builder.Entity<IdentityUserLogin<int>>(i =>
            {
                i.ToTable("CA_UserLogin");
                i.HasKey(x => new { x.ProviderKey, x.LoginProvider });
            });
            builder.Entity<IdentityRoleClaim<int>>(i =>
            {
                i.ToTable("CA_RoleClaims");
                i.HasKey(x => x.Id);
            });
            builder.Entity<IdentityUserClaim<int>>(i =>
            {
                i.ToTable("CA_UserClaims");
                i.HasKey(x => x.Id);
            });
            builder.Entity<IdentityUserToken<int>>(i =>
            {
                i.ToTable("CA_UserTokens");
            });
        }

        public override int SaveChanges()
        {
            UpdateCareerEntities();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateCareerEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateCareerEntities();
            return base.SaveChangesAsync(cancellationToken);
        }

        public Task<int> SaveChangesAsync()
        {
            UpdateCareerEntities();
            return base.SaveChangesAsync();
        }

        private void UpdateCareerEntities()
        {
            IEnumerable<EntityEntry> modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is ICareerEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (EntityEntry entry in modifiedEntries)
            {
                ICareerEntity entity = (ICareerEntity)entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    if (entry.Entity is IBaseEntity baseEntity)
                    {
                        //baseEntity.RowId = Guid.NewGuid().ToString();
                        //Random random = new Random();
                        //baseEntity.RowId = random.Next();
                    }
                    entity.CreatedDate = now;
                    entity.CreatedBy = CurrentUserId.ToString();
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                if (entry.Entity is IHostEntity hostEntity)
                {
                    hostEntity.Host = GetClientIp();
                }

                entity.UpdatedDate = now;
                entity.UpdatedBy = CurrentUserId.ToString();
            }
        }

        private string GetClientIp()
        {
            return Convert.ToString(ContextAccessor.HttpContext?.Connection?.RemoteIpAddress);
        }
    }
}

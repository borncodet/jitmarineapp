﻿namespace CareerApp.Core.Constants
{
    public static class PropertyConstants
    {
        public const string FirstName = "firstname";

        public const string LastName = "lastname";

        public const string Configuration = "configuration";

        public const string ExtensionData = "extensiondata";
    }
}

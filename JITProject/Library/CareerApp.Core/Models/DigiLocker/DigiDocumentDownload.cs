﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CareerApp.Core.Models.DigiLocker
{
    [Table(name: "CA_DigiDocumentDownload",Schema ="dbo")]
    public class DigiDocumentDownload : BaseEntity
    {
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }

        [Required]
        public string AccessKey { get; set; }
        public DateTimeOffset Expires { get; set; }

        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

    }
}

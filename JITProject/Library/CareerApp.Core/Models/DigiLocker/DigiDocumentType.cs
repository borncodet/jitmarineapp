﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_DigiDocumentTypes")]
    public class DigiDocumentType : BaseEntity
    {
        [Required]
        public int DigiDocumentTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("DigiDocumentTypeId")]
        public ICollection<DigiDocumentDetails> DigiDocumentDetails { get; set; }
    }
}

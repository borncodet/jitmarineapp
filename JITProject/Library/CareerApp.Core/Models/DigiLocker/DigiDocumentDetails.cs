﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_DigiDocumentDetails")]
    public class DigiDocumentDetails : BaseEntity
    {
        [Required]
        public int DigiDocumentDetailId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string DocumentNumber { get; set; }
        public string Description { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        public int DigiDocumentTypeId { get; set; }
        public DigiDocumentType DigiDocumentType { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool ExpiryFlag { get; set; }

        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<DigiDocumentUpload> DigiDocumentUpload { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<DigiDocumentShared> DigiDocumentShared { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<PassportInformation> PassportInformation { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<Training> Training { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<EducationQualification> EducationQualification { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<BankDetails> BankDetails { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<SeamanBookCdc> SeamanBookCdc { get; set; }
        [ForeignKey("DigiDocumentDetailId")]
        public ICollection<JobAppliedDigiDocMap> JobAppliedDigiDocMap { get; set; }
    }
}

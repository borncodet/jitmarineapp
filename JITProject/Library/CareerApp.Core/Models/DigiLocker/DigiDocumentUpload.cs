﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_DigiDocumentUpload")]
    public class DigiDocumentUpload : BaseEntity
    {
        [Required]
        public int DigiDocumentUploadId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }
        [Required]
        public string DigiDocument { get; set; }
    }
}

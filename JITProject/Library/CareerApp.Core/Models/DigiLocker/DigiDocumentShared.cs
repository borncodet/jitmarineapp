﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_DigiDocumentShared")]
    public class DigiDocumentShared : BaseEntity
    {
        [Required]
        public int DigiDocumentSharedId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }
        public string SharedThrough { get; set; }
    }
}

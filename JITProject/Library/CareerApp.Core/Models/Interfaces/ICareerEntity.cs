﻿using System;

namespace CareerApp.Core.Models.Interfaces
{
    public interface ICareerEntity
    {
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
    }
}

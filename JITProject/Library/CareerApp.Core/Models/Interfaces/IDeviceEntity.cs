﻿//using CareerApp.Shared.Enums;

namespace CareerApp.Core.Models.Interfaces
{
    public interface IDeviceEntity
    {
        //DeviceType DeviceType { get; set; }
        string DeviceId { get; set; }
        string DeviceName { get; set; }
    }
}

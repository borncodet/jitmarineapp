﻿namespace CareerApp.Core.Models.Interfaces
{
    public interface IActiveEntity
    {
        bool IsActive { get; set; }
    }
}

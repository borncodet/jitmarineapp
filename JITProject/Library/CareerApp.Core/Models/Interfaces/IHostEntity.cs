﻿using NetTopologySuite.Geometries;

namespace CareerApp.Core.Models.Interfaces
{
	public interface IHostEntity
	{
		string Host { get; set; }
		string Location { get; set; }
	}
}

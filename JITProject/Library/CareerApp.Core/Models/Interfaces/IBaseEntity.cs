﻿namespace CareerApp.Core.Models.Interfaces
{
    public interface IBaseEntity
    {
        int RowId { get; set; }
    }
}

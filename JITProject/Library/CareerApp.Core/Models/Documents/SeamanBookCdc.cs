﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_SeamanBookCdc")]
    public class SeamanBookCdc : BaseEntity
    {
        [Required]
        public int SeamanBookCdcId { get; set; }
        [Required]
        [StringLength(50)]
        public string CdcNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string PlaceIssued { get; set; }
        [Required]
        public DateTime DateIssued { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }
    }
}

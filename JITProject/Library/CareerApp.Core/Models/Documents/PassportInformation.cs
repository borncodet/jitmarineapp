﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_PassportInformation")]
    public class PassportInformation : BaseEntity
    {
        [Required]
        public int PassportInformationId { get; set; }
        [Required]
        [StringLength(50)]
        public string PassportNo { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        [StringLength(50)]
        public string PlaceIssued { get; set; }
        [Required]
        [StringLength(50)]
        public string ECRStatus { get; set; }

        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }

        public int? DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }

        //[ForeignKey("PassportInformationId")]
        //public ICollection<Candidate> Candidates { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_LicenceInformation")]
    public class LicenceInformation : BaseEntity
    {
        [Required]
        public int LicenceInformationId { get; set; }
        [Required]
        [StringLength(50)]
        public string LicenceNo { get; set; }
        [Required]
        public DateTime ValidTill { get; set; }
        [Required]
        public string Licence { get; set; }

        //[ForeignKey("LicenceInformationId")]
        //public ICollection<Candidate> Candidates { get; set; }
    }
}

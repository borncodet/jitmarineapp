﻿using CareerApp.Core.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    public class CareerEntity : ICareerEntity
    {
        public string CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        [NotMapped]
        public virtual ApplicationUser CreatedByUser { get; set; }

        public string UpdatedBy { get; set; }
        [ForeignKey("UpdatedBy")]
        [NotMapped]
        public virtual ApplicationUser UpdatedByUser { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public CareerEntity()
        {
            UpdatedDate = DateTime.UtcNow;
            CreatedDate = DateTime.UtcNow;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_DashboardList")]
    public class DashboardList : BaseEntity
    {
        [Required]
        public int DashboardListId { get; set; }
        [Required]
        public string DashboardName { get; set; }
        [Required]
        public string DashboardImage { get; set; }
    }
}

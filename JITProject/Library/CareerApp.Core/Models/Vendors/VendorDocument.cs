﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_VendorDocuments")]
    public class VendorDocument : BaseEntity
    {
        [Required]
        public int VendorDocumentId { get; set; }
        [Required]
        public string Document1 { get; set; }
        public string Document2 { get; set; }
        public string Document3 { get; set; }

        [Required]
        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }

        [Required]
        public int VerificationStatus { get; set; }

        public int EmployeeId { get; set; }

    }
}

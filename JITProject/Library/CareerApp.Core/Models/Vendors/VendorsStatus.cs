﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_VendorsStatus")]
    public class VendorsStatus : BaseEntity
    {
        [Required]
        public int VendorsStatusId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("VendorStatusId")]
        public ICollection<Vendor> Vendors { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_VendorProfileImage")]
    public class VendorProfileImage : BaseEntity
    {
        [Required]
        public int VendorProfileImageId { get; set; }
        [Required]
        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        public string Base64Image { get; set; }
    }
}

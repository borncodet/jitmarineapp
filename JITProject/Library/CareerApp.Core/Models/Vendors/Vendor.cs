﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Vendors")]
    public class Vendor : BaseEntity
    {
        [Required]
        public int VendorId { get; set; }
        [StringLength(50)]
        public string VendorName { get; set; }
        public string JobRole { get; set; }
        public string Location { get; set; }
        public string Designation { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(50)]
        public string Organisation { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public string VendorDocument { get; set; }
        [Required]
        public int UserId { get; set; }

        [Required]
        public int VendorStatusId { get; set; }
        public VendorsStatus VendorsStatus { get; set; }

        [ForeignKey("VendorId")]
        public ICollection<VendorDocument> VendorDocuments { get; set; }
        [ForeignKey("VendorId")]
        public ICollection<VendorJobList> VendorJobLists { get; set; }
        [ForeignKey("VendorId")]
        public ICollection<VendorProfileImage> VendorProfileImage { get; set; }
    }
}

﻿using CareerApp.Core.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    public abstract class BaseEntity : CareerEntity, IBaseEntity, IActiveEntity
    {
        [Key, Column(Order = 0)]
        [MaxLength(256)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RowId { get; set; }
        public bool IsActive { get; set; }
    }
}

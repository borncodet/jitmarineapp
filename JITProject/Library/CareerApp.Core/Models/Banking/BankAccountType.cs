﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_BankAccountType")]
    public class BankAccountType : BaseEntity
    {
        [Required]
        public int BankAccountTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("BankAccountTypeId")]
        public ICollection<BankDetails> BankDetails { get; set; }
    }
}

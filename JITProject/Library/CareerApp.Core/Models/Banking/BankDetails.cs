﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_BankDetails")]
    public class BankDetails : BaseEntity
    {
        [Required]
        public int BankDetailsId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        [StringLength(50)]
        public string BankName { get; set; }
        [Required]
        [StringLength(50)]
        public string BranchName { get; set; }
        [StringLength(20)]
        public string AccountNo { get; set; }
        [StringLength(20)]
        public string IFSCIBANSWIFTNo { get; set; }
        [Required]
        public int BankAccountTypeId { get; set; }
        public BankAccountType BankAccountType { get; set; }
        [Required]
        public int StateId { get; set; }
        public State State { get; set; }
        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }
    }
}

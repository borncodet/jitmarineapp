﻿using CareerApp.Core.Models.Interfaces;
using CareerApp.Shared.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using CareerApp.Shared.Enums;
//using CareerApp.Core.Domain;

namespace CareerApp.Core.Models.Accounts
{
    public class LoggedDeviceHistory : BaseEntity, IDeviceEntity
    {
        [Required]
        public int IdentityRef { get; set; }
        [ForeignKey("IdentityRef")]
        public virtual ApplicationUser IdentityRefUser { get; set; }

        [Required]
        public DeviceType DeviceType { get; set; }
        [Required]
        public string DeviceId { get; set; }
        [Required]
        public string DeviceName { get; set; }
    }
}

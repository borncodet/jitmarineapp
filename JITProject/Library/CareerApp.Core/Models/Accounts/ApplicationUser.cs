﻿using CareerApp.Core.Models.Interfaces;
//using CareerApp.Shared.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.Core.Models
{
    public class ApplicationUser : IdentityUser<int>, ICareerEntity
    {
        public int UserId { get; set; }
        [Required(ErrorMessage = "Required")]
        public int RoleId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string CountryCode { get; set; }
        public bool EmployerFlag { get; set; }
        public bool VendorFlag { get; set; }
        public bool EmployeeFlag { get; set; }
        public bool CandidateFlag { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        //[Required]
        public DateTime EmailOTPCreatedDate { get; set; }
        //[Required]
        public DateTime PhoneOTPCreatedDate { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime UpdatedDate { get; set; }

        public virtual ICollection<IdentityUserRole<string>> Roles { get; set; }
        public virtual ICollection<IdentityUserClaim<string>> Claims { get; set; }

    }
}

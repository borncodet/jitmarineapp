﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateEducationCertificate")]
    public class CandidateEducationCertificate : BaseEntity
    {
        [Required]
        public int CandidateEducationCertificateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        public int EducationQualificationId { get; set; }
        public EducationQualification EducationQualification { get; set; }
        [Required]
        public string Certificate { get; set; }
    }
}

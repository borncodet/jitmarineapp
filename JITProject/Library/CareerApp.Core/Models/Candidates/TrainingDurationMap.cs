﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_TrainingDurationMap")]
    public class TrainingDurationMap : BaseEntity
    {
        [Required]
        public int TrainingDurationMapId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        public Training Training { get; set; }
        [Required]
        public int DurationId { get; set; }
        public Duration Duration { get; set; }
    }
}

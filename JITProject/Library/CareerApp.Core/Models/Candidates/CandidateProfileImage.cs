﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateProfileImage")]
    public class CandidateProfileImage : BaseEntity
    {
        [Required]
        public int CandidateProfileImageId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        public string Base64Image { get; set; }
    }
}

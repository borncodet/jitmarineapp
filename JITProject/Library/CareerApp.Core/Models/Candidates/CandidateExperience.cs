﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateExperience")]
    public class CandidateExperience : BaseEntity
    {
        [Required]
        public int CandidateExperienceId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        [StringLength(50)]
        public string EmployerName { get; set; }
        public string LocationId { get; set; }
        //[Required]
        //public int JobRoleId { get; set; }
        //public JobRole JobRole { get; set; }
        [Required]
        public string JobRole { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool CurrentlyWorkHereFlag { get; set; }
        public string Responsibilities { get; set; }
        public string Achievements { get; set; }

        [ForeignKey("CandidateExperienceId")]
        public ICollection<CandidateAchievements> CandidateAchievements { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateRelatives")]
    public class CandidateRelatives : BaseEntity
    {
        [Required]
        public int CandidateRelativeId { get; set; }
        public int? NamePrefixId { get; set; }
        public virtual NamePrefix NamePrefix { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        public int GenderId { get; set; }
        public Gender Gender { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        [Required]
        public int RelationshipId { get; set; }
        public Relationship Relationship { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
    }
}

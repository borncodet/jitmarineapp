﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Candidate")]
    public class Candidate : BaseEntity
    {
        [Required]
        public int CandidateId { get; set; }

        public int? NamePrefixId { get; set; }
        public virtual NamePrefix NamePrefix { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }

        public int? GenderId { get; set; }
        public virtual Gender Gender { get; set; }

        public int? MartialStatusId { get; set; }
        public virtual MartialStatus MartialStatus { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }

        public int? LicenceInformationId { get; set; }
        public virtual LicenceInformation LicenceInformation { get; set; }

        public int? PassportInformationId { get; set; }
        public virtual PassportInformation PassportInformation { get; set; }

        public int? SeamanBookCdcId { get; set; }
        public virtual SeamanBookCdc SeamanBookCdc { get; set; }

        [StringLength(20)]
        public string PanNo { get; set; }
        public int? PanDigiDocumentDetailId { get; set; }

        [StringLength(20)]
        public string AadharNo { get; set; }
        public int? AadharDigiDocumentDetailId { get; set; }

        public string ResidenceId { get; set; }
        public int? ResidenceDigiDocumentDetailId { get; set; }

        public int? JobCategoryId { get; set; }
        public virtual JobCategory JobCategory { get; set; }

        public int? DesignationId { get; set; }
        public virtual Designation Designation { get; set; }

        public int? JobTypeId { get; set; }
        public virtual JobType JobType { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal CurrentCTC { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ExpectedPackage { get; set; }

        public int? IndustryId { get; set; }
        public virtual Industry Industry { get; set; }

        public int? FunctionalAreaId { get; set; }
        public virtual FunctionalArea FunctionalArea { get; set; }

        public int? JobRoleId { get; set; }
        public virtual JobRole JobRole { get; set; }

        public string Skills { get; set; }
        public string ProfileSummary { get; set; }
        public string PositionApplyingFor { get; set; }
        public DateTime? Dob { get; set; }
        public string CurrentAddress1 { get; set; }
        public string CurrentAddress2 { get; set; }
        public string PermanantAddress1 { get; set; }
        public string PermanantAddress2 { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        [StringLength(10)]
        public string ZipCode { get; set; }
        public int UserId { get; set; }

        [ForeignKey("CandidateId")]
        public ICollection<JobApplied> JobApplied { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<JobAppliedDetails> JobAppliedDetails { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateLanguageMap> CandidateLanguageMap { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<BankDetails> BankDetails { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateExperience> CandidateExperience { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<EducationQualification> EducationQualification { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateEducationCertificate> CandidateEducationCertificate { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateOtherCertificate> CandidateOtherCertificate { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<Training> Training { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateRelatives> CandidateRelative { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateSkills> CandidateSkills { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateProjects> CandidateProjects { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<SocialAccounts> SocialAccounts { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<JobBookmarked> JobBookmarked { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<JobShared> JobShared { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<DigiDocumentDetails> DigiDocumentDetails { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateReferences> CandidateReferences { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<ResumeCandidateMap> ResumeCandidateMap { get; set; }
        [ForeignKey("CandidateId")]
        public ICollection<CandidateProfileImage> CandidateProfileImage { get; set; }
    }
}

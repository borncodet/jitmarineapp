﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ProjectRole")]
    public class ProjectRole : BaseEntity
    {
        [Required]
        public int ProjectRoleId { get; set; }
        [Required]
        public int CandidateProjectId { get; set; }
        public CandidateProjects CandidateProjects { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        public JobRole JobRole { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobApplied")]
    public class JobApplied : BaseEntity
    {
        [Required]
        public int JobAppliedId { get; set; }

        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }
    }
}

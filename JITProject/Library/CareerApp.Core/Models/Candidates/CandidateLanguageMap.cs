﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateLanguageMap")]
    public class CandidateLanguageMap : BaseEntity
    {
        [Required]
        public int CandidateLanguageMapId { get; set; }

        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

        [Required]
        public int LanguageId { get; set; }
        public Language Language { get; set; }
    }
}

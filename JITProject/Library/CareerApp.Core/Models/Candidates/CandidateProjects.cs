﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateProjects")]
    public class CandidateProjects : BaseEntity
    {
        [Required]
        public int CandidateProjectId { get; set; }
        [Required]
        public string ProjectName { get; set; }
        [Required]
        public int TeamSize { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

        [ForeignKey("CandidateProjectId")]
        public ICollection<ProjectRole> ProjectRole { get; set; }
    }
}

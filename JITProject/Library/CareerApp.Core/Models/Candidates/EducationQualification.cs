﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_EducationQualification")]
    public class EducationQualification : BaseEntity
    {
        [Required]
        public int EducationQualificationId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        //[Required]
        //public int CourseId { get; set; }
        //public Course Course { get; set; }
        [Required]
        public string Course { get; set; }
        [Required]
        public string University { get; set; }
        [StringLength(10)]
        public string Grade { get; set; }
        [Required]
        public DateTime DateFrom { get; set; }
        [Required]
        public DateTime DateTo { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }

        [ForeignKey("EducationQualificationId")]
        public ICollection<CandidateEducationCertificate> CandidateEducationCertificate { get; set; }
    }
}

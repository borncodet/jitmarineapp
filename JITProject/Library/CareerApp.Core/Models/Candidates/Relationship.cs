﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Relationship")]
    public class Relationship : BaseEntity
    {
        [Required]
        public int RelationshipId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("RelationshipId")]
        public ICollection<CandidateRelatives> CandidateRelative { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobBookmarked")]
    public class JobBookmarked : BaseEntity
    {
        [Required]
        public int JobBookmarkedId { get; set; }

        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }
    }
}

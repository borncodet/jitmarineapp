﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateOtherCertificate")]
    public class CandidateOtherCertificate : BaseEntity
    {
        [Required]
        public int CandidateOtherCertificateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        public int TrainingId { get; set; }
        public Training Training { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool ReminderOnExpiryFlag { get; set; }
        [Required]
        public string Certificate { get; set; }
    }
}

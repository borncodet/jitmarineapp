﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_SocialAccounts")]
    public class SocialAccounts : BaseEntity
    {
        [Required]
        public int SocialAccountId { get; set; }
        [Required]
        public string Facebooks { get; set; }
        [Required]
        public string Google { get; set; }
        [Required]
        public string Twitter { get; set; }
        [Required]
        public string LinkedIn { get; set; }
        [Required]
        public string Pinterest { get; set; }
        [Required]
        public string Instagram { get; set; }
        [Required]
        public string Other { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
    }
}

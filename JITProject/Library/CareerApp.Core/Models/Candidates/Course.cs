﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Courses")]
    public class Course : BaseEntity
    {
        [Required]
        public int CourseId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("CourseId")]
        public ICollection<JobCourseMap> JobCourseMaps { get; set; }
        //[ForeignKey("CourseId")]
        //public ICollection<EducationQualification> EducationQualification { get; set; }
        //[ForeignKey("CourseId")]
        //public ICollection<Training> Training { get; set; }
    }
}

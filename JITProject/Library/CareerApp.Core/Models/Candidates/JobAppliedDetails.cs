﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAppliedDetails")]
    public class JobAppliedDetails : BaseEntity
    {
        [Required]
        public int JobAppliedDetailsId { get; set; }

        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }

        public int? ResumeCandidateMapId { get; set; }
        public ResumeCandidateMap ResumeCandidateMap { get; set; }

        public int? CoverLetterCandidateMapId { get; set; }
        //public CoverLetterCandidateMap CoverLetterCandidateMap { get; set; }

        public string ResumeDocument { get; set; }
        public string CoverLetterDocument { get; set; }

        [ForeignKey("JobAppliedDetailsId")]
        public ICollection<JobAppliedDigiDocMap> JobAppliedDigiDocMap { get; set; }

    }
}

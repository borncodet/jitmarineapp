﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Training")]
    public class Training : BaseEntity
    {
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        //[Required]
        //public int CourseId { get; set; }
        //public Course Course { get; set; }
        //[Required]
        //public string IssuedBy { get; set; }
        //[Required]
        //public DateTime ValidUpTo { get; set; }
        [Required]
        public string TrainingCertificate { get; set; }
        [Required]
        public string Institute { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }

        [ForeignKey("TrainingId")]
        public ICollection<TrainingRenewal> TrainingRenewal { get; set; }
        [ForeignKey("TrainingId")]
        public ICollection<TrainingDurationMap> TrainingDurationMap { get; set; }
        [ForeignKey("TrainingId")]
        public ICollection<CandidateOtherCertificate> CandidateOtherCertificate { get; set; }
    }
}

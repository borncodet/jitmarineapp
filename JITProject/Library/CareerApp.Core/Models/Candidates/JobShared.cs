﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobShared")]
    public class JobShared : BaseEntity
    {
        [Required]
        public int JobSharedId { get; set; }

        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }

        public string SharedThrough { get; set; }
    }
}

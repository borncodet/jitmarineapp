﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateSkills")]
    public class CandidateSkills : BaseEntity
    {
        [Required]
        public int CandidateSkillId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        public string SkillName { get; set; }
        [Required]
        public int ProficiencyId { get; set; }
        public Proficiency Proficiency { get; set; }
    }
}

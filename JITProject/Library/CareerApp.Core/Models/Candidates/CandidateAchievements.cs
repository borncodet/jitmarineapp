﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CandidateAchievements")]
    public class CandidateAchievements : BaseEntity
    {
        [Required]
        public int CandidateAchievementId { get; set; }
        [Required]
        public int CandidateExperienceId { get; set; }
        public CandidateExperience CandidateExperience { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

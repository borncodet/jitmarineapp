﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_TrainingRenewal")]
    public class TrainingRenewal : BaseEntity
    {
        [Required]
        public int TrainingRenewalId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        public Training Training { get; set; }
        [Required]
        public DateTime ValidUpTo { get; set; }
    }
}

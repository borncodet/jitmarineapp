﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAppliedDigiDocMap")]
    public class JobAppliedDigiDocMap : BaseEntity
    {
        [Required]
        public int JobAppliedDigiDocMapId { get; set; }

        public int? JobAppliedDetailsId { get; set; }
        public JobAppliedDetails JobAppliedDetails { get; set; }

        public int? DigiDocumentDetailId { get; set; }
        public DigiDocumentDetails DigiDocumentDetails { get; set; }
    }
}

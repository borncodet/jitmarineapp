﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ExpereinceTypes")]
    public class ExpereinceTypes : BaseEntity
    {
        [Required]
        public int ExpereinceTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        public int FromDay { get; set; }
        [Required]
        public int FromMonth { get; set; }
        [Required]
        public int FromYear { get; set; }
        [Required]
        public int ToDay { get; set; }
        [Required]
        public int ToMonth { get; set; }
        [Required]
        public int ToYear { get; set; }
        public string Description { get; set; }
    }
}

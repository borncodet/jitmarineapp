﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ResumeCandidateMap")]
    public class ResumeCandidateMap : BaseEntity
    {
        [Required]
        public int ResumeCandidateMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        public ResumeTemplate ResumeTemplate { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public Candidate Candidate { get; set; }
        [Required]
        public string Resume { get; set; }
        [Required]
        public string ResumeName { get; set; }
        public string ResumeFile { get; set; }
        public string ResumeImage { get; set; }

        [ForeignKey("ResumeCandidateMapId")]
        public ICollection<JobAppliedDetails> JobAppliedDetails { get; set; }

    }
}

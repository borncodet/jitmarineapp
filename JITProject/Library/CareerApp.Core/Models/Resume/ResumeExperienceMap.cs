﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ResumeExperienceMap")]
    public class ResumeExperienceMap : BaseEntity
    {
        [Required]
        public int ResumeExperienceMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        public virtual ResumeTemplate ResumeTemplate { get; set; }
        [Required]
        public int ExpereinceTypeId { get; set; }
        public virtual ExpereinceType ExpereinceType { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ResumeTemplates")]
    public class ResumeTemplate : BaseEntity
    {
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public string ResumeContent { get; set; }
        [Required]
        public string ResumeImage { get; set; }

        [ForeignKey("ResumeTemplateId")]
        public ICollection<ResumeCandidateMap> ResumeCandidateMap { get; set; }
        [ForeignKey("ResumeTemplateId")]
        public ICollection<ResumeExpertiseMap> ResumeExpertiseMap { get; set; }
        [ForeignKey("ResumeTemplateId")]
        public ICollection<ResumeExperienceMap> ResumeExperienceMap { get; set; }
        [ForeignKey("ResumeTemplateId")]
        public ICollection<ResumeDesignationMap> ResumeDesignationMap { get; set; }
    }
}

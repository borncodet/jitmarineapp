﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_FieldOfExpertise")]
    public class FieldOfExpertise : BaseEntity
    {
        [Required]
        public int FieldOfExpertiseId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("FieldOfExpertiseId")]
        public ICollection<ResumeExpertiseMap> ResumeExpertiseMap { get; set; }
    }
}

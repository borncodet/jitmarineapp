﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ResumeExpertiseMap")]
    public class ResumeExpertiseMap : BaseEntity
    {
        [Required]
        public int ResumeExpertiseMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        public virtual ResumeTemplate ResumeTemplate { get; set; }
        [Required]
        public int FieldOfExpertiseId { get; set; }
        public virtual FieldOfExpertise FieldOfExpertise { get; set; }
    }
}

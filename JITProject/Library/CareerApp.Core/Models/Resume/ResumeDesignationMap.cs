﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ResumeDesignationMap")]
    public class ResumeDesignationMap : BaseEntity
    {
        [Required]
        public int ResumeDesignationMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        public virtual ResumeTemplate ResumeTemplate { get; set; }
        [Required]
        public int DesignationId { get; set; }
        public virtual Designation Designation { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_MartialStatus")]
    public class MartialStatus : BaseEntity
    {
        [Required]
        public int MartialStatusId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        //[ForeignKey("MartialStatusId")]
        //public ICollection<Candidate> Candidates { get; set; }
    }
}

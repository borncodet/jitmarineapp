﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_NamePrefix")]
    public class NamePrefix : BaseEntity
    {
        [Required]
        public int NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        //[ForeignKey("NamePrefixId")]
        //public ICollection<Candidate> Candidates { get; set; }
        //[ForeignKey("NamePrefixId")]
        //public ICollection<CandidateRelatives> CandidateRelative { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_States")]
    public class State : BaseEntity
    {
        [Required]
        public int StateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }

        //[ForeignKey("StateId")]
        //public ICollection<Candidate> Candidates { get; set; }
        [ForeignKey("StateId")]
        public ICollection<Employer> Employers { get; set; }
        [ForeignKey("StateId")]
        public ICollection<Vendor> Vendors { get; set; }
        [ForeignKey("StateId")]
        public ICollection<University> University { get; set; }
    }
}

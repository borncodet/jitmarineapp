﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Gender")]
    public class Gender : BaseEntity
    {
        [Required]
        public int GenderId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        //[ForeignKey("GenderId")]
        //public ICollection<Candidate> Candidates { get; set; }
    }
}

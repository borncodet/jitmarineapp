﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Proficiency")]
    public class Proficiency : BaseEntity
    {
        [Required]
        public int ProficiencyId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("ProficiencyId")]
        public ICollection<CandidateSkills> CandidateSkills { get; set; }
    }
}

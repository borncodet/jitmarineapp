﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Durations")]
    public class Duration : BaseEntity
    {
        [Required]
        public int DurationId { get; set; }
        [Required]
        public int Day { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Year { get; set; }

        [ForeignKey("DurationId")]
        public ICollection<TrainingDurationMap> TrainingDurationMap { get; set; }
        [ForeignKey("DurationId")]
        public ICollection<JobAlertExperienceMap> JobAlertExperienceMap { get; set; }
    }
}

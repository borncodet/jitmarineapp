﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Currencies")]
    public class Currency : BaseEntity
    {
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string CurrencySymbol { get; set; }
        [Required]
        public int DecimalPoints { get; set; }
        [Required]
        [StringLength(10)]
        public string MainSuffix { get; set; }
        [Required]
        [StringLength(10)]
        public string SubSuffix { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string CurrencyCode { get; set; }
        [Required]
        public decimal Rate { get; set; }

        [ForeignKey("CurrencyId")]
        public ICollection<JobList> JobLists { get; set; }
        [ForeignKey("CurrencyId")]
        public ICollection<Country> Countries { get; set; }
    }
}

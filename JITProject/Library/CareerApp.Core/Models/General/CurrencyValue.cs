﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_CurrencyValues")]
    public class CurrencyValue : BaseEntity
    {
        [Required]
        public string CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }

        [Required]
        public string ValueInId { get; set; }
        public virtual Currency ValueIn { get; set; }

        [Required]
        [Column(TypeName = "decimal(8, 5)")]
        public decimal Value { get; set; }
    }
}

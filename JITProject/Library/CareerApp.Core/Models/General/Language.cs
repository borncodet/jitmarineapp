﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Languages")]
    public class Language : BaseEntity
    {
        [Required]
        public int LanguageId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LanguageCode { get; set; }

        [ForeignKey("PreferedLangId")]
        public ICollection<JobList> JobLists { get; set; }
        [ForeignKey("LanguageId")]
        public ICollection<JobLanguageMap> JobLanguageMap { get; set; }
        [ForeignKey("LanguageId")]
        public ICollection<CandidateLanguageMap> CandidateLanguageMap { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Designation")]
    public class Designation : BaseEntity
    {
        [Required]
        public int DesignationId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        //[ForeignKey("DesignationId")]
        //public ICollection<Candidate> Candidates { get; set; }

        [ForeignKey("DesignationId")]
        public ICollection<ResumeDesignationMap> ResumeDesignationMap { get; set; }
    }
}

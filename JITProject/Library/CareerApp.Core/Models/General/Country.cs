﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Countries")]
    public class Country : BaseEntity
    {
        [Required]
        public int CountryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string CountryCode { get; set; }
        public Currency Currency { get; set; }

        //[ForeignKey("CountryId")]
        //public ICollection<Candidate> Candidates { get; set; }
        [ForeignKey("CountryId")]
        public ICollection<Employer> Employers { get; set; }
        [ForeignKey("CountryId")]
        public ICollection<Vendor> Vendors { get; set; }
        [ForeignKey("CountryId")]
        public ICollection<State> States { get; set; }
        [ForeignKey("CountryId")]
        public ICollection<BankDetails> BankDetails { get; set; }
        [ForeignKey("CountryId")]
        public ICollection<University> University { get; set; }
        [ForeignKey("CountryId")]
        public ICollection<PassportInformation> PassportInformation { get; set; }
    }
}

﻿using CareerApp.Core.Models.Interfaces;
//using CareerApp.Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.Core.Models
{
    public class DeviceEntity : IDeviceEntity
    {
        [Required]
        public string DeviceId { get; set; }
        [Required]
        public string DeviceName { get; set; }
        //[Required]
        //public DeviceType DeviceType { get; set; }
    }
}

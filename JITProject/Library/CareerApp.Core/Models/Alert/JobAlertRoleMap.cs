﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAlertRoleMap")]
    public class JobAlertRoleMap : BaseEntity
    {
        [Required]
        public int JobAlertRoleMapId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        public JobRole JobRole { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public JobPostAlerts JobPostAlerts { get; set; }
    }
}

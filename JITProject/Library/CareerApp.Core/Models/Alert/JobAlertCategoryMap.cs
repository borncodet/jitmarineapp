﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAlertCategoryMap")]
    public class JobAlertCategoryMap : BaseEntity
    {
        [Required]
        public int JobAlertCategoryMapId { get; set; }
        [Required]
        public int JobCategoryId { get; set; }
        public JobCategory JobCategory { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public JobPostAlerts JobPostAlerts { get; set; }
    }
}

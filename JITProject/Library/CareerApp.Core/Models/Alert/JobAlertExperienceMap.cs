﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAlertExperienceMap")]
    public class JobAlertExperienceMap : BaseEntity
    {
        [Required]
        public int JobAlertExperienceMapId { get; set; }
        [Required]
        public int DurationId { get; set; }
        public Duration Duration { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public JobPostAlerts JobPostAlerts { get; set; }
    }
}

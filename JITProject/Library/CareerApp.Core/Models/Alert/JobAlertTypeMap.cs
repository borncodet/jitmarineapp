﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAlertTypeMap")]
    public class JobAlertTypeMap : BaseEntity
    {
        [Required]
        public int JobAlertTypeMapId { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        public JobType JobType { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public JobPostAlerts JobPostAlerts { get; set; }
    }
}

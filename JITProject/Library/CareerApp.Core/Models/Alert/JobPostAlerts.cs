﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobPostAlerts")]
    public class JobPostAlerts : BaseEntity
    {
        [Required]
        public int JobAlertId { get; set; }
        [Required]
        public string Keywords { get; set; }
        [StringLength(50)]
        public string TotalExperience { get; set; }
        public string LocationId { get; set; }
        [Required]
        [StringLength(50)]
        public string AlertTitle { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal SalaryFrom { get; set; }
        [Required]
        public int UserId { get; set; }

        [ForeignKey("JobAlertId")]
        public ICollection<JobAlertCategoryMap> JobAlertCategoryMap { get; set; }
        [ForeignKey("JobAlertId")]
        public ICollection<JobAlertIndustryMap> JobAlertIndustryMap { get; set; }
        [ForeignKey("JobAlertId")]
        public ICollection<JobAlertRoleMap> JobAlertRoleMap { get; set; }
        [ForeignKey("JobAlertId")]
        public ICollection<JobAlertExperienceMap> JobAlertExperienceMap { get; set; }
        [ForeignKey("JobAlertId")]
        public ICollection<JobAlertTypeMap> JobAlertTypeMap { get; set; }
    }
}

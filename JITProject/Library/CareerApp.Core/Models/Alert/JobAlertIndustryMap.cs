﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobAlertIndustryMap")]
    public class JobAlertIndustryMap : BaseEntity
    {
        [Required]
        public int JobAlertIndustryMapId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        public Industry Industry { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public JobPostAlerts JobPostAlerts { get; set; }
    }
}

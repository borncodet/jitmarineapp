﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobByAdmin")]
    public class JobByAdmin : BaseEntity
    {
        [Required]
        public int JobByAdminId { get; set; }
        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }

        public int EmployeeId { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ExpereinceTypes")]
    public class ExpereinceType : BaseEntity
    {
        [Required]
        public int ExpereinceTypeId { get; set; }
        [Required]
        [StringLength(100)]
        public string Title { get; set; }
        [Required]
        public int FromDay { get; set; }
        [Required]
        public int FromMonth { get; set; }
        [Required]
        public int FromYear { get; set; }
        [Required]
        public int ToDay { get; set; }
        [Required]
        public int ToMonth { get; set; }
        [Required]
        public int ToYear { get; set; }
        public string Description { get; set; }

        [ForeignKey("ExpereinceTypeId")]
        public ICollection<ResumeExperienceMap> ResumeExperienceMap { get; set; }
    }
}

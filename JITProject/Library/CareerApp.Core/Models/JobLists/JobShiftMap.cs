﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobShiftMap")]
    public class JobShiftMap : BaseEntity
    {
        [Required]
        public int JobShiftMapId { get; set; }

        [Required]
        public int ShiftId { get; set; }
        public ShiftAvailable ShiftAvailable { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobRoles")]
    public class JobRole : BaseEntity
    {
        [Required]
        public int JobRoleId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        //[ForeignKey("JobRoleId")]
        //public ICollection<Candidate> Candidates { get; set; }
        //[ForeignKey("JobRoleId")]
        //public ICollection<CandidateExperience> CandidateExperience { get; set; }
        [ForeignKey("JobRoleId")]
        public ICollection<ProjectRole> ProjectRole { get; set; }
        [ForeignKey("JobRoleId")]
        public ICollection<JobAlertRoleMap> JobAlertRoleMap { get; set; }
    }
}

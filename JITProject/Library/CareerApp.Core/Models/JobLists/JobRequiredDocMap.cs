﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobRequiredDocMap")]
    public class JobRequiredDocMap : BaseEntity
    {
        [Required]
        public int JobRequiredDocMapId { get; set; }

        [Required]
        public int RequiredDocId { get; set; }
        public RequiredDocumentType RequiredDocumentType { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobByVendors")]
    public class JobByVendor : BaseEntity
    {
        [Required]
        public int JobByVendorId { get; set; }
        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }

        [Required]
        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }

        public int VerificationStatus { get; set; }

        public int EmployeeId { get; set; }
    }
}

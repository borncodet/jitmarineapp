﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_ShiftAvailable")]
    public class ShiftAvailable : BaseEntity
    {
        [Required]
        public int ShiftAvailableId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("ShiftId")]
        public ICollection<JobShiftMap> JobShiftMaps { get; set; }
    }
}

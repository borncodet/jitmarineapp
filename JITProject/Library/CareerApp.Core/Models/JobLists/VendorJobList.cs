﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_VendorJobLists")]
    public class VendorJobList : BaseEntity
    {
        [Required]
        public int JobId { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public JobCategory JobCategory { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [Required]
        public int MinExperienceId { get; set; }
        [Required]
        public int MaxExperienceId { get; set; }

        [ForeignKey("MinExperienceId")]
        public virtual ExpereinceType MinExpereinceType { get; set; }
        [ForeignKey("MaxExperienceId")]
        public virtual ExpereinceType MaxExpereinceType { get; set; }

        [Required]
        public int NumberOfVacancies { get; set; }

        [Required]
        public int JobTypeId { get; set; }
        public JobType JobType { get; set; }

        [Required]
        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }

    }
}

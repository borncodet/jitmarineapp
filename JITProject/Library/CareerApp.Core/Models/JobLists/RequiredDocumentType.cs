﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_RequiredDocumentTypes")]
    public class RequiredDocumentType : BaseEntity
    {
        [Required]
        public int RequiredDocumentTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("RequiredDocId")]
        public ICollection<JobRequiredDocMap> JobRequiredDocMaps { get; set; }
    }
}

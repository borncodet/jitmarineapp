﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobCategories")]
    public class JobCategory : BaseEntity
    {
        [Required]
        public int JobCategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        //[ForeignKey("JobCategoryId")]
        //public ICollection<Candidate> Candidates { get; set; }

        [ForeignKey("CategoryId")]
        public ICollection<JobList> JobLists { get; set; }

        [ForeignKey("CategoryId")]
        public ICollection<VendorJobList> VendorJobLists { get; set; }

        [ForeignKey("JobCategoryId")]
        public ICollection<JobAlertCategoryMap> JobAlertCategoryMap { get; set; }
    }
}

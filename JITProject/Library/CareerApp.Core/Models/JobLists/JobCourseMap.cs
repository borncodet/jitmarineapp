﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobCourseMap")]
    public class JobCourseMap : BaseEntity
    {
        [Required]
        public int JobCourseMapId { get; set; }

        [Required]
        public int CourseId { get; set; }
        public Course Course { get; set; }

        [Required]
        public int JobId { get; set; }
        public JobList JobList { get; set; }
    }
}

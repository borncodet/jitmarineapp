﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Industries")]
    public class Industry : BaseEntity
    {
        [Required]
        public int IndustryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("IndustryId")]
        public ICollection<JobList> JobLists { get; set; }
        //[ForeignKey("IndustryId")]
        //public ICollection<Candidate> Candidates { get; set; }
        [ForeignKey("IndustryId")]
        public ICollection<JobAlertIndustryMap> JobAlertIndustryMap { get; set; }
    }
}

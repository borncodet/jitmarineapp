﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_JobLists")]
    public class JobList : BaseEntity
    {
        [Required]
        public int JobId { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public JobCategory JobCategory { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [Required]
        public int ExperienceId { get; set; }
        [ForeignKey("ExperienceId")]
        public virtual ExpereinceType ExpereinceType { get; set; }

        [Required]
        public int NumberOfVacancies { get; set; }

        [Required]
        public int JobTypeId { get; set; }
        public JobType JobType { get; set; }

        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        [Required]
        public decimal MinAnnualSalary { get; set; }
        [Required]
        public decimal MaxAnnualSalary { get; set; }

        [Required]
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }

        [Required]
        public int IndustryId { get; set; }
        public Industry Industry { get; set; }

        [Required]
        public int FunctionalAreaId { get; set; }
        public FunctionalArea FunctionalArea { get; set; }

        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }

        [Required]
        public int PreferedLangId { get; set; }
        public Language Language { get; set; }

        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }

        [ForeignKey("JobId")]
        public ICollection<JobByAdmin> JobByAdmins { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobByVendor> JobByVendors { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobCourseMap> JobCourseMaps { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobShiftMap> JobShiftMaps { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobRequiredDocMap> JobRequiredDocMaps { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobLanguageMap> JobLanguageMaps { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobApplied> JobApplied { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobAppliedDetails> JobAppliedDetails { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobBookmarked> JobBookmarked { get; set; }
        [ForeignKey("JobId")]
        public ICollection<JobShared> JobShared { get; set; }
    }
}

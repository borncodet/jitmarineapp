﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_FunctionalAreas")]
    public class FunctionalArea : BaseEntity
    {
        [Required]
        public int FunctionalAreaId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }

        [ForeignKey("FunctionalAreaId")]
        public ICollection<JobList> JobLists { get; set; }
        //[ForeignKey("FunctionalAreaId")]
        //public ICollection<Candidate> CandidateS { get; set; }
    }
}

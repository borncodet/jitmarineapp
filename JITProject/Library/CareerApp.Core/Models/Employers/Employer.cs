﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.Core.Models
{
    [Table(name: "CA_Employer")]
    public class Employer : BaseEntity
    {
        [Required]
        public int EmployerId { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }
        [Required]
        [StringLength(50)]
        public string RegisterNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [StringLength(50)]
        public string City { get; set; }

        public int StateId { get; set; }
        public State State { get; set; }

        public int CountryId { get; set; }
        public Country Country { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}

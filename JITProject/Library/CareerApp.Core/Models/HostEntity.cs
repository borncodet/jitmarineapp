﻿using CareerApp.Core.Models.Interfaces;
using NetTopologySuite.Geometries;

namespace CareerApp.Core.Models
{
	public class HostEntity : IHostEntity
	{
		public string Host { get; set; }
		public string Location { get; set; }
	}
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class Jobapplieddocsmakingnotmandatorycorrected : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CA_JobAppliedDigiDocMap_CA_DigiDocumentDetails_DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap");

            migrationBuilder.DropIndex(
                name: "IX_CA_JobAppliedDigiDocMap_DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap");

            migrationBuilder.DropColumn(
                name: "DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDigiDocMap_DigiDocumentDetailId",
                table: "CA_JobAppliedDigiDocMap",
                column: "DigiDocumentDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_CA_JobAppliedDigiDocMap_CA_DigiDocumentDetails_DigiDocumentDetailId",
                table: "CA_JobAppliedDigiDocMap",
                column: "DigiDocumentDetailId",
                principalTable: "CA_DigiDocumentDetails",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CA_JobAppliedDigiDocMap_CA_DigiDocumentDetails_DigiDocumentDetailId",
                table: "CA_JobAppliedDigiDocMap");

            migrationBuilder.DropIndex(
                name: "IX_CA_JobAppliedDigiDocMap_DigiDocumentDetailId",
                table: "CA_JobAppliedDigiDocMap");

            migrationBuilder.AddColumn<int>(
                name: "DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDigiDocMap_DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap",
                column: "DigiDocumentDetailsRowId");

            migrationBuilder.AddForeignKey(
                name: "FK_CA_JobAppliedDigiDocMap_CA_DigiDocumentDetails_DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap",
                column: "DigiDocumentDetailsRowId",
                principalTable: "CA_DigiDocumentDetails",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

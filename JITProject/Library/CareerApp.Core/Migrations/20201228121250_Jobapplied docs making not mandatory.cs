﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class Jobapplieddocsmakingnotmandatory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "JobAppliedDetailsId",
                table: "CA_JobAppliedDigiDocMap",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DigiDocumentDetailId",
                table: "CA_JobAppliedDigiDocMap",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "JobAppliedDetailsId",
                table: "CA_JobAppliedDigiDocMap",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DigiDocumentDetailId",
                table: "CA_JobAppliedDigiDocMap",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}

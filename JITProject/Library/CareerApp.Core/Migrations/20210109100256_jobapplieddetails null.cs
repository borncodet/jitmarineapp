﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class jobapplieddetailsnull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CoverLetterCandidateMapId",
                table: "CA_JobAppliedDetails",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CoverLetterCandidateMapId",
                table: "CA_JobAppliedDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}

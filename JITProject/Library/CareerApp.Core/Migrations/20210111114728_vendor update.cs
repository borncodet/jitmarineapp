﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class vendorupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "RegisterNumber",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "CA_Vendors");

            migrationBuilder.RenameColumn(
                name: "City",
                table: "CA_Vendors",
                newName: "VendorName");

            migrationBuilder.RenameColumn(
                name: "Address2",
                table: "CA_Vendors",
                newName: "Location");

            migrationBuilder.RenameColumn(
                name: "Address1",
                table: "CA_Vendors",
                newName: "CountryCode");

            migrationBuilder.AlterColumn<int>(
                name: "StateId",
                table: "CA_Vendors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "CA_Vendors",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "AboutMe",
                table: "CA_Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Designation",
                table: "CA_Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Document",
                table: "CA_Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "JobRole",
                table: "CA_Vendors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organisation",
                table: "CA_Vendors",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AboutMe",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "Designation",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "Document",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "JobRole",
                table: "CA_Vendors");

            migrationBuilder.DropColumn(
                name: "Organisation",
                table: "CA_Vendors");

            migrationBuilder.RenameColumn(
                name: "VendorName",
                table: "CA_Vendors",
                newName: "City");

            migrationBuilder.RenameColumn(
                name: "Location",
                table: "CA_Vendors",
                newName: "Address2");

            migrationBuilder.RenameColumn(
                name: "CountryCode",
                table: "CA_Vendors",
                newName: "Address1");

            migrationBuilder.AlterColumn<int>(
                name: "StateId",
                table: "CA_Vendors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "CA_Vendors",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "CA_Vendors",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RegisterNumber",
                table: "CA_Vendors",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "CA_Vendors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "CA_Vendors",
                maxLength: 10,
                nullable: true);
        }
    }
}

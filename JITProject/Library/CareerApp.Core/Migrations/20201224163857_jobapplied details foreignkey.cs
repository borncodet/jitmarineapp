﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class jobapplieddetailsforeignkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "CA_BankAccountType",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    BankAccountTypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_BankAccountType", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Courses",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CourseId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Courses", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Currencies",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    CurrencySymbol = table.Column<string>(nullable: false),
                    DecimalPoints = table.Column<int>(nullable: false),
                    MainSuffix = table.Column<string>(maxLength: 10, nullable: false),
                    SubSuffix = table.Column<string>(maxLength: 10, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    CurrencyCode = table.Column<string>(maxLength: 10, nullable: false),
                    Rate = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Currencies", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_DatePosted",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DatePostedId = table.Column<int>(nullable: false),
                    Day = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_DatePosted", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Designation",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DesignationId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Designation", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_DigiDocumentTypes",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DigiDocumentTypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_DigiDocumentTypes", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Durations",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DurationId = table.Column<int>(nullable: false),
                    Day = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Durations", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_ExpereinceTypes",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ExpereinceTypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    FromDay = table.Column<int>(nullable: false),
                    FromMonth = table.Column<int>(nullable: false),
                    FromYear = table.Column<int>(nullable: false),
                    ToDay = table.Column<int>(nullable: false),
                    ToMonth = table.Column<int>(nullable: false),
                    ToYear = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ExpereinceTypes", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_FieldOfExpertise",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    FieldOfExpertiseId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_FieldOfExpertise", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_FunctionalAreas",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    FunctionalAreaId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_FunctionalAreas", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Gender",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    GenderId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Gender", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Industries",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IndustryId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Industries", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobCategories",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobCategoryId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobCategories", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobPostAlerts",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAlertId = table.Column<int>(nullable: false),
                    Keywords = table.Column<string>(nullable: false),
                    TotalExperience = table.Column<string>(maxLength: 50, nullable: true),
                    LocationId = table.Column<string>(nullable: true),
                    AlertTitle = table.Column<string>(maxLength: 50, nullable: false),
                    SalaryFrom = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobPostAlerts", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobRoles",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobRoleId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobRoles", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobTypes",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobTypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobTypes", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Languages",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Languages", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_LicenceInformation",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    LicenceInformationId = table.Column<int>(nullable: false),
                    LicenceNo = table.Column<string>(maxLength: 50, nullable: false),
                    ValidTill = table.Column<DateTime>(nullable: false),
                    Licence = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_LicenceInformation", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_MartialStatus",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    MartialStatusId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_MartialStatus", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_NamePrefix",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    NamePrefixId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_NamePrefix", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Proficiency",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ProficiencyId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Proficiency", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Relationship",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    RelationshipId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Relationship", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_RequiredDocumentTypes",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    RequiredDocumentTypeId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_RequiredDocumentTypes", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_ResumeTemplates",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ResumeTemplateId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ResumeContent = table.Column<string>(nullable: false),
                    ResumeImage = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ResumeTemplates", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Role",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CA_SeamanBookCdc",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    SeamanBookCdcId = table.Column<int>(nullable: false),
                    CdcNumber = table.Column<string>(maxLength: 50, nullable: false),
                    PlaceIssued = table.Column<string>(maxLength: 50, nullable: false),
                    DateIssued = table.Column<DateTime>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_SeamanBookCdc", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_ShiftAvailable",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ShiftAvailableId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ShiftAvailable", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    CountryCode = table.Column<string>(nullable: false),
                    EmployerFlag = table.Column<bool>(nullable: false),
                    VendorFlag = table.Column<bool>(nullable: false),
                    EmployeeFlag = table.Column<bool>(nullable: false),
                    CandidateFlag = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CA_VendorsStatus",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    VendorsStatusId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_VendorsStatus", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "OpenIddictApplications",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ClientId = table.Column<string>(maxLength: 100, nullable: false),
                    ClientSecret = table.Column<string>(nullable: true),
                    ConcurrencyToken = table.Column<string>(maxLength: 50, nullable: true),
                    ConsentType = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Permissions = table.Column<string>(nullable: true),
                    PostLogoutRedirectUris = table.Column<string>(nullable: true),
                    Properties = table.Column<string>(nullable: true),
                    RedirectUris = table.Column<string>(nullable: true),
                    Type = table.Column<string>(maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpenIddictApplications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OpenIddictScopes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyToken = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Properties = table.Column<string>(nullable: true),
                    Resources = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpenIddictScopes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CA_Countries",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    CountryCode = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Countries", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_Countries_CA_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "CA_Currencies",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CurrencyValues",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CurrencyId = table.Column<string>(nullable: false),
                    CurrencyRowId = table.Column<int>(nullable: true),
                    ValueInId = table.Column<string>(nullable: false),
                    ValueInRowId = table.Column<int>(nullable: true),
                    Value = table.Column<decimal>(type: "decimal(8, 5)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CurrencyValues", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_CurrencyValues_CA_Currencies_CurrencyRowId",
                        column: x => x.CurrencyRowId,
                        principalTable: "CA_Currencies",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_CurrencyValues_CA_Currencies_ValueInRowId",
                        column: x => x.ValueInRowId,
                        principalTable: "CA_Currencies",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAlertCategoryMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAlertCategoryMapId = table.Column<int>(nullable: false),
                    JobCategoryId = table.Column<int>(nullable: false),
                    JobAlertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAlertCategoryMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertCategoryMap_CA_JobPostAlerts_JobAlertId",
                        column: x => x.JobAlertId,
                        principalTable: "CA_JobPostAlerts",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertCategoryMap_CA_JobCategories_JobCategoryId",
                        column: x => x.JobCategoryId,
                        principalTable: "CA_JobCategories",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAlertExperienceMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAlertExperienceMapId = table.Column<int>(nullable: false),
                    DurationId = table.Column<int>(nullable: false),
                    JobAlertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAlertExperienceMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertExperienceMap_CA_Durations_DurationId",
                        column: x => x.DurationId,
                        principalTable: "CA_Durations",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertExperienceMap_CA_JobPostAlerts_JobAlertId",
                        column: x => x.JobAlertId,
                        principalTable: "CA_JobPostAlerts",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAlertIndustryMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAlertIndustryMapId = table.Column<int>(nullable: false),
                    IndustryId = table.Column<int>(nullable: false),
                    JobAlertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAlertIndustryMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertIndustryMap_CA_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "CA_Industries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertIndustryMap_CA_JobPostAlerts_JobAlertId",
                        column: x => x.JobAlertId,
                        principalTable: "CA_JobPostAlerts",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAlertRoleMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAlertRoleMapId = table.Column<int>(nullable: false),
                    JobRoleId = table.Column<int>(nullable: false),
                    JobAlertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAlertRoleMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertRoleMap_CA_JobPostAlerts_JobAlertId",
                        column: x => x.JobAlertId,
                        principalTable: "CA_JobPostAlerts",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertRoleMap_CA_JobRoles_JobRoleId",
                        column: x => x.JobRoleId,
                        principalTable: "CA_JobRoles",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAlertTypeMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAlertTypeMapId = table.Column<int>(nullable: false),
                    JobTypeId = table.Column<int>(nullable: false),
                    JobAlertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAlertTypeMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertTypeMap_CA_JobPostAlerts_JobAlertId",
                        column: x => x.JobAlertId,
                        principalTable: "CA_JobPostAlerts",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAlertTypeMap_CA_JobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "CA_JobTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobLists",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ExperienceId = table.Column<int>(nullable: false),
                    NumberOfVacancies = table.Column<int>(nullable: false),
                    JobTypeId = table.Column<int>(nullable: false),
                    IsPreferred = table.Column<bool>(nullable: false),
                    IsRequired = table.Column<bool>(nullable: false),
                    LocationId = table.Column<string>(nullable: true),
                    RegionId = table.Column<string>(nullable: true),
                    TerritoryId = table.Column<string>(nullable: true),
                    MinAnnualSalary = table.Column<decimal>(nullable: false),
                    MaxAnnualSalary = table.Column<decimal>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    IndustryId = table.Column<int>(nullable: false),
                    FunctionalAreaId = table.Column<int>(nullable: false),
                    ProfileDescription = table.Column<string>(nullable: true),
                    WillingnessToTravelFlag = table.Column<bool>(nullable: false),
                    PreferedLangId = table.Column<int>(nullable: false),
                    AutoScreeningFilterFlag = table.Column<bool>(nullable: false),
                    AutoSkillAssessmentFlag = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobLists", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_JobCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "CA_JobCategories",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "CA_Currencies",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_ExpereinceTypes_ExperienceId",
                        column: x => x.ExperienceId,
                        principalTable: "CA_ExpereinceTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_FunctionalAreas_FunctionalAreaId",
                        column: x => x.FunctionalAreaId,
                        principalTable: "CA_FunctionalAreas",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "CA_Industries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_JobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "CA_JobTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLists_CA_Languages_PreferedLangId",
                        column: x => x.PreferedLangId,
                        principalTable: "CA_Languages",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_ResumeDesignationMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ResumeDesignationMapId = table.Column<int>(nullable: false),
                    ResumeTemplateId = table.Column<int>(nullable: false),
                    DesignationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ResumeDesignationMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_ResumeDesignationMap_CA_Designation_DesignationId",
                        column: x => x.DesignationId,
                        principalTable: "CA_Designation",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_ResumeDesignationMap_CA_ResumeTemplates_ResumeTemplateId",
                        column: x => x.ResumeTemplateId,
                        principalTable: "CA_ResumeTemplates",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_ResumeExperienceMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ResumeExperienceMapId = table.Column<int>(nullable: false),
                    ResumeTemplateId = table.Column<int>(nullable: false),
                    ExpereinceTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ResumeExperienceMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_ResumeExperienceMap_CA_ExpereinceTypes_ExpereinceTypeId",
                        column: x => x.ExpereinceTypeId,
                        principalTable: "CA_ExpereinceTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_ResumeExperienceMap_CA_ResumeTemplates_ResumeTemplateId",
                        column: x => x.ResumeTemplateId,
                        principalTable: "CA_ResumeTemplates",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_ResumeExpertiseMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ResumeExpertiseMapId = table.Column<int>(nullable: false),
                    ResumeTemplateId = table.Column<int>(nullable: false),
                    FieldOfExpertiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ResumeExpertiseMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_ResumeExpertiseMap_CA_FieldOfExpertise_FieldOfExpertiseId",
                        column: x => x.FieldOfExpertiseId,
                        principalTable: "CA_FieldOfExpertise",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_ResumeExpertiseMap_CA_ResumeTemplates_ResumeTemplateId",
                        column: x => x.ResumeTemplateId,
                        principalTable: "CA_ResumeTemplates",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CA_RoleClaims_CA_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "CA_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CA_UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CA_UserClaims_CA_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "CA_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CA_UserLogin",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_UserLogin", x => new { x.ProviderKey, x.LoginProvider });
                    table.UniqueConstraint("AK_CA_UserLogin_LoginProvider_ProviderKey", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_CA_UserLogin_CA_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "CA_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CA_UserRole",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_UserRole", x => new { x.RoleId, x.UserId });
                    table.UniqueConstraint("AK_CA_UserRole_UserId_RoleId", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_CA_UserRole_CA_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "CA_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CA_UserRole_CA_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "CA_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CA_UserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_CA_UserTokens_CA_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "CA_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoggedDeviceHistorys",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IdentityRef = table.Column<int>(nullable: false),
                    DeviceType = table.Column<int>(nullable: false),
                    DeviceId = table.Column<string>(nullable: false),
                    DeviceName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoggedDeviceHistorys", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_LoggedDeviceHistorys_CA_Users_IdentityRef",
                        column: x => x.IdentityRef,
                        principalTable: "CA_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OpenIddictAuthorizations",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ApplicationId = table.Column<string>(nullable: true),
                    ConcurrencyToken = table.Column<string>(maxLength: 50, nullable: true),
                    Properties = table.Column<string>(nullable: true),
                    Scopes = table.Column<string>(nullable: true),
                    Status = table.Column<string>(maxLength: 25, nullable: false),
                    Subject = table.Column<string>(maxLength: 450, nullable: false),
                    Type = table.Column<string>(maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpenIddictAuthorizations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OpenIddictAuthorizations_OpenIddictApplications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "OpenIddictApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_States",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    CountryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_States", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_States_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobByAdmin",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobByAdminId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobByAdmin", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobByAdmin_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobCourseMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobCourseMapId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobCourseMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobCourseMap_CA_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "CA_Courses",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobCourseMap_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobLanguageMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobLanguageMapId = table.Column<int>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobLanguageMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobLanguageMap_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobLanguageMap_CA_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "CA_Languages",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobRequiredDocMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobRequiredDocMapId = table.Column<int>(nullable: false),
                    RequiredDocId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobRequiredDocMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobRequiredDocMap_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobRequiredDocMap_CA_RequiredDocumentTypes_RequiredDocId",
                        column: x => x.RequiredDocId,
                        principalTable: "CA_RequiredDocumentTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobShiftMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobShiftMapId = table.Column<int>(nullable: false),
                    ShiftId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobShiftMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobShiftMap_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobShiftMap_CA_ShiftAvailable_ShiftId",
                        column: x => x.ShiftId,
                        principalTable: "CA_ShiftAvailable",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OpenIddictTokens",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ApplicationId = table.Column<string>(nullable: true),
                    AuthorizationId = table.Column<string>(nullable: true),
                    ConcurrencyToken = table.Column<string>(maxLength: 50, nullable: true),
                    CreationDate = table.Column<DateTimeOffset>(nullable: true),
                    ExpirationDate = table.Column<DateTimeOffset>(nullable: true),
                    Payload = table.Column<string>(nullable: true),
                    Properties = table.Column<string>(nullable: true),
                    ReferenceId = table.Column<string>(maxLength: 100, nullable: true),
                    Status = table.Column<string>(maxLength: 25, nullable: false),
                    Subject = table.Column<string>(maxLength: 450, nullable: false),
                    Type = table.Column<string>(maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpenIddictTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OpenIddictTokens_OpenIddictApplications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "OpenIddictApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OpenIddictTokens_OpenIddictAuthorizations_AuthorizationId",
                        column: x => x.AuthorizationId,
                        principalTable: "OpenIddictAuthorizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_Employer",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    EmployerId = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(maxLength: 50, nullable: false),
                    RegisterNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: false),
                    Address1 = table.Column<string>(nullable: false),
                    Address2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 50, nullable: true),
                    StateId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    ZipCode = table.Column<string>(maxLength: 10, nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Employer", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_Employer_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Employer_CA_States_StateId",
                        column: x => x.StateId,
                        principalTable: "CA_States",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_University",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    UniversityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    TelephoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Address1 = table.Column<string>(nullable: false),
                    Address2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 50, nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    ZipCode = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_University", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_University_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_University_CA_States_StateId",
                        column: x => x.StateId,
                        principalTable: "CA_States",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_Vendors",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    VendorId = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(maxLength: 50, nullable: false),
                    RegisterNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: false),
                    Address1 = table.Column<string>(nullable: false),
                    Address2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 50, nullable: true),
                    StateId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    ZipCode = table.Column<string>(maxLength: 10, nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    VendorStatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Vendors", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_Vendors_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Vendors_CA_States_StateId",
                        column: x => x.StateId,
                        principalTable: "CA_States",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Vendors_CA_VendorsStatus_VendorStatusId",
                        column: x => x.VendorStatusId,
                        principalTable: "CA_VendorsStatus",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobByVendors",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobByVendorId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    VendorId = table.Column<int>(nullable: false),
                    VerificationStatus = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobByVendors", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobByVendors_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobByVendors_CA_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "CA_Vendors",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_VendorDocuments",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    VendorDocumentId = table.Column<int>(nullable: false),
                    Document1 = table.Column<string>(nullable: false),
                    Document2 = table.Column<string>(nullable: true),
                    Document3 = table.Column<string>(nullable: true),
                    VendorId = table.Column<int>(nullable: false),
                    VerificationStatus = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_VendorDocuments", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_VendorDocuments_CA_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "CA_Vendors",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_VendorJobLists",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    MinExperienceId = table.Column<int>(nullable: false),
                    MaxExperienceId = table.Column<int>(nullable: false),
                    NumberOfVacancies = table.Column<int>(nullable: false),
                    JobTypeId = table.Column<int>(nullable: false),
                    VendorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_VendorJobLists", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_VendorJobLists_CA_JobCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "CA_JobCategories",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_VendorJobLists_CA_JobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "CA_JobTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_VendorJobLists_CA_ExpereinceTypes_MaxExperienceId",
                        column: x => x.MaxExperienceId,
                        principalTable: "CA_ExpereinceTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_VendorJobLists_CA_ExpereinceTypes_MinExperienceId",
                        column: x => x.MinExperienceId,
                        principalTable: "CA_ExpereinceTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_VendorJobLists_CA_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "CA_Vendors",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_BankDetails",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    BankDetailsId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    BankName = table.Column<string>(maxLength: 50, nullable: false),
                    BranchName = table.Column<string>(maxLength: 50, nullable: false),
                    AccountNo = table.Column<string>(maxLength: 20, nullable: true),
                    IFSCIBANSWIFTNo = table.Column<string>(maxLength: 20, nullable: true),
                    BankAccountTypeId = table.Column<int>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_BankDetails", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_BankDetails_CA_BankAccountType_BankAccountTypeId",
                        column: x => x.BankAccountTypeId,
                        principalTable: "CA_BankAccountType",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_BankDetails_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_BankDetails_CA_States_StateId",
                        column: x => x.StateId,
                        principalTable: "CA_States",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateEducationCertificate",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateEducationCertificateId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    EducationQualificationId = table.Column<int>(nullable: false),
                    Certificate = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateEducationCertificate", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateExperience",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateExperienceId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    EmployerName = table.Column<string>(maxLength: 50, nullable: false),
                    LocationId = table.Column<string>(nullable: true),
                    JobRole = table.Column<string>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    CurrentlyWorkHereFlag = table.Column<bool>(nullable: false),
                    Responsibilities = table.Column<string>(nullable: true),
                    Achievements = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateExperience", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateAchievements",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateAchievementId = table.Column<int>(nullable: false),
                    CandidateExperienceId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateAchievements", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_CandidateAchievements_CA_CandidateExperience_CandidateExperienceId",
                        column: x => x.CandidateExperienceId,
                        principalTable: "CA_CandidateExperience",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateLanguageMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateLanguageMapId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateLanguageMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_CandidateLanguageMap_CA_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "CA_Languages",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateOtherCertificate",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateOtherCertificateId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    TrainingId = table.Column<int>(nullable: false),
                    DocumentName = table.Column<string>(nullable: true),
                    DocumentNumber = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: false),
                    ReminderOnExpiryFlag = table.Column<bool>(nullable: false),
                    Certificate = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateOtherCertificate", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateProfileImage",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateProfileImageId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Base64Image = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateProfileImage", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateProjects",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateProjectId = table.Column<int>(nullable: false),
                    ProjectName = table.Column<string>(nullable: false),
                    TeamSize = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateProjects", x => x.RowId);
                });

            migrationBuilder.CreateTable(
                name: "CA_ProjectRole",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ProjectRoleId = table.Column<int>(nullable: false),
                    CandidateProjectId = table.Column<int>(nullable: false),
                    JobRoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ProjectRole", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_ProjectRole_CA_CandidateProjects_CandidateProjectId",
                        column: x => x.CandidateProjectId,
                        principalTable: "CA_CandidateProjects",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_ProjectRole_CA_JobRoles_JobRoleId",
                        column: x => x.JobRoleId,
                        principalTable: "CA_JobRoles",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateReferences",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateReferenceId = table.Column<int>(nullable: false),
                    NamePrefixId = table.Column<int>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: false),
                    CandidateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateReferences", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_CandidateReferences_CA_NamePrefix_NamePrefixId",
                        column: x => x.NamePrefixId,
                        principalTable: "CA_NamePrefix",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateRelatives",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateRelativeId = table.Column<int>(nullable: false),
                    NamePrefixId = table.Column<int>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    GenderId = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: false),
                    TelephoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    RelationshipId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateRelatives", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_CandidateRelatives_CA_Gender_GenderId",
                        column: x => x.GenderId,
                        principalTable: "CA_Gender",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_CandidateRelatives_CA_NamePrefix_NamePrefixId",
                        column: x => x.NamePrefixId,
                        principalTable: "CA_NamePrefix",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_CandidateRelatives_CA_Relationship_RelationshipId",
                        column: x => x.RelationshipId,
                        principalTable: "CA_Relationship",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_CandidateSkills",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateSkillId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    SkillName = table.Column<string>(nullable: false),
                    ProficiencyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_CandidateSkills", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_CandidateSkills_CA_Proficiency_ProficiencyId",
                        column: x => x.ProficiencyId,
                        principalTable: "CA_Proficiency",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_DigiDocumentDetails",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    DocumentNumber = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CandidateId = table.Column<int>(nullable: false),
                    DigiDocumentTypeId = table.Column<int>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false),
                    ExpiryFlag = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_DigiDocumentDetails", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_DigiDocumentDetails_CA_DigiDocumentTypes_DigiDocumentTypeId",
                        column: x => x.DigiDocumentTypeId,
                        principalTable: "CA_DigiDocumentTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_DigiDocumentShared",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DigiDocumentSharedId = table.Column<int>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: false),
                    SharedThrough = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_DigiDocumentShared", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_DigiDocumentShared_CA_DigiDocumentDetails_DigiDocumentDetailId",
                        column: x => x.DigiDocumentDetailId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_DigiDocumentUpload",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DigiDocumentUploadId = table.Column<int>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: false),
                    DigiDocument = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_DigiDocumentUpload", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_DigiDocumentUpload_CA_DigiDocumentDetails_DigiDocumentDetailId",
                        column: x => x.DigiDocumentDetailId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_PassportInformation",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    PassportInformationId = table.Column<int>(nullable: false),
                    PassportNo = table.Column<string>(maxLength: 50, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PlaceIssued = table.Column<string>(maxLength: 50, nullable: false),
                    ECRStatus = table.Column<string>(maxLength: 50, nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_PassportInformation", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_PassportInformation_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_PassportInformation_CA_DigiDocumentDetails_DigiDocumentDetailId",
                        column: x => x.DigiDocumentDetailId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_Candidate",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    NamePrefixId = table.Column<int>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    GenderId = table.Column<int>(nullable: true),
                    MartialStatusId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: false),
                    TelephoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    LicenceInformationId = table.Column<int>(nullable: true),
                    PassportInformationId = table.Column<int>(nullable: true),
                    SeamanBookCdcId = table.Column<int>(nullable: true),
                    PanNo = table.Column<string>(maxLength: 20, nullable: true),
                    PanDigiDocumentDetailId = table.Column<int>(nullable: true),
                    AadharNo = table.Column<string>(maxLength: 20, nullable: true),
                    AadharDigiDocumentDetailId = table.Column<int>(nullable: true),
                    ResidenceId = table.Column<string>(nullable: true),
                    ResidenceDigiDocumentDetailId = table.Column<int>(nullable: true),
                    JobCategoryId = table.Column<int>(nullable: true),
                    DesignationId = table.Column<int>(nullable: true),
                    JobTypeId = table.Column<int>(nullable: true),
                    CurrentCTC = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ExpectedPackage = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    IndustryId = table.Column<int>(nullable: true),
                    FunctionalAreaId = table.Column<int>(nullable: true),
                    JobRoleId = table.Column<int>(nullable: true),
                    Skills = table.Column<string>(nullable: true),
                    ProfileSummary = table.Column<string>(nullable: true),
                    PositionApplyingFor = table.Column<string>(nullable: true),
                    Dob = table.Column<DateTime>(nullable: false),
                    CurrentAddress1 = table.Column<string>(nullable: true),
                    CurrentAddress2 = table.Column<string>(nullable: true),
                    PermanantAddress1 = table.Column<string>(nullable: true),
                    PermanantAddress2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 50, nullable: true),
                    StateId = table.Column<int>(nullable: true),
                    CountryId = table.Column<int>(nullable: true),
                    ZipCode = table.Column<string>(maxLength: 10, nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Candidate", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "CA_Countries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_Designation_DesignationId",
                        column: x => x.DesignationId,
                        principalTable: "CA_Designation",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_FunctionalAreas_FunctionalAreaId",
                        column: x => x.FunctionalAreaId,
                        principalTable: "CA_FunctionalAreas",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_Gender_GenderId",
                        column: x => x.GenderId,
                        principalTable: "CA_Gender",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "CA_Industries",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_JobCategories_JobCategoryId",
                        column: x => x.JobCategoryId,
                        principalTable: "CA_JobCategories",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_JobRoles_JobRoleId",
                        column: x => x.JobRoleId,
                        principalTable: "CA_JobRoles",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_JobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "CA_JobTypes",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_LicenceInformation_LicenceInformationId",
                        column: x => x.LicenceInformationId,
                        principalTable: "CA_LicenceInformation",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_MartialStatus_MartialStatusId",
                        column: x => x.MartialStatusId,
                        principalTable: "CA_MartialStatus",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_NamePrefix_NamePrefixId",
                        column: x => x.NamePrefixId,
                        principalTable: "CA_NamePrefix",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_PassportInformation_PassportInformationId",
                        column: x => x.PassportInformationId,
                        principalTable: "CA_PassportInformation",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_SeamanBookCdc_SeamanBookCdcId",
                        column: x => x.SeamanBookCdcId,
                        principalTable: "CA_SeamanBookCdc",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Candidate_CA_States_StateId",
                        column: x => x.StateId,
                        principalTable: "CA_States",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_EducationQualification",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    EducationQualificationId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    Course = table.Column<string>(nullable: false),
                    University = table.Column<string>(nullable: false),
                    Grade = table.Column<string>(maxLength: 10, nullable: true),
                    DateFrom = table.Column<DateTime>(nullable: false),
                    DateTo = table.Column<DateTime>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_EducationQualification", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_EducationQualification_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_EducationQualification_CA_DigiDocumentDetails_DigiDocumentDetailId",
                        column: x => x.DigiDocumentDetailId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobApplied",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAppliedId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobApplied", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobApplied_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobApplied_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobBookmarked",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobBookmarkedId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobBookmarked", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobBookmarked_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobBookmarked_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobShared",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobSharedId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    SharedThrough = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobShared", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobShared_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobShared_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_ResumeCandidateMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ResumeCandidateMapId = table.Column<int>(nullable: false),
                    ResumeTemplateId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    Resume = table.Column<string>(nullable: false),
                    ResumeName = table.Column<string>(nullable: false),
                    ResumeFile = table.Column<string>(nullable: true),
                    ResumeImage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_ResumeCandidateMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_ResumeCandidateMap_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_ResumeCandidateMap_CA_ResumeTemplates_ResumeTemplateId",
                        column: x => x.ResumeTemplateId,
                        principalTable: "CA_ResumeTemplates",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_SocialAccounts",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    SocialAccountId = table.Column<int>(nullable: false),
                    Facebooks = table.Column<string>(nullable: false),
                    Google = table.Column<string>(nullable: false),
                    Twitter = table.Column<string>(nullable: false),
                    LinkedIn = table.Column<string>(nullable: false),
                    Pinterest = table.Column<string>(nullable: false),
                    Instagram = table.Column<string>(nullable: false),
                    Other = table.Column<string>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_SocialAccounts", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_SocialAccounts_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_Training",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TrainingId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    TrainingCertificate = table.Column<string>(nullable: false),
                    Institute = table.Column<string>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_Training", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_Training_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_Training_CA_DigiDocumentDetails_DigiDocumentDetailId",
                        column: x => x.DigiDocumentDetailId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_DigiDocumentDownload",
                schema: "dbo",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: false),
                    DigiDocumentDetailsRowId = table.Column<int>(nullable: true),
                    AccessKey = table.Column<string>(nullable: false),
                    Expires = table.Column<DateTimeOffset>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_DigiDocumentDownload", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_DigiDocumentDownload_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_DigiDocumentDownload_CA_DigiDocumentDetails_DigiDocumentDetailsRowId",
                        column: x => x.DigiDocumentDetailsRowId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAppliedDetails",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAppliedDetailsId = table.Column<int>(nullable: false),
                    CandidateId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    ResumeCandidateMapId = table.Column<int>(nullable: true),
                    CoverLetterCandidateMapId = table.Column<int>(nullable: false),
                    ResumeDocument = table.Column<string>(nullable: true),
                    CoverLetterDocument = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAppliedDetails", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAppliedDetails_CA_Candidate_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "CA_Candidate",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAppliedDetails_CA_JobLists_JobId",
                        column: x => x.JobId,
                        principalTable: "CA_JobLists",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAppliedDetails_CA_ResumeCandidateMap_ResumeCandidateMapId",
                        column: x => x.ResumeCandidateMapId,
                        principalTable: "CA_ResumeCandidateMap",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_TrainingDurationMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TrainingDurationMapId = table.Column<int>(nullable: false),
                    TrainingId = table.Column<int>(nullable: false),
                    DurationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_TrainingDurationMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_TrainingDurationMap_CA_Durations_DurationId",
                        column: x => x.DurationId,
                        principalTable: "CA_Durations",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_TrainingDurationMap_CA_Training_TrainingId",
                        column: x => x.TrainingId,
                        principalTable: "CA_Training",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_TrainingRenewal",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TrainingRenewalId = table.Column<int>(nullable: false),
                    TrainingId = table.Column<int>(nullable: false),
                    ValidUpTo = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_TrainingRenewal", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_TrainingRenewal_CA_Training_TrainingId",
                        column: x => x.TrainingId,
                        principalTable: "CA_Training",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CA_JobAppliedDigiDocMap",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    JobAppliedDigiDocMapId = table.Column<int>(nullable: false),
                    JobAppliedDetailsId = table.Column<int>(nullable: false),
                    DigiDocumentDetailId = table.Column<int>(nullable: false),
                    DigiDocumentDetailsRowId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_JobAppliedDigiDocMap", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_JobAppliedDigiDocMap_CA_DigiDocumentDetails_DigiDocumentDetailsRowId",
                        column: x => x.DigiDocumentDetailsRowId,
                        principalTable: "CA_DigiDocumentDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CA_JobAppliedDigiDocMap_CA_JobAppliedDetails_JobAppliedDetailsId",
                        column: x => x.JobAppliedDetailsId,
                        principalTable: "CA_JobAppliedDetails",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CA_BankDetails_BankAccountTypeId",
                table: "CA_BankDetails",
                column: "BankAccountTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_BankDetails_CandidateId",
                table: "CA_BankDetails",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_BankDetails_CountryId",
                table: "CA_BankDetails",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_BankDetails_StateId",
                table: "CA_BankDetails",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_CountryId",
                table: "CA_Candidate",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_DesignationId",
                table: "CA_Candidate",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_FunctionalAreaId",
                table: "CA_Candidate",
                column: "FunctionalAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_GenderId",
                table: "CA_Candidate",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_IndustryId",
                table: "CA_Candidate",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_JobCategoryId",
                table: "CA_Candidate",
                column: "JobCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_JobRoleId",
                table: "CA_Candidate",
                column: "JobRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_JobTypeId",
                table: "CA_Candidate",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_LicenceInformationId",
                table: "CA_Candidate",
                column: "LicenceInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_MartialStatusId",
                table: "CA_Candidate",
                column: "MartialStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_NamePrefixId",
                table: "CA_Candidate",
                column: "NamePrefixId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_PassportInformationId",
                table: "CA_Candidate",
                column: "PassportInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_SeamanBookCdcId",
                table: "CA_Candidate",
                column: "SeamanBookCdcId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Candidate_StateId",
                table: "CA_Candidate",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateAchievements_CandidateExperienceId",
                table: "CA_CandidateAchievements",
                column: "CandidateExperienceId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateEducationCertificate_CandidateId",
                table: "CA_CandidateEducationCertificate",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateEducationCertificate_EducationQualificationId",
                table: "CA_CandidateEducationCertificate",
                column: "EducationQualificationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateExperience_CandidateId",
                table: "CA_CandidateExperience",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateLanguageMap_CandidateId",
                table: "CA_CandidateLanguageMap",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateLanguageMap_LanguageId",
                table: "CA_CandidateLanguageMap",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateOtherCertificate_CandidateId",
                table: "CA_CandidateOtherCertificate",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateOtherCertificate_TrainingId",
                table: "CA_CandidateOtherCertificate",
                column: "TrainingId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateProfileImage_CandidateId",
                table: "CA_CandidateProfileImage",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateProjects_CandidateId",
                table: "CA_CandidateProjects",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateReferences_CandidateId",
                table: "CA_CandidateReferences",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateReferences_NamePrefixId",
                table: "CA_CandidateReferences",
                column: "NamePrefixId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateRelatives_CandidateId",
                table: "CA_CandidateRelatives",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateRelatives_GenderId",
                table: "CA_CandidateRelatives",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateRelatives_NamePrefixId",
                table: "CA_CandidateRelatives",
                column: "NamePrefixId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateRelatives_RelationshipId",
                table: "CA_CandidateRelatives",
                column: "RelationshipId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateSkills_CandidateId",
                table: "CA_CandidateSkills",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CandidateSkills_ProficiencyId",
                table: "CA_CandidateSkills",
                column: "ProficiencyId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Countries_CurrencyId",
                table: "CA_Countries",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CurrencyValues_CurrencyRowId",
                table: "CA_CurrencyValues",
                column: "CurrencyRowId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_CurrencyValues_ValueInRowId",
                table: "CA_CurrencyValues",
                column: "ValueInRowId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_DigiDocumentDetails_CandidateId",
                table: "CA_DigiDocumentDetails",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_DigiDocumentDetails_DigiDocumentTypeId",
                table: "CA_DigiDocumentDetails",
                column: "DigiDocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_DigiDocumentShared_DigiDocumentDetailId",
                table: "CA_DigiDocumentShared",
                column: "DigiDocumentDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_DigiDocumentUpload_DigiDocumentDetailId",
                table: "CA_DigiDocumentUpload",
                column: "DigiDocumentDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_EducationQualification_CandidateId",
                table: "CA_EducationQualification",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_EducationQualification_DigiDocumentDetailId",
                table: "CA_EducationQualification",
                column: "DigiDocumentDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Employer_CountryId",
                table: "CA_Employer",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Employer_StateId",
                table: "CA_Employer",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertCategoryMap_JobAlertId",
                table: "CA_JobAlertCategoryMap",
                column: "JobAlertId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertCategoryMap_JobCategoryId",
                table: "CA_JobAlertCategoryMap",
                column: "JobCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertExperienceMap_DurationId",
                table: "CA_JobAlertExperienceMap",
                column: "DurationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertExperienceMap_JobAlertId",
                table: "CA_JobAlertExperienceMap",
                column: "JobAlertId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertIndustryMap_IndustryId",
                table: "CA_JobAlertIndustryMap",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertIndustryMap_JobAlertId",
                table: "CA_JobAlertIndustryMap",
                column: "JobAlertId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertRoleMap_JobAlertId",
                table: "CA_JobAlertRoleMap",
                column: "JobAlertId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertRoleMap_JobRoleId",
                table: "CA_JobAlertRoleMap",
                column: "JobRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertTypeMap_JobAlertId",
                table: "CA_JobAlertTypeMap",
                column: "JobAlertId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAlertTypeMap_JobTypeId",
                table: "CA_JobAlertTypeMap",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobApplied_CandidateId",
                table: "CA_JobApplied",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobApplied_JobId",
                table: "CA_JobApplied",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDetails_CandidateId",
                table: "CA_JobAppliedDetails",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDetails_JobId",
                table: "CA_JobAppliedDetails",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDetails_ResumeCandidateMapId",
                table: "CA_JobAppliedDetails",
                column: "ResumeCandidateMapId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDigiDocMap_DigiDocumentDetailsRowId",
                table: "CA_JobAppliedDigiDocMap",
                column: "DigiDocumentDetailsRowId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobAppliedDigiDocMap_JobAppliedDetailsId",
                table: "CA_JobAppliedDigiDocMap",
                column: "JobAppliedDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobBookmarked_CandidateId",
                table: "CA_JobBookmarked",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobBookmarked_JobId",
                table: "CA_JobBookmarked",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobByAdmin_JobId",
                table: "CA_JobByAdmin",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobByVendors_JobId",
                table: "CA_JobByVendors",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobByVendors_VendorId",
                table: "CA_JobByVendors",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobCourseMap_CourseId",
                table: "CA_JobCourseMap",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobCourseMap_JobId",
                table: "CA_JobCourseMap",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLanguageMap_JobId",
                table: "CA_JobLanguageMap",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLanguageMap_LanguageId",
                table: "CA_JobLanguageMap",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_CategoryId",
                table: "CA_JobLists",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_CurrencyId",
                table: "CA_JobLists",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_ExperienceId",
                table: "CA_JobLists",
                column: "ExperienceId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_FunctionalAreaId",
                table: "CA_JobLists",
                column: "FunctionalAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_IndustryId",
                table: "CA_JobLists",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_JobTypeId",
                table: "CA_JobLists",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobLists_PreferedLangId",
                table: "CA_JobLists",
                column: "PreferedLangId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobRequiredDocMap_JobId",
                table: "CA_JobRequiredDocMap",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobRequiredDocMap_RequiredDocId",
                table: "CA_JobRequiredDocMap",
                column: "RequiredDocId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobShared_CandidateId",
                table: "CA_JobShared",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobShared_JobId",
                table: "CA_JobShared",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobShiftMap_JobId",
                table: "CA_JobShiftMap",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_JobShiftMap_ShiftId",
                table: "CA_JobShiftMap",
                column: "ShiftId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_PassportInformation_CountryId",
                table: "CA_PassportInformation",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_PassportInformation_DigiDocumentDetailId",
                table: "CA_PassportInformation",
                column: "DigiDocumentDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ProjectRole_CandidateProjectId",
                table: "CA_ProjectRole",
                column: "CandidateProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ProjectRole_JobRoleId",
                table: "CA_ProjectRole",
                column: "JobRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeCandidateMap_CandidateId",
                table: "CA_ResumeCandidateMap",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeCandidateMap_ResumeTemplateId",
                table: "CA_ResumeCandidateMap",
                column: "ResumeTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeDesignationMap_DesignationId",
                table: "CA_ResumeDesignationMap",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeDesignationMap_ResumeTemplateId",
                table: "CA_ResumeDesignationMap",
                column: "ResumeTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeExperienceMap_ExpereinceTypeId",
                table: "CA_ResumeExperienceMap",
                column: "ExpereinceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeExperienceMap_ResumeTemplateId",
                table: "CA_ResumeExperienceMap",
                column: "ResumeTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeExpertiseMap_FieldOfExpertiseId",
                table: "CA_ResumeExpertiseMap",
                column: "FieldOfExpertiseId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_ResumeExpertiseMap_ResumeTemplateId",
                table: "CA_ResumeExpertiseMap",
                column: "ResumeTemplateId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "CA_Role",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CA_RoleClaims_RoleId",
                table: "CA_RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_SocialAccounts_CandidateId",
                table: "CA_SocialAccounts",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_States_CountryId",
                table: "CA_States",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Training_CandidateId",
                table: "CA_Training",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Training_DigiDocumentDetailId",
                table: "CA_Training",
                column: "DigiDocumentDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_TrainingDurationMap_DurationId",
                table: "CA_TrainingDurationMap",
                column: "DurationId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_TrainingDurationMap_TrainingId",
                table: "CA_TrainingDurationMap",
                column: "TrainingId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_TrainingRenewal_TrainingId",
                table: "CA_TrainingRenewal",
                column: "TrainingId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_University_CountryId",
                table: "CA_University",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_University_StateId",
                table: "CA_University",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_UserClaims_UserId",
                table: "CA_UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_UserLogin_UserId",
                table: "CA_UserLogin",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "CA_Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "CA_Users",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorDocuments_VendorId",
                table: "CA_VendorDocuments",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorJobLists_CategoryId",
                table: "CA_VendorJobLists",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorJobLists_JobTypeId",
                table: "CA_VendorJobLists",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorJobLists_MaxExperienceId",
                table: "CA_VendorJobLists",
                column: "MaxExperienceId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorJobLists_MinExperienceId",
                table: "CA_VendorJobLists",
                column: "MinExperienceId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorJobLists_VendorId",
                table: "CA_VendorJobLists",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Vendors_CountryId",
                table: "CA_Vendors",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Vendors_StateId",
                table: "CA_Vendors",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_Vendors_VendorStatusId",
                table: "CA_Vendors",
                column: "VendorStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_LoggedDeviceHistorys_IdentityRef",
                table: "LoggedDeviceHistorys",
                column: "IdentityRef");

            migrationBuilder.CreateIndex(
                name: "IX_OpenIddictApplications_ClientId",
                table: "OpenIddictApplications",
                column: "ClientId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OpenIddictAuthorizations_ApplicationId_Status_Subject_Type",
                table: "OpenIddictAuthorizations",
                columns: new[] { "ApplicationId", "Status", "Subject", "Type" });

            migrationBuilder.CreateIndex(
                name: "IX_OpenIddictScopes_Name",
                table: "OpenIddictScopes",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OpenIddictTokens_AuthorizationId",
                table: "OpenIddictTokens",
                column: "AuthorizationId");

            migrationBuilder.CreateIndex(
                name: "IX_OpenIddictTokens_ReferenceId",
                table: "OpenIddictTokens",
                column: "ReferenceId",
                unique: true,
                filter: "[ReferenceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_OpenIddictTokens_ApplicationId_Status_Subject_Type",
                table: "OpenIddictTokens",
                columns: new[] { "ApplicationId", "Status", "Subject", "Type" });

            migrationBuilder.CreateIndex(
                name: "IX_CA_DigiDocumentDownload_CandidateId",
                schema: "dbo",
                table: "CA_DigiDocumentDownload",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_DigiDocumentDownload_DigiDocumentDetailsRowId",
                schema: "dbo",
                table: "CA_DigiDocumentDownload",
                column: "DigiDocumentDetailsRowId");

            migrationBuilder.AddForeignKey(
                name: "FK_CA_BankDetails_CA_Candidate_CandidateId",
                table: "CA_BankDetails",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateEducationCertificate_CA_Candidate_CandidateId",
                table: "CA_CandidateEducationCertificate",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateEducationCertificate_CA_EducationQualification_EducationQualificationId",
                table: "CA_CandidateEducationCertificate",
                column: "EducationQualificationId",
                principalTable: "CA_EducationQualification",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateExperience_CA_Candidate_CandidateId",
                table: "CA_CandidateExperience",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateLanguageMap_CA_Candidate_CandidateId",
                table: "CA_CandidateLanguageMap",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateOtherCertificate_CA_Candidate_CandidateId",
                table: "CA_CandidateOtherCertificate",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateOtherCertificate_CA_Training_TrainingId",
                table: "CA_CandidateOtherCertificate",
                column: "TrainingId",
                principalTable: "CA_Training",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateProfileImage_CA_Candidate_CandidateId",
                table: "CA_CandidateProfileImage",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateProjects_CA_Candidate_CandidateId",
                table: "CA_CandidateProjects",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateReferences_CA_Candidate_CandidateId",
                table: "CA_CandidateReferences",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateRelatives_CA_Candidate_CandidateId",
                table: "CA_CandidateRelatives",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_CandidateSkills_CA_Candidate_CandidateId",
                table: "CA_CandidateSkills",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_DigiDocumentDetails_CA_Candidate_CandidateId",
                table: "CA_DigiDocumentDetails",
                column: "CandidateId",
                principalTable: "CA_Candidate",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CA_DigiDocumentDetails_CA_Candidate_CandidateId",
                table: "CA_DigiDocumentDetails");

            migrationBuilder.DropTable(
                name: "CA_BankDetails");

            migrationBuilder.DropTable(
                name: "CA_CandidateAchievements");

            migrationBuilder.DropTable(
                name: "CA_CandidateEducationCertificate");

            migrationBuilder.DropTable(
                name: "CA_CandidateLanguageMap");

            migrationBuilder.DropTable(
                name: "CA_CandidateOtherCertificate");

            migrationBuilder.DropTable(
                name: "CA_CandidateProfileImage");

            migrationBuilder.DropTable(
                name: "CA_CandidateReferences");

            migrationBuilder.DropTable(
                name: "CA_CandidateRelatives");

            migrationBuilder.DropTable(
                name: "CA_CandidateSkills");

            migrationBuilder.DropTable(
                name: "CA_CurrencyValues");

            migrationBuilder.DropTable(
                name: "CA_DatePosted");

            migrationBuilder.DropTable(
                name: "CA_DigiDocumentShared");

            migrationBuilder.DropTable(
                name: "CA_DigiDocumentUpload");

            migrationBuilder.DropTable(
                name: "CA_Employer");

            migrationBuilder.DropTable(
                name: "CA_JobAlertCategoryMap");

            migrationBuilder.DropTable(
                name: "CA_JobAlertExperienceMap");

            migrationBuilder.DropTable(
                name: "CA_JobAlertIndustryMap");

            migrationBuilder.DropTable(
                name: "CA_JobAlertRoleMap");

            migrationBuilder.DropTable(
                name: "CA_JobAlertTypeMap");

            migrationBuilder.DropTable(
                name: "CA_JobApplied");

            migrationBuilder.DropTable(
                name: "CA_JobAppliedDigiDocMap");

            migrationBuilder.DropTable(
                name: "CA_JobBookmarked");

            migrationBuilder.DropTable(
                name: "CA_JobByAdmin");

            migrationBuilder.DropTable(
                name: "CA_JobByVendors");

            migrationBuilder.DropTable(
                name: "CA_JobCourseMap");

            migrationBuilder.DropTable(
                name: "CA_JobLanguageMap");

            migrationBuilder.DropTable(
                name: "CA_JobRequiredDocMap");

            migrationBuilder.DropTable(
                name: "CA_JobShared");

            migrationBuilder.DropTable(
                name: "CA_JobShiftMap");

            migrationBuilder.DropTable(
                name: "CA_ProjectRole");

            migrationBuilder.DropTable(
                name: "CA_ResumeDesignationMap");

            migrationBuilder.DropTable(
                name: "CA_ResumeExperienceMap");

            migrationBuilder.DropTable(
                name: "CA_ResumeExpertiseMap");

            migrationBuilder.DropTable(
                name: "CA_RoleClaims");

            migrationBuilder.DropTable(
                name: "CA_SocialAccounts");

            migrationBuilder.DropTable(
                name: "CA_TrainingDurationMap");

            migrationBuilder.DropTable(
                name: "CA_TrainingRenewal");

            migrationBuilder.DropTable(
                name: "CA_University");

            migrationBuilder.DropTable(
                name: "CA_UserClaims");

            migrationBuilder.DropTable(
                name: "CA_UserLogin");

            migrationBuilder.DropTable(
                name: "CA_UserRole");

            migrationBuilder.DropTable(
                name: "CA_UserTokens");

            migrationBuilder.DropTable(
                name: "CA_VendorDocuments");

            migrationBuilder.DropTable(
                name: "CA_VendorJobLists");

            migrationBuilder.DropTable(
                name: "LoggedDeviceHistorys");

            migrationBuilder.DropTable(
                name: "OpenIddictScopes");

            migrationBuilder.DropTable(
                name: "OpenIddictTokens");

            migrationBuilder.DropTable(
                name: "CA_DigiDocumentDownload",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "CA_BankAccountType");

            migrationBuilder.DropTable(
                name: "CA_CandidateExperience");

            migrationBuilder.DropTable(
                name: "CA_EducationQualification");

            migrationBuilder.DropTable(
                name: "CA_Relationship");

            migrationBuilder.DropTable(
                name: "CA_Proficiency");

            migrationBuilder.DropTable(
                name: "CA_JobPostAlerts");

            migrationBuilder.DropTable(
                name: "CA_JobAppliedDetails");

            migrationBuilder.DropTable(
                name: "CA_Courses");

            migrationBuilder.DropTable(
                name: "CA_RequiredDocumentTypes");

            migrationBuilder.DropTable(
                name: "CA_ShiftAvailable");

            migrationBuilder.DropTable(
                name: "CA_CandidateProjects");

            migrationBuilder.DropTable(
                name: "CA_FieldOfExpertise");

            migrationBuilder.DropTable(
                name: "CA_Durations");

            migrationBuilder.DropTable(
                name: "CA_Training");

            migrationBuilder.DropTable(
                name: "CA_Role");

            migrationBuilder.DropTable(
                name: "CA_Vendors");

            migrationBuilder.DropTable(
                name: "CA_Users");

            migrationBuilder.DropTable(
                name: "OpenIddictAuthorizations");

            migrationBuilder.DropTable(
                name: "CA_JobLists");

            migrationBuilder.DropTable(
                name: "CA_ResumeCandidateMap");

            migrationBuilder.DropTable(
                name: "CA_VendorsStatus");

            migrationBuilder.DropTable(
                name: "OpenIddictApplications");

            migrationBuilder.DropTable(
                name: "CA_ExpereinceTypes");

            migrationBuilder.DropTable(
                name: "CA_Languages");

            migrationBuilder.DropTable(
                name: "CA_ResumeTemplates");

            migrationBuilder.DropTable(
                name: "CA_Candidate");

            migrationBuilder.DropTable(
                name: "CA_Designation");

            migrationBuilder.DropTable(
                name: "CA_FunctionalAreas");

            migrationBuilder.DropTable(
                name: "CA_Gender");

            migrationBuilder.DropTable(
                name: "CA_Industries");

            migrationBuilder.DropTable(
                name: "CA_JobCategories");

            migrationBuilder.DropTable(
                name: "CA_JobRoles");

            migrationBuilder.DropTable(
                name: "CA_JobTypes");

            migrationBuilder.DropTable(
                name: "CA_LicenceInformation");

            migrationBuilder.DropTable(
                name: "CA_MartialStatus");

            migrationBuilder.DropTable(
                name: "CA_NamePrefix");

            migrationBuilder.DropTable(
                name: "CA_PassportInformation");

            migrationBuilder.DropTable(
                name: "CA_SeamanBookCdc");

            migrationBuilder.DropTable(
                name: "CA_States");

            migrationBuilder.DropTable(
                name: "CA_DigiDocumentDetails");

            migrationBuilder.DropTable(
                name: "CA_Countries");

            migrationBuilder.DropTable(
                name: "CA_DigiDocumentTypes");

            migrationBuilder.DropTable(
                name: "CA_Currencies");
        }
    }
}

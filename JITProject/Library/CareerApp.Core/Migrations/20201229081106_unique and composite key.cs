﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class uniqueandcompositekey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "CA_Users",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CountryCode",
                table: "CA_Users",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddUniqueConstraint(
                name: "AK_CA_Users_CountryCode_PhoneNumber",
                table: "CA_Users",
                columns: new[] { "CountryCode", "PhoneNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_CA_Users_Email",
                table: "CA_Users",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_CA_Users_CountryCode_PhoneNumber",
                table: "CA_Users");

            migrationBuilder.DropIndex(
                name: "IX_CA_Users_Email",
                table: "CA_Users");

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "CA_Users",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CountryCode",
                table: "CA_Users",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}

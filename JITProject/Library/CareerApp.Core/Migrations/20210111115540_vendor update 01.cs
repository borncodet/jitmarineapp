﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class vendorupdate01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Document",
                table: "CA_Vendors",
                newName: "VendorDocument");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VendorDocument",
                table: "CA_Vendors",
                newName: "Document");
        }
    }
}

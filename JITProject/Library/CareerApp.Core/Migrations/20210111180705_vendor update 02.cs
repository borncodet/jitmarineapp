﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class vendorupdate02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "CA_Vendors",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "CA_Vendors");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class Seamandocandbankdetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DigiDocumentDetailId",
                table: "CA_SeamanBookCdc",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DigiDocumentDetailId",
                table: "CA_BankDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CA_SeamanBookCdc_DigiDocumentDetailId",
                table: "CA_SeamanBookCdc",
                column: "DigiDocumentDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_CA_BankDetails_DigiDocumentDetailId",
                table: "CA_BankDetails",
                column: "DigiDocumentDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_CA_BankDetails_CA_DigiDocumentDetails_DigiDocumentDetailId",
                table: "CA_BankDetails",
                column: "DigiDocumentDetailId",
                principalTable: "CA_DigiDocumentDetails",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CA_SeamanBookCdc_CA_DigiDocumentDetails_DigiDocumentDetailId",
                table: "CA_SeamanBookCdc",
                column: "DigiDocumentDetailId",
                principalTable: "CA_DigiDocumentDetails",
                principalColumn: "RowId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CA_BankDetails_CA_DigiDocumentDetails_DigiDocumentDetailId",
                table: "CA_BankDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_CA_SeamanBookCdc_CA_DigiDocumentDetails_DigiDocumentDetailId",
                table: "CA_SeamanBookCdc");

            migrationBuilder.DropIndex(
                name: "IX_CA_SeamanBookCdc_DigiDocumentDetailId",
                table: "CA_SeamanBookCdc");

            migrationBuilder.DropIndex(
                name: "IX_CA_BankDetails_DigiDocumentDetailId",
                table: "CA_BankDetails");

            migrationBuilder.DropColumn(
                name: "DigiDocumentDetailId",
                table: "CA_SeamanBookCdc");

            migrationBuilder.DropColumn(
                name: "DigiDocumentDetailId",
                table: "CA_BankDetails");
        }
    }
}

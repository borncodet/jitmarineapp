﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CareerApp.Core.Migrations
{
    public partial class vendorprofileimage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CA_VendorProfileImage",
                columns: table => new
                {
                    RowId = table.Column<int>(maxLength: 256, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    VendorProfileImageId = table.Column<int>(nullable: false),
                    VendorId = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Base64Image = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CA_VendorProfileImage", x => x.RowId);
                    table.ForeignKey(
                        name: "FK_CA_VendorProfileImage_CA_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "CA_Vendors",
                        principalColumn: "RowId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CA_VendorProfileImage_VendorId",
                table: "CA_VendorProfileImage",
                column: "VendorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CA_VendorProfileImage");
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobTypeSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
        public IEnumerable<ValueCaptionPair> States { get; set; }
    }

    public class JobTypePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobTypeInfo
    {
        public int RowId { get; set; }
        public int JobTypeId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobTypeDataResultModel : DataSourceResultModel<JobTypeInfo>
    {

    }

    public class JobTypeDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobTypeEditResponse : BaseResponse
    {
        public JobTypeEditModel Data { get; set; }

        public JobTypeEditResponse()
        {
            Data = new JobTypeEditModel();
        }
    }

    public class JobTypeEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public JobTypeEditModel()
        {
            RowId = 0;
            JobTypeId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

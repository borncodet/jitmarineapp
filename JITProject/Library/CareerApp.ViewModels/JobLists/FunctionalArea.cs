﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class FunctionalAreaSelectBoxDataViewModel
    {

    }

    public class FunctionalAreaPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int FunctionalAreaId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class FunctionalAreaInfo
    {
        public int RowId { get; set; }
        public int FunctionalAreaId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class FunctionalAreaDataResultModel : DataSourceResultModel<FunctionalAreaInfo>
    {

    }

    public class FunctionalAreaDataRequestModel : DataSourceRequestModel
    {

    }

    public class FunctionalAreaEditResponse : BaseResponse
    {
        public FunctionalAreaEditModel Data { get; set; }

        public FunctionalAreaEditResponse()
        {
            Data = new FunctionalAreaEditModel();
        }
    }

    public class FunctionalAreaEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int FunctionalAreaId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public FunctionalAreaEditModel()
        {
            RowId = 0;
            FunctionalAreaId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class VendorJobListSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Categories { get; set; }
        public IEnumerable<ValueCaptionPair> MinExperiences { get; set; }
        public IEnumerable<ValueCaptionPair> MaxExperiences { get; set; }
        public IEnumerable<ValueCaptionPair> JobTypes { get; set; }
    }

    public class VendorJobListPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int MinExperienceId { get; set; }
        [Required]
        public int MaxExperienceId { get; set; }
        [Required]
        public int NumberOfVacancies { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        [Required]
        public int VendorId { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorJobListInfo
    {
        public int RowId { get; set; }
        public int JobId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int MinExperienceId { get; set; }
        public string MinimumExperience { get; set; }
        public int MaxExperienceId { get; set; }
        public string MaximumExperience { get; set; }
        public int NumberOfVacancies { get; set; }
        public int JobTypeId { get; set; }
        public string JobType { get; set; }
        public int VendorId { get; set; }
        public string CompanyName { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorJobListDataResultModel : DataSourceResultModel<VendorJobListInfo>
    {

    }

    public class VendorJobListDataRequestModel : DataSourceRequestModel
    {

    }

    public class VendorJobListEditResponse : BaseResponse
    {
        public VendorJobListEditModel Data { get; set; }

        public VendorJobListEditResponse()
        {
            Data = new VendorJobListEditModel();
        }
    }

    public class VendorJobListEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int MinExperienceId { get; set; }
        [Required]
        public int MaxExperienceId { get; set; }
        [Required]
        public int NumberOfVacancies { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        [Required]
        public int VendorId { get; set; }
        public bool IsActive { get; set; }

        public VendorJobListEditModel()
        {
            RowId = 0;
            JobId = 0;
            CategoryId = 0;
            Title = "";
            Description = "";
            MinExperienceId = 0;
            MaxExperienceId = 0;
            NumberOfVacancies = 0;
            JobTypeId = 0;
            VendorId = 0;
            IsActive = true;
        }
    }
}

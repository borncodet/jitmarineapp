﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ShiftAvailableSelectBoxDataViewModel
    {

    }

    public class ShiftAvailablePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ShiftAvailableId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class ShiftAvailableInfo
    {
        public int RowId { get; set; }
        public int ShiftAvailableId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class ShiftAvailableDataResultModel : DataSourceResultModel<ShiftAvailableInfo>
    {

    }

    public class ShiftAvailableDataRequestModel : DataSourceRequestModel
    {

    }

    public class ShiftAvailableEditResponse : BaseResponse
    {
        public ShiftAvailableEditModel Data { get; set; }

        public ShiftAvailableEditResponse()
        {
            Data = new ShiftAvailableEditModel();
        }
    }

    public class ShiftAvailableEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ShiftAvailableId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public ShiftAvailableEditModel()
        {
            RowId = 0;
            ShiftAvailableId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobByVendorSelectBoxDataViewModel
    {

    }

    public class JobByVendorPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobByVendorId { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int VendorId { get; set; }
        public int VerificationStatus { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobByVendorInfo
    {
        public int RowId { get; set; }
        public int JobByVendorId { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public int VendorId { get; set; }
        public string CompanyName { get; set; }
        public int VerificationStatus { get; set; }
        public string VerificationStatusTitile { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobByVendorDataResultModel : DataSourceResultModel<JobByVendorInfo>
    {

    }

    public class JobByVendorDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobByVendorEditResponse : BaseResponse
    {
        public JobByVendorEditModel Data { get; set; }

        public JobByVendorEditResponse()
        {
            Data = new JobByVendorEditModel();
        }
    }

    public class JobByVendorEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobByVendorId { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int VendorId { get; set; }
        public int VerificationStatus { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }

        public JobByVendorEditModel()
        {
            RowId = 0;
            JobByVendorId = 0;
            JobId = 0;
            VendorId = 0;
            VerificationStatus = 0;
            EmployeeId = 0;
            IsActive = true;
        }
    }
}

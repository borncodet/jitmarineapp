﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobShiftMapSelectBoxDataViewModel
    {

    }

    public class JobShiftMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobShiftMapId { get; set; }
        [Required]
        public int ShiftId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobShiftMapInfo
    {
        public int RowId { get; set; }
        public int JobShiftMapId { get; set; }
        public int ShiftId { get; set; }
        public string ShiftTitle { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobShiftMapDataResultModel : DataSourceResultModel<JobShiftMapInfo>
    {

    }

    public class JobShiftMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobShiftMapEditResponse : BaseResponse
    {
        public JobShiftMapEditModel Data { get; set; }

        public JobShiftMapEditResponse()
        {
            Data = new JobShiftMapEditModel();
        }
    }

    public class JobShiftMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobShiftMapId { get; set; }
        [Required]
        public int ShiftId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }

        public JobShiftMapEditModel()
        {
            RowId = 0;
            JobShiftMapId = 0;
            ShiftId = 0;
            JobId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobRoleSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
        public IEnumerable<ValueCaptionPair> States { get; set; }
    }

    public class JobRolePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobRoleInfo
    {
        public int RowId { get; set; }
        public int JobRoleId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobRoleDataResultModel : DataSourceResultModel<JobRoleInfo>
    {

    }

    public class JobRoleDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobRoleEditResponse : BaseResponse
    {
        public JobRoleEditModel Data { get; set; }

        public JobRoleEditResponse()
        {
            Data = new JobRoleEditModel();
        }
    }

    public class JobRoleEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public JobRoleEditModel()
        {
            RowId = 0;
            JobRoleId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

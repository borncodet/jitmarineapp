﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DatePostedSelectBoxDataViewModel
    {

    }

    public class DatePostedPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DatePostedId { get; set; }
        [Required]
        public int Day { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Year { get; set; }
        public bool IsActive { get; set; }
    }

    public class DatePostedInfo
    {
        public int RowId { get; set; }
        public int DatePostedId { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public bool IsActive { get; set; }
    }

    public class DatePostedDataResultModel : DataSourceResultModel<DatePostedInfo>
    {

    }

    public class DatePostedDataRequestModel : DataSourceRequestModel
    {

    }

    public class DatePostedEditResponse : BaseResponse
    {
        public DatePostedEditModel Data { get; set; }

        public DatePostedEditResponse()
        {
            Data = new DatePostedEditModel();
        }
    }

    public class DatePostedEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DatePostedId { get; set; }
        [Required]
        public int Day { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Year { get; set; }
        public bool IsActive { get; set; }

        public DatePostedEditModel()
        {
            RowId = 0;
            DatePostedId = 0;
            Day = 0;
            Month = 0;
            Year = 0;
            IsActive = true;
        }
    }
}

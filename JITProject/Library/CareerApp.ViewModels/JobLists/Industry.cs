﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class IndustrySelectBoxDataViewModel
    {

    }

    public class IndustryPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class IndustryInfo
    {
        public int RowId { get; set; }
        public int IndustryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class IndustryDataResultModel : DataSourceResultModel<IndustryInfo>
    {

    }

    public class IndustryDataRequestModel : DataSourceRequestModel
    {

    }

    public class IndustryEditResponse : BaseResponse
    {
        public IndustryEditModel Data { get; set; }

        public IndustryEditResponse()
        {
            Data = new IndustryEditModel();
        }
    }

    public class IndustryEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public IndustryEditModel()
        {
            RowId = 0;
            IndustryId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

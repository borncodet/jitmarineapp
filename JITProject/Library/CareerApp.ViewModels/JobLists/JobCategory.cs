﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobCategorySelectBoxDataViewModel
    {

    }

    public class JobCategoryPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobCategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobCategoryInfo
    {
        public int RowId { get; set; }
        public int JobCategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobCategoryDataResultModel : DataSourceResultModel<JobCategoryInfo>
    {

    }

    public class JobCategoryDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobCategoryEditResponse : BaseResponse
    {
        public JobCategoryEditModel Data { get; set; }

        public JobCategoryEditResponse()
        {
            Data = new JobCategoryEditModel();
        }
    }

    public class JobCategoryEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobCategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public JobCategoryEditModel()
        {
            RowId = 0;
            JobCategoryId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

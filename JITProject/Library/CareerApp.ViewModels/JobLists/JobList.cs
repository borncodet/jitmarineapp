﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobListSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Categories { get; set; }
        public IEnumerable<ValueCaptionPair> MinExperiences { get; set; }
        public IEnumerable<ValueCaptionPair> MaxExperiences { get; set; }
        public IEnumerable<ValueCaptionPair> JobTypes { get; set; }
        public IEnumerable<ValueCaptionPair> Currencies { get; set; }
        public IEnumerable<ValueCaptionPair> Industries { get; set; }
        public IEnumerable<ValueCaptionPair> FunctionalAreas { get; set; }
        public IEnumerable<ValueCaptionPair> PreferedLanguages { get; set; }
    }

    public class JobListPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int ExperienceId { get; set; }
        [Required]
        public int NumberOfVacancies { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        [Required]
        public decimal MinAnnualSalary { get; set; }
        [Required]
        public decimal MaxAnnualSalary { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        [Required]
        public int FunctionalAreaId { get; set; }
        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }
        [Required]
        public int PreferedLangId { get; set; }
        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobListInfo
    {
        public int RowId { get; set; }
        public int JobId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ExperienceId { get; set; }
        public string Experience { get; set; }
        public int NumberOfVacancies { get; set; }
        public int JobTypeId { get; set; }
        public string JobType { get; set; }
        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        public decimal MinAnnualSalary { get; set; }
        public decimal MaxAnnualSalary { get; set; }
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public int IndustryId { get; set; }
        public string Industry { get; set; }
        public int FunctionalAreaId { get; set; }
        public string FunctionalArea { get; set; }
        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }
        public int PreferedLangId { get; set; }
        public string PreferedLanguage { get; set; }
        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }
        public DateTime PostedDate { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsApplied { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobListDataResultModel : DataSourceResultModel<JobListInfo>
    {

    }

    public class JobListDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobListEditResponse : BaseResponse
    {
        public JobListEditModel Data { get; set; }

        public JobListEditResponse()
        {
            Data = new JobListEditModel();
        }
    }

    public class JobListEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int ExperienceId { get; set; }
        [Required]
        public int NumberOfVacancies { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        [Required]
        public decimal MinAnnualSalary { get; set; }
        [Required]
        public decimal MaxAnnualSalary { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        [Required]
        public int FunctionalAreaId { get; set; }
        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }
        [Required]
        public int PreferedLangId { get; set; }
        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }
        public bool IsActive { get; set; }

        public JobListEditModel()
        {
            RowId = 0;
            JobId = 0;
            CategoryId = 0;
            Title = "";
            Description = "";
            ExperienceId = 0;
            NumberOfVacancies = 0;
            JobTypeId = 0;
            IsPreferred = false;
            IsRequired = false;
            LocationId = "";
            RegionId = "";
            TerritoryId = "";
            MinAnnualSalary = 0;
            MaxAnnualSalary = 0;
            CurrencyId = 0;
            IndustryId = 0;
            FunctionalAreaId = 0;
            ProfileDescription = "";
            WillingnessToTravelFlag = false;
            PreferedLangId = 0;
            AutoScreeningFilterFlag = false;
            AutoSkillAssessmentFlag = false;
            IsActive = true;
        }
    }
}

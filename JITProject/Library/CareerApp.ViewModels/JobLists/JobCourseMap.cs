﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobCourseMapSelectBoxDataViewModel
    {

    }

    public class JobCourseMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobCourseMapId { get; set; }
        [Required]
        public int CourseId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobCourseMapInfo
    {
        public int RowId { get; set; }
        public int JobCourseMapId { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobCourseMapDataResultModel : DataSourceResultModel<JobCourseMapInfo>
    {

    }

    public class JobCourseMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobCourseMapEditResponse : BaseResponse
    {
        public JobCourseMapEditModel Data { get; set; }

        public JobCourseMapEditResponse()
        {
            Data = new JobCourseMapEditModel();
        }
    }

    public class JobCourseMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobCourseMapId { get; set; }
        [Required]
        public int CourseId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }

        public JobCourseMapEditModel()
        {
            RowId = 0;
            JobCourseMapId = 0;
            CourseId = 0;
            JobId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobRequiredDocMapSelectBoxDataViewModel
    {

    }

    public class JobRequiredDocMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobRequiredDocMapId { get; set; }
        [Required]
        public int RequiredDocId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobRequiredDocMapInfo
    {
        public int RowId { get; set; }
        public int JobRequiredDocMapId { get; set; }
        public int RequiredDocId { get; set; }
        public string RequiredDocTitle { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobRequiredDocMapDataResultModel : DataSourceResultModel<JobRequiredDocMapInfo>
    {

    }

    public class JobRequiredDocMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobRequiredDocMapEditResponse : BaseResponse
    {
        public JobRequiredDocMapEditModel Data { get; set; }

        public JobRequiredDocMapEditResponse()
        {
            Data = new JobRequiredDocMapEditModel();
        }
    }

    public class JobRequiredDocMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobRequiredDocMapId { get; set; }
        [Required]
        public int RequiredDocId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }

        public JobRequiredDocMapEditModel()
        {
            RowId = 0;
            JobRequiredDocMapId = 0;
            RequiredDocId = 0;
            JobId = 0;
            IsActive = true;
        }
    }
}

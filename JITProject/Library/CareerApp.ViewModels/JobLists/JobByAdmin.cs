﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobByAdminSelectBoxDataViewModel
    {

    }

    public class JobByAdminPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobByAdminId { get; set; }
        [Required]
        public int JobId { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobByAdminInfo
    {
        public int RowId { get; set; }
        public int JobByAdminId { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobByAdminDataResultModel : DataSourceResultModel<JobByAdminInfo>
    {

    }

    public class JobByAdminDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobByAdminEditResponse : BaseResponse
    {
        public JobByAdminEditModel Data { get; set; }

        public JobByAdminEditResponse()
        {
            Data = new JobByAdminEditModel();
        }
    }

    public class JobByAdminEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobByAdminId { get; set; }
        [Required]
        public int JobId { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }

        public JobByAdminEditModel()
        {
            RowId = 0;
            JobByAdminId = 0;
            JobId = 0;
            EmployeeId = 0;
            IsActive = true;
        }
    }
}

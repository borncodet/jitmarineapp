﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class RequiredDocumentTypeSelectBoxDataViewModel
    {

    }

    public class RequiredDocumentTypePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int RequiredDocumentTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class RequiredDocumentTypeInfo
    {
        public int RowId { get; set; }
        public int RequiredDocumentTypeId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class RequiredDocumentTypeDataResultModel : DataSourceResultModel<RequiredDocumentTypeInfo>
    {

    }

    public class RequiredDocumentTypeDataRequestModel : DataSourceRequestModel
    {

    }

    public class RequiredDocumentTypeEditResponse : BaseResponse
    {
        public RequiredDocumentTypeEditModel Data { get; set; }

        public RequiredDocumentTypeEditResponse()
        {
            Data = new RequiredDocumentTypeEditModel();
        }
    }

    public class RequiredDocumentTypeEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int RequiredDocumentTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public RequiredDocumentTypeEditModel()
        {
            RowId = 0;
            RequiredDocumentTypeId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

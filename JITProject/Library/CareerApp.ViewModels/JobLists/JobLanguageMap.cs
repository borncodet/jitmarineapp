﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobLanguageMapSelectBoxDataViewModel
    {

    }

    public class JobLanguageMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobLanguageMapId { get; set; }
        [Required]
        public int LanguageId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobLanguageMapInfo
    {
        public int RowId { get; set; }
        public int JobLanguageMapId { get; set; }
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobLanguageMapDataResultModel : DataSourceResultModel<JobLanguageMapInfo>
    {

    }

    public class JobLanguageMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobLanguageMapEditResponse : BaseResponse
    {
        public JobLanguageMapEditModel Data { get; set; }

        public JobLanguageMapEditResponse()
        {
            Data = new JobLanguageMapEditModel();
        }
    }

    public class JobLanguageMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobLanguageMapId { get; set; }
        [Required]
        public int LanguageId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }

        public JobLanguageMapEditModel()
        {
            RowId = 0;
            JobLanguageMapId = 0;
            LanguageId = 0;
            JobId = 0;
            IsActive = true;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels.Shared
{
    public class BaseViewModel
    {
        public int RowId { get; set; }
    }

    public class BaseViewModelWithIdRequired
    {
        [Required]
        public int RowId { get; set; }
    }

    public class Jobnotbookmarkmodel
    {
        [Required]
        public int JobId { get; set; }
        [Required]
        public int CandidateId { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace CareerApp.ViewModels.Shared
{
    public class BaseSearchViewModel
    {
        public string Title { get; set; }
        public string Location { get; set; }
        public string Type { get; set; }
        public string Expereince { get; set; }
        public int LastDays { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool ShowInactive { get; set; }

        public BaseSearchViewModel()
        {
            Title = string.Empty;
            Location = string.Empty;
            Type = string.Empty;
            Expereince = string.Empty;
            LastDays = 0;
            PageIndex = 0;
            PageSize = int.MaxValue;
            ShowInactive = false;
        }
    }

    public class BaseSearchMultipleViewModel
    {
        public List<string> Title { get; set; }
        public List<string> Location { get; set; }
        public List<int> Type { get; set; }
        public List<int> Expereince { get; set; }
        public List<int> LastDays { get; set; }
        public int CandidateId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool ShowInactive { get; set; }
    }

    public class AppliedSearchMultipleViewModel
    {
        public int CandidateId { get; set; }
        public List<string> Title { get; set; }
        public List<string> Location { get; set; }
        public List<int> Type { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool ShowInactive { get; set; }
    }

    public class BaseSearchByCandidateViewModel : BaseSearchViewModel
    {
        public int CandidateId { get; set; }

        public BaseSearchByCandidateViewModel()
        {
            CandidateId = 0;
        }
    }

    public class BaseCandidateSearchViewModel
    {
        public int CandidateId { get; set; }
        public int DigiDocumentTypeId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool ShowInactive { get; set; }

        public BaseCandidateSearchViewModel()
        {
            CandidateId = 0;
            PageIndex = 0;
            PageSize = int.MaxValue;
            ShowInactive = false;
        }
    }

    public class BaseResumeSearchViewModel
    {
        public List<int> FieldOfExpertiseId { get; set; }
        public List<int> ExperienceTypeId { get; set; }
        public List<int> DesignationId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool ShowInactive { get; set; }
    }

    public class BaseResumeSearchByCandidateViewModel : BaseResumeSearchViewModel
    {
        public int CandidateId { get; set; }

        public BaseResumeSearchByCandidateViewModel()
        {
            CandidateId = 0;
        }
    }
}

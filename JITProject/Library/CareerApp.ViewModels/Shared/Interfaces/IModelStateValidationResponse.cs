﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CareerApp.ViewModels.Shared.Interfaces
{
	public interface IModelStateValidationResponse
	{
		void CreateModelStateValidationResponse(string message = null);
		void CreateModelStateValidationResponse(ModelStateDictionary modelState);
	}
}

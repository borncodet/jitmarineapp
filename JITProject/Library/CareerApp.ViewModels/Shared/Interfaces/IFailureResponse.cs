﻿namespace CareerApp.ViewModels.Shared.Interfaces
{
	public interface IFailureResponse
	{
		void CreateFailureResponse(string message = null);
	}
}

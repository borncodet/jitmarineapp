﻿namespace CareerApp.ViewModels.Shared.Interfaces
{
	public interface ISuccessResponse
	{
		void CreateSuccessResponse(string message = null);
	}
}

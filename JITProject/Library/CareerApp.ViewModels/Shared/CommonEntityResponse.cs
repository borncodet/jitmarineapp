﻿namespace CareerApp.ViewModels.Shared
{
    public class CommonEntityResponse : BaseEntityResponse
    {
        public CommonEntityResponse(int entityRef) : base(entityRef)
        {

        }

        public CommonEntityResponse()
        {

        }
    }
}

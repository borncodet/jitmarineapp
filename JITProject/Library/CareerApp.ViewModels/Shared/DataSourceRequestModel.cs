﻿namespace CareerApp.ViewModels.Shared
{
    public class DataSourceRequestModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; } = string.Empty;
        public string SortOrder { get; set; } = string.Empty;

        public DataSourceRequestModel()
        {
            SearchTerm = string.Empty;
            Page = 1;
            PageSize = 10;
            SortOrder = string.Empty;
        }
    }

    public class TitleRequestModel
    {
        public int CategoryId { get; set; }

        public TitleRequestModel()
        {
            CategoryId = 0;
        }
    }

    public class DataSourceCandidateRequestModel
    {
        public int CandidateId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; } = string.Empty;
        public string SortOrder { get; set; } = string.Empty;
        public bool ShowInactive { get; set; }

        public DataSourceCandidateRequestModel()
        {
            CandidateId = 0;
            SearchTerm = string.Empty;
            Page = 1;
            PageSize = 10;
            SortOrder = string.Empty;
            ShowInactive = false;
        }
    }

    public class DataSourceVendorRequestModel
    {
        public int VendorId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; } = string.Empty;
        public string SortOrder { get; set; } = string.Empty;
        public bool ShowInactive { get; set; }

        public DataSourceVendorRequestModel()
        {
            VendorId = 0;
            SearchTerm = string.Empty;
            Page = 1;
            PageSize = 10;
            SortOrder = string.Empty;
            ShowInactive = false;
        }
    }

    public class DataSourceCandidateJobAppliedRequestModel
    {
        public int CandidateId { get; set; }
        public int JobId { get; set; }
        public bool ShowInactive { get; set; }

        public DataSourceCandidateJobAppliedRequestModel()
        {
            CandidateId = 0;
            JobId = 0;
            ShowInactive = false;
        }
    }
}

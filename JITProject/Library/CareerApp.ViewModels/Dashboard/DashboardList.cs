﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DashboardListSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> DashboardList { get; set; }
    }

    public class DashboardListPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DashboardListId { get; set; }
        [Required]
        public string DashboardName { get; set; }
        [Required]
        public string DashboardImage { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }
    }

    public class DashboardListInfo
    {
        public int RowId { get; set; }
        public int DashboardListId { get; set; }
        public string DashboardName { get; set; }
        public string DashboardImage { get; set; }
        public string DashboardImageFullPath
        {
            get
            {

                if (DashboardImage != null)
                {
                    return @"/Upload/DashboardList/" + DashboardImage;
                }
                else
                {
                    return "";
                }

            }
        }
        public bool IsActive { get; set; }
    }

    public class DashboardListDataResultModel : DataSourceResultModel<DashboardListInfo>
    {

    }

    public class DashboardListDataRequestModel : DataSourceRequestModel
    {

    }

    public class DashboardListEditResponse : BaseResponse
    {
        public DashboardListEditModel Data { get; set; }

        public DashboardListEditResponse()
        {
            Data = new DashboardListEditModel();
        }
    }

    public class DashboardListEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DashboardListId { get; set; }
        [Required]
        public string DashboardName { get; set; }
        [Required]
        public string DashboardImage { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }

        public DashboardListEditModel()
        {
            RowId = 0;
            DashboardListId = 0;
            DashboardName = "";
            DashboardImage = "";
            IsActive = true;
        }
    }
}

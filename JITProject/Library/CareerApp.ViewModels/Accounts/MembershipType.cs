﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CareerApp.ViewModels.Shared;

namespace CareerApp.ViewModels.Accounts
{
	public class MembershipTypePostmodel 
	{
		public string Id { get; set; }
		[Required]
		public string Title { get; set; }
		public string Description { get; set; }
		[Required]
		public bool Active { get; set; }
	}

	public class MembershipTypeEditModel
	{
		public string Id { get; set; }
		[Required]
		public string Title { get; set; }
		public string Description { get; set; }
		[Required]
		public bool Active { get; set; }
	}

	public class MembershipTypeInfo
	{
		public string Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public bool Active { get; set; }
	}

	public class MembershipTypeDataResultModel : DataSourceResultModel<MembershipTypeInfo>
	{

	}

	public class MembershipTypeDataRequestModel : DataSourceRequestModel
	{

	}
}

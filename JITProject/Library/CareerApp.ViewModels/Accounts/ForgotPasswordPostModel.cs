﻿
using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels.Accounts
{
    public class ForgotPasswordPostModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class ForgotPasswordViewModel : BaseResponse
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CareerApp.ViewModels.Accounts
{
 public	class PasswordEditPostModel
	{

		[Required]
		public string CurrentPassword { get; set; }
		[Required]
		public string NewPassword { get; set; }
	}
}

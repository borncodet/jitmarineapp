﻿using System.ComponentModel.DataAnnotations;
//using CareerApp.Shared.Enums;

namespace CareerApp.ViewModels.Accounts
{
    public class UserRegisterPostModel
    {
        //[Required(ErrorMessage = "Required")]
        public int RowId { get; set; }
        public int UserId { get; set; }
        //[Required(ErrorMessage = "Required")]
        public int RoleId { get; set; }
        //[Required(ErrorMessage = "Required")]
        //public string UserName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Required")]
        public string CountryCode { get; set; }
        [Required(ErrorMessage = "Required")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string Password { get; set; }
        public bool EmployerFlag { get; set; }
        public bool VendorFlag { get; set; }
        public bool EmployeeFlag { get; set; }
        public bool CandidateFlag { get; set; }
        public bool IsActive { get; set; }
    }
}

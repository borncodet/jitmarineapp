﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareerApp.ViewModels
{
	
	public class UserInfo
	{
		public string Id { get; set; }
		public string UserName { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string ImageURL { get; set; }
		public bool IsEnabled { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
	}

	public class UserDataResultModel : DataSourceResultModel<UserInfo>
	{

	}

	public class UserDataRequestModel : DataSourceRequestModel
	{

	}
}

﻿using CareerApp.ViewModels.Shared;

namespace CareerApp.ViewModels.Accounts
{
    public class UserInfoViewModel : BaseResponse
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
    }
}

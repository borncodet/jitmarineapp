﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAlertTypeMapSelectBoxDataViewModel
    {

    }

    public class JobAlertTypeMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertTypeMapId { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertTypeMapInfo
    {
        public int RowId { get; set; }
        public int JobAlertTypeMapId { get; set; }
        public int JobTypeId { get; set; }
        public string JobTypeTitle { get; set; }
        public int JobAlertId { get; set; }
        public string JobAlertTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertTypeMapDataResultModel : DataSourceResultModel<JobAlertTypeMapInfo>
    {

    }

    public class JobAlertTypeMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAlertTypeMapEditResponse : BaseResponse
    {
        public JobAlertTypeMapEditModel Data { get; set; }

        public JobAlertTypeMapEditResponse()
        {
            Data = new JobAlertTypeMapEditModel();
        }
    }

    public class JobAlertTypeMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertTypeMapId { get; set; }
        [Required]
        public int JobTypeId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }

        public JobAlertTypeMapEditModel()
        {
            RowId = 0;
            JobAlertTypeMapId = 0;
            JobTypeId = 0;
            JobAlertId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.ViewModels
{
    public class JobPostAlertSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> JobTitles { get; set; }
    }

    public class JobPostAlertPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        [Required]
        public string Keywords { get; set; }
        [StringLength(50)]
        public string TotalExperience { get; set; }
        public string LocationId { get; set; }
        [Required]
        [StringLength(50)]
        public string AlertTitle { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal SalaryFrom { get; set; }
        [Required]
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobPostAlertInfo
    {
        public int RowId { get; set; }
        public int JobAlertId { get; set; }
        public string Keywords { get; set; }
        public string TotalExperience { get; set; }
        public string LocationId { get; set; }
        public string AlertTitle { get; set; }
        public decimal SalaryFrom { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobPostAlertDataResultModel : DataSourceResultModel<JobPostAlertInfo>
    {

    }

    public class JobPostAlertDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobPostAlertEditResponse : BaseResponse
    {
        public JobPostAlertEditModel Data { get; set; }

        public JobPostAlertEditResponse()
        {
            Data = new JobPostAlertEditModel();
        }
    }

    public class JobPostAlertEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        [Required]
        public string Keywords { get; set; }
        [StringLength(50)]
        public string TotalExperience { get; set; }
        public string LocationId { get; set; }
        [Required]
        [StringLength(50)]
        public string AlertTitle { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal SalaryFrom { get; set; }
        [Required]
        public int UserId { get; set; }
        public bool IsActive { get; set; }

        public JobPostAlertEditModel()
        {
            RowId = 0;
            JobAlertId = 0;
            Keywords = "";
            TotalExperience = "";
            LocationId = "";
            AlertTitle = "";
            SalaryFrom = 0;
            UserId = 0;
            IsActive = true;
        }
    }
}

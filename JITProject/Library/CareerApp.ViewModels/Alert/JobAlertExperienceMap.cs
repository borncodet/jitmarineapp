﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAlertExperienceMapSelectBoxDataViewModel
    {

    }

    public class JobAlertExperienceMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertExperienceMapId { get; set; }
        [Required]
        public int DurationId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertExperienceMapInfo
    {
        public int RowId { get; set; }
        public int JobAlertExperienceMapId { get; set; }
        public int DurationId { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int JobAlertId { get; set; }
        public string JobAlertTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertExperienceMapDataResultModel : DataSourceResultModel<JobAlertExperienceMapInfo>
    {

    }

    public class JobAlertExperienceMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAlertExperienceMapEditResponse : BaseResponse
    {
        public JobAlertExperienceMapEditModel Data { get; set; }

        public JobAlertExperienceMapEditResponse()
        {
            Data = new JobAlertExperienceMapEditModel();
        }
    }

    public class JobAlertExperienceMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertExperienceMapId { get; set; }
        [Required]
        public int DurationId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }

        public JobAlertExperienceMapEditModel()
        {
            RowId = 0;
            JobAlertExperienceMapId = 0;
            DurationId = 0;
            JobAlertId = 0;
            IsActive = true;
        }
    }
}

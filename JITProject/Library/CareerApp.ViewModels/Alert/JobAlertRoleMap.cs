﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAlertRoleMapSelectBoxDataViewModel
    {

    }

    public class JobAlertRoleMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertRoleMapId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertRoleMapInfo
    {
        public int RowId { get; set; }
        public int JobAlertRoleMapId { get; set; }
        public int JobRoleId { get; set; }
        public string JobRoleTitle { get; set; }
        public int JobAlertId { get; set; }
        public string JobAlertTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertRoleMapDataResultModel : DataSourceResultModel<JobAlertRoleMapInfo>
    {

    }

    public class JobAlertRoleMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAlertRoleMapEditResponse : BaseResponse
    {
        public JobAlertRoleMapEditModel Data { get; set; }

        public JobAlertRoleMapEditResponse()
        {
            Data = new JobAlertRoleMapEditModel();
        }
    }

    public class JobAlertRoleMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertRoleMapId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }

        public JobAlertRoleMapEditModel()
        {
            RowId = 0;
            JobAlertRoleMapId = 0;
            JobRoleId = 0;
            JobAlertId = 0;
            IsActive = true;
        }
    }
}

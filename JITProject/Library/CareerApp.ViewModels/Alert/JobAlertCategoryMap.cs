﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAlertCategoryMapSelectBoxDataViewModel
    {

    }

    public class JobAlertCategoryMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertCategoryMapId { get; set; }
        [Required]
        public int JobCategoryId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertCategoryMapInfo
    {
        public int RowId { get; set; }
        public int JobAlertCategoryMapId { get; set; }
        public int JobCategoryId { get; set; }
        public string JobCategoryTitle { get; set; }
        public int JobAlertId { get; set; }
        public string JobAlertTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertCategoryMapDataResultModel : DataSourceResultModel<JobAlertCategoryMapInfo>
    {

    }

    public class JobAlertCategoryMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAlertCategoryMapEditResponse : BaseResponse
    {
        public JobAlertCategoryMapEditModel Data { get; set; }

        public JobAlertCategoryMapEditResponse()
        {
            Data = new JobAlertCategoryMapEditModel();
        }
    }

    public class JobAlertCategoryMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertCategoryMapId { get; set; }
        [Required]
        public int JobCategoryId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }

        public JobAlertCategoryMapEditModel()
        {
            RowId = 0;
            JobAlertCategoryMapId = 0;
            JobCategoryId = 0;
            JobAlertId = 0;
            IsActive = true;
        }
    }
}

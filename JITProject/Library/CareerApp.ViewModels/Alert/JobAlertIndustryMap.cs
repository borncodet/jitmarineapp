﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAlertIndustryMapSelectBoxDataViewModel
    {

    }

    public class JobAlertIndustryMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertIndustryMapId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertIndustryMapInfo
    {
        public int RowId { get; set; }
        public int JobAlertIndustryMapId { get; set; }
        public int IndustryId { get; set; }
        public string IndustryTitle { get; set; }
        public int JobAlertId { get; set; }
        public string JobAlertTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAlertIndustryMapDataResultModel : DataSourceResultModel<JobAlertIndustryMapInfo>
    {

    }

    public class JobAlertIndustryMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAlertIndustryMapEditResponse : BaseResponse
    {
        public JobAlertIndustryMapEditModel Data { get; set; }

        public JobAlertIndustryMapEditResponse()
        {
            Data = new JobAlertIndustryMapEditModel();
        }
    }

    public class JobAlertIndustryMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAlertIndustryMapId { get; set; }
        [Required]
        public int IndustryId { get; set; }
        [Required]
        public int JobAlertId { get; set; }
        public bool IsActive { get; set; }

        public JobAlertIndustryMapEditModel()
        {
            RowId = 0;
            JobAlertIndustryMapId = 0;
            IndustryId = 0;
            JobAlertId = 0;
            IsActive = true;
        }
    }
}

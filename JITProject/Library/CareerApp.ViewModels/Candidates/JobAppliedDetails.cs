﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAppliedDetailsSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> DigiDocumentDetails { get; set; }
    }

    public class JobAppliedDetailsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAppliedDetailsId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public int? ResumeCandidateMapId { get; set; }
        public int? CoverLetterCandidateMapId { get; set; }
        public string ResumeDocument { get; set; }
        public IFormFile ResumeDocumentFile { get; set; }
        public string CoverLetterDocument { get; set; }
        public IFormFile CoverLetterDocumentFile { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAppliedDataPostmodel
    {
        public JobAppliedDetailsPostmodel JobAppliedDetailsPostmodel { get; set; }
        public List<JobAppliedDigiDocMapPostmodel> JobAppliedDigiDocMapPostmodel { get; set; }
    }

    public class JobAppliedDetailsInfo
    {
        public int RowId { get; set; }
        public int JobAppliedDetailsId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public int? ResumeCandidateMapId { get; set; }
        public int? CoverLetterCandidateMapId { get; set; }
        public string ResumeDocument { get; set; }
        public string CoverLetterDocument { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsApplied { get; set; }
        public bool IsActive { get; set; }
    }

    public class RecentlyAppliedJobDetailsInfo
    {
        public int RowId { get; set; }
        public int JobId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ExperienceId { get; set; }
        public string Experience { get; set; }
        public int NumberOfVacancies { get; set; }
        public int JobTypeId { get; set; }
        public string JobType { get; set; }
        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        public decimal MinAnnualSalary { get; set; }
        public decimal MaxAnnualSalary { get; set; }
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public int IndustryId { get; set; }
        public string Industry { get; set; }
        public int FunctionalAreaId { get; set; }
        public string FunctionalArea { get; set; }
        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }
        public int PreferedLangId { get; set; }
        public string PreferedLanguage { get; set; }
        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }
        public DateTime PostedDate { get; set; }
        public int JobAppliedDetailsId { get; set; }
        public int? ResumeCandidateMapId { get; set; }
        public int? CoverLetterCandidateMapId { get; set; }
        public string ResumeDocument { get; set; }
        public string CoverLetterDocument { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public string DaysAgo { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsApplied { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAppliedDetailsCandidateDataResultModel : DataSourceResultModel<RecentlyAppliedJobDetailsInfo>
    {

    }

    public class JobAppliedDetailsCandidateDataRequestModel : DataSourceCandidateRequestModel
    {

    }

    public class JobAppliedAndDocInfo
    {
        public JobAppliedDetailsInfo JobAppliedDetailsInfo { get; set; }
        public List<JobAppliedDigiDocMapInfo> JobAppliedDigiDocMapInfo { get; set; }
    }

    public class JobAppiledInfo
    {
        public List<JobAppliedDetailsInfo> DigiDocs { get; set; }
        public List<DigiDocumentCountInfo> ExpiryDigiDocs { get; set; }
    }

    public class JobAppliedDetailsDataResultModel : DataSourceResultModel<JobAppliedDetailsInfo>
    {

    }

    public class JobAppliedDetailsDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAppliedDetailsEditResponse : BaseResponse
    {
        public JobAppliedDetailsEditModel Data { get; set; }

        public JobAppliedDetailsEditResponse()
        {
            Data = new JobAppliedDetailsEditModel();
        }
    }

    public class JobAppliedDetailsEditModel
    {
        public int RowId { get; set; }
        public int JobAppliedDetailsId { get; set; }
        public int CandidateId { get; set; }
        public int JobId { get; set; }
        public int? ResumeCandidateMapId { get; set; }
        public int? CoverLetterCandidateMapId { get; set; }
        public string ResumeDocument { get; set; }
        public IFormFile ResumeDocumentFile { get; set; }
        public string CoverLetterDocument { get; set; }
        public IFormFile CoverLetterDocumentFile { get; set; }
        public bool IsActive { get; set; }

        public JobAppliedDetailsEditModel()
        {
            RowId = 0;
            JobAppliedDetailsId = 0;
            CandidateId = 0;
            JobId = 0;
            ResumeCandidateMapId = 0;
            CoverLetterCandidateMapId = 0;
            ResumeDocument = "";
            CoverLetterDocument = "";
            IsActive = true;
        }
    }
}

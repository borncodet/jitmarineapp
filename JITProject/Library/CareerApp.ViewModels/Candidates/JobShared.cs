﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobSharedSelectBoxDataViewModel
    {
    }

    public class JobSharedPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobSharedId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public string SharedThrough { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobSharedInfo
    {
        public int RowId { get; set; }
        public int JobSharedId { get; set; }
        public int CandidateId { get; set; }
        public string CandidatetName { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public string SharedThrough { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobSharedDataResultModel : DataSourceResultModel<JobSharedInfo>
    {

    }

    public class JobSharedDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobSharedEditResponse : BaseResponse
    {
        public JobSharedEditModel Data { get; set; }

        public JobSharedEditResponse()
        {
            Data = new JobSharedEditModel();
        }
    }

    public class JobSharedEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobSharedId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public string SharedThrough { get; set; }
        public bool IsActive { get; set; }

        public JobSharedEditModel()
        {
            RowId = 0;
            JobSharedId = 0;
            CandidateId = 0;
            JobId = 0;
            SharedThrough = "";
            IsActive = true;
        }
    }
}

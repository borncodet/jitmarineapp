﻿using CareerApp.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class TrainingRenewalSelectBoxDataViewModel
    {
    }

    public class TrainingRenewalPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int TrainingRenewalId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public DateTime ValidUpTo { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingRenewalInfo
    {
        public int RowId { get; set; }
        public int TrainingRenewalId { get; set; }
        public int TrainingId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int CourseId { get; set; }
        public string CourseTitle { get; set; }
        public string IssuedBy { get; set; }
        public DateTime ValidUpTo { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingRenewalDataResultModel : DataSourceResultModel<TrainingRenewalInfo>
    {

    }

    public class TrainingRenewalDataRequestModel : DataSourceRequestModel
    {

    }

    public class TrainingRenewalEditResponse : BaseResponse
    {
        public TrainingRenewalEditModel Data { get; set; }

        public TrainingRenewalEditResponse()
        {
            Data = new TrainingRenewalEditModel();
        }
    }

    public class TrainingRenewalEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int TrainingRenewalId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public DateTime ValidUpTo { get; set; }
        public bool IsActive { get; set; }

        public TrainingRenewalEditModel()
        {
            RowId = 0;
            TrainingRenewalId = 0;
            TrainingId = 0;
            ValidUpTo = DateTime.Now;
            IsActive = true;
        }
    }
}

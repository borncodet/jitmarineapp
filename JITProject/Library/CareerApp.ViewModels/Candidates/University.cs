﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class UniversitySelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> States { get; set; }
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
    }

    public class UniversityPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int UniversityId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        [StringLength(10)]
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
    }

    public class UniversityInfo
    {
        public int RowId { get; set; }
        public int UniversityId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
    }

    public class UniversityDataResultModel : DataSourceResultModel<UniversityInfo>
    {

    }

    public class UniversityDataRequestModel : DataSourceRequestModel
    {

    }

    public class UniversityEditResponse : BaseResponse
    {
        public UniversityEditModel Data { get; set; }

        public UniversityEditResponse()
        {
            Data = new UniversityEditModel();
        }
    }

    public class UniversityEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int UniversityId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        [StringLength(10)]
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }

        public UniversityEditModel()
        {
            RowId = 0;
            UniversityId = 0;
            Name = "";
            Email = "";
            TelephoneNumber = "";
            Address1 = "";
            Address2 = "";
            City = "";
            StateId = 0;
            CountryId = 0;
            ZipCode = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CourseSelectBoxDataViewModel
    {

    }

    public class CoursePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CourseId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class CourseInfo
    {
        public int RowId { get; set; }
        public int CourseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class CourseDataResultModel : DataSourceResultModel<CourseInfo>
    {

    }

    public class CourseDataRequestModel : DataSourceRequestModel
    {

    }

    public class CourseEditResponse : BaseResponse
    {
        public CourseEditModel Data { get; set; }

        public CourseEditResponse()
        {
            Data = new CourseEditModel();
        }
    }

    public class CourseEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CourseId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public CourseEditModel()
        {
            RowId = 0;
            CourseId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateReferencesSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> NamePrefix { get; set; }
        public IEnumerable<ValueCaptionPair> Genders { get; set; }
        public IEnumerable<ValueCaptionPair> Relationships { get; set; }
    }

    public class CandidateReferencesPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateReferenceId { get; set; }
        public int? NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateReferencesInfo
    {
        public int RowId { get; set; }
        public int CandidateReferenceId { get; set; }
        public int? NamePrefixId { get; set; }
        public string NamePrefixTitle { get; set; }
        public string ReferenceFirstName { get; set; }
        public string ReferenceLastName { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public int CandidateId { get; set; }
        public string CandidateFirstName { get; set; }
        public string CandidateLastName { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateReferencesDataResultModel : DataSourceResultModel<CandidateReferencesInfo>
    {

    }

    public class CandidateReferencesDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateReferencesEditResponse : BaseResponse
    {
        public CandidateReferencesEditModel Data { get; set; }

        public CandidateReferencesEditResponse()
        {
            Data = new CandidateReferencesEditModel();
        }
    }

    public class CandidateReferencesEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateReferenceId { get; set; }
        public int? NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }

        public CandidateReferencesEditModel()
        {
            RowId = 0;
            CandidateReferenceId = 0;
            NamePrefixId = null;
            FirstName = "";
            LastName = "";
            Email = "";
            CountryCode = "";
            PhoneNumber = "";
            CandidateId = 0;
            IsActive = true;
        }
    }
}

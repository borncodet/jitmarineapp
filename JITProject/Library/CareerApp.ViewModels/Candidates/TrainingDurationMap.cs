﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class TrainingDurationMapSelectBoxDataViewModel
    {
    }

    public class TrainingDurationMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int TrainingDurationMapId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public int DurationId { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingDurationMapInfo
    {
        public int RowId { get; set; }
        public int TrainingDurationMapId { get; set; }
        public int TrainingId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int CourseId { get; set; }
        public string CourseTitle { get; set; }
        public string IssuedBy { get; set; }
        public int DurationId { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingDurationMapDataResultModel : DataSourceResultModel<TrainingDurationMapInfo>
    {

    }

    public class TrainingDurationMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class TrainingDurationMapEditResponse : BaseResponse
    {
        public TrainingDurationMapEditModel Data { get; set; }

        public TrainingDurationMapEditResponse()
        {
            Data = new TrainingDurationMapEditModel();
        }
    }

    public class TrainingDurationMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int TrainingDurationMapId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public int DurationId { get; set; }
        public bool IsActive { get; set; }

        public TrainingDurationMapEditModel()
        {
            RowId = 0;
            TrainingDurationMapId = 0;
            TrainingId = 0;
            DurationId = 0;
            IsActive = true;
        }
    }
}

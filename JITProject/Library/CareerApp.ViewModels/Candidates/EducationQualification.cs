﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class EducationQualificationSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Courses { get; set; }
    }

    public class EducationQualificationPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int EducationQualificationId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        //[Required]
        //public int CourseId { get; set; }
        [Required]
        public string Course { get; set; }
        [Required]
        public string University { get; set; }
        [StringLength(10)]
        public string Grade { get; set; }
        [Required]
        public DateTime DateFrom { get; set; }
        [Required]
        public DateTime DateTo { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class EducationQualificationInfo
    {
        public int RowId { get; set; }
        public int EducationQualificationId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        //public int CourseId { get; set; }
        //public string CourseTitle { get; set; }
        //[Required]
        public string Course { get; set; }
        public string University { get; set; }
        public string Grade { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class EducationQualificationDataResultModel : DataSourceResultModel<EducationQualificationInfo>
    {

    }

    public class EducationQualificationDataRequestModel : DataSourceRequestModel
    {

    }

    public class EducationQualificationEditResponse : BaseResponse
    {
        public EducationQualificationEditModel Data { get; set; }

        public EducationQualificationEditResponse()
        {
            Data = new EducationQualificationEditModel();
        }
    }

    public class EducationQualificationEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int EducationQualificationId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        //[Required]
        //public int CourseId { get; set; }
        [Required]
        public string Course { get; set; }
        [Required]
        public string University { get; set; }
        [StringLength(10)]
        public string Grade { get; set; }
        [Required]
        public DateTime DateFrom { get; set; }
        [Required]
        public DateTime DateTo { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }

        public EducationQualificationEditModel()
        {
            RowId = 0;
            EducationQualificationId = 0;
            CandidateId = 0;
            Course = "";
            University = "";
            Grade = "";
            DateFrom = DateTime.Now;
            DateTo = DateTime.Now;
            DigiDocumentDetailId = 0;
            IsActive = true;
        }
    }
}

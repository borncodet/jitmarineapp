﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class TrainingSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Courses { get; set; }
    }

    public class TrainingPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        //[Required]
        //public int CourseId { get; set; }
        //[Required]
        //public string IssuedBy { get; set; }
        //[Required]
        //public DateTime ValidUpTo { get; set; }
        [Required]
        public string TrainingCertificate { get; set; }
        [Required]
        public string Institute { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingInfo
    {
        public int RowId { get; set; }
        public int TrainingId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        //public int CourseId { get; set; }
        //public string CourseTitle { get; set; }
        //public string IssuedBy { get; set; }
        //public DateTime ValidUpTo { get; set; }
        public string TrainingCertificate { get; set; }
        public string Institute { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrainingDataResultModel : DataSourceResultModel<TrainingInfo>
    {

    }

    public class TrainingDataRequestModel : DataSourceRequestModel
    {

    }

    public class TrainingEditResponse : BaseResponse
    {
        public TrainingEditModel Data { get; set; }

        public TrainingEditResponse()
        {
            Data = new TrainingEditModel();
        }
    }

    public class TrainingEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        //[Required]
        //public int CourseId { get; set; }
        //[Required]
        //public string IssuedBy { get; set; }
        //[Required]
        //public DateTime ValidUpTo { get; set; }
        [Required]
        public string TrainingCertificate { get; set; }
        [Required]
        public string Institute { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }

        public TrainingEditModel()
        {
            RowId = 0;
            TrainingId = 0;
            CandidateId = 0;
            //CourseId = 0;
            //IssuedBy = "";
            //ValidUpTo = DateTime.Now;
            TrainingCertificate = "";
            Institute = "";
            DigiDocumentDetailId = 0;
            IsActive = true;
        }
    }
}

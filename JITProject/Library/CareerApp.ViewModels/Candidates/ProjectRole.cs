﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ProjectRoleSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> JobRoles { get; set; }
    }

    public class ProjectRolePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ProjectRoleId { get; set; }
        [Required]
        public int CandidateProjectId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ProjectRoleInfo
    {
        public int RowId { get; set; }
        public int ProjectRoleId { get; set; }
        public int CandidateProjectId { get; set; }
        public string ProjectName { get; set; }
        public int JobRoleId { get; set; }
        public string JobRoleTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class ProjectRoleDataResultModel : DataSourceResultModel<ProjectRoleInfo>
    {

    }

    public class ProjectRoleDataRequestModel : DataSourceRequestModel
    {

    }

    public class ProjectRoleEditResponse : BaseResponse
    {
        public ProjectRoleEditModel Data { get; set; }

        public ProjectRoleEditResponse()
        {
            Data = new ProjectRoleEditModel();
        }
    }

    public class ProjectRoleEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ProjectRoleId { get; set; }
        [Required]
        public int CandidateProjectId { get; set; }
        [Required]
        public int JobRoleId { get; set; }
        public bool IsActive { get; set; }

        public ProjectRoleEditModel()
        {
            RowId = 0;
            ProjectRoleId = 0;
            CandidateProjectId = 0;
            JobRoleId = 0;
            IsActive = true;
        }
    }
}

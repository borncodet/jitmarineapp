﻿using CareerApp.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobBookmarkedSelectBoxDataViewModel
    {
    }

    public class JobBookmarkedPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobBookmarkedId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobBookmarkedInfo
    {
        public int RowId { get; set; }
        public int JobBookmarkedId { get; set; }
        public int CandidateId { get; set; }
        public string CandidatetName { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsActive { get; set; }
    }


    public class RecentlySavedJobInfo
    {
        public int RowId { get; set; }
        public int JobId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ExperienceId { get; set; }
        public string Experience { get; set; }
        public int NumberOfVacancies { get; set; }
        public int JobTypeId { get; set; }
        public string JobType { get; set; }
        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        public decimal MinAnnualSalary { get; set; }
        public decimal MaxAnnualSalary { get; set; }
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public int IndustryId { get; set; }
        public string Industry { get; set; }
        public int FunctionalAreaId { get; set; }
        public string FunctionalArea { get; set; }
        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }
        public int PreferedLangId { get; set; }
        public string PreferedLanguage { get; set; }
        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }
        public DateTime PostedDate { get; set; }
        public int JobBookmarkedId { get; set; }
        public int CandidateId { get; set; }
        public string CandidatetName { get; set; }
        public string DaysAgo { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsApplied { get; set; }
        public bool IsActive { get; set; }
    }


    public class JobBookmarkedDataResultModel : DataSourceResultModel<JobBookmarkedInfo>
    {

    }

    public class JobBookmarkedDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobBookmarkedCandidateDataResultModel : DataSourceResultModel<RecentlySavedJobInfo>
    {

    }

    public class JobBookmarkedCandidateDataRequestModel : DataSourceCandidateRequestModel
    {

    }

    public class JobBookmarkedEditResponse : BaseResponse
    {
        public JobBookmarkedEditModel Data { get; set; }

        public JobBookmarkedEditResponse()
        {
            Data = new JobBookmarkedEditModel();
        }
    }

    public class JobBookmarkedEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobBookmarkedId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }

        public JobBookmarkedEditModel()
        {
            RowId = 0;
            JobBookmarkedId = 0;
            CandidateId = 0;
            JobId = 0;
            IsActive = true;
        }
    }
}

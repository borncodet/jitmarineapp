﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateEducationCertificateSelectBoxDataViewModel
    {
    }

    public class CandidateEducationCertificatePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateEducationCertificateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int EducationQualificationId { get; set; }
        [Required]
        public string Certificate { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateEducationCertificateInfo
    {
        public int RowId { get; set; }
        public int CandidateEducationCertificateId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int EducationQualificationId { get; set; }
        public string Course { get; set; }
        public string University { get; set; }
        public string Grade { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Certificate { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateEducationCertificateDataResultModel : DataSourceResultModel<CandidateEducationCertificateInfo>
    {

    }

    public class CandidateEducationCertificateDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateEducationCertificateEditResponse : BaseResponse
    {
        public CandidateEducationCertificateEditModel Data { get; set; }

        public CandidateEducationCertificateEditResponse()
        {
            Data = new CandidateEducationCertificateEditModel();
        }
    }

    public class CandidateEducationCertificateEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateEducationCertificateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int EducationQualificationId { get; set; }
        [Required]
        public string Certificate { get; set; }
        public bool IsActive { get; set; }

        public CandidateEducationCertificateEditModel()
        {
            RowId = 0;
            CandidateEducationCertificateId = 0;
            CandidateId = 0;
            EducationQualificationId = 0;
            Certificate = "";
            IsActive = true;
        }
    }
}

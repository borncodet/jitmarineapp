﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateProfileImageSelectBoxDataViewModel
    {

    }

    public class CandidateProfileImagePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateProfileImageId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        [Required]
        public IFormFile Document { get; set; }
        public string Base64Image { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateProfileImageInfo
    {
        public int RowId { get; set; }
        public int CandidateProfileImageId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public string ImageUrl { get; set; }
        public string Base64Image { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateProfileImageDataResultModel : DataSourceResultModel<CandidateProfileImageInfo>
    {

    }

    public class CandidateProfileImageDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateProfileImageEditResponse : BaseResponse
    {
        public CandidateProfileImageEditModel Data { get; set; }

        public CandidateProfileImageEditResponse()
        {
            Data = new CandidateProfileImageEditModel();
        }
    }

    public class CandidateProfileImageEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateProfileImageId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        public string Base64Image { get; set; }
        public bool IsActive { get; set; }

        public CandidateProfileImageEditModel()
        {
            RowId = 0;
            CandidateProfileImageId = 0;
            ImageUrl = "";
            Base64Image = "";
            IsActive = true;
        }
    }
}

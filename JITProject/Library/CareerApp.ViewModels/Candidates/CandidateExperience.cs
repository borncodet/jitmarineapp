﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateExperienceSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> JobRoles { get; set; }
    }

    public class CandidateExperiencePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateExperienceId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        [StringLength(50)]
        public string EmployerName { get; set; }
        public string LocationId { get; set; }
        //[Required]
        //public int JobRoleId { get; set; }
        [Required]
        public string JobRole { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool CurrentlyWorkHereFlag { get; set; }
        public string Responsibilities { get; set; }
        public string Achievements { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateExperienceInfo
    {
        public int RowId { get; set; }
        public int CandidateExperienceId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public string EmployerName { get; set; }
        public string LocationId { get; set; }
        //public int JobRoleId { get; set; }
        //public string JobRoleTitle { get; set; }
        public string JobRole { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool CurrentlyWorkHereFlag { get; set; }
        public string Responsibilities { get; set; }
        public string Achievements { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateExperienceDataResultModel : DataSourceResultModel<CandidateExperienceInfo>
    {

    }

    public class CandidateExperienceDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateExperienceEditResponse : BaseResponse
    {
        public CandidateExperienceEditModel Data { get; set; }

        public CandidateExperienceEditResponse()
        {
            Data = new CandidateExperienceEditModel();
        }
    }

    public class CandidateExperienceEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateExperienceId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        [StringLength(50)]
        public string EmployerName { get; set; }
        public string LocationId { get; set; }
        //[Required]
        //public int JobRoleId { get; set; }
        [Required]
        public string JobRole { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool CurrentlyWorkHereFlag { get; set; }
        public string Responsibilities { get; set; }
        public string Achievements { get; set; }
        public bool IsActive { get; set; }

        public CandidateExperienceEditModel()
        {
            RowId = 0;
            CandidateExperienceId = 0;
            CandidateId = 0;
            EmployerName = "";
            LocationId = "";
            JobRole = "";
            FromDate = DateTime.Now;
            ToDate = null;
            CurrentlyWorkHereFlag = true;
            Responsibilities = "";
            Achievements = "";
            IsActive = true;
        }
    }
}

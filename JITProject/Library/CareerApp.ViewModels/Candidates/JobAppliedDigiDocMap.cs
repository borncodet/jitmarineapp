﻿using CareerApp.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAppliedDigiDocMapSelectBoxDataViewModel
    {
    }

    public class JobAppliedDigiDocMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAppliedDigiDocMapId { get; set; }
        [Required]
        public int JobAppliedDetailsId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAppliedDigiDocMapInfo
    {
        public int RowId { get; set; }
        public int JobAppliedDigiDocMapId { get; set; }
        public int JobAppliedDetailsId { get; set; }
        public int DigiDocumentDetailId { get; set; }
        public string Name { get; set; }
        public string DocumentNumber { get; set; }
        public string Description { get; set; }
        public int DigiDocumentTypeId { get; set; }
        public string DigiDocumentTypeTitle { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool ExpiryFlag { get; set; }
        public bool IsExpired { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAppliedDigiDocMapDataResultModel : DataSourceResultModel<JobAppliedDigiDocMapInfo>
    {

    }

    public class JobAppliedDigiDocMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAppliedDigiDocMapEditResponse : BaseResponse
    {
        public JobAppliedDigiDocMapEditModel Data { get; set; }

        public JobAppliedDigiDocMapEditResponse()
        {
            Data = new JobAppliedDigiDocMapEditModel();
        }
    }

    public class JobAppliedDigiDocMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAppliedDigiDocMapId { get; set; }
        [Required]
        public int JobAppliedDetailsId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }

        public JobAppliedDigiDocMapEditModel()
        {
            RowId = 0;
            JobAppliedDigiDocMapId = 0;
            JobAppliedDetailsId = 0;
            DigiDocumentDetailId = 0;
            IsActive = true;
        }
    }
}

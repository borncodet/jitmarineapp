﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareerApp.ViewModels
{
    public class CandidateSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> NamePrefix { get; set; }
        public IEnumerable<ValueCaptionPair> Genders { get; set; }
        public IEnumerable<ValueCaptionPair> MartialStatus { get; set; }
        public IEnumerable<ValueCaptionPair> JobCategories { get; set; }
        public IEnumerable<ValueCaptionPair> Designations { get; set; }
        public IEnumerable<ValueCaptionPair> JobTypes { get; set; }
        public IEnumerable<ValueCaptionPair> Industries { get; set; }
        public IEnumerable<ValueCaptionPair> FunctionalAreas { get; set; }
        public IEnumerable<ValueCaptionPair> JobRoles { get; set; }
        public IEnumerable<ValueCaptionPair> States { get; set; }
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
        public IEnumerable<ValueCaptionPair> CountryCode { get; set; }
        public IEnumerable<ValueCaptionPair> Languages { get; set; }
    }

    public class CandidatePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public int? NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public int? GenderId { get; set; }
        public int? MartialStatusId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        public int? LicenceInformationId { get; set; }
        public int? PassportInformationId { get; set; }
        public int? SeamanBookCdcId { get; set; }
        [StringLength(20)]
        public string PanNo { get; set; }
        [StringLength(20)]
        public string AadharNo { get; set; }
        public string ResidenceId { get; set; }
        public int? PanDigiDocumentDetailId { get; set; }
        public int? AadharDigiDocumentDetailId { get; set; }
        public int? ResidenceDigiDocumentDetailId { get; set; }
        public int? CategoryId { get; set; }
        public int? DesignationId { get; set; }
        public int? JobTypeId { get; set; }
        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CurrentCTC { get; set; }
        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ExpectedPackage { get; set; }
        public int? IndustryId { get; set; }
        public int? FunctionalAreaId { get; set; }
        public int? JobRoleId { get; set; }
        [Required]
        public string Skills { get; set; }
        [Required]
        public string ProfileSummary { get; set; }
        public string PositionApplyingFor { get; set; }
        public DateTime? Dob { get; set; }
        [Required]
        public string CurrentAddress1 { get; set; }
        public string CurrentAddress2 { get; set; }
        [Required]
        public string PermanantAddress1 { get; set; }
        public string PermanantAddress2 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        [Required]
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required]
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateInfo
    {
        public int RowId { get; set; }
        public int CandidateId { get; set; }
        public int? NamePrefixId { get; set; }
        public string NamePrefixTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? GenderId { get; set; }
        public string GenderTitle { get; set; }
        public int? MartialStatusId { get; set; }
        public string MartialStatusTitle { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string TelephoneNumber { get; set; }
        public int? LicenceInformationId { get; set; }
        public string LicenceNo { get; set; }
        public int? PassportInformationId { get; set; }
        public string PassportNo { get; set; }
        public int? SeamanBookCdcId { get; set; }
        public string CdcNumber { get; set; }
        public string PanNo { get; set; }
        public string AadharNo { get; set; }
        public string ResidenceId { get; set; }
        public int? PanDigiDocumentDetailId { get; set; }
        public int? AadharDigiDocumentDetailId { get; set; }
        public int? ResidenceDigiDocumentDetailId { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? DesignationId { get; set; }
        public string DesignationName { get; set; }
        public int? JobTypeId { get; set; }
        public string JobTypeName { get; set; }
        public decimal CurrentCTC { get; set; }
        public decimal ExpectedPackage { get; set; }
        public int? IndustryId { get; set; }
        public string IndustryName { get; set; }
        public int? FunctionalAreaId { get; set; }
        public string FunctionalAreaName { get; set; }
        public int? JobRoleId { get; set; }
        public string JobRoleName { get; set; }
        public string Skills { get; set; }
        public string ProfileSummary { get; set; }
        public string PositionApplyingFor { get; set; }
        public DateTime? Dob { get; set; }
        public string CurrentAddress1 { get; set; }
        public string CurrentAddress2 { get; set; }
        public string PermanantAddress1 { get; set; }
        public string PermanantAddress2 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateDataResultModel : DataSourceResultModel<CandidateInfo>
    {

    }

    public class CandidateDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateEditResponse : BaseResponse
    {
        public CandidateEditModel Data { get; set; }

        public CandidateEditResponse()
        {
            Data = new CandidateEditModel();
        }
    }

    public class CandidateEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public int? NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public int? GenderId { get; set; }
        public int? MartialStatusId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        public int? PassportInformationId { get; set; }
        public int? LicenceInformationId { get; set; }
        public int? SeamanBookCdcId { get; set; }
        [StringLength(20)]
        public string PanNo { get; set; }
        [StringLength(20)]
        public string AadharNo { get; set; }
        public string ResidenceId { get; set; }
        public int? PanDigiDocumentDetailId { get; set; }
        public int? AadharDigiDocumentDetailId { get; set; }
        public int? ResidenceDigiDocumentDetailId { get; set; }
        public int? CategoryId { get; set; }
        public int? DesignationId { get; set; }
        public int? JobTypeId { get; set; }
        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CurrentCTC { get; set; }
        [Required]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ExpectedPackage { get; set; }
        public int? IndustryId { get; set; }
        public int? FunctionalAreaId { get; set; }
        public int? JobRoleId { get; set; }
        [Required]
        public string Skills { get; set; }
        [Required]
        public string ProfileSummary { get; set; }
        public string PositionApplyingFor { get; set; }
        public DateTime? Dob { get; set; }
        [Required]
        public string CurrentAddress1 { get; set; }
        public string CurrentAddress2 { get; set; }
        [Required]
        public string PermanantAddress1 { get; set; }
        public string PermanantAddress2 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        [Required]
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required]
        public int UserId { get; set; }
        public bool IsActive { get; set; }

        public CandidateEditModel()
        {
            RowId = 0;
            CandidateId = 0;
            NamePrefixId = null;
            FirstName = "";
            LastName = "";
            GenderId = 0;
            MartialStatusId = null;
            Email = "";
            CountryCode = "";
            PhoneNumber = "";
            TelephoneNumber = "";
            LicenceInformationId = null;
            PassportInformationId = null;
            SeamanBookCdcId = null;
            PanNo = "";
            AadharNo = "";
            ResidenceId = "";
            PanDigiDocumentDetailId = 0;
            AadharDigiDocumentDetailId = 0;
            ResidenceDigiDocumentDetailId = 0;
            CategoryId = null;
            DesignationId = null;
            JobTypeId = null;
            CurrentCTC = 0;
            ExpectedPackage = 0;
            IndustryId = null;
            FunctionalAreaId = null;
            JobRoleId = null;
            Skills = "";
            ProfileSummary = "";
            PositionApplyingFor = "";
            Dob = DateTime.Now;
            CurrentAddress1 = "";
            CurrentAddress2 = "";
            PermanantAddress1 = "";
            PermanantAddress2 = "";
            City = "";
            StateId = null;
            CountryId = null;
            ZipCode = "";
            UserId = 0;
            IsActive = true;
        }
    }
}

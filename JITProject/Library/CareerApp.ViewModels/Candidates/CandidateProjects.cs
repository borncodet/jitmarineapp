﻿using CareerApp.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateProjectsSelectBoxDataViewModel
    {
    }

    public class CandidateProjectsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateProjectId { get; set; }
        [Required]
        public string ProjectName { get; set; }
        [Required]
        public int TeamSize { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateProjectsInfo
    {
        public int RowId { get; set; }
        public int CandidateProjectId { get; set; }
        public string ProjectName { get; set; }
        public int TeamSize { get; set; }
        public string Description { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateProjectsDataResultModel : DataSourceResultModel<CandidateProjectsInfo>
    {

    }

    public class CandidateProjectsDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateProjectsEditResponse : BaseResponse
    {
        public CandidateProjectsEditModel Data { get; set; }

        public CandidateProjectsEditResponse()
        {
            Data = new CandidateProjectsEditModel();
        }
    }

    public class CandidateProjectsEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateProjectId { get; set; }
        [Required]
        public string ProjectName { get; set; }
        [Required]
        public int TeamSize { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }

        public CandidateProjectsEditModel()
        {
            RowId = 0;
            CandidateProjectId = 0;
            ProjectName = "";
            TeamSize = 0;
            Description = "";
            FromDate = DateTime.Now;
            ToDate = null;
            CandidateId = 0;
            IsActive = true;
        }
    }
}

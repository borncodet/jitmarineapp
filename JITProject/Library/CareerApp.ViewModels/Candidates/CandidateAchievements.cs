﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateAchievementsSelectBoxDataViewModel
    {
    }

    public class CandidateAchievementsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateAchievementId { get; set; }
        [Required]
        public int CandidateExperienceId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateAchievementsInfo
    {
        public int RowId { get; set; }
        public int CandidateAchievementId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int CandidateExperienceId { get; set; }
        public string EmployerName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateAchievementsDataResultModel : DataSourceResultModel<CandidateAchievementsInfo>
    {

    }

    public class CandidateAchievementsDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateAchievementsEditResponse : BaseResponse
    {
        public CandidateAchievementsEditModel Data { get; set; }

        public CandidateAchievementsEditResponse()
        {
            Data = new CandidateAchievementsEditModel();
        }
    }

    public class CandidateAchievementsEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateAchievementId { get; set; }
        [Required]
        public int CandidateExperienceId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public CandidateAchievementsEditModel()
        {
            RowId = 0;
            CandidateAchievementId = 0;
            CandidateExperienceId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

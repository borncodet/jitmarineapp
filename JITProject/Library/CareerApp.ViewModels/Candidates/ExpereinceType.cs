﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ExpereinceTypeSelectBoxDataViewModel
    {

    }

    public class ExpereinceTypePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ExpereinceTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        public int FromDay { get; set; }
        [Required]
        public int FromMonth { get; set; }
        [Required]
        public int FromYear { get; set; }
        [Required]
        public int ToDay { get; set; }
        [Required]
        public int ToMonth { get; set; }
        [Required]
        public int ToYear { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class ExpereinceTypeInfo
    {
        public int RowId { get; set; }
        public int ExpereinceTypeId { get; set; }
        public string Title { get; set; }
        public int FromDay { get; set; }
        public int FromMonth { get; set; }
        public int FromYear { get; set; }
        public int ToDay { get; set; }
        public int ToMonth { get; set; }
        public int ToYear { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class ExpereinceTypeDataResultModel : DataSourceResultModel<ExpereinceTypeInfo>
    {

    }

    public class ExpereinceTypeDataRequestModel : DataSourceRequestModel
    {

    }

    public class ExpereinceTypeEditResponse : BaseResponse
    {
        public ExpereinceTypeEditModel Data { get; set; }

        public ExpereinceTypeEditResponse()
        {
            Data = new ExpereinceTypeEditModel();
        }
    }

    public class ExpereinceTypeEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ExpereinceTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        public int FromDay { get; set; }
        [Required]
        public int FromMonth { get; set; }
        [Required]
        public int FromYear { get; set; }
        [Required]
        public int ToDay { get; set; }
        [Required]
        public int ToMonth { get; set; }
        [Required]
        public int ToYear { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public ExpereinceTypeEditModel()
        {
            RowId = 0;
            ExpereinceTypeId = 0;
            Title = "";
            FromDay = 0;
            FromMonth = 0;
            FromYear = 0;
            ToDay = 0;
            ToMonth = 0;
            ToYear = 0;
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateSkillsSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Proficiencies { get; set; }
    }

    public class CandidateSkillsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateSkillId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public string SkillName { get; set; }
        [Required]
        public int ProficiencyId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateSkillsInfo
    {
        public int RowId { get; set; }
        public int CandidateSkillId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public string SkillName { get; set; }
        public int ProficiencyId { get; set; }
        public string ProficiencyTitle { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateSkillsDataResultModel : DataSourceResultModel<CandidateSkillsInfo>
    {

    }

    public class CandidateSkillsDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateSkillsEditResponse : BaseResponse
    {
        public CandidateSkillsEditModel Data { get; set; }

        public CandidateSkillsEditResponse()
        {
            Data = new CandidateSkillsEditModel();
        }
    }

    public class CandidateSkillsEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateSkillId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public string SkillName { get; set; }
        [Required]
        public int ProficiencyId { get; set; }
        public bool IsActive { get; set; }

        public CandidateSkillsEditModel()
        {
            RowId = 0;
            CandidateSkillId = 0;
            CandidateId = 0;
            SkillName = "";
            ProficiencyId = 0;
            IsActive = true;
        }
    }
}

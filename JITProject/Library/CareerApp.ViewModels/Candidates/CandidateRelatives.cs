﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateRelativesSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> NamePrefix { get; set; }
        public IEnumerable<ValueCaptionPair> Genders { get; set; }
        public IEnumerable<ValueCaptionPair> Relationships { get; set; }
    }

    public class CandidateRelativesPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateRelativeId { get; set; }
        public int? NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        public int GenderId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        [Required]
        public int RelationshipId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateRelativesInfo
    {
        public int RowId { get; set; }
        public int CandidateRelativeId { get; set; }
        public int? NamePrefixId { get; set; }
        public string NamePrefixTitle { get; set; }
        public string RelativeFirstName { get; set; }
        public string RelativeLastName { get; set; }
        public int GenderId { get; set; }
        public string GenderTitle { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string TelephoneNumber { get; set; }
        public int RelationshipId { get; set; }
        public string RelationshipTitle { get; set; }
        public int CandidateId { get; set; }
        public string CandidateFirstName { get; set; }
        public string CandidateLastName { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateRelativesDataResultModel : DataSourceResultModel<CandidateRelativesInfo>
    {

    }

    public class CandidateRelativesDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateRelativesEditResponse : BaseResponse
    {
        public CandidateRelativesEditModel Data { get; set; }

        public CandidateRelativesEditResponse()
        {
            Data = new CandidateRelativesEditModel();
        }
    }

    public class CandidateRelativesEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateRelativeId { get; set; }
        public int? NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        public int GenderId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string TelephoneNumber { get; set; }
        [Required]
        public int RelationshipId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }

        public CandidateRelativesEditModel()
        {
            RowId = 0;
            CandidateRelativeId = 0;
            NamePrefixId = null;
            FirstName = "";
            LastName = "";
            GenderId = 0;
            Email = "";
            CountryCode = "";
            PhoneNumber = "";
            TelephoneNumber = "";
            RelationshipId = 0;
            CandidateId = 0;
            IsActive = true;
        }
    }
}

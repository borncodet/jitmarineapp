﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class SocialAccountsSelectBoxDataViewModel
    {

    }

    public class SocialAccountsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int SocialAccountId { get; set; }
        [Required]
        public string Facebooks { get; set; }
        [Required]
        public string Google { get; set; }
        [Required]
        public string Twitter { get; set; }
        [Required]
        public string LinkedIn { get; set; }
        [Required]
        public string Pinterest { get; set; }
        [Required]
        public string Instagram { get; set; }
        [Required]
        public string Other { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }
    }

    public class SocialAccountsInfo
    {
        public int RowId { get; set; }
        public int SocialAccountId { get; set; }
        public string Facebooks { get; set; }
        public string Google { get; set; }
        public string Twitter { get; set; }
        public string LinkedIn { get; set; }
        public string Pinterest { get; set; }
        public string Instagram { get; set; }
        public string Other { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public bool IsActive { get; set; }
    }

    public class SocialAccountsDataResultModel : DataSourceResultModel<SocialAccountsInfo>
    {

    }

    public class SocialAccountsDataRequestModel : DataSourceRequestModel
    {

    }

    public class SocialAccountsEditResponse : BaseResponse
    {
        public SocialAccountsEditModel Data { get; set; }

        public SocialAccountsEditResponse()
        {
            Data = new SocialAccountsEditModel();
        }
    }

    public class SocialAccountsEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int SocialAccountId { get; set; }
        [Required]
        public string Facebooks { get; set; }
        [Required]
        public string Google { get; set; }
        [Required]
        public string Twitter { get; set; }
        [Required]
        public string LinkedIn { get; set; }
        [Required]
        public string Pinterest { get; set; }
        [Required]
        public string Instagram { get; set; }
        [Required]
        public string Other { get; set; }
        [Required]
        public int CandidateId { get; set; }
        public bool IsActive { get; set; }

        public SocialAccountsEditModel()
        {
            RowId = 0;
            SocialAccountId = 0;
            Facebooks = "";
            Google = "";
            Twitter = "";
            LinkedIn = "";
            Pinterest = "";
            Instagram = "";
            Other = "";
            CandidateId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class RelationshipSelectBoxDataViewModel
    {

    }

    public class RelationshipPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int RelationshipId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class RelationshipInfo
    {
        public int RowId { get; set; }
        public int RelationshipId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class RelationshipDataResultModel : DataSourceResultModel<RelationshipInfo>
    {

    }

    public class RelationshipDataRequestModel : DataSourceRequestModel
    {

    }

    public class RelationshipEditResponse : BaseResponse
    {
        public RelationshipEditModel Data { get; set; }

        public RelationshipEditResponse()
        {
            Data = new RelationshipEditModel();
        }
    }

    public class RelationshipEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int RelationshipId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public RelationshipEditModel()
        {
            RowId = 0;
            RelationshipId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

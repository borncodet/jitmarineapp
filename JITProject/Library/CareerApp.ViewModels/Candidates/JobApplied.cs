﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class JobAppliedSelectBoxDataViewModel
    {
    }

    public class JobAppliedPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAppliedId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAppliedInfo
    {
        public int RowId { get; set; }
        public int JobAppliedId { get; set; }
        public int CandidateId { get; set; }
        public string CandidatetName { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsApplied { get; set; }
        public bool IsActive { get; set; }
    }

    public class CountInfo
    {
        public int JobApplied { get; set; }
        public int JobSaved { get; set; }
        public int ResumeCreated { get; set; }
        public int NewAlerts { get; set; }
        public int ShortListed { get; set; }
        public int NewMessages { get; set; }
        public List<DigiDocumentCountInfo> DigiDocs { get; set; }
        public List<DigiDocumentCountInfo> ExpiryDigiDocs { get; set; }
    }

    public class ProgressInfo
    {
        public int ProfileProgress { get; set; }
        public int ResumeProgress { get; set; }
    }

    public class RecentlyAppliedJobInfo
    {
        public int RowId { get; set; }
        public int JobId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ExperienceId { get; set; }
        public string Experience { get; set; }
        public int NumberOfVacancies { get; set; }
        public int JobTypeId { get; set; }
        public string JobType { get; set; }
        public bool IsPreferred { get; set; }
        public bool IsRequired { get; set; }
        public string LocationId { get; set; }
        public string RegionId { get; set; }
        public string TerritoryId { get; set; }
        public decimal MinAnnualSalary { get; set; }
        public decimal MaxAnnualSalary { get; set; }
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public int IndustryId { get; set; }
        public string Industry { get; set; }
        public int FunctionalAreaId { get; set; }
        public string FunctionalArea { get; set; }
        public string ProfileDescription { get; set; }
        public bool WillingnessToTravelFlag { get; set; }
        public int PreferedLangId { get; set; }
        public string PreferedLanguage { get; set; }
        public bool AutoScreeningFilterFlag { get; set; }
        public bool AutoSkillAssessmentFlag { get; set; }
        public DateTime PostedDate { get; set; }
        public int JobAppliedId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public string DaysAgo { get; set; }
        public bool IsBookmarked { get; set; }
        public bool IsApplied { get; set; }
        public bool IsActive { get; set; }
    }

    public class JobAppliedDataResultModel : DataSourceResultModel<JobAppliedInfo>
    {

    }

    public class JobAppliedDataRequestModel : DataSourceRequestModel
    {

    }

    public class JobAppliedCandidateDataResultModel : DataSourceResultModel<RecentlyAppliedJobInfo>
    {

    }

    public class JobAppliedCandidateDataRequestModel : DataSourceCandidateRequestModel
    {

    }

    public class JobAppliedEditResponse : BaseResponse
    {
        public JobAppliedEditModel Data { get; set; }

        public JobAppliedEditResponse()
        {
            Data = new JobAppliedEditModel();
        }
    }

    public class JobAppliedEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int JobAppliedId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int JobId { get; set; }
        public bool IsActive { get; set; }

        public JobAppliedEditModel()
        {
            RowId = 0;
            JobAppliedId = 0;
            CandidateId = 0;
            JobId = 0;
            IsActive = true;
        }
    }
}

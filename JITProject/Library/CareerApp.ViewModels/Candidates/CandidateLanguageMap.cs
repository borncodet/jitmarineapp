﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateLanguageMapSelectBoxDataViewModel
    {
    }

    public class CandidateLanguageMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateLanguageMapId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public List<int> LanguageId { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateLanguageMapInfo
    {
        public int RowId { get; set; }
        public int CandidateLanguageMapId { get; set; }
        public int CandidateId { get; set; }
        public string CandidatetName { get; set; }
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateLanguageMapDataResultModel : DataSourceResultModel<CandidateLanguageMapInfo>
    {

    }

    public class CandidateLanguageMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateLanguageMapEditResponse : BaseResponse
    {
        public CandidateLanguageMapEditModel Data { get; set; }

        public CandidateLanguageMapEditResponse()
        {
            Data = new CandidateLanguageMapEditModel();
        }
    }

    public class CandidateLanguageMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateLanguageMapId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int LanguageId { get; set; }
        public bool IsActive { get; set; }

        public CandidateLanguageMapEditModel()
        {
            RowId = 0;
            CandidateLanguageMapId = 0;
            CandidateId = 0;
            LanguageId = 0;
            IsActive = true;
        }
    }
}

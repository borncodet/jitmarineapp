﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CandidateOtherCertificateSelectBoxDataViewModel
    {
    }

    public class CandidateOtherCertificatePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateOtherCertificateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool ReminderOnExpiryFlag { get; set; }
        [Required]
        public string Certificate { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateOtherCertificateInfo
    {
        public int RowId { get; set; }
        public int CandidateOtherCertificateId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int TrainingId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool ReminderOnExpiryFlag { get; set; }
        public string Certificate { get; set; }
        public bool IsActive { get; set; }
    }

    public class CandidateOtherCertificateDataResultModel : DataSourceResultModel<CandidateOtherCertificateInfo>
    {

    }

    public class CandidateOtherCertificateDataRequestModel : DataSourceRequestModel
    {

    }

    public class CandidateOtherCertificateEditResponse : BaseResponse
    {
        public CandidateOtherCertificateEditModel Data { get; set; }

        public CandidateOtherCertificateEditResponse()
        {
            Data = new CandidateOtherCertificateEditModel();
        }
    }

    public class CandidateOtherCertificateEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CandidateOtherCertificateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int TrainingId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool ReminderOnExpiryFlag { get; set; }
        [Required]
        public string Certificate { get; set; }
        public bool IsActive { get; set; }

        public CandidateOtherCertificateEditModel()
        {
            RowId = 0;
            CandidateOtherCertificateId = 0;
            CandidateId = 0;
            TrainingId = 0;
            DocumentName = "";
            DocumentNumber = "";
            ExpiryDate = DateTime.Now;
            ReminderOnExpiryFlag = true;
            Certificate = "";
            IsActive = true;
        }
    }
}

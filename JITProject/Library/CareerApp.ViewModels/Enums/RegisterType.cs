﻿namespace CareerApp.ViewModels.Enums
{
	public enum RegisterType
	{
		Individual = 1,
		GroupAdmin,
		GroupMember
	}
}

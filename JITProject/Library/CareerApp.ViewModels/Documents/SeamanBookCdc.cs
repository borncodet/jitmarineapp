﻿using CareerApp.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.Core.Models
{
    public class SeamanBookCdcSelectBoxDataViewModel
    {
    }

    public class SeamanBookCdcPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int SeamanBookCdcId { get; set; }
        [Required]
        [StringLength(50)]
        public string CdcNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string PlaceIssued { get; set; }
        [Required]
        public DateTime DateIssued { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class SeamanBookCdcInfo
    {
        public int RowId { get; set; }
        public int SeamanBookCdcId { get; set; }
        public string CdcNumber { get; set; }
        public string PlaceIssued { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class SeamanBookCdcDataResultModel : DataSourceResultModel<SeamanBookCdcInfo>
    {

    }

    public class SeamanBookCdcDataRequestModel : DataSourceRequestModel
    {

    }

    public class SeamanBookCdcEditResponse : BaseResponse
    {
        public SeamanBookCdcEditModel Data { get; set; }

        public SeamanBookCdcEditResponse()
        {
            Data = new SeamanBookCdcEditModel();
        }
    }

    public class SeamanBookCdcEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int SeamanBookCdcId { get; set; }
        [Required]
        [StringLength(50)]
        public string CdcNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string PlaceIssued { get; set; }
        [Required]
        public DateTime DateIssued { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }

        public SeamanBookCdcEditModel()
        {
            RowId = 0;
            SeamanBookCdcId = 0;
            CdcNumber = "";
            PlaceIssued = "";
            DateIssued = DateTime.Now;
            ExpiryDate = DateTime.Now;
            DigiDocumentDetailId = 0;
            IsActive = true;
        }
    }
}

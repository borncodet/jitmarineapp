﻿using CareerApp.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.Core.Models
{
    public class PassportInformationSelectBoxDataViewModel
    {
    }

    public class PassportInformationPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int PassportInformationId { get; set; }
        [Required]
        [StringLength(50)]
        public string PassportNo { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        [StringLength(50)]
        public string PlaceIssued { get; set; }
        [Required]
        public string ECRStatus { get; set; }
        [Required]
        public int CountryId { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class PassportInformationInfo
    {
        public int RowId { get; set; }
        public int PassportInformationId { get; set; }
        public string PassportNo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PlaceIssued { get; set; }
        public string ECRStatus { get; set; }
        public int CountryId { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class PassportInformationDataResultModel : DataSourceResultModel<PassportInformationInfo>
    {

    }

    public class PassportInformationDataRequestModel : DataSourceRequestModel
    {

    }

    public class PassportInformationEditResponse : BaseResponse
    {
        public PassportInformationEditModel Data { get; set; }

        public PassportInformationEditResponse()
        {
            Data = new PassportInformationEditModel();
        }
    }

    public class PassportInformationEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int PassportInformationId { get; set; }
        [Required]
        [StringLength(50)]
        public string PassportNo { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        [StringLength(50)]
        public string PlaceIssued { get; set; }
        [Required]
        public string ECRStatus { get; set; }
        [Required]
        public int CountryId { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }

        public PassportInformationEditModel()
        {
            RowId = 0;
            PassportInformationId = 0;
            PassportNo = "";
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            PlaceIssued = "";
            ECRStatus = "";
            CountryId = 0;
            DigiDocumentDetailId = 0;
            IsActive = true;
        }
    }
}

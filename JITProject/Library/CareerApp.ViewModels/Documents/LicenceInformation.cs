﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class LicenceInformationSelectBoxDataViewModel
    {
    }

    public class LicenceInformationPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int LicenceInformationId { get; set; }
        [Required]
        public string LicenceNo { get; set; }
        [Required]
        public DateTime ValidTill { get; set; }
        [Required]
        public string Licence { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }
    }

    public class LicenceInformationInfo
    {
        public int RowId { get; set; }
        public int LicenceInformationId { get; set; }
        public string LicenceNo { get; set; }
        public DateTime ValidTill { get; set; }
        public string Licence { get; set; }
        public bool IsActive { get; set; }
    }

    public class LicenceInformationDataResultModel : DataSourceResultModel<LicenceInformationInfo>
    {

    }

    public class LicenceInformationDataRequestModel : DataSourceRequestModel
    {

    }

    public class LicenceInformationEditResponse : BaseResponse
    {
        public LicenceInformationEditModel Data { get; set; }

        public LicenceInformationEditResponse()
        {
            Data = new LicenceInformationEditModel();
        }
    }

    public class LicenceInformationEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int LicenceInformationId { get; set; }
        [Required]
        public string LicenceNo { get; set; }
        [Required]
        public DateTime ValidTill { get; set; }
        [Required]
        public string Licence { get; set; }
        public bool IsActive { get; set; }

        public LicenceInformationEditModel()
        {
            RowId = 0;
            LicenceInformationId = 0;
            LicenceNo = "";
            ValidTill = DateTime.Now;
            Licence = "";
            IsActive = true;
        }
    }
}

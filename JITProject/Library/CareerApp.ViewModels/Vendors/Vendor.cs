﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class VendorSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
        public IEnumerable<ValueCaptionPair> States { get; set; }
        public IEnumerable<ValueCaptionPair> VendorStatuses { get; set; }
    }

    public class VendorPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorId { get; set; }
        [StringLength(50)]
        public string VendorName { get; set; }
        public string JobRole { get; set; }
        public string Location { get; set; }
        public string Designation { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(50)]
        public string Organisation { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public string VendorDocument { get; set; }
        public IFormFile Document { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int VendorStatusId { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorInfo
    {
        public int RowId { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string JobRole { get; set; }
        public string Location { get; set; }
        public string Designation { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Organisation { get; set; }
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public string VendorDocument { get; set; }
        [Required]
        public int UserId { get; set; }
        public int VendorStatusId { get; set; }
        public string VendorStatus { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorDataResultModel : DataSourceResultModel<VendorInfo>
    {

    }

    public class VendorDataRequestModel : DataSourceRequestModel
    {

    }

    public class VendorEditResponse : BaseResponse
    {
        public VendorEditModel Data { get; set; }

        public VendorEditResponse()
        {
            Data = new VendorEditModel();
        }
    }

    public class VendorEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorId { get; set; }
        [StringLength(50)]
        public string VendorName { get; set; }
        public string JobRole { get; set; }
        public string Location { get; set; }
        public string Designation { get; set; }
        [Required]
        public string CountryCode { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [StringLength(50)]
        public string Organisation { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public string VendorDocument { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int VendorStatusId { get; set; }
        public bool IsActive { get; set; }

        public VendorEditModel()
        {
            RowId = 0;
            VendorId = 0;
            VendorName = "";
            JobRole = "";
            Location = "";
            Designation = "";
            CountryCode = "";
            PhoneNumber = "";
            Organisation = "";
            Email = "";
            AboutMe = "";
            VendorDocument = "";
            UserId = 0;
            VendorStatusId = 0;
            IsActive = true;
        }
    }
}

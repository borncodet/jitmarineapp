﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class VendorDocumentSelectBoxDataViewModel
    {
    }

    public class VendorDocumentPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorDocumentId { get; set; }
        [Required]
        public string Document1 { get; set; }
        public string Document2 { get; set; }
        public string Document3 { get; set; }
        [Required]
        public int VendorId { get; set; }
        [Required]
        public int VerificationStatus { get; set; }
        public int EmployeeId { get; set; }
        public List<IFormFile> Documents { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorDocumentInfo
    {
        public int RowId { get; set; }
        public int VendorDocumentId { get; set; }
        public string Document1 { get; set; }
        public string Document2 { get; set; }
        public string Document3 { get; set; }
        public int VendorId { get; set; }
        public string CompanyName { get; set; }
        public int VerificationStatus { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorDocumentDataResultModel : DataSourceResultModel<VendorDocumentInfo>
    {

    }

    public class VendorDocumentDataRequestModel : DataSourceRequestModel
    {

    }

    public class VendorDocumentEditResponse : BaseResponse
    {
        public VendorDocumentEditModel Data { get; set; }

        public VendorDocumentEditResponse()
        {
            Data = new VendorDocumentEditModel();
        }
    }

    public class VendorDocumentEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorDocumentId { get; set; }
        [Required]
        public string Document1 { get; set; }
        public string Document2 { get; set; }
        public string Document3 { get; set; }
        [Required]
        public int VendorId { get; set; }
        [Required]
        public int VerificationStatus { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }

        public VendorDocumentEditModel()
        {
            RowId = 0;
            VendorDocumentId = 0;
            Document1 = "";
            Document2 = "";
            Document3 = "";
            VendorId = 0;
            VerificationStatus = 0;
            EmployeeId = 0;
            IsActive = true;
        }
    }
}

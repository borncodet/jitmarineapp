﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class VendorsStatusSelectBoxDataViewModel
    {
    }

    public class VendorsStatusPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorsStatusId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorsStatusInfo
    {
        public int RowId { get; set; }
        public int VendorsStatusId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorsStatusDataResultModel : DataSourceResultModel<VendorsStatusInfo>
    {

    }

    public class VendorsStatusDataRequestModel : DataSourceRequestModel
    {

    }

    public class VendorsStatusEditResponse : BaseResponse
    {
        public VendorsStatusEditModel Data { get; set; }

        public VendorsStatusEditResponse()
        {
            Data = new VendorsStatusEditModel();
        }
    }

    public class VendorsStatusEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorsStatusId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public VendorsStatusEditModel()
        {
            RowId = 0;
            VendorsStatusId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class VendorProfileImageSelectBoxDataViewModel
    {

    }

    public class VendorProfileImagePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorProfileImageId { get; set; }
        [Required]
        public int VendorId { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        [Required]
        public IFormFile Document { get; set; }
        public string Base64Image { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorProfileImageInfo
    {
        public int RowId { get; set; }
        public int VendorProfileImageId { get; set; }
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public string ImageUrl { get; set; }
        public string Base64Image { get; set; }
        public bool IsActive { get; set; }
    }

    public class VendorProfileImageDataResultModel : DataSourceResultModel<VendorProfileImageInfo>
    {

    }

    public class VendorProfileImageDataRequestModel : DataSourceRequestModel
    {

    }

    public class VendorProfileImageEditResponse : BaseResponse
    {
        public VendorProfileImageEditModel Data { get; set; }

        public VendorProfileImageEditResponse()
        {
            Data = new VendorProfileImageEditModel();
        }
    }

    public class VendorProfileImageEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int VendorProfileImageId { get; set; }
        [Required]
        public int VendorId { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        public string Base64Image { get; set; }
        public bool IsActive { get; set; }

        public VendorProfileImageEditModel()
        {
            RowId = 0;
            VendorProfileImageId = 0;
            ImageUrl = "";
            Base64Image = "";
            IsActive = true;
        }
    }
}

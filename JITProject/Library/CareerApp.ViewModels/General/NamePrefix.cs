﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class NamePrefixSelectBoxDataViewModel
    {

    }

    public class NamePrefixPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class NamePrefixInfo
    {
        public int RowId { get; set; }
        public int NamePrefixId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class NamePrefixDataResultModel : DataSourceResultModel<NamePrefixInfo>
    {

    }

    public class NamePrefixDataRequestModel : DataSourceRequestModel
    {

    }

    public class NamePrefixEditResponse : BaseResponse
    {
        public NamePrefixEditModel Data { get; set; }

        public NamePrefixEditResponse()
        {
            Data = new NamePrefixEditModel();
        }
    }

    public class NamePrefixEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int NamePrefixId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public NamePrefixEditModel()
        {
            RowId = 0;
            NamePrefixId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ProficiencySelectBoxDataViewModel
    {

    }

    public class ProficiencyPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ProficiencyId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class ProficiencyInfo
    {
        public int RowId { get; set; }
        public int ProficiencyId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class ProficiencyDataResultModel : DataSourceResultModel<ProficiencyInfo>
    {

    }

    public class ProficiencyDataRequestModel : DataSourceRequestModel
    {

    }

    public class ProficiencyEditResponse : BaseResponse
    {
        public ProficiencyEditModel Data { get; set; }

        public ProficiencyEditResponse()
        {
            Data = new ProficiencyEditModel();
        }
    }

    public class ProficiencyEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ProficiencyId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public ProficiencyEditModel()
        {
            RowId = 0;
            ProficiencyId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

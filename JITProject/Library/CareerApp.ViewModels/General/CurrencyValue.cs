﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CurrencyValuePostmodel
    {
        public List<CurrencyValueSingle> Items { get; set; }
    }

    public class CurrencyValueSingle
    {
        public string RowId { get; set; }
        [Required]
        public string CurrencyId { get; set; }
        [Required]
        public string ValueInId { get; set; }
        [Required]
        public decimal Value { get; set; }
        public bool Active { get; set; }
    }

    public class CurrencyValueEditModel
    {
        public string Id { get; set; }
        [Required]
        public string CurrencyId { get; set; }
        [Required]
        public string ValueInId { get; set; }
        [Required]
        public decimal Value { get; set; }
        public bool Active { get; set; }
    }

    public class CurrencyValueInfo
    {
        public string Id { get; set; }
        public string CurrencyId { get; set; }
        public string ValueInId { get; set; }
        public decimal Value { get; set; }
        public bool Active { get; set; }
    }

    public class CurrencyValueDataResultModel : DataSourceResultModel<CurrencyValueInfo>
    {

    }

    public class CurrencyValueDataRequestModel : DataSourceRequestModel
    {

    }
}

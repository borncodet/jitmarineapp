﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CountryPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string CountryCode { get; set; }
        public bool IsActive { get; set; }
    }

    public class CountryEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string CountryCode { get; set; }
        public bool IsActive { get; set; }
    }

    public class CountryInfo
    {
        public int RowId { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public int CurrencyId { get; set; }
        public string CountryCode { get; set; }
        public bool IsActive { get; set; }
    }

    public class CountryDataResultModel : DataSourceResultModel<CountryInfo>
    {

    }

    public class CountryDataRequestModel : DataSourceRequestModel
    {

    }
}

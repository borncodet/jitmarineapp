﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareerApp.ViewModels
{
	public class ValidationResultViewModel
	{
		public bool IsValid { get; set; }
		public string ValidationMessage { get; set; }
	}
}

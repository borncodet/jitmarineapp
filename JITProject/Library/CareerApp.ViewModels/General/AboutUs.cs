﻿using CareerApp.Shared.Resources;
using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareerApp.ViewModels
{
	public class AboutUsViewModel
	{
		public string Title { get; set; } = Resource.ProductName;
		public string Description { get; set; } = Resource.ProductDescription;
		public string EMail { get; set; } = Resource.Email;
		public string Phone1 { get; set; } = Resource.Phone1;
		public string Phone2 { get; set; } = Resource.Phone2;
		public string Website { get; set; } = Resource.Website;
		public string ImageUrl { get; set; } = Resource.AboutUsImageUrl;
	}

	public class AboutUsResultViewModel : BaseResponse
	{
		public AboutUsViewModel AboutUs { get; set; }
	}
}

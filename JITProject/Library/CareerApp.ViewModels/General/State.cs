﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class StatePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public int CountryId { get; set; }
        public bool IsActive { get; set; }
    }

    public class StateEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public int CountryId { get; set; }
        public bool IsActive { get; set; }
    }

    public class StateInfo
    {
        public int RowId { get; set; }
        public int StateId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public bool IsActive { get; set; }
    }

    public class StateDataResultModel : DataSourceResultModel<StateInfo>
    {

    }

    public class StateDataRequestModel : DataSourceRequestModel
    {

    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class CurrencyPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string CurrencySymbol { get; set; }
        [Required]
        public int DecimalPoints { get; set; }
        [Required]
        [StringLength(10)]
        public string MainSuffix { get; set; }
        [Required]
        [StringLength(10)]
        public string SubSuffix { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string CurrencyCode { get; set; }
        [Required]
        public decimal Rate { get; set; }
        public bool Active { get; set; }
    }

    public class CurrencyEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int CurrencyId { get; set; }
        [Required]
        public string CurrencySymbol { get; set; }
        [Required]
        public int DecimalPoints { get; set; }
        [Required]
        [StringLength(10)]
        public string MainSuffix { get; set; }
        [Required]
        [StringLength(10)]
        public string SubSuffix { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string CurrencyCode { get; set; }
        [Required]
        public decimal Rate { get; set; }
        public bool Active { get; set; }
    }

    public class CurrencyInfo
    {
        public int RowId { get; set; }
        public int CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
        public int DecimalPoints { get; set; }
        public string MainSuffix { get; set; }
        public string SubSuffix { get; set; }
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Rate { get; set; }
        public bool Active { get; set; }
    }

    public class CurrencyDataResultModel : DataSourceResultModel<CurrencyInfo>
    {

    }

    public class CurrencyDataRequestModel : DataSourceRequestModel
    {

    }
}

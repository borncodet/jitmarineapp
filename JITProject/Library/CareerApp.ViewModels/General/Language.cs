﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class LanguagePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int LanguageId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LanguageCode { get; set; }
        public bool Active { get; set; }
    }

    public class LanguageEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int LanguageId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string LanguageCode { get; set; }
        [Required]
        public decimal Rate { get; set; }
        public bool Active { get; set; }
    }

    public class LanguageInfo
    {
        public int RowId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        public decimal Rate { get; set; }
        public bool Active { get; set; }
    }

    public class LanguageDataResultModel : DataSourceResultModel<LanguageInfo>
    {

    }

    public class LanguageDataRequestModel : DataSourceRequestModel
    {

    }
}

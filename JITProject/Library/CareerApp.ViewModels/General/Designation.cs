﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DesignationSelectBoxDataViewModel
    {

    }

    public class DesignationPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DesignationId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class DesignationInfo
    {
        public int RowId { get; set; }
        public int DesignationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class DesignationDataResultModel : DataSourceResultModel<DesignationInfo>
    {

    }

    public class DesignationDataRequestModel : DataSourceRequestModel
    {

    }

    public class DesignationEditResponse : BaseResponse
    {
        public DesignationEditModel Data { get; set; }

        public DesignationEditResponse()
        {
            Data = new DesignationEditModel();
        }
    }

    public class DesignationEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DesignationId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public DesignationEditModel()
        {
            RowId = 0;
            DesignationId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DurationSelectBoxDataViewModel
    {

    }

    public class DurationPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DurationId { get; set; }
        [Required]
        public int Day { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Year { get; set; }
        public bool IsActive { get; set; }
    }

    public class DurationInfo
    {
        public int RowId { get; set; }
        public int DurationId { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public bool IsActive { get; set; }
    }

    public class DurationDataResultModel : DataSourceResultModel<DurationInfo>
    {

    }

    public class DurationDataRequestModel : DataSourceRequestModel
    {

    }

    public class DurationEditResponse : BaseResponse
    {
        public DurationEditModel Data { get; set; }

        public DurationEditResponse()
        {
            Data = new DurationEditModel();
        }
    }

    public class DurationEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DurationId { get; set; }
        [Required]
        public int Day { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Year { get; set; }
        public bool IsActive { get; set; }

        public DurationEditModel()
        {
            RowId = 0;
            DurationId = 0;
            Day = 0;
            Month = 0;
            Year = 0;
            IsActive = true;
        }
    }
}

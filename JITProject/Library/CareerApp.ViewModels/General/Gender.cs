﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class GenderSelectBoxDataViewModel
    {

    }

    public class GenderPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int GenderId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class GenderInfo
    {
        public int RowId { get; set; }
        public int GenderId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class GenderDataResultModel : DataSourceResultModel<GenderInfo>
    {

    }

    public class GenderDataRequestModel : DataSourceRequestModel
    {

    }

    public class GenderEditResponse : BaseResponse
    {
        public GenderEditModel Data { get; set; }

        public GenderEditResponse()
        {
            Data = new GenderEditModel();
        }
    }

    public class GenderEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int GenderId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public GenderEditModel()
        {
            RowId = 0;
            GenderId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class MartialStatusSelectBoxDataViewModel
    {

    }

    public class MartialStatusPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int MartialStatusId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class MartialStatusInfo
    {
        public int RowId { get; set; }
        public int MartialStatusId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class MartialStatusDataResultModel : DataSourceResultModel<MartialStatusInfo>
    {

    }

    public class MartialStatusDataRequestModel : DataSourceRequestModel
    {

    }

    public class MartialStatusEditResponse : BaseResponse
    {
        public MartialStatusEditModel Data { get; set; }

        public MartialStatusEditResponse()
        {
            Data = new MartialStatusEditModel();
        }
    }

    public class MartialStatusEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int MartialStatusId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public MartialStatusEditModel()
        {
            RowId = 0;
            MartialStatusId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DigiDocumentTypeSelectBoxDataViewModel
    {

    }

    public class DigiDocumentTypePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentTypeInfo
    {
        public int RowId { get; set; }
        public int DigiDocumentTypeId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentTypeDataResultModel : DataSourceResultModel<DigiDocumentTypeInfo>
    {

    }

    public class DigiDocumentTypeDataRequestModel : DataSourceRequestModel
    {

    }

    public class DigiDocumentTypeEditResponse : BaseResponse
    {
        public DigiDocumentTypeEditModel Data { get; set; }

        public DigiDocumentTypeEditResponse()
        {
            Data = new DigiDocumentTypeEditModel();
        }
    }

    public class DigiDocumentTypeEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public DigiDocumentTypeEditModel()
        {
            RowId = 0;
            DigiDocumentTypeId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

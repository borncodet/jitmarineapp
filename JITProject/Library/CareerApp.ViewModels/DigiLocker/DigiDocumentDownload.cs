﻿using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CareerApp.ViewModels.DigiLocker
{
    public class DigiDocumentDownloadModel
    {
        [Required]
        public int DigiDocumentDetailId { get; set; }
    }

    public class DigiDocumentDownloadResponse : BaseResponse
    {
        public string DownloadUrl { get; set; }
    }
}

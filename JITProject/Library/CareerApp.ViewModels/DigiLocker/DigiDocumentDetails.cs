﻿using CareerApp.Core.Constants;
using CareerApp.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace CareerApp.ViewModels
{
    public class DigiDocumentDetailsSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> DigiDocumentTypes { get; set; }
    }

    public class DigiDocumentDetailsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string DocumentNumber { get; set; }
        public string Description { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int DigiDocumentTypeId { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool ExpiryFlag { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentDetailsMultiPostmodel
    {
        [Required]
        public int[] RowId { get; set; }
        [Required]
        public int DigiDocumentTypeId { get; set; }
    }

    public class DigiDocumentDetailsInfo
    {

        public int RowId { get; set; }
        public int DigiDocumentDetailId { get; set; }
        public string Name { get; set; }
        public string DocumentNumber { get; set; }
        public string Description { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public int DigiDocumentTypeId { get; set; }
        public string DigiDocumentTypeTitle { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool ExpiryFlag { get; set; }
        public bool SelectedFlag { get; set; }
        public bool IsExpired { get; set; }
        public bool IsActive { get; set; }
        public string DigiDocument { get; set; }

        public string DocumentType
        {
            get
            {
                if (!string.IsNullOrEmpty(DigiDocument))
                {
                    return ImageHelper.GetMimeType(Path.GetExtension(DigiDocument).ToLowerInvariant());
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public string DigDcoumentFullPath
        {
            get
            {

                if (DigiDocument != null && ImageHelper.ImageExtensions.Contains(Path.GetExtension(DigiDocument).ToUpperInvariant()))
                {
                    return @"/Upload/" + DigiDocumentTypeTitle + @"/" + DigiDocument;
                }
                else
                {
                    return "";
                }

            }
        }

    }

    public class DigiDocumentCountInfo
    {
        public int DocumentCount { get; set; }
        public string DocumentTypeTitle { get; set; }
    }

    public class DigiDocumentCountDataResultModel : DataSourceResultModel<DigiDocumentCountInfo>
    {

    }

    public class DigiDocumentDetailsDataResultModel : DataSourceResultModel<DigiDocumentDetailsInfo>
    {

    }

    public class DigiDocumentDetailsDataRequestModel : DataSourceRequestModel
    {

    }

    public class DigiDocumentDetailsEditResponse : BaseResponse
    {
        public DigiDocumentDetailsEditModel Data { get; set; }

        public DigiDocumentDetailsEditResponse()
        {
            Data = new DigiDocumentDetailsEditModel();
        }
    }

    public class DigiDocumentDetailsEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string DocumentNumber { get; set; }
        public string Description { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public int DigiDocumentTypeId { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool ExpiryFlag { get; set; }
        public bool IsActive { get; set; }
        public string DigiDocumentTypeTitle { get; set; }


        public string DigiDocument { get; set; }

        public string DocumentType
        {
            get
            {
                if (!string.IsNullOrEmpty(DigiDocument))
                {
                    return ImageHelper.GetMimeType(Path.GetExtension(DigiDocument).ToLowerInvariant());
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public string DigDcoumentFullPath
        {
            get
            {

                if (DigiDocument != null && ImageHelper.ImageExtensions.Contains(Path.GetExtension(DigiDocument).ToUpperInvariant()))
                {
                    return @"/Upload/" + DigiDocumentTypeTitle + @"/" + DigiDocument;
                }
                else
                {
                    return "";
                }

            }
        }
        public DigiDocumentDetailsEditModel()
        {
            RowId = 0;
            DigiDocumentDetailId = 0;
            Name = "";
            DocumentNumber = "";
            Description = "";
            CandidateId = 0;
            DigiDocumentTypeId = 0;
            ExpiryDate = null;
            UpdatedDate = DateTime.Now;
            ExpiryFlag = true;
            IsActive = true;
        }
    }
}

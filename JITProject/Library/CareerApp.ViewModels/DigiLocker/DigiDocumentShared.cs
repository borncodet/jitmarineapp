﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DigiDocumentSharedSelectBoxDataViewModel
    {
    }

    public class DigiDocumentSharedPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentSharedId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public string SharedThrough { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentSharedInfo
    {
        public int RowId { get; set; }
        public int DigiDocumentSharedId { get; set; }
        public int DigiDocumentDetailId { get; set; }
        public string SharedThrough { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentSharedDataResultModel : DataSourceResultModel<DigiDocumentSharedInfo>
    {

    }

    public class DigiDocumentSharedDataRequestModel : DataSourceRequestModel
    {

    }

    public class DigiDocumentSharedEditResponse : BaseResponse
    {
        public DigiDocumentSharedEditModel Data { get; set; }

        public DigiDocumentSharedEditResponse()
        {
            Data = new DigiDocumentSharedEditModel();
        }
    }

    public class DigiDocumentSharedEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentSharedId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        public string SharedThrough { get; set; }
        public bool IsActive { get; set; }

        public DigiDocumentSharedEditModel()
        {
            RowId = 0;
            DigiDocumentSharedId = 0;
            DigiDocumentDetailId = 0;
            SharedThrough = "";
            IsActive = true;
        }
    }
}

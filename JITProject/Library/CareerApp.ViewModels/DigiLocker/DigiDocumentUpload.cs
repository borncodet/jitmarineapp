﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class DigiDocumentUploadSelectBoxDataViewModel
    {

    }

    public class DigiDocumentUploadPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentUploadId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        [Required]
        public string DigiDocument { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentUploadInfo
    {
        public int RowId { get; set; }
        public int DigiDocumentUploadId { get; set; }
        public int DigiDocumentDetailId { get; set; }
        public string DigiDocument { get; set; }
        public bool IsActive { get; set; }
    }

    public class DigiDocumentUploadDataResultModel : DataSourceResultModel<DigiDocumentUploadInfo>
    {

    }

    public class DigiDocumentUploadDataRequestModel : DataSourceRequestModel
    {

    }

    public class DigiDocumentUploadEditResponse : BaseResponse
    {
        public DigiDocumentUploadEditModel Data { get; set; }

        public DigiDocumentUploadEditResponse()
        {
            Data = new DigiDocumentUploadEditModel();
        }
    }

    public class DigiDocumentUploadEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int DigiDocumentUploadId { get; set; }
        [Required]
        public int DigiDocumentDetailId { get; set; }
        [Required]
        public string DigiDocument { get; set; }
        public bool IsActive { get; set; }

        public DigiDocumentUploadEditModel()
        {
            RowId = 0;
            DigiDocumentUploadId = 0;
            DigiDocumentDetailId = 0;
            DigiDocument = "";
            IsActive = true;
        }
    }
}

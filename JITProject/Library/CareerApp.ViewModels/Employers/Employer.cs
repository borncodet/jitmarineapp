﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class EmployerSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
        public IEnumerable<ValueCaptionPair> States { get; set; }
    }

    public class EmployerPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int EmployerId { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }
        [Required]
        [StringLength(50)]
        public string RegisterNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required]
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }

    public class EmployerInfo
    {
        public int RowId { get; set; }
        public int EmployerId { get; set; }
        public string CompanyName { get; set; }
        public string RegisterNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }

    public class EmployerDataResultModel : DataSourceResultModel<EmployerInfo>
    {

    }

    public class EmployerDataRequestModel : DataSourceRequestModel
    {

    }

    public class EmployerEditResponse : BaseResponse
    {
        public EmployerEditModel Data { get; set; }

        public EmployerEditResponse()
        {
            Data = new EmployerEditModel();
        }
    }

    public class EmployerEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int EmployerId { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }
        [Required]
        [StringLength(50)]
        public string RegisterNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [StringLength(50)]
        public string City { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required]
        public int UserId { get; set; }
        public bool IsActive { get; set; }

        public EmployerEditModel()
        {
            RowId = 0;
            EmployerId = 0;
            CompanyName = "";
            RegisterNumber = "";
            Email = "";
            PhoneNumber = "";
            Address1 = "";
            Address2 = "";
            City = "";
            ZipCode = "";
            UserId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class FieldOfExpertiseSelectBoxDataViewModel
    {

    }

    public class FieldOfExpertisePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int FieldOfExpertiseId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class FieldOfExpertiseInfo
    {
        public int RowId { get; set; }
        public int FieldOfExpertiseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class FieldOfExpertiseDataResultModel : DataSourceResultModel<FieldOfExpertiseInfo>
    {

    }

    public class FieldOfExpertiseDataRequestModel : DataSourceRequestModel
    {

    }

    public class FieldOfExpertiseEditResponse : BaseResponse
    {
        public FieldOfExpertiseEditModel Data { get; set; }

        public FieldOfExpertiseEditResponse()
        {
            Data = new FieldOfExpertiseEditModel();
        }
    }

    public class FieldOfExpertiseEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int FieldOfExpertiseId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public FieldOfExpertiseEditModel()
        {
            RowId = 0;
            FieldOfExpertiseId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ResumeExperienceMapSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> ResumeTemplateId { get; set; }
    }

    public class ResumeExperienceMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeExperienceMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int ExpereinceTypeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeExperienceMapInfo
    {
        public int RowId { get; set; }
        public int ResumeExperienceMapId { get; set; }
        public int ResumeTemplateId { get; set; }
        public int ExpereinceTypeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeExperienceMapDataResultModel : DataSourceResultModel<ResumeExperienceMapInfo>
    {

    }

    public class ResumeExperienceMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class ResumeExperienceMapEditResponse : BaseResponse
    {
        public ResumeExperienceMapEditModel Data { get; set; }

        public ResumeExperienceMapEditResponse()
        {
            Data = new ResumeExperienceMapEditModel();
        }
    }

    public class ResumeExperienceMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeExperienceMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int ExpereinceTypeId { get; set; }
        public bool IsActive { get; set; }

        public ResumeExperienceMapEditModel()
        {
            RowId = 0;
            ResumeExperienceMapId = 0;
            ResumeTemplateId = 0;
            ExpereinceTypeId = 0;
            IsActive = true;
        }
    }
}

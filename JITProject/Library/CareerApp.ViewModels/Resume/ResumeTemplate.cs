﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ResumeTemplateSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> FieldOfExeprtises { get; set; }
        public IEnumerable<ValueCaptionPair> ExperienceTypes { get; set; }
        public IEnumerable<ValueCaptionPair> Designations { get; set; }
    }

    public class ResumeTemplatePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public string ResumeContent { get; set; }
        [Required]
        public string ResumeImage { get; set; }
        public IFormFile Document { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeTemplateNewPostmodel
    {
        [Required]
        public ResumeTemplatePostmodel ResumeTemplatePostmodel { get; set; }
        public List<ResumeDesignationMapPostmodel> ResumeDesignationMapPostmodel { get; set; }
        public List<ResumeExperienceMapPostmodel> ResumeExperienceMapPostmodel { get; set; }
        public List<ResumeExpertiseMapPostmodel> ResumeExpertiseMapPostmodel { get; set; }

    }

    public class ResumeTemplateInfo
    {
        public int RowId { get; set; }
        public int ResumeTemplateId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResumeContent { get; set; }
        public string ResumeImage { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeTemplateDataResultModel : DataSourceResultModel<ResumeTemplateInfo>
    {

    }

    public class ResumeTemplateDataRequestModel : DataSourceRequestModel
    {

    }

    public class ResumeTemplateEditResponse : BaseResponse
    {
        public ResumeTemplateEditModel Data { get; set; }

        public ResumeTemplateEditResponse()
        {
            Data = new ResumeTemplateEditModel();
        }
    }

    public class ResumeTemplateEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public string ResumeContent { get; set; }
        [Required]
        public string ResumeImage { get; set; }
        public bool IsActive { get; set; }

        public ResumeTemplateEditModel()
        {
            RowId = 0;
            ResumeTemplateId = 0;
            Title = "";
            Description = "";
            ResumeContent = "";
            ResumeImage = "";
            IsActive = true;
        }
    }
}

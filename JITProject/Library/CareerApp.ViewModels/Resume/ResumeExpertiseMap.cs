﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ResumeExpertiseMapSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> ResumeTemplateId { get; set; }
    }

    public class ResumeExpertiseMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeExpertiseMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int FieldOfExpertiseId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeExpertiseMapInfo
    {
        public int RowId { get; set; }
        public int ResumeExpertiseMapId { get; set; }
        public int ResumeTemplateId { get; set; }
        public int FieldOfExpertiseId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeExpertiseMapDataResultModel : DataSourceResultModel<ResumeExpertiseMapInfo>
    {

    }

    public class ResumeExpertiseMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class ResumeExpertiseMapEditResponse : BaseResponse
    {
        public ResumeExpertiseMapEditModel Data { get; set; }

        public ResumeExpertiseMapEditResponse()
        {
            Data = new ResumeExpertiseMapEditModel();
        }
    }

    public class ResumeExpertiseMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeExpertiseMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int FieldOfExpertiseId { get; set; }
        public bool IsActive { get; set; }

        public ResumeExpertiseMapEditModel()
        {
            RowId = 0;
            ResumeExpertiseMapId = 0;
            ResumeTemplateId = 0;
            FieldOfExpertiseId = 0;
            IsActive = true;
        }
    }
}

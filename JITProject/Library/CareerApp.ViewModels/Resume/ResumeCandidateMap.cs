﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ResumeCandidateMapSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> ResumeTemplateId { get; set; }
    }

    public class ResumeCandidateMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeCandidateMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public string Resume { get; set; }
        [Required]
        public string ResumeName { get; set; }
        public string ResumeFile { get; set; }
        public IFormFile ResumeFileDocument { get; set; }
        public string ResumeImage { get; set; }
        public IFormFile ResumeImageDocument { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeCandidateMapInfo
    {
        public int RowId { get; set; }
        public int ResumeCandidateMapId { get; set; }
        public int ResumeTemplateId { get; set; }
        public int CandidateId { get; set; }
        public string Resume { get; set; }
        public string ResumeName { get; set; }
        public string ResumeFile { get; set; }
        public string ResumeFileFullPath
        {
            get
            {

                if (ResumeFile != null)
                {
                    return @"/Upload/ResumeFile/" + ResumeFile;
                }
                else
                {
                    return "";
                }

            }
        }
        public string ResumeImage { get; set; }
        public string ResumeImageFullPath
        {
            get
            {

                if (ResumeFile != null)
                {
                    return @"/Upload/ResumeImage/" + ResumeImage;
                }
                else
                {
                    return "";
                }

            }
        }
        public bool IsActive { get; set; }
    }

    public class ResumeContent
    {
        public List<CandidateInfo> CandidateInfo { get; set; }
        public List<CandidateRelativesInfo> CandidateRelativesInfo { get; set; }
        public List<CandidateSkillsInfo> CandidateSkillsInfo { get; set; }
        public List<CandidateReferencesInfo> CandidateReferencesInfo { get; set; }
        public List<CandidateProjectsInfo> CandidateProjectsInfo { get; set; }
        public List<CandidateProfileImageInfo> CandidateProfileImageInfo { get; set; }
        public List<CandidateOtherCertificateInfo> CandidateOtherCertificateInfo { get; set; }
        public List<CandidateLanguageMapInfo> CandidateLanguageMapInfo { get; set; }
        public List<CandidateExperienceInfo> CandidateExperienceInfo { get; set; }
        public List<CandidateEducationCertificateInfo> CandidateEducationCertificateInfo { get; set; }
        public List<CandidateAchievementsInfo> CandidateAchievementsInfo { get; set; }
        public List<EducationQualificationInfo> EducationQualificationInfo { get; set; }
        public List<SocialAccountsInfo> SocialAccountsInfo { get; set; }
        public List<TrainingInfo> TrainingInfo { get; set; }
        public List<BankDetailsInfo> BankDetailsInfo { get; set; }
    }

    public class ResumeCandidateMapDataResultModel : DataSourceResultModel<ResumeCandidateMapInfo>
    {

    }

    public class ResumeCandidateMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class ResumeCandidateMapTemplateDataResultModel : DataSourceResultModel<ResumeCandidateMapTemplateInfo>
    {

    }

    public class ResumeCandidateMapEditResponse : BaseResponse
    {
        public ResumeCandidateMapEditModel Data { get; set; }

        public ResumeCandidateMapEditResponse()
        {
            Data = new ResumeCandidateMapEditModel();
        }
    }

    public class ResumeCandidateMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeCandidateMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        public string Resume { get; set; }
        [Required]
        public string ResumeName { get; set; }
        public string ResumeFile { get; set; }
        public string ResumeImage { get; set; }
        public bool IsActive { get; set; }

        public ResumeCandidateMapEditModel()
        {
            RowId = 0;
            ResumeCandidateMapId = 0;
            ResumeTemplateId = 0;
            CandidateId = 0;
            Resume = "";
            ResumeName = "";
            ResumeFile = "";
            ResumeImage = "";
            IsActive = true;
        }
    }

    public class ResumeCandidateMapTemplateInfo
    {
        public int RowId { get; set; }
        public int ResumeCandidateMapId { get; set; }
        public int ResumeTemplateId { get; set; }
        public int CandidateId { get; set; }
        public string Resume { get; set; }
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResumeContent { get; set; }
        public string ResumeImage { get; set; }
        public string ResumeName { get; set; }
        public string ResumeFile { get; set; }
        public string ResumeFileFullPath
        {
            get
            {

                if (ResumeFile != null)
                {
                    return @"/Upload/ResumeFile/" + ResumeFile;
                }
                else
                {
                    return "";
                }

            }
        }
        public string CandidateResumeImage { get; set; }
        public string CandidateResumeImageFullPath
        {
            get
            {

                if (ResumeFile != null)
                {
                    return @"/Upload/ResumeImage/" + CandidateResumeImage;
                }
                else
                {
                    return "";
                }

            }
        }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

}

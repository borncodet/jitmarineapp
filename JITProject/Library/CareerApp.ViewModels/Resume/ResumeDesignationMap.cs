﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class ResumeDesignationMapSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> ResumeTemplateId { get; set; }
    }

    public class ResumeDesignationMapPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeDesignationMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int DesignationId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeDesignationMapInfo
    {
        public int RowId { get; set; }
        public int ResumeDesignationMapId { get; set; }
        public int ResumeTemplateId { get; set; }
        public int DesignationId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ResumeDesignationMapDataResultModel : DataSourceResultModel<ResumeDesignationMapInfo>
    {

    }

    public class ResumeDesignationMapDataRequestModel : DataSourceRequestModel
    {

    }

    public class ResumeDesignationMapEditResponse : BaseResponse
    {
        public ResumeDesignationMapEditModel Data { get; set; }

        public ResumeDesignationMapEditResponse()
        {
            Data = new ResumeDesignationMapEditModel();
        }
    }

    public class ResumeDesignationMapEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int ResumeDesignationMapId { get; set; }
        [Required]
        public int ResumeTemplateId { get; set; }
        [Required]
        public int DesignationId { get; set; }
        public bool IsActive { get; set; }

        public ResumeDesignationMapEditModel()
        {
            RowId = 0;
            ResumeDesignationMapId = 0;
            ResumeTemplateId = 0;
            DesignationId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class BankDetailsSelectBoxDataViewModel
    {
        public IEnumerable<ValueCaptionPair> BankAccountTypes { get; set; }
        public IEnumerable<ValueCaptionPair> States { get; set; }
        public IEnumerable<ValueCaptionPair> Countries { get; set; }
    }

    public class BankDetailsPostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int BankDetailsId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        [StringLength(50)]
        public string BankName { get; set; }
        [Required]
        [StringLength(50)]
        public string BranchName { get; set; }
        [StringLength(20)]
        public string AccountNo { get; set; }
        [StringLength(20)]
        public string IFSCIBANSWIFTNo { get; set; }
        [Required]
        public int BankAccountTypeId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public int CountryId { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class BankDetailsInfo
    {
        public int RowId { get; set; }
        public int BankDetailsId { get; set; }
        public int CandidateId { get; set; }
        public string CandidateName { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string AccountNo { get; set; }
        public string IFSCIBANSWIFTNo { get; set; }
        public int BankAccountTypeId { get; set; }
        public string BankAccountTypeTitle { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }
    }

    public class BankDetailsDataResultModel : DataSourceResultModel<BankDetailsInfo>
    {

    }

    public class BankDetailsDataRequestModel : DataSourceRequestModel
    {

    }

    public class BankDetailsEditResponse : BaseResponse
    {
        public BankDetailsEditModel Data { get; set; }

        public BankDetailsEditResponse()
        {
            Data = new BankDetailsEditModel();
        }
    }

    public class BankDetailsEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int BankDetailsId { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        [StringLength(50)]
        public string BankName { get; set; }
        [Required]
        [StringLength(50)]
        public string BranchName { get; set; }
        [StringLength(20)]
        public string AccountNo { get; set; }
        [StringLength(20)]
        public string IFSCIBANSWIFTNo { get; set; }
        [Required]
        public int BankAccountTypeId { get; set; }
        [Required]
        public int StateId { get; set; }
        [Required]
        public int CountryId { get; set; }
        public int? DigiDocumentDetailId { get; set; }
        public bool IsActive { get; set; }

        public BankDetailsEditModel()
        {
            RowId = 0;
            BankDetailsId = 0;
            CandidateId = 0;
            BankName = "";
            BranchName = "";
            AccountNo = "";
            IFSCIBANSWIFTNo = "";
            BankAccountTypeId = 0;
            StateId = 0;
            CountryId = 0;
            DigiDocumentDetailId = 0;
            IsActive = true;
        }
    }
}

﻿using CareerApp.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace CareerApp.ViewModels
{
    public class BankAccountTypeSelectBoxDataViewModel
    {

    }

    public class BankAccountTypePostmodel
    {
        public int RowId { get; set; }
        [Required]
        public int BankAccountTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class BankAccountTypeInfo
    {
        public int RowId { get; set; }
        public int BankAccountTypeId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class BankAccountTypeDataResultModel : DataSourceResultModel<BankAccountTypeInfo>
    {

    }

    public class BankAccountTypeDataRequestModel : DataSourceRequestModel
    {

    }

    public class BankAccountTypeEditResponse : BaseResponse
    {
        public BankAccountTypeEditModel Data { get; set; }

        public BankAccountTypeEditResponse()
        {
            Data = new BankAccountTypeEditModel();
        }
    }

    public class BankAccountTypeEditModel
    {
        public int RowId { get; set; }
        [Required]
        public int BankAccountTypeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public BankAccountTypeEditModel()
        {
            RowId = 0;
            BankAccountTypeId = 0;
            Title = "";
            Description = "";
            IsActive = true;
        }
    }
}

﻿using CareerApp.Shared.Enums;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CareerApp.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            string errorRef = "Error :" + exception.Message;//ErrorConstants.GetErrorRefernce();
            errorRef += " Inner Exc:" + exception.InnerException;
            if (errorRef.Contains("AK_CA_Users_CountryCode_PhoneNumber"))
            {
                errorRef = "Error occured while register user. Phone number is already taken..";
            }
            var code = HttpStatusCode.InternalServerError; // default set
            var result = JsonConvert.SerializeObject(new CommonEntityResponse
            {
                IsSuccess = false,
                Type = ResponseType.Error,
                //Message = string.Format(ErrorConstants.SometingWentWrongWithErrorRef, errorRef),
                Message = string.Format(errorRef),
                Code = ResponseCode.InvalidData
            });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ErrorHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}

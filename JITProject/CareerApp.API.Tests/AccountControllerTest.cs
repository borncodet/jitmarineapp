﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.API.Helpers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Shared.Enums;
using CareerApp.ViewModels.Accounts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace CareerApp.API.Tests
{
    public class AccountControllerTest
    {
        private readonly AccountController _accountController;
        private readonly BaseController _baseController;
        private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IHostingEnvironment> _hostingEnvironmentMock = new Mock<IHostingEnvironment>();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IAccountManager> _accountManagerMock = new Mock<IAccountManager>();
        private readonly Mock<ILogger<AccountController>> _loggerAccountMock = new Mock<ILogger<AccountController>>();
        private readonly Mock<ILogger<BaseController>> _loggerBaseMock = new Mock<ILogger<BaseController>>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private readonly Mock<IOptions<TwilioVerifySettings>> _twilloVerifySettings = new Mock<IOptions<TwilioVerifySettings>>();
        private readonly Mock<IEmailSender> _emailSender = new Mock<IEmailSender>();
        private readonly Mock<IConfiguration> _configuration = new Mock<IConfiguration>();
        public AccountControllerTest()
        {
            var identity = new GenericIdentity("dominik.ernst@xyz123.de");
            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "2"));
            var principal = new GenericPrincipal(identity, new[] { "user" });
            _httpContextAccessorMock.Setup(s => s.HttpContext.User).Returns(principal);

            _accountController = new AccountController(_httpContextAccessorMock.Object, _hostingEnvironmentMock.Object,
                                                       _unitOfWorkMock.Object, _accountManagerMock.Object,
                                                       _loggerAccountMock.Object, _mapperMock.Object,
                                                       _twilloVerifySettings.Object, _emailSender.Object,
                                                       _configuration.Object);
            _baseController = new BaseController(_httpContextAccessorMock.Object, _hostingEnvironmentMock.Object,
                                                 _unitOfWorkMock.Object, _accountManagerMock.Object,
                                                 _loggerBaseMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetUserBasicDetailsAsync_ShouldReturnCustomerDetails_WhenCustomerExists()
        {
            // Arrange

            var userId = _accountController.GetCurrentUserId();
            var roleId = 1;
            var employerFlag = true;
            var candidateFlag = true;
            var userName = "bct1";
            var email = "borncode@gmail.com";
            var phoneNumber = "95621613031";
            var createdDate = DateTime.UtcNow;
            var updatedDate = DateTime.UtcNow;
            var _isActive = true;

            ApplicationUser appUser = new ApplicationUser
            {
                Id = userId,
                RoleId = roleId,
                EmployerFlag = employerFlag,
                CandidateFlag = candidateFlag,
                UserName = userName,
                Email = email,
                PhoneNumber = phoneNumber,
                CreatedDate = createdDate,
                UpdatedDate = updatedDate,
                IsActive = _isActive
            };

            _accountManagerMock.Setup(x => x.GetUserByIdAsync(userId))
                .ReturnsAsync(appUser);

            // Act
            var applicationUser = await _accountController.GetUserBasicDetailsAsync();

            //Assert
            Assert.Equal(userName, applicationUser.UserName);
        }

        [Fact]
        public async Task GetUserBasicDetailsAsync_ShouldReturnFailureMessage_WhenCustomerDoesNotExist()
        {
            // Arrange
            //int userId = new Random().Next();
            int userId = 2;
            UserInfoViewModel userInfoViewModel = new UserInfoViewModel
            {
                UserName = null,
                Email = null,
                CountryCode = null,
                PhoneNumber = null,
                FullName = null,
                IsSuccess = false,
                Type = ResponseType.Error,
                Message = "User not found.",
                Code = ResponseCode.RequestWasFailure,
            };

            _accountManagerMock.Setup(x => x.GetUserByIdAsync(userId)).ReturnsAsync(() => null);

            // Act
            var applicationUser = await _accountController.GetUserBasicDetailsAsync();

            // Assert
            //_loggerBaseMock.Verify(x => x.LogInformation($"User not found.{userId}"), Times.Once);

            Assert.Equal(userInfoViewModel.Message, applicationUser.Message);
        }

    }
}

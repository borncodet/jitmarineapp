﻿using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CareerApp.API.Filters
{
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                CommonResponseViewModel viewModel = new CommonResponseViewModel();
                viewModel.CreateModelStateValidationResponse();
                context.Result = new BadRequestObjectResult(viewModel);
            }
        }
    }
}

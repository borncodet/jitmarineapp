﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAlertCategoryMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAlertCategoryMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAlertCategoryMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertCategoryMapDataResultModel), 200)]
        public async Task<JobAlertCategoryMapDataResultModel> GetAllActiveAsync([FromBody]JobAlertCategoryMapDataRequestModel model)
        {
            IPagedList<JobAlertCategoryMapInfo> data = await _unitOfWork.JobAlertCategoryMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobAlertCategoryMapDataResultModel, JobAlertCategoryMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAlertCategoryMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAlertCategoryMap JobAlertCategoryMap = await _unitOfWork.JobAlertCategoryMap.GetFirstOrDefaultAsync(x => x.JobAlertId.Equals(model.JobAlertId) && x.JobCategoryId.Equals(model.JobCategoryId) && x.IsActive == true);

            if (JobAlertCategoryMap != null && JobAlertCategoryMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Job category already mapped to the alert.");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobAlertCategoryMap = await _unitOfWork.JobAlertCategoryMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobAlertCategoryMap != null)
            {
                JobAlertCategoryMap = _mapper.Map(model, JobAlertCategoryMap);
                await _unitOfWork.JobAlertCategoryMap.UpdateAsync(JobAlertCategoryMap);
            }
            else
            {
                JobAlertCategoryMap = _mapper.Map<JobAlertCategoryMapPostmodel, JobAlertCategoryMap>(model);
                await _unitOfWork.JobAlertCategoryMap.AddAsync(JobAlertCategoryMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobAlertCategoryMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertCategoryMapSelectBoxDataViewModel), 200)]
        public async Task<JobAlertCategoryMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAlertCategoryMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertCategoryMapEditResponse), 200)]
        public async Task<JobAlertCategoryMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAlertCategoryMapEditResponse response = new JobAlertCategoryMapEditResponse();

            JobAlertCategoryMap JobAlertCategoryMap = await _unitOfWork.JobAlertCategoryMap.GetAsync(model.RowId);

            if (JobAlertCategoryMap != null)
            {
                response.Data = _mapper.Map<JobAlertCategoryMapEditModel>(JobAlertCategoryMap);
            }
            else
            {
                response.Data = new JobAlertCategoryMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAlertCategoryMap JobAlertCategoryMap = await _unitOfWork.JobAlertCategoryMap.GetAsync(model.RowId);
            if (JobAlertCategoryMap == null)
            {
                viewModel.CreateFailureResponse("Job category alert map not found.");
                return viewModel;
            }

            JobAlertCategoryMap.IsActive = false;
            await _unitOfWork.JobAlertCategoryMap.UpdateAsync(JobAlertCategoryMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

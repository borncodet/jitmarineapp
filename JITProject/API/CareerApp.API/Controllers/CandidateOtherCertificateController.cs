﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateOtherCertificateController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateOtherCertificateController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateOtherCertificateController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateOtherCertificateDataResultModel), 200)]
        public async Task<CandidateOtherCertificateDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateOtherCertificateInfo> data = await _unitOfWork.CandidateOtherCertificates.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateOtherCertificateDataResultModel, CandidateOtherCertificateInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] CandidateOtherCertificatePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateOtherCertificate CandidateOtherCertificate = await _unitOfWork.CandidateOtherCertificates.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.TrainingId.Equals(model.TrainingId) && x.IsActive == true);

            if (CandidateOtherCertificate != null && CandidateOtherCertificate.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate other certification already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                model.Certificate = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\TrainingCertificate\", model.Document);
            }

            if (model.RowId != 0)
            {
                CandidateOtherCertificate = await _unitOfWork.CandidateOtherCertificates.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateOtherCertificate != null)
            {
                CandidateOtherCertificate = _mapper.Map(model, CandidateOtherCertificate);
                await _unitOfWork.CandidateOtherCertificates.UpdateAsync(CandidateOtherCertificate);
            }
            else
            {
                CandidateOtherCertificate = _mapper.Map<CandidateOtherCertificatePostmodel, CandidateOtherCertificate>(model);
                await _unitOfWork.CandidateOtherCertificates.AddAsync(CandidateOtherCertificate);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateOtherCertificate.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateOtherCertificateSelectBoxDataViewModel), 200)]
        public async Task<CandidateOtherCertificateSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateOtherCertificates.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateOtherCertificateEditResponse), 200)]
        public async Task<CandidateOtherCertificateEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateOtherCertificateEditResponse response = new CandidateOtherCertificateEditResponse();

            CandidateOtherCertificate CandidateOtherCertificate = await _unitOfWork.CandidateOtherCertificates.GetAsync(model.RowId);

            if (CandidateOtherCertificate != null)
            {
                response.Data = _mapper.Map<CandidateOtherCertificateEditModel>(CandidateOtherCertificate);
            }
            else
            {
                response.Data = new CandidateOtherCertificateEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateOtherCertificate CandidateOtherCertificate = await _unitOfWork.CandidateOtherCertificates.GetAsync(model.RowId);
            if (CandidateOtherCertificate == null)
            {
                viewModel.CreateFailureResponse("Candidate other certification not found.");
                return viewModel;
            }

            CandidateOtherCertificate.IsActive = false;
            await _unitOfWork.CandidateOtherCertificates.UpdateAsync(CandidateOtherCertificate);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

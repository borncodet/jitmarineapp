﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateReferencesController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateReferencesController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateReferencesController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateReferencesDataResultModel), 200)]
        public async Task<CandidateReferencesDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateReferencesInfo> data = await _unitOfWork.CandidateReferences.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateReferencesDataResultModel, CandidateReferencesInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateReferencesPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateReferences CandidateReferences = await _unitOfWork.CandidateReferences.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.FirstName.Equals(model.FirstName) && x.LastName.Equals(model.LastName) && x.PhoneNumber.Equals(model.PhoneNumber) && x.IsActive == true);

            if (CandidateReferences != null && CandidateReferences.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate reference already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                CandidateReferences = await _unitOfWork.CandidateReferences.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateReferences != null)
            {
                CandidateReferences = _mapper.Map(model, CandidateReferences);
                await _unitOfWork.CandidateReferences.UpdateAsync(CandidateReferences);
            }
            else
            {
                CandidateReferences = _mapper.Map<CandidateReferencesPostmodel, CandidateReferences>(model);
                await _unitOfWork.CandidateReferences.AddAsync(CandidateReferences);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateReferences.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateReferencesSelectBoxDataViewModel), 200)]
        public async Task<CandidateReferencesSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateReferences.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateReferencesEditResponse), 200)]
        public async Task<CandidateReferencesEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateReferencesEditResponse response = new CandidateReferencesEditResponse();

            CandidateReferences CandidateReferences = await _unitOfWork.CandidateReferences.GetAsync(model.RowId);

            if (CandidateReferences != null)
            {
                response.Data = _mapper.Map<CandidateReferencesEditModel>(CandidateReferences);
            }
            else
            {
                response.Data = new CandidateReferencesEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateReferences CandidateReferences = await _unitOfWork.CandidateReferences.GetAsync(model.RowId);
            if (CandidateReferences == null)
            {
                viewModel.CreateFailureResponse("Candidate reference not found.");
                return viewModel;
            }

            CandidateReferences.IsActive = false;
            await _unitOfWork.CandidateReferences.UpdateAsync(CandidateReferences);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class FunctionalAreaController : BaseController
    {
        private readonly IMapper _mapper;

        public FunctionalAreaController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<FunctionalAreaController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(FunctionalAreaDataResultModel), 200)]
        public async Task<FunctionalAreaDataResultModel> GetAllActiveAsync([FromBody]FunctionalAreaDataRequestModel model)
        {
            IPagedList<FunctionalArea> data = await _unitOfWork.FunctionalAreas.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<FunctionalAreaDataResultModel, FunctionalAreaInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(FunctionalAreaDataResultModel), 200)]
        public async Task<FunctionalAreaDataResultModel> GetAllInactiveAsync([FromBody]FunctionalAreaDataRequestModel model)
        {
            IPagedList<FunctionalArea> data = await _unitOfWork.FunctionalAreas.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<FunctionalAreaDataResultModel, FunctionalAreaInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(FunctionalAreaEditModel), 200)]
        public async Task<FunctionalAreaEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            FunctionalArea item = await _unitOfWork.FunctionalAreas.GetAsync(model.RowId);
            FunctionalAreaEditModel viewModel = _mapper.Map<FunctionalAreaEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] FunctionalAreaPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            FunctionalArea item = await _unitOfWork.FunctionalAreas.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Functional Area is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.FunctionalAreas.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.FunctionalAreas.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<FunctionalAreaPostmodel, FunctionalArea>(model);
                await _unitOfWork.FunctionalAreas.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            FunctionalArea item = await _unitOfWork.FunctionalAreas.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Functional Area not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.FunctionalAreas.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

﻿/// <summary>
/// APIs to manage vendor's profile image
/// </summary>
using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class VendorProfileImageController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ILogger<VendorProfileImageController> _logger;

        /// <summary>
        /// VendorProfileImage Constructor
        /// Dependency Injection is done here - used parameter based dependency injection
        /// </summary>
        public VendorProfileImageController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<VendorProfileImageController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Get all active vendor profile image list, can be filterd by using vendorid as well
        /// Api path will be api/vendorprofileimage/gaaa
        /// </summary>
        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorProfileImageDataResultModel), 200)]
        public async Task<VendorProfileImageDataResultModel> GetAllActiveAsync([FromBody]DataSourceVendorRequestModel model)
        {
            _logger.LogInformation("Calling api/vendorprofileimage/gaaa api to get all active vendor list, can be filterd by using vendorid as well.");
            IPagedList<VendorProfileImageInfo> data = await _unitOfWork.VendorProfileImage.GetAllAsync(model);
            return PopulateResponseWithoutMap<VendorProfileImageDataResultModel, VendorProfileImageInfo>(data);
        }

        /// <summary>
        /// Create or update vendor profile image
        /// Api path will be api/vendor/coea
        /// </summary>
        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] VendorProfileImagePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            VendorProfileImage VendorProfileImage = await _unitOfWork.VendorProfileImage.GetFirstOrDefaultAsync(x => x.VendorId.Equals(model.VendorId) && x.IsActive.Equals(true));

            _logger.LogInformation("Check whether the vendor profile image already exist or not.");
            if (VendorProfileImage != null && VendorProfileImage.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Vendor profile image already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                _logger.LogInformation("Uploading vendor profile image to " + _env.WebRootPath + @"\Upload\VendorProfileImage\" + ".");
                model.ImageUrl = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\VendorProfileImage\", model.Document);
                _logger.LogInformation("Converting vendor profile image to Base64Image and assigning it to corresponding database column.");
                byte[] imageArray = System.IO.File.ReadAllBytes(_env.WebRootPath + @"\Upload\VendorProfileImage\" + model.ImageUrl);
                model.Base64Image = Convert.ToBase64String(imageArray);
            }

            if (model.RowId != 0)
            {
                _logger.LogInformation("Gets vendor profile image details by RowId.");
                VendorProfileImage = await _unitOfWork.VendorProfileImage.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (VendorProfileImage != null)
            {
                _logger.LogInformation("Update vendor profile image details.");
                VendorProfileImage = _mapper.Map(model, VendorProfileImage);
                await _unitOfWork.VendorProfileImage.UpdateAsync(VendorProfileImage);
            }
            else
            {
                _logger.LogInformation("Maps vendor profile image post model with vendor profle image model.");
                VendorProfileImage = _mapper.Map<VendorProfileImagePostmodel, VendorProfileImage>(model);
                _logger.LogInformation("Add vendor profile image entity to dbcontext.");
                await _unitOfWork.VendorProfileImage.AddAsync(VendorProfileImage);
            }

            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = VendorProfileImage.RowId;
            _logger.LogInformation("Create success response.");
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        /// <summary>
        /// Get the lists of items going to be used by vendor profile image page
        /// Api path will be api/vendorprofile image/get-selectbox-data
        /// </summary>
        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorProfileImageSelectBoxDataViewModel), 200)]
        public async Task<VendorProfileImageSelectBoxDataViewModel> GetSelectboxData()
        {
            _logger.LogInformation("Gets all the dropdown list to be displayed in the vendor profile page.");
            return await _unitOfWork.VendorProfileImage.GetSelectBoxData();
        }

        /// <summary>
        /// Get vendor profile image details by id
        /// Api path will be api/vendorprofileimage/ga
        /// </summary>
        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorProfileImageEditResponse), 200)]
        public async Task<VendorProfileImageEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            VendorProfileImageEditResponse response = new VendorProfileImageEditResponse();

            _logger.LogInformation("Gets vendor profile image details by RowId.");
            VendorProfileImage VendorProfileImage = await _unitOfWork.VendorProfileImage.GetAsync(model.RowId);

            if (VendorProfileImage != null)
            {
                _logger.LogInformation("Sets the retrieved vendor profile image details to the response data.");
                response.Data = _mapper.Map<VendorProfileImageEditModel>(VendorProfileImage);
            }
            else
            {
                response.Data = new VendorProfileImageEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        /// <summary>
        /// Delete vendor profile image by id
        /// Api path will be api/vendorprofileimage/da
        /// </summary>[HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            _logger.LogInformation("Gets vendor profile image details by RowId.");
            VendorProfileImage VendorProfileImage = await _unitOfWork.VendorProfileImage.GetAsync(model.RowId);

            if (VendorProfileImage == null)
            {
                _logger.LogWarning("Create a failure message - Vendor profile image not found.");
                viewModel.CreateFailureResponse("Vendor profile image not found.");
                return viewModel;
            }

            _logger.LogInformation("Update vendor profile image details - change vendor profile image IsActive field to false.");
            VendorProfileImage.IsActive = false;
            await _unitOfWork.VendorProfileImage.UpdateAsync(VendorProfileImage);
            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ProficiencyController : BaseController
    {
        private readonly IMapper _mapper;

        public ProficiencyController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ProficiencyController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ProficiencyDataResultModel), 200)]
        public async Task<ProficiencyDataResultModel> GetAllActiveAsync([FromBody]ProficiencyDataRequestModel model)
        {
            IPagedList<Proficiency> data = await _unitOfWork.Proficiencies.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<ProficiencyDataResultModel, ProficiencyInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(ProficiencyDataResultModel), 200)]
        public async Task<ProficiencyDataResultModel> GetAllInactiveAsync([FromBody]ProficiencyDataRequestModel model)
        {
            IPagedList<Proficiency> data = await _unitOfWork.Proficiencies.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<ProficiencyDataResultModel, ProficiencyInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ProficiencyEditModel), 200)]
        public async Task<ProficiencyEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Proficiency item = await _unitOfWork.Proficiencies.GetAsync(model.RowId);
            ProficiencyEditModel viewModel = _mapper.Map<ProficiencyEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ProficiencyPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Proficiency item = await _unitOfWork.Proficiencies.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Proficiency is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Proficiencies.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Proficiencies.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<ProficiencyPostmodel, Proficiency>(model);
                await _unitOfWork.Proficiencies.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Proficiency item = await _unitOfWork.Proficiencies.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Proficiency not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Proficiencies.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

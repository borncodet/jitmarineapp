﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class NamePrefixController : BaseController
    {
        private readonly IMapper _mapper;

        public NamePrefixController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<NamePrefixController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(NamePrefixDataResultModel), 200)]
        public async Task<NamePrefixDataResultModel> GetAllActiveAsync([FromBody]NamePrefixDataRequestModel model)
        {
            IPagedList<NamePrefix> data = await _unitOfWork.NamePrefix.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<NamePrefixDataResultModel, NamePrefixInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(NamePrefixDataResultModel), 200)]
        public async Task<NamePrefixDataResultModel> GetAllInactiveAsync([FromBody]NamePrefixDataRequestModel model)
        {
            IPagedList<NamePrefix> data = await _unitOfWork.NamePrefix.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<NamePrefixDataResultModel, NamePrefixInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(NamePrefixEditModel), 200)]
        public async Task<NamePrefixEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            NamePrefix item = await _unitOfWork.NamePrefix.GetAsync(model.RowId);
            NamePrefixEditModel viewModel = _mapper.Map<NamePrefixEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] NamePrefixPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            NamePrefix item = await _unitOfWork.NamePrefix.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("NamePrefix is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.NamePrefix.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.NamePrefix.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<NamePrefixPostmodel, NamePrefix>(model);
                await _unitOfWork.NamePrefix.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            NamePrefix item = await _unitOfWork.NamePrefix.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("NamePrefix not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.NamePrefix.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

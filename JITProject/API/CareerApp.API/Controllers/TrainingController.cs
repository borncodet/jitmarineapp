﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class TrainingController : BaseController
    {
        private readonly IMapper _mapper;

        public TrainingController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<TrainingController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingDataResultModel), 200)]
        public async Task<TrainingDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<TrainingInfo> data = await _unitOfWork.Trainings.GetAllAsync(model);
            return PopulateResponseWithoutMap<TrainingDataResultModel, TrainingInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] TrainingPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Training Training = await _unitOfWork.Trainings.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.TrainingCertificate.Equals(model.TrainingCertificate) && x.IsActive == true);

            if (Training != null && Training.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Training details already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                Training = await _unitOfWork.Trainings.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (Training != null)
            {
                Training = _mapper.Map(model, Training);
                await _unitOfWork.Trainings.UpdateAsync(Training);
            }
            else
            {
                Training = _mapper.Map<TrainingPostmodel, Training>(model);
                await _unitOfWork.Trainings.AddAsync(Training);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = Training.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingSelectBoxDataViewModel), 200)]
        public async Task<TrainingSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.Trainings.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingEditResponse), 200)]
        public async Task<TrainingEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            TrainingEditResponse response = new TrainingEditResponse();

            Training Training = await _unitOfWork.Trainings.GetAsync(model.RowId);

            if (Training != null)
            {
                response.Data = _mapper.Map<TrainingEditModel>(Training);
            }
            else
            {
                response.Data = new TrainingEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Training Training = await _unitOfWork.Trainings.GetAsync(model.RowId);
            if (Training == null)
            {
                viewModel.CreateFailureResponse("Training details not found.");
                return viewModel;
            }

            Training.IsActive = false;
            await _unitOfWork.Trainings.UpdateAsync(Training);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class BankAccountTypeController : BaseController
    {
        private readonly IMapper _mapper;

        public BankAccountTypeController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<BankAccountTypeController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(BankAccountTypeDataResultModel), 200)]
        public async Task<BankAccountTypeDataResultModel> GetAllActiveAsync([FromBody]BankAccountTypeDataRequestModel model)
        {
            IPagedList<BankAccountTypeInfo> data = await _unitOfWork.BankAccountTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<BankAccountTypeDataResultModel, BankAccountTypeInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] BankAccountTypePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            BankAccountType BankAccountType = await _unitOfWork.BankAccountTypes.GetFirstOrDefaultAsync(x => x.Title.Equals(model.Title) && x.IsActive == true);

            if (BankAccountType != null && BankAccountType.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Bank account type already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                BankAccountType = await _unitOfWork.BankAccountTypes.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (BankAccountType != null)
            {
                BankAccountType = _mapper.Map(model, BankAccountType);
                await _unitOfWork.BankAccountTypes.UpdateAsync(BankAccountType);
            }
            else
            {
                BankAccountType = _mapper.Map<BankAccountTypePostmodel, BankAccountType>(model);
                await _unitOfWork.BankAccountTypes.AddAsync(BankAccountType);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = BankAccountType.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(BankAccountTypeSelectBoxDataViewModel), 200)]
        public async Task<BankAccountTypeSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.BankAccountTypes.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(BankAccountTypeEditResponse), 200)]
        public async Task<BankAccountTypeEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            BankAccountTypeEditResponse response = new BankAccountTypeEditResponse();

            BankAccountType BankAccountType = await _unitOfWork.BankAccountTypes.GetAsync(model.RowId);

            if (BankAccountType != null)
            {
                response.Data = _mapper.Map<BankAccountTypeEditModel>(BankAccountType);
            }
            else
            {
                response.Data = new BankAccountTypeEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            BankAccountType BankAccountType = await _unitOfWork.BankAccountTypes.GetAsync(model.RowId);
            if (BankAccountType == null)
            {
                viewModel.CreateFailureResponse("Bank account type not found.");
                return viewModel;
            }

            BankAccountType.IsActive = false;
            await _unitOfWork.BankAccountTypes.UpdateAsync(BankAccountType);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

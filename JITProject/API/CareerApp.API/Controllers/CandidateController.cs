﻿/// <summary>
/// APIs to manage Candidate
/// </summary>
using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CandidateController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ILogger<CandidateController> _logger;

        /// <summary>
        /// Candidate Constructor
        /// Dependency Injection is done here - used parameter based dependency injection
        /// </summary>
        public CandidateController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
            _logger = logger;
        }


        /// <summary>
        /// Get all active candidates list
        /// Api path will be api/candidate/gaaa
        /// </summary>
        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateDataResultModel), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<CandidateDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            _logger.LogInformation("Calling api/candidate/gaaa api to get all active Candidate list.");
            IPagedList<CandidateInfo> data = await _unitOfWork.Candidates.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateDataResultModel, CandidateInfo>(data);
        }

        /// <summary>
        /// Get candidate id by user id
        /// Api path will be api/candidate/gc
        /// </summary>
        [Route("gc/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(int), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<int> GetCandidateIdAsync(int id)
        {
            _logger.LogInformation("Calling api/candidate/gc api to get Candidate id.");
            var candidateId = await _unitOfWork.Candidates.GetCandidateIdAsync(id);
            return candidateId;
        }

        /// <summary>
        /// Create or update candidate
        /// Api path will be api/candidate/coea
        /// </summary>
        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidatePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            _logger.LogInformation("Gets candidate details by email.");
            Candidate Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.Email.ToLower().Equals(model.Email.ToLower()) && x.RowId != model.RowId && x.IsActive == true);
            if (Candidate != null)
            {
                _logger.LogWarning("Create a failure message - email already exist.");
                viewModel.CreateFailureResponse("Email is already exist!");
                return viewModel;
            }

            _logger.LogInformation("Gets candidate details by phone.");
            Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.PhoneNumber.ToLower().Equals(model.PhoneNumber.ToLower()) && x.CountryCode.ToLower().Equals(model.CountryCode.ToLower()) && x.RowId != model.RowId && x.IsActive == true);
            if (Candidate != null)
            {
                _logger.LogWarning("Create a failure message - phone already exist.");
                viewModel.CreateFailureResponse("Phone is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                _logger.LogInformation("Gets candidate details by RowId.");
                Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (Candidate != null)
            {
                _logger.LogInformation("Update candidate details.");
                Candidate = _mapper.Map(model, Candidate);
                await _unitOfWork.Candidates.UpdateAsync(Candidate);
            }
            else
            {
                _logger.LogInformation("Maps candidate post model with candidate model.");
                Candidate = _mapper.Map<CandidatePostmodel, Candidate>(model);
                _logger.LogInformation("Add Candidate entity to dbcontext.");
                await _unitOfWork.Candidates.AddAsync(Candidate);
            }

            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = Candidate.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        /// <summary>
        /// Get the lists of items going to be used by candidate
        /// Api path will be api/candidate/get-selectbox-data
        /// </summary>
        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateSelectBoxDataViewModel), 200)]
        public async Task<CandidateSelectBoxDataViewModel> GetSelectboxData()
        {
            _logger.LogInformation("Gets all the dropdown list to be displayed in the candidate page.");
            return await _unitOfWork.Candidates.GetSelectBoxData();
        }

        /// <summary>
        /// Get candidate details by id
        /// Api path will be api/candidate/ga
        /// </summary>
        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateEditResponse), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<CandidateEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateEditResponse response = new CandidateEditResponse();

            _logger.LogInformation("Gets candidate details by RowId.");
            Candidate Candidate = await _unitOfWork.Candidates.GetAsync(model.RowId);

            if (Candidate != null)
            {
                _logger.LogInformation("Sets the retrieved candidate details to the response data.");
                response.Data = _mapper.Map<CandidateEditModel>(Candidate);
            }
            else
            {
                response.Data = new CandidateEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        /// <summary>
        /// Delete candidate by id
        /// Api path will be api/candidate/da
        /// </summary>
        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            _logger.LogInformation("Gets candidate details by RowId.");
            Candidate Candidate = await _unitOfWork.Candidates.GetAsync(model.RowId);
            if (Candidate == null)
            {
                _logger.LogWarning("Create a failure message - Candidate not found.");
                viewModel.CreateFailureResponse("Candidate not found.");
                return viewModel;
            }

            _logger.LogInformation("Update candidate details - change candidate's IsActive field to false.");
            Candidate.IsActive = false;
            await _unitOfWork.Candidates.UpdateAsync(Candidate);
            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

        //candidate for verify email or phone during forgot password process
        [AllowAnonymous]
        [HttpPost("verify-candidate")]
        public async Task<IActionResult> CheckCandidateExitsByEmailOrPhoneAsync(string email, string phoneNumber, string countryCode, string type)
        {
            try
            {
                Candidate Candidate = null;

                if (type.ToLower().Equals("email"))
                {
                    Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.Email != null && x.Email.ToLower().Equals(email.ToLower()) && x.IsActive == true);

                }
                if (type.ToLower().Equals("mobile"))
                {
                    Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.PhoneNumber != null && x.PhoneNumber.ToLower().Equals(phoneNumber.ToLower()) && x.IsActive == true && x.CountryCode != null && x.CountryCode.ToLower().Equals(countryCode.ToLower()));
                }

                if (Candidate != null)
                {
                    return Ok("Valid user");
                }
                return BadRequest("Not a valid user");

            }
            catch (Exception ex)
            {
                return BadRequest("Not a valid user");
            }
        }

    }
}

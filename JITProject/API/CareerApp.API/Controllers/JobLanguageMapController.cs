﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobLanguageMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobLanguageMapController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobLanguageMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobLanguageMapDataResultModel), 200)]
        public async Task<JobLanguageMapDataResultModel> GetAllActiveAsync([FromBody]JobLanguageMapDataRequestModel model)
        {
            IPagedList<JobLanguageMapInfo> data = await _unitOfWork.JobLanguageMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobLanguageMapDataResultModel, JobLanguageMapInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobLanguageMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobLanguageMap JobLanguageMap = await _unitOfWork.JobLanguageMap.GetFirstOrDefaultAsync(x => x.JobId == model.JobId && x.LanguageId == model.LanguageId && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobLanguageMap != null && JobLanguageMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Mapping is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobLanguageMap = await _unitOfWork.JobLanguageMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobLanguageMap != null)
            {
                JobLanguageMap = _mapper.Map(model, JobLanguageMap);
                await _unitOfWork.JobLanguageMap.UpdateAsync(JobLanguageMap);
            }
            else
            {
                JobLanguageMap = _mapper.Map<JobLanguageMapPostmodel, JobLanguageMap>(model);
                await _unitOfWork.JobLanguageMap.AddAsync(JobLanguageMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobLanguageMap.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobLanguageMapSelectBoxDataViewModel), 200)]
        public async Task<JobLanguageMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobLanguageMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobLanguageMapEditResponse), 200)]
        public async Task<JobLanguageMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobLanguageMapEditResponse response = new JobLanguageMapEditResponse();

            JobLanguageMap JobLanguageMap = await _unitOfWork.JobLanguageMap.GetAsync(model.RowId);

            if (JobLanguageMap != null)
            {
                response.Data = _mapper.Map<JobLanguageMapEditModel>(JobLanguageMap);
            }
            else
            {
                response.Data = new JobLanguageMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobLanguageMap JobLanguageMap = await _unitOfWork.JobLanguageMap.GetAsync(model.RowId);
            if (JobLanguageMap == null)
            {
                viewModel.CreateFailureResponse("Mapping not found.");
                return viewModel;
            }

            JobLanguageMap.IsActive = false;
            await _unitOfWork.JobLanguageMap.UpdateAsync(JobLanguageMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class RelationshipController : BaseController
    {
        private readonly IMapper _mapper;

        public RelationshipController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<RelationshipController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(RelationshipDataResultModel), 200)]
        public async Task<RelationshipDataResultModel> GetAllActiveAsync([FromBody]RelationshipDataRequestModel model)
        {
            IPagedList<RelationshipInfo> data = await _unitOfWork.Relationships.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<RelationshipDataResultModel, RelationshipInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] RelationshipPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Relationship Relationship = await _unitOfWork.Relationships.GetFirstOrDefaultAsync(x => x.Title.Equals(model.Title) && x.IsActive == true);

            if (Relationship != null && Relationship.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Relationship already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                Relationship = await _unitOfWork.Relationships.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (Relationship != null)
            {
                Relationship = _mapper.Map(model, Relationship);
                await _unitOfWork.Relationships.UpdateAsync(Relationship);
            }
            else
            {
                Relationship = _mapper.Map<RelationshipPostmodel, Relationship>(model);
                await _unitOfWork.Relationships.AddAsync(Relationship);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = Relationship.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(RelationshipSelectBoxDataViewModel), 200)]
        public async Task<RelationshipSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.Relationships.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(RelationshipEditResponse), 200)]
        public async Task<RelationshipEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            RelationshipEditResponse response = new RelationshipEditResponse();

            Relationship Relationship = await _unitOfWork.Relationships.GetAsync(model.RowId);

            if (Relationship != null)
            {
                response.Data = _mapper.Map<RelationshipEditModel>(Relationship);
            }
            else
            {
                response.Data = new RelationshipEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Relationship Relationship = await _unitOfWork.Relationships.GetAsync(model.RowId);
            if (Relationship == null)
            {
                viewModel.CreateFailureResponse("Relationship not found.");
                return viewModel;
            }

            Relationship.IsActive = false;
            await _unitOfWork.Relationships.UpdateAsync(Relationship);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

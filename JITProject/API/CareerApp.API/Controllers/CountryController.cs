﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CountryController : BaseController
    {
        private readonly IMapper _mapper;

        public CountryController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CountryController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CountryDataResultModel), 200)]
        public async Task<CountryDataResultModel> GetAllActiveAsync([FromBody]CountryDataRequestModel model)
        {
            IPagedList<Country> data = await _unitOfWork.Countries.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<CountryDataResultModel, CountryInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(CountryDataResultModel), 200)]
        public async Task<CountryDataResultModel> GetAllInactiveAsync([FromBody]CountryDataRequestModel model)
        {
            IPagedList<Country> data = await _unitOfWork.Countries.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<CountryDataResultModel, CountryInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CountryEditModel), 200)]
        public async Task<CountryEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Country item = await _unitOfWork.Countries.GetAsync(model.RowId);
            CountryEditModel viewModel = _mapper.Map<CountryEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CountryPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Country item = await _unitOfWork.Countries.GetFirstOrDefaultAsync(x => x.Name.ToLower().Equals(model.Name.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Country is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Countries.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Countries.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<CountryPostmodel, Country>(model);
                await _unitOfWork.Countries.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Country item = await _unitOfWork.Countries.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Country not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Countries.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

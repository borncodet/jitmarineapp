﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ShiftAvailableController : BaseController
    {
        private readonly IMapper _mapper;

        public ShiftAvailableController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ShiftAvailableController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ShiftAvailableDataResultModel), 200)]
        public async Task<ShiftAvailableDataResultModel> GetAllActiveAsync([FromBody]ShiftAvailableDataRequestModel model)
        {
            IPagedList<ShiftAvailable> data = await _unitOfWork.ShiftAvailable.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<ShiftAvailableDataResultModel, ShiftAvailableInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(ShiftAvailableDataResultModel), 200)]
        public async Task<ShiftAvailableDataResultModel> GetAllInactiveAsync([FromBody]ShiftAvailableDataRequestModel model)
        {
            IPagedList<ShiftAvailable> data = await _unitOfWork.ShiftAvailable.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<ShiftAvailableDataResultModel, ShiftAvailableInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ShiftAvailableEditModel), 200)]
        public async Task<ShiftAvailableEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            ShiftAvailable item = await _unitOfWork.ShiftAvailable.GetAsync(model.RowId);
            ShiftAvailableEditModel viewModel = _mapper.Map<ShiftAvailableEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ShiftAvailablePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ShiftAvailable item = await _unitOfWork.ShiftAvailable.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Shift Available is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.ShiftAvailable.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.ShiftAvailable.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<ShiftAvailablePostmodel, ShiftAvailable>(model);
                await _unitOfWork.ShiftAvailable.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ShiftAvailable item = await _unitOfWork.ShiftAvailable.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Shift Available not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.ShiftAvailable.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

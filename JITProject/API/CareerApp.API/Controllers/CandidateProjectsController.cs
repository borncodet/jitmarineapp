﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateProjectsController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateProjectsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateProjectsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateProjectsDataResultModel), 200)]
        public async Task<CandidateProjectsDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateProjectsInfo> data = await _unitOfWork.CandidateProjects.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateProjectsDataResultModel, CandidateProjectsInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateProjectsPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateProjects CandidateProjects = await _unitOfWork.CandidateProjects.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.ProjectName.Equals(model.ProjectName) && x.FromDate.Equals(model.FromDate) && x.ToDate.Equals(model.ToDate) && x.IsActive == true);

            if (CandidateProjects != null && CandidateProjects.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate project already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                CandidateProjects = await _unitOfWork.CandidateProjects.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateProjects != null)
            {
                CandidateProjects = _mapper.Map(model, CandidateProjects);
                await _unitOfWork.CandidateProjects.UpdateAsync(CandidateProjects);
            }
            else
            {
                CandidateProjects = _mapper.Map<CandidateProjectsPostmodel, CandidateProjects>(model);
                await _unitOfWork.CandidateProjects.AddAsync(CandidateProjects);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateProjects.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateProjectsSelectBoxDataViewModel), 200)]
        public async Task<CandidateProjectsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateProjects.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateProjectsEditResponse), 200)]
        public async Task<CandidateProjectsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateProjectsEditResponse response = new CandidateProjectsEditResponse();

            CandidateProjects CandidateProjects = await _unitOfWork.CandidateProjects.GetAsync(model.RowId);

            if (CandidateProjects != null)
            {
                response.Data = _mapper.Map<CandidateProjectsEditModel>(CandidateProjects);
            }
            else
            {
                response.Data = new CandidateProjectsEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateProjects CandidateProjects = await _unitOfWork.CandidateProjects.GetAsync(model.RowId);
            if (CandidateProjects == null)
            {
                viewModel.CreateFailureResponse("Candidate project not found.");
                return viewModel;
            }

            CandidateProjects.IsActive = false;
            await _unitOfWork.CandidateProjects.UpdateAsync(CandidateProjects);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

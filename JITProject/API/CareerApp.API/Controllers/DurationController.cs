﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DurationController : BaseController
    {
        private readonly IMapper _mapper;

        public DurationController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DurationController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DurationDataResultModel), 200)]
        public async Task<DurationDataResultModel> GetAllActiveAsync([FromBody]DurationDataRequestModel model)
        {
            IPagedList<Duration> data = await _unitOfWork.Durations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<DurationDataResultModel, DurationInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(DurationDataResultModel), 200)]
        public async Task<DurationDataResultModel> GetAllInactiveAsync([FromBody]DurationDataRequestModel model)
        {
            IPagedList<Duration> data = await _unitOfWork.Durations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<DurationDataResultModel, DurationInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DurationEditModel), 200)]
        public async Task<DurationEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Duration item = await _unitOfWork.Durations.GetAsync(model.RowId);
            DurationEditModel viewModel = _mapper.Map<DurationEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] DurationPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Duration item = await _unitOfWork.Durations.GetFirstOrDefaultAsync(x => x.Day.Equals(model.Day) && x.Month.Equals(model.Month) && x.Year.Equals(model.Year) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Duration is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Durations.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Durations.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<DurationPostmodel, Duration>(model);
                await _unitOfWork.Durations.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Duration item = await _unitOfWork.Durations.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Duration not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Durations.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

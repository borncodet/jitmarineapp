﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAlertRoleMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAlertRoleMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAlertRoleMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertRoleMapDataResultModel), 200)]
        public async Task<JobAlertRoleMapDataResultModel> GetAllActiveAsync([FromBody]JobAlertRoleMapDataRequestModel model)
        {
            IPagedList<JobAlertRoleMapInfo> data = await _unitOfWork.JobAlertRoleMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobAlertRoleMapDataResultModel, JobAlertRoleMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAlertRoleMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAlertRoleMap JobAlertRoleMap = await _unitOfWork.JobAlertRoleMap.GetFirstOrDefaultAsync(x => x.JobAlertId.Equals(model.JobAlertId) && x.JobRoleId.Equals(model.JobRoleId) && x.IsActive == true);

            if (JobAlertRoleMap != null && JobAlertRoleMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Job role already mapped to the alert.");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobAlertRoleMap = await _unitOfWork.JobAlertRoleMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobAlertRoleMap != null)
            {
                JobAlertRoleMap = _mapper.Map(model, JobAlertRoleMap);
                await _unitOfWork.JobAlertRoleMap.UpdateAsync(JobAlertRoleMap);
            }
            else
            {
                JobAlertRoleMap = _mapper.Map<JobAlertRoleMapPostmodel, JobAlertRoleMap>(model);
                await _unitOfWork.JobAlertRoleMap.AddAsync(JobAlertRoleMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobAlertRoleMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertRoleMapSelectBoxDataViewModel), 200)]
        public async Task<JobAlertRoleMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAlertRoleMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertRoleMapEditResponse), 200)]
        public async Task<JobAlertRoleMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAlertRoleMapEditResponse response = new JobAlertRoleMapEditResponse();

            JobAlertRoleMap JobAlertRoleMap = await _unitOfWork.JobAlertRoleMap.GetAsync(model.RowId);

            if (JobAlertRoleMap != null)
            {
                response.Data = _mapper.Map<JobAlertRoleMapEditModel>(JobAlertRoleMap);
            }
            else
            {
                response.Data = new JobAlertRoleMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAlertRoleMap JobAlertRoleMap = await _unitOfWork.JobAlertRoleMap.GetAsync(model.RowId);
            if (JobAlertRoleMap == null)
            {
                viewModel.CreateFailureResponse("Job type alert map not found.");
                return viewModel;
            }

            JobAlertRoleMap.IsActive = false;
            await _unitOfWork.JobAlertRoleMap.UpdateAsync(JobAlertRoleMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

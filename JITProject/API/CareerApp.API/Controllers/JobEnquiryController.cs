﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobEnquiryController : BaseController
    {
        private readonly IMapper _mapper;

        public JobEnquiryController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobEnquiryController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorJobListDataResultModel), 200)]
        public async Task<VendorJobListDataResultModel> GetAllActiveAsync([FromBody]VendorJobListDataRequestModel model)
        {
            IPagedList<VendorJobListInfo> data = await _unitOfWork.VendorJobLists.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<VendorJobListDataResultModel, VendorJobListInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] VendorJobListPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            VendorJobList VendorJobList = await _unitOfWork.VendorJobLists.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            var user = User.Identity.Name;

            if (VendorJobList != null && VendorJobList.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Job Title is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                VendorJobList = await _unitOfWork.VendorJobLists.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (VendorJobList != null)
            {
                VendorJobList = _mapper.Map(model, VendorJobList);
                await _unitOfWork.VendorJobLists.UpdateAsync(VendorJobList);
            }
            else
            {
                VendorJobList = _mapper.Map<VendorJobListPostmodel, VendorJobList>(model);
                await _unitOfWork.VendorJobLists.AddAsync(VendorJobList);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = VendorJobList.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorJobListSelectBoxDataViewModel), 200)]
        public async Task<VendorJobListSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.VendorJobLists.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorJobListEditResponse), 200)]
        public async Task<VendorJobListEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            VendorJobListEditResponse response = new VendorJobListEditResponse();

            VendorJobList VendorJobList = await _unitOfWork.VendorJobLists.GetAsync(model.RowId);

            if (VendorJobList != null)
            {
                response.Data = _mapper.Map<VendorJobListEditModel>(VendorJobList);
            }
            else
            {
                response.Data = new VendorJobListEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            VendorJobList VendorJobList = await _unitOfWork.VendorJobLists.GetAsync(model.RowId);
            if (VendorJobList == null)
            {
                viewModel.CreateFailureResponse("Job not found.");
                return viewModel;
            }

            VendorJobList.IsActive = false;
            await _unitOfWork.VendorJobLists.UpdateAsync(VendorJobList);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

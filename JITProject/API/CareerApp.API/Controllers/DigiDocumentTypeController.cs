﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DigiDocumentTypeController : BaseController
    {
        private readonly IMapper _mapper;

        public DigiDocumentTypeController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DigiDocumentTypeController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentTypeDataResultModel), 200)]
        public async Task<DigiDocumentTypeDataResultModel> GetAllActiveAsync([FromBody]DigiDocumentTypeDataRequestModel model)
        {
            IPagedList<DigiDocumentType> data = await _unitOfWork.DigiDocumentTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<DigiDocumentTypeDataResultModel, DigiDocumentTypeInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentTypeDataResultModel), 200)]
        public async Task<DigiDocumentTypeDataResultModel> GetAllInactiveAsync([FromBody]DigiDocumentTypeDataRequestModel model)
        {
            IPagedList<DigiDocumentType> data = await _unitOfWork.DigiDocumentTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<DigiDocumentTypeDataResultModel, DigiDocumentTypeInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentTypeEditModel), 200)]
        public async Task<DigiDocumentTypeEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            DigiDocumentType item = await _unitOfWork.DigiDocumentTypes.GetAsync(model.RowId);
            DigiDocumentTypeEditModel viewModel = _mapper.Map<DigiDocumentTypeEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] DigiDocumentTypePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DigiDocumentType item = await _unitOfWork.DigiDocumentTypes.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Digi Document Type is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.DigiDocumentTypes.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.DigiDocumentTypes.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<DigiDocumentTypePostmodel, DigiDocumentType>(model);
                await _unitOfWork.DigiDocumentTypes.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            DigiDocumentType item = await _unitOfWork.DigiDocumentTypes.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Digi Document Type not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.DigiDocumentTypes.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

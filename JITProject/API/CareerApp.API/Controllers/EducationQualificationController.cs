﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class EducationQualificationController : BaseController
    {
        private readonly IMapper _mapper;

        public EducationQualificationController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<EducationQualificationController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(EducationQualificationDataResultModel), 200)]
        public async Task<EducationQualificationDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<EducationQualificationInfo> data = await _unitOfWork.EducationQualifications.GetAllAsync(model);
            return PopulateResponseWithoutMap<EducationQualificationDataResultModel, EducationQualificationInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] EducationQualificationPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            EducationQualification EducationQualification = await _unitOfWork.EducationQualifications.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.Course.Equals(model.Course) && x.IsActive == true);

            if (EducationQualification != null && EducationQualification.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Education qualification already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                EducationQualification = await _unitOfWork.EducationQualifications.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (EducationQualification != null)
            {
                EducationQualification = _mapper.Map(model, EducationQualification);
                await _unitOfWork.EducationQualifications.UpdateAsync(EducationQualification);
            }
            else
            {
                EducationQualification = _mapper.Map<EducationQualificationPostmodel, EducationQualification>(model);
                await _unitOfWork.EducationQualifications.AddAsync(EducationQualification);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = EducationQualification.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(EducationQualificationSelectBoxDataViewModel), 200)]
        public async Task<EducationQualificationSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.EducationQualifications.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(EducationQualificationEditResponse), 200)]
        public async Task<EducationQualificationEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            EducationQualificationEditResponse response = new EducationQualificationEditResponse();

            EducationQualification EducationQualification = await _unitOfWork.EducationQualifications.GetAsync(model.RowId);

            if (EducationQualification != null)
            {
                response.Data = _mapper.Map<EducationQualificationEditModel>(EducationQualification);
            }
            else
            {
                response.Data = new EducationQualificationEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            EducationQualification EducationQualification = await _unitOfWork.EducationQualifications.GetAsync(model.RowId);
            if (EducationQualification == null)
            {
                viewModel.CreateFailureResponse("Education qualification not found.");
                return viewModel;
            }

            EducationQualification.IsActive = false;
            await _unitOfWork.EducationQualifications.UpdateAsync(EducationQualification);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

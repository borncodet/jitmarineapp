﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class GenderController : BaseController
    {
        private readonly IMapper _mapper;

        public GenderController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<GenderController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(GenderDataResultModel), 200)]
        public async Task<GenderDataResultModel> GetAllActiveAsync([FromBody]GenderDataRequestModel model)
        {
            IPagedList<Gender> data = await _unitOfWork.Genders.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<GenderDataResultModel, GenderInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(GenderDataResultModel), 200)]
        public async Task<GenderDataResultModel> GetAllInactiveAsync([FromBody]GenderDataRequestModel model)
        {
            IPagedList<Gender> data = await _unitOfWork.Genders.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<GenderDataResultModel, GenderInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(GenderEditModel), 200)]
        public async Task<GenderEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Gender item = await _unitOfWork.Genders.GetAsync(model.RowId);
            GenderEditModel viewModel = _mapper.Map<GenderEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] GenderPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Gender item = await _unitOfWork.Genders.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Gender is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Genders.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Genders.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<GenderPostmodel, Gender>(model);
                await _unitOfWork.Genders.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Gender item = await _unitOfWork.Genders.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Gender not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Genders.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

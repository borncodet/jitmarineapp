﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ResumeTemplateController : BaseController
    {
        private readonly IMapper _mapper;

        public ResumeTemplateController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ResumeTemplateController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeTemplateDataResultModel), 200)]
        public async Task<ResumeTemplateDataResultModel> GetAllActiveAsync([FromBody]BaseResumeSearchByCandidateViewModel model)
        {
            IPagedList<ResumeTemplateInfo> data = await _unitOfWork.ResumeTemplates.GetAllAsync(model);
            return PopulateResponseWithoutMap<ResumeTemplateDataResultModel, ResumeTemplateInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] ResumeTemplatePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ResumeTemplate ResumeTemplate = await _unitOfWork.ResumeTemplates.GetFirstOrDefaultAsync(x => x.Title.Equals(model.Title) && x.IsActive == true);

            if (ResumeTemplate != null && ResumeTemplate.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Resume template already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                model.ResumeImage = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\ResumeImage\", model.Document);
            }

            if (model.RowId != 0)
            {
                ResumeTemplate = await _unitOfWork.ResumeTemplates.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (ResumeTemplate != null)
            {
                ResumeTemplate = _mapper.Map(model, ResumeTemplate);
                await _unitOfWork.ResumeTemplates.UpdateAsync(ResumeTemplate);
            }
            else
            {
                ResumeTemplate = _mapper.Map<ResumeTemplatePostmodel, ResumeTemplate>(model);
                await _unitOfWork.ResumeTemplates.AddAsync(ResumeTemplate);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ResumeTemplate.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        // Create resume async
        [Route("cra")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateResumeAsync([FromForm] ResumeTemplateNewPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ResumeTemplate ResumeTemplate = await _unitOfWork.ResumeTemplates.GetFirstOrDefaultAsync(x => x.Title.Equals(model.ResumeTemplatePostmodel.Title));

            if (ResumeTemplate != null && ResumeTemplate.RowId != model.ResumeTemplatePostmodel.RowId)
            {
                viewModel.CreateFailureResponse("Resume template already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.ResumeTemplatePostmodel.Document != null)
            {
                model.ResumeTemplatePostmodel.ResumeImage = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\ResumeImage\", model.ResumeTemplatePostmodel.Document);
            }

            if (model.ResumeTemplatePostmodel.RowId != 0)
            {
                ResumeTemplate = await _unitOfWork.ResumeTemplates.GetFirstOrDefaultAsync(x => x.RowId == model.ResumeTemplatePostmodel.RowId);
            }

            if (ResumeTemplate != null)
            {
                ResumeTemplate = _mapper.Map(model.ResumeTemplatePostmodel, ResumeTemplate);
                await _unitOfWork.ResumeTemplates.UpdateAsync(ResumeTemplate);
            }
            else
            {
                ResumeTemplate = _mapper.Map<ResumeTemplatePostmodel, ResumeTemplate>(model.ResumeTemplatePostmodel);
                await _unitOfWork.ResumeTemplates.AddAsync(ResumeTemplate);
            }

            //Resume expertise mapping
            if (model.ResumeExpertiseMapPostmodel != null)
            {
                if (model.ResumeExpertiseMapPostmodel.Count > 0)
                {
                    // Remove all existing expertise for resume
                    var ResumeExpertiseMapList = await _unitOfWork.ResumeExpertiseMap.FindAsync(x => x.ResumeTemplateId.Equals(ResumeTemplate.RowId));
                    await _unitOfWork.ResumeExpertiseMap.RemoveRangeAsync(ResumeExpertiseMapList);
                    // Add new expertise list for resume
                    foreach (var item in model.ResumeExpertiseMapPostmodel)
                    {
                        item.ResumeTemplateId = ResumeTemplate.RowId;
                        ResumeExpertiseMap ResumeExpertiseMap = _mapper.Map<ResumeExpertiseMapPostmodel, ResumeExpertiseMap>(item);
                        await _unitOfWork.ResumeExpertiseMap.AddAsync(ResumeExpertiseMap);
                    }
                }
            }

            //Resume experience mapping
            if (model.ResumeExperienceMapPostmodel != null)
            {
                if (model.ResumeExperienceMapPostmodel.Count > 0)
                {
                    // Remove all existing experience for resume
                    var ResumeExperienceMapList = await _unitOfWork.ResumeExperienceMap.FindAsync(x => x.ResumeTemplateId.Equals(ResumeTemplate.RowId));
                    await _unitOfWork.ResumeExperienceMap.RemoveRangeAsync(ResumeExperienceMapList);
                    // Add new experience list for resume
                    foreach (var item in model.ResumeExperienceMapPostmodel)
                    {
                        item.ResumeTemplateId = ResumeTemplate.RowId;
                        ResumeExperienceMap ResumeExperienceMap = _mapper.Map<ResumeExperienceMapPostmodel, ResumeExperienceMap>(item);
                        await _unitOfWork.ResumeExperienceMap.AddAsync(ResumeExperienceMap);
                    }
                }
            }

            //Resume designation mapping
            if (model.ResumeDesignationMapPostmodel != null)
            {
                if (model.ResumeDesignationMapPostmodel.Count > 0)
                {
                    // Remove all existing designation for resume
                    var ResumeDesignationMapList = await _unitOfWork.ResumeDesignationMap.FindAsync(x => x.ResumeTemplateId.Equals(ResumeTemplate.RowId));
                    await _unitOfWork.ResumeDesignationMap.RemoveRangeAsync(ResumeDesignationMapList);
                    // Add new designation list for resume
                    foreach (var item in model.ResumeDesignationMapPostmodel)
                    {
                        item.ResumeTemplateId = ResumeTemplate.RowId;
                        ResumeDesignationMap ResumeDesignationMap = _mapper.Map<ResumeDesignationMapPostmodel, ResumeDesignationMap>(item);
                        await _unitOfWork.ResumeDesignationMap.AddAsync(ResumeDesignationMap);
                    }
                }
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ResumeTemplate.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeTemplateSelectBoxDataViewModel), 200)]
        public async Task<ResumeTemplateSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.ResumeTemplates.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeTemplateEditResponse), 200)]
        public async Task<ResumeTemplateEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            ResumeTemplateEditResponse response = new ResumeTemplateEditResponse();

            ResumeTemplate ResumeTemplate = await _unitOfWork.ResumeTemplates.GetAsync(model.RowId);

            if (ResumeTemplate != null)
            {
                response.Data = _mapper.Map<ResumeTemplateEditModel>(ResumeTemplate);
            }
            else
            {
                response.Data = new ResumeTemplateEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ResumeTemplate ResumeTemplate = await _unitOfWork.ResumeTemplates.GetAsync(model.RowId);
            if (ResumeTemplate == null)
            {
                viewModel.CreateFailureResponse("Resume tmeplate not found.");
                return viewModel;
            }

            ResumeTemplate.IsActive = false;
            await _unitOfWork.ResumeTemplates.UpdateAsync(ResumeTemplate);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

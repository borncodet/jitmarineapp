﻿using AutoMapper;
using CareerApp.API.Helpers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models.DigiLocker;
using CareerApp.Respository.Interface;
using CareerApp.ViewModels.DigiLocker;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class DigiDocumentDownloadController : BaseController
    {
        private readonly IOptions<ApplicationConfiguration> _applicationConfiguration;
        private const long DOWNLOAD_URL_EXPIRY_MINUTES = 120;
        public DigiDocumentDownloadController(IHttpContextAccessor accessor, IHostingEnvironment env, IUnitOfWork unitOfWork, IAccountManager accountManager, ILogger<BaseController> logger, IMapper mapper,

            IOptions<ApplicationConfiguration> applicationConfiguration) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            this._applicationConfiguration = applicationConfiguration;
        }

        [AllowAnonymous]
        [HttpGet("file")]
        public async Task<string> Download([FromQuery] string AccessKey, long Expires)
        {
            //1. get file info
            var downloadDoc = _unitOfWork.DigiDocumentDownload.TableNoTracking.Where(c => c.AccessKey == AccessKey && c.Expires > DateTimeOffset.UtcNow).FirstOrDefault();

            //2. If file expire or not found return 404
            if (downloadDoc == null)
            {
                return "File expired";
            }

            //3. Get document and file path
            var doc = _unitOfWork.DigiDocumentUpload.TableNoTracking.FirstOrDefault(c => c.IsActive && c.DigiDocumentDetailId == downloadDoc.DigiDocumentDetailId);
            var digiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetAsync(doc.DigiDocumentDetailId);
            if (digiDocumentDetails != null)
            {
                var digiDocumentType = await _unitOfWork.DigiDocumentTypes.GetAsync(digiDocumentDetails.DigiDocumentTypeId);

                var fileFullPath = @"/Upload/" + digiDocumentType.Title + @"/" + doc.DigiDocument;

                return fileFullPath;

            }
            else
            {
                return "Not Found";
            }
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentDownloadResponse), 200)]
        public async Task<DigiDocumentDownloadResponse> GetAsync([FromBody]DigiDocumentDownloadModel model)
        {
            var viewModel = new DigiDocumentDownloadResponse();
            //1. Get candidate from user token
            //var candidate = (await GetCurrentCandidateId()).CandidateId;

            //2. Validate Document
            var document = await _unitOfWork.DigiDocumentDetails.GetAsync(model.DigiDocumentDetailId);
            if (document == null)
            {
                viewModel.CreateFailureResponse("Digi document not exist or its not accessible");
                return viewModel;
            }

            var candidate = document.CandidateId;

            //3. Create a document download
            var documentDownload = new DigiDocumentDownload
            {
                DigiDocumentDetailId = model.DigiDocumentDetailId,
                AccessKey = Guid.NewGuid().ToString("N"),
                Expires = DateTime.UtcNow.AddMinutes(DOWNLOAD_URL_EXPIRY_MINUTES),
                CandidateId = candidate
            };
            await _unitOfWork.DigiDocumentDownload.AddAsync(documentDownload);
            await _unitOfWork.SaveChangesAsync();
            //4. Generate URL
            viewModel.DownloadUrl = $"{_applicationConfiguration.Value.ApplicationUrl}/DigiDocumentDownload/file?AccessKey={ documentDownload.AccessKey }&Expires={documentDownload.Expires.Ticks}";
            return viewModel;
        }
    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class BankDetailsController : BaseController
    {
        private readonly IMapper _mapper;

        public BankDetailsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<BankDetailsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(BankDetailsDataResultModel), 200)]
        public async Task<BankDetailsDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<BankDetailsInfo> data = await _unitOfWork.BankDetails.GetAllAsync(model);
            return PopulateResponseWithoutMap<BankDetailsDataResultModel, BankDetailsInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] BankDetailsPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();
            BankDetails BankDetails = await _unitOfWork.BankDetails.GetFirstOrDefaultAsync(x => x.AccountNo.Equals(model.AccountNo) && x.BankName.Equals(model.BankName) && x.IsActive == true);

            if (BankDetails != null && BankDetails.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Bank account details already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                BankDetails = await _unitOfWork.BankDetails.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (BankDetails != null)
            {
                BankDetails = _mapper.Map(model, BankDetails);
                await _unitOfWork.BankDetails.UpdateAsync(BankDetails);
            }
            else
            {
                BankDetails = _mapper.Map<BankDetailsPostmodel, BankDetails>(model);
                await _unitOfWork.BankDetails.AddAsync(BankDetails);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = BankDetails.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(BankDetailsSelectBoxDataViewModel), 200)]
        public async Task<BankDetailsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.BankDetails.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(BankDetailsEditResponse), 200)]
        public async Task<BankDetailsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            BankDetailsEditResponse response = new BankDetailsEditResponse();

            BankDetails BankDetails = await _unitOfWork.BankDetails.GetAsync(model.RowId);

            if (BankDetails != null)
            {
                response.Data = _mapper.Map<BankDetailsEditModel>(BankDetails);
            }
            else
            {
                response.Data = new BankDetailsEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            BankDetails BankDetails = await _unitOfWork.BankDetails.GetAsync(model.RowId);
            if (BankDetails == null)
            {
                viewModel.CreateFailureResponse("Bank account details not found.");
                return viewModel;
            }

            BankDetails.IsActive = false;
            await _unitOfWork.BankDetails.UpdateAsync(BankDetails);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

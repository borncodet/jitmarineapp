﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ResumeDesignationMapController : BaseController
    {
        private readonly IMapper _mapper;

        public ResumeDesignationMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ResumeDesignationMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeDesignationMapDataResultModel), 200)]
        public async Task<ResumeDesignationMapDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<ResumeDesignationMapInfo> data = await _unitOfWork.ResumeDesignationMap.GetAllAsync(model);
            return PopulateResponseWithoutMap<ResumeDesignationMapDataResultModel, ResumeDesignationMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ResumeDesignationMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ResumeDesignationMap ResumeDesignationMap = await _unitOfWork.ResumeDesignationMap.GetFirstOrDefaultAsync(x => x.DesignationId.Equals(model.DesignationId) && x.ResumeTemplateId.Equals(model.ResumeTemplateId) && x.IsActive == true);

            if (ResumeDesignationMap != null && ResumeDesignationMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Designation already mapped to the specified resume!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                ResumeDesignationMap = await _unitOfWork.ResumeDesignationMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (ResumeDesignationMap != null)
            {
                ResumeDesignationMap = _mapper.Map(model, ResumeDesignationMap);
                await _unitOfWork.ResumeDesignationMap.UpdateAsync(ResumeDesignationMap);
            }
            else
            {
                ResumeDesignationMap = _mapper.Map<ResumeDesignationMapPostmodel, ResumeDesignationMap>(model);
                await _unitOfWork.ResumeDesignationMap.AddAsync(ResumeDesignationMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ResumeDesignationMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeDesignationMapSelectBoxDataViewModel), 200)]
        public async Task<ResumeDesignationMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.ResumeDesignationMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeDesignationMapEditResponse), 200)]
        public async Task<ResumeDesignationMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            ResumeDesignationMapEditResponse response = new ResumeDesignationMapEditResponse();

            ResumeDesignationMap ResumeDesignationMap = await _unitOfWork.ResumeDesignationMap.GetAsync(model.RowId);

            if (ResumeDesignationMap != null)
            {
                response.Data = _mapper.Map<ResumeDesignationMapEditModel>(ResumeDesignationMap);
            }
            else
            {
                response.Data = new ResumeDesignationMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ResumeDesignationMap ResumeDesignationMap = await _unitOfWork.ResumeDesignationMap.GetAsync(model.RowId);
            if (ResumeDesignationMap == null)
            {
                viewModel.CreateFailureResponse("Designation mapped resume not found.");
                return viewModel;
            }

            ResumeDesignationMap.IsActive = false;
            await _unitOfWork.ResumeDesignationMap.UpdateAsync(ResumeDesignationMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

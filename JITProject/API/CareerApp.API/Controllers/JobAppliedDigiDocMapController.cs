﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAppliedDigiDocMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAppliedDigiDocMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAppliedDigiDocMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDigiDocMapDataResultModel), 200)]
        public async Task<JobAppliedDigiDocMapDataResultModel> GetAllAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<JobAppliedDigiDocMapInfo> data = await _unitOfWork.JobAppliedDigiDocMap.GetAllAsync(model);
            return PopulateResponseWithoutMap<JobAppliedDigiDocMapDataResultModel, JobAppliedDigiDocMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAppliedDigiDocMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAppliedDigiDocMap JobAppliedDigiDocMap = await _unitOfWork.JobAppliedDigiDocMap.GetFirstOrDefaultAsync(x => x.JobAppliedDetailsId.Equals(model.JobAppliedDetailsId) && x.DigiDocumentDetailId.Equals(model.DigiDocumentDetailId) && x.IsActive == true);

            if (JobAppliedDigiDocMap != null && JobAppliedDigiDocMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Document already mapped!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobAppliedDigiDocMap = await _unitOfWork.JobAppliedDigiDocMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobAppliedDigiDocMap != null)
            {
                JobAppliedDigiDocMap = _mapper.Map(model, JobAppliedDigiDocMap);
                await _unitOfWork.JobAppliedDigiDocMap.UpdateAsync(JobAppliedDigiDocMap);
            }
            else
            {
                JobAppliedDigiDocMap = _mapper.Map<JobAppliedDigiDocMapPostmodel, JobAppliedDigiDocMap>(model);
                await _unitOfWork.JobAppliedDigiDocMap.AddAsync(JobAppliedDigiDocMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobAppliedDigiDocMap.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDigiDocMapSelectBoxDataViewModel), 200)]
        public async Task<JobAppliedDigiDocMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAppliedDigiDocMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDigiDocMapEditResponse), 200)]
        public async Task<JobAppliedDigiDocMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAppliedDigiDocMapEditResponse response = new JobAppliedDigiDocMapEditResponse();

            JobAppliedDigiDocMap JobAppliedDigiDocMap = await _unitOfWork.JobAppliedDigiDocMap.GetAsync(model.RowId);

            if (JobAppliedDigiDocMap != null)
            {
                response.Data = _mapper.Map<JobAppliedDigiDocMapEditModel>(JobAppliedDigiDocMap);
            }
            else
            {
                response.Data = new JobAppliedDigiDocMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAppliedDigiDocMap JobAppliedDigiDocMap = await _unitOfWork.JobAppliedDigiDocMap.GetAsync(model.RowId);
            if (JobAppliedDigiDocMap == null)
            {
                viewModel.CreateFailureResponse("Document mapping not found.");
                return viewModel;
            }

            JobAppliedDigiDocMap.IsActive = false;
            await _unitOfWork.JobAppliedDigiDocMap.UpdateAsync(JobAppliedDigiDocMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

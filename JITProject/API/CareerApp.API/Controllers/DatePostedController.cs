﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DatePostedController : BaseController
    {
        private readonly IMapper _mapper;

        public DatePostedController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DatePostedController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DatePostedDataResultModel), 200)]
        public async Task<DatePostedDataResultModel> GetAllActiveAsync([FromBody]DatePostedDataRequestModel model)
        {
            IPagedList<DatePosted> data = await _unitOfWork.DatePosted.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<DatePostedDataResultModel, DatePostedInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(DatePostedDataResultModel), 200)]
        public async Task<DatePostedDataResultModel> GetAllInactiveAsync([FromBody]DatePostedDataRequestModel model)
        {
            IPagedList<DatePosted> data = await _unitOfWork.DatePosted.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<DatePostedDataResultModel, DatePostedInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DatePostedEditModel), 200)]
        public async Task<DatePostedEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            DatePosted item = await _unitOfWork.DatePosted.GetAsync(model.RowId);
            DatePostedEditModel viewModel = _mapper.Map<DatePostedEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] DatePostedPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DatePosted item = await _unitOfWork.DatePosted.GetFirstOrDefaultAsync(x => x.Day.Equals(model.Day) && x.Month.Equals(model.Month) && x.Year.Equals(model.Year) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("DatePosted is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.DatePosted.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.DatePosted.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<DatePostedPostmodel, DatePosted>(model);
                await _unitOfWork.DatePosted.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            DatePosted item = await _unitOfWork.DatePosted.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("DatePosted not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.DatePosted.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class LanguageController : BaseController
    {
        private readonly IMapper _mapper;

        public LanguageController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<LanguageController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(LanguageDataResultModel), 200)]
        public async Task<LanguageDataResultModel> GetAllActiveAsync([FromBody]LanguageDataRequestModel model)
        {
            IPagedList<Language> data = await _unitOfWork.Languages.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<LanguageDataResultModel, LanguageInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(LanguageDataResultModel), 200)]
        public async Task<LanguageDataResultModel> GetAllInactiveAsync([FromBody]LanguageDataRequestModel model)
        {
            IPagedList<Language> data = await _unitOfWork.Languages.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<LanguageDataResultModel, LanguageInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(LanguageEditModel), 200)]
        public async Task<LanguageEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Language item = await _unitOfWork.Languages.GetAsync(model.RowId);
            LanguageEditModel viewModel = _mapper.Map<LanguageEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] LanguagePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Language item = await _unitOfWork.Languages.GetFirstOrDefaultAsync(x => x.Name.ToLower().Equals(model.Name.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Language is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Languages.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Languages.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<LanguagePostmodel, Language>(model);
                await _unitOfWork.Languages.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Language item = await _unitOfWork.Languages.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Language not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Languages.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

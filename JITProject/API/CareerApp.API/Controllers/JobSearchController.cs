﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class JobSearchController : BaseController
    {
        private readonly IMapper _mapper;

        public JobSearchController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobPostController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> GetAllActiveAsync([FromBody]JobListDataRequestModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }

        [Route("sa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> SearchAsync([FromBody]BaseSearchMultipleViewModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.SearchMultipleAsync(model);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }
    }
}

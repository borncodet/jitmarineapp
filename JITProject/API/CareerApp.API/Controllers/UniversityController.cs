﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class UniversityController : BaseController
    {
        private readonly IMapper _mapper;

        public UniversityController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<UniversityController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(UniversityDataResultModel), 200)]
        public async Task<UniversityDataResultModel> GetAllActiveAsync([FromBody]UniversityDataRequestModel model)
        {
            IPagedList<UniversityInfo> data = await _unitOfWork.Universities.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<UniversityDataResultModel, UniversityInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] UniversityPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            University University = await _unitOfWork.Universities.GetFirstOrDefaultAsync(x => x.Name.Equals(model.Name) && x.IsActive == true);

            if (University != null && University.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("University already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                University = await _unitOfWork.Universities.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (University != null)
            {
                University = _mapper.Map(model, University);
                await _unitOfWork.Universities.UpdateAsync(University);
            }
            else
            {
                University = _mapper.Map<UniversityPostmodel, University>(model);
                await _unitOfWork.Universities.AddAsync(University);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = University.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(UniversitySelectBoxDataViewModel), 200)]
        public async Task<UniversitySelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.Universities.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(UniversityEditResponse), 200)]
        public async Task<UniversityEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            UniversityEditResponse response = new UniversityEditResponse();

            University University = await _unitOfWork.Universities.GetAsync(model.RowId);

            if (University != null)
            {
                response.Data = _mapper.Map<UniversityEditModel>(University);
            }
            else
            {
                response.Data = new UniversityEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            University University = await _unitOfWork.Universities.GetAsync(model.RowId);
            if (University == null)
            {
                viewModel.CreateFailureResponse("University not found.");
                return viewModel;
            }

            University.IsActive = false;
            await _unitOfWork.Universities.UpdateAsync(University);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

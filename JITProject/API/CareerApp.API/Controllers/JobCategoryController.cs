﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobCategoryController : BaseController
    {
        private readonly IMapper _mapper;

        public JobCategoryController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobCategoryController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobCategoryDataResultModel), 200)]
        public async Task<JobCategoryDataResultModel> GetAllActiveAsync([FromBody]JobCategoryDataRequestModel model)
        {
            IPagedList<JobCategory> data = await _unitOfWork.JobCategories.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<JobCategoryDataResultModel, JobCategoryInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(JobCategoryDataResultModel), 200)]
        public async Task<JobCategoryDataResultModel> GetAllInactiveAsync([FromBody]JobCategoryDataRequestModel model)
        {
            IPagedList<JobCategory> data = await _unitOfWork.JobCategories.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<JobCategoryDataResultModel, JobCategoryInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobCategoryEditModel), 200)]
        public async Task<JobCategoryEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            JobCategory item = await _unitOfWork.JobCategories.GetAsync(model.RowId);
            JobCategoryEditModel viewModel = _mapper.Map<JobCategoryEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobCategoryPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobCategory item = await _unitOfWork.JobCategories.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Job Category is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.JobCategories.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.JobCategories.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<JobCategoryPostmodel, JobCategory>(model);
                await _unitOfWork.JobCategories.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobCategory item = await _unitOfWork.JobCategories.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Job Category not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.JobCategories.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

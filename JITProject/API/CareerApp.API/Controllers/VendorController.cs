﻿/// <summary>
/// APIs to manage Vendor
/// </summary>
using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class VendorController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ILogger<VendorController> _logger;

        /// <summary>
        /// Vendor Constructor
        /// Dependency Injection is done here - used parameter based dependency injection
        /// </summary>
        public VendorController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<VendorController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Get all active vendor list
        /// Api path will be api/vendor/gaaa
        /// </summary>
        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorDataResultModel), 200)]
        public async Task<VendorDataResultModel> GetAllActiveAsync([FromBody]DataSourceVendorRequestModel model)
        {
            _logger.LogInformation("Calling api/vendor/gaaa api to get all active vendor list.");
            IPagedList<VendorInfo> data = await _unitOfWork.Vendors.GetAllAsync(model);
            return PopulateResponseWithoutMap<VendorDataResultModel, VendorInfo>(data);
        }

        /// <summary>
        /// Get vendor id by user id
        /// Api path will be api/vendor/gc
        /// </summary>
        [Route("gv/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(int), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<int> GetVendorIdAsync(int id)
        {
            _logger.LogInformation("Calling api/vendor/gc api to get vendor id.");
            var vendorId = await _unitOfWork.Vendors.GetVendorIdAsync(id);
            return vendorId;
        }

        /// <summary>
        /// Create or update vendor
        /// Api path will be api/vendor/coea
        /// </summary>
        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] VendorPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();
            Vendor Vendor = new Vendor();

            _logger.LogInformation("Check whether vendor name, country code, phone number and email is empty or not.");
            if (string.IsNullOrEmpty(model.VendorName) || string.IsNullOrWhiteSpace(model.VendorName))
            {
                viewModel.CreateFailureResponse("Please enter valid vendor name!");
                return viewModel;
            }
            else if (string.IsNullOrEmpty(model.CountryCode) || string.IsNullOrWhiteSpace(model.CountryCode))
            {
                viewModel.CreateFailureResponse("Please enter valid country code!");
                return viewModel;
            }
            else if (string.IsNullOrEmpty(model.PhoneNumber) || string.IsNullOrWhiteSpace(model.PhoneNumber))
            {
                viewModel.CreateFailureResponse("Please enter valid phone number!");
                return viewModel;
            }
            else if (string.IsNullOrEmpty(model.Email) || string.IsNullOrWhiteSpace(model.Email))
            {
                viewModel.CreateFailureResponse("Please enter valid email!");
                return viewModel;
            }

            _logger.LogInformation("Check whether organisation is empty or not.");
            if (!string.IsNullOrEmpty(model.Organisation) || !string.IsNullOrWhiteSpace(model.Organisation))
            {
                _logger.LogInformation("Gets vendor details by organisation.");
                Vendor = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.Organisation.ToLower().Equals(model.Organisation.ToLower()) && x.IsActive == true);
            }

            _logger.LogInformation("Check whether organisation already exist or not.");
            if (Vendor != null && Vendor.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Organisation is already exist!");
                return viewModel;
            }

            _logger.LogInformation("Gets vendor details by email.");
            Vendor = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.Email.ToLower().Equals(model.Email.ToLower()) && x.RowId != model.RowId && x.IsActive == true);
            if (Vendor != null)
            {
                _logger.LogWarning("Create a failure message - email already exist.");
                viewModel.CreateFailureResponse("Email is already exist!");
                return viewModel;
            }

            _logger.LogInformation("Gets vendor details by phone.");
            Vendor = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.PhoneNumber.ToLower().Equals(model.PhoneNumber.ToLower()) && x.CountryCode.ToLower().Equals(model.CountryCode.ToLower()) && x.RowId != model.RowId && x.IsActive == true);
            if (Vendor != null)
            {
                _logger.LogWarning("Create a failure message - phone already exist.");
                viewModel.CreateFailureResponse("Phone is already exist!");
                return viewModel;
            }

            //Uploading Licence or Registration Document
            if (model.Document != null)
            {
                _logger.LogInformation("Uploading Licence or Registration Document.");
                model.VendorDocument = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\VendorDocument", model.Document);
            }

            if (model.RowId != 0)
            {
                _logger.LogInformation("Gets vendor details by RowId.");
                Vendor = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (Vendor != null)
            {
                _logger.LogInformation("Update vendor details.");
                Vendor = _mapper.Map(model, Vendor);
                await _unitOfWork.Vendors.UpdateAsync(Vendor);
            }
            else
            {
                _logger.LogInformation("Maps vendor post model with vendor model.");
                Vendor = _mapper.Map<VendorPostmodel, Vendor>(model);
                _logger.LogInformation("Add vendor entity to dbcontext.");
                await _unitOfWork.Vendors.AddAsync(Vendor);
            }

            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = Vendor.RowId;
            _logger.LogInformation("Create success response.");
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        /// <summary>
        /// Get the lists of items going to be used by vendor page
        /// Api path will be api/vendor/get-selectbox-data
        /// </summary>
        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorSelectBoxDataViewModel), 200)]
        public async Task<VendorSelectBoxDataViewModel> GetSelectboxData()
        {
            _logger.LogInformation("Gets all the dropdown list to be displayed in the vendor page.");
            return await _unitOfWork.Vendors.GetSelectBoxData();
        }

        /// <summary>
        /// Get vendor details by id
        /// Api path will be api/vendor/ga
        /// </summary>
        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorEditResponse), 200)]
        public async Task<VendorEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            VendorEditResponse response = new VendorEditResponse();

            _logger.LogInformation("Gets vendor details by RowId.");
            Vendor Vendor = await _unitOfWork.Vendors.GetAsync(model.RowId);

            if (Vendor != null)
            {
                _logger.LogInformation("Sets the retrieved vendor details to the response data.");
                response.Data = _mapper.Map<VendorEditModel>(Vendor);
            }
            else
            {
                response.Data = new VendorEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        /// <summary>
        /// Delete vendor by id
        /// Api path will be api/vendor/da
        /// </summary>
        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            _logger.LogInformation("Gets vendor details by RowId.");
            Vendor Vendor = await _unitOfWork.Vendors.GetAsync(model.RowId);

            if (Vendor == null)
            {
                _logger.LogWarning("Create a failure message - Vendor not found.");
                viewModel.CreateFailureResponse("Vendor not found.");
                return viewModel;
            }

            _logger.LogInformation("Update vendor details - change vendor's IsActive field to false.");
            Vendor.IsActive = false;
            await _unitOfWork.Vendors.UpdateAsync(Vendor);
            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

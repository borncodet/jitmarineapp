﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ResumeCandidateMapController : BaseController
    {
        private readonly IMapper _mapper;

        public ResumeCandidateMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ResumeCandidateMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeCandidateMapDataResultModel), 200)]
        public async Task<ResumeCandidateMapDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<ResumeCandidateMapInfo> data = await _unitOfWork.ResumeCandidateMap.GetAllAsync(model);
            return PopulateResponseWithoutMap<ResumeCandidateMapDataResultModel, ResumeCandidateMapInfo>(data);
        }

        [Route("gcaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeCandidateMapTemplateDataResultModel), 200)]
        public async Task<ResumeCandidateMapTemplateDataResultModel> GetCandidateAllActiveAsync([FromBody] DataSourceCandidateRequestModel model)
        {
            IPagedList<ResumeCandidateMapTemplateInfo> data = await _unitOfWork.ResumeCandidateMap.GetCandidateAllAsync(model);
            return PopulateResponseWithoutMap<ResumeCandidateMapTemplateDataResultModel, ResumeCandidateMapTemplateInfo>(data);
        }

        [Route("gcra")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeContent), 200)]
        public async Task<ResumeContent> GetResumeAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            ResumeContent data = await _unitOfWork.ResumeCandidateMap.GetResumeAsync(model);
            return data;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] ResumeCandidateMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ResumeCandidateMap ResumeCandidateMap = await _unitOfWork.ResumeCandidateMap.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.ResumeName.Equals(model.ResumeName) && x.IsActive == true);

            if (ResumeCandidateMap != null && ResumeCandidateMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Resume name  already exist!");
                return viewModel;
            }

            //Uploading Resume Files 
            if (model.ResumeFileDocument != null)
            {
                model.ResumeFile = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\ResumeFile\", model.ResumeFileDocument);
            }
            //Uploading Image Files 
            if (model.ResumeFileDocument != null)
            {
                model.ResumeImage = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\ResumeImage\", model.ResumeImageDocument);
            }

            if (model.RowId != 0)
            {
                ResumeCandidateMap = await _unitOfWork.ResumeCandidateMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (ResumeCandidateMap != null)
            {
                ResumeCandidateMap = _mapper.Map(model, ResumeCandidateMap);
                await _unitOfWork.ResumeCandidateMap.UpdateAsync(ResumeCandidateMap);
            }
            else
            {
                ResumeCandidateMap = _mapper.Map<ResumeCandidateMapPostmodel, ResumeCandidateMap>(model);
                await _unitOfWork.ResumeCandidateMap.AddAsync(ResumeCandidateMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ResumeCandidateMap.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeCandidateMapSelectBoxDataViewModel), 200)]
        public async Task<ResumeCandidateMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.ResumeCandidateMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeCandidateMapEditResponse), 200)]
        public async Task<ResumeCandidateMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            ResumeCandidateMapEditResponse response = new ResumeCandidateMapEditResponse();

            ResumeCandidateMap ResumeCandidateMap = await _unitOfWork.ResumeCandidateMap.GetAsync(model.RowId);

            if (ResumeCandidateMap != null)
            {
                response.Data = _mapper.Map<ResumeCandidateMapEditModel>(ResumeCandidateMap);
            }
            else
            {
                response.Data = new ResumeCandidateMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ResumeCandidateMap ResumeCandidateMap = await _unitOfWork.ResumeCandidateMap.GetAsync(model.RowId);
            if (ResumeCandidateMap == null)
            {
                viewModel.CreateFailureResponse("Candidate mapped resume not found.");
                return viewModel;
            }

            ResumeCandidateMap.IsActive = false;
            await _unitOfWork.ResumeCandidateMap.UpdateAsync(ResumeCandidateMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

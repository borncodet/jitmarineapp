﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobByAdminController : BaseController
    {
        private readonly IMapper _mapper;

        public JobByAdminController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobByAdminController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobByAdminDataResultModel), 200)]
        public async Task<JobByAdminDataResultModel> GetAllActiveAsync([FromBody]JobByAdminDataRequestModel model)
        {
            IPagedList<JobByAdminInfo> data = await _unitOfWork.JobByAdmin.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobByAdminDataResultModel, JobByAdminInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobByAdminPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobByAdmin JobByAdmin = await _unitOfWork.JobByAdmin.GetFirstOrDefaultAsync(x => x.JobId == model.JobId && x.EmployeeId == model.EmployeeId && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobByAdmin != null && JobByAdmin.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Mapping is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobByAdmin = await _unitOfWork.JobByAdmin.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobByAdmin != null)
            {
                JobByAdmin = _mapper.Map(model, JobByAdmin);
                await _unitOfWork.JobByAdmin.UpdateAsync(JobByAdmin);
            }
            else
            {
                JobByAdmin = _mapper.Map<JobByAdminPostmodel, JobByAdmin>(model);
                await _unitOfWork.JobByAdmin.AddAsync(JobByAdmin);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobByAdmin.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobByAdminSelectBoxDataViewModel), 200)]
        public async Task<JobByAdminSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobByAdmin.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobByAdminEditResponse), 200)]
        public async Task<JobByAdminEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobByAdminEditResponse response = new JobByAdminEditResponse();

            JobByAdmin JobByAdmin = await _unitOfWork.JobByAdmin.GetAsync(model.RowId);

            if (JobByAdmin != null)
            {
                response.Data = _mapper.Map<JobByAdminEditModel>(JobByAdmin);
            }
            else
            {
                response.Data = new JobByAdminEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobByAdmin JobByAdmin = await _unitOfWork.JobByAdmin.GetAsync(model.RowId);
            if (JobByAdmin == null)
            {
                viewModel.CreateFailureResponse("Mapping not found.");
                return viewModel;
            }

            JobByAdmin.IsActive = false;
            await _unitOfWork.JobByAdmin.UpdateAsync(JobByAdmin);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

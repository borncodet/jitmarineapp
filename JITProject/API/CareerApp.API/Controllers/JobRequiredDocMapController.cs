﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobRequiredDocMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobRequiredDocMapController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobRequiredDocMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobRequiredDocMapDataResultModel), 200)]
        public async Task<JobRequiredDocMapDataResultModel> GetAllActiveAsync([FromBody]JobRequiredDocMapDataRequestModel model)
        {
            IPagedList<JobRequiredDocMapInfo> data = await _unitOfWork.JobRequiredDocMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobRequiredDocMapDataResultModel, JobRequiredDocMapInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobRequiredDocMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobRequiredDocMap JobRequiredDocMap = await _unitOfWork.JobRequiredDocMap.GetFirstOrDefaultAsync(x => x.JobId == model.JobId && x.RequiredDocId == model.RequiredDocId && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobRequiredDocMap != null && JobRequiredDocMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Mapping is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobRequiredDocMap = await _unitOfWork.JobRequiredDocMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobRequiredDocMap != null)
            {
                JobRequiredDocMap = _mapper.Map(model, JobRequiredDocMap);
                await _unitOfWork.JobRequiredDocMap.UpdateAsync(JobRequiredDocMap);
            }
            else
            {
                JobRequiredDocMap = _mapper.Map<JobRequiredDocMapPostmodel, JobRequiredDocMap>(model);
                await _unitOfWork.JobRequiredDocMap.AddAsync(JobRequiredDocMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobRequiredDocMap.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobRequiredDocMapSelectBoxDataViewModel), 200)]
        public async Task<JobRequiredDocMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobRequiredDocMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobRequiredDocMapEditResponse), 200)]
        public async Task<JobRequiredDocMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobRequiredDocMapEditResponse response = new JobRequiredDocMapEditResponse();

            JobRequiredDocMap JobRequiredDocMap = await _unitOfWork.JobRequiredDocMap.GetAsync(model.RowId);

            if (JobRequiredDocMap != null)
            {
                response.Data = _mapper.Map<JobRequiredDocMapEditModel>(JobRequiredDocMap);
            }
            else
            {
                response.Data = new JobRequiredDocMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobRequiredDocMap JobRequiredDocMap = await _unitOfWork.JobRequiredDocMap.GetAsync(model.RowId);
            if (JobRequiredDocMap == null)
            {
                viewModel.CreateFailureResponse("Mapping not found.");
                return viewModel;
            }

            JobRequiredDocMap.IsActive = false;
            await _unitOfWork.JobRequiredDocMap.UpdateAsync(JobRequiredDocMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

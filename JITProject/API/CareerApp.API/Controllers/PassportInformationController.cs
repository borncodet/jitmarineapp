﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class PassportInformationController : BaseController
    {
        private readonly IMapper _mapper;

        public PassportInformationController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<PassportInformationController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(PassportInformationDataResultModel), 200)]
        public async Task<PassportInformationDataResultModel> GetAllActiveAsync([FromBody]PassportInformationDataRequestModel model)
        {
            IPagedList<PassportInformation> data = await _unitOfWork.PassportInformations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<PassportInformationDataResultModel, PassportInformationInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(PassportInformationDataResultModel), 200)]
        public async Task<PassportInformationDataResultModel> GetAllInactiveAsync([FromBody]PassportInformationDataRequestModel model)
        {
            IPagedList<PassportInformation> data = await _unitOfWork.PassportInformations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<PassportInformationDataResultModel, PassportInformationInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(PassportInformationEditModel), 200)]
        public async Task<PassportInformationEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            PassportInformation item = await _unitOfWork.PassportInformations.GetAsync(model.RowId);
            PassportInformationEditModel viewModel = _mapper.Map<PassportInformationEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] PassportInformationPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            PassportInformation item = await _unitOfWork.PassportInformations.GetFirstOrDefaultAsync(x => x.PassportNo.ToLower().Equals(model.PassportNo.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("PassportInformation is already exist!");
                return viewModel;
            }

            ////Uploading Files 
            //if (model.Document != null)
            //{
            //    model.Passport = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\Passport\", model.Document);
            //}

            if (model.RowId != 0)
            {
                item = await _unitOfWork.PassportInformations.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.PassportInformations.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<PassportInformationPostmodel, PassportInformation>(model);
                await _unitOfWork.PassportInformations.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            PassportInformation item = await _unitOfWork.PassportInformations.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("PassportInformation not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.PassportInformations.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

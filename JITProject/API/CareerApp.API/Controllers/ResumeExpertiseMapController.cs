﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ResumeExpertiseMapController : BaseController
    {
        private readonly IMapper _mapper;

        public ResumeExpertiseMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ResumeExpertiseMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeExpertiseMapDataResultModel), 200)]
        public async Task<ResumeExpertiseMapDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<ResumeExpertiseMapInfo> data = await _unitOfWork.ResumeExpertiseMap.GetAllAsync(model);
            return PopulateResponseWithoutMap<ResumeExpertiseMapDataResultModel, ResumeExpertiseMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ResumeExpertiseMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ResumeExpertiseMap ResumeExpertiseMap = await _unitOfWork.ResumeExpertiseMap.GetFirstOrDefaultAsync(x => x.FieldOfExpertiseId.Equals(model.FieldOfExpertiseId) && x.ResumeTemplateId.Equals(model.ResumeTemplateId) && x.IsActive == true);

            if (ResumeExpertiseMap != null && ResumeExpertiseMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Expertise already mapped to the specified resume!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                ResumeExpertiseMap = await _unitOfWork.ResumeExpertiseMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (ResumeExpertiseMap != null)
            {
                ResumeExpertiseMap = _mapper.Map(model, ResumeExpertiseMap);
                await _unitOfWork.ResumeExpertiseMap.UpdateAsync(ResumeExpertiseMap);
            }
            else
            {
                ResumeExpertiseMap = _mapper.Map<ResumeExpertiseMapPostmodel, ResumeExpertiseMap>(model);
                await _unitOfWork.ResumeExpertiseMap.AddAsync(ResumeExpertiseMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ResumeExpertiseMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeExpertiseMapSelectBoxDataViewModel), 200)]
        public async Task<ResumeExpertiseMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.ResumeExpertiseMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeExpertiseMapEditResponse), 200)]
        public async Task<ResumeExpertiseMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            ResumeExpertiseMapEditResponse response = new ResumeExpertiseMapEditResponse();

            ResumeExpertiseMap ResumeExpertiseMap = await _unitOfWork.ResumeExpertiseMap.GetAsync(model.RowId);

            if (ResumeExpertiseMap != null)
            {
                response.Data = _mapper.Map<ResumeExpertiseMapEditModel>(ResumeExpertiseMap);
            }
            else
            {
                response.Data = new ResumeExpertiseMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ResumeExpertiseMap ResumeExpertiseMap = await _unitOfWork.ResumeExpertiseMap.GetAsync(model.RowId);
            if (ResumeExpertiseMap == null)
            {
                viewModel.CreateFailureResponse("Expertise mapped resume not found.");
                return viewModel;
            }

            ResumeExpertiseMap.IsActive = false;
            await _unitOfWork.ResumeExpertiseMap.UpdateAsync(ResumeExpertiseMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

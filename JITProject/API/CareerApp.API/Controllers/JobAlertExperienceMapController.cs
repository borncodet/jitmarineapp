﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAlertExperienceMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAlertExperienceMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAlertExperienceMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertExperienceMapDataResultModel), 200)]
        public async Task<JobAlertExperienceMapDataResultModel> GetAllActiveAsync([FromBody]JobAlertExperienceMapDataRequestModel model)
        {
            IPagedList<JobAlertExperienceMapInfo> data = await _unitOfWork.JobAlertExperienceMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobAlertExperienceMapDataResultModel, JobAlertExperienceMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAlertExperienceMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAlertExperienceMap JobAlertExperienceMap = await _unitOfWork.JobAlertExperienceMap.GetFirstOrDefaultAsync(x => x.JobAlertId.Equals(model.JobAlertId) && x.DurationId.Equals(model.DurationId) && x.IsActive == true);

            if (JobAlertExperienceMap != null && JobAlertExperienceMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Duration already mapped to the alert.");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobAlertExperienceMap = await _unitOfWork.JobAlertExperienceMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobAlertExperienceMap != null)
            {
                JobAlertExperienceMap = _mapper.Map(model, JobAlertExperienceMap);
                await _unitOfWork.JobAlertExperienceMap.UpdateAsync(JobAlertExperienceMap);
            }
            else
            {
                JobAlertExperienceMap = _mapper.Map<JobAlertExperienceMapPostmodel, JobAlertExperienceMap>(model);
                await _unitOfWork.JobAlertExperienceMap.AddAsync(JobAlertExperienceMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobAlertExperienceMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertExperienceMapSelectBoxDataViewModel), 200)]
        public async Task<JobAlertExperienceMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAlertExperienceMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertExperienceMapEditResponse), 200)]
        public async Task<JobAlertExperienceMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAlertExperienceMapEditResponse response = new JobAlertExperienceMapEditResponse();

            JobAlertExperienceMap JobAlertExperienceMap = await _unitOfWork.JobAlertExperienceMap.GetAsync(model.RowId);

            if (JobAlertExperienceMap != null)
            {
                response.Data = _mapper.Map<JobAlertExperienceMapEditModel>(JobAlertExperienceMap);
            }
            else
            {
                response.Data = new JobAlertExperienceMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAlertExperienceMap JobAlertExperienceMap = await _unitOfWork.JobAlertExperienceMap.GetAsync(model.RowId);
            if (JobAlertExperienceMap == null)
            {
                viewModel.CreateFailureResponse("Duration alert map not found.");
                return viewModel;
            }

            JobAlertExperienceMap.IsActive = false;
            await _unitOfWork.JobAlertExperienceMap.UpdateAsync(JobAlertExperienceMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

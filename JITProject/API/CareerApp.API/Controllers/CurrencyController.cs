﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CurrencyController : BaseController
    {
        private readonly IMapper _mapper;

        public CurrencyController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CurrencyController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CurrencyDataResultModel), 200)]
        public async Task<CurrencyDataResultModel> GetAllActiveAsync([FromBody]CurrencyDataRequestModel model)
        {
            IPagedList<Currency> data = await _unitOfWork.Currencies.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<CurrencyDataResultModel, CurrencyInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(CurrencyDataResultModel), 200)]
        public async Task<CurrencyDataResultModel> GetAllInactiveAsync([FromBody]CurrencyDataRequestModel model)
        {
            IPagedList<Currency> data = await _unitOfWork.Currencies.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<CurrencyDataResultModel, CurrencyInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CurrencyEditModel), 200)]
        public async Task<CurrencyEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Currency item = await _unitOfWork.Currencies.GetAsync(model.RowId);
            CurrencyEditModel viewModel = _mapper.Map<CurrencyEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CurrencyPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Currency item = await _unitOfWork.Currencies.GetFirstOrDefaultAsync(x => x.Name.ToLower().Equals(model.Name.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Currency is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Currencies.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Currencies.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<CurrencyPostmodel, Currency>(model);
                await _unitOfWork.Currencies.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Currency item = await _unitOfWork.Currencies.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Currency not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Currencies.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

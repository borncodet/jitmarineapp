﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class IndustryController : BaseController
    {
        private readonly IMapper _mapper;

        public IndustryController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<IndustryController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(IndustryDataResultModel), 200)]
        public async Task<IndustryDataResultModel> GetAllActiveAsync([FromBody]IndustryDataRequestModel model)
        {
            IPagedList<Industry> data = await _unitOfWork.Industries.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<IndustryDataResultModel, IndustryInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(IndustryDataResultModel), 200)]
        public async Task<IndustryDataResultModel> GetAllInactiveAsync([FromBody]IndustryDataRequestModel model)
        {
            IPagedList<Industry> data = await _unitOfWork.Industries.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<IndustryDataResultModel, IndustryInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(IndustryEditModel), 200)]
        public async Task<IndustryEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Industry item = await _unitOfWork.Industries.GetAsync(model.RowId);
            IndustryEditModel viewModel = _mapper.Map<IndustryEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] IndustryPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Industry item = await _unitOfWork.Industries.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Industry is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Industries.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Industries.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<IndustryPostmodel, Industry>(model);
                await _unitOfWork.Industries.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Industry item = await _unitOfWork.Industries.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Industry not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Industries.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

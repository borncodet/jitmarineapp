﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class SocialAccountsController : BaseController
    {
        private readonly IMapper _mapper;

        public SocialAccountsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<SocialAccountsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(SocialAccountsDataResultModel), 200)]
        public async Task<SocialAccountsDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<SocialAccountsInfo> data = await _unitOfWork.SocialAccounts.GetAllAsync(model);
            return PopulateResponseWithoutMap<SocialAccountsDataResultModel, SocialAccountsInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] SocialAccountsPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            SocialAccounts SocialAccounts = await _unitOfWork.SocialAccounts.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && (
                                                                                                         x.Facebooks.Equals(model.Facebooks) ||
                                                                                                         x.Google.Equals(model.Google) ||
                                                                                                         x.Twitter.Equals(model.Twitter) ||
                                                                                                         x.LinkedIn.Equals(model.LinkedIn) ||
                                                                                                         x.Pinterest.Equals(model.Pinterest) ||
                                                                                                         x.Instagram.Equals(model.Instagram) ||
                                                                                                         x.Other.Equals(model.Other)) && x.IsActive == true);

            if (SocialAccounts != null && SocialAccounts.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Social account already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                SocialAccounts = await _unitOfWork.SocialAccounts.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (SocialAccounts != null)
            {
                SocialAccounts = _mapper.Map(model, SocialAccounts);
                await _unitOfWork.SocialAccounts.UpdateAsync(SocialAccounts);
            }
            else
            {
                SocialAccounts = _mapper.Map<SocialAccountsPostmodel, SocialAccounts>(model);
                await _unitOfWork.SocialAccounts.AddAsync(SocialAccounts);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = SocialAccounts.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(SocialAccountsSelectBoxDataViewModel), 200)]
        public async Task<SocialAccountsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.SocialAccounts.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(SocialAccountsEditResponse), 200)]
        public async Task<SocialAccountsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            SocialAccountsEditResponse response = new SocialAccountsEditResponse();

            SocialAccounts SocialAccounts = await _unitOfWork.SocialAccounts.GetAsync(model.RowId);

            if (SocialAccounts != null)
            {
                response.Data = _mapper.Map<SocialAccountsEditModel>(SocialAccounts);
            }
            else
            {
                response.Data = new SocialAccountsEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            SocialAccounts SocialAccounts = await _unitOfWork.SocialAccounts.GetAsync(model.RowId);
            if (SocialAccounts == null)
            {
                viewModel.CreateFailureResponse("Social account not found.");
                return viewModel;
            }

            SocialAccounts.IsActive = false;
            await _unitOfWork.SocialAccounts.UpdateAsync(SocialAccounts);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

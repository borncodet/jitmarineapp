﻿using AutoMapper;
using CareerApp.API.Helpers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.Shared.Helpers;
using CareerApp.ViewModels.Shared;
using CareerApp.ViewModels.Shared.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected readonly IHttpContextAccessor _accessor;
        protected readonly IHostingEnvironment _env;
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IAccountManager _accountManager;
        protected readonly ILogger _logger;
        private readonly IMapper _mapper;

        public BaseController(
            IHttpContextAccessor accessor,
            IHostingEnvironment env,
            IUnitOfWork unitOfWork,
            IAccountManager accountManager,
            ILogger<BaseController> logger,
            IMapper mapper)
        {
            _accountManager = accountManager;
            _unitOfWork = unitOfWork;
            _accessor = accessor;
            _env = env;
            _logger = logger;
            _mapper = mapper;
        }

        protected string GetClientIp()
        {
            return Convert.ToString(_accessor.HttpContext?.Connection?.RemoteIpAddress);
        }

        [NonAction]
        public int GetCurrentUserId()
        {
            var userId = _accessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value?.Trim() ?? "0";
            //return int.Parse(_accessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value?.Trim());
            return int.Parse(userId);
        }

        protected async Task<Candidate> GetCurrentCandidateId()
        {
            var userId = GetCurrentUserId();
            return await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.UserId == userId);

        }

        protected async Task<Vendor> GetCurrentVendorId()
        {
            var userId = GetCurrentUserId();
            return await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.UserId == userId);

        }

        protected string GetCurrentUserRoleId()
        {
            return _accessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value?.Trim();
        }

        protected void LogAndCreateFailureResponse(Exception ex, IFailureResponse failureResponse, object model = null)
        {
            string errorRef = "";
            _logger.LogCritical(LoggingEvents.InternalError, ex, LoggingEvents.InternalError.Name, model, errorRef);

            failureResponse.CreateFailureResponse(string.Format(ErrorConstants.SometingWentWrongWithErrorRef, errorRef));
        }

        protected T PopulateResponseWithMap<T, U>(IPagedList data) where T : DataSourceResultModel<U>, new()
        {
            try
            {
                var response = new T
                {
                    //Data = Mapper.Map<U[]>(data),
                    Data = _mapper.Map<U[]>(data),
                    CurrentPage = data.PageIndex,
                    CurrentPageSize = data.PageSize,
                    HasNext = data.HasNextPage,
                    HasPreviousPage = data.HasPreviousPage,
                    Total = data.TotalCount
                };
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected T PopulateResponseWithoutMap<T, U>(IPagedList<U> data) where T : DataSourceResultModel<U>, new()
        {
            var response = new T
            {
                Data = data.ToArray(),
                CurrentPage = data.PageIndex,
                CurrentPageSize = data.PageSize,
                HasNext = data.HasNextPage,
                HasPreviousPage = data.HasPreviousPage,
                Total = data.TotalCount
            };
            return response;
        }
    }
}

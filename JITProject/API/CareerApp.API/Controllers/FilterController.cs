﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class FilterController : BaseController
    {
        private readonly IMapper _mapper;

        public FilterController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<FilterController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("jta")]
        [HttpPost]
        [ProducesResponseType(typeof(JobTypeDataResultModel), 200)]
        public async Task<JobTypeDataResultModel> GetAllJobTypeAsync([FromBody]JobTypeDataRequestModel model)
        {
            IPagedList<JobType> data = await _unitOfWork.JobTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<JobTypeDataResultModel, JobTypeInfo>(data);
        }

        [Route("eta")]
        [HttpPost]
        [ProducesResponseType(typeof(ExpereinceTypeDataResultModel), 200)]
        public async Task<ExpereinceTypeDataResultModel> GetAllExpereinceTypeAsync([FromBody]ExpereinceTypeDataRequestModel model)
        {
            IPagedList<ExpereinceType> data = await _unitOfWork.ExpereinceTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<ExpereinceTypeDataResultModel, ExpereinceTypeInfo>(data);
        }

        [Route("dpa")]
        [HttpPost]
        [ProducesResponseType(typeof(DatePostedDataResultModel), 200)]
        public async Task<DatePostedDataResultModel> GetAllActiveAsync([FromBody]DatePostedDataRequestModel model)
        {
            IPagedList<DatePosted> data = await _unitOfWork.DatePosted.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<DatePostedDataResultModel, DatePostedInfo>(data);
        }
    }
}

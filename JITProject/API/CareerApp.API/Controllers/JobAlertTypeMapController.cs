﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAlertTypeMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAlertTypeMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAlertTypeMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertTypeMapDataResultModel), 200)]
        public async Task<JobAlertTypeMapDataResultModel> GetAllActiveAsync([FromBody]JobAlertTypeMapDataRequestModel model)
        {
            IPagedList<JobAlertTypeMapInfo> data = await _unitOfWork.JobAlertTypeMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobAlertTypeMapDataResultModel, JobAlertTypeMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAlertTypeMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAlertTypeMap JobAlertTypeMap = await _unitOfWork.JobAlertTypeMap.GetFirstOrDefaultAsync(x => x.JobAlertId.Equals(model.JobAlertId) && x.JobTypeId.Equals(model.JobTypeId) && x.IsActive == true);

            if (JobAlertTypeMap != null && JobAlertTypeMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Job type already mapped to the alert.");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobAlertTypeMap = await _unitOfWork.JobAlertTypeMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobAlertTypeMap != null)
            {
                JobAlertTypeMap = _mapper.Map(model, JobAlertTypeMap);
                await _unitOfWork.JobAlertTypeMap.UpdateAsync(JobAlertTypeMap);
            }
            else
            {
                JobAlertTypeMap = _mapper.Map<JobAlertTypeMapPostmodel, JobAlertTypeMap>(model);
                await _unitOfWork.JobAlertTypeMap.AddAsync(JobAlertTypeMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobAlertTypeMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertTypeMapSelectBoxDataViewModel), 200)]
        public async Task<JobAlertTypeMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAlertTypeMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertTypeMapEditResponse), 200)]
        public async Task<JobAlertTypeMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAlertTypeMapEditResponse response = new JobAlertTypeMapEditResponse();

            JobAlertTypeMap JobAlertTypeMap = await _unitOfWork.JobAlertTypeMap.GetAsync(model.RowId);

            if (JobAlertTypeMap != null)
            {
                response.Data = _mapper.Map<JobAlertTypeMapEditModel>(JobAlertTypeMap);
            }
            else
            {
                response.Data = new JobAlertTypeMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAlertTypeMap JobAlertTypeMap = await _unitOfWork.JobAlertTypeMap.GetAsync(model.RowId);
            if (JobAlertTypeMap == null)
            {
                viewModel.CreateFailureResponse("Job type alert map not found.");
                return viewModel;
            }

            JobAlertTypeMap.IsActive = false;
            await _unitOfWork.JobAlertTypeMap.UpdateAsync(JobAlertTypeMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAppliedController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAppliedController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAppliedController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDataResultModel), 200)]
        public async Task<JobAppliedDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<JobAppliedInfo> data = await _unitOfWork.JobApplied.GetAllAsync(model);
            return PopulateResponseWithoutMap<JobAppliedDataResultModel, JobAppliedInfo>(data);
        }

        [Route("graj")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedCandidateDataResultModel), 200)]
        public async Task<JobAppliedCandidateDataResultModel> GetRecentlyAppliedJobAsync([FromBody]JobAppliedCandidateDataRequestModel model)
        {
            IPagedList<RecentlyAppliedJobInfo> data = await _unitOfWork.JobApplied.GetRecentlyAppliedJobAsync(model.CandidateId, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobAppliedCandidateDataResultModel, RecentlyAppliedJobInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAppliedPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobApplied JobApplied = await _unitOfWork.JobApplied.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.JobId.Equals(model.JobId) && x.IsActive == true);

            if (JobApplied != null && JobApplied.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate already applied for this job!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobApplied = await _unitOfWork.JobApplied.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobApplied != null)
            {
                JobApplied = _mapper.Map(model, JobApplied);
                await _unitOfWork.JobApplied.UpdateAsync(JobApplied);
            }
            else
            {
                JobApplied = _mapper.Map<JobAppliedPostmodel, JobApplied>(model);
                await _unitOfWork.JobApplied.AddAsync(JobApplied);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobApplied.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedSelectBoxDataViewModel), 200)]
        public async Task<JobAppliedSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobApplied.GetSelectBoxData();
        }

        [Route("gjac")]
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)]
        public async Task<int> GetJobAppliedCountAsync(BaseCandidateSearchViewModel model)
        {
            return await _unitOfWork.JobApplied.GetJobAppliedCountAsync(model);
        }

        [Route("gaca")]
        [HttpPost]
        [ProducesResponseType(typeof(CountInfo), 200)]
        public async Task<CountInfo> GetAllCountAsync(DataSourceCandidateRequestModel model)
        {
            return await _unitOfWork.JobApplied.GetCountAsync(model);
        }

        [Route("gpa")]
        [HttpPost]
        [ProducesResponseType(typeof(ProgressInfo), 200)]
        public async Task<ProgressInfo> GetProgressAsync(DataSourceCandidateRequestModel model)
        {
            return await _unitOfWork.JobApplied.GetProgressAsync(model);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedEditResponse), 200)]
        public async Task<JobAppliedEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAppliedEditResponse response = new JobAppliedEditResponse();

            JobApplied JobApplied = await _unitOfWork.JobApplied.GetAsync(model.RowId);

            if (JobApplied != null)
            {
                response.Data = _mapper.Map<JobAppliedEditModel>(JobApplied);
            }
            else
            {
                response.Data = new JobAppliedEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobApplied JobApplied = await _unitOfWork.JobApplied.GetAsync(model.RowId);
            if (JobApplied == null)
            {
                viewModel.CreateFailureResponse("Candidate applied job not found.");
                return viewModel;
            }

            JobApplied.IsActive = false;
            await _unitOfWork.JobApplied.UpdateAsync(JobApplied);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateRelativesController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateRelativesController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateRelativesController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateRelativesDataResultModel), 200)]
        public async Task<CandidateRelativesDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateRelativesInfo> data = await _unitOfWork.CandidateRelatives.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateRelativesDataResultModel, CandidateRelativesInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateRelativesPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateRelatives CandidateRelatives = await _unitOfWork.CandidateRelatives.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.FirstName.Equals(model.FirstName) && x.LastName.Equals(model.LastName) && x.PhoneNumber.Equals(model.PhoneNumber) && x.RelationshipId.Equals(model.RelationshipId) && x.IsActive == true);

            if (CandidateRelatives != null && CandidateRelatives.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate relative already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                CandidateRelatives = await _unitOfWork.CandidateRelatives.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateRelatives != null)
            {
                CandidateRelatives = _mapper.Map(model, CandidateRelatives);
                await _unitOfWork.CandidateRelatives.UpdateAsync(CandidateRelatives);
            }
            else
            {
                CandidateRelatives = _mapper.Map<CandidateRelativesPostmodel, CandidateRelatives>(model);
                await _unitOfWork.CandidateRelatives.AddAsync(CandidateRelatives);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateRelatives.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateRelativesSelectBoxDataViewModel), 200)]
        public async Task<CandidateRelativesSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateRelatives.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateRelativesEditResponse), 200)]
        public async Task<CandidateRelativesEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateRelativesEditResponse response = new CandidateRelativesEditResponse();

            CandidateRelatives CandidateRelatives = await _unitOfWork.CandidateRelatives.GetAsync(model.RowId);

            if (CandidateRelatives != null)
            {
                response.Data = _mapper.Map<CandidateRelativesEditModel>(CandidateRelatives);
            }
            else
            {
                response.Data = new CandidateRelativesEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateRelatives CandidateRelatives = await _unitOfWork.CandidateRelatives.GetAsync(model.RowId);
            if (CandidateRelatives == null)
            {
                viewModel.CreateFailureResponse("Candidate relative not found.");
                return viewModel;
            }

            CandidateRelatives.IsActive = false;
            await _unitOfWork.CandidateRelatives.UpdateAsync(CandidateRelatives);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

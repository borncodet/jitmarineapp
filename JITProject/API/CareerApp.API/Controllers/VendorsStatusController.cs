﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class VendorsStatusController : BaseController
    {
        private readonly IMapper _mapper;

        public VendorsStatusController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<VendorsStatusController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorsStatusDataResultModel), 200)]
        public async Task<VendorsStatusDataResultModel> GetAllActiveAsync([FromBody]VendorsStatusDataRequestModel model)
        {
            IPagedList<VendorsStatus> data = await _unitOfWork.VendorsStatus.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<VendorsStatusDataResultModel, VendorsStatusInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorsStatusDataResultModel), 200)]
        public async Task<VendorsStatusDataResultModel> GetAllInactiveAsync([FromBody]VendorsStatusDataRequestModel model)
        {
            IPagedList<VendorsStatus> data = await _unitOfWork.VendorsStatus.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<VendorsStatusDataResultModel, VendorsStatusInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorsStatusEditModel), 200)]
        public async Task<VendorsStatusEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            VendorsStatus item = await _unitOfWork.VendorsStatus.GetAsync(model.RowId);
            VendorsStatusEditModel viewModel = _mapper.Map<VendorsStatusEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] VendorsStatusPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            VendorsStatus item = await _unitOfWork.VendorsStatus.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Vendor Status is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.VendorsStatus.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.VendorsStatus.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<VendorsStatusPostmodel, VendorsStatus>(model);
                await _unitOfWork.VendorsStatus.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            VendorsStatus item = await _unitOfWork.VendorsStatus.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Vendor Status not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.VendorsStatus.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

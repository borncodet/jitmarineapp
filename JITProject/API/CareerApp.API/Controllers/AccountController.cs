﻿using AutoMapper;
using CareerApp.API.Helpers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Accounts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Twilio.Exceptions;
using Twilio.Rest.Verify.V2.Service;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class AccountController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly TwilioVerifySettings _settings;
        private readonly IEmailSender _emailSender;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(
              IHttpContextAccessor accessor,
              IHostingEnvironment env,
              IUnitOfWork unitOfWork,
              IAccountManager accountManager,
              ILogger<AccountController> logger,
              IMapper mapper,
              IOptions<TwilioVerifySettings> settings,
              IEmailSender emailSender,
              IConfiguration configuration) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
            _settings = settings.Value;
            _emailSender = emailSender;
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        [HttpPost("register-me")]
        [ProducesResponseType(typeof(UserRegisterViewModel), 200)]
        public async Task<UserRegisterViewModel> RegisterMeAsync(UserRegisterPostModel user)
        {
            UserRegisterViewModel userRegister = new UserRegisterViewModel();
            string appUserId = string.Empty;
            try
            {
                if (user == null)
                {
                    userRegister.CreateFailureResponse("Post data cannot be null");
                }
                //For candidate registration
                else if (user.CandidateFlag)
                {
                    //Set role id as 4
                    user.RoleId = 4;
                    //Check whether email exist in candidate table
                    Candidate CandidateEmailExist = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.Email.ToLower().Equals(user.Email.ToLower()));
                    //Check whether phone number exist in candidate table
                    Candidate CandidatePhoneExist = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.PhoneNumber.ToLower().Equals(user.PhoneNumber.ToLower()) && x.CountryCode.ToLower().Equals(user.CountryCode.ToLower()));
                    if (CandidateEmailExist != null && CandidatePhoneExist != null)
                    {
                        userRegister.CreateFailureResponse("Email and phone number already exist.");
                    }
                    else if (CandidateEmailExist != null)
                    {
                        userRegister.CreateFailureResponse("Email already exist.");
                    }
                    else if (CandidatePhoneExist != null)
                    {
                        userRegister.CreateFailureResponse("Phone number already exist.");
                    }
                    //Check whether email exist in user table
                    else if (await _accountManager.IsEmailExist(user.Email))
                    {
                        //Get user details of that user
                        var userDetails = await _accountManager.GetUserByEmailAsync(user.Email);
                        userRegister.RowId = userDetails.Id;

                        //Check whether phone number already exist in the user table
                        if (await _accountManager.IsPhoneNumberWithCountryExist(user.PhoneNumber, user.CountryCode))
                        {
                            //Get user details of that user by country code and phone number
                            var userDetailsByPhone = await _accountManager.GetUserByPhoneNumberWithCountryCodeAsync(user.PhoneNumber, user.CountryCode);
                            var minutes = (DateTime.UtcNow - userDetailsByPhone.PhoneOTPCreatedDate).TotalMinutes;
                            if (minutes < 10 && userDetailsByPhone.Email != user.Email)
                            {
                                userRegister.CreateFailureResponse("Phone number already exist.");
                            }
                            else
                            {
                                //Update user details with new details
                                userDetails.RoleId = user.RoleId;
                                userDetails.EmployerFlag = false;
                                userDetails.VendorFlag = false;
                                userDetails.EmployeeFlag = false;
                                userDetails.CandidateFlag = true;
                                userDetails.IsActive = true;
                                userDetails.Email = user.Email;
                                userDetails.CountryCode = user.CountryCode;
                                userDetails.PhoneNumber = user.PhoneNumber;
                                userDetails.UserName = Guid.NewGuid().ToString();
                                userDetails.CreatedDate = DateTime.UtcNow;
                                userDetails.UpdatedDate = DateTime.UtcNow;
                                userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                                userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                                var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                                if (Succeeded)
                                {
                                    //Check wheher email not confirmed for that user
                                    //if (!userDetails.EmailConfirmed)
                                    //{
                                    //send email otp
                                    await SendEmailOTP(user.Email);

                                    userRegister.RowId = userDetails.Id;
                                    userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");

                                    //}
                                    //Check wheher phone number not confirmed for that user
                                    //else if (!userDetails.PhoneNumberConfirmed)
                                    //{
                                    //send phone otp
                                    //await SendPhoneOTP(user.CountryCode, user.PhoneNumber);

                                    //userRegister.RowId = userDetails.Id;
                                    //userRegister.CreateSuccessResponse("A otp has been sent to phone number " + user.CountryCode + " " + user.PhoneNumber + ", Verify your otp!");
                                    //}
                                }
                            }
                        }
                        else
                        {
                            //Update user details with new details
                            userDetails.RoleId = user.RoleId;
                            userDetails.EmployerFlag = false;
                            userDetails.VendorFlag = false;
                            userDetails.EmployeeFlag = false;
                            userDetails.CandidateFlag = true;
                            userDetails.IsActive = true;
                            userDetails.Email = user.Email;
                            userDetails.CountryCode = user.CountryCode;
                            userDetails.PhoneNumber = user.PhoneNumber;
                            userDetails.UserName = Guid.NewGuid().ToString();
                            userDetails.CreatedDate = DateTime.UtcNow;
                            userDetails.UpdatedDate = DateTime.UtcNow;
                            userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                            userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                            var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                            if (Succeeded)
                            {
                                //Check wheher email not confirmed for that user
                                //if (!userDetails.EmailConfirmed)
                                //{
                                //send email otp
                                await SendEmailOTP(user.Email);

                                userRegister.RowId = userDetails.Id;
                                userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                                //}
                                //Check wheher phone number not confirmed for that user
                                //else if (!userDetails.PhoneNumberConfirmed)
                                //{
                                //send phone otp
                                //await SendPhoneOTP(user.CountryCode, user.PhoneNumber);

                                //userRegister.RowId = userDetails.Id;
                                //userRegister.CreateSuccessResponse("A otp has been sent to phone number " + user.CountryCode + " " + user.PhoneNumber + ", Verify your otp!");
                                //}
                            }
                        }
                    }
                    //Check whether phone exist in user table
                    else if (await _accountManager.IsPhoneNumberWithCountryExist(user.PhoneNumber, user.CountryCode))
                    {
                        //Get user details of that user by country code and phone number
                        var userDetails = await _accountManager.GetUserByPhoneNumberWithCountryCodeAsync(user.PhoneNumber, user.CountryCode);
                        userRegister.RowId = userDetails.Id;

                        //Check whether email already exist in the user table
                        if (await _accountManager.IsEmailExist(user.Email))
                        {
                            //Get user details of that user by email
                            var userDetailsByEmail = await _accountManager.GetUserByEmailAsync(user.Email);
                            var minutes = (DateTime.UtcNow - userDetailsByEmail.EmailOTPCreatedDate).TotalMinutes;
                            if (minutes < 10 && userDetailsByEmail.CountryCode != user.CountryCode && userDetailsByEmail.PhoneNumber != user.PhoneNumber)
                            {
                                userRegister.CreateFailureResponse("Email already exist.");
                            }
                            else
                            {
                                //Update user details with new details
                                userDetails.RoleId = user.RoleId;
                                userDetails.EmployerFlag = false;
                                userDetails.VendorFlag = false;
                                userDetails.EmployeeFlag = false;
                                userDetails.CandidateFlag = true;
                                userDetails.IsActive = true;
                                userDetails.Email = user.Email;
                                userDetails.CountryCode = user.CountryCode;
                                userDetails.PhoneNumber = user.PhoneNumber;
                                userDetails.UserName = Guid.NewGuid().ToString();
                                userDetails.CreatedDate = DateTime.UtcNow;
                                userDetails.UpdatedDate = DateTime.UtcNow;
                                userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                                userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                                var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                                if (Succeeded)
                                {
                                    //send email otp
                                    await SendEmailOTP(user.Email);

                                    userRegister.RowId = userDetails.Id;
                                    userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                                }
                            }
                        }
                        else
                        {
                            //Update user details with new details
                            userDetails.RoleId = user.RoleId;
                            userDetails.EmployerFlag = false;
                            userDetails.VendorFlag = false;
                            userDetails.EmployeeFlag = false;
                            userDetails.CandidateFlag = true;
                            userDetails.IsActive = true;
                            userDetails.Email = user.Email;
                            userDetails.CountryCode = user.CountryCode;
                            userDetails.PhoneNumber = user.PhoneNumber;
                            userDetails.UserName = Guid.NewGuid().ToString();
                            userDetails.CreatedDate = DateTime.UtcNow;
                            userDetails.UpdatedDate = DateTime.UtcNow;
                            userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                            userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                            var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                            if (Succeeded)
                            {
                                //send email otp
                                await SendEmailOTP(user.Email);

                                userRegister.RowId = userDetails.Id;
                                userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                            }
                        }
                    }
                    //Both email and phone number not exist
                    else
                    {
                        //Set role id as 4
                        user.RoleId = 4;

                        ApplicationUser appUser = new ApplicationUser
                        {
                            RoleId = user.RoleId,
                            EmployerFlag = false,
                            VendorFlag = false,
                            EmployeeFlag = false,
                            CandidateFlag = true,
                            IsActive = true,
                            Email = user.Email,
                            CountryCode = user.CountryCode,
                            PhoneNumber = user.PhoneNumber,
                            UserName = Guid.NewGuid().ToString(),
                            CreatedDate = DateTime.UtcNow,
                            UpdatedDate = DateTime.UtcNow,
                            EmailOTPCreatedDate = DateTime.UtcNow,
                            PhoneOTPCreatedDate = DateTime.UtcNow
                        };

                        var (Succeeded, Errors) = await _accountManager.CreateUserAsync(appUser, new string[] { _accountManager.GetRoleByIdAsync(user.RoleId).Result.Name }, user.Password);

                        if (Succeeded)
                        {
                            //send email otp
                            if (user.EmployerFlag || user.CandidateFlag)
                            {
                                await SendEmailOTP(user.Email);
                            }

                            userRegister.RowId = appUser.Id;
                            userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                        }
                        else
                        {
                            userRegister.CreateFailureResponse($"Error occured while register user. {Errors.FirstOrDefault()}.");
                        }

                    }
                }
                //For vendor registration
                else if (user.VendorFlag)
                {
                    //Set role id as 2
                    user.RoleId = 2;
                    //Check whether email exist in vendor table
                    Vendor VendorEmailExist = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.Email.ToLower().Equals(user.Email.ToLower()));
                    //Check whether phone number exist in Vendor table
                    Vendor VendorPhoneExist = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.PhoneNumber.ToLower().Equals(user.PhoneNumber.ToLower()) && x.CountryCode.ToLower().Equals(user.CountryCode.ToLower()));
                    if (VendorEmailExist != null && VendorPhoneExist != null)
                    {
                        userRegister.CreateFailureResponse("Email and phone number already exist.");
                    }
                    else if (VendorEmailExist != null)
                    {
                        userRegister.CreateFailureResponse("Email already exist.");
                    }
                    else if (VendorPhoneExist != null)
                    {
                        userRegister.CreateFailureResponse("Phone number already exist.");
                    }
                    //Check whether email exist in user table
                    else if (await _accountManager.IsEmailExist(user.Email))
                    {
                        //Get user details of that user
                        var userDetails = await _accountManager.GetUserByEmailAsync(user.Email);
                        userRegister.RowId = userDetails.Id;

                        //Check whether phone number already exist in the user table
                        if (await _accountManager.IsPhoneNumberWithCountryExist(user.PhoneNumber, user.CountryCode))
                        {
                            //Get user details of that user by country code and phone number
                            var userDetailsByPhone = await _accountManager.GetUserByPhoneNumberWithCountryCodeAsync(user.PhoneNumber, user.CountryCode);
                            var minutes = (DateTime.UtcNow - userDetailsByPhone.PhoneOTPCreatedDate).TotalMinutes;
                            if (minutes < 10 && userDetailsByPhone.Email != user.Email)
                            {
                                userRegister.CreateFailureResponse("Phone number already exist.");
                            }
                            else
                            {
                                //Update user details with new details
                                userDetails.RoleId = user.RoleId;
                                userDetails.EmployerFlag = false;
                                userDetails.VendorFlag = true;
                                userDetails.EmployeeFlag = false;
                                userDetails.CandidateFlag = false;
                                userDetails.IsActive = true;
                                userDetails.Email = user.Email;
                                userDetails.CountryCode = user.CountryCode;
                                userDetails.PhoneNumber = user.PhoneNumber;
                                userDetails.UserName = Guid.NewGuid().ToString();
                                userDetails.CreatedDate = DateTime.UtcNow;
                                userDetails.UpdatedDate = DateTime.UtcNow;
                                userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                                userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                                var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                                if (Succeeded)
                                {
                                    //Check wheher email not confirmed for that user
                                    //if (!userDetails.EmailConfirmed)
                                    //{
                                    //send email otp
                                    await SendEmailOTP(user.Email);

                                    userRegister.RowId = userDetails.Id;
                                    userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");

                                    //}
                                    //Check wheher phone number not confirmed for that user
                                    //else if (!userDetails.PhoneNumberConfirmed)
                                    //{
                                    //send phone otp
                                    //await SendPhoneOTP(user.CountryCode, user.PhoneNumber);

                                    //userRegister.RowId = userDetails.Id;
                                    //userRegister.CreateSuccessResponse("A otp has been sent to phone number " + user.CountryCode + " " + user.PhoneNumber + ", Verify your otp!");
                                    //}
                                }
                            }
                        }
                        else
                        {
                            //Update user details with new details
                            userDetails.RoleId = user.RoleId;
                            userDetails.EmployerFlag = false;
                            userDetails.VendorFlag = true;
                            userDetails.EmployeeFlag = false;
                            userDetails.CandidateFlag = false;
                            userDetails.IsActive = true;
                            userDetails.Email = user.Email;
                            userDetails.CountryCode = user.CountryCode;
                            userDetails.PhoneNumber = user.PhoneNumber;
                            userDetails.UserName = Guid.NewGuid().ToString();
                            userDetails.CreatedDate = DateTime.UtcNow;
                            userDetails.UpdatedDate = DateTime.UtcNow;
                            userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                            userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                            var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                            if (Succeeded)
                            {
                                //Check wheher email not confirmed for that user
                                //if (!userDetails.EmailConfirmed)
                                //{
                                //send email otp
                                await SendEmailOTP(user.Email);

                                userRegister.RowId = userDetails.Id;
                                userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                                //}
                                //Check wheher phone number not confirmed for that user
                                //else if (!userDetails.PhoneNumberConfirmed)
                                //{
                                //send phone otp
                                //await SendPhoneOTP(user.CountryCode, user.PhoneNumber);

                                //userRegister.RowId = userDetails.Id;
                                //userRegister.CreateSuccessResponse("A otp has been sent to phone number " + user.CountryCode + " " + user.PhoneNumber + ", Verify your otp!");
                                //}
                            }
                        }
                    }
                    //Check whether phone exist in user table
                    else if (await _accountManager.IsPhoneNumberWithCountryExist(user.PhoneNumber, user.CountryCode))
                    {
                        //Get user details of that user by country code and phone number
                        var userDetails = await _accountManager.GetUserByPhoneNumberWithCountryCodeAsync(user.PhoneNumber, user.CountryCode);
                        userRegister.RowId = userDetails.Id;

                        //Check whether email already exist in the user table
                        if (await _accountManager.IsEmailExist(user.Email))
                        {
                            //Get user details of that user by email
                            var userDetailsByEmail = await _accountManager.GetUserByEmailAsync(user.Email);
                            var minutes = (DateTime.UtcNow - userDetailsByEmail.EmailOTPCreatedDate).TotalMinutes;
                            if (minutes < 10 && userDetailsByEmail.CountryCode != user.CountryCode && userDetailsByEmail.PhoneNumber != user.PhoneNumber)
                            {
                                userRegister.CreateFailureResponse("Email already exist.");
                            }
                            else
                            {
                                //Update user details with new details
                                userDetails.RoleId = user.RoleId;
                                userDetails.EmployerFlag = false;
                                userDetails.VendorFlag = true;
                                userDetails.EmployeeFlag = false;
                                userDetails.CandidateFlag = false;
                                userDetails.IsActive = true;
                                userDetails.Email = user.Email;
                                userDetails.CountryCode = user.CountryCode;
                                userDetails.PhoneNumber = user.PhoneNumber;
                                userDetails.UserName = Guid.NewGuid().ToString();
                                userDetails.CreatedDate = DateTime.UtcNow;
                                userDetails.UpdatedDate = DateTime.UtcNow;
                                userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                                userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                                var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                                if (Succeeded)
                                {
                                    //send email otp
                                    await SendEmailOTP(user.Email);

                                    userRegister.RowId = userDetails.Id;
                                    userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                                }
                            }
                        }
                        else
                        {
                            //Update user details with new details
                            userDetails.RoleId = user.RoleId;
                            userDetails.EmployerFlag = false;
                            userDetails.VendorFlag = true;
                            userDetails.EmployeeFlag = false;
                            userDetails.CandidateFlag = false;
                            userDetails.IsActive = true;
                            userDetails.Email = user.Email;
                            userDetails.CountryCode = user.CountryCode;
                            userDetails.PhoneNumber = user.PhoneNumber;
                            userDetails.UserName = Guid.NewGuid().ToString();
                            userDetails.CreatedDate = DateTime.UtcNow;
                            userDetails.UpdatedDate = DateTime.UtcNow;
                            userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                            userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                            var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);

                            if (Succeeded)
                            {
                                //send email otp
                                await SendEmailOTP(user.Email);

                                userRegister.RowId = userDetails.Id;
                                userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                            }
                        }
                    }
                    //Both email and phone number not exist
                    else
                    {
                        //Set role id as 4
                        user.RoleId = 4;

                        ApplicationUser appUser = new ApplicationUser
                        {
                            RoleId = user.RoleId,
                            EmployerFlag = false,
                            VendorFlag = true,
                            EmployeeFlag = false,
                            CandidateFlag = false,
                            IsActive = true,
                            Email = user.Email,
                            CountryCode = user.CountryCode,
                            PhoneNumber = user.PhoneNumber,
                            UserName = Guid.NewGuid().ToString(),
                            CreatedDate = DateTime.UtcNow,
                            UpdatedDate = DateTime.UtcNow,
                            EmailOTPCreatedDate = DateTime.UtcNow,
                            PhoneOTPCreatedDate = DateTime.UtcNow
                        };

                        var (Succeeded, Errors) = await _accountManager.CreateUserAsync(appUser, new string[] { _accountManager.GetRoleByIdAsync(user.RoleId).Result.Name }, user.Password);

                        if (Succeeded)
                        {
                            //send email otp
                            if (user.EmployerFlag || user.VendorFlag)
                            {
                                await SendEmailOTP(user.Email);
                            }

                            userRegister.RowId = appUser.Id;
                            userRegister.CreateSuccessResponse("A otp has been sent to email " + user.Email + ", Verify your otp!");
                        }
                        else
                        {
                            userRegister.CreateFailureResponse($"Error occured while register user. {Errors.FirstOrDefault()}.");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, $"Error occurred while creating new user.{user.Email} {user.CountryCode} {user.PhoneNumber}");
                throw;
            }
            return userRegister;
        }

        //used for send and resend confirm email
        [AllowAnonymous]
        [HttpPost("send-confirm-email")]
        public async Task<string> SendConfirmEmail(string email)
        {
            try
            {
                // Find the user by email
                var appUser = await _accountManager.GetUserByEmailAsync(email);
                string confirmationToken = await _accountManager.GenerateEmailConfirmationTokenAsync(appUser);
                string confirmationLink = _configuration["ConfirmEmail:url"] + "userId=" + appUser.Id + "&token=" + confirmationToken + "&roleId=" + appUser.RoleId;
                var response = await _emailSender.SendEmailAsync(appUser.Email, "Thanks for joining Jitmarine", "Please confirm that your email address is correct to continue. Click the link below to get started.", "<a href='" + confirmationLink + "'>Confirm Email Address</a>");
                return response;
            }
            catch (TwilioException ex)
            {
                return "There was an error in sending confirm email, please try again.";
            }
        }

        //used for send and resend phone otp
        [AllowAnonymous]
        [HttpPost("send-phone-otp")]
        public async Task<string> SendPhoneOTP(string countryCode, string phoneNumber)
        {
            try
            {
                //send phonenumber verification
                string phonePattern = @"^\+[1-9]\d{1,14}$";
                bool isPhoneValid = Regex.IsMatch(countryCode + "" + phoneNumber, phonePattern);
                var serviceId = _settings.VerificationServiceSID;

                if (!isPhoneValid)
                {
                    return "Invalid phonenumber";
                }
                else if (string.IsNullOrEmpty(serviceId))
                {
                    return "Invalid service id";
                }

                var verification = await VerificationResource.CreateAsync(
                                          to: countryCode + "" + phoneNumber,
                                          channel: "sms",
                                          pathServiceSid: serviceId
                                      );

                //Get user details by country code and phone number
                var userDetails = await _accountManager.GetUserByPhoneNumberWithCountryCodeAsync(phoneNumber, countryCode);
                //Update phone otp sendtime
                userDetails.PhoneOTPCreatedDate = DateTime.UtcNow;
                var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);
                return verification.Status;

            }
            catch (TwilioException ex)
            {
                return "There was an error in sending otp, please try again.";
            }
        }

        //used for verify code
        [AllowAnonymous]
        [HttpPost("verify-code")]
        public async Task<IActionResult> CheckVerificationAsync(string countryCode, string phoneNumber, string code)
        {
            try
            {
                var verificationCheckResource = await VerificationCheckResource.CreateAsync(
                    to: countryCode + "" + phoneNumber,
                    code: code,
                    pathServiceSid: _settings.VerificationServiceSID
                    );
                var result = verificationCheckResource.Status.Equals("approved") ? "Verified" : "Wrong code. Try again.";
                if (result == "Verified")
                {
                    var user = await _accountManager.GetUserByPhoneNumberAsync(phoneNumber);
                    user.PhoneNumberConfirmed = true;
                    var updateResult = await _accountManager.UpdateUserAsync(user);
                }
                else
                {
                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (TwilioException ex)
            {
                return BadRequest("There was an error confirming the code, please check the verification code is correct and try again.");
            }
        }

        //used for verify code during phone number updation in communication profile
        [AllowAnonymous]
        [HttpPost("update-phone-verify-code")]
        public async Task<IActionResult> PhoneNumberUpdationAsync(string newCountryCode, string newPhoneNumber, string oldCountryCode, string oldPhoneNumber, string code)
        {
            try
            {
                var verificationCheckResource = await VerificationCheckResource.CreateAsync(
                    to: newCountryCode + "" + newPhoneNumber,
                    code: code,
                    pathServiceSid: _settings.VerificationServiceSID
                    );
                var result = verificationCheckResource.Status.Equals("approved") ? "Verified" : "Wrong code. Try again.";
                if (result == "Verified")
                {
                    var user = await _accountManager.GetUserByPhoneNumberAsync(oldPhoneNumber);
                    user.CountryCode = newCountryCode;
                    user.PhoneNumber = newPhoneNumber;
                    user.PhoneNumberConfirmed = true;
                    var updateResult = await _accountManager.UpdateUserAsync(user);
                }
                else
                {
                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (TwilioException ex)
            {
                return BadRequest("There was an error confirming the code, please check the verification code is correct and try again.");
            }
        }

        //user for verify code
        [AllowAnonymous]
        [HttpPost("email-verify-code")]
        public async Task<IActionResult> CheckEmailVerificationAsync(string email, string code)
        {
            try
            {
                var verificationCheckResource = await VerificationCheckResource.CreateAsync(
                    to: email,
                    code: code,
                    pathServiceSid: _settings.VerificationServiceSID
                    );
                var result = verificationCheckResource.Status.Equals("approved") ? "Verified" : "Wrong code. Try again.";
                if (result == "Verified")
                {
                    var user = await _accountManager.GetUserByEmailAsync(email);
                    user.EmailConfirmed = true;
                    var updateResult = await _accountManager.UpdateUserAsync(user);
                }
                else
                {
                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (TwilioException ex)
            {
                return BadRequest("There was an error confirming the code, please check the verification code is correct and try again.");
            }
        }

        //used for send and resend email otp
        [AllowAnonymous]
        [HttpPost("send-email-otp")]
        public async Task<string> SendEmailOTP(string email)
        {
            try
            {
                var channelConfiguration = new Dictionary<string, Object>()
                                            {
                                                {"template_id", "d-da3399d6d8194b49b0b6752151733263"},
                                                {"from", "abhilash15102020@gmail.com"},
                                                {"from_name", "JitMarine"}
                                            };
                var verification = await VerificationResource.CreateAsync(
                                            channelConfiguration: channelConfiguration,
                                            to: email,
                                            channel: "email",
                                            pathServiceSid: _settings.VerificationServiceSID
                                      );

                //Get user details by country code and phone number
                var userDetails = await _accountManager.GetUserByEmailAsync(email);
                //Update email otp sendtime
                userDetails.EmailOTPCreatedDate = DateTime.UtcNow;
                var (Succeeded, Errors) = await _accountManager.UpdateUserAsync(userDetails);
                return verification.Status;

            }
            catch (TwilioException ex)
            {
                return "There was an error in sending otp, please try again.";
            }
        }

        [HttpPost("get-basic-details")]
        [Produces(typeof(UserInfoViewModel))]
        [Authorize]
        public async Task<UserInfoViewModel> GetUserBasicDetailsAsync()
        {
            UserInfoViewModel viewModel = new UserInfoViewModel();
            var userId = GetCurrentUserId();
            //int userId = 1000;
            var user = await _accountManager.GetUserByIdAsync(userId);
            if (user == null)
            {
                viewModel.CreateFailureResponse("User not found.");
                _logger.LogInformation("User not found.");
                return viewModel;
            }

            viewModel.UserId = userId;
            viewModel.UserName = user.UserName;
            viewModel.Email = user.Email;
            viewModel.CountryCode = user.CountryCode;
            viewModel.PhoneNumber = user.PhoneNumber;
            if (user.RoleId == 4)
            {
                Candidate Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.UserId == userId);
                if (Candidate != null)
                {
                    viewModel.FullName = Candidate.FirstName;
                }
            }
            else if (user.RoleId == 2)
            {
                Vendor Vendor = await _unitOfWork.Vendors.GetFirstOrDefaultAsync(x => x.UserId == userId);
                if (Vendor != null)
                {
                    viewModel.FullName = Vendor.VendorName;
                }
            }
            viewModel.CreateSuccessResponse();
            return viewModel;
        }

        [HttpPost("profile-edit")]
        [Produces(typeof(ProfileEditViewModel))]
        [Authorize]
        public async Task<ProfileEditViewModel> ProfileEdit(ProfileEditPostModel model)
        {
            ProfileEditViewModel result = new ProfileEditViewModel();

            var userId = GetCurrentUserId();
            var user = await _accountManager.GetUserByIdAsync(userId);
            if (user == null)
            {
                result.CreateFailureResponse("User not found.");
                return result;
            }
            //viewModel.UserName = user.UserName;
            //viewModel.Email = user.Email;
            user.CountryCode = model.CountryCode;
            user.PhoneNumber = model.PhoneNumber;
            await _accountManager.UpdateUserAsync(user);
            result.CreateSuccessResponse();
            return result;
        }

        [HttpPost("change-password")]
        [Produces(typeof(string))]
        [Authorize]
        public async Task<string> ChangePassword(PasswordEditPostModel model)
        {
            //ProfileEditViewModel result = new ProfileEditViewModel();

            var userId = GetCurrentUserId();
            var user = await _accountManager.GetUserByIdAsync(userId);


            PasswordHasher<ApplicationUser> passwordHasher = new PasswordHasher<ApplicationUser>();
            if (passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.CurrentPassword) != PasswordVerificationResult.Failed)
            {
                user.PasswordHash = passwordHasher.HashPassword(user, model.NewPassword);
                var result = await _accountManager.UpdateUserAsync(user);
                return "Password successfully updated";
            }


            ////result.CreateSuccessResponse();
            return "Please enter correct Password";

        }

        //confirm the mail
        [HttpPost("confirm-email-link")]
        [Produces(typeof(string))]
        [AllowAnonymous]
        public async Task<string> ConfirmEmail(int userId, string token, int roleId)
        {
            ApplicationUser user = await _accountManager.GetUserByIdAsync(userId);
            var result = await _accountManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return "Success";
            }
            else
            {
                return "Error";
            }
        }

        [HttpPost("forgot-password-link")]
        [Produces(typeof(string))]
        [AllowAnonymous]
        public async Task<string> ForgotPassword(ForgotPasswordPostModel model)
        {
            // Find the user by email
            var user = await _accountManager.GetUserByEmailAsync(model.Email);
            // If the user is found and email is confirmed
            if (user != null && await _accountManager.IsEmailConfirmedAsync(user))
            {
                // Generate the reset password token
                var token = await _accountManager.GeneratePasswordResetTokenAsync(user);

                // Build the password reset link
                //var passwordResetLink = Url.Action("ResetPassword", "Account",
                //        new { email = model.Email, token = token }, Request.Scheme);

                string passwordResetLink = _configuration["ResetPassword:url"] + "userId=" + user.Id + "&token=" + token + "&roleId=" + user.RoleId + "&email=" + model.Email;
                var response = await _emailSender.SendEmailAsync(user.Email, "Password Reset - Jitmarine", "If you've lost your password or wish to reset it, use the link below to get started.", "<a href='" + passwordResetLink + "'>Reset Your Password</a>");

                // Log the password reset link
                _logger.LogWarning(passwordResetLink);

                // Send the user to Forgot Password Confirmation view
                return "If you have an account with us, we have sent an email with the instructions to reset your password.";

            }
            return "If you have an account with us, we have sent an email with the instructions to reset your password.";
        }

        [HttpPost("reset-password")]
        [Produces(typeof(string))]
        [AllowAnonymous]
        public async Task<string> ResetPassword(ResetPasswordPostModel model)
        {
            // Find the user by email
            var user = await _accountManager.GetUserByEmailAsync(model.Email);

            // reset the user password
            var result = await _accountManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (result.Succeeded)
            {
                return "Success";
            }
            else
            {
                return "Error";
            }
        }

        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        //candidate for verify email or phone during forgot password process
        [AllowAnonymous]
        [HttpPost("verify-candidate")]
        public async Task<IActionResult> CheckCandidateExitsByEmailOrPhoneAsync(string email, string phoneNumber, string countryCode, string type)
        {
            try
            {
                Candidate Candidate = null;

                if (type.ToLower().Equals("email"))
                {
                    Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.Email != null && x.Email.ToLower().Equals(email.ToLower()) && x.IsActive == true);

                }
                if (type.ToLower().Equals("mobile"))
                {
                    Candidate = await _unitOfWork.Candidates.GetFirstOrDefaultAsync(x => x.PhoneNumber != null && x.PhoneNumber.ToLower().Equals(phoneNumber.ToLower()) && x.IsActive == true && x.CountryCode != null && x.CountryCode.ToLower().Equals(countryCode.ToLower()));
                }

                if (Candidate != null)
                {
                    ApplicationUser user = await _accountManager.GetUserByIdAsync(Candidate.UserId);
                    var token = await _accountManager.GeneratePasswordResetTokenAsync(user);
                    return Ok(new { message = "Valid user", token = token, email = user.Email });
                }
                return BadRequest("Not a valid user");

            }
            catch (Exception ex)
            {
                return BadRequest("Not a valid user");
            }
        }

        [AllowAnonymous]
        [HttpPost("reset-password-otp")]
        public async Task<IActionResult> OTPResetPassword(string email, string token, string password, string confirmPassord)
        {
            try
            {
                if (password.Equals(confirmPassord))
                {
                    // Find the user by email
                    var user = await _accountManager.GetUserByEmailAsync(email);

                    // reset the user password
                    var result = await _accountManager.ResetPasswordAsync(user, token, password);
                    if (result.Succeeded)
                    {
                        return Ok("Success");
                    }
                    else
                    {
                        return BadRequest("Not able to reset password");
                    }
                }
                return BadRequest("Password Mismatch");
            }
            catch (Exception ex)
            {
                return BadRequest("Not able to reset password");
            }
        }

    }
}

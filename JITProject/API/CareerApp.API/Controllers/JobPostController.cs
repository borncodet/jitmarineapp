﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobPostController : BaseController
    {
        private readonly IMapper _mapper;

        public JobPostController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobPostController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> GetAllActiveAsync([FromBody]JobListDataRequestModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }

        [Route("sa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> SearchAsync([FromBody]BaseSearchMultipleViewModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.SearchMultipleAsync(model);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }

        [Route("saja")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedCandidateDataResultModel), 200)]
        public async Task<JobAppliedCandidateDataResultModel> SearchAppliedJobAsync([FromBody]AppliedSearchMultipleViewModel searchModel)
        {
            IPagedList<RecentlyAppliedJobInfo> data = await _unitOfWork.JobLists.SearchAppliedJobAsync(searchModel);
            return PopulateResponseWithoutMap<JobAppliedCandidateDataResultModel, RecentlyAppliedJobInfo>(data);
        }

        [Route("sbca")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> SearchByCandidateAsync([FromBody]BaseSearchByCandidateViewModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.SearchByCandidateAsync(model);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }

        [Route("gaja")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> GetAlertJobAsync([FromBody]BaseSearchByCandidateViewModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.GetAlertJobAsync(model);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }

        [Route("sgja")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListDataResultModel), 200)]
        public async Task<JobListDataResultModel> SuggestedJobAsync([FromBody]BaseSearchByCandidateViewModel model)
        {
            IPagedList<JobListInfo> data = await _unitOfWork.JobLists.SuggestedJobAsync(model);
            return PopulateResponseWithoutMap<JobListDataResultModel, JobListInfo>(data);
        }

        [Route("gajc")]
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)]
        public async Task<int> GetAlertJobCountAsync(BaseCandidateSearchViewModel model)
        {
            return await _unitOfWork.JobLists.GetAlertJobCountAsync(model);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobListPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobList JobList = await _unitOfWork.JobLists.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobList != null && JobList.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Job Title is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobList = await _unitOfWork.JobLists.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobList != null)
            {
                JobList = _mapper.Map(model, JobList);
                await _unitOfWork.JobLists.UpdateAsync(JobList);
            }
            else
            {
                JobList = _mapper.Map<JobListPostmodel, JobList>(model);
                await _unitOfWork.JobLists.AddAsync(JobList);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobList.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListSelectBoxDataViewModel), 200)]
        public async Task<JobListSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobLists.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobListEditResponse), 200)]
        public async Task<JobListEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobListEditResponse response = new JobListEditResponse();

            JobList JobList = await _unitOfWork.JobLists.GetAsync(model.RowId);

            if (JobList != null)
            {
                response.Data = _mapper.Map<JobListEditModel>(JobList);
            }
            else
            {
                response.Data = new JobListEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobList JobList = await _unitOfWork.JobLists.GetAsync(model.RowId);
            if (JobList == null)
            {
                viewModel.CreateFailureResponse("Job not found.");
                return viewModel;
            }

            JobList.IsActive = false;
            await _unitOfWork.JobLists.UpdateAsync(JobList);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

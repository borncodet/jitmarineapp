﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobByVendorController : BaseController
    {
        private readonly IMapper _mapper;

        public JobByVendorController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobByVendorController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobByVendorDataResultModel), 200)]
        public async Task<JobByVendorDataResultModel> GetAllActiveAsync([FromBody]JobByVendorDataRequestModel model)
        {
            IPagedList<JobByVendorInfo> data = await _unitOfWork.JobByVendors.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobByVendorDataResultModel, JobByVendorInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobByVendorPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobByVendor JobByVendor = await _unitOfWork.JobByVendors.GetFirstOrDefaultAsync(x => x.JobId == model.JobId && x.VendorId == model.VendorId && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobByVendor != null && JobByVendor.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Mapping is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobByVendor = await _unitOfWork.JobByVendors.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobByVendor != null)
            {
                JobByVendor = _mapper.Map(model, JobByVendor);
                await _unitOfWork.JobByVendors.UpdateAsync(JobByVendor);
            }
            else
            {
                JobByVendor = _mapper.Map<JobByVendorPostmodel, JobByVendor>(model);
                await _unitOfWork.JobByVendors.AddAsync(JobByVendor);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobByVendor.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobByVendorSelectBoxDataViewModel), 200)]
        public async Task<JobByVendorSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobByVendors.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobByVendorEditResponse), 200)]
        public async Task<JobByVendorEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobByVendorEditResponse response = new JobByVendorEditResponse();

            JobByVendor JobByVendor = await _unitOfWork.JobByVendors.GetAsync(model.RowId);

            if (JobByVendor != null)
            {
                response.Data = _mapper.Map<JobByVendorEditModel>(JobByVendor);
            }
            else
            {
                response.Data = new JobByVendorEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobByVendor JobByVendor = await _unitOfWork.JobByVendors.GetAsync(model.RowId);
            if (JobByVendor == null)
            {
                viewModel.CreateFailureResponse("Mapping not found.");
                return viewModel;
            }

            JobByVendor.IsActive = false;
            await _unitOfWork.JobByVendors.UpdateAsync(JobByVendor);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

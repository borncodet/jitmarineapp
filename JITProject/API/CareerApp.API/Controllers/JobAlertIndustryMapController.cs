﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAlertIndustryMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAlertIndustryMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAlertIndustryMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertIndustryMapDataResultModel), 200)]
        public async Task<JobAlertIndustryMapDataResultModel> GetAllActiveAsync([FromBody]JobAlertIndustryMapDataRequestModel model)
        {
            IPagedList<JobAlertIndustryMapInfo> data = await _unitOfWork.JobAlertIndustryMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobAlertIndustryMapDataResultModel, JobAlertIndustryMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobAlertIndustryMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAlertIndustryMap JobAlertIndustryMap = await _unitOfWork.JobAlertIndustryMap.GetFirstOrDefaultAsync(x => x.JobAlertId.Equals(model.JobAlertId) && x.IndustryId.Equals(model.IndustryId) && x.IsActive == true);

            if (JobAlertIndustryMap != null && JobAlertIndustryMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Industry already mapped to the alert.");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobAlertIndustryMap = await _unitOfWork.JobAlertIndustryMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobAlertIndustryMap != null)
            {
                JobAlertIndustryMap = _mapper.Map(model, JobAlertIndustryMap);
                await _unitOfWork.JobAlertIndustryMap.UpdateAsync(JobAlertIndustryMap);
            }
            else
            {
                JobAlertIndustryMap = _mapper.Map<JobAlertIndustryMapPostmodel, JobAlertIndustryMap>(model);
                await _unitOfWork.JobAlertIndustryMap.AddAsync(JobAlertIndustryMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobAlertIndustryMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertIndustryMapSelectBoxDataViewModel), 200)]
        public async Task<JobAlertIndustryMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAlertIndustryMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAlertIndustryMapEditResponse), 200)]
        public async Task<JobAlertIndustryMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAlertIndustryMapEditResponse response = new JobAlertIndustryMapEditResponse();

            JobAlertIndustryMap JobAlertIndustryMap = await _unitOfWork.JobAlertIndustryMap.GetAsync(model.RowId);

            if (JobAlertIndustryMap != null)
            {
                response.Data = _mapper.Map<JobAlertIndustryMapEditModel>(JobAlertIndustryMap);
            }
            else
            {
                response.Data = new JobAlertIndustryMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAlertIndustryMap JobAlertIndustryMap = await _unitOfWork.JobAlertIndustryMap.GetAsync(model.RowId);
            if (JobAlertIndustryMap == null)
            {
                viewModel.CreateFailureResponse("Industry alert map not found.");
                return viewModel;
            }

            JobAlertIndustryMap.IsActive = false;
            await _unitOfWork.JobAlertIndustryMap.UpdateAsync(JobAlertIndustryMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

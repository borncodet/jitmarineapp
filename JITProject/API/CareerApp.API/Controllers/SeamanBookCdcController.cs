﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class SeamanBookCdcController : BaseController
    {
        private readonly IMapper _mapper;

        public SeamanBookCdcController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<SeamanBookCdcController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(SeamanBookCdcDataResultModel), 200)]
        public async Task<SeamanBookCdcDataResultModel> GetAllActiveAsync([FromBody]SeamanBookCdcDataRequestModel model)
        {
            IPagedList<SeamanBookCdc> data = await _unitOfWork.SeamanBookCdcs.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<SeamanBookCdcDataResultModel, SeamanBookCdcInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(SeamanBookCdcDataResultModel), 200)]
        public async Task<SeamanBookCdcDataResultModel> GetAllInactiveAsync([FromBody]SeamanBookCdcDataRequestModel model)
        {
            IPagedList<SeamanBookCdc> data = await _unitOfWork.SeamanBookCdcs.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<SeamanBookCdcDataResultModel, SeamanBookCdcInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(SeamanBookCdcEditModel), 200)]
        public async Task<SeamanBookCdcEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            SeamanBookCdc item = await _unitOfWork.SeamanBookCdcs.GetAsync(model.RowId);
            SeamanBookCdcEditModel viewModel = _mapper.Map<SeamanBookCdcEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] SeamanBookCdcPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            SeamanBookCdc item = await _unitOfWork.SeamanBookCdcs.GetFirstOrDefaultAsync(x => x.CdcNumber.ToLower().Equals(model.CdcNumber.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("SeamanBookCdc is already exist!");
                return viewModel;
            }

            //Uploading Files 
            //if (model.Document != null)
            //{
            //    model.Passport = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\Passport\", model.Document);
            //}

            if (model.RowId != 0)
            {
                item = await _unitOfWork.SeamanBookCdcs.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.SeamanBookCdcs.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<SeamanBookCdcPostmodel, SeamanBookCdc>(model);
                await _unitOfWork.SeamanBookCdcs.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            SeamanBookCdc item = await _unitOfWork.SeamanBookCdcs.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("SeamanBookCdc not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.SeamanBookCdcs.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

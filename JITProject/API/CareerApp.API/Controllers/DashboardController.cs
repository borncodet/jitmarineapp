﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DashboardListController : BaseController
    {
        private readonly IMapper _mapper;

        public DashboardListController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DashboardListController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DashboardListDataResultModel), 200)]
        public async Task<DashboardListDataResultModel> GetAllActiveAsync([FromBody]DashboardListDataRequestModel model)
        {
            IPagedList<DashboardList> data = await _unitOfWork.DashboardList.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<DashboardListDataResultModel, DashboardListInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(DashboardListDataResultModel), 200)]
        public async Task<DashboardListDataResultModel> GetAllInactiveAsync([FromBody]DashboardListDataRequestModel model)
        {
            IPagedList<DashboardList> data = await _unitOfWork.DashboardList.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<DashboardListDataResultModel, DashboardListInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DashboardListEditModel), 200)]
        public async Task<DashboardListEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            DashboardList item = await _unitOfWork.DashboardList.GetAsync(model.RowId);
            DashboardListEditModel viewModel = _mapper.Map<DashboardListEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] DashboardListPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DashboardList item = await _unitOfWork.DashboardList.GetFirstOrDefaultAsync(x => x.DashboardName.ToLower().Equals(model.DashboardName.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Dashboard is already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                model.DashboardImage = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\DashboardList\", model.Document);
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.DashboardList.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.DashboardList.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<DashboardListPostmodel, DashboardList>(model);
                await _unitOfWork.DashboardList.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(DashboardListSelectBoxDataViewModel), 200)]
        public async Task<DashboardListSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.DashboardList.GetSelectBoxDataAsync();
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            DashboardList item = await _unitOfWork.DashboardList.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Dashboard not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.DashboardList.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

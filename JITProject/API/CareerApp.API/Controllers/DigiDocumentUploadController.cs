﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DigiDocumentUploadController : BaseController
    {
        private readonly IMapper _mapper;

        public DigiDocumentUploadController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DigiDocumentUploadController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentUploadDataResultModel), 200)]
        public async Task<DigiDocumentUploadDataResultModel> GetAllActiveAsync([FromBody]BaseCandidateSearchViewModel model)
        {
            IPagedList<DigiDocumentUploadInfo> data = await _unitOfWork.DigiDocumentUpload.GetAllAsync(model);
            return PopulateResponseWithoutMap<DigiDocumentUploadDataResultModel, DigiDocumentUploadInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] DigiDocumentUploadPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DigiDocumentUpload DigiDocumentUpload = await _unitOfWork.DigiDocumentUpload.GetFirstOrDefaultAsync(x => x.DigiDocumentDetailId.Equals(model.DigiDocumentDetailId) && x.IsActive == true);

            //if (DigiDocumentUpload != null && DigiDocumentUpload.RowId != model.RowId)
            //{
            //    viewModel.CreateFailureResponse("Document already exist!");
            //    return viewModel;
            //}

            //Uploading Files 
            if (model.Document != null)
            {
                DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetAsync(model.DigiDocumentDetailId);
                if (DigiDocumentDetails != null)
                {
                    DigiDocumentType digiDocumentType = await _unitOfWork.DigiDocumentTypes.GetAsync(DigiDocumentDetails.DigiDocumentTypeId);
                    model.DigiDocument = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\" + digiDocumentType.Title + @"\", model.Document);
                }
            }

            if (model.RowId != 0)
            {
                DigiDocumentUpload = await _unitOfWork.DigiDocumentUpload.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (DigiDocumentUpload != null)
            {
                // DigiDocumentUpload = _mapper.Map(model, DigiDocumentUpload);

                DigiDocumentUpload.DigiDocument = model.DigiDocument;

                await _unitOfWork.DigiDocumentUpload.UpdateAsync(DigiDocumentUpload);
            }
            else
            {
                DigiDocumentUpload = _mapper.Map<DigiDocumentUploadPostmodel, DigiDocumentUpload>(model);
                await _unitOfWork.DigiDocumentUpload.AddAsync(DigiDocumentUpload);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = DigiDocumentUpload.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentUploadSelectBoxDataViewModel), 200)]
        public async Task<DigiDocumentUploadSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.DigiDocumentUpload.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentUploadEditResponse), 200)]
        public async Task<DigiDocumentUploadEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            DigiDocumentUploadEditResponse response = new DigiDocumentUploadEditResponse();

            DigiDocumentUpload DigiDocumentUpload = await _unitOfWork.DigiDocumentUpload.GetAsync(model.RowId);

            if (DigiDocumentUpload != null)
            {
                response.Data = _mapper.Map<DigiDocumentUploadEditModel>(DigiDocumentUpload);
            }
            else
            {
                response.Data = new DigiDocumentUploadEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            DigiDocumentUpload DigiDocumentUpload = await _unitOfWork.DigiDocumentUpload.GetAsync(model.RowId);
            if (DigiDocumentUpload == null)
            {
                viewModel.CreateFailureResponse("Document not found.");
                return viewModel;
            }

            DigiDocumentUpload.IsActive = false;
            await _unitOfWork.DigiDocumentUpload.UpdateAsync(DigiDocumentUpload);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

        [HttpGet("download-doc/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Download(int id)
        {
            DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetAsync(id);
            if (DigiDocumentDetails != null)
            {
                // Get document type
                DigiDocumentType digiDocumentType = await _unitOfWork.DigiDocumentTypes.GetAsync(DigiDocumentDetails.DigiDocumentTypeId);
                var digiUploadDocuments = await _unitOfWork.DigiDocumentUpload.FindAsync(x => x.DigiDocumentDetailId == id);
                var fileList = new List<FileContentResult>();
                foreach (var item in digiUploadDocuments)
                {
                    string someUrl = _env.WebRootPath + @"\Upload\" + digiDocumentType.Title + @"\" + item.DigiDocument;
                    using (var webClient = new WebClient())
                    {
                        byte[] fileBytes = webClient.DownloadData(someUrl);
                        var mimeType = "application/....";
                        return new FileContentResult(fileBytes, mimeType)
                        {
                            FileDownloadName = item.DigiDocument
                        };
                    }
                    ////4. download from file
                    //var stream = System.IO.File.OpenRead(someUrl);

                    //if (stream == null)
                    //{
                    //    return NotFound();
                    //}

                    //return File(stream, "application/octet-stream", item.DigiDocument);
                }
            }
            return null;
        }

        [HttpGet("share-doc/{id}")]
        [AllowAnonymous]
        public async Task<string> ShareDocument(int id)
        {
            DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetAsync(id);
            if (DigiDocumentDetails != null)
            {
                // Get document type
                DigiDocumentType digiDocumentType = await _unitOfWork.DigiDocumentTypes.GetAsync(DigiDocumentDetails.DigiDocumentTypeId);
                var digiUploadDocuments = await _unitOfWork.DigiDocumentUpload.FindAsync(x => x.DigiDocumentDetailId == id);
                var fileList = new List<FileContentResult>();
                foreach (var item in digiUploadDocuments)
                {
                    string someUrl = @"/Upload/" + digiDocumentType.Title + @"/" + item.DigiDocument;
                    return someUrl;
                }
            }
            return string.Empty;
        }
    }
}

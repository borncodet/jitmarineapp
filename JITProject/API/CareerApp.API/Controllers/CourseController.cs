﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CourseController : BaseController
    {
        private readonly IMapper _mapper;

        public CourseController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CourseController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CourseDataResultModel), 200)]
        public async Task<CourseDataResultModel> GetAllActiveAsync([FromBody]CourseDataRequestModel model)
        {
            IPagedList<Course> data = await _unitOfWork.Courses.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<CourseDataResultModel, CourseInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(CourseDataResultModel), 200)]
        public async Task<CourseDataResultModel> GetAllInactiveAsync([FromBody]CourseDataRequestModel model)
        {
            IPagedList<Course> data = await _unitOfWork.Courses.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<CourseDataResultModel, CourseInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CourseEditModel), 200)]
        public async Task<CourseEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Course item = await _unitOfWork.Courses.GetAsync(model.RowId);
            CourseEditModel viewModel = _mapper.Map<CourseEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CoursePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Course item = await _unitOfWork.Courses.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Course is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Courses.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Courses.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<CoursePostmodel, Course>(model);
                await _unitOfWork.Courses.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Course item = await _unitOfWork.Courses.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Course not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Courses.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

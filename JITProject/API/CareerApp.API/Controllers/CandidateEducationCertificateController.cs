﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateEducationCertificateController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateEducationCertificateController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateEducationCertificateController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateEducationCertificateDataResultModel), 200)]
        public async Task<CandidateEducationCertificateDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateEducationCertificateInfo> data = await _unitOfWork.CandidateEducationCertificates.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateEducationCertificateDataResultModel, CandidateEducationCertificateInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] CandidateEducationCertificatePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateEducationCertificate CandidateEducationCertificate = await _unitOfWork.CandidateEducationCertificates.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.EducationQualificationId.Equals(model.EducationQualificationId) && x.IsActive == true);

            if (CandidateEducationCertificate != null && CandidateEducationCertificate.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate education certification already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                model.Certificate = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\EducationCertificate\", model.Document);
            }

            if (model.RowId != 0)
            {
                CandidateEducationCertificate = await _unitOfWork.CandidateEducationCertificates.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateEducationCertificate != null)
            {
                CandidateEducationCertificate = _mapper.Map(model, CandidateEducationCertificate);
                await _unitOfWork.CandidateEducationCertificates.UpdateAsync(CandidateEducationCertificate);
            }
            else
            {
                CandidateEducationCertificate = _mapper.Map<CandidateEducationCertificatePostmodel, CandidateEducationCertificate>(model);
                await _unitOfWork.CandidateEducationCertificates.AddAsync(CandidateEducationCertificate);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateEducationCertificate.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateEducationCertificateSelectBoxDataViewModel), 200)]
        public async Task<CandidateEducationCertificateSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateEducationCertificates.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateEducationCertificateEditResponse), 200)]
        public async Task<CandidateEducationCertificateEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateEducationCertificateEditResponse response = new CandidateEducationCertificateEditResponse();

            CandidateEducationCertificate CandidateEducationCertificate = await _unitOfWork.CandidateEducationCertificates.GetAsync(model.RowId);

            if (CandidateEducationCertificate != null)
            {
                response.Data = _mapper.Map<CandidateEducationCertificateEditModel>(CandidateEducationCertificate);
            }
            else
            {
                response.Data = new CandidateEducationCertificateEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateEducationCertificate CandidateEducationCertificate = await _unitOfWork.CandidateEducationCertificates.GetAsync(model.RowId);
            if (CandidateEducationCertificate == null)
            {
                viewModel.CreateFailureResponse("Candidate education certification not found.");
                return viewModel;
            }

            CandidateEducationCertificate.IsActive = false;
            await _unitOfWork.CandidateEducationCertificates.UpdateAsync(CandidateEducationCertificate);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

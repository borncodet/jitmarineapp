﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class StateController : BaseController
    {
        private readonly IMapper _mapper;

        public StateController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<StateController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(StateDataResultModel), 200)]
        public async Task<StateDataResultModel> GetAllActiveAsync([FromBody]StateDataRequestModel model)
        {
            IPagedList<State> data = await _unitOfWork.States.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<StateDataResultModel, StateInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(StateDataResultModel), 200)]
        public async Task<StateDataResultModel> GetAllInactiveAsync([FromBody]StateDataRequestModel model)
        {
            IPagedList<State> data = await _unitOfWork.States.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<StateDataResultModel, StateInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(StateEditModel), 200)]
        public async Task<StateEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            State item = await _unitOfWork.States.GetAsync(model.RowId);
            StateEditModel viewModel = _mapper.Map<StateEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] StatePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            State item = await _unitOfWork.States.GetFirstOrDefaultAsync(x => x.Name.ToLower().Equals(model.Name.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("State is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.States.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.States.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<StatePostmodel, State>(model);
                await _unitOfWork.States.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            State item = await _unitOfWork.States.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("State not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.States.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

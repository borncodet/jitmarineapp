﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class MartialStatusController : BaseController
    {
        private readonly IMapper _mapper;

        public MartialStatusController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<MartialStatusController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(MartialStatusDataResultModel), 200)]
        public async Task<MartialStatusDataResultModel> GetAllActiveAsync([FromBody]MartialStatusDataRequestModel model)
        {
            IPagedList<MartialStatus> data = await _unitOfWork.MartialStatus.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<MartialStatusDataResultModel, MartialStatusInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(MartialStatusDataResultModel), 200)]
        public async Task<MartialStatusDataResultModel> GetAllInactiveAsync([FromBody]MartialStatusDataRequestModel model)
        {
            IPagedList<MartialStatus> data = await _unitOfWork.MartialStatus.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<MartialStatusDataResultModel, MartialStatusInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(MartialStatusEditModel), 200)]
        public async Task<MartialStatusEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            MartialStatus item = await _unitOfWork.MartialStatus.GetAsync(model.RowId);
            MartialStatusEditModel viewModel = _mapper.Map<MartialStatusEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] MartialStatusPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            MartialStatus item = await _unitOfWork.MartialStatus.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("MartialStatus is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.MartialStatus.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.MartialStatus.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<MartialStatusPostmodel, MartialStatus>(model);
                await _unitOfWork.MartialStatus.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            MartialStatus item = await _unitOfWork.MartialStatus.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("MartialStatus not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.MartialStatus.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

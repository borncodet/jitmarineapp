﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobPostAlertController : BaseController
    {
        private readonly IMapper _mapper;

        public JobPostAlertController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobPostAlertController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobPostAlertDataResultModel), 200)]
        public async Task<JobPostAlertDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<JobPostAlertInfo> data = await _unitOfWork.JobPostAlert.GetAllAsync(model);
            return PopulateResponseWithoutMap<JobPostAlertDataResultModel, JobPostAlertInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobPostAlertPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobPostAlerts JobPostAlert = await _unitOfWork.JobPostAlert.GetFirstOrDefaultAsync(x => x.UserId.Equals(model.UserId) && x.Keywords.Equals(model.Keywords) && x.IsActive == true);

            if (JobPostAlert != null && JobPostAlert.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Alert already created.");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobPostAlert = await _unitOfWork.JobPostAlert.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobPostAlert != null)
            {
                JobPostAlert = _mapper.Map(model, JobPostAlert);
                await _unitOfWork.JobPostAlert.UpdateAsync(JobPostAlert);
            }
            else
            {
                JobPostAlert = _mapper.Map<JobPostAlertPostmodel, JobPostAlerts>(model);
                await _unitOfWork.JobPostAlert.AddAsync(JobPostAlert);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobPostAlert.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobPostAlertSelectBoxDataViewModel), 200)]
        public async Task<JobPostAlertSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobPostAlert.GetSelectBoxData();
        }

        [Route("get-title")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<ValueCaptionPair>), 200)]
        public async Task<IEnumerable<ValueCaptionPair>> GetTitleByCategory(TitleRequestModel model)
        {
            return await _unitOfWork.JobPostAlert.GetTitleByCategory(model);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobPostAlertEditResponse), 200)]
        public async Task<JobPostAlertEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobPostAlertEditResponse response = new JobPostAlertEditResponse();

            JobPostAlerts JobPostAlert = await _unitOfWork.JobPostAlert.GetAsync(model.RowId);

            if (JobPostAlert != null)
            {
                response.Data = _mapper.Map<JobPostAlertEditModel>(JobPostAlert);
            }
            else
            {
                response.Data = new JobPostAlertEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobPostAlerts JobPostAlert = await _unitOfWork.JobPostAlert.GetAsync(model.RowId);
            if (JobPostAlert == null)
            {
                viewModel.CreateFailureResponse("Job alert not found.");
                return viewModel;
            }

            JobPostAlert.IsActive = false;
            await _unitOfWork.JobPostAlert.UpdateAsync(JobPostAlert);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

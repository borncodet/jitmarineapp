﻿/// <summary>
/// APIs to manage Employer
/// </summary>
using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class EmployerController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ILogger<EmployerController> _logger;

        /// <summary>
        /// Employer Constructor
        /// Dependency Injection is done here - use parameter based dependency injection
        /// </summary>
        public EmployerController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<EmployerController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
            _logger = logger;
        }


        /// <summary>
        /// Get all active employers list
        /// Api path will be api/employer/gaaa
        /// </summary>
        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(EmployerDataResultModel), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<EmployerDataResultModel> GetAllActiveAsync([FromBody]EmployerDataRequestModel model)
        {
            _logger.LogInformation("Calling api/employer/gaaa api to get all active employer list.");
            IPagedList<EmployerInfo> data = await _unitOfWork.Employers.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<EmployerDataResultModel, EmployerInfo>(data);
        }

        /// <summary>
        /// Create or update an employer
        /// Api path will be api/employer/coea
        /// </summary>
        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] EmployerPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();
            _logger.LogInformation("Gets emplyer details by company name.");
            Employer Employer = await _unitOfWork.Employers.GetFirstOrDefaultAsync(x => x.CompanyName.ToLower().Equals(model.CompanyName.ToLower()) && x.IsActive == true);

            var user = User.Identity.Name;

            if (Employer != null && Employer.RowId != model.RowId)
            {
                _logger.LogWarning("Create a failure message - company name already exist.");
                viewModel.CreateFailureResponse("CompanyName is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                _logger.LogInformation("Gets employer details by RowId.");
                Employer = await _unitOfWork.Employers.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (Employer != null)
            {
                _logger.LogInformation("Update employer details.");
                Employer = _mapper.Map(model, Employer);
                await _unitOfWork.Employers.UpdateAsync(Employer);
            }
            else
            {
                _logger.LogInformation("Maps employer post model with employee model.");
                Employer = _mapper.Map<EmployerPostmodel, Employer>(model);
                _logger.LogInformation("Add employer entity to dbcontext.");
                await _unitOfWork.Employers.AddAsync(Employer);
            }

            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = Employer.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        /// <summary>
        /// Get the lists of items going to be used by employer
        /// Api path will be api/employer/get-selectbox-data
        /// </summary>
        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(EmployerSelectBoxDataViewModel), 200)]
        public async Task<EmployerSelectBoxDataViewModel> GetSelectboxData()
        {
            _logger.LogInformation("Gets all the dropdown list to be displayed in the employer page.");
            return await _unitOfWork.Employers.GetSelectBoxData();
        }

        /// <summary>
        /// Get an employer details by id
        /// Api path will be api/employer/ga
        /// </summary>
        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(EmployerEditResponse), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<EmployerEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            EmployerEditResponse response = new EmployerEditResponse();

            _logger.LogInformation("Gets employer details by RowId.");
            Employer Employer = await _unitOfWork.Employers.GetAsync(model.RowId);

            if (Employer != null)
            {
                _logger.LogInformation("Sets the retrieved employer details to the response data.");
                response.Data = _mapper.Map<EmployerEditModel>(Employer);
            }
            else
            {
                response.Data = new EmployerEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        /// <summary>
        /// Delete an employer by id
        /// Api path will be api/employer/da
        /// </summary>
        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            _logger.LogInformation("Gets employer details by RowId.");
            Employer Employer = await _unitOfWork.Employers.GetAsync(model.RowId);
            if (Employer == null)
            {
                _logger.LogWarning("Create a failure message - Employer not found.");
                viewModel.CreateFailureResponse("Employer not found.");
                return viewModel;
            }

            _logger.LogInformation("Update employer details - change empoyer's IsActive field to false.");
            Employer.IsActive = false;
            await _unitOfWork.Employers.UpdateAsync(Employer);
            _logger.LogInformation("Save changes to the database.");
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

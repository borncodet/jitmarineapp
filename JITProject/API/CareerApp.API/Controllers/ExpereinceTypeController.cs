﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ExpereinceTypeController : BaseController
    {
        private readonly IMapper _mapper;

        public ExpereinceTypeController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ExpereinceTypeController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ExpereinceTypeDataResultModel), 200)]
        public async Task<ExpereinceTypeDataResultModel> GetAllActiveAsync([FromBody]ExpereinceTypeDataRequestModel model)
        {
            IPagedList<ExpereinceType> data = await _unitOfWork.ExpereinceTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<ExpereinceTypeDataResultModel, ExpereinceTypeInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(ExpereinceTypeDataResultModel), 200)]
        public async Task<ExpereinceTypeDataResultModel> GetAllInactiveAsync([FromBody]ExpereinceTypeDataRequestModel model)
        {
            IPagedList<ExpereinceType> data = await _unitOfWork.ExpereinceTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<ExpereinceTypeDataResultModel, ExpereinceTypeInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ExpereinceTypeEditModel), 200)]
        public async Task<ExpereinceTypeEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            ExpereinceType item = await _unitOfWork.ExpereinceTypes.GetAsync(model.RowId);
            ExpereinceTypeEditModel viewModel = _mapper.Map<ExpereinceTypeEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ExpereinceTypePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            model.Title = string.Empty;

            if (model.FromYear < 0 || model.ToYear < 0)
            {
                viewModel.CreateFailureResponse("Invalid year!");
                return viewModel;
            }
            if (model.FromYear > model.ToYear && model.ToYear != 0)
            {
                viewModel.CreateFailureResponse("Invalid year!");
                return viewModel;
            }
            if (model.FromMonth > 12 || model.ToMonth > 12 || model.FromMonth < 0 || model.ToMonth < 0)
            {
                viewModel.CreateFailureResponse("Invalid month!");
                return viewModel;
            }

            if (model.FromYear > 0)
            {
                model.Title += model.FromYear + " Years ";
            }
            if (model.FromMonth > 0)
            {
                model.Title += model.FromMonth + " Months ";
            }
            if (model.FromDay > 0)
            {
                model.Title += model.FromDay + " Days ";
            }

            if (!string.IsNullOrEmpty(model.Title) && !string.IsNullOrWhiteSpace(model.Title))
            {
                model.Title += " - ";
            }
            else
            {
                model.Title += "< ";
            }

            if (model.ToYear > 0)
            {
                model.Title += model.ToYear + " Years ";
            }
            if (model.ToMonth > 0)
            {
                model.Title += model.ToMonth + " Months ";
            }
            if (model.ToDay > 0)
            {
                model.Title += model.ToDay + " Days ";
            }

            if (model.Title == "< ")
            {
                model.Title = "0 Years ";
            }
            if (model.Title.EndsWith(" - "))
            {
                model.Title = "> " + model.Title.Split(" - ")[0];
            }

            ExpereinceType item = await _unitOfWork.ExpereinceTypes.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("ExpereinceType is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.ExpereinceTypes.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.ExpereinceTypes.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<ExpereinceTypePostmodel, ExpereinceType>(model);
                await _unitOfWork.ExpereinceTypes.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ExpereinceType item = await _unitOfWork.ExpereinceTypes.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("ExpereinceType not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.ExpereinceTypes.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

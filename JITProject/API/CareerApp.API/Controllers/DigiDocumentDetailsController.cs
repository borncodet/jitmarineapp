﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DigiDocumentDetailsController : BaseController
    {
        private readonly IMapper _mapper;

        public DigiDocumentDetailsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DigiDocumentDetailsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        //Get all digi document details - for candidate, admin
        [Route("gaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentDetailsDataResultModel), 200)]
        public async Task<DigiDocumentDetailsDataResultModel> GetAllAsync([FromBody]BaseCandidateSearchViewModel model)
        {
            IPagedList<DigiDocumentDetailsInfo> data = await _unitOfWork.DigiDocumentDetails.GetAllAsync(model);
            return PopulateResponseWithoutMap<DigiDocumentDetailsDataResultModel, DigiDocumentDetailsInfo>(data);
        }

        //Get all expiring digi document details - for candidate, admin
        [Route("geda")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentDetailsDataResultModel), 200)]
        public async Task<DigiDocumentDetailsDataResultModel> GetExpiryDocumentAsync([FromBody]BaseCandidateSearchViewModel model)
        {
            IPagedList<DigiDocumentDetailsInfo> data = await _unitOfWork.DigiDocumentDetails.GetExpiryDocumentAsync(model);
            return PopulateResponseWithoutMap<DigiDocumentDetailsDataResultModel, DigiDocumentDetailsInfo>(data);
        }

        //Get count of all digi document - for candidate, admin
        [Route("gdca")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentCountDataResultModel), 200)]
        public async Task<DigiDocumentCountDataResultModel> GetDigiDocumentCountAsync([FromBody]BaseCandidateSearchViewModel model)
        {
            IPagedList<DigiDocumentCountInfo> data = await _unitOfWork.DigiDocumentDetails.GetDigiDocumentCountAsync(model);
            return PopulateResponseWithoutMap<DigiDocumentCountDataResultModel, DigiDocumentCountInfo>(data);
        }

        //Get count of all expiring digi document - for candidate, admin
        [Route("gedca")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentCountDataResultModel), 200)]
        public async Task<DigiDocumentCountDataResultModel> GetExpiryDocumentCountAsync([FromBody]BaseCandidateSearchViewModel model)
        {
            IPagedList<DigiDocumentCountInfo> data = await _unitOfWork.DigiDocumentDetails.GetExpiryDocumentCountAsync(model);
            return PopulateResponseWithoutMap<DigiDocumentCountDataResultModel, DigiDocumentCountInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] DigiDocumentDetailsPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.Name.Equals(model.Name) && x.IsActive.Equals(true));

            if (DigiDocumentDetails != null && DigiDocumentDetails.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Document already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (DigiDocumentDetails != null)
            {
                DigiDocumentDetails = _mapper.Map(model, DigiDocumentDetails);
                await _unitOfWork.DigiDocumentDetails.UpdateAsync(DigiDocumentDetails);
            }
            else
            {
                DigiDocumentDetails = _mapper.Map<DigiDocumentDetailsPostmodel, DigiDocumentDetails>(model);
                await _unitOfWork.DigiDocumentDetails.AddAsync(DigiDocumentDetails);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = DigiDocumentDetails.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("doc-move")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DocumentMoveAsync([FromBody] DigiDocumentDetailsMultiPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetFirstOrDefaultAsync(x => x.DigiDocumentTypeId.Equals(model.DigiDocumentTypeId));

            if (model.RowId != null)
            {
                foreach (var item in model.RowId)
                {
                    DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetFirstOrDefaultAsync(x => x.RowId == item);
                    if (DigiDocumentDetails != null)
                    {
                        //Get source document type details
                        DigiDocumentType sourceDigiDocumentType = await _unitOfWork.DigiDocumentTypes.GetAsync(DigiDocumentDetails.DigiDocumentTypeId);
                        //Get source document details
                        var sourceDigiUploadDocuments = await _unitOfWork.DigiDocumentUpload.FindAsync(x => x.DigiDocumentDetailId == DigiDocumentDetails.RowId);
                        foreach (var sourceDigiFile in sourceDigiUploadDocuments)
                        {
                            string sourceFile = _env.WebRootPath + @"/Upload/" + sourceDigiDocumentType.Title + @"/" + sourceDigiFile.DigiDocument;
                            if (System.IO.File.Exists(sourceFile))
                            {
                                //Getting destination document type details
                                DigiDocumentType destinationDigiDocumentType = await _unitOfWork.DigiDocumentTypes.GetAsync(model.DigiDocumentTypeId);
                                //Setting destination document path
                                string destinationFile = _env.WebRootPath + @"/Upload/" + destinationDigiDocumentType.Title + @"/" + sourceDigiFile.DigiDocument;
                                //Moving file from source to destination
                                System.IO.File.Move(sourceFile, destinationFile);
                            }
                        }
                        DigiDocumentDetails.DigiDocumentTypeId = model.DigiDocumentTypeId;
                        await _unitOfWork.DigiDocumentDetails.UpdateAsync(DigiDocumentDetails);
                    }

                }
                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                viewModel.CreateFailureResponse("Please select document to move!");
                return viewModel;
            }

            viewModel.CreateSuccessResponse();
            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentDetailsSelectBoxDataViewModel), 200)]
        public async Task<DigiDocumentDetailsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.DigiDocumentDetails.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentDetailsEditResponse), 200)]
        public async Task<DigiDocumentDetailsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {

            DigiDocumentDetailsEditResponse response = new DigiDocumentDetailsEditResponse();
            response.Data = await _unitOfWork.DigiDocumentDetails.GetDigiDocumentDetailsById(model.RowId);

            //DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetAsync(model.RowId);

            //if (DigiDocumentDetails != null)
            //{
            //    response.Data = _mapper.Map<DigiDocumentDetailsEditModel>(DigiDocumentDetails);
            //}
            //else
            //{
            //    response.Data = new DigiDocumentDetailsEditModel();
            //}

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            DigiDocumentDetails DigiDocumentDetails = await _unitOfWork.DigiDocumentDetails.GetAsync(model.RowId);
            if (DigiDocumentDetails == null)
            {
                viewModel.CreateFailureResponse("Document not found.");
                return viewModel;
            }

            DigiDocumentDetails.IsActive = false;
            await _unitOfWork.DigiDocumentDetails.UpdateAsync(DigiDocumentDetails);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobAppliedDetailsController : BaseController
    {
        private readonly IMapper _mapper;

        public JobAppliedDetailsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobAppliedDetailsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        //Get applied job list - for admin(brief)
        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDetailsDataResultModel), 200)]
        public async Task<JobAppliedDetailsDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<JobAppliedDetailsInfo> data = await _unitOfWork.JobAppliedDetails.GetAllAsync(model);
            return PopulateResponseWithoutMap<JobAppliedDetailsDataResultModel, JobAppliedDetailsInfo>(data);
        }

        //Get applied job list - for candidate(detailed)
        [Route("graj")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDetailsCandidateDataResultModel), 200)]
        public async Task<JobAppliedDetailsCandidateDataResultModel> GetRecentlyAppliedJobAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            //var candidate = await GetCurrentCandidateId();
            //model.CandidateId = candidate.CandidateId;
            IPagedList<RecentlyAppliedJobDetailsInfo> data = await _unitOfWork.JobAppliedDetails.GetRecentlyAppliedJobAsync(model);
            return PopulateResponseWithoutMap<JobAppliedDetailsCandidateDataResultModel, RecentlyAppliedJobDetailsInfo>(data);
        }

        //Get applied job details
        [Route("gjad")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedAndDocInfo), 200)]
        public async Task<JobAppliedAndDocInfo> GetJobAppliedDetailsAync([FromBody]DataSourceCandidateJobAppliedRequestModel model)
        {
            return await _unitOfWork.JobAppliedDetails.GetJobAppliedDetails(model);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] JobAppliedDataPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobAppliedDetails JobAppliedDetails = await _unitOfWork.JobAppliedDetails.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.JobAppliedDetailsPostmodel.CandidateId) && x.JobId.Equals(model.JobAppliedDetailsPostmodel.JobId) && x.IsActive == true);

            if (model.JobAppliedDetailsPostmodel.ResumeCandidateMapId == null && model.JobAppliedDetailsPostmodel.CoverLetterCandidateMapId == null && model.JobAppliedDetailsPostmodel.ResumeDocumentFile == null && model.JobAppliedDetailsPostmodel.CoverLetterDocumentFile == null)
            {
                viewModel.CreateFailureResponse("Either choose/upload resume or cover letter.");
                return viewModel;
            }
            else if (JobAppliedDetails != null && JobAppliedDetails.RowId != model.JobAppliedDetailsPostmodel.RowId)
            {
                // Candidate already applied for this job, candidate job applied RowId should be passed to update candidate job applied details
                viewModel.CreateFailureResponse("Candidate already applied for this job.");
                return viewModel;
            }
            else
            {
                //Uploading Resume 
                if (model.JobAppliedDetailsPostmodel.ResumeDocumentFile != null)
                {
                    model.JobAppliedDetailsPostmodel.ResumeDocument = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\ResumeUpload", model.JobAppliedDetailsPostmodel.ResumeDocumentFile);
                }
                //Uploading Coverletted 
                if (model.JobAppliedDetailsPostmodel.CoverLetterDocumentFile != null)
                {
                    model.JobAppliedDetailsPostmodel.CoverLetterDocument = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\CoverLetterUpload", model.JobAppliedDetailsPostmodel.CoverLetterDocumentFile);
                }

                if (model.JobAppliedDetailsPostmodel.RowId != 0)
                {
                    JobAppliedDetails = await _unitOfWork.JobAppliedDetails.GetFirstOrDefaultAsync(x => x.RowId == model.JobAppliedDetailsPostmodel.RowId);
                }

                if (JobAppliedDetails != null)
                {
                    JobAppliedDetails = _mapper.Map(model.JobAppliedDetailsPostmodel, JobAppliedDetails);
                    await _unitOfWork.JobAppliedDetails.UpdateAsync(JobAppliedDetails);
                }
                else
                {
                    JobAppliedDetails = _mapper.Map<JobAppliedDetailsPostmodel, JobAppliedDetails>(model.JobAppliedDetailsPostmodel);
                    await _unitOfWork.JobAppliedDetails.AddAsync(JobAppliedDetails);
                }

                await InsertOrUpdateDoc(JobAppliedDetails.RowId, model.JobAppliedDigiDocMapPostmodel);
                await _unitOfWork.SaveChangesAsync();
            }

            viewModel.EntityId = JobAppliedDetails.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpGet]
        public async Task InsertOrUpdateDoc(int JobAppliedDetailsId, List<JobAppliedDigiDocMapPostmodel> JobAppliedDigiDocMapPostmodel)
        {
            // Select all documents currently mapped to the job applied
            var JobAppliedDigiDocMapList = await _unitOfWork.JobAppliedDigiDocMap.FindAsync(x => x.JobAppliedDetailsId.Equals(JobAppliedDetailsId));
            if (JobAppliedDigiDocMapList != null)
            {
                // Remove currently mapped document
                await _unitOfWork.JobAppliedDigiDocMap.RemoveRangeAsync(JobAppliedDigiDocMapList);
            }

            if (JobAppliedDigiDocMapPostmodel != null)
            {
                // Insert or update new document
                foreach (var doc in JobAppliedDigiDocMapPostmodel)
                {

                    doc.JobAppliedDetailsId = JobAppliedDetailsId;
                    JobAppliedDigiDocMap JobAppliedDigiDocMap = await _unitOfWork.JobAppliedDigiDocMap.GetFirstOrDefaultAsync(x => x.JobAppliedDetailsId.Equals(JobAppliedDetailsId) && x.DigiDocumentDetailId.Equals(doc.DigiDocumentDetailId));
                    JobAppliedDigiDocMap = _mapper.Map<JobAppliedDigiDocMapPostmodel, JobAppliedDigiDocMap>(doc);
                    await _unitOfWork.JobAppliedDigiDocMap.AddAsync(JobAppliedDigiDocMap);
                }
            }
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDetailsSelectBoxDataViewModel), 200)]
        public async Task<JobAppliedDetailsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobAppliedDetails.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobAppliedDetailsEditResponse), 200)]
        public async Task<JobAppliedDetailsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobAppliedDetailsEditResponse response = new JobAppliedDetailsEditResponse();

            JobAppliedDetails JobAppliedDetails = await _unitOfWork.JobAppliedDetails.GetAsync(model.RowId);

            if (JobAppliedDetails != null)
            {
                response.Data = _mapper.Map<JobAppliedDetailsEditModel>(JobAppliedDetails);
            }
            else
            {
                response.Data = new JobAppliedDetailsEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobAppliedDetails JobAppliedDetails = await _unitOfWork.JobAppliedDetails.GetAsync(model.RowId);
            if (JobAppliedDetails == null)
            {
                viewModel.CreateFailureResponse("Candidate applied job not found.");
                return viewModel;
            }

            JobAppliedDetails.IsActive = false;
            await _unitOfWork.JobAppliedDetails.UpdateAsync(JobAppliedDetails);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateLanguageMapController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateLanguageMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateLanguageMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateLanguageMapDataResultModel), 200)]
        public async Task<CandidateLanguageMapDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateLanguageMapInfo> data = await _unitOfWork.CandidateLanguageMap.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateLanguageMapDataResultModel, CandidateLanguageMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateLanguageMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            var CandidateLanguageMapList = await _unitOfWork.CandidateLanguageMap.FindAsync(x => x.CandidateId.Equals(model.CandidateId) && x.IsActive == true);

            if (CandidateLanguageMapList != null)
            {
                await _unitOfWork.CandidateLanguageMap.RemoveRangeAsync(CandidateLanguageMapList);
            }

            foreach (var languageId in model.LanguageId)
            {
                CandidateLanguageMap candidateLanguageMap = new CandidateLanguageMap();
                candidateLanguageMap.CandidateLanguageMapId = model.CandidateLanguageMapId;
                candidateLanguageMap.CandidateId = model.CandidateId;
                candidateLanguageMap.LanguageId = languageId;
                candidateLanguageMap.IsActive = true;
                await _unitOfWork.CandidateLanguageMap.AddAsync(candidateLanguageMap);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse();

            return viewModel;

            //CommonEntityResponse viewModel = new CommonEntityResponse();

            //CandidateLanguageMap CandidateLanguageMap = await _unitOfWork.CandidateLanguageMap.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.LanguageId.Equals(model.LanguageId));

            //if (CandidateLanguageMap != null && CandidateLanguageMap.RowId != model.RowId)
            //{
            //    viewModel.CreateFailureResponse("Candidate already mapped to the specified language!");
            //    return viewModel;
            //}

            //if (model.RowId != 0)
            //{
            //    CandidateLanguageMap = await _unitOfWork.CandidateLanguageMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            //}

            //if (CandidateLanguageMap != null)
            //{
            //    CandidateLanguageMap = _mapper.Map(model, CandidateLanguageMap);
            //    await _unitOfWork.CandidateLanguageMap.UpdateAsync(CandidateLanguageMap);
            //}
            //else
            //{
            //    CandidateLanguageMap = _mapper.Map<CandidateLanguageMapPostmodel, CandidateLanguageMap>(model);
            //    await _unitOfWork.CandidateLanguageMap.AddAsync(CandidateLanguageMap);
            //}
            //await _unitOfWork.SaveChangesAsync();

            //viewModel.EntityId = CandidateLanguageMap.RowId;
            //viewModel.CreateSuccessResponse();

            //return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateLanguageMapSelectBoxDataViewModel), 200)]
        public async Task<CandidateLanguageMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateLanguageMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateLanguageMapEditResponse), 200)]
        public async Task<CandidateLanguageMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateLanguageMapEditResponse response = new CandidateLanguageMapEditResponse();

            CandidateLanguageMap CandidateLanguageMap = await _unitOfWork.CandidateLanguageMap.GetAsync(model.RowId);

            if (CandidateLanguageMap != null)
            {
                response.Data = _mapper.Map<CandidateLanguageMapEditModel>(CandidateLanguageMap);
            }
            else
            {
                response.Data = new CandidateLanguageMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateLanguageMap CandidateLanguageMap = await _unitOfWork.CandidateLanguageMap.GetAsync(model.RowId);
            if (CandidateLanguageMap == null)
            {
                viewModel.CreateFailureResponse("Candidate mapped language not found.");
                return viewModel;
            }

            CandidateLanguageMap.IsActive = false;
            await _unitOfWork.CandidateLanguageMap.UpdateAsync(CandidateLanguageMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobBookmarkedController : BaseController
    {
        private readonly IMapper _mapper;

        public JobBookmarkedController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobBookmarkedController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobBookmarkedDataResultModel), 200)]
        public async Task<JobBookmarkedDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<JobBookmarkedInfo> data = await _unitOfWork.JobBookmarked.GetAllAsync(model);
            return PopulateResponseWithoutMap<JobBookmarkedDataResultModel, JobBookmarkedInfo>(data);
        }

        [Route("grsj")]
        [HttpPost]
        [ProducesResponseType(typeof(JobBookmarkedCandidateDataResultModel), 200)]
        public async Task<JobBookmarkedCandidateDataResultModel> GetRecentlySavedJobAsync([FromBody]JobAppliedCandidateDataRequestModel model)
        {
            IPagedList<RecentlySavedJobInfo> data = await _unitOfWork.JobBookmarked.GetRecentlySavedJobAsync(model.CandidateId, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobBookmarkedCandidateDataResultModel, RecentlySavedJobInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobBookmarkedPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobBookmarked JobBookmarked = await _unitOfWork.JobBookmarked.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.JobId.Equals(model.JobId) && x.IsActive == true);

            if (JobBookmarked != null && JobBookmarked.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate already bookmarked this job!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobBookmarked = await _unitOfWork.JobBookmarked.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobBookmarked != null)
            {
                JobBookmarked = _mapper.Map(model, JobBookmarked);
                await _unitOfWork.JobBookmarked.UpdateAsync(JobBookmarked);
            }
            else
            {
                JobBookmarked = _mapper.Map<JobBookmarkedPostmodel, JobBookmarked>(model);
                await _unitOfWork.JobBookmarked.AddAsync(JobBookmarked);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobBookmarked.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobBookmarkedSelectBoxDataViewModel), 200)]
        public async Task<JobBookmarkedSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobBookmarked.GetSelectBoxData();
        }

        [Route("gjsc")]
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)]
        public async Task<int> GetJobSavedCountAsync(BaseCandidateSearchViewModel model)
        {
            return await _unitOfWork.JobBookmarked.GetJobSavedCountAsync(model);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobBookmarkedEditResponse), 200)]
        public async Task<JobBookmarkedEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobBookmarkedEditResponse response = new JobBookmarkedEditResponse();

            JobBookmarked JobBookmarked = await _unitOfWork.JobBookmarked.GetAsync(model.RowId);

            if (JobBookmarked != null)
            {
                response.Data = _mapper.Map<JobBookmarkedEditModel>(JobBookmarked);
            }
            else
            {
                response.Data = new JobBookmarkedEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobBookmarked JobBookmarked = await _unitOfWork.JobBookmarked.GetAsync(model.RowId);
            if (JobBookmarked == null)
            {
                viewModel.CreateFailureResponse("Candidate bookmarked job not found.");
                return viewModel;
            }

            JobBookmarked.IsActive = false;
            await _unitOfWork.JobBookmarked.UpdateAsync(JobBookmarked);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }


        [HttpPost]
        [Route("nba")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> NotboomarkAsync([FromBody]Jobnotbookmarkmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobBookmarked JobBookmarked = await _unitOfWork.JobBookmarked.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.JobId.Equals(model.JobId) && x.IsActive == true);
            if (JobBookmarked == null)
            {
                viewModel.CreateFailureResponse("Candidate bookmarked job not found.");
                return viewModel;
            }
            //Deleting the bookmark entry
            await _unitOfWork.JobBookmarked.RemoveAsync(JobBookmarked);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobBookmarked.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateAchievementsController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateAchievementsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateAchievementsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateAchievementsDataResultModel), 200)]
        public async Task<CandidateAchievementsDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateAchievementsInfo> data = await _unitOfWork.CandidateAchievements.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateAchievementsDataResultModel, CandidateAchievementsInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateAchievementsPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateAchievements CandidateAchievements = await _unitOfWork.CandidateAchievements.GetFirstOrDefaultAsync(x => x.Title.Equals(model.Title) && x.CandidateExperienceId.Equals(model.CandidateExperienceId) && x.IsActive == true);
            if (CandidateAchievements != null && CandidateAchievements.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate achievement already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                CandidateAchievements = await _unitOfWork.CandidateAchievements.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateAchievements != null)
            {
                CandidateAchievements = _mapper.Map(model, CandidateAchievements);
                await _unitOfWork.CandidateAchievements.UpdateAsync(CandidateAchievements);
            }
            else
            {
                CandidateAchievements = _mapper.Map<CandidateAchievementsPostmodel, CandidateAchievements>(model);
                await _unitOfWork.CandidateAchievements.AddAsync(CandidateAchievements);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateAchievements.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateAchievementsSelectBoxDataViewModel), 200)]
        public async Task<CandidateAchievementsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateAchievements.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateAchievementsEditResponse), 200)]
        public async Task<CandidateAchievementsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateAchievementsEditResponse response = new CandidateAchievementsEditResponse();

            CandidateAchievements CandidateAchievements = await _unitOfWork.CandidateAchievements.GetAsync(model.RowId);

            if (CandidateAchievements != null)
            {
                response.Data = _mapper.Map<CandidateAchievementsEditModel>(CandidateAchievements);
            }
            else
            {
                response.Data = new CandidateAchievementsEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateAchievements CandidateAchievements = await _unitOfWork.CandidateAchievements.GetAsync(model.RowId);
            if (CandidateAchievements == null)
            {
                viewModel.CreateFailureResponse("Candidate achievement not found.");
                return viewModel;
            }

            CandidateAchievements.IsActive = false;
            await _unitOfWork.CandidateAchievements.UpdateAsync(CandidateAchievements);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

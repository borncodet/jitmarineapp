﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DesignationController : BaseController
    {
        private readonly IMapper _mapper;

        public DesignationController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DesignationController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DesignationDataResultModel), 200)]
        public async Task<DesignationDataResultModel> GetAllActiveAsync([FromBody]DesignationDataRequestModel model)
        {
            IPagedList<Designation> data = await _unitOfWork.Designations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<DesignationDataResultModel, DesignationInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(DesignationDataResultModel), 200)]
        public async Task<DesignationDataResultModel> GetAllInactiveAsync([FromBody]DesignationDataRequestModel model)
        {
            IPagedList<Designation> data = await _unitOfWork.Designations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<DesignationDataResultModel, DesignationInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DesignationEditModel), 200)]
        public async Task<DesignationEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            Designation item = await _unitOfWork.Designations.GetAsync(model.RowId);
            DesignationEditModel viewModel = _mapper.Map<DesignationEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] DesignationPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            Designation item = await _unitOfWork.Designations.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Designation is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.Designations.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.Designations.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<DesignationPostmodel, Designation>(model);
                await _unitOfWork.Designations.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            Designation item = await _unitOfWork.Designations.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Designation not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.Designations.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

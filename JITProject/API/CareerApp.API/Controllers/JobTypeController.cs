﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobTypeController : BaseController
    {
        private readonly IMapper _mapper;

        public JobTypeController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobTypeController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobTypeDataResultModel), 200)]
        public async Task<JobTypeDataResultModel> GetAllActiveAsync([FromBody]JobTypeDataRequestModel model)
        {
            IPagedList<JobType> data = await _unitOfWork.JobTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<JobTypeDataResultModel, JobTypeInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(JobTypeDataResultModel), 200)]
        public async Task<JobTypeDataResultModel> GetAllInactiveAsync([FromBody]JobTypeDataRequestModel model)
        {
            IPagedList<JobType> data = await _unitOfWork.JobTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<JobTypeDataResultModel, JobTypeInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobTypeEditModel), 200)]
        public async Task<JobTypeEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            JobType item = await _unitOfWork.JobTypes.GetAsync(model.RowId);
            JobTypeEditModel viewModel = _mapper.Map<JobTypeEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobTypePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobType item = await _unitOfWork.JobTypes.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("JobType is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.JobTypes.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.JobTypes.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<JobTypePostmodel, JobType>(model);
                await _unitOfWork.JobTypes.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobType item = await _unitOfWork.JobTypes.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("JobType not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.JobTypes.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

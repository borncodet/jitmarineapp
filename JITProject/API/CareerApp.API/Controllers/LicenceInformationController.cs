﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class LicenceInformationController : BaseController
    {
        private readonly IMapper _mapper;

        public LicenceInformationController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<LicenceInformationController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(LicenceInformationDataResultModel), 200)]
        public async Task<LicenceInformationDataResultModel> GetAllActiveAsync([FromBody]LicenceInformationDataRequestModel model)
        {
            IPagedList<LicenceInformation> data = await _unitOfWork.LicenceInformations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<LicenceInformationDataResultModel, LicenceInformationInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(LicenceInformationDataResultModel), 200)]
        public async Task<LicenceInformationDataResultModel> GetAllInactiveAsync([FromBody]LicenceInformationDataRequestModel model)
        {
            IPagedList<LicenceInformation> data = await _unitOfWork.LicenceInformations.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<LicenceInformationDataResultModel, LicenceInformationInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(LicenceInformationEditModel), 200)]
        public async Task<LicenceInformationEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            LicenceInformation item = await _unitOfWork.LicenceInformations.GetAsync(model.RowId);
            LicenceInformationEditModel viewModel = _mapper.Map<LicenceInformationEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] LicenceInformationPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            LicenceInformation item = await _unitOfWork.LicenceInformations.GetFirstOrDefaultAsync(x => x.LicenceNo.ToLower().Equals(model.LicenceNo.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("LicenceInformation is already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                model.Licence = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\Licence\", model.Document);
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.LicenceInformations.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.LicenceInformations.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<LicenceInformationPostmodel, LicenceInformation>(model);
                await _unitOfWork.LicenceInformations.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            LicenceInformation item = await _unitOfWork.LicenceInformations.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("LicenceInformation not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.LicenceInformations.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }

        //[HttpGet("download-doc/{id}")]
        //[AllowAnonymous]
        //public async Task<IActionResult> Download(int id)
        //{
        //    LicenceInformation item = await _unitOfWork.LicenceInformations.GetFirstOrDefaultAsync(x => x.RowId == id);

        //    string someUrl = _env.WebRootPath + @"\Upload\Licence\" + item.Licence;
        //    using (var webClient = new WebClient())
        //    {
        //        byte[] fileBytes = webClient.DownloadData(someUrl);
        //        var mimeType = "application/....";

        //        return new FileContentResult(fileBytes, mimeType)
        //        {
        //            FileDownloadName = item.Licence
        //        };
        //    }
        //}
    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateExperienceController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateExperienceController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateExperienceController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateExperienceDataResultModel), 200)]
        public async Task<CandidateExperienceDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateExperienceInfo> data = await _unitOfWork.CandidateExperiences.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateExperienceDataResultModel, CandidateExperienceInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateExperiencePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateExperience CandidateExperience = await _unitOfWork.CandidateExperiences.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.EmployerName.Equals(model.EmployerName) && x.FromDate.Equals(model.FromDate) && x.ToDate.Equals(model.ToDate) && x.IsActive == true);

            if (CandidateExperience != null && CandidateExperience.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate experience already exist!");
                return viewModel;
            }

            CandidateExperience = await _unitOfWork.CandidateExperiences.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && model.FromDate >= x.FromDate && model.FromDate <= x.ToDate && x.IsActive==true);

            if (CandidateExperience != null && CandidateExperience.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate experience already exist for the given date!");
                return viewModel;
            }

            CandidateExperience = await _unitOfWork.CandidateExperiences.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && model.ToDate >= x.FromDate && model.ToDate <= x.ToDate && x.IsActive == true);

            if (CandidateExperience != null && CandidateExperience.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate experience already exist for the given date!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                CandidateExperience = await _unitOfWork.CandidateExperiences.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateExperience != null)
            {
                CandidateExperience = _mapper.Map(model, CandidateExperience);
                await _unitOfWork.CandidateExperiences.UpdateAsync(CandidateExperience);
            }
            else
            {
                CandidateExperience = _mapper.Map<CandidateExperiencePostmodel, CandidateExperience>(model);
                await _unitOfWork.CandidateExperiences.AddAsync(CandidateExperience);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateExperience.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateExperienceSelectBoxDataViewModel), 200)]
        public async Task<CandidateExperienceSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateExperiences.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateExperienceEditResponse), 200)]
        public async Task<CandidateExperienceEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateExperienceEditResponse response = new CandidateExperienceEditResponse();

            CandidateExperience CandidateExperience = await _unitOfWork.CandidateExperiences.GetAsync(model.RowId);

            if (CandidateExperience != null)
            {
                response.Data = _mapper.Map<CandidateExperienceEditModel>(CandidateExperience);
            }
            else
            {
                response.Data = new CandidateExperienceEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateExperience CandidateExperience = await _unitOfWork.CandidateExperiences.GetAsync(model.RowId);
            if (CandidateExperience == null)
            {
                viewModel.CreateFailureResponse("Candidate experience not found.");
                return viewModel;
            }

            CandidateExperience.IsActive = false;
            await _unitOfWork.CandidateExperiences.UpdateAsync(CandidateExperience);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

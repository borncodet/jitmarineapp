﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class RequiredDocumentTypeController : BaseController
    {
        private readonly IMapper _mapper;

        public RequiredDocumentTypeController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<RequiredDocumentTypeController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(RequiredDocumentTypeDataResultModel), 200)]
        public async Task<RequiredDocumentTypeDataResultModel> GetAllActiveAsync([FromBody]RequiredDocumentTypeDataRequestModel model)
        {
            IPagedList<RequiredDocumentType> data = await _unitOfWork.RequiredDocumentTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<RequiredDocumentTypeDataResultModel, RequiredDocumentTypeInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(RequiredDocumentTypeDataResultModel), 200)]
        public async Task<RequiredDocumentTypeDataResultModel> GetAllInactiveAsync([FromBody]RequiredDocumentTypeDataRequestModel model)
        {
            IPagedList<RequiredDocumentType> data = await _unitOfWork.RequiredDocumentTypes.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<RequiredDocumentTypeDataResultModel, RequiredDocumentTypeInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(RequiredDocumentTypeEditModel), 200)]
        public async Task<RequiredDocumentTypeEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            RequiredDocumentType item = await _unitOfWork.RequiredDocumentTypes.GetAsync(model.RowId);
            RequiredDocumentTypeEditModel viewModel = _mapper.Map<RequiredDocumentTypeEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] RequiredDocumentTypePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            RequiredDocumentType item = await _unitOfWork.RequiredDocumentTypes.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Required Document Type is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.RequiredDocumentTypes.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.RequiredDocumentTypes.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<RequiredDocumentTypePostmodel, RequiredDocumentType>(model);
                await _unitOfWork.RequiredDocumentTypes.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            RequiredDocumentType item = await _unitOfWork.RequiredDocumentTypes.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("Required Document Type not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.RequiredDocumentTypes.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

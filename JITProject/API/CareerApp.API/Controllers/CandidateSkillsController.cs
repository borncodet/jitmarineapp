﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateSkillsController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateSkillsController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateSkillsController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateSkillsDataResultModel), 200)]
        public async Task<CandidateSkillsDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateSkillsInfo> data = await _unitOfWork.CandidateSkills.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateSkillsDataResultModel, CandidateSkillsInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] CandidateSkillsPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateSkills CandidateSkills = await _unitOfWork.CandidateSkills.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.SkillName.Equals(model.SkillName) && x.IsActive == true);

            if (CandidateSkills != null && CandidateSkills.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate skill already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                CandidateSkills = await _unitOfWork.CandidateSkills.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateSkills != null)
            {
                CandidateSkills = _mapper.Map(model, CandidateSkills);
                await _unitOfWork.CandidateSkills.UpdateAsync(CandidateSkills);
            }
            else
            {
                CandidateSkills = _mapper.Map<CandidateSkillsPostmodel, CandidateSkills>(model);
                await _unitOfWork.CandidateSkills.AddAsync(CandidateSkills);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateSkills.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateSkillsSelectBoxDataViewModel), 200)]
        public async Task<CandidateSkillsSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateSkills.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateSkillsEditResponse), 200)]
        public async Task<CandidateSkillsEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateSkillsEditResponse response = new CandidateSkillsEditResponse();

            CandidateSkills CandidateSkills = await _unitOfWork.CandidateSkills.GetAsync(model.RowId);

            if (CandidateSkills != null)
            {
                response.Data = _mapper.Map<CandidateSkillsEditModel>(CandidateSkills);
            }
            else
            {
                response.Data = new CandidateSkillsEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateSkills CandidateSkills = await _unitOfWork.CandidateSkills.GetAsync(model.RowId);
            if (CandidateSkills == null)
            {
                viewModel.CreateFailureResponse("Candidate skill not found.");
                return viewModel;
            }

            CandidateSkills.IsActive = false;
            await _unitOfWork.CandidateSkills.UpdateAsync(CandidateSkills);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

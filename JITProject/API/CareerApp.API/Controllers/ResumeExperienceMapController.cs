﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ResumeExperienceMapController : BaseController
    {
        private readonly IMapper _mapper;

        public ResumeExperienceMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ResumeExperienceMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeExperienceMapDataResultModel), 200)]
        public async Task<ResumeExperienceMapDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<ResumeExperienceMapInfo> data = await _unitOfWork.ResumeExperienceMap.GetAllAsync(model);
            return PopulateResponseWithoutMap<ResumeExperienceMapDataResultModel, ResumeExperienceMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ResumeExperienceMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ResumeExperienceMap ResumeExperienceMap = await _unitOfWork.ResumeExperienceMap.GetFirstOrDefaultAsync(x => x.ExpereinceTypeId.Equals(model.ExpereinceTypeId) && x.ResumeTemplateId.Equals(model.ResumeTemplateId) && x.IsActive == true);

            if (ResumeExperienceMap != null && ResumeExperienceMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Experience already mapped to the specified resume!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                ResumeExperienceMap = await _unitOfWork.ResumeExperienceMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (ResumeExperienceMap != null)
            {
                ResumeExperienceMap = _mapper.Map(model, ResumeExperienceMap);
                await _unitOfWork.ResumeExperienceMap.UpdateAsync(ResumeExperienceMap);
            }
            else
            {
                ResumeExperienceMap = _mapper.Map<ResumeExperienceMapPostmodel, ResumeExperienceMap>(model);
                await _unitOfWork.ResumeExperienceMap.AddAsync(ResumeExperienceMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ResumeExperienceMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeExperienceMapSelectBoxDataViewModel), 200)]
        public async Task<ResumeExperienceMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.ResumeExperienceMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ResumeExperienceMapEditResponse), 200)]
        public async Task<ResumeExperienceMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            ResumeExperienceMapEditResponse response = new ResumeExperienceMapEditResponse();

            ResumeExperienceMap ResumeExperienceMap = await _unitOfWork.ResumeExperienceMap.GetAsync(model.RowId);

            if (ResumeExperienceMap != null)
            {
                response.Data = _mapper.Map<ResumeExperienceMapEditModel>(ResumeExperienceMap);
            }
            else
            {
                response.Data = new ResumeExperienceMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ResumeExperienceMap ResumeExperienceMap = await _unitOfWork.ResumeExperienceMap.GetAsync(model.RowId);
            if (ResumeExperienceMap == null)
            {
                viewModel.CreateFailureResponse("Experience mapped resume not found.");
                return viewModel;
            }

            ResumeExperienceMap.IsActive = false;
            await _unitOfWork.ResumeExperienceMap.UpdateAsync(ResumeExperienceMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class TrainingDurationMapController : BaseController
    {
        private readonly IMapper _mapper;

        public TrainingDurationMapController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<TrainingDurationMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingDurationMapDataResultModel), 200)]
        public async Task<TrainingDurationMapDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<TrainingDurationMapInfo> data = await _unitOfWork.TrainingDurationMaps.GetAllAsync(model);
            return PopulateResponseWithoutMap<TrainingDurationMapDataResultModel, TrainingDurationMapInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] TrainingDurationMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            TrainingDurationMap TrainingDurationMap = await _unitOfWork.TrainingDurationMaps.GetFirstOrDefaultAsync(x => x.TrainingId.Equals(model.TrainingId) && x.DurationId.Equals(model.DurationId) && x.IsActive == true);

            if (TrainingDurationMap != null && TrainingDurationMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Training duration map already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                TrainingDurationMap = await _unitOfWork.TrainingDurationMaps.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (TrainingDurationMap != null)
            {
                TrainingDurationMap = _mapper.Map(model, TrainingDurationMap);
                await _unitOfWork.TrainingDurationMaps.UpdateAsync(TrainingDurationMap);
            }
            else
            {
                TrainingDurationMap = _mapper.Map<TrainingDurationMapPostmodel, TrainingDurationMap>(model);
                await _unitOfWork.TrainingDurationMaps.AddAsync(TrainingDurationMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = TrainingDurationMap.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingDurationMapSelectBoxDataViewModel), 200)]
        public async Task<TrainingDurationMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.TrainingDurationMaps.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingDurationMapEditResponse), 200)]
        public async Task<TrainingDurationMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            TrainingDurationMapEditResponse response = new TrainingDurationMapEditResponse();

            TrainingDurationMap TrainingDurationMap = await _unitOfWork.TrainingDurationMaps.GetAsync(model.RowId);

            if (TrainingDurationMap != null)
            {
                response.Data = _mapper.Map<TrainingDurationMapEditModel>(TrainingDurationMap);
            }
            else
            {
                response.Data = new TrainingDurationMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            TrainingDurationMap TrainingDurationMap = await _unitOfWork.TrainingDurationMaps.GetAsync(model.RowId);
            if (TrainingDurationMap == null)
            {
                viewModel.CreateFailureResponse("Training duration map not found.");
                return viewModel;
            }

            TrainingDurationMap.IsActive = false;
            await _unitOfWork.TrainingDurationMaps.UpdateAsync(TrainingDurationMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class DigiDocumentSharedController : BaseController
    {
        private readonly IMapper _mapper;

        public DigiDocumentSharedController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<DigiDocumentSharedController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentSharedDataResultModel), 200)]
        public async Task<DigiDocumentSharedDataResultModel> GetAllActiveAsync([FromBody]BaseCandidateSearchViewModel model)
        {
            IPagedList<DigiDocumentSharedInfo> data = await _unitOfWork.DigiDocumentShared.GetAllAsync(model);
            return PopulateResponseWithoutMap<DigiDocumentSharedDataResultModel, DigiDocumentSharedInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] DigiDocumentSharedPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            DigiDocumentShared DigiDocumentShared = await _unitOfWork.DigiDocumentShared.GetFirstOrDefaultAsync(x => x.DigiDocumentDetailId.Equals(model.DigiDocumentDetailId) && x.SharedThrough.Equals(model.SharedThrough) && x.IsActive == true);

            //if (DigiDocumentShared != null && DigiDocumentShared.RowId != model.RowId)
            //{
            //    viewModel.CreateFailureResponse("Document already exist!");
            //    return viewModel;
            //}

            if (model.RowId != 0)
            {
                DigiDocumentShared = await _unitOfWork.DigiDocumentShared.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (DigiDocumentShared != null)
            {
                DigiDocumentShared = _mapper.Map(model, DigiDocumentShared);
                await _unitOfWork.DigiDocumentShared.UpdateAsync(DigiDocumentShared);
            }
            else
            {
                DigiDocumentShared = _mapper.Map<DigiDocumentSharedPostmodel, DigiDocumentShared>(model);
                await _unitOfWork.DigiDocumentShared.AddAsync(DigiDocumentShared);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = DigiDocumentShared.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentSharedSelectBoxDataViewModel), 200)]
        public async Task<DigiDocumentSharedSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.DigiDocumentShared.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(DigiDocumentSharedEditResponse), 200)]
        public async Task<DigiDocumentSharedEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            DigiDocumentSharedEditResponse response = new DigiDocumentSharedEditResponse();

            DigiDocumentShared DigiDocumentShared = await _unitOfWork.DigiDocumentShared.GetAsync(model.RowId);

            if (DigiDocumentShared != null)
            {
                response.Data = _mapper.Map<DigiDocumentSharedEditModel>(DigiDocumentShared);
            }
            else
            {
                response.Data = new DigiDocumentSharedEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            DigiDocumentShared DigiDocumentShared = await _unitOfWork.DigiDocumentShared.GetAsync(model.RowId);
            if (DigiDocumentShared == null)
            {
                viewModel.CreateFailureResponse("Document shared not found.");
                return viewModel;
            }

            DigiDocumentShared.IsActive = false;
            await _unitOfWork.DigiDocumentShared.UpdateAsync(DigiDocumentShared);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

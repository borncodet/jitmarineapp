﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class FieldOfExpertiseController : BaseController
    {
        private readonly IMapper _mapper;

        public FieldOfExpertiseController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<FieldOfExpertiseController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(FieldOfExpertiseDataResultModel), 200)]
        public async Task<FieldOfExpertiseDataResultModel> GetAllActiveAsync([FromBody]FieldOfExpertiseDataRequestModel model)
        {
            IPagedList<FieldOfExpertise> data = await _unitOfWork.FieldOfExpertises.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<FieldOfExpertiseDataResultModel, FieldOfExpertiseInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(FieldOfExpertiseDataResultModel), 200)]
        public async Task<FieldOfExpertiseDataResultModel> GetAllInactiveAsync([FromBody]FieldOfExpertiseDataRequestModel model)
        {
            IPagedList<FieldOfExpertise> data = await _unitOfWork.FieldOfExpertises.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<FieldOfExpertiseDataResultModel, FieldOfExpertiseInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(FieldOfExpertiseEditModel), 200)]
        public async Task<FieldOfExpertiseEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            FieldOfExpertise item = await _unitOfWork.FieldOfExpertises.GetAsync(model.RowId);
            FieldOfExpertiseEditModel viewModel = _mapper.Map<FieldOfExpertiseEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] FieldOfExpertisePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            FieldOfExpertise item = await _unitOfWork.FieldOfExpertises.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("FieldOfExpertise is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.FieldOfExpertises.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.FieldOfExpertises.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<FieldOfExpertisePostmodel, FieldOfExpertise>(model);
                await _unitOfWork.FieldOfExpertises.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            FieldOfExpertise item = await _unitOfWork.FieldOfExpertises.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("FieldOfExpertise not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.FieldOfExpertises.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

﻿using AutoMapper;
using CareerApp.API.Controllers;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AudioBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobRoleController : BaseController
    {
        private readonly IMapper _mapper;

        public JobRoleController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobRoleController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobRoleDataResultModel), 200)]
        public async Task<JobRoleDataResultModel> GetAllActiveAsync([FromBody]JobRoleDataRequestModel model)
        {
            IPagedList<JobRole> data = await _unitOfWork.JobRoles.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithMap<JobRoleDataResultModel, JobRoleInfo>(data);
        }

        [Route("gaia")]
        [HttpPost]
        [ProducesResponseType(typeof(JobRoleDataResultModel), 200)]
        public async Task<JobRoleDataResultModel> GetAllInactiveAsync([FromBody]JobRoleDataRequestModel model)
        {
            IPagedList<JobRole> data = await _unitOfWork.JobRoles.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, true);
            return PopulateResponseWithMap<JobRoleDataResultModel, JobRoleInfo>(data);
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobRoleEditModel), 200)]
        public async Task<JobRoleEditModel> GetAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            JobRole item = await _unitOfWork.JobRoles.GetAsync(model.RowId);
            JobRoleEditModel viewModel = _mapper.Map<JobRoleEditModel>(item);
            return viewModel;
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobRolePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobRole item = await _unitOfWork.JobRoles.GetFirstOrDefaultAsync(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.IsActive == true);

            if (item != null && item.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("JobRole is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                item = await _unitOfWork.JobRoles.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (item != null)
            {
                item = _mapper.Map(model, item);
                await _unitOfWork.JobRoles.UpdateAsync(item);
            }
            else
            {
                item = _mapper.Map<JobRolePostmodel, JobRole>(model);
                await _unitOfWork.JobRoles.AddAsync(item);
            }

            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobRole item = await _unitOfWork.JobRoles.GetAsync(model.RowId);
            if (item == null)
            {
                viewModel.CreateFailureResponse("JobRole not found.");
                return viewModel;
            }

            item.IsActive = false;
            await _unitOfWork.JobRoles.UpdateAsync(item);
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = item.RowId;
            viewModel.CreateSuccessResponse();
            return viewModel;
        }
    }
}

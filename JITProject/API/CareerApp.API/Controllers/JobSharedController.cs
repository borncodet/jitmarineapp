﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobSharedController : BaseController
    {
        private readonly IMapper _mapper;

        public JobSharedController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobSharedController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobSharedDataResultModel), 200)]
        public async Task<JobSharedDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<JobSharedInfo> data = await _unitOfWork.JobShared.GetAllAsync(model);
            return PopulateResponseWithoutMap<JobSharedDataResultModel, JobSharedInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobSharedPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobShared JobShared = await _unitOfWork.JobShared.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.JobId.Equals(model.JobId) && x.IsActive == true);

            //if (JobShared != null && JobShared.RowId != model.RowId)
            //{
            //    viewModel.CreateFailureResponse("Candidate already shared this job!");
            //    return viewModel;
            //}

            if (model.RowId != 0)
            {
                JobShared = await _unitOfWork.JobShared.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobShared != null)
            {
                JobShared = _mapper.Map(model, JobShared);
                await _unitOfWork.JobShared.UpdateAsync(JobShared);
            }
            else
            {
                JobShared = _mapper.Map<JobSharedPostmodel, JobShared>(model);
                await _unitOfWork.JobShared.AddAsync(JobShared);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobShared.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobSharedSelectBoxDataViewModel), 200)]
        public async Task<JobSharedSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobShared.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobSharedEditResponse), 200)]
        public async Task<JobSharedEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobSharedEditResponse response = new JobSharedEditResponse();

            JobShared JobShared = await _unitOfWork.JobShared.GetAsync(model.RowId);

            if (JobShared != null)
            {
                response.Data = _mapper.Map<JobSharedEditModel>(JobShared);
            }
            else
            {
                response.Data = new JobSharedEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobShared JobShared = await _unitOfWork.JobShared.GetAsync(model.RowId);
            if (JobShared == null)
            {
                viewModel.CreateFailureResponse("Candidate bookmarked job not found.");
                return viewModel;
            }

            JobShared.IsActive = false;
            await _unitOfWork.JobShared.UpdateAsync(JobShared);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

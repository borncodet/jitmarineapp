﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class VendorDocumentController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _env;

        public VendorDocumentController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<VendorDocumentController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
            _env = env;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorDocumentDataResultModel), 200)]
        public async Task<VendorDocumentDataResultModel> GetAllActiveAsync([FromBody]VendorDocumentDataRequestModel model)
        {
            IPagedList<VendorDocumentInfo> data = await _unitOfWork.VendorDocuments.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<VendorDocumentDataResultModel, VendorDocumentInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] VendorDocumentPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            VendorDocument VendorDocument = await _unitOfWork.VendorDocuments.GetFirstOrDefaultAsync(x => x.VendorId == model.VendorId && x.IsActive == true);

            //Uploading Files 
            List<string> fileNames = new List<string>();
            if (model.Documents != null)
            {
                fileNames = await _unitOfWork.Medias.SaveMultipleFile(_env.WebRootPath + @"\Upload\VendorDocument\", model.Documents);
            }

            switch (fileNames.Count)
            {
                case 3:
                    model.Document1 = fileNames[0];
                    model.Document2 = fileNames[1];
                    model.Document3 = fileNames[2];
                    break;
                case 2:
                    model.Document1 = fileNames[0];
                    model.Document2 = fileNames[1];
                    break;
                default:
                    model.Document1 = fileNames[0];
                    break;
            }

            if (model.RowId != 0)
            {
                VendorDocument = await _unitOfWork.VendorDocuments.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (VendorDocument != null)
            {
                VendorDocument = _mapper.Map(model, VendorDocument);
                await _unitOfWork.VendorDocuments.UpdateAsync(VendorDocument);
            }
            else
            {
                VendorDocument = _mapper.Map<VendorDocumentPostmodel, VendorDocument>(model);
                await _unitOfWork.VendorDocuments.AddAsync(VendorDocument);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = VendorDocument.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorDocumentSelectBoxDataViewModel), 200)]
        public async Task<VendorDocumentSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.VendorDocuments.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(VendorDocumentEditResponse), 200)]
        public async Task<VendorDocumentEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            VendorDocumentEditResponse response = new VendorDocumentEditResponse();

            VendorDocument VendorDocument = await _unitOfWork.VendorDocuments.GetAsync(model.RowId);

            if (VendorDocument != null)
            {
                response.Data = _mapper.Map<VendorDocumentEditModel>(VendorDocument);
            }
            else
            {
                response.Data = new VendorDocumentEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            VendorDocument VendorDocument = await _unitOfWork.VendorDocuments.GetAsync(model.RowId);
            if (VendorDocument == null)
            {
                viewModel.CreateFailureResponse("VendorDocument not found.");
                return viewModel;
            }

            VendorDocument.IsActive = false;
            await _unitOfWork.VendorDocuments.UpdateAsync(VendorDocument);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

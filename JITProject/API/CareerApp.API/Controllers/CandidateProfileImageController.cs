﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CandidateProfileImageController : BaseController
    {
        private readonly IMapper _mapper;

        public CandidateProfileImageController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<CandidateProfileImageController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateProfileImageDataResultModel), 200)]
        public async Task<CandidateProfileImageDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<CandidateProfileImageInfo> data = await _unitOfWork.CandidateProfileImage.GetAllAsync(model);
            return PopulateResponseWithoutMap<CandidateProfileImageDataResultModel, CandidateProfileImageInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromForm] CandidateProfileImagePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            CandidateProfileImage CandidateProfileImage = await _unitOfWork.CandidateProfileImage.GetFirstOrDefaultAsync(x => x.CandidateId.Equals(model.CandidateId) && x.IsActive.Equals(true));

            if (CandidateProfileImage != null && CandidateProfileImage.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Candidate profile image already exist!");
                return viewModel;
            }

            //Uploading Files 
            if (model.Document != null)
            {
                model.ImageUrl = await _unitOfWork.Medias.SaveFile(_env.WebRootPath + @"\Upload\ProfileImage\", model.Document);
                byte[] imageArray = System.IO.File.ReadAllBytes(_env.WebRootPath + @"\Upload\ProfileImage\" + model.ImageUrl);
                model.Base64Image = Convert.ToBase64String(imageArray);
            }

            if (model.RowId != 0)
            {
                CandidateProfileImage = await _unitOfWork.CandidateProfileImage.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (CandidateProfileImage != null)
            {
                CandidateProfileImage = _mapper.Map(model, CandidateProfileImage);
                await _unitOfWork.CandidateProfileImage.UpdateAsync(CandidateProfileImage);
            }
            else
            {
                CandidateProfileImage = _mapper.Map<CandidateProfileImagePostmodel, CandidateProfileImage>(model);
                await _unitOfWork.CandidateProfileImage.AddAsync(CandidateProfileImage);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = CandidateProfileImage.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateProfileImageSelectBoxDataViewModel), 200)]
        public async Task<CandidateProfileImageSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.CandidateProfileImage.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(CandidateProfileImageEditResponse), 200)]
        public async Task<CandidateProfileImageEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            CandidateProfileImageEditResponse response = new CandidateProfileImageEditResponse();

            CandidateProfileImage CandidateProfileImage = await _unitOfWork.CandidateProfileImage.GetAsync(model.RowId);

            if (CandidateProfileImage != null)
            {
                response.Data = _mapper.Map<CandidateProfileImageEditModel>(CandidateProfileImage);
            }
            else
            {
                response.Data = new CandidateProfileImageEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            CandidateProfileImage CandidateProfileImage = await _unitOfWork.CandidateProfileImage.GetAsync(model.RowId);
            if (CandidateProfileImage == null)
            {
                viewModel.CreateFailureResponse("Candidate profile image not found.");
                return viewModel;
            }

            CandidateProfileImage.IsActive = false;
            await _unitOfWork.CandidateProfileImage.UpdateAsync(CandidateProfileImage);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

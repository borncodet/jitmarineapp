﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobCourseMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobCourseMapController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobCourseMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobCourseMapDataResultModel), 200)]
        public async Task<JobCourseMapDataResultModel> GetAllActiveAsync([FromBody]JobCourseMapDataRequestModel model)
        {
            IPagedList<JobCourseMapInfo> data = await _unitOfWork.JobCourseMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobCourseMapDataResultModel, JobCourseMapInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobCourseMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobCourseMap JobCourseMap = await _unitOfWork.JobCourseMap.GetFirstOrDefaultAsync(x => x.JobId == model.JobId && x.CourseId == model.CourseId && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobCourseMap != null && JobCourseMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Mapping is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobCourseMap = await _unitOfWork.JobCourseMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobCourseMap != null)
            {
                JobCourseMap = _mapper.Map(model, JobCourseMap);
                await _unitOfWork.JobCourseMap.UpdateAsync(JobCourseMap);
            }
            else
            {
                JobCourseMap = _mapper.Map<JobCourseMapPostmodel, JobCourseMap>(model);
                await _unitOfWork.JobCourseMap.AddAsync(JobCourseMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobCourseMap.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobCourseMapSelectBoxDataViewModel), 200)]
        public async Task<JobCourseMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobCourseMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobCourseMapEditResponse), 200)]
        public async Task<JobCourseMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobCourseMapEditResponse response = new JobCourseMapEditResponse();

            JobCourseMap JobCourseMap = await _unitOfWork.JobCourseMap.GetAsync(model.RowId);

            if (JobCourseMap != null)
            {
                response.Data = _mapper.Map<JobCourseMapEditModel>(JobCourseMap);
            }
            else
            {
                response.Data = new JobCourseMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobCourseMap JobCourseMap = await _unitOfWork.JobCourseMap.GetAsync(model.RowId);
            if (JobCourseMap == null)
            {
                viewModel.CreateFailureResponse("Mapping not found.");
                return viewModel;
            }

            JobCourseMap.IsActive = false;
            await _unitOfWork.JobCourseMap.UpdateAsync(JobCourseMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

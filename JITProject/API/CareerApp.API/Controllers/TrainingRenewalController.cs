﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class TrainingRenewalController : BaseController
    {
        private readonly IMapper _mapper;

        public TrainingRenewalController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<TrainingRenewalController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingRenewalDataResultModel), 200)]
        public async Task<TrainingRenewalDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<TrainingRenewalInfo> data = await _unitOfWork.TrainingRenewals.GetAllAsync(model);
            return PopulateResponseWithoutMap<TrainingRenewalDataResultModel, TrainingRenewalInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] TrainingRenewalPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            TrainingRenewal TrainingRenewal = await _unitOfWork.TrainingRenewals.GetFirstOrDefaultAsync(x => x.TrainingId.Equals(model.TrainingId) && x.ValidUpTo.Equals(model.ValidUpTo) && x.IsActive == true);

            if (TrainingRenewal != null && TrainingRenewal.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Training renewal already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                TrainingRenewal = await _unitOfWork.TrainingRenewals.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (TrainingRenewal != null)
            {
                TrainingRenewal = _mapper.Map(model, TrainingRenewal);
                await _unitOfWork.TrainingRenewals.UpdateAsync(TrainingRenewal);
            }
            else
            {
                TrainingRenewal = _mapper.Map<TrainingRenewalPostmodel, TrainingRenewal>(model);
                await _unitOfWork.TrainingRenewals.AddAsync(TrainingRenewal);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = TrainingRenewal.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingRenewalSelectBoxDataViewModel), 200)]
        public async Task<TrainingRenewalSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.TrainingRenewals.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(TrainingRenewalEditResponse), 200)]
        public async Task<TrainingRenewalEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            TrainingRenewalEditResponse response = new TrainingRenewalEditResponse();

            TrainingRenewal TrainingRenewal = await _unitOfWork.TrainingRenewals.GetAsync(model.RowId);

            if (TrainingRenewal != null)
            {
                response.Data = _mapper.Map<TrainingRenewalEditModel>(TrainingRenewal);
            }
            else
            {
                response.Data = new TrainingRenewalEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            TrainingRenewal TrainingRenewal = await _unitOfWork.TrainingRenewals.GetAsync(model.RowId);
            if (TrainingRenewal == null)
            {
                viewModel.CreateFailureResponse("Training renewal not found.");
                return viewModel;
            }

            TrainingRenewal.IsActive = false;
            await _unitOfWork.TrainingRenewals.UpdateAsync(TrainingRenewal);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class ProjectRoleController : BaseController
    {
        private readonly IMapper _mapper;

        public ProjectRoleController(
                    IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<ProjectRoleController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }

        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(ProjectRoleDataResultModel), 200)]
        public async Task<ProjectRoleDataResultModel> GetAllActiveAsync([FromBody]DataSourceCandidateRequestModel model)
        {
            IPagedList<ProjectRoleInfo> data = await _unitOfWork.ProjectRoles.GetAllAsync(model);
            return PopulateResponseWithoutMap<ProjectRoleDataResultModel, ProjectRoleInfo>(data);
        }

        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] ProjectRolePostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            ProjectRole ProjectRole = await _unitOfWork.ProjectRoles.GetFirstOrDefaultAsync(x => x.CandidateProjectId.Equals(model.CandidateProjectId) && x.JobRoleId.Equals(model.JobRoleId) && x.IsActive == true);

            if (ProjectRole != null && ProjectRole.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Project role already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                ProjectRole = await _unitOfWork.ProjectRoles.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (ProjectRole != null)
            {
                ProjectRole = _mapper.Map(model, ProjectRole);
                await _unitOfWork.ProjectRoles.UpdateAsync(ProjectRole);
            }
            else
            {
                ProjectRole = _mapper.Map<ProjectRolePostmodel, ProjectRole>(model);
                await _unitOfWork.ProjectRoles.AddAsync(ProjectRole);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = ProjectRole.RowId;
            viewModel.CreateSuccessResponse();



            return viewModel;
        }

        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(ProjectRoleSelectBoxDataViewModel), 200)]
        public async Task<ProjectRoleSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.ProjectRoles.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(ProjectRoleEditResponse), 200)]
        public async Task<ProjectRoleEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            ProjectRoleEditResponse response = new ProjectRoleEditResponse();

            ProjectRole ProjectRole = await _unitOfWork.ProjectRoles.GetAsync(model.RowId);

            if (ProjectRole != null)
            {
                response.Data = _mapper.Map<ProjectRoleEditModel>(ProjectRole);
            }
            else
            {
                response.Data = new ProjectRoleEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            ProjectRole ProjectRole = await _unitOfWork.ProjectRoles.GetAsync(model.RowId);
            if (ProjectRole == null)
            {
                viewModel.CreateFailureResponse("Project role not found.");
                return viewModel;
            }

            ProjectRole.IsActive = false;
            await _unitOfWork.ProjectRoles.UpdateAsync(ProjectRole);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

﻿using AutoMapper;
using CareerApp.Core.Interfaces;
using CareerApp.Core.Models;
using CareerApp.Respository.Interface;
using CareerApp.Respository.Interfaces;
using CareerApp.ViewModels;
using CareerApp.ViewModels.Shared;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CareerApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class JobShiftMapController : BaseController
    {
        private readonly IMapper _mapper;

        public JobShiftMapController(
            IHttpContextAccessor accessor,
                    IHostingEnvironment env,
                    IUnitOfWork unitOfWork,
                    IAccountManager accountManager,
                    ILogger<JobShiftMapController> logger,
                    IMapper mapper) : base(accessor, env, unitOfWork, accountManager, logger, mapper)
        {
            _mapper = mapper;
        }


        [Route("gaaa")]
        [HttpPost]
        [ProducesResponseType(typeof(JobShiftMapDataResultModel), 200)]
        public async Task<JobShiftMapDataResultModel> GetAllActiveAsync([FromBody]JobShiftMapDataRequestModel model)
        {
            IPagedList<JobShiftMapInfo> data = await _unitOfWork.JobShiftMap.GetAllAsync(model.SearchTerm, model.Page, model.PageSize, false);
            return PopulateResponseWithoutMap<JobShiftMapDataResultModel, JobShiftMapInfo>(data);
        }


        [Route("coea")]
        [HttpPost]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> CreateOrEditAsync([FromBody] JobShiftMapPostmodel model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse();

            JobShiftMap JobShiftMap = await _unitOfWork.JobShiftMap.GetFirstOrDefaultAsync(x => x.JobId == model.JobId && x.ShiftId == model.ShiftId && x.IsActive == true);

            var user = User.Identity.Name;

            if (JobShiftMap != null && JobShiftMap.RowId != model.RowId)
            {
                viewModel.CreateFailureResponse("Mapping is already exist!");
                return viewModel;
            }

            if (model.RowId != 0)
            {
                JobShiftMap = await _unitOfWork.JobShiftMap.GetFirstOrDefaultAsync(x => x.RowId == model.RowId);
            }

            if (JobShiftMap != null)
            {
                JobShiftMap = _mapper.Map(model, JobShiftMap);
                await _unitOfWork.JobShiftMap.UpdateAsync(JobShiftMap);
            }
            else
            {
                JobShiftMap = _mapper.Map<JobShiftMapPostmodel, JobShiftMap>(model);
                await _unitOfWork.JobShiftMap.AddAsync(JobShiftMap);
            }
            await _unitOfWork.SaveChangesAsync();

            viewModel.EntityId = JobShiftMap.RowId;
            viewModel.CreateSuccessResponse();

            return viewModel;
        }


        [Route("get-selectbox-data")]
        [HttpPost]
        [ProducesResponseType(typeof(JobShiftMapSelectBoxDataViewModel), 200)]
        public async Task<JobShiftMapSelectBoxDataViewModel> GetSelectboxData()
        {
            return await _unitOfWork.JobShiftMap.GetSelectBoxData();
        }

        [Route("ga")]
        [HttpPost]
        [ProducesResponseType(typeof(JobShiftMapEditResponse), 200)]
        public async Task<JobShiftMapEditResponse> GetAsync([FromBody]BaseViewModel model)
        {
            JobShiftMapEditResponse response = new JobShiftMapEditResponse();

            JobShiftMap JobShiftMap = await _unitOfWork.JobShiftMap.GetAsync(model.RowId);

            if (JobShiftMap != null)
            {
                response.Data = _mapper.Map<JobShiftMapEditModel>(JobShiftMap);
            }
            else
            {
                response.Data = new JobShiftMapEditModel();
            }

            response.CreateSuccessResponse();
            return response;
        }

        [HttpPost]
        [Route("da")]
        [ProducesResponseType(typeof(CommonEntityResponse), 200)]
        public async Task<CommonEntityResponse> DeleteAsync([FromBody]BaseViewModelWithIdRequired model)
        {
            CommonEntityResponse viewModel = new CommonEntityResponse(model.RowId);

            JobShiftMap JobShiftMap = await _unitOfWork.JobShiftMap.GetAsync(model.RowId);
            if (JobShiftMap == null)
            {
                viewModel.CreateFailureResponse("Mapping not found.");
                return viewModel;
            }

            JobShiftMap.IsActive = false;
            await _unitOfWork.JobShiftMap.UpdateAsync(JobShiftMap);
            await _unitOfWork.SaveChangesAsync();

            viewModel.CreateSuccessResponse(Convert.ToString(model.RowId));
            return viewModel;
        }

    }
}

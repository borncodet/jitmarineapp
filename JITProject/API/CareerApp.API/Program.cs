﻿using CareerApp.Core.DB.Interfaces;
using CareerApp.Shared.Helpers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;

namespace CareerApp.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                IWebHost host = CreateWebHostBuilder(args).Build();

                // 1 - use third party logging provider called serilog, not use appsettings.json
                // Log.Logger = new LoggerConfiguration()
                //.MinimumLevel.Debug() // Set the minimun log level
                //.WriteTo.File("Logs\\log-.json", rollingInterval: RollingInterval.Day) // this is for logging into file system
                //.WriteTo.Seq("http://localhost:5341/")
                //.CreateLogger();
                // Log.Information("Starting up");

                // 2 - use third party logging provider called serilog, not use appsettings.json
                //   Log.Logger = new LoggerConfiguration()
                //  .MinimumLevel.Debug()
                //  .WriteTo.Console()
                //  .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Information).WriteTo.File(@"Logs\Info-.log"))
                //  .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Debug).WriteTo.File(@"Logs\Debug-.log"))
                //  .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Warning).WriteTo.File(@"Logs\Warning-.log"))
                //  .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Error).WriteTo.File(@"Logs\Error-.log"))
                //  .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Fatal).WriteTo.File(@"Logs\Fatal-.log"))
                //  .WriteTo.File(@"Logs\Verbose-.log")
                //  .CreateLogger();

                #region Uncomment this block for Database Initialisation

                using (var scope = host.Services.CreateScope())
                {
                    var services = scope.ServiceProvider;
                    try
                    {
                        var databaseInitializer = services.GetRequiredService<IDatabaseInitializer>();
                        databaseInitializer.SeedAsync().Wait();
                    }
                    catch (Exception ex)
                    {
                        var logger = services.GetRequiredService<ILogger<Program>>();
                        logger.LogCritical(LoggingEvents.INIT_DATABASE, ex, LoggingEvents.INIT_DATABASE.Name);
                    }
                }

                #endregion

                host.Run();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(true)
                .UseSetting("detailedErrors", "true")
                .ConfigureKestrel((context, options) =>
                {
                    // Set properties and call methods on options
                })
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    IHostingEnvironment env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    //.AddJsonFile("azurekeyvault.json", optional: true, reloadOnChange: true);
                    config.AddEnvironmentVariables();

                    // 1
                    //config.addazurekeyvault("https://resource-group-keyvault.vault.azure.net/",
                    //"c187f6d8-af41-4095-852e-5c81f636c065",
                    //"902vccb-mdooz7v23koblp-ffev3-u_6~_");

                    // 2
                    //var root = config.Build();

                    //var vault = root["azureKeyVault:vault"];
                    //if (!string.IsNullOrEmpty(vault))
                    //{
                    //    config.AddAzureKeyVault(
                    //    $"https://{root["azureKeyVault:vault"]}.vault.azure.net/",
                    //    root["azureKeyVault:clientId"],
                    //    root["azureKeyVault:clientSecret"]);
                    //}

                })
                // 3 - read from appsettings.json, use inbuilt microsoft logging
                //.ConfigureLogging((hostingContext, logging) =>
                //{
                //    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                //    logging.AddConsole();
                //    logging.AddDebug();
                //    logging.AddEventSourceLogger();
                //})
                // 4 - read from appsettings.json, use third party logging provider called serilog
                .UseSerilog((hostingContext, loggerConfiguration) =>
                                             loggerConfiguration
                                             .ReadFrom.Configuration(hostingContext.Configuration))
                .UseIISIntegration()
                .UseStartup<CareerApp.API.Startup>();
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;


namespace CareerApp.API.Helpers
{
    public interface IEmailSender
    {
        Task<string> SendEmailAsync(string email, string subject, string message, string htmlContent);
    }

    public class EmailSender : IEmailSender
    {
        private readonly IConfiguration _configuration;

        public EmailSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> SendEmailAsync(string toEmail, string subject, string message, string htmlContent)
        {
            try
            {
                var apiKey = _configuration["SendGrid:ApiKey"];
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress("abhilash15102020@gmail.com", "Jitmarine");
                var to = new EmailAddress(toEmail);
                var msg = MailHelper.CreateSingleEmail(from, to, subject, message, htmlContent);
                var response = await client.SendEmailAsync(msg);
                return response.StatusCode.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}

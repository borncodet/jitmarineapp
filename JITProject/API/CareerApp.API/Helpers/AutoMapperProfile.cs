﻿using AutoMapper;
using CareerApp.Core.Models;
using CareerApp.ViewModels;

namespace CareerApp.API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CandidatePostmodel, Candidate>().ReverseMap();
            CreateMap<CandidatePostmodel, Candidate>().ReverseMap();
            CreateMap<Candidate, CandidateInfo>().ReverseMap();
            CreateMap<Candidate, CandidateEditModel>().ReverseMap();

            CreateMap<BankAccountTypePostmodel, BankAccountType>().ReverseMap();
            CreateMap<BankAccountType, BankAccountTypeInfo>().ReverseMap();
            CreateMap<BankAccountType, BankAccountTypeEditModel>().ReverseMap();

            CreateMap<BankDetailsPostmodel, BankDetails>().ReverseMap();
            CreateMap<BankDetails, BankDetailsInfo>().ReverseMap();
            CreateMap<BankDetails, BankDetailsEditModel>().ReverseMap();

            CreateMap<CandidateAchievementsPostmodel, CandidateAchievements>().ReverseMap();
            CreateMap<CandidateAchievements, CandidateAchievementsInfo>().ReverseMap();
            CreateMap<CandidateAchievements, CandidateAchievementsEditModel>().ReverseMap();

            CreateMap<CandidateEducationCertificatePostmodel, CandidateEducationCertificate>().ReverseMap();
            CreateMap<CandidateEducationCertificate, CandidateEducationCertificateInfo>().ReverseMap();
            CreateMap<CandidateEducationCertificate, CandidateEducationCertificateEditModel>().ReverseMap();

            CreateMap<CandidateExperiencePostmodel, CandidateExperience>().ReverseMap();
            CreateMap<CandidateExperience, CandidateExperienceInfo>().ReverseMap();
            CreateMap<CandidateExperience, CandidateExperienceEditModel>().ReverseMap();

            CreateMap<CandidateOtherCertificatePostmodel, CandidateOtherCertificate>().ReverseMap();
            CreateMap<CandidateOtherCertificate, CandidateOtherCertificateInfo>().ReverseMap();
            CreateMap<CandidateOtherCertificate, CandidateOtherCertificateEditModel>().ReverseMap();

            CreateMap<CandidateProfileImagePostmodel, CandidateProfileImage>().ReverseMap();
            CreateMap<CandidateProfileImage, CandidateProfileImageInfo>().ReverseMap();
            CreateMap<CandidateProfileImage, CandidateProfileImageEditModel>().ReverseMap();

            CreateMap<CandidateProjectsPostmodel, CandidateProjects>().ReverseMap();
            CreateMap<CandidateProjects, CandidateProjectsInfo>().ReverseMap();
            CreateMap<CandidateProjects, CandidateProjectsEditModel>().ReverseMap();

            CreateMap<CandidateRelativesPostmodel, CandidateRelatives>().ReverseMap();
            CreateMap<CandidateRelatives, CandidateRelativesInfo>().ReverseMap();
            CreateMap<CandidateRelatives, CandidateRelativesEditModel>().ReverseMap();

            CreateMap<CandidateReferencesPostmodel, CandidateReferences>().ReverseMap();
            CreateMap<CandidateReferences, CandidateReferencesInfo>().ReverseMap();
            CreateMap<CandidateReferences, CandidateReferencesEditModel>().ReverseMap();

            CreateMap<CandidateSkillsPostmodel, CandidateSkills>().ReverseMap();
            CreateMap<CandidateSkills, CandidateSkillsInfo>().ReverseMap();
            CreateMap<CandidateSkills, CandidateSkillsEditModel>().ReverseMap();

            CreateMap<EducationQualificationPostmodel, EducationQualification>().ReverseMap();
            CreateMap<EducationQualification, EducationQualificationInfo>().ReverseMap();
            CreateMap<EducationQualification, EducationQualificationEditModel>().ReverseMap();

            CreateMap<ProjectRolePostmodel, ProjectRole>().ReverseMap();
            CreateMap<ProjectRole, ProjectRoleInfo>().ReverseMap();
            CreateMap<ProjectRole, ProjectRoleEditModel>().ReverseMap();

            CreateMap<RelationshipPostmodel, Relationship>().ReverseMap();
            CreateMap<Relationship, RelationshipInfo>().ReverseMap();
            CreateMap<Relationship, RelationshipEditModel>().ReverseMap();

            CreateMap<SocialAccountsPostmodel, SocialAccounts>().ReverseMap();
            CreateMap<SocialAccounts, SocialAccountsInfo>().ReverseMap();
            CreateMap<SocialAccounts, SocialAccountsEditModel>().ReverseMap();

            CreateMap<TrainingPostmodel, Training>().ReverseMap();
            CreateMap<Training, TrainingInfo>().ReverseMap();
            CreateMap<Training, TrainingEditModel>().ReverseMap();

            CreateMap<TrainingDurationMapPostmodel, TrainingDurationMap>().ReverseMap();
            CreateMap<TrainingDurationMap, TrainingDurationMapInfo>().ReverseMap();
            CreateMap<TrainingDurationMap, TrainingDurationMapEditModel>().ReverseMap();

            CreateMap<TrainingRenewalPostmodel, TrainingRenewal>().ReverseMap();
            CreateMap<TrainingRenewal, TrainingRenewalInfo>().ReverseMap();
            CreateMap<TrainingRenewal, TrainingRenewalEditModel>().ReverseMap();

            CreateMap<UniversityPostmodel, University>().ReverseMap();
            CreateMap<University, UniversityInfo>().ReverseMap();
            CreateMap<University, UniversityEditModel>().ReverseMap();

            CreateMap<DurationPostmodel, Duration>().ReverseMap();
            CreateMap<Duration, DurationInfo>().ReverseMap();
            CreateMap<Duration, DurationEditModel>().ReverseMap();

            CreateMap<DatePostedPostmodel, DatePosted>().ReverseMap();
            CreateMap<DatePosted, DatePostedInfo>().ReverseMap();
            CreateMap<DatePosted, DatePostedEditModel>().ReverseMap();

            CreateMap<ProficiencyPostmodel, Proficiency>().ReverseMap();
            CreateMap<Proficiency, ProficiencyInfo>().ReverseMap();
            CreateMap<Proficiency, ProficiencyEditModel>().ReverseMap();

            CreateMap<JobAppliedPostmodel, JobApplied>().ReverseMap();
            CreateMap<JobApplied, JobAppliedInfo>().ReverseMap();
            CreateMap<JobApplied, JobAppliedEditModel>().ReverseMap();

            CreateMap<JobAppliedDetailsPostmodel, JobAppliedDetails>().ReverseMap();
            CreateMap<JobAppliedDetails, JobAppliedDetailsInfo>().ReverseMap();
            CreateMap<JobAppliedDetails, JobAppliedDetailsEditModel>().ReverseMap();

            CreateMap<JobAppliedDigiDocMapPostmodel, JobAppliedDigiDocMap>().ReverseMap();
            CreateMap<JobAppliedDigiDocMap, JobAppliedDigiDocMapInfo>().ReverseMap();
            CreateMap<JobAppliedDigiDocMap, JobAppliedDigiDocMapEditModel>().ReverseMap();

            CreateMap<JobBookmarkedPostmodel, JobBookmarked>().ReverseMap();
            CreateMap<JobBookmarked, JobBookmarkedInfo>().ReverseMap();
            CreateMap<JobBookmarked, JobBookmarkedEditModel>().ReverseMap();

            CreateMap<JobSharedPostmodel, JobShared>().ReverseMap();
            CreateMap<JobShared, JobSharedInfo>().ReverseMap();
            CreateMap<JobShared, JobSharedEditModel>().ReverseMap();

            CreateMap<JobPostAlertPostmodel, JobPostAlerts>().ReverseMap();
            CreateMap<JobPostAlerts, JobPostAlertInfo>().ReverseMap();
            CreateMap<JobPostAlerts, JobPostAlertEditModel>().ReverseMap();

            CreateMap<JobAlertTypeMapPostmodel, JobAlertTypeMap>().ReverseMap();
            CreateMap<JobAlertTypeMap, JobAlertTypeMapInfo>().ReverseMap();
            CreateMap<JobAlertTypeMap, JobAlertTypeMapEditModel>().ReverseMap();

            CreateMap<JobAlertRoleMapPostmodel, JobAlertRoleMap>().ReverseMap();
            CreateMap<JobAlertRoleMap, JobAlertRoleMapInfo>().ReverseMap();
            CreateMap<JobAlertRoleMap, JobAlertRoleMapEditModel>().ReverseMap();

            CreateMap<JobAlertIndustryMapPostmodel, JobAlertIndustryMap>().ReverseMap();
            CreateMap<JobAlertIndustryMap, JobAlertIndustryMapInfo>().ReverseMap();
            CreateMap<JobAlertIndustryMap, JobAlertIndustryMapEditModel>().ReverseMap();

            CreateMap<JobAlertExperienceMapPostmodel, JobAlertExperienceMap>().ReverseMap();
            CreateMap<JobAlertExperienceMap, JobAlertExperienceMapInfo>().ReverseMap();
            CreateMap<JobAlertExperienceMap, JobAlertExperienceMapEditModel>().ReverseMap();

            CreateMap<JobAlertCategoryMapPostmodel, JobAlertCategoryMap>().ReverseMap();
            CreateMap<JobAlertCategoryMap, JobAlertCategoryMapInfo>().ReverseMap();
            CreateMap<JobAlertCategoryMap, JobAlertCategoryMapEditModel>().ReverseMap();

            CreateMap<CandidateLanguageMapPostmodel, CandidateLanguageMap>().ReverseMap();
            CreateMap<CandidateLanguageMap, CandidateLanguageMapInfo>().ReverseMap();
            CreateMap<CandidateLanguageMap, CandidateLanguageMapEditModel>().ReverseMap();

            CreateMap<LicenceInformationPostmodel, LicenceInformation>().ReverseMap();
            CreateMap<LicenceInformation, LicenceInformationInfo>().ReverseMap();
            CreateMap<LicenceInformation, LicenceInformationEditModel>().ReverseMap();

            CreateMap<PassportInformationPostmodel, PassportInformation>().ReverseMap();
            CreateMap<PassportInformation, PassportInformationInfo>().ReverseMap();
            CreateMap<PassportInformation, PassportInformationEditModel>().ReverseMap();

            CreateMap<SeamanBookCdcPostmodel, SeamanBookCdc>().ReverseMap();
            CreateMap<SeamanBookCdc, SeamanBookCdcInfo>().ReverseMap();
            CreateMap<SeamanBookCdc, SeamanBookCdcEditModel>().ReverseMap();

            CreateMap<DigiDocumentTypePostmodel, DigiDocumentType>().ReverseMap();
            CreateMap<DigiDocumentType, DigiDocumentTypeInfo>().ReverseMap();
            CreateMap<DigiDocumentType, DigiDocumentTypeEditModel>().ReverseMap();

            CreateMap<DigiDocumentDetailsPostmodel, DigiDocumentDetails>().ReverseMap();
            CreateMap<DigiDocumentDetails, DigiDocumentDetailsInfo>().ReverseMap();
            CreateMap<DigiDocumentDetails, DigiDocumentDetailsEditModel>().ReverseMap();

            CreateMap<DigiDocumentSharedPostmodel, DigiDocumentShared>().ReverseMap();
            CreateMap<DigiDocumentShared, DigiDocumentSharedInfo>().ReverseMap();
            CreateMap<DigiDocumentShared, DigiDocumentSharedEditModel>().ReverseMap();

            CreateMap<DigiDocumentUploadPostmodel, DigiDocumentUpload>().ReverseMap();
            CreateMap<DigiDocumentUpload, DigiDocumentUploadInfo>().ReverseMap();
            CreateMap<DigiDocumentUpload, DigiDocumentUploadEditModel>().ReverseMap();

            CreateMap<DashboardListPostmodel, DashboardList>().ReverseMap();
            CreateMap<DashboardList, DashboardListInfo>().ReverseMap();
            CreateMap<DashboardList, DashboardListEditModel>().ReverseMap();

            CreateMap<FieldOfExpertisePostmodel, FieldOfExpertise>().ReverseMap();
            CreateMap<FieldOfExpertise, FieldOfExpertiseInfo>().ReverseMap();
            CreateMap<FieldOfExpertise, FieldOfExpertiseEditModel>().ReverseMap();

            CreateMap<ResumeTemplatePostmodel, ResumeTemplate>().ReverseMap();
            CreateMap<ResumeTemplate, ResumeTemplateInfo>().ReverseMap();
            CreateMap<ResumeTemplate, ResumeTemplateEditModel>().ReverseMap();

            CreateMap<ResumeCandidateMapPostmodel, ResumeCandidateMap>().ReverseMap();
            CreateMap<ResumeCandidateMap, ResumeCandidateMapInfo>().ReverseMap();
            CreateMap<ResumeCandidateMap, ResumeCandidateMapEditModel>().ReverseMap();

            CreateMap<ResumeDesignationMapPostmodel, ResumeDesignationMap>().ReverseMap();
            CreateMap<ResumeDesignationMap, ResumeDesignationMapInfo>().ReverseMap();
            CreateMap<ResumeDesignationMap, ResumeDesignationMapEditModel>().ReverseMap();

            CreateMap<ResumeExperienceMapPostmodel, ResumeExperienceMap>().ReverseMap();
            CreateMap<ResumeExperienceMap, ResumeExperienceMapInfo>().ReverseMap();
            CreateMap<ResumeExperienceMap, ResumeExperienceMapEditModel>().ReverseMap();

            CreateMap<ResumeExpertiseMapPostmodel, ResumeExpertiseMap>().ReverseMap();
            CreateMap<ResumeExpertiseMap, ResumeExpertiseMapInfo>().ReverseMap();
            CreateMap<ResumeExpertiseMap, ResumeExpertiseMapEditModel>().ReverseMap();

            CreateMap<DesignationPostmodel, Designation>().ReverseMap();
            CreateMap<Designation, DesignationInfo>().ReverseMap();
            CreateMap<Designation, DesignationEditModel>().ReverseMap();

            CreateMap<GenderPostmodel, Gender>().ReverseMap();
            CreateMap<Gender, GenderInfo>().ReverseMap();
            CreateMap<Gender, GenderEditModel>().ReverseMap();

            CreateMap<MartialStatusPostmodel, MartialStatus>().ReverseMap();
            CreateMap<MartialStatus, MartialStatusInfo>().ReverseMap();
            CreateMap<MartialStatus, MartialStatusEditModel>().ReverseMap();

            CreateMap<NamePrefixPostmodel, NamePrefix>().ReverseMap();
            CreateMap<NamePrefix, NamePrefixInfo>().ReverseMap();
            CreateMap<NamePrefix, NamePrefixEditModel>().ReverseMap();

            CreateMap<EmployerPostmodel, Employer>().ReverseMap();
            CreateMap<Employer, EmployerInfo>().ReverseMap();
            CreateMap<Employer, EmployerEditModel>().ReverseMap();

            CreateMap<CountryPostmodel, Country>().ReverseMap();
            CreateMap<Country, CountryInfo>().ReverseMap();
            CreateMap<Country, CountryEditModel>().ReverseMap();

            CreateMap<CurrencyPostmodel, Currency>().ReverseMap();
            CreateMap<Currency, CurrencyInfo>().ReverseMap();
            CreateMap<Currency, CurrencyEditModel>().ReverseMap();

            CreateMap<CurrencyValuePostmodel, CurrencyValue>().ReverseMap();
            CreateMap<CurrencyValue, CurrencyValueInfo>().ReverseMap();
            CreateMap<CurrencyValue, CurrencyValueEditModel>().ReverseMap();

            CreateMap<LanguagePostmodel, Language>().ReverseMap();
            CreateMap<Language, LanguageInfo>().ReverseMap();
            CreateMap<Language, LanguageEditModel>().ReverseMap();

            CreateMap<StatePostmodel, State>().ReverseMap();
            CreateMap<State, StateInfo>().ReverseMap();
            CreateMap<State, StateEditModel>().ReverseMap();

            CreateMap<CoursePostmodel, Course>().ReverseMap();
            CreateMap<Course, CourseInfo>().ReverseMap();
            CreateMap<Course, CourseEditModel>().ReverseMap();

            CreateMap<ExpereinceTypePostmodel, ExpereinceType>().ReverseMap();
            CreateMap<ExpereinceType, ExpereinceTypeInfo>().ReverseMap();
            CreateMap<ExpereinceType, ExpereinceTypeEditModel>().ReverseMap();

            CreateMap<FunctionalAreaPostmodel, FunctionalArea>().ReverseMap();
            CreateMap<FunctionalArea, FunctionalAreaInfo>().ReverseMap();
            CreateMap<FunctionalArea, FunctionalAreaEditModel>().ReverseMap();

            CreateMap<IndustryPostmodel, Industry>().ReverseMap();
            CreateMap<Industry, IndustryInfo>().ReverseMap();
            CreateMap<Industry, IndustryEditModel>().ReverseMap();

            CreateMap<JobByAdminPostmodel, JobByAdmin>().ReverseMap();
            CreateMap<JobByAdmin, JobByAdminInfo>().ReverseMap();
            CreateMap<JobByAdmin, JobByAdminEditModel>().ReverseMap();

            CreateMap<JobByVendorPostmodel, JobByVendor>().ReverseMap();
            CreateMap<JobByVendor, JobByVendorInfo>().ReverseMap();
            CreateMap<JobByVendor, JobByVendorEditModel>().ReverseMap();

            CreateMap<JobCategoryPostmodel, JobCategory>().ReverseMap();
            CreateMap<JobCategory, JobCategoryInfo>().ReverseMap();
            CreateMap<JobCategory, JobCategoryEditModel>().ReverseMap();

            CreateMap<JobCourseMapPostmodel, JobCourseMap>().ReverseMap();
            CreateMap<JobCourseMap, JobCourseMapInfo>().ReverseMap();
            CreateMap<JobCourseMap, JobCourseMapEditModel>().ReverseMap();

            CreateMap<JobLanguageMapPostmodel, JobLanguageMap>().ReverseMap();
            CreateMap<JobLanguageMap, JobLanguageMapInfo>().ReverseMap();
            CreateMap<JobLanguageMap, JobLanguageMapEditModel>().ReverseMap();

            CreateMap<JobListPostmodel, JobList>().ReverseMap();
            CreateMap<JobList, JobListInfo>().ReverseMap();
            CreateMap<JobList, JobListEditModel>().ReverseMap();

            CreateMap<JobRequiredDocMapPostmodel, JobRequiredDocMap>().ReverseMap();
            CreateMap<JobRequiredDocMap, JobRequiredDocMapInfo>().ReverseMap();
            CreateMap<JobRequiredDocMap, JobRequiredDocMapEditModel>().ReverseMap();

            CreateMap<JobShiftMapPostmodel, JobShiftMap>().ReverseMap();
            CreateMap<JobShiftMap, JobShiftMapInfo>().ReverseMap();
            CreateMap<JobShiftMap, JobShiftMapEditModel>().ReverseMap();

            CreateMap<JobTypePostmodel, JobType>().ReverseMap();
            CreateMap<JobType, JobTypeInfo>().ReverseMap();
            CreateMap<JobType, JobTypeEditModel>().ReverseMap();

            CreateMap<JobRolePostmodel, JobRole>().ReverseMap();
            CreateMap<JobRole, JobRoleInfo>().ReverseMap();
            CreateMap<JobRole, JobRoleEditModel>().ReverseMap();

            CreateMap<RequiredDocumentTypePostmodel, RequiredDocumentType>().ReverseMap();
            CreateMap<RequiredDocumentType, RequiredDocumentTypeInfo>().ReverseMap();
            CreateMap<RequiredDocumentType, RequiredDocumentTypeEditModel>().ReverseMap();

            CreateMap<ShiftAvailablePostmodel, ShiftAvailable>().ReverseMap();
            CreateMap<ShiftAvailable, ShiftAvailableInfo>().ReverseMap();
            CreateMap<ShiftAvailable, ShiftAvailableEditModel>().ReverseMap();

            CreateMap<VendorJobListPostmodel, VendorJobList>().ReverseMap();
            CreateMap<VendorJobList, VendorJobListInfo>().ReverseMap();
            CreateMap<VendorJobList, VendorJobListEditModel>().ReverseMap();

            CreateMap<VendorPostmodel, Vendor>().ReverseMap();
            CreateMap<Vendor, VendorInfo>().ReverseMap();
            CreateMap<Vendor, VendorEditModel>().ReverseMap();

            CreateMap<VendorDocumentPostmodel, VendorDocument>().ReverseMap();
            CreateMap<VendorDocument, VendorDocumentInfo>().ReverseMap();
            CreateMap<VendorDocument, VendorDocumentEditModel>().ReverseMap();

            CreateMap<VendorsStatusPostmodel, VendorsStatus>().ReverseMap();
            CreateMap<VendorsStatus, VendorsStatusInfo>().ReverseMap();
            CreateMap<VendorsStatus, VendorsStatusEditModel>().ReverseMap();

            CreateMap<VendorProfileImagePostmodel, VendorProfileImage>().ReverseMap();
            CreateMap<VendorProfileImage, VendorProfileImageInfo>().ReverseMap();
            CreateMap<VendorProfileImage, VendorProfileImageEditModel>().ReverseMap();
        }

    }
}

﻿using System;

namespace CareerApp.API.Helpers
{
    public static class ErrorConstants
    {
        public const string SometingWentWrong = "Something went wrong. Please try again after sometime.";

        public const string SometingWentWrongWithErrorRef = "Something went wrong, Please try again after sometime or contact site administrator with following error reference {0}";

        public static string GetErrorRefernce()
        {
            long ticks = DateTime.Now.Ticks;
            byte[] bytes = BitConverter.GetBytes(ticks);
            string id = Convert.ToBase64String(bytes)
                                    .Replace('+', '_')
                                    .Replace('/', '-')
                                    .TrimEnd('=');
            return id;
        }
    }
}

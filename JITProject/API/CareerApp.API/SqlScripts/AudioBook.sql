﻿USE [CareerApp.API]
GO
/****** Object:  Table [dbo].[Audios]    Script Date: 22-Aug-2019 6:44:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Audios](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[ProductId] [nvarchar](256) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[ShortDescription] [nvarchar](max) NOT NULL,
	[FullDescription] [nvarchar](max) NOT NULL,
	[AdminComment] [nvarchar](max) NULL,
	[LanguageId] [nvarchar](256) NOT NULL,
	[PublisherId] [nvarchar](256) NOT NULL,
	[BookPublisherId] [nvarchar](max) NOT NULL,
	[LengthHour] [int] NOT NULL,
	[LengthMinute] [int] NOT NULL,
	[LengthSecond] [int] NOT NULL,
	[IsFree] [bit] NOT NULL,
	[CurrencyId] [nvarchar](256) NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[OldPrice] [decimal](18, 2) NOT NULL,
	[TaxCategoryId] [nvarchar](256) NULL,
	[IncTax] [bit] NOT NULL,
	[ReleaseDate] [datetime2](7) NOT NULL,
	[Sku] [nvarchar](max) NULL,
	[IsFeaturedProduct] [bit] NOT NULL,
	[ShowOnHomePage] [bit] NOT NULL,
	[MetaKeywords] [nvarchar](max) NULL,
	[AllowCustomerReviews] [bit] NOT NULL,
	[TotalStarRate] [real] NOT NULL,
	[ApprovedTotalReviews] [int] NOT NULL,
	[NotApprovedTotalReviews] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[AvailableStartDateTimeUtc] [datetime2](7) NULL,
	[AvailableEndDateTimeUtc] [datetime2](7) NULL,
	[Published] [bit] NOT NULL,
	[AvailableForPreOrder] [bit] NOT NULL,
	[PreOrderAvailabilityStartDateTimeUtc] [datetime2](7) NULL,
	[CallForPrice] [bit] NOT NULL,
 CONSTRAINT [PK_Audios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaxCategoryItems]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaxCategoryItems](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[TaxCategoryId] [nvarchar](256) NOT NULL,
	[TaxItemId] [nvarchar](256) NOT NULL,
	[TaxPercentage] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_TaxCategoryItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaxCategories]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaxCategories](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[TaxCategoryName] [nvarchar](max) NOT NULL,
	[CountryID] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_TaxCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[viAudioTax]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[viAudioTax]
as
Select A.Id as AudioId,Sum(TaxPercentage)as TaxPercentage
From Audios A
LEFT JOIN TaxCategories TC on TC.Id=A.TaxCategoryId
LEFT JOIN TaxCategoryItems TI on TC.Id=TI.TaxCategoryId and TC.Active=1
Group by A.Id
GO
/****** Object:  View [dbo].[viAudioPrice]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[viAudioPrice]
as
Select A.Id as AudioId,Case when IncTax=0 then  Price+(Price*Sum(TaxPercentage)/100) else Price end as Price,
Case when IncTax=0 then Price else Price/((Sum(TaxPercentage)/100)+1) end as NetPrice
From Audios A
JOIN viAudioTax T on A.Id=T.AudioId
Group by A.Id,Price,IncTax
GO
/****** Object:  Table [dbo].[PromotionItems]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionItems](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[PromotionId] [nvarchar](256) NULL,
	[ItemTypeId] [int] NOT NULL,
	[ItemId] [nvarchar](max) NULL,
	[PromotionGetType] [int] NOT NULL,
	[Get] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_PromotionItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Promotions]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Promotions](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[PromotionCode] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[ValidFrom] [datetime2](7) NOT NULL,
	[ValidTo] [datetime2](7) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Promotions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PromotionAvailableToMaps]    Script Date: 22-Aug-2019 6:44:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionAvailableToMaps](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[PromotionId] [nvarchar](256) NULL,
	[RoleId] [nvarchar](max) NULL,
 CONSTRAINT [PK_PromotionAvailableToMaps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[viAudioOfferPrice]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viAudioOfferPrice]
as
Select A.AudioId,RoleId,Case when IncTax=0 then  Price+(Price*Sum(TaxPercentage)/100) else Price end as Price
From
(SELECT   A.Id as AudioId,A.IncTax, PAM.RoleId,case PIt.PromotionGetType when 1 then (A.Price-PIt.Get) 
When 2 then (A.Price-(A.Price*PIt.Get/100))
When 3 then (Pit.Get) end Price
FROM Audios A
JOIN PromotionItems PIt on case when PIt.ItemTypeId=1 then A.Id end = Pit.ItemId
JOIN Promotions P on PIt.PromotionId=P.Id and GETDATE() between P.ValidFrom and P.ValidTo and P.Active=1
JOIN PromotionAvailableToMaps PAM on PAM.PromotionId=P.Id) A
JOIN viAudioTax T on A.AudioId=T.AudioId
Group by A.AudioId,RoleId,IncTax,Price
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[ApplicationRoleId] [nvarchar](450) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ApplicationRoleId] [nvarchar](450) NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[MiddleName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Configuration] [nvarchar](max) NULL,
	[Culture] [nvarchar](100) NULL,
	[ExtensionData] [nvarchar](max) NULL,
	[IsEnabled] [bit] NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Host] [nvarchar](max) NOT NULL,
	[Location] [geography] NULL,
	[DeviceType] [int] NOT NULL,
	[DeviceId] [nvarchar](max) NULL,
	[DeviceName] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AudioFiles]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AudioFiles](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioTypeId] [int] NOT NULL,
	[AudioId] [nvarchar](256) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[AudioUrl] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NULL,
	[ContentLegth] [nvarchar](max) NULL,
 CONSTRAINT [PK_AudioFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AudioNarratorMaps]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AudioNarratorMaps](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioId] [nvarchar](256) NOT NULL,
	[NarratorId] [nvarchar](256) NULL,
 CONSTRAINT [PK_AudioNarratorMaps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AudioPictures]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AudioPictures](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioId] [nvarchar](256) NULL,
	[PictureId] [nvarchar](256) NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_AudioPictures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AudioRates]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AudioRates](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioId] [nvarchar](256) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[RateType] [int] NOT NULL,
	[Rate] [int] NOT NULL,
	[Host] [nvarchar](max) NOT NULL,
	[Location] [geography] NULL,
 CONSTRAINT [PK_AudioRates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AudioReviews]    Script Date: 22-Aug-2019 6:44:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AudioReviews](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioId] [nvarchar](256) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Host] [nvarchar](max) NOT NULL,
	[Location] [geography] NULL,
 CONSTRAINT [PK_AudioReviews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AudioTranslatorMaps]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AudioTranslatorMaps](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioId] [nvarchar](256) NOT NULL,
	[TranslatorId] [nvarchar](256) NULL,
 CONSTRAINT [PK_AudioTranslatorMaps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Carts]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carts](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserId] [nvarchar](max) NOT NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
	[AudioId] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Carts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CurrencyId] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Currencies]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currencies](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[MainSuffix] [nvarchar](max) NOT NULL,
	[SubSuffix] [nvarchar](max) NOT NULL,
	[CurrencySymbol] [nvarchar](max) NOT NULL,
	[DecimalPoints] [int] NOT NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CurrencyValues]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyValues](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[CurrencyId] [nvarchar](256) NOT NULL,
	[ValueInId] [nvarchar](256) NOT NULL,
	[Value] [decimal](8, 5) NOT NULL,
 CONSTRAINT [PK_CurrencyValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedbacks]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedbacks](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[Rate] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Host] [nvarchar](max) NOT NULL,
	[Location] [geography] NULL,
 CONSTRAINT [PK_Feedbacks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoggedDeviceHistorys]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoggedDeviceHistorys](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[IdentityRef] [nvarchar](450) NOT NULL,
	[DeviceType] [int] NOT NULL,
	[DeviceId] [nvarchar](max) NOT NULL,
	[DeviceName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LoggedDeviceHistorys] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Medias]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medias](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Ref] [nvarchar](max) NOT NULL,
	[MediaType] [int] NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[Extension] [nvarchar](max) NOT NULL,
	[Source] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NOT NULL,
	[ContentLength] [int] NOT NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
 CONSTRAINT [PK_Medias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Memberships]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Memberships](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[MembershipTypeId] [nvarchar](256) NOT NULL,
	[ApplicationUserId] [nvarchar](450) NOT NULL,
	[MembershipTypeName] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[CurrentMembersCount] [int] NOT NULL,
	[ExpectedMembersCount] [int] NOT NULL,
 CONSTRAINT [PK_Memberships] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MembershipTypes]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MembershipTypes](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_MembershipTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Narrators]    Script Date: 22-Aug-2019 6:44:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Narrators](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Gender] [int] NOT NULL,
 CONSTRAINT [PK_Narrators] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Offers]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offers](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[AudioId] [nvarchar](256) NULL,
	[Offers] [nvarchar](max) NULL,
	[OfferDiscount] [real] NOT NULL,
 CONSTRAINT [PK_Offers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Picture]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Picture](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[MimeType] [nvarchar](max) NULL,
	[Filename] [nvarchar](max) NULL,
	[SeoFilename] [nvarchar](max) NULL,
	[AltAttribute] [nvarchar](max) NULL,
	[TitleAttribute] [nvarchar](max) NULL,
	[IsNew] [bit] NOT NULL,
 CONSTRAINT [PK_Picture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductAuthorMaps]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductAuthorMaps](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[ProductId] [nvarchar](256) NOT NULL,
	[AuthorId] [nvarchar](256) NULL,
 CONSTRAINT [PK_ProductAuthorMaps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[MembershipTypeId] [nvarchar](256) NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSubCategoryMaps]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSubCategoryMaps](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[ProductId] [nvarchar](256) NOT NULL,
	[SubCategoryId] [nvarchar](256) NULL,
 CONSTRAINT [PK_ProductSubCategoryMaps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Publishers]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Publishers](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Publishers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Purchases]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purchases](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserId] [int] NOT NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
	[AudioId] [nvarchar](256) NULL,
	[PurchaseAmount] [int] NOT NULL,
	[Host] [nvarchar](max) NOT NULL,
	[Location] [geography] NULL,
 CONSTRAINT [PK_Purchases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategories]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategories](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_SubCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaxItems]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaxItems](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[TaxItemName] [nvarchar](max) NULL,
 CONSTRAINT [PK_TaxItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Translators]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Translators](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Translators] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WishLists]    Script Date: 22-Aug-2019 6:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WishLists](
	[Id] [nvarchar](256) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
	[UserId] [nvarchar](max) NOT NULL,
	[ApplicationUserId] [nvarchar](450) NULL,
	[AudioId] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_WishLists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190820051508_initial', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190820060616_currency-change', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190820081159_currency-value', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190820081548_currency-values', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190821050541_currency-updates', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190821101241_promotion', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190821102145_promotion-relationship', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190821105501_promotion-availabletomaps', N'2.2.6-servicing-10079')
SET IDENTITY_INSERT [dbo].[AspNetRoleClaims] ON 

INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (1, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'users.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (2, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'users.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (3, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'roles.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (4, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'roles.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (5, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'roles.assign', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (6, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'members.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (7, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'members.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (8, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'user.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (9, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'member.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (10, N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'permission', N'all.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (11, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'users.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (12, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'users.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (13, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'roles.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (14, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'roles.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (15, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'roles.assign', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (16, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'members.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (17, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'members.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (18, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'user.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (19, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'member.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (20, N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'permission', N'all.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (21, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'users.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (22, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'users.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (23, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'roles.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (24, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'roles.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (25, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'roles.assign', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (26, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'members.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (27, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'members.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (28, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'user.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (29, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'member.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (30, N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'permission', N'all.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (31, N'5c1cb59c-53d7-4eb7-a3b0-920b7ea712e8', N'permission', N'members.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (32, N'5c1cb59c-53d7-4eb7-a3b0-920b7ea712e8', N'permission', N'members.manage', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (33, N'9ea68c79-e442-45d3-9dc1-e8cad9ba3c5b', N'permission', N'member.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (34, N'f6adf5f2-0bba-4f44-8620-2540906afd9f', N'permission', N'user.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (35, N'f23a9b22-bbd1-476b-b840-bf2a24ff49e6', N'permission', N'all.view', NULL)
INSERT [dbo].[AspNetRoleClaims] ([Id], [RoleId], [ClaimType], [ClaimValue], [ApplicationRoleId]) VALUES (36, N'886cdf90-0a71-4e96-918f-e013336ec72a', N'permission', N'all.view', NULL)
SET IDENTITY_INSERT [dbo].[AspNetRoleClaims] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'598a94d7-5725-4f17-a06a-2880ab48a36c', N'SiteAdministrator', N'SITEADMINISTRATOR', N'76870136-b152-4390-b7e3-de9a561d4ed6', N'Default site administrator', N'', N'', CAST(N'2019-07-01T07:43:36.5101308' AS DateTime2), CAST(N'2019-07-01T07:43:38.3832165' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'5c1cb59c-53d7-4eb7-a3b0-920b7ea712e8', N'GroupAdmin', N'GROUPADMIN', N'c183f885-cd01-4eb8-baf3-aaca0086d997', N'Group Admin', N'', N'', CAST(N'2019-07-01T07:43:38.6964704' AS DateTime2), CAST(N'2019-07-01T07:43:38.7219023' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'886cdf90-0a71-4e96-918f-e013336ec72a', N'MobileGuest', N'MOBILEGUEST', N'6add523e-13f5-4d06-8865-7dfb300004ac', N'Mobile Guest', N'', N'', CAST(N'2019-07-01T07:43:39.1610051' AS DateTime2), CAST(N'2019-07-01T07:43:39.1805842' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'9ea68c79-e442-45d3-9dc1-e8cad9ba3c5b', N'GroupMember', N'GROUPMEMBER', N'7c8a763d-6499-4af7-8cb2-8fe16569e091', N'Group Member', N'', N'', CAST(N'2019-07-01T07:43:38.7381507' AS DateTime2), CAST(N'2019-07-01T07:43:38.7477676' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'e6de4035-75d0-43d1-9e12-4fd74364266a', N'WebAdministrator', N'WEBADMINISTRATOR', N'81885d3a-b6d8-4046-a88c-bed88d0aaed7', N'Web administrator', N'', N'', CAST(N'2019-07-01T07:43:38.4249525' AS DateTime2), CAST(N'2019-07-01T07:43:38.5344433' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'f23a9b22-bbd1-476b-b840-bf2a24ff49e6', N'WebGuest', N'WEBGUEST', N'265b6fb0-5a1d-4f20-bbc9-4bfe73a5cef4', N'Web Guest', N'', N'', CAST(N'2019-07-01T07:43:39.1129508' AS DateTime2), CAST(N'2019-07-01T07:43:39.1345113' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'f45bf2d6-5219-4698-83fe-a5b09a86b00a', N'StaffAdministrator', N'STAFFADMINISTRATOR', N'64341299-dd30-49de-9312-43a19ec4d79e', N'Staff administrator', N'', N'', CAST(N'2019-07-01T07:43:38.5460282' AS DateTime2), CAST(N'2019-07-01T07:43:38.6814494' AS DateTime2))
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp], [Description], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate]) VALUES (N'f6adf5f2-0bba-4f44-8620-2540906afd9f', N'IndividualUser', N'INDIVIDUALUSER', N'8067ef89-dca0-4893-b5d1-409865f998ad', N'Individual User', N'', N'', CAST(N'2019-07-01T07:43:38.9279932' AS DateTime2), CAST(N'2019-07-01T07:43:38.9909930' AS DateTime2))
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [ApplicationRoleId], [ApplicationUserId]) VALUES (N'76881879-2f09-4d76-8f2b-a870e93f16ba', N'f6adf5f2-0bba-4f44-8620-2540906afd9f', NULL, NULL)
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [ApplicationRoleId], [ApplicationUserId]) VALUES (N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'f6adf5f2-0bba-4f44-8620-2540906afd9f', NULL, NULL)
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [ApplicationRoleId], [ApplicationUserId]) VALUES (N'ab0aef01-302d-411f-8c12-f33577b6994a', N'f6adf5f2-0bba-4f44-8620-2540906afd9f', NULL, NULL)
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [ApplicationRoleId], [ApplicationUserId]) VALUES (N'aef4a93b-cd80-451e-ad93-a41d6b21768b', N'f6adf5f2-0bba-4f44-8620-2540906afd9f', NULL, NULL)
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId], [ApplicationRoleId], [ApplicationUserId]) VALUES (N'e6245feb-f73e-4b83-bd84-fd119166c997', N'598a94d7-5725-4f17-a06a-2880ab48a36c', NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [MiddleName], [LastName], [Configuration], [Culture], [ExtensionData], [IsEnabled], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Host], [Location], [DeviceType], [DeviceId], [DeviceName]) VALUES (N'76881879-2f09-4d76-8f2b-a870e93f16ba', N'ashiquebinraheem@gmail.com', N'ASHIQUEBINRAHEEM@GMAIL.COM', N'ashiquebinraheem@gmail.com', N'ASHIQUEBINRAHEEM@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEIQT4+HX30/2KXfXx25jQZc45YgPbxQiJZsXOuvG/jTCtlPadl3rExOeOyzQOaotRA==', N'LFRGTAGDNYEGVX6HYDC73DOLDV2N4CKY', N'82e25d63-16db-4215-90f1-41557cb05424', N'Bct@2019', 0, 0, NULL, 1, 0, N'Ashique', NULL, N'Muhammad', NULL, NULL, NULL, 1, N'', N'', CAST(N'2019-07-15T09:45:40.9760598' AS DateTime2), CAST(N'2019-07-31T06:39:41.4322408' AS DateTime2), N'192.168.1.15', NULL, 0, NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [MiddleName], [LastName], [Configuration], [Culture], [ExtensionData], [IsEnabled], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Host], [Location], [DeviceType], [DeviceId], [DeviceName]) VALUES (N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'jivya@borncode.in', N'JIVYA@BORNCODE.IN', N'jivya@borncode.in', N'JIVYA@BORNCODE.IN', 0, N'AQAAAAEAACcQAAAAEDXFY/vz+Pl3wnFt3jpmea/fvReFOtnO7lu0iVY9obuNMXC6WLTPjUHb4YZ9UqUF9A==', N'BJF4TUDMLIMOA7ZSHZ67GLCL22TSAQCO', N'f6e71e40-45d8-4211-81f1-a8077cdd16e0', N'11111', 0, 0, NULL, 1, 0, N'Jivya', NULL, N'P', NULL, NULL, NULL, 0, N'', N'', CAST(N'2019-08-05T04:06:56.0822816' AS DateTime2), CAST(N'2019-08-05T04:06:59.1324649' AS DateTime2), N'::1', NULL, 0, NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [MiddleName], [LastName], [Configuration], [Culture], [ExtensionData], [IsEnabled], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Host], [Location], [DeviceType], [DeviceId], [DeviceName]) VALUES (N'ab0aef01-302d-411f-8c12-f33577b6994a', N'labeeb@borncode.in', N'LABEEB@BORNCODE.IN', N'labeeb@borncode.in', N'LABEEB@BORNCODE.IN', 0, N'AQAAAAEAACcQAAAAEIyrsySXcMmOwyF7UnD+B1Jb9LDw6+JKERFHflRrbacXV78II80a2DGOUSMXJvNe9g==', N'M6YSHTNXTBMLZCCHV3FV5Y2X5BLH7GW3', N'a6fd22c4-7de1-4257-b295-5bf49c8516a1', N'33333', 0, 0, NULL, 1, 0, N'Labeeb', NULL, N'H', NULL, NULL, NULL, 0, N'', N'', CAST(N'2019-08-05T04:08:21.4890867' AS DateTime2), CAST(N'2019-08-05T04:08:21.6589925' AS DateTime2), N'192.168.1.15', NULL, 0, NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [MiddleName], [LastName], [Configuration], [Culture], [ExtensionData], [IsEnabled], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Host], [Location], [DeviceType], [DeviceId], [DeviceName]) VALUES (N'aef4a93b-cd80-451e-ad93-a41d6b21768b', N'nikhil@borncode.in', N'NIKHIL@BORNCODE.IN', N'nikhil@borncode.in', N'NIKHIL@BORNCODE.IN', 0, N'AQAAAAEAACcQAAAAEHY0csR8ZgxPQqAbex8SIwF+foduBM7vd0Z1tbMOcogwycIhna/8HdHXcpwx3BjinA==', N'ZKI7ZQZEYV2WF53Z23LS5LNYG6RCSCBS', N'589d9d7b-2712-48a8-9966-1f83e01b4de2', N'222222', 0, 0, NULL, 1, 0, N'Nikhil', NULL, N'P', NULL, NULL, NULL, 0, N'', N'', CAST(N'2019-08-05T04:07:54.8502425' AS DateTime2), CAST(N'2019-08-05T04:07:55.1559135' AS DateTime2), N'192.168.1.15', NULL, 0, NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [MiddleName], [LastName], [Configuration], [Culture], [ExtensionData], [IsEnabled], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Host], [Location], [DeviceType], [DeviceId], [DeviceName]) VALUES (N'e6245feb-f73e-4b83-bd84-fd119166c997', N'nishila2', N'NISHILA2', N'nishila2@gmail.com', N'NISHILA2@GMAIL.COM', 1, N'AQAAAAEAACcQAAAAEOY9Ij0DP/lg2zrEbh1wzL86qxcXBLLLosQIySDaAjXG/5VpkS2UwIluapT2pgFEAw==', N'GJLOS5WZEOQ4F7QRHGVGF2LU6V2GJJGZ', N'30c12971-cddd-46a6-bb58-4707fd91944f', N'+919562161303', 1, 0, NULL, 1, 0, N'Site', NULL, N'Administrator', NULL, NULL, NULL, 1, N'', N'', CAST(N'2019-07-01T07:43:40.4376023' AS DateTime2), CAST(N'2019-07-01T07:43:44.0197058' AS DateTime2), N'', NULL, 0, NULL, NULL)
INSERT [dbo].[AudioFiles] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioTypeId], [AudioId], [Title], [Description], [AudioUrl], [ContentType], [ContentLegth]) VALUES (N'118ff0ae-2ac4-4d8e-865b-1ec4832662ae', N'', N'', CAST(N'2019-08-07T06:17:35.5315885' AS DateTime2), CAST(N'2019-08-07T06:17:35.5315885' AS DateTime2), 1, 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', NULL, N'', N'https://samples.audible.com/bk/rhuk/003054/bk_rhuk_003054_sample.mp3', N'mp3', N'10')
INSERT [dbo].[AudioFiles] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioTypeId], [AudioId], [Title], [Description], [AudioUrl], [ContentType], [ContentLegth]) VALUES (N'1e11f4d6-82b8-422c-9b44-b05c754a5b86', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, 1, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', NULL, NULL, N'http://www.music.helsinki.fi/tmt/opetus/uusmedia/esim/a2002011001-e02-128k.mp3', N'mp3', N'10')
INSERT [dbo].[AudioFiles] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioTypeId], [AudioId], [Title], [Description], [AudioUrl], [ContentType], [ContentLegth]) VALUES (N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, 1, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', NULL, NULL, N'http://www.music.helsinki.fi/tmt/opetus/uusmedia/esim/a2002011001-e02-128k.mp3', N'mp3', N'10')
INSERT [dbo].[AudioFiles] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioTypeId], [AudioId], [Title], [Description], [AudioUrl], [ContentType], [ContentLegth]) VALUES (N'1e11f4d6-82b8-422c-9b44-b05c76a5a86', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, 2, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', NULL, NULL, N'http://www.music.helsinki.fi/tmt/opetus/uusmedia/esim/a2002011001-e02-128k.mp3', N'mp3', N'10')
INSERT [dbo].[AudioFiles] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioTypeId], [AudioId], [Title], [Description], [AudioUrl], [ContentType], [ContentLegth]) VALUES (N'70be6184-34e2-4e99-abad-4aa4f3f43f63', N'', N'', CAST(N'2019-08-07T06:17:35.5324535' AS DateTime2), CAST(N'2019-08-07T06:17:35.5324535' AS DateTime2), 1, 2, N'14032467-5953-43c0-837c-b9e6f4ec944d', NULL, N'', N'http://www.music.helsinki.fi/tmt/opetus/uusmedia/esim/a2002011001-e02-128k.mp3', N'mp3', N'20')
INSERT [dbo].[AudioFiles] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioTypeId], [AudioId], [Title], [Description], [AudioUrl], [ContentType], [ContentLegth]) VALUES (N'80be6184-34e2-4e99-abad-4aa4ff43f63', N'', N'', CAST(N'2019-08-07T06:17:35.5324535' AS DateTime2), CAST(N'2019-08-07T06:17:35.5324535' AS DateTime2), 1, 2, N'14032467-5953-43c0-837c-b9e6f4ec944d', NULL, N'', N'https://samples.audible.com/bk/rhuk/003054/bk_rhuk_003054_sample.mp3', N'mp3', N'20')
INSERT [dbo].[AudioNarratorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [NarratorId]) VALUES (N'0b6e1a44-8fda-478a-b671-38b3180a2f0f', N'', N'', CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 0, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'27612e73-99cc-44d7-9f51-9e88d49b123b')
INSERT [dbo].[AudioNarratorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [NarratorId]) VALUES (N'0b6e1a44-8fda-478a-b671-38b3184a2f0f', N'', N'', CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 0, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'f5912739-d4b0-485c-9c94-35272dfcff46')
INSERT [dbo].[AudioNarratorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [NarratorId]) VALUES (N'6ec20eae-954c-4e02-82ea-4a34dcef9cf2', N'', N'', CAST(N'2019-08-07T05:04:48.4224137' AS DateTime2), CAST(N'2019-08-07T05:04:48.4224137' AS DateTime2), 0, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'b35766af-0ee0-4983-bd86-1eb83922420f')
INSERT [dbo].[AudioNarratorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [NarratorId]) VALUES (N'78e2776c-115c-4cc1-acf8-dc2692f1f62b', N'', N'', CAST(N'2019-08-05T11:00:10.5047170' AS DateTime2), CAST(N'2019-08-05T11:00:10.5047170' AS DateTime2), 0, N'825cd1b0-1621-4573-b5a9-3ab19c20d72d', N'f5912739-d4b0-485c-9c94-35272dfcff46')
INSERT [dbo].[AudioNarratorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [NarratorId]) VALUES (N'cf54b193-8f18-4e5a-a43e-f8b67ea2447e', N'', N'', CAST(N'2019-08-05T10:47:08.5067753' AS DateTime2), CAST(N'2019-08-05T10:47:08.5067753' AS DateTime2), 0, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', N'f5912739-d4b0-485c-9c94-35272dfcff46')
INSERT [dbo].[AudioPictures] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [PictureId], [DisplayOrder]) VALUES (N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', N'eaeb3598-1c6a-4981-90ae-49fd6b854c44', 1)
INSERT [dbo].[AudioPictures] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [PictureId], [DisplayOrder]) VALUES (N'a0bccf25-9fc3-4bb2-862b-72968ce591c7', N'', N'', CAST(N'2019-08-07T10:54:59.3346509' AS DateTime2), CAST(N'2019-08-07T11:12:01.9430378' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'f97a5c25-d29a-4cb1-9331-97cff7c3f242', 3)
INSERT [dbo].[AudioPictures] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [PictureId], [DisplayOrder]) VALUES (N'd06b07ed-62a8-49af-96ad-e93b2ba94c95', N'', N'', CAST(N'2019-08-07T10:54:59.3344816' AS DateTime2), CAST(N'2019-08-07T11:12:01.9419943' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'b6fc4dec-2feb-44af-9f0c-65e94859321c', 2)
INSERT [dbo].[AudioPictures] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [PictureId], [DisplayOrder]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b8543c46', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'eaeb3598-1c6a-4981-90ae-49fd6b854c49', 1)
INSERT [dbo].[AudioPictures] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [PictureId], [DisplayOrder]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c45', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'eaeb3598-1c6a-4981-90ae-49fd6b854c45', 1)
INSERT [dbo].[AudioPictures] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [PictureId], [DisplayOrder]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c46', NULL, NULL, CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), CAST(N'2019-08-05T10:45:34.2031535' AS DateTime2), 1, N'825cd1b0-1621-4573-b5a9-3ab19c20d72d', N'eaeb3598-1c6a-4981-90ae-49fd6b854c48', 1)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'14032467-5953-43c0-837c-b9e6f4ec944d', NULL, NULL, CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'76881879-2f09-4d76-8f2b-a870e93f16ba', 1, 3, N'1', NULL)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'3DC7D849-56C9-4F9C-B1E9-E977CC5EC35C', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-08T09:29:31.9466667' AS DateTime2), CAST(N'2019-08-08T11:00:44.3333333' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 1, 5, N'1:1', NULL)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'4F237B13-B53E-4619-B759-6C7E49FE96F2', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-08T15:53:20.9833333' AS DateTime2), CAST(N'2019-08-19T16:07:55.8200000' AS DateTime2), 1, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 2, 3, N'1:1', NULL)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'7FA23EB1-872A-45F4-8882-4F7CEA9FA8BA', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-08T12:34:28.9300000' AS DateTime2), CAST(N'2019-08-19T16:06:26.8066667' AS DateTime2), 1, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 1, 4, N'1:1', NULL)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'C3BF2E59-662A-4A65-99E0-BA242997F122', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-08T12:34:32.8900000' AS DateTime2), CAST(N'2019-08-08T12:36:11.8900000' AS DateTime2), 1, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 2, 3, N'1:1', NULL)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'E230D6E4-2659-408F-BDE9-D58BC736C419', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-08T15:53:24.3866667' AS DateTime2), CAST(N'2019-08-08T15:53:24.3866667' AS DateTime2), 1, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 1, 4, N'1:1', NULL)
INSERT [dbo].[AudioRates] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [RateType], [Rate], [Host], [Location]) VALUES (N'EA9C4433-F910-4758-A658-06BF6EA920E5', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-08T09:33:33.1200000' AS DateTime2), CAST(N'2019-08-08T11:00:32.9366667' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 2, 5, N'1:1', NULL)
INSERT [dbo].[AudioReviews] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [Title], [Description], [Host], [Location]) VALUES (N'14032467-5953-43c0-837c-b9e6f4ec944d', NULL, NULL, CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'76881879-2f09-4d76-8f2b-a870e93f16ba', N'Awsome', N'Well done well done', N'1', NULL)
INSERT [dbo].[AudioReviews] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [Title], [Description], [Host], [Location]) VALUES (N'a52c472d-af84-437d-a1ad-ff94117f3911', N'', N'', CAST(N'2019-08-08T07:06:28.3388857' AS DateTime2), CAST(N'2019-08-19T10:36:54.8983916' AS DateTime2), 1, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'test', N'Asia', N'192.168.1.17', NULL)
INSERT [dbo].[AudioReviews] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [Title], [Description], [Host], [Location]) VALUES (N'fc01eebc-6d63-49b7-be20-f28cf570d07f', N'', N'', CAST(N'2019-08-08T06:09:45.2953386' AS DateTime2), CAST(N'2019-08-08T06:33:48.7541357' AS DateTime2), 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'ttt', N'', N'192.168.1.17', NULL)
INSERT [dbo].[AudioReviews] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [AudioId], [UserId], [Title], [Description], [Host], [Location]) VALUES (N'fd671aa9-d700-4382-9308-becf6b35c24f', N'', N'', CAST(N'2019-08-08T10:23:48.5880899' AS DateTime2), CAST(N'2019-08-19T10:38:04.4390772' AS DateTime2), 1, N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'we’re', N'We wear rwee', N'192.168.1.17', NULL)
INSERT [dbo].[Audios] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [Title], [ShortDescription], [FullDescription], [AdminComment], [LanguageId], [PublisherId], [BookPublisherId], [LengthHour], [LengthMinute], [LengthSecond], [IsFree], [CurrencyId], [Price], [OldPrice], [TaxCategoryId], [IncTax], [ReleaseDate], [Sku], [IsFeaturedProduct], [ShowOnHomePage], [MetaKeywords], [AllowCustomerReviews], [TotalStarRate], [ApprovedTotalReviews], [NotApprovedTotalReviews], [DisplayOrder], [AvailableStartDateTimeUtc], [AvailableEndDateTimeUtc], [Published], [AvailableForPreOrder], [PreOrderAvailabilityStartDateTimeUtc], [CallForPrice]) VALUES (N'14032467-5953-43c0-837c-b9e6f4ec944d', N'', N'', CAST(N'2019-08-07T05:04:48.1586863' AS DateTime2), CAST(N'2019-08-07T05:04:48.1586863' AS DateTime2), 1, N'b329ad38-048f-4f53-9769-c3193bef8ad5', N'Sapiens', N'Opening up a controversial topic with spirit and thoroughness, Sapiens will challenge your preconceptions, provoke discussion and, most importantly, push you to think for yourself.', N'The Sunday Times best seller.Earth is 4.5 billion years old. In just a fraction of that time, one species among countless others has conquered it. Us.We are the most advanced and most destructive animals ever to have lived. What makes us brilliant? What makes us deadly? What makes us sapiens?In this bold and provocative CareerApp.API, Yuval Noah Harari explores who we are, how we got here, and where we''re going.Sapiens is a thrilling account of humankind''s extraordinary history from the Stone Age to the Silicon Age and our journey from insignificant apes to rulers of the world.', N'', N'4419ce2d-3927-4a31-9d3d-e8377c5bc3fb', N'd602e2c7-ba0d-4271-87dd-6f997d31fab1', N'3c2b39c6-630d-4cce-91aa-0a0433cf3f94', 15, 18, 0, 0, N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', CAST(750.00 AS Decimal(18, 2)), CAST(820.00 AS Decimal(18, 2)), N'234f743c-bb97-45e2-8023-740ac6eb4ae6', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'', 1, 1, N'', 0, 4.3, 0, 0, 0, NULL, NULL, 0, 0, NULL, 0)
INSERT [dbo].[Audios] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [Title], [ShortDescription], [FullDescription], [AdminComment], [LanguageId], [PublisherId], [BookPublisherId], [LengthHour], [LengthMinute], [LengthSecond], [IsFree], [CurrencyId], [Price], [OldPrice], [TaxCategoryId], [IncTax], [ReleaseDate], [Sku], [IsFeaturedProduct], [ShowOnHomePage], [MetaKeywords], [AllowCustomerReviews], [TotalStarRate], [ApprovedTotalReviews], [NotApprovedTotalReviews], [DisplayOrder], [AvailableStartDateTimeUtc], [AvailableEndDateTimeUtc], [Published], [AvailableForPreOrder], [PreOrderAvailabilityStartDateTimeUtc], [CallForPrice]) VALUES (N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', N'', N'', CAST(N'2019-08-05T10:03:45.1986246' AS DateTime2), CAST(N'2019-08-05T10:47:08.3714840' AS DateTime2), 1, N'4a0cf807-d97e-40c4-b5f4-1d5454878571', N'Rise of Kali', N'The Mahabharata endures as the great epic of India.', N'While Jaya is the story of the Pandavas, told from the perspective of the victors of Kurukshetra, Ajaya is the tale of the Kauravas, who were decimated to the last man. From the pen of the author who gave voice to Ravana in the national best seller Asura comes the riveting narrative which compels us to question the truth behind the Mahabharata.', N'', N'4419ce2d-3927-4a31-9d3d-e8377c5bc3fb', N'954d2a42-2e5b-46e3-9c47-497cee0db2d0', N'6e92f212-8c35-42da-851a-468418282553', 16, 10, 0, 0, N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', CAST(150.00 AS Decimal(18, 2)), CAST(154.00 AS Decimal(18, 2)), N'1c02cb3b-fa71-4c17-a2bc-759f1546b411', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'', 1, 1, NULL, 0, 3.5, 0, 0, 0, NULL, NULL, 0, 0, NULL, 0)
INSERT [dbo].[Audios] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [Title], [ShortDescription], [FullDescription], [AdminComment], [LanguageId], [PublisherId], [BookPublisherId], [LengthHour], [LengthMinute], [LengthSecond], [IsFree], [CurrencyId], [Price], [OldPrice], [TaxCategoryId], [IncTax], [ReleaseDate], [Sku], [IsFeaturedProduct], [ShowOnHomePage], [MetaKeywords], [AllowCustomerReviews], [TotalStarRate], [ApprovedTotalReviews], [NotApprovedTotalReviews], [DisplayOrder], [AvailableStartDateTimeUtc], [AvailableEndDateTimeUtc], [Published], [AvailableForPreOrder], [PreOrderAvailabilityStartDateTimeUtc], [CallForPrice]) VALUES (N'339e90f9-5b3c-4119-93d7-7541ae665a7b', N'', N'', CAST(N'2019-08-05T10:41:01.0485360' AS DateTime2), CAST(N'2019-08-05T10:45:33.6845697' AS DateTime2), 1, N'a2e36a5a-0f7c-4a2b-86dd-048f30393a4b', N'The Monk Who Sold His Ferrari', N'Inspiring parable about a lawyer, Julian Mantle, whose out-of-balance life provokes a spiritual crisis', N'The Monk Who Sold His Ferrari is an inspiring parable about a lawyer, Julian Mantle, whose out-of-balance life provokes a spiritual crisis. As he journeys to an ancient culture, he gains powerful and practical wisdom. The story offers a step-by-step approach to a life of balance, courage, abundance, and joy.Author Robin Sharma reads this international best-seller, allowing you to gain the wisdom and lessons from the original book while you commute. This abridged CareerApp.API is one of Sharma Leadership International''s all-time best-selling audio products.', N'', N'4419ce2d-3927-4a31-9d3d-e8377c5bc3fb', N'62ae34ba-edb8-4dd9-b4bd-87411d8f9daa', N'3c2b39c6-630d-4cce-91aa-0a0433cf3f94', 2, 40, 0, 0, N'2c85f04a-8bd4-4f94-b8e6-7c190d476275', CAST(300.00 AS Decimal(18, 2)), CAST(351.00 AS Decimal(18, 2)), N'1c02cb3b-fa71-4c17-a2bc-759f1546b411', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'', 1, 1, NULL, 0, 3.5, 0, 0, 0, NULL, NULL, 0, 0, NULL, 0)
INSERT [dbo].[Audios] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [Title], [ShortDescription], [FullDescription], [AdminComment], [LanguageId], [PublisherId], [BookPublisherId], [LengthHour], [LengthMinute], [LengthSecond], [IsFree], [CurrencyId], [Price], [OldPrice], [TaxCategoryId], [IncTax], [ReleaseDate], [Sku], [IsFeaturedProduct], [ShowOnHomePage], [MetaKeywords], [AllowCustomerReviews], [TotalStarRate], [ApprovedTotalReviews], [NotApprovedTotalReviews], [DisplayOrder], [AvailableStartDateTimeUtc], [AvailableEndDateTimeUtc], [Published], [AvailableForPreOrder], [PreOrderAvailabilityStartDateTimeUtc], [CallForPrice]) VALUES (N'825cd1b0-1621-4573-b5a9-3ab19c20d72d', N'', N'', CAST(N'2019-08-05T11:00:10.4348789' AS DateTime2), CAST(N'2019-08-05T11:00:10.4348789' AS DateTime2), 1, N'20f2a982-c398-4677-b3e2-b69238cca90c', N'Learning How to Fly', N'Motivation', N'Dr A. P. J. Abdul Kalam had a great belief in the power of the youth. He met over 21 million children and young people in India and outside and spoke to them about the power of knowledge, ambition, moral behaviour and the need to bring about change in society. He travelled to almost every corner of the country meeting the youth in schools, universities and institutions and interacted with them like a committed teacher.In Learning How to Fly, some of his nearly 2,000 lectures have been compiled. These lectures were addressed to teachers and students in school and beyond. In each one of them he has spoken about preparing oneself best for life, to identify and overcome challenges and how to bring out the best within each individual. Through stories from his own life, those of his teachers and mentors as well as stories of some of the greatest men and women of the world, and the latest developments in science and technology, he shows us the importance of dreams and the hard work needed to turn those dreams into reality.Filled with warmth, inspiration and a positive attitude, Learning How to Fly is essential listening for every Indian, young and old.', N'', N'4419ce2d-3927-4a31-9d3d-e8377c5bc3fb', N'954d2a42-2e5b-46e3-9c47-497cee0db2d0', N'954d2a42-2e5b-46e3-9c47-497cee0db2d0', 16, 10, 0, 0, N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', CAST(150.00 AS Decimal(18, 2)), CAST(154.00 AS Decimal(18, 2)), N'1c02cb3b-fa71-4c17-a2bc-759f1546b411', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'', 1, 1, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, 0)
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'14fd6546-fdbe-45a0-b368-e703f9e45643', N'', N'', CAST(N'2019-08-05T10:50:18.3701669' AS DateTime2), CAST(N'2019-08-05T10:50:18.3701669' AS DateTime2), 1, N'A. P. J. Abdul Kalam', N'')
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'1f1d7712-031b-4346-b28b-f73c0ad5a2fb', N'', N'', CAST(N'2019-08-05T10:35:29.5710844' AS DateTime2), CAST(N'2019-08-05T10:35:29.5710844' AS DateTime2), 1, N'Robin Sharma', N'')
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'2954c3ee-23ed-46e7-b0d9-0ec247148394', N'', N'', CAST(N'2019-08-07T04:55:44.9367407' AS DateTime2), CAST(N'2019-08-07T04:55:44.9367407' AS DateTime2), 1, N'Yuval Noah Harari', N'')
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'7d8f5f51-cfd8-4653-b4a3-2fc63a698c28', N'', N'', CAST(N'2019-07-19T06:29:20.8726204' AS DateTime2), CAST(N'2019-07-19T06:29:20.8726204' AS DateTime2), 1, N'Basheer', N'Malayalam novelist')
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'aea06610-466c-4f24-9a0c-0e2c04c07b4d', N'', N'', CAST(N'2019-08-05T04:10:24.7103777' AS DateTime2), CAST(N'2019-08-05T04:10:24.7103777' AS DateTime2), 1, N'Kamlesh D. Patel', N'')
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'c11f7180-173a-4523-a842-ff83831322b5', N'', N'', CAST(N'2019-07-16T04:31:41.8924570' AS DateTime2), CAST(N'2019-07-16T05:26:38.3831465' AS DateTime2), 1, N'JK Rowling', N'British novelist, screenwriter, producer, and philanthropist')
INSERT [dbo].[Authors] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'd6b8af7b-c5ca-4ba7-b802-2a6150e5341d', N'', N'', CAST(N'2019-08-05T04:43:06.9988550' AS DateTime2), CAST(N'2019-08-05T04:43:06.9988550' AS DateTime2), 1, N'Anand Neelakantan', N'')
INSERT [dbo].[Carts] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [UserId], [ApplicationUserId], [AudioId]) VALUES (N'b05f4fc5-92bf-4204-ad3f-b5c8170d51a3', N'', N'', CAST(N'2019-08-21T06:47:09.3710399' AS DateTime2), CAST(N'2019-08-21T06:47:09.3710399' AS DateTime2), 0, N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', NULL, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'023e4201-b81a-46f5-98b6-8bb1eb9d5be4', N'', N'', CAST(N'2019-08-05T04:23:27.0982592' AS DateTime2), CAST(N'2019-08-05T04:23:27.0982592' AS DateTime2), 1, N'Literature & Fiction', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'0486b06f-e4fc-4ba7-9cd4-96a3ad9ea443', N'', N'', CAST(N'2019-08-05T04:23:51.1838096' AS DateTime2), CAST(N'2019-08-05T04:23:51.1838096' AS DateTime2), 1, N'Mysteries & Thrillers', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'0db72a89-d093-45fb-afc4-17b1fed7883e', N'', N'', CAST(N'2019-08-05T04:41:11.6874377' AS DateTime2), CAST(N'2019-08-05T04:41:11.6874377' AS DateTime2), 1, N'Mythological fiction', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'0f689f87-fb58-4e06-9110-81513bcff492', N'', N'', CAST(N'2019-08-05T04:22:51.0125683' AS DateTime2), CAST(N'2019-08-05T04:22:51.0125683' AS DateTime2), 1, N'Food & Wine', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'11442345-2826-4e0d-8469-e0cebf1ce7af', N'', N'', CAST(N'2019-08-05T04:24:45.6423854' AS DateTime2), CAST(N'2019-08-05T04:24:45.6423854' AS DateTime2), 1, N'Science & Math', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'11d825aa-1ef4-425c-8c0d-3578cd14421a', N'', N'', CAST(N'2019-08-05T04:22:26.5837042' AS DateTime2), CAST(N'2019-08-05T04:22:26.5837042' AS DateTime2), 1, N'Computers & Technology', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'132ea667-3ef6-4494-8bc1-386ac81db232', N'', N'', CAST(N'2019-07-16T09:51:43.6541425' AS DateTime2), CAST(N'2019-07-16T09:53:18.7969066' AS DateTime2), 1, N'Arts & Entertainment', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'1aaef59a-9f96-4817-8ad9-4db86b2eae10', N'', N'', CAST(N'2019-08-05T04:22:07.9593415' AS DateTime2), CAST(N'2019-08-05T04:22:07.9593415' AS DateTime2), 1, N'Comedy', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'2252c338-b981-46fa-bfb1-7964fe9ebe57', N'', N'', CAST(N'2019-08-05T04:24:02.4117171' AS DateTime2), CAST(N'2019-08-05T04:24:02.4117171' AS DateTime2), 1, N'Parenting & Relationships', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'24e0abf2-5282-4dea-a3bb-622a61f744fb', N'', N'', CAST(N'2019-08-05T04:26:19.5871264' AS DateTime2), CAST(N'2019-08-05T04:26:19.5871264' AS DateTime2), 1, N'Travel', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'2bd82981-3d89-473d-a25c-21f296a6b0ab', N'', N'', CAST(N'2019-07-31T11:37:42.3732123' AS DateTime2), CAST(N'2019-07-31T11:37:42.3732123' AS DateTime2), 1, N'Biographies & Memoirs', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'2d9170f3-5435-43e8-a68b-e0d9efeb3e30', N'', N'', CAST(N'2019-07-31T11:32:20.7509507' AS DateTime2), CAST(N'2019-07-31T11:32:20.7509507' AS DateTime2), 1, N'Business & Money', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'3b461038-bd2f-4499-9f75-7e9120250eb2', N'', N'', CAST(N'2019-08-05T04:24:26.8083359' AS DateTime2), CAST(N'2019-08-05T04:24:26.8083359' AS DateTime2), 1, N'Religion & Spirituality', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'722da254-7b2a-4fa4-9d8f-722d5ac04f61', N'', N'', CAST(N'2019-08-05T04:25:25.6274199' AS DateTime2), CAST(N'2019-08-05T04:25:25.6274199' AS DateTime2), 1, N'Self-Development', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'79c9ae89-efa8-4389-9d35-a9805421a5bd', N'', N'', CAST(N'2019-07-24T10:03:24.3075816' AS DateTime2), CAST(N'2019-07-24T10:03:24.3075816' AS DateTime2), 1, N'Classics', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'832d9518-13b0-4bea-af80-658f4d62a922', N'', N'', CAST(N'2019-08-05T04:23:37.5076888' AS DateTime2), CAST(N'2019-08-05T04:23:37.5076888' AS DateTime2), 1, N'Mature Content', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'9677cf0b-28d8-4fd6-841a-d79db8f8d408', N'', N'', CAST(N'2019-08-05T04:22:41.4120267' AS DateTime2), CAST(N'2019-08-05T04:22:41.4120267' AS DateTime2), 1, N'Education & Teaching', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'a55fe2a9-158e-4712-80b7-2a0f4006065c', N'', N'', CAST(N'2019-08-05T04:26:05.6321079' AS DateTime2), CAST(N'2019-08-05T04:26:05.6321079' AS DateTime2), 1, N'Teen & Young Adult', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'b767c835-b8b7-4ce3-a62e-806b09a08bf3', N'', N'', CAST(N'2019-08-05T04:23:01.0814856' AS DateTime2), CAST(N'2019-08-05T04:23:01.0814856' AS DateTime2), 1, N'Health, Fitness & Dieting', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'b83b3c71-2323-42d1-9ac9-6395fff54848', N'', N'', CAST(N'2019-08-05T04:25:50.4227410' AS DateTime2), CAST(N'2019-08-05T04:25:50.4227410' AS DateTime2), 1, N'Sports & Outdoors', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'd788daf5-c310-45e9-bb99-491678fe31c4', N'', N'', CAST(N'2019-08-05T04:24:37.3085498' AS DateTime2), CAST(N'2019-08-05T04:24:37.3085498' AS DateTime2), 1, N'Romance', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'dca7311b-f37d-4756-a155-c81292dee442', N'', N'', CAST(N'2019-08-05T04:23:14.9781893' AS DateTime2), CAST(N'2019-08-05T04:23:14.9781893' AS DateTime2), 1, N'History', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'df494c95-324e-4387-b720-c09df78f3217', N'', N'', CAST(N'2019-08-05T04:25:09.3524360' AS DateTime2), CAST(N'2019-08-05T04:25:09.3524360' AS DateTime2), 1, N'Science Fiction & Fantasy', N'')
INSERT [dbo].[Categories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'ffa05167-40dc-4c7f-898f-ae64e63d3fa8', N'', N'', CAST(N'2019-08-05T04:24:15.7529788' AS DateTime2), CAST(N'2019-08-05T04:24:15.7529788' AS DateTime2), 1, N'Politics & Social Sciences', N'')
INSERT [dbo].[Countries] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [CurrencyId]) VALUES (N'b9178022-9d0c-4ca5-aacb-b8f149ac4738', N'', N'', CAST(N'2019-08-20T05:31:44.5704136' AS DateTime2), CAST(N'2019-08-20T05:31:44.5704136' AS DateTime2), 1, N'India', N'f2cb39ad-1272-4adb-a8f4-82f4b894a431')
INSERT [dbo].[Currencies] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [MainSuffix], [SubSuffix], [CurrencySymbol], [DecimalPoints]) VALUES (N'1ed8d83d-ef7a-4069-bf15-5eec05ba13a4', N'', N'', CAST(N'2019-08-20T06:13:10.8690148' AS DateTime2), CAST(N'2019-08-20T06:13:10.8690148' AS DateTime2), 1, N'KWD', N'Kuwaiti Dinar', N'Fils', N'KWD', 3)
INSERT [dbo].[Currencies] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [MainSuffix], [SubSuffix], [CurrencySymbol], [DecimalPoints]) VALUES (N'2c85f04a-8bd4-4f94-b8e6-7c190d476275', N'', N'', CAST(N'2019-08-20T06:11:56.0671591' AS DateTime2), CAST(N'2019-08-20T06:11:56.0671591' AS DateTime2), 1, N'OMR', N'Omani Riyals', N'Baisa', N'OMR', 3)
INSERT [dbo].[Currencies] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [MainSuffix], [SubSuffix], [CurrencySymbol], [DecimalPoints]) VALUES (N'34524b31-bc9d-4699-a8bc-86d5c02032c7', N'', N'', CAST(N'2019-08-20T06:14:04.1980198' AS DateTime2), CAST(N'2019-08-20T06:14:04.1980198' AS DateTime2), 1, N'AED', N'Dirhams', N'Fils', N'AED', 2)
INSERT [dbo].[Currencies] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [MainSuffix], [SubSuffix], [CurrencySymbol], [DecimalPoints]) VALUES (N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', N'', N'', CAST(N'2019-08-20T05:27:00.5542474' AS DateTime2), CAST(N'2019-08-20T05:27:00.5542474' AS DateTime2), 1, N'INR', N'Rupees', N'Paisa', N'₹', 2)
INSERT [dbo].[CurrencyValues] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [CurrencyId], [ValueInId], [Value]) VALUES (N'200F0FB3-870D-4E19-93C2-6D975536352D', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-20T20:03:26.0733333' AS DateTime2), CAST(N'2019-08-21T09:34:04.7233333' AS DateTime2), 1, N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', N'34524b31-bc9d-4699-a8bc-86d5c02032c7', CAST(19.52000 AS Decimal(8, 5)))
INSERT [dbo].[CurrencyValues] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [CurrencyId], [ValueInId], [Value]) VALUES (N'21CCD889-9E9D-44B3-8CEE-31CF23F784BA', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-20T17:57:12.4633333' AS DateTime2), CAST(N'2019-08-21T09:34:04.7100000' AS DateTime2), 1, N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', N'2c85f04a-8bd4-4f94-b8e6-7c190d476275', CAST(186.23000 AS Decimal(8, 5)))
INSERT [dbo].[CurrencyValues] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [CurrencyId], [ValueInId], [Value]) VALUES (N'31702F94-2851-4F0A-94B0-F72FE19BDE00', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T09:33:22.8900000' AS DateTime2), CAST(N'2019-08-21T09:33:22.8900000' AS DateTime2), 1, N'2c85f04a-8bd4-4f94-b8e6-7c190d476275', N'f2cb39ad-1272-4adb-a8f4-82f4b894a431', CAST(0.00500 AS Decimal(8, 5)))
INSERT [dbo].[CurrencyValues] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [CurrencyId], [ValueInId], [Value]) VALUES (N'5B7F3AF7-D609-49DE-8C67-CF5655575379', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-20T19:47:07.7966667' AS DateTime2), CAST(N'2019-08-21T09:34:04.7533333' AS DateTime2), 1, N'2c85f04a-8bd4-4f94-b8e6-7c190d476275', N'34524b31-bc9d-4699-a8bc-86d5c02032c7', CAST(0.10000 AS Decimal(8, 5)))
INSERT [dbo].[Feedbacks] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [UserId], [Rate], [Title], [Description], [Host], [Location]) VALUES (N'5a929962-a4a2-4335-ae20-d0edfc48a5f3', N'', N'', CAST(N'2019-08-09T04:12:47.2192440' AS DateTime2), CAST(N'2019-08-09T05:03:25.9960320' AS DateTime2), 1, N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', 5, N'', N'Your feedback here', N'192.168.1.17', NULL)
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'28909290-0a61-4056-86b2-2741e8bfc173', N'', N'', CAST(N'2019-08-20T10:28:58.6276574' AS DateTime2), CAST(N'2019-08-20T10:28:58.6276574' AS DateTime2), 0, N'0eff', N'dfgsG', N'0ff')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'4419ce2d-3927-4a31-9d3d-e8377c5bc3fb', N'', N'', CAST(N'2019-07-16T06:14:30.8308797' AS DateTime2), CAST(N'2019-08-21T06:25:46.3890370' AS DateTime2), 1, N'English', N'vsadg', N'EN')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'7a189134-8fb2-4838-8549-216e8778595d', N'', N'', CAST(N'2019-07-18T05:00:19.9507221' AS DateTime2), CAST(N'2019-08-22T07:38:22.4610018' AS DateTime2), 1, N'Malayalam', N'Malayalam is a Nice Language', N'ML')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'8df32dfc-b33d-4003-b025-1aa699d1be3d', N'', N'', CAST(N'2019-08-20T11:10:04.1337019' AS DateTime2), CAST(N'2019-08-20T11:10:04.1337019' AS DateTime2), 1, N'Kannada', N'Kannada is a nice language', N'KN')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'9334a7e1-9886-4888-9b88-a6b5374663aa', N'', N'', CAST(N'2019-08-21T03:29:17.6254367' AS DateTime2), CAST(N'2019-08-21T04:18:12.3483703' AS DateTime2), 0, N'dssdff', N'DFSADF', N'sDFSdf')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'93ddd4e8-0f4a-4349-834c-aa18313129d2', N'', N'', CAST(N'2019-08-21T03:26:19.2906549' AS DateTime2), CAST(N'2019-08-21T03:26:19.2906549' AS DateTime2), 0, N'12', N'12', N'1924521')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'b5988753-d396-4872-a681-d55ca7ff3b00', N'', N'', CAST(N'2019-08-21T03:28:54.4559751' AS DateTime2), CAST(N'2019-08-21T04:18:45.5263058' AS DateTime2), 0, N'DWFdsf', N'afdsadf', N'SDFsdf')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'b5b0cfd8-d75a-43ba-b521-6875bc27fc3d', N'', N'', CAST(N'2019-08-21T06:09:05.3883448' AS DateTime2), CAST(N'2019-08-22T07:37:25.7719620' AS DateTime2), 1, N'Telugu', N'Telugu is a nice language', N'TL')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'd3ce1fca-a44b-4d65-829c-ad9de41c7f78', N'', N'', CAST(N'2019-08-20T10:46:18.7025898' AS DateTime2), CAST(N'2019-08-20T10:46:18.7025898' AS DateTime2), 0, N'Hindi', N'Hindi is a nice language', N'HN')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'd94f6af5-b79f-4ece-8ed9-12d6aa399d30', N'', N'', CAST(N'2019-08-21T06:19:11.3828511' AS DateTime2), CAST(N'2019-08-21T06:26:01.2886339' AS DateTime2), 1, N'Gujarati', N'Gujarati is a nice language...', N'GJ')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'dee09e78-93d0-44e8-8694-8b5e56fdd42b', N'', N'', CAST(N'2019-08-21T03:30:51.5039586' AS DateTime2), CAST(N'2019-08-21T05:13:06.9569918' AS DateTime2), 0, N'QDADFAdf', N'asfsdafasdf', N'FAfadsf')
INSERT [dbo].[Languages] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Code]) VALUES (N'e9ab59bf-57d3-4021-a0c4-7e97767e4e58', N'', N'', CAST(N'2019-08-21T03:24:05.3837886' AS DateTime2), CAST(N'2019-08-21T03:24:05.3837886' AS DateTime2), 0, N'23', N'23', N'23')
INSERT [dbo].[Medias] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Ref], [MediaType], [FileName], [Extension], [Source], [ContentType], [ContentLength], [ApplicationUserId]) VALUES (N'650b79c7-8b71-4e3c-b9e8-46968c00abf8', NULL, NULL, CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), 1, N'21a89ebe-3dd4-4785-9083-ab3df5e91fcc', 2, N'https://images.template.net/wp-content/uploads/2014/05/Vintage-Book-Cover-Template.jpg', N'jpg', N'', N'image/jpeg', 20, NULL)
INSERT [dbo].[Medias] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Ref], [MediaType], [FileName], [Extension], [Source], [ContentType], [ContentLength], [ApplicationUserId]) VALUES (N'650b79c7-8b71-4e3c-b9e8-4696c00abf8', NULL, NULL, CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), 1, N'21a89ebe-3dd4-4785-9083-ab3df5e91fcc', 2, N'https://images.template.net/wp-content/uploads/2014/05/Vintage-Book-Cover-Template.jpg', N'jpg', N'', N'image/jpeg', 20, NULL)
INSERT [dbo].[MembershipTypes] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [Description]) VALUES (N'36296f87-1ed4-4e13-a679-79be2f026931', N'', N'', CAST(N'2019-07-01T07:43:44.3011581' AS DateTime2), CAST(N'2019-07-01T07:43:44.3011581' AS DateTime2), 1, N'Library', N'Library')
INSERT [dbo].[MembershipTypes] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [Description]) VALUES (N'7b2aafc2-b00f-4954-856d-174f42e25fd2', N'', N'', CAST(N'2019-07-01T07:43:44.3314843' AS DateTime2), CAST(N'2019-07-01T07:43:44.3314843' AS DateTime2), 1, N'Society', N'Society')
INSERT [dbo].[MembershipTypes] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [Description]) VALUES (N'c6a1430c-2fc8-40ee-9210-3f1b3f9f4e95', N'', N'', CAST(N'2019-07-01T07:43:44.3442116' AS DateTime2), CAST(N'2019-07-01T07:43:44.3442116' AS DateTime2), 1, N'Organisation', N'Organisation')
INSERT [dbo].[MembershipTypes] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Name], [Description]) VALUES (N'f21532af-a8c5-473e-859a-0fab18d6e192', N'', N'', CAST(N'2019-07-01T07:43:44.3882520' AS DateTime2), CAST(N'2019-07-01T07:43:44.3882520' AS DateTime2), 1, N'Other', N'Other')
INSERT [dbo].[Narrators] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Gender]) VALUES (N'27612e73-99cc-44d7-9f51-9e88d49b123b', N'', N'', CAST(N'2019-08-05T10:38:10.6516457' AS DateTime2), CAST(N'2019-08-05T10:38:10.6516457' AS DateTime2), 1, N'Robin Sharma', N'', 0)
INSERT [dbo].[Narrators] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Gender]) VALUES (N'650b79c7-8b71-4e3c-b9e8-46968c00abf8', N'', N'', CAST(N'2019-07-16T06:42:48.9030635' AS DateTime2), CAST(N'2019-07-16T06:43:02.0465233' AS DateTime2), 1, N'Balakrishnan', N'', 0)
INSERT [dbo].[Narrators] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Gender]) VALUES (N'b35766af-0ee0-4983-bd86-1eb83922420f', N'', N'', CAST(N'2019-08-07T05:00:28.0493923' AS DateTime2), CAST(N'2019-08-07T05:00:28.0493923' AS DateTime2), 1, N'Derek Perkins', N'', 0)
INSERT [dbo].[Narrators] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [Gender]) VALUES (N'f5912739-d4b0-485c-9c94-35272dfcff46', N'', N'', CAST(N'2019-08-05T04:12:49.8083484' AS DateTime2), CAST(N'2019-08-05T04:12:49.8083484' AS DateTime2), 1, N'Siddhanta Pinto', N'', 0)
INSERT [dbo].[Picture] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MimeType], [Filename], [SeoFilename], [AltAttribute], [TitleAttribute], [IsNew]) VALUES (N'b6fc4dec-2feb-44af-9f0c-65e94859321c', N'', N'', CAST(N'2019-08-07T10:54:59.3336006' AS DateTime2), CAST(N'2019-08-07T11:12:01.9423170' AS DateTime2), 1, N'', N'https://m.media-amazon.com/images/I/51t+htMs9oL._SL500_.jpg', N'', NULL, NULL, 0)
INSERT [dbo].[Picture] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MimeType], [Filename], [SeoFilename], [AltAttribute], [TitleAttribute], [IsNew]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c44', NULL, NULL, CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), 1, NULL, N'https://m.media-amazon.com/images/I/41IGLTetzeL._SL500_.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Picture] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MimeType], [Filename], [SeoFilename], [AltAttribute], [TitleAttribute], [IsNew]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c45', NULL, NULL, CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), 1, NULL, N'https://m.media-amazon.com/images/I/51AAweqxA1L._SL500_.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Picture] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MimeType], [Filename], [SeoFilename], [AltAttribute], [TitleAttribute], [IsNew]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c48', NULL, NULL, CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), 1, NULL, N'https://m.media-amazon.com/images/I/51UCKitsVKL._SL500_.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Picture] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MimeType], [Filename], [SeoFilename], [AltAttribute], [TitleAttribute], [IsNew]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c49', NULL, NULL, CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), 1, NULL, N'https://m.media-amazon.com/images/I/51t+htMs9oL._SL500_.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Picture] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MimeType], [Filename], [SeoFilename], [AltAttribute], [TitleAttribute], [IsNew]) VALUES (N'f97a5c25-d29a-4cb1-9331-97cff7c3f242', N'', N'', CAST(N'2019-08-07T10:54:59.3346219' AS DateTime2), CAST(N'2019-08-07T11:12:01.9436101' AS DateTime2), 1, N'', N'https://m.media-amazon.com/images/I/511HccWipML._SL500_.jpg', N'', NULL, NULL, 0)
INSERT [dbo].[ProductAuthorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [AuthorId]) VALUES (N'3761fe54-741b-4896-a201-743bb51f0c45', N'', N'', CAST(N'2019-08-05T10:36:31.3513361' AS DateTime2), CAST(N'2019-08-05T10:36:31.3513361' AS DateTime2), 0, N'a2e36a5a-0f7c-4a2b-86dd-048f30393a4b', N'1f1d7712-031b-4346-b28b-f73c0ad5a2fb')
INSERT [dbo].[ProductAuthorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [AuthorId]) VALUES (N'c7cd1f54-073b-4d47-bc0f-c7afcfbf3987', N'', N'', CAST(N'2019-08-05T09:17:01.7312133' AS DateTime2), CAST(N'2019-08-05T09:17:01.7312133' AS DateTime2), 0, N'4a0cf807-d97e-40c4-b5f4-1d5454878571', N'd6b8af7b-c5ca-4ba7-b802-2a6150e5341d')
INSERT [dbo].[ProductAuthorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [AuthorId]) VALUES (N'e864d67d-553a-47bb-9155-9f8038675d3d', N'', N'', CAST(N'2019-08-07T04:56:17.1126493' AS DateTime2), CAST(N'2019-08-07T04:56:17.1126493' AS DateTime2), 0, N'b329ad38-048f-4f53-9769-c3193bef8ad5', N'2954c3ee-23ed-46e7-b0d9-0ec247148394')
INSERT [dbo].[ProductAuthorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [AuthorId]) VALUES (N'fe404ac3-c969-4a55-bf28-fe2a5af7484e', N'', N'', CAST(N'2019-08-05T10:54:40.9077657' AS DateTime2), CAST(N'2019-08-05T10:54:40.9077657' AS DateTime2), 0, N'20f2a982-c398-4677-b3e2-b69238cca90c', N'14fd6546-fdbe-45a0-b368-e703f9e45643')
INSERT [dbo].[ProductAuthorMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [AuthorId]) VALUES (N'fe404ac3-c969-4a55-bf28-fe2a5f7484e', N'', N'', CAST(N'2019-08-05T10:54:40.9077657' AS DateTime2), CAST(N'2019-08-05T10:54:40.9077657' AS DateTime2), 0, N'4a0cf807-d97e-40c4-b5f4-1d5454878571', N'aea06610-466c-4f24-9a0c-0e2c04c07b4d')
INSERT [dbo].[Products] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MembershipTypeId]) VALUES (N'20f2a982-c398-4677-b3e2-b69238cca90c', N'', N'', CAST(N'2019-08-05T10:54:40.7694553' AS DateTime2), CAST(N'2019-08-05T10:54:40.7694553' AS DateTime2), 1, NULL)
INSERT [dbo].[Products] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MembershipTypeId]) VALUES (N'4a0cf807-d97e-40c4-b5f4-1d5454878571', N'', N'', CAST(N'2019-08-05T09:17:01.6205983' AS DateTime2), CAST(N'2019-08-05T09:17:01.6205983' AS DateTime2), 1, NULL)
INSERT [dbo].[Products] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MembershipTypeId]) VALUES (N'a2e36a5a-0f7c-4a2b-86dd-048f30393a4b', N'', N'', CAST(N'2019-08-05T10:36:31.0589457' AS DateTime2), CAST(N'2019-08-05T10:36:31.0589457' AS DateTime2), 1, NULL)
INSERT [dbo].[Products] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [MembershipTypeId]) VALUES (N'b329ad38-048f-4f53-9769-c3193bef8ad5', N'', N'', CAST(N'2019-08-07T04:56:16.7894675' AS DateTime2), CAST(N'2019-08-07T04:56:16.7894675' AS DateTime2), 1, NULL)
INSERT [dbo].[ProductSubCategoryMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [SubCategoryId]) VALUES (N'396a6efe-477e-45e8-8c4d-7c76acbab2ed', N'', N'', CAST(N'2019-08-07T04:56:17.1126037' AS DateTime2), CAST(N'2019-08-07T04:56:17.1126037' AS DateTime2), 1, N'b329ad38-048f-4f53-9769-c3193bef8ad5', N'71a5cf4a-a338-4c80-ad5b-f15468fbaabe')
INSERT [dbo].[ProductSubCategoryMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [SubCategoryId]) VALUES (N'763a4851-ebcd-4a98-8c7d-8fbfad4acf97', N'', N'', CAST(N'2019-08-05T10:36:31.3512512' AS DateTime2), CAST(N'2019-08-05T10:36:31.3512512' AS DateTime2), 1, N'a2e36a5a-0f7c-4a2b-86dd-048f30393a4b', N'd347756f-acba-45fd-b714-d1cfa1281d89')
INSERT [dbo].[ProductSubCategoryMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [SubCategoryId]) VALUES (N'763a4851-ebcd-4a98-8c7d-8fbfad4acf98', N'', N'', CAST(N'2019-08-05T10:36:31.3512512' AS DateTime2), CAST(N'2019-08-05T10:36:31.3512512' AS DateTime2), 1, N'a2e36a5a-0f7c-4a2b-86dd-048f30393a4b', N'b740260d-b22a-4579-8002-e8cbd114f027')
INSERT [dbo].[ProductSubCategoryMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [SubCategoryId]) VALUES (N'a2f13acf-06a4-431b-9556-bd68d58c5ea5', N'', N'', CAST(N'2019-08-05T09:17:01.7312025' AS DateTime2), CAST(N'2019-08-05T09:17:01.7312025' AS DateTime2), 1, N'4a0cf807-d97e-40c4-b5f4-1d5454878571', N'd347756f-acba-45fd-b714-d1cfa1281d89')
INSERT [dbo].[ProductSubCategoryMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [ProductId], [SubCategoryId]) VALUES (N'b70d240e-0458-493d-bd64-98045cdef8aa', N'', N'', CAST(N'2019-08-05T10:54:40.9077008' AS DateTime2), CAST(N'2019-08-05T10:54:40.9077008' AS DateTime2), 1, N'20f2a982-c398-4677-b3e2-b69238cca90c', N'6a760488-8d09-4e64-adec-16a06ab8c5f2')
INSERT [dbo].[PromotionAvailableToMaps] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionId], [RoleId]) VALUES (N'E7810E42-231C-43C3-A130-0A2BEB548A61', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T20:06:16.5266667' AS DateTime2), CAST(N'2019-08-21T20:06:16.5266667' AS DateTime2), 1, N'F76A75EC-611A-4FC8-9743-C8620131CF8D', N'f6adf5f2-0bba-4f44-8620-2540906afd9f')
INSERT [dbo].[PromotionItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionId], [ItemTypeId], [ItemId], [PromotionGetType], [Get]) VALUES (N'5FBD0FBB-00EF-4EBB-9483-FB64224842B0', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T19:54:08.2933333' AS DateTime2), CAST(N'2019-08-21T20:06:16.4866667' AS DateTime2), 1, N'F76A75EC-611A-4FC8-9743-C8620131CF8D', 1, N'14032467-5953-43c0-837c-b9e6f4ec944d', 1, CAST(40.00 AS Decimal(18, 2)))
INSERT [dbo].[PromotionItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionId], [ItemTypeId], [ItemId], [PromotionGetType], [Get]) VALUES (N'8495919E-7408-48E2-92FE-1489FF97FBA8', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T19:54:08.3333333' AS DateTime2), CAST(N'2019-08-21T19:54:08.3333333' AS DateTime2), 1, N'C4A89A55-A6CF-4F60-95DB-34A301309D62', 1, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86', 1, CAST(30.00 AS Decimal(18, 2)))
INSERT [dbo].[Promotions] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionCode], [Title], [ValidFrom], [ValidTo], [Description]) VALUES (N'1E7C872E-D9C2-4052-9DF7-6188909409FA', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T19:36:13.1866667' AS DateTime2), CAST(N'2019-08-21T19:36:13.1866667' AS DateTime2), 1, N'ONAM22', N'Onam Bumber', CAST(N'2019-08-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-08-31T00:00:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[Promotions] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionCode], [Title], [ValidFrom], [ValidTo], [Description]) VALUES (N'662FA5EF-065C-4765-AB46-4BBA31751810', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T20:00:23.9766667' AS DateTime2), CAST(N'2019-08-21T20:00:23.9766667' AS DateTime2), 1, N'ONAM22', N'Onam Bumber', CAST(N'2019-08-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-08-31T00:00:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[Promotions] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionCode], [Title], [ValidFrom], [ValidTo], [Description]) VALUES (N'BD7C8DA6-A419-48F6-A5B6-4A204376B97D', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T19:00:15.1333333' AS DateTime2), CAST(N'2019-08-21T19:00:15.1333333' AS DateTime2), 1, N'ONAM22', N'Onam Bumber', CAST(N'2019-08-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-08-31T00:00:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[Promotions] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionCode], [Title], [ValidFrom], [ValidTo], [Description]) VALUES (N'C4A89A55-A6CF-4F60-95DB-34A301309D62', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T19:54:08.1100000' AS DateTime2), CAST(N'2019-08-21T19:54:08.1100000' AS DateTime2), 1, N'ONAM22', N'Onam Bumber', CAST(N'2019-08-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-08-31T00:00:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[Promotions] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [PromotionCode], [Title], [ValidFrom], [ValidTo], [Description]) VALUES (N'F76A75EC-611A-4FC8-9743-C8620131CF8D', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', CAST(N'2019-08-21T20:06:16.3466667' AS DateTime2), CAST(N'2019-08-21T20:06:16.3466667' AS DateTime2), 1, N'ONAM22', N'Onam Bumber', CAST(N'2019-08-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-08-31T00:00:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[Publishers] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'3c2b39c6-630d-4cce-91aa-0a0433cf3f94', N'', N'', CAST(N'2019-07-16T06:17:43.5680259' AS DateTime2), CAST(N'2019-07-16T06:17:43.5680259' AS DateTime2), 1, N'DC Books', N'')
INSERT [dbo].[Publishers] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'62ae34ba-edb8-4dd9-b4bd-87411d8f9daa', N'', N'', CAST(N'2019-08-05T08:59:38.3440658' AS DateTime2), CAST(N'2019-08-05T08:59:38.3440658' AS DateTime2), 1, N'Kerala Media Acadamy', N'')
INSERT [dbo].[Publishers] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'6e92f212-8c35-42da-851a-468418282553', N'', N'', CAST(N'2019-08-05T04:45:12.2798446' AS DateTime2), CAST(N'2019-08-05T04:45:12.2798446' AS DateTime2), 1, N'Audible Studios', N'')
INSERT [dbo].[Publishers] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'954d2a42-2e5b-46e3-9c47-497cee0db2d0', N'', N'', CAST(N'2019-08-05T04:39:14.2770901' AS DateTime2), CAST(N'2019-08-05T04:39:14.2770901' AS DateTime2), 1, N'Leadstart Publishing', N'')
INSERT [dbo].[Publishers] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'cc4d7252-648a-4de7-9efb-e965a87897cd', N'', N'', CAST(N'2019-08-05T08:59:20.2932467' AS DateTime2), CAST(N'2019-08-05T08:59:20.2932467' AS DateTime2), 1, N'Podium Publishing', N'')
INSERT [dbo].[Publishers] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'd602e2c7-ba0d-4271-87dd-6f997d31fab1', N'', N'', CAST(N'2019-07-16T05:43:13.6510219' AS DateTime2), CAST(N'2019-07-16T06:39:09.8848291' AS DateTime2), 0, N'Bloomsbury Publishing', N'British independent, worldwide publishing house of fiction and non-fiction')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'0ae1925d-fe22-4d4a-af9e-36996adf598f', N'', N'', CAST(N'2019-08-05T07:33:33.0173956' AS DateTime2), CAST(N'2019-08-05T07:33:33.0173956' AS DateTime2), 1, N'Comedians', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'12dd5b08-bfb5-4803-aff1-c473704c245d', N'', N'', CAST(N'2019-08-05T07:19:29.6065947' AS DateTime2), CAST(N'2019-08-05T07:19:29.6065947' AS DateTime2), 1, N'Film, Radio & TV', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'1526478b-1949-47fc-b622-516821c49e2e', N'', N'', CAST(N'2019-08-05T07:21:15.4748703' AS DateTime2), CAST(N'2019-08-05T07:21:15.4748703' AS DateTime2), 1, N'Theatre', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'1df7cbdc-d04b-4bf0-ae18-f446a595a6e0', N'', N'', CAST(N'2019-08-05T07:22:35.4866645' AS DateTime2), CAST(N'2019-08-05T07:22:35.4866645' AS DateTime2), 1, N'Business Leaders', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'26e694b5-b9ad-421c-bbef-7620f92f5daf', N'', N'', CAST(N'2019-08-05T07:18:22.1150892' AS DateTime2), CAST(N'2019-08-05T07:18:22.1150892' AS DateTime2), 1, N'Art', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'32e2fe3a-1e51-447d-8826-71c162fcc2bc', N'', N'', CAST(N'2019-08-05T07:21:37.7533010' AS DateTime2), CAST(N'2019-08-05T07:21:37.7533010' AS DateTime2), 1, N'Visual Arts', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'5056be88-1dc2-487b-ada1-e51d83e38a0c', N'', N'', CAST(N'2019-08-05T07:34:20.8955166' AS DateTime2), CAST(N'2019-08-05T07:34:20.8955166' AS DateTime2), 1, N'Historical', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'66909f93-9c6b-4339-b083-ef2c569e95ee', N'', N'', CAST(N'2019-08-05T07:33:48.6935090' AS DateTime2), CAST(N'2019-08-05T07:33:48.6935090' AS DateTime2), 1, N'Criminals', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'6a760488-8d09-4e64-adec-16a06ab8c5f2', N'', N'', CAST(N'2019-08-05T10:54:24.7766522' AS DateTime2), CAST(N'2019-08-05T10:54:24.7766522' AS DateTime2), 1, N'Personal Development & Self-Help Books', N'', N'b767c835-b8b7-4ce3-a62e-806b09a08bf3')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'71a5cf4a-a338-4c80-ad5b-f15468fbaabe', N'', N'', CAST(N'2019-08-07T04:54:36.5092676' AS DateTime2), CAST(N'2019-08-07T04:54:36.5092676' AS DateTime2), 1, N'World', N'', N'dca7311b-f37d-4756-a155-c81292dee442')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'968b8627-9576-4253-aacb-1b43f9dba7c8', N'', N'', CAST(N'2019-08-05T07:22:21.9782538' AS DateTime2), CAST(N'2019-08-05T07:22:21.9782538' AS DateTime2), 1, N'Artists, Authors & Musicians ', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'9be3d9ab-6bc8-4bd0-8b81-5b7804fcc38b', N'', N'', CAST(N'2019-08-05T07:19:09.6565413' AS DateTime2), CAST(N'2019-08-05T07:19:09.6565413' AS DateTime2), 1, N'Celebrity Bios', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'b740260d-b22a-4579-8002-e8cbd114f027', N'', N'', CAST(N'2019-08-05T07:22:12.1380891' AS DateTime2), CAST(N'2019-08-05T07:22:12.1380891' AS DateTime2), 1, N'Architects', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'd347756f-acba-45fd-b714-d1cfa1281d89', N'', N'', CAST(N'2019-08-05T09:16:39.3407883' AS DateTime2), CAST(N'2019-08-05T09:16:39.3407883' AS DateTime2), 1, N'Contemporary Fiction', N'', N'023e4201-b81a-46f5-98b6-8bb1eb9d5be4')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'd7a3cc8b-7950-4a53-87c3-52fb474c6536', N'', N'', CAST(N'2019-08-05T07:19:54.0316358' AS DateTime2), CAST(N'2019-08-05T07:19:54.0316358' AS DateTime2), 1, N'Music', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'daec3e8e-286c-40de-8dad-1f0dbceee829', N'', N'', CAST(N'2019-08-05T07:19:39.6738776' AS DateTime2), CAST(N'2019-08-05T07:19:39.6738776' AS DateTime2), 1, N'Interviews & Panels', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'e32a5fe4-e294-44e4-83a9-a12de0eb5704', N'', N'', CAST(N'2019-08-05T07:18:51.2937769' AS DateTime2), CAST(N'2019-08-05T07:18:51.2937769' AS DateTime2), 1, N'Art History', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'f4b5156b-4e4b-46e5-819d-3119113f41b3', N'', N'', CAST(N'2019-08-05T07:20:08.6307651' AS DateTime2), CAST(N'2019-08-05T07:20:57.9751326' AS DateTime2), 1, N'Stand-Up & Live Comedy', N'', N'132ea667-3ef6-4494-8bc1-386ac81db232')
INSERT [dbo].[SubCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description], [CategoryId]) VALUES (N'f58abe64-3445-4a8e-910c-31016db3006b', N'', N'', CAST(N'2019-08-05T07:34:04.0565733' AS DateTime2), CAST(N'2019-08-05T07:34:04.0565733' AS DateTime2), 1, N'Ethnic & National', N'', N'2bd82981-3d89-473d-a25c-21f296a6b0ab')
INSERT [dbo].[TaxCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxCategoryName], [CountryID]) VALUES (N'1c02cb3b-fa71-4c17-a2bc-759f1546b411', N'', N'', CAST(N'2019-08-19T12:30:36.1056256' AS DateTime2), CAST(N'2019-08-19T12:30:36.1056256' AS DateTime2), 1, N'GST 8%', N'b9178022-9d0c-4ca5-aacb-b8f149ac4738')
INSERT [dbo].[TaxCategories] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxCategoryName], [CountryID]) VALUES (N'234f743c-bb97-45e2-8023-740ac6eb4ae6', N'', N'', CAST(N'2019-08-19T12:26:13.9504087' AS DateTime2), CAST(N'2019-08-19T12:26:13.9504087' AS DateTime2), 1, N'GST 10%', N'b9178022-9d0c-4ca5-aacb-b8f149ac4738')
INSERT [dbo].[TaxCategoryItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxCategoryId], [TaxItemId], [TaxPercentage]) VALUES (N'8ae48c76-7f8d-4b12-bba5-db048f37d9f2', N'', N'', CAST(N'2019-08-19T12:30:36.1057191' AS DateTime2), CAST(N'2019-08-19T12:30:36.1057191' AS DateTime2), 0, N'1c02cb3b-fa71-4c17-a2bc-759f1546b411', N'c266ae6b-577d-40b1-a0af-00ccf283dc55', CAST(4.00 AS Decimal(18, 2)))
INSERT [dbo].[TaxCategoryItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxCategoryId], [TaxItemId], [TaxPercentage]) VALUES (N'9283a3e3-7d3b-42b9-8256-7476a19937b7', N'', N'', CAST(N'2019-08-19T12:26:13.9511397' AS DateTime2), CAST(N'2019-08-19T12:26:13.9511397' AS DateTime2), 0, N'234f743c-bb97-45e2-8023-740ac6eb4ae6', N'c266ae6b-577d-40b1-a0af-00ccf283dc55', CAST(5.00 AS Decimal(18, 2)))
INSERT [dbo].[TaxCategoryItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxCategoryId], [TaxItemId], [TaxPercentage]) VALUES (N'b8fc4ad5-d668-4e23-bde7-d59419b07831', N'', N'', CAST(N'2019-08-19T12:30:36.1057106' AS DateTime2), CAST(N'2019-08-19T12:30:36.1057106' AS DateTime2), 0, N'1c02cb3b-fa71-4c17-a2bc-759f1546b411', N'22bef0f9-1049-45a0-80b0-4ce5673fffd2', CAST(4.00 AS Decimal(18, 2)))
INSERT [dbo].[TaxCategoryItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxCategoryId], [TaxItemId], [TaxPercentage]) VALUES (N'dfe03364-78b6-4a36-9543-fa33202227bf', N'', N'', CAST(N'2019-08-19T12:26:13.9510684' AS DateTime2), CAST(N'2019-08-19T12:26:13.9510684' AS DateTime2), 0, N'234f743c-bb97-45e2-8023-740ac6eb4ae6', N'22bef0f9-1049-45a0-80b0-4ce5673fffd2', CAST(5.00 AS Decimal(18, 2)))
INSERT [dbo].[TaxItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxItemName]) VALUES (N'22bef0f9-1049-45a0-80b0-4ce5673fffd2', N'', N'', CAST(N'2019-08-19T08:08:43.6269148' AS DateTime2), CAST(N'2019-08-19T08:08:43.6269148' AS DateTime2), 1, N'CGST')
INSERT [dbo].[TaxItems] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [TaxItemName]) VALUES (N'c266ae6b-577d-40b1-a0af-00ccf283dc55', N'', N'', CAST(N'2019-08-19T08:14:13.6172006' AS DateTime2), CAST(N'2019-08-19T08:14:13.6172006' AS DateTime2), 1, N'SGST')
INSERT [dbo].[Translators] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [Title], [Description]) VALUES (N'eaeb3598-1c6a-4981-90ae-49fd6b854c45', NULL, NULL, CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), CAST(N'2019-08-01T11:16:45.7702878' AS DateTime2), 1, N'Murukan', N' ')
INSERT [dbo].[WishLists] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [UserId], [ApplicationUserId], [AudioId]) VALUES (N'ea4da143-9891-44b6-9523-9a4562f038fb', N'', N'', CAST(N'2019-08-21T06:08:29.1879640' AS DateTime2), CAST(N'2019-08-21T06:08:29.1879640' AS DateTime2), 1, N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', NULL, N'339e90f9-5b3c-4119-93d7-7541ae665a7b')
INSERT [dbo].[WishLists] ([Id], [CreatedBy], [UpdatedBy], [CreatedDate], [UpdatedDate], [Active], [UserId], [ApplicationUserId], [AudioId]) VALUES (N'f1167dbb-daf7-4c34-9f54-4a48705f06c9', N'', N'', CAST(N'2019-08-21T05:53:16.4832150' AS DateTime2), CAST(N'2019-08-21T05:53:16.4832150' AS DateTime2), 1, N'7f643f8d-6bc1-44ba-b8a9-0e98d326d1e9', NULL, N'1e11f4d6-82b8-422c-9b44-b05c764a5a86')
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_ApplicationRoleId] FOREIGN KEY([ApplicationRoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_ApplicationRoleId]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_ApplicationRoleId] FOREIGN KEY([ApplicationRoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_ApplicationRoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AudioFiles]  WITH CHECK ADD  CONSTRAINT [FK_AudioFiles_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[AudioFiles] CHECK CONSTRAINT [FK_AudioFiles_Audios_AudioId]
GO
ALTER TABLE [dbo].[AudioNarratorMaps]  WITH CHECK ADD  CONSTRAINT [FK_AudioNarratorMaps_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[AudioNarratorMaps] CHECK CONSTRAINT [FK_AudioNarratorMaps_Audios_AudioId]
GO
ALTER TABLE [dbo].[AudioNarratorMaps]  WITH CHECK ADD  CONSTRAINT [FK_AudioNarratorMaps_Narrators_NarratorId] FOREIGN KEY([NarratorId])
REFERENCES [dbo].[Narrators] ([Id])
GO
ALTER TABLE [dbo].[AudioNarratorMaps] CHECK CONSTRAINT [FK_AudioNarratorMaps_Narrators_NarratorId]
GO
ALTER TABLE [dbo].[AudioPictures]  WITH CHECK ADD  CONSTRAINT [FK_AudioPictures_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[AudioPictures] CHECK CONSTRAINT [FK_AudioPictures_Audios_AudioId]
GO
ALTER TABLE [dbo].[AudioPictures]  WITH CHECK ADD  CONSTRAINT [FK_AudioPictures_Picture_PictureId] FOREIGN KEY([PictureId])
REFERENCES [dbo].[Picture] ([Id])
GO
ALTER TABLE [dbo].[AudioPictures] CHECK CONSTRAINT [FK_AudioPictures_Picture_PictureId]
GO
ALTER TABLE [dbo].[AudioRates]  WITH CHECK ADD  CONSTRAINT [FK_AudioRates_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AudioRates] CHECK CONSTRAINT [FK_AudioRates_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AudioRates]  WITH CHECK ADD  CONSTRAINT [FK_AudioRates_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[AudioRates] CHECK CONSTRAINT [FK_AudioRates_Audios_AudioId]
GO
ALTER TABLE [dbo].[AudioReviews]  WITH CHECK ADD  CONSTRAINT [FK_AudioReviews_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AudioReviews] CHECK CONSTRAINT [FK_AudioReviews_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AudioReviews]  WITH CHECK ADD  CONSTRAINT [FK_AudioReviews_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[AudioReviews] CHECK CONSTRAINT [FK_AudioReviews_Audios_AudioId]
GO
ALTER TABLE [dbo].[Audios]  WITH CHECK ADD  CONSTRAINT [FK_Audios_Currencies_CurrencyId] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[Audios] CHECK CONSTRAINT [FK_Audios_Currencies_CurrencyId]
GO
ALTER TABLE [dbo].[Audios]  WITH CHECK ADD  CONSTRAINT [FK_Audios_Languages_LanguageId] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[Audios] CHECK CONSTRAINT [FK_Audios_Languages_LanguageId]
GO
ALTER TABLE [dbo].[Audios]  WITH CHECK ADD  CONSTRAINT [FK_Audios_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Audios] CHECK CONSTRAINT [FK_Audios_Products_ProductId]
GO
ALTER TABLE [dbo].[Audios]  WITH CHECK ADD  CONSTRAINT [FK_Audios_Publishers_PublisherId] FOREIGN KEY([PublisherId])
REFERENCES [dbo].[Publishers] ([Id])
GO
ALTER TABLE [dbo].[Audios] CHECK CONSTRAINT [FK_Audios_Publishers_PublisherId]
GO
ALTER TABLE [dbo].[Audios]  WITH CHECK ADD  CONSTRAINT [FK_Audios_TaxCategories_TaxCategoryId] FOREIGN KEY([TaxCategoryId])
REFERENCES [dbo].[TaxCategories] ([Id])
GO
ALTER TABLE [dbo].[Audios] CHECK CONSTRAINT [FK_Audios_TaxCategories_TaxCategoryId]
GO
ALTER TABLE [dbo].[AudioTranslatorMaps]  WITH CHECK ADD  CONSTRAINT [FK_AudioTranslatorMaps_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[AudioTranslatorMaps] CHECK CONSTRAINT [FK_AudioTranslatorMaps_Audios_AudioId]
GO
ALTER TABLE [dbo].[AudioTranslatorMaps]  WITH CHECK ADD  CONSTRAINT [FK_AudioTranslatorMaps_Translators_TranslatorId] FOREIGN KEY([TranslatorId])
REFERENCES [dbo].[Translators] ([Id])
GO
ALTER TABLE [dbo].[AudioTranslatorMaps] CHECK CONSTRAINT [FK_AudioTranslatorMaps_Translators_TranslatorId]
GO
ALTER TABLE [dbo].[Carts]  WITH CHECK ADD  CONSTRAINT [FK_Carts_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Carts] CHECK CONSTRAINT [FK_Carts_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[Carts]  WITH CHECK ADD  CONSTRAINT [FK_Carts_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[Carts] CHECK CONSTRAINT [FK_Carts_Audios_AudioId]
GO
ALTER TABLE [dbo].[Countries]  WITH CHECK ADD  CONSTRAINT [FK_Countries_Currencies_CurrencyId] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[Countries] CHECK CONSTRAINT [FK_Countries_Currencies_CurrencyId]
GO
ALTER TABLE [dbo].[CurrencyValues]  WITH CHECK ADD  CONSTRAINT [FK_CurrencyValues_Currencies_CurrencyId] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[CurrencyValues] CHECK CONSTRAINT [FK_CurrencyValues_Currencies_CurrencyId]
GO
ALTER TABLE [dbo].[CurrencyValues]  WITH CHECK ADD  CONSTRAINT [FK_CurrencyValues_Currencies_ValueInId] FOREIGN KEY([ValueInId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[CurrencyValues] CHECK CONSTRAINT [FK_CurrencyValues_Currencies_ValueInId]
GO
ALTER TABLE [dbo].[Feedbacks]  WITH CHECK ADD  CONSTRAINT [FK_Feedbacks_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Feedbacks] CHECK CONSTRAINT [FK_Feedbacks_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[LoggedDeviceHistorys]  WITH CHECK ADD  CONSTRAINT [FK_LoggedDeviceHistorys_AspNetUsers_IdentityRef] FOREIGN KEY([IdentityRef])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[LoggedDeviceHistorys] CHECK CONSTRAINT [FK_LoggedDeviceHistorys_AspNetUsers_IdentityRef]
GO
ALTER TABLE [dbo].[Medias]  WITH CHECK ADD  CONSTRAINT [FK_Medias_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Medias] CHECK CONSTRAINT [FK_Medias_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [FK_Memberships_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [FK_Memberships_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[Memberships]  WITH CHECK ADD  CONSTRAINT [FK_Memberships_MembershipTypes_MembershipTypeId] FOREIGN KEY([MembershipTypeId])
REFERENCES [dbo].[MembershipTypes] ([Id])
GO
ALTER TABLE [dbo].[Memberships] CHECK CONSTRAINT [FK_Memberships_MembershipTypes_MembershipTypeId]
GO
ALTER TABLE [dbo].[Offers]  WITH CHECK ADD  CONSTRAINT [FK_Offers_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[Offers] CHECK CONSTRAINT [FK_Offers_Audios_AudioId]
GO
ALTER TABLE [dbo].[ProductAuthorMaps]  WITH CHECK ADD  CONSTRAINT [FK_ProductAuthorMaps_Authors_AuthorId] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Authors] ([Id])
GO
ALTER TABLE [dbo].[ProductAuthorMaps] CHECK CONSTRAINT [FK_ProductAuthorMaps_Authors_AuthorId]
GO
ALTER TABLE [dbo].[ProductAuthorMaps]  WITH CHECK ADD  CONSTRAINT [FK_ProductAuthorMaps_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductAuthorMaps] CHECK CONSTRAINT [FK_ProductAuthorMaps_Products_ProductId]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_MembershipTypes_MembershipTypeId] FOREIGN KEY([MembershipTypeId])
REFERENCES [dbo].[MembershipTypes] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_MembershipTypes_MembershipTypeId]
GO
ALTER TABLE [dbo].[ProductSubCategoryMaps]  WITH CHECK ADD  CONSTRAINT [FK_ProductSubCategoryMaps_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[ProductSubCategoryMaps] CHECK CONSTRAINT [FK_ProductSubCategoryMaps_Products_ProductId]
GO
ALTER TABLE [dbo].[ProductSubCategoryMaps]  WITH CHECK ADD  CONSTRAINT [FK_ProductSubCategoryMaps_SubCategories_SubCategoryId] FOREIGN KEY([SubCategoryId])
REFERENCES [dbo].[SubCategories] ([Id])
GO
ALTER TABLE [dbo].[ProductSubCategoryMaps] CHECK CONSTRAINT [FK_ProductSubCategoryMaps_SubCategories_SubCategoryId]
GO
ALTER TABLE [dbo].[PromotionAvailableToMaps]  WITH CHECK ADD  CONSTRAINT [FK_PromotionAvailableToMaps_Promotions_PromotionId] FOREIGN KEY([PromotionId])
REFERENCES [dbo].[Promotions] ([Id])
GO
ALTER TABLE [dbo].[PromotionAvailableToMaps] CHECK CONSTRAINT [FK_PromotionAvailableToMaps_Promotions_PromotionId]
GO
ALTER TABLE [dbo].[PromotionItems]  WITH CHECK ADD  CONSTRAINT [FK_PromotionItems_Promotions_PromotionId] FOREIGN KEY([PromotionId])
REFERENCES [dbo].[Promotions] ([Id])
GO
ALTER TABLE [dbo].[PromotionItems] CHECK CONSTRAINT [FK_PromotionItems_Promotions_PromotionId]
GO
ALTER TABLE [dbo].[Purchases]  WITH CHECK ADD  CONSTRAINT [FK_Purchases_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Purchases] CHECK CONSTRAINT [FK_Purchases_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[Purchases]  WITH CHECK ADD  CONSTRAINT [FK_Purchases_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[Purchases] CHECK CONSTRAINT [FK_Purchases_Audios_AudioId]
GO
ALTER TABLE [dbo].[SubCategories]  WITH CHECK ADD  CONSTRAINT [FK_SubCategories_Categories_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[SubCategories] CHECK CONSTRAINT [FK_SubCategories_Categories_CategoryId]
GO
ALTER TABLE [dbo].[TaxCategories]  WITH CHECK ADD  CONSTRAINT [FK_TaxCategories_Countries_CountryID] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Countries] ([Id])
GO
ALTER TABLE [dbo].[TaxCategories] CHECK CONSTRAINT [FK_TaxCategories_Countries_CountryID]
GO
ALTER TABLE [dbo].[TaxCategoryItems]  WITH CHECK ADD  CONSTRAINT [FK_TaxCategoryItems_TaxCategories_TaxCategoryId] FOREIGN KEY([TaxCategoryId])
REFERENCES [dbo].[TaxCategories] ([Id])
GO
ALTER TABLE [dbo].[TaxCategoryItems] CHECK CONSTRAINT [FK_TaxCategoryItems_TaxCategories_TaxCategoryId]
GO
ALTER TABLE [dbo].[TaxCategoryItems]  WITH CHECK ADD  CONSTRAINT [FK_TaxCategoryItems_TaxItems_TaxItemId] FOREIGN KEY([TaxItemId])
REFERENCES [dbo].[TaxItems] ([Id])
GO
ALTER TABLE [dbo].[TaxCategoryItems] CHECK CONSTRAINT [FK_TaxCategoryItems_TaxItems_TaxItemId]
GO
ALTER TABLE [dbo].[WishLists]  WITH CHECK ADD  CONSTRAINT [FK_WishLists_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[WishLists] CHECK CONSTRAINT [FK_WishLists_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[WishLists]  WITH CHECK ADD  CONSTRAINT [FK_WishLists_Audios_AudioId] FOREIGN KEY([AudioId])
REFERENCES [dbo].[Audios] ([Id])
GO
ALTER TABLE [dbo].[WishLists] CHECK CONSTRAINT [FK_WishLists_Audios_AudioId]
GO
